# Full list of game credits

## General credits

Game by EugeneLoza

Created in Castle Game Engine (https://castle-engine.io/)

## Special thanks

First of all huge thanks to Michalis Kamburelis, Author of Casatle Game Engine, for a lot of support, consultations and other contributions to the project, not mentioning for making the best game engine in the world for the best programming language in the world :)

A special thanks also belongs to all those who downloaded and played the game - it might not seem much, but you should know that this project exists for you and thanks to you it has made this far.

I'm really scared to miss some nicknames - all the feedback provided is extremely valuable to me, but I'd really like to add a special thank-you for those players actively providing feedback and testing for the game (in no particular order):

WildObsidian, Aoife Ó Dubhuir, Naz, Dekryl, Ezofuji the kitsune, Fiffy, R3c0N, Kvolik, beeceen, Arirni Mation, Reizi Tigerfish, flamespecter, Shadow4545, RappyTheToy, Foxso Funso, BenTheUltimateLegend, Evil Operative, bibinha, Impy, Piopio Juegos, summonerof, Deathcomes18, tlandurEa

## References to licenses details

* CC-BY 3.0 - https://creativecommons.org/licenses/by/3.0/
* CC-BY 4.0 - https://creativecommons.org/licenses/by/4.0/
* CC-0 - https://creativecommons.org/publicdomain/zero/1.0/
* SIL Open Font License (OFL) https://openfontlicense.org/

## Third party assets used in the game core

TALS-style (title-author-license-source-changes) credits recommended by Creative Commons https://wiki.creativecommons.org/wiki/Recommended_practices_for_attribution

#### Fonts

/data/ui/fonts/bender/bender.bold_fixed.ttf
/data/ui/fonts/bender/bender.regular_fixed.ttf
SIL Open Font License (OFL) by Oleg Zhuravlev, Gladkikh Ivan
https://www.1001fonts.com/bender-font.html
Minor changes and fixes by EugeneLoza,
changes licensed under CC0

/data/ui/fonts/soniano/soniano-sans-unicode.regular_fixed.ttf
SIL Open Font License (OFL) by Jayvee D. Enaguas
https://www.1001fonts.com/soniano-sans-unicode-font.html
Minor changes and fixes by EugeneLoza
licensed under CC0

#### Graphical assets

/data/core/enter.png
by RL-Tiles and Dungeon Crawl Stone Soup contributors
licensed under CC0
https://github.com/crawl/crawl/tree/master/crawl-ref/source/rltiles
Minor changes by EugeneLoza
changes licensed under CC0

/data/map/tileset
by RL-Tiles and Dungeon Crawl Stone Soup contributors
https://github.com/crawl/crawl/tree/master/crawl-ref/source/rltiles
licensed under CC0
Colorized by EugeneLoza
changes licensed under CC0

/data/core/light-thorny-triskelion_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/ui/background/vintage-retro-papier-hintergrund_cc0_by_Andrea_Stockel[c2b261e].jpg
CC0 by Andrea Stöckel
https://www.publicdomainpictures.net/en/view-image.php?image=390603
Colorized by EugeneLoza
changes licensed under CC0

/data/ui/castle-engine/castle_game_engine_icon.png
Copyright 2008-2018 Paweł Wojciechowicz, Michalis Kamburelis, Katarzyna Obrycka.
Distributed on the same license as the Castle Game Engine itself: GNU GPL >= 2 or GNU LGPL >= 2, with static linking exception; Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
https://github.com/castle-engine/castle-engine/tree/master/doc/pasdoc/logo

/data/ui/game-icons/cloak-dagger_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/ui/game-icons/crossed-swords_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/ui/kodiakgraphics/background.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza
changes licensed under CC0

/data/ui/kodiakgraphics/buttonfocus.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/buttonnormal.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/buttonpress.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/camp_focus.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"Campfire" image
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/camp_normal.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"Campfire" image
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/camp_press.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"Campfire" image
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/character_screen_focus.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"person" image
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/character_screen_normal.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"person" image
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/character_screen_pressed.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"person" image
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/fast_forward.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/inventory_button_light_disabled.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/inventory_button_light_focus.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/inventory_button_light_normal.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/inventory_button_light_press.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/inventory_button_light_selected_disabled.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/inventory_button_light_selected_focus.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/inventory_button_light_selected_normal.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/inventory_button_light_selected_press.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/inventory_hide_focus.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized, added text by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/inventory_hide_normal.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized, added text by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/inventory_hide_press.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized, added text by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/inventory_show_focused.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"Profit" image
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/inventory_show_normal.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"Profit" image
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/inventory_show_pressed.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"Profit" image
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/medium_bar_empty.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/medium_bar_full.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/nopathfinding_disabled.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"path-distance" image
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Significantly modified by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/nopathfinding_focus.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"path-distance" image
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Significantly modified by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/nopathfinding_normal.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"path-distance" image
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Significantly modified by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/nopathfinding_pressed.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"path-distance" image
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Significantly modified by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/party_focus.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"two-shadows" image
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/party_normal.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"two-shadows" image
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/party_press.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"two-shadows" image
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/pathfinding_disabled.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"path-distance" image
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Significantly modified by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/pathfinding_focus.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"path-distance" image
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Significantly modified by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/pathfinding_normal.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"path-distance" image
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Significantly modified by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/pathfinding_pressed.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"path-distance" image
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Significantly modified by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/pause.png
CC0 by EugeneLoza
Drawn based on
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface

/data/ui/kodiakgraphics/pause_focus.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized, added text by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/pausemenu_focus.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"cog" image
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/pausemenu_normal.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"cog" image
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/pausemenu_press.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0
"cog" image
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/pause_normal.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized, added text by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/pause_press.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized, added text by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/scrollbar_background.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/scrollbar_slider.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/stealth_danger_meter.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/stealth_focus.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/stealth_normal.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/stealth_press.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/xp_bar.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/xp_bar_full.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/xp_slider.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/zoom_in_disabled.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized, added text by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/zoom_in_focus.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized, added text by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/zoom_in_normal.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized, added text by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/zoom_in_press.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized, added text by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/zoom_out_disabled.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized, added text by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/zoom_out_focus.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized, added text by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/zoom_out_normal.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized, added text by EugeneLoza,
changes licensed under CC0

/data/ui/kodiakgraphics/zoom_out_press.png
CC0 by KodiakGraphics
https://opengameart.org/content/rpg-user-interface
Cropped, colorized, added text by EugeneLoza,
changes licensed under CC0

## Third party assets used in game packages

### Base game package

#### Graphical assets

/data/packages/base/monsters/binders/images/cloudy-fork_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/binders/images/elysium-shade_CC_BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/binders/images/yellow_mite_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/binders/images/yellow_slime.png
CC0 by pzUH
https://opengameart.org/content/free-platformer-game-tileset
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/binders/images/yellow_aspheroid.png
CC0 by Arvin61r58
https://openclipart.org/detail/239197/black-jelly-fixed
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/binders/marks/curling-vines_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/binders/marks/fishing-net_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/binders/marks/prayer-beads_CC_BY_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/binders/marks/spider-web_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/containers/images/barrel_CC0_by_AntumDeluge_n_Hyptosis.png
CC0 by AntumDeluge/Hyptosis
https://opengameart.org/content/barrels-mage-city-arcanos-rework
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/containers/images/chests_32x32_CC0_by_Blarumyrran[metal-1].png
CC0 by Blarumyrran
https://opengameart.org/content/treasure-chests-32x32-and-16x16
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/containers/images/chests_32x32_CC0_by_Blarumyrran[metal-2].png
CC0 by Blarumyrran
https://opengameart.org/content/treasure-chests-32x32-and-16x16
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/containers/images/chests_32x32_CC0_by_Blarumyrran[wood-1].png
CC0 by Blarumyrran
https://opengameart.org/content/treasure-chests-32x32-and-16x16
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/containers/images/chests_32x32_CC0_by_Blarumyrran[wood-2].png
CC0 by Blarumyrran
https://opengameart.org/content/treasure-chests-32x32-and-16x16
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/containers/marks/fist_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/dream_catcher/images/cabbage_CC-BY_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/ectoghost/marks/foam_CC-BY_by_Lorc[cyan].png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/glassram/images/drop_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/gluebeetle/images/scarab-beetle_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/gluebeetle/images/scarab-beetle_CC-BY_by_Lorc[red].png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/gluebeetle/marks/foam_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/inventory_breakers/images/floating-tentacles_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/inventory_breakers/images/floating-tentacles_CC-BY_by_delapouite[blue].png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/leasher/images/ancient-screw_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/leasher/images/crystal-ball_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/leasher/images/fairy_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/leasher/images/mooring-bollard_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/leasher/images/snail_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/leasher/images/stone-tower_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/leasher/marks/andromeda-chain_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorize by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/leasher/marks/belt-buckles_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorize by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/leasher/marks/bouncing-spring_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/leasher/marks/mailed-fist_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorize by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/leasher/marks/star-swirl_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorize by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/livingsuit/marks/interlaced-tentacles_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorize by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/manacler/images/behold_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/manacler/images/behold_CC-BY_by_Lorc[purple].png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/manacler/images/behold_CC-BY_by_Lorc[transparent].png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/manacler/marks/magic-swirl_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/manacler/marks/manacles_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/medicus/images/harpy_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/medicus/images/harpy_CC-BY_by_Lorc[yellow].png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/medicus/marks/bleeding-heart_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/pinchers/images/sad-crab_CC-BY_by_Lorc[red].png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/pinchers/images/sad-crab_CC-BY_by_Lorc[yellow].png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/pinchers/marks/arrow-dunk_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/pinchers/marks/crab-claw_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/quareller/images/fleur-de-lys_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/quareller/marks/divided-square_CC-BY_by_skoll.png
by Skoll
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/repair_thief/images/clockwork_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/repair_thief/marks/mighty-spanner_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/repair_trap/images/raw-egg_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/repair_trap/marks/cog-lock_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/rune_thief/images/heartburn_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/rune_thief/marks/chained-heart_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/rune_thief/marks/flaming-claw_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/slow_shooter/images/cigale_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/slow_shooter/images/intricate-necklace_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/slow_shooter/marks/azul-flake_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/slow_shooter/marks/glass-ball_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/straiter/images/evil-bat_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/straiter/marks/arrow-dunk_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/straiter/marks/grab_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/syringe/images/companion-cube_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/syringe/marks/syringe_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/ticklers/images/cookie_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/ticklers/images/cyan_aspheroid.png
CC0 by Arvin61r58
https://openclipart.org/detail/239197/black-jelly-fixed
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/ticklers/images/cyan_mite_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/ticklers/images/cyan_slime.png
CC0 by pzUH
https://opengameart.org/content/free-platformer-game-tileset
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/ticklers/images/infested-mass_CC_BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/ticklers/images/suckered-tentacle_CC-BY_by_lorc[monster].png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/ticklers/marks/magic-swirl_CC-BY_by_Lorc[cyan].png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/ticklers/marks/suckered-tentacle_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/twinkle/marks/lightning-trio_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/alien-egg_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/black_aspheroid.png
CC0 by Arvin61r58
https://openclipart.org/detail/239197/black-jelly-fixed
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/blackball_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/black-hole-bolas_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/black-hole-bolas_CC-BY_by_Lorc[blue].png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/black-hole-bolas_CC-BY_by_Lorc[gray].png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/black-hole-bolas_CC-BY_by_Lorc[green].png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/black_mite_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/black_slime.png
CC0 by pzUH
https://opengameart.org/content/free-platformer-game-tileset
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/blue_aspheroid.png
CC0 by Arvin61r58
https://openclipart.org/detail/239197/black-jelly-fixed
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/blue_mite_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/blue_slime.png
CC0 by pzUH
https://opengameart.org/content/free-platformer-game-tileset
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/bull-horns_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/bull-horns_CC-BY_by_Lorc[pale].png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/dandelion-flower_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/dice-target_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/embryo_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/embryo_CC-BY_by_Lorc[yellow].png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/foam_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/gooey-eyed-sun_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/gray_slime.png
CC0 by pzUH
https://opengameart.org/content/free-platformer-game-tileset
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/green_aspheroid.png
CC0 by Arvin61r58
https://openclipart.org/detail/239197/black-jelly-fixed
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/green_slime.png
CC0 by pzUH
https://opengameart.org/content/free-platformer-game-tileset
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/processor_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/purple_aspheroid.png
CC0 by Arvin61r58
https://openclipart.org/detail/239197/black-jelly-fixed
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/purple_slime.png
CC0 by pzUH
https://opengameart.org/content/free-platformer-game-tileset
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/red_aspheroid.png
CC0 by Arvin61r58
https://openclipart.org/detail/239197/black-jelly-fixed
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/red_mite_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/red_slime.png
CC0 by pzUH
https://opengameart.org/content/free-platformer-game-tileset
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/reticule_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/ringing-bell_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/spiked-ball_CC-BY_by_skoll.png
by Skoll
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/thief_slime.png
CC0 by pzUH
https://opengameart.org/content/free-platformer-game-tileset
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/tick_CC-BY-by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/white_aspheroid.png
CC0 by Arvin61r58
https://openclipart.org/detail/239197/black-jelly-fixed
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/white_slime.png
CC0 by pzUH
https://opengameart.org/content/free-platformer-game-tileset
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/images/white_mite_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/algae_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/atom-core_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/beveled-star_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/bowen-knot_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/broken-heart-zone+run_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/bubbles_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/dice-six-faces-six_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/divided-square_CC-BY_by_skoll.png
by Skoll
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/fangs_CC-BY_by_Skoll.png
by Skoll
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/skoll/fangs.html
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/fangs-circle_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/fishing-hook_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/flamed-leaf_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/gooey-eyed-sun_CC-BY_by_lorc[projectile].png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/gooey-molecule_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/hand_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/lamprey-mouth_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/lightning-spanner_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/long-antennae-bug_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/quicksand_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/sharp-lips_CC-BY-by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/snatch_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/targeting_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/transportation-rings_CC-BY_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_monsters/marks/whip_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_traps/marks/broken-heart-zone+run_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_traps/marks/gooey-molecule_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_traps/marks/lamprey-mouth_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_traps/marks/quicksand_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/unsorted_traps/marks/transportation-rings_CC-BY_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vaccum_stealer/images/green_mite_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vaccum_stealer/images/triton-head_CC-BY_by_Lorc[cyan].png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vaccum_stealer/marks/boomerang-cross_CC-BY_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vacuum_cleaner/images/triton-head_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vacuum_cleaner/marks/quicksand_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vampires/images/angel-outfit_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vampires/images/batwing-emblem_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vampires/images/boss-key_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vampires/images/delighted_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vampires/images/haunting_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vampires/images/imp_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vampires/images/winged-emblem_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vampires/marks/bleeding-eye_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vampires/marks/heart-wings_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vampires/marks/pretty-fangs_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vibrators/images/chess-pawn_CC-BY_by_skoll.png
by Skoll
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/skoll/fangs.html
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vibrators/items/marks/love-mystery_CC-BY_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vibrators/marks/baton_CC-BY_by_skol.png
by Skoll
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/skoll/fangs.html
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/vibrators/marks/self-love_CC-BY_by_lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/web_shooter/images/black-hole-bolas_CC-BY_by_Lorc[purple].png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/web_shooter/images/purple_mite_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/web_shooter/marks/spider-web_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/wrapper/images/wool_CC-BY_by_delapouite.png
by Delapouite
licensed under CC-BY 3.0
Available on https://game-icons.net/
Colorized, applied filters by EugeneLoza
changes licensed under CC0

/data/packages/base/monsters/wrapper/marks/twin-shell_CC-BY_by_Lorc.png
by Lorc
licensed under CC-BY 3.0
Available on https://game-icons.net/1x1/lorc/targeting.html
Colorized by EugeneLoza
changes licensed under CC0

#### Music assets

/data/packages/base/music/audionautix/captured/HouseOfEvil_CC_BY_by_Audionautix.ogg
CC-BY 4.0
Creative Commons Music by Jason Shaw on Audionautix.com
https://audionautix.com/creative-commons-music
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/audionautix/dungeon-hard/DarkMystery.ogg
CC-BY 4.0
Creative Commons Music by Jason Shaw on Audionautix.com
https://audionautix.com/creative-commons-music
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/audionautix/dungeon-hard/DestinationUnknown.ogg
CC-BY 4.0
Creative Commons Music by Jason Shaw on Audionautix.com
https://audionautix.com/creative-commons-music
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/audionautix/dungeon-hard/SearchAndDestroy.ogg
CC-BY 4.0
Creative Commons Music by Jason Shaw on Audionautix.com
https://audionautix.com/creative-commons-music
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/audionautix/dungeon-hard/TheVisitors.ogg
CC-BY 4.0
Creative Commons Music by Jason Shaw on Audionautix.com
https://audionautix.com/creative-commons-music
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/audionautix/settlement-empty/EmeraldTherapy.ogg
CC-BY 4.0
Creative Commons Music by Jason Shaw on Audionautix.com
https://audionautix.com/creative-commons-music
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/audionautix/settlement-empty/TheDeadlyYear.ogg
CC-BY 4.0
Creative Commons Music by Jason Shaw on Audionautix.com
https://audionautix.com/creative-commons-music
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/audionautix/settlement-empty/TheTalk.ogg
CC-BY 4.0
Creative Commons Music by Jason Shaw on Audionautix.com
https://audionautix.com/creative-commons-music
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/audionautix/settlement-full/AcousticGuitar1.ogg
CC-BY 4.0
Creative Commons Music by Jason Shaw on Audionautix.com
https://audionautix.com/creative-commons-music
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/audionautix/settlement-full/AcousticShuffle.ogg
CC-BY 4.0
Creative Commons Music by Jason Shaw on Audionautix.com
https://audionautix.com/creative-commons-music
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/audionautix/settlement-full/GreenLeaves.ogg
CC-BY 4.0
Creative Commons Music by Jason Shaw on Audionautix.com
https://audionautix.com/creative-commons-music
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/audionautix/settlement-full/OneFineDay.ogg
CC-BY 4.0
Creative Commons Music by Jason Shaw on Audionautix.com
https://audionautix.com/creative-commons-music
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/audionautix/settlement-half-empty/AMomentsReflection.ogg
CC-BY 4.0
Creative Commons Music by Jason Shaw on Audionautix.com
https://audionautix.com/creative-commons-music
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/audionautix/settlement-half-empty/Clouds[1].ogg
CC-BY 4.0
Creative Commons Music by Jason Shaw on Audionautix.com
https://audionautix.com/creative-commons-music
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/audionautix/settlement-half-empty/Clouds[2].ogg
CC-BY 4.0
Creative Commons Music by Jason Shaw on Audionautix.com
https://audionautix.com/creative-commons-music
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/audionautix/settlement-half-empty/ColdMorning.ogg
CC-BY 4.0
Creative Commons Music by Jason Shaw on Audionautix.com
https://audionautix.com/creative-commons-music
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/audionautix/title/Essence2.ogg
CC-BY 4.0
Creative Commons Music by Jason Shaw on Audionautix.com
https://audionautix.com/creative-commons-music
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/maxstack/dungeon-easy/advanced_simulacra.ogg
CC-BY-SA 3.0 by MaxStack
https://opengameart.org/users/maxstack
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/maxstack/dungeon-easy/awakening.ogg
CC-BY-SA 3.0 by MaxStack
https://opengameart.org/users/maxstack
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/maxstack/dungeon-easy/bazaarnet.ogg
CC-BY-SA 3.0 by MaxStack
https://opengameart.org/users/maxstack
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/maxstack/dungeon-easy/dead_city.ogg
CC-BY-SA 3.0 by MaxStack
https://opengameart.org/users/maxstack
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/maxstack/dungeon-easy/deprecation.ogg
CC-BY-SA 3.0 by MaxStack
https://opengameart.org/users/maxstack
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/maxstack/dungeon-easy/inevitable.ogg
CC-BY-SA 3.0 by MaxStack
https://opengameart.org/users/maxstack
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/maxstack/dungeon-easy/mediathreat.ogg
CC-BY-SA 3.0 by MaxStack
https://opengameart.org/users/maxstack
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/maxstack/dungeon-easy/nebula.ogg
CC-BY-SA 3.0 by MaxStack
https://opengameart.org/users/maxstack
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/maxstack/dungeon-easy/restoration_completed.ogg
CC-BY-SA 3.0 by MaxStack
https://opengameart.org/users/maxstack
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/maxstack/dungeon-easy/the_client.ogg
CC-BY-SA 3.0 by MaxStack
https://opengameart.org/users/maxstack
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/maxstack/dungeon-easy/through_space.ogg
CC-BY-SA 3.0 by MaxStack
https://opengameart.org/users/maxstack
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/maxstack/dungeon-easy/win.ogg
CC-BY-SA 3.0 by MaxStack
https://opengameart.org/users/maxstack
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/maxstack/dungeon-hard/ROAM_Hold_The_Fort.ogg
CC-BY-SA 3.0 by MaxStack
https://opengameart.org/users/maxstack
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/trevorlentz/dungeon-easy/New_S-Type_song.ogg
CC-BY-SA by Trevor Lentz
https://opengameart.org/users/trevor-lentz
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/trevorlentz/dungeon-hard/08_Portal.ogg
CC-BY-SA by Trevor Lentz
https://opengameart.org/users/trevor-lentz
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/trevorlentz/dungeon-hard/Biohazard.ogg
CC-BY-SA by Trevor Lentz
https://opengameart.org/users/trevor-lentz
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/trevorlentz/dungeon-hard/dungeon.ogg
CC-BY-SA by Trevor Lentz
https://opengameart.org/users/trevor-lentz
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/trevorlentz/dungeon-hard/ethereal_realms.ogg
CC-BY-SA by Trevor Lentz
https://opengameart.org/users/trevor-lentz
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/trevorlentz/dungeon-hard/Stasis.ogg
CC-BY-SA by Trevor Lentz
https://opengameart.org/users/trevor-lentz
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/trevorlentz/dungeon-hard/Temple_boss.ogg
CC-BY-SA by Trevor Lentz
https://opengameart.org/users/trevor-lentz
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/trevorlentz/dungeon-hard/undead_chase.ogg
CC-BY-SA by Trevor Lentz
https://opengameart.org/users/trevor-lentz
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/music/trevorlentz/settlement-empty/mystic_temple.ogg
CC-BY-SA by Trevor Lentz
https://opengameart.org/users/trevor-lentz
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

#### Sounds assets

/data/packages/base/monsters/binders/sounds/391946__ssierra1202__ropes_CC0_by_ssierra1202[mixed].wav
CC0 by ssierra1202
https://freesound.org/people/ssierra1202/sounds/391946/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/containers/sounds/rubberduck/wooded_box_open_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/containers/sounds/rubberduck/wood_slam_03_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/inventory_breakers/sounds/noise_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/inventory_breakers/sounds/noise_02_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/inventory_breakers/sounds/weird_06_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/dream_catcher/sounds/460477__djczyszy__chimes-overheads_004_CC0_by_DJczyszy[crop].ogg
CC0 by DJczyszy
https://freesound.org/people/DJczyszy/sounds/460477/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/dream_catcher/sounds/460478__djczyszy__chimes-overheads_003_CC0_by_DJczyszy.ogg
CC0 by DJczyszy
https://freesound.org/people/DJczyszy/sounds/460478/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/dream_catcher/sounds/460479__djczyszy__chimes-overheads_002_CC0_by_DJczyszy[crop].ogg
CC0 by DJczyszy
https://freesound.org/people/DJczyszy/sounds/460479/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/dream_catcher/sounds/460481__djczyszy__chimes-overheads_006_CC0_by_DJczyszy[crop].ogg
CC0 by DJczyszy
https://freesound.org/people/DJczyszy/sounds/460481/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/dream_catcher/sounds/460482__djczyszy__chimes-overheads_005_CC0_by_DJczyszy.ogg
CC0 by DJczyszy
https://freesound.org/people/DJczyszy/sounds/460482/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/item_misc_05_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/item_misc_05_CC0_by_rubberduck[reverse].wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/item_misc_06_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/item_misc_06_CC0_by_rubberduck[reverse].wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/sfx_04a_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/sfx_04b_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/sfx_04c_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/sfx_09a_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/sfx_09a_CC0_by_rubberduck[reverse].wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/sfx_09b_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/sfx_09b_CC0_by_rubberduck[reverse].wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/sfx_14a_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/sfx_14b_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/sfx_14c_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/weird_05_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/weird_05_CC0_by_rubberduck[reverse].wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/mickdow/253327__mickdow__rubber-stretch-rip-1_CC0_by_mickdow[3].wav
CC0 by mickdow
https://freesound.org/people/mickdow/sounds/253327/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/mickdow/253327__mickdow__rubber-stretch-rip-1_CC0_by_mickdow[7short].wav
CC0 by mickdow
https://freesound.org/people/mickdow/sounds/253327/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/mickdow/253327__mickdow__rubber-stretch-rip-1_CC0_by_mickdow[11].wav
CC0 by mickdow
https://freesound.org/people/mickdow/sounds/253327/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/mickdow/253327__mickdow__rubber-stretch-rip-1_CC0_by_mickdow[12].wav
CC0 by mickdow
https://freesound.org/people/mickdow/sounds/253327/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/mickdow/253327__mickdow__rubber-stretch-rip-1_CC0_by_mickdow[17].wav
CC0 by mickdow
https://freesound.org/people/mickdow/sounds/253327/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/mickdow/253327__mickdow__rubber-stretch-rip-1_CC0_by_mickdow[17][reverse].wav
CC0 by mickdow
https://freesound.org/people/mickdow/sounds/253327/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/mickdow/253327__mickdow__rubber-stretch-rip-1_CC0_by_mickdow[18].wav
CC0 by mickdow
https://freesound.org/people/mickdow/sounds/253327/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/mickdow/253327__mickdow__rubber-stretch-rip-1_CC0_by_mickdow[18][reverse].wav
CC0 by mickdow
https://freesound.org/people/mickdow/sounds/253327/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/mickdow/253327__mickdow__rubber-stretch-rip-1_CC0_by_mickdow[19].wav
CC0 by mickdow
https://freesound.org/people/mickdow/sounds/253327/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/mickdow/253327__mickdow__rubber-stretch-rip-1_CC0_by_mickdow[19][reverse].wav
CC0 by mickdow
https://freesound.org/people/mickdow/sounds/253327/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/quareller/sounds/515625__mrickey13__throwswipe_CC0_by_mrickey13.wav
CC0 by mrickey13
https://freesound.org/people/mrickey13/sounds/515625/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/rune_thief/sounds/weird_20_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/rune_thief/sounds/weird_20_CC0_by_rubberduck[reverse].wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/slow_shooter/sounds/stomp_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/slow_shooter/sounds/weird_21_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/straiter/sounds/synth_misc_07_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/syringe/sounds/weird_13_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/twinkle/sounds/chain_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/twinkle/sounds/chain_03_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/twinkle/sounds/sfx_05a_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/vampires/sounds/creature_misc_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/vampires/sounds/weird_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/vibrators/sounds/253327__mickdow__rubber-stretch-rip-1_CC0_by_mickdow[1].wav
CC0 by mickdow
https://freesound.org/people/mickdow/sounds/253327/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/vibrators/sounds/253327__mickdow__rubber-stretch-rip-1_CC0_by_mickdow[2].wav
CC0 by mickdow
https://freesound.org/people/mickdow/sounds/253327/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/vibrators/sounds/253327__mickdow__rubber-stretch-rip-1_CC0_by_mickdow[8].wav
CC0 by mickdow
https://freesound.org/people/mickdow/sounds/253327/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/vibrators/sounds/253327__mickdow__rubber-stretch-rip-1_CC0_by_mickdow[9].wav
CC0 by mickdow
https://freesound.org/people/mickdow/sounds/253327/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/vibrators/sounds/rubberduck/machine_02_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/vibrators/sounds/rubberduck/machine_04_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/vibrators/sounds/rubberduck/machine_04_CC0_by_rubberduck[lowerpitch].wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/vibrators/sounds/rubberduck/machine_05_CC0_by_rubberduck[cutfade].wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/vibrators/sounds/rubberduck/machine_10_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/web_shooter/sounds/mud_09_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/web_shooter/sounds/mud_22_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/monsters/leasher/sounds/metalClick+metalLatch_CC0_by_kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/x01-133440__reg7783__cloth-ripping_CC0_by_reg7783.wav
CC0 by reg7783
https://freesound.org/people/reg7783/sounds/133440/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/x03-133440__reg7783__cloth-ripping_CC0_by_reg7783.wav
CC0 by reg7783
https://freesound.org/people/reg7783/sounds/133440/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/x07-174627__altfuture__ripping-clothes_CC0_by_altfuture.wav
CC0 by altfuture
https://freesound.org/people/altfuture/sounds/174627/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/y01-133440__reg7783__cloth-ripping_CC0_by_reg7783.wav
CC0 by reg7783
https://freesound.org/people/reg7783/sounds/133440/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/y02-133440__reg7783__cloth-ripping_CC0_by_reg7783.wav
CC0 by reg7783
https://freesound.org/people/reg7783/sounds/133440/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/y03-133440__reg7783__cloth-ripping_CC0_by_reg7783.wav
CC0 by reg7783
https://freesound.org/people/reg7783/sounds/133440/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/y06-174627__altfuture__ripping-clothes_CC0_by_altfuture.wav
CC0 by altfuture
https://freesound.org/people/altfuture/sounds/174627/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/y07-174627__altfuture__ripping-clothes_CC0_by_altfuture.wav
CC0 by altfuture
https://freesound.org/people/altfuture/sounds/174627/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/y08-449126__harpyharpharp__fabric-ripping_CC0_by_HarpyHarpHarp.wav
CC0 by HarpyHarpHarp
https://freesound.org/people/HarpyHarpHarp/sounds/449126/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/y09-449126__harpyharpharp__fabric-ripping_CC0_by_HarpyHarpHarp.wav
CC0 by HarpyHarpHarp
https://freesound.org/people/HarpyHarpHarp/sounds/449126/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/y11-449126__harpyharpharp__fabric-ripping_CC0_by_HarpyHarpHarp.wav
CC0 by HarpyHarpHarp
https://freesound.org/people/HarpyHarpHarp/sounds/449126/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/z01-133440__reg7783__cloth-ripping_CC0_by_reg7783.wav
CC0 by reg7783
https://freesound.org/people/reg7783/sounds/133440/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/z02-133440__reg7783__cloth-ripping_CC0_by_reg7783.wav
CC0 by reg7783
https://freesound.org/people/reg7783/sounds/133440/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/z03-133440__reg7783__cloth-ripping_CC0_by_reg7783.wav
CC0 by reg7783
https://freesound.org/people/reg7783/sounds/133440/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/z03b-133440__reg7783__cloth-ripping_CC0_by_reg7783.wav
CC0 by reg7783
https://freesound.org/people/reg7783/sounds/133440/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/z04-174627__altfuture__ripping-clothes_CC0_by_altfuture.wav
CC0 by altfuture
https://freesound.org/people/altfuture/sounds/174627/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/z05-174627__altfuture__ripping-clothes_CC0_by_altfuture.wav
CC0 by altfuture
https://freesound.org/people/altfuture/sounds/174627/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/z06-174627__altfuture__ripping-clothes_CC0_by_altfuture.wav
CC0 by altfuture
https://freesound.org/people/altfuture/sounds/174627/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/z07-174627__altfuture__ripping-clothes_CC0_by_altfuture.wav
CC0 by altfuture
https://freesound.org/people/altfuture/sounds/174627/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/z08-449126__harpyharpharp__fabric-ripping_CC0_by_HarpyHarpHarp.wav
CC0 by HarpyHarpHarp
https://freesound.org/people/HarpyHarpHarp/sounds/449126/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/z09-449126__harpyharpharp__fabric-ripping_CC0_by_HarpyHarpHarp.wav
CC0 by HarpyHarpHarp
https://freesound.org/people/HarpyHarpHarp/sounds/449126/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/z10-449126__harpyharpharp__fabric-ripping_CC0_by_HarpyHarpHarp.wav
CC0 by HarpyHarpHarp
https://freesound.org/people/HarpyHarpHarp/sounds/449126/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/z11-449126__harpyharpharp__fabric-ripping_CC0_by_HarpyHarpHarp.wav
CC0 by HarpyHarpHarp
https://freesound.org/people/HarpyHarpHarp/sounds/449126/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_rip/rip/z12-449126__harpyharpharp__fabric-ripping_CC0_by_HarpyHarpHarp.wav
CC0 by HarpyHarpHarp
https://freesound.org/people/HarpyHarpHarp/sounds/449126/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/ifm123/500330__ifm123__awesome-kick-drum_CC0_by_IFM123.wav
CC0 by IFM123
https://freesound.org/people/IFM123/sounds/500330/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/ifm123/500330__ifm123__awesome-kick-drum_CC0_by_IFM123_twice.wav
CC0 by IFM123
https://freesound.org/people/IFM123/sounds/500330/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/kenney/bookFlip2_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/51-ui-sound-effects-buttons-switches-and-clicks
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/kenney/bookFlip3_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/51-ui-sound-effects-buttons-switches-and-clicks
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/kenney/clothBelt2_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/51-ui-sound-effects-buttons-switches-and-clicks
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/kenney/clothBelt_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/51-ui-sound-effects-buttons-switches-and-clicks
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/kenney/footstep00+others_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/51-ui-sound-effects-buttons-switches-and-clicks
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/kenney/switch1_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/51-ui-sound-effects-buttons-switches-and-clicks
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/kenney/switch2_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/51-ui-sound-effects-buttons-switches-and-clicks
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/kenney/switch3_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/51-ui-sound-effects-buttons-switches-and-clicks
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/kenney/switch16_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/51-ui-sound-effects-buttons-switches-and-clicks
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/kenney/switch17_CC0_by_kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/51-ui-sound-effects-buttons-switches-and-clicks
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/kenney/switch29_CC0_by_kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/51-ui-sound-effects-buttons-switches-and-clicks
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/kenney/switch33_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/51-ui-sound-effects-buttons-switches-and-clicks
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/kenney/switch34_CC0_by_kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/51-ui-sound-effects-buttons-switches-and-clicks
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/kenney/switch37_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/51-ui-sound-effects-buttons-switches-and-clicks
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/kenney/switch38_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/51-ui-sound-effects-buttons-switches-and-clicks
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/rubberduck/bell_01_CC0_by_rubberduck.wav
CC0 sounds by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/rubberduck/sfx100v2_misc_37_CC0_by_rubberduck.wav
CC0 sounds by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/subspaceaudio/sfx_sounds_impact9_CC0_by_SubspaceAudio.wav
CC0 by Juhani Junkala (SubspaceAudio)
https://opengameart.org/content/512-sound-effects-8-bit-style
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/subspaceaudio/sfx_sounds_impact9_CC0_by_SubspaceAudio[crop].wav
CC0 by Juhani Junkala (SubspaceAudio)
https://opengameart.org/content/512-sound-effects-8-bit-style
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/subspaceaudio/sfx_sounds_powerup11_CC0_by_SubspaceAudio.wav
CC0 by Juhani Junkala (SubspaceAudio)
https://opengameart.org/content/512-sound-effects-8-bit-style
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_ui/ueffects/207865__ueffects__camera-focusing-and-shutter_CC0_by_uEffects.wav
CC0 by uEffects
https://freesound.org/people/uEffects/sounds/207865/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/kenney/beltHandle2_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/kenney/cloth2_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/kenney/cloth3_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/kenney/cloth4_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/kenney/drawKnife1_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/kenney/drawKnife2_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/kenney/drawKnife3_CC0_by_Kenney.wav
CC0 by Kenney Vleugels (www.kenney.nl)
https://opengameart.org/content/50-rpg-sound-effects
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/lock/440800__pkri__keys_CC0_by_pkri[1].wav
CC0 by pkri
https://freesound.org/people/pkri/sounds/440800/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/lock/440800__pkri__keys_CC0_by_pkri[2].wav
CC0 by pkri
https://freesound.org/people/pkri/sounds/440800/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/lock/440800__pkri__keys_CC0_by_pkri[3].wav
CC0 by pkri
https://freesound.org/people/pkri/sounds/440800/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/lock/440800__pkri__keys_CC0_by_pkri[4].wav
CC0 by pkri
https://freesound.org/people/pkri/sounds/440800/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/lock/440800__pkri__keys_CC0_by_pkri[5].wav
CC0 by pkri
https://freesound.org/people/pkri/sounds/440800/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/other/142348__branrainey__cartoony-whooshes_CC0_by_BranRainey[nopauses].wav
CC0 by BranRainey
https://freesound.org/people/BranRainey/sounds/142348/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/other/248027__sophiehall3535__grab-keys_CC0_by_sophiehall3535.wav
CC0 by sophiehall3535
https://freesound.org/people/CJspellsfish/sounds/609569/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/other/529925__scifisounds__whip-crack_CC0_by_SciFiSounds[cut].wav
CC0 by CJspellsfish
https://freesound.org/people/sophiehall3535/sounds/248027/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/other/609569__cjspellsfish__grabitemcloth_CC0_by_CJspellsfish.wav
CC0 by SciFiSounds
https://freesound.org/people/SciFiSounds/sounds/529925/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/subspaceaudio/sfx_alarm_loop4_CC0_by_Juhani_Junkala.wav
CC0 by Juhani Junkala
https://opengameart.org/content/512-sound-effects-8-bit-style
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/subspaceaudio/sfx_alarm_loop5_CC0_by_Juhani_Junkala.wav
CC0 by Juhani Junkala
https://opengameart.org/content/512-sound-effects-8-bit-style
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/subspaceaudio/sfx_deathscream_alien3_CC0_by_Juhani_Junkala.wav
CC0 by Juhani Junkala
https://opengameart.org/content/512-sound-effects-8-bit-style
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/subspaceaudio/sfx_deathscream_alien4_CC0_by_Juhani_Junkala.wav
CC0 by Juhani Junkala
https://opengameart.org/content/512-sound-effects-8-bit-style
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/subspaceaudio/sfx_deathscream_android1_CC0_by_Juhani_Junkala.wav
CC0 by Juhani Junkala
https://opengameart.org/content/512-sound-effects-8-bit-style
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/bell_02+bang_10_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/bell_02+weird_19_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/fw_02_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/keys_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/keys_04_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/keys_05_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/keys_06_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/keys_07_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/metal_08_CC0_by_rubberduck[reverb].wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/metal_close_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/metal_falling_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/metal_open_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/weird_12_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/weird_18_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/weird_19_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/alarm/beep_01+02+03_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/alarm/weird_06[copy-copy]_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/amulets/item_stone_02_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/amulets/item_stone_03_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/amulets/item_stone_04_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/amulets/stones_04_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/anesthetic/sfx_06_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/anesthetic/sfx_18a_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/anesthetic/sfx_06_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/anesthetic/sfx_18a_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/bugs/bug_05_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/crafters/synth_misc_14_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/crafters/synth_misc_15_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/crafters/weird_11_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/dice/stones_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/dice/stones_02_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/dice/stones_03_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/eye_shooter/retro_die_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/eye_shooter/retro_die_03_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/eye_shooter/retro_explosion_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/eye_shooter/retro_explosion_02_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/eye_shooter/retro_explosion_03_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/ghost/sfx_12a_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/ghost/sfx_12b_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/ghost/sfx_12c_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/ghost/weird_05_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/glass/bfh1_glass_breaking_05_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/glass/bfh1_glass_breaking_06_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/glass/mud_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/grab/weird_10_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/grab/weird_14_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/grab/weird_15_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/grab/weird_25_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/lock/tools_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/lock/tools_02_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/lock/tools_03_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/lock/tools_04_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/lock/tools_05_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/lock/tools_07_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/lock/tools_09_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/lock/tools_12_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/metal/bfh1_metal_hit_02_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/metal/metal_sheet_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/metal-hollow/metal_slam_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/metal-hollow/misc_06_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/punch/wood_hit_08_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/ram/slam_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/shooter/mud_02_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/shooter/mud_03_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/shooter/plop_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/shooter/plop_02_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/shooter/slime_09_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/slimes/slime_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/slimes/slime_03_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/slimes/slime_07_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/slimes/slime_08_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/slimes/slime_15_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/slimes/slime_16_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/slimes/splash_04_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/slimes/splash_07_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/slimes/splash_08_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/spawner_trap/bfh1_metal_falling_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/steal/bfh1_breaking_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/steal/weird_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/vampire/creature_misc_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/wisps/sfx_01a_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/wisps/sfx_01b_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/wisps/sfx_01c_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/wisps/sfx_16a_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/wisps/sfx_16b_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/wood/bfh1_hit_06_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/wood/sfx100v2_wood_hit_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/wood/wood_cracking_03_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/wood/wood_squeak_01_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/wood/wood_squeak_02_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/sounds_unsorted/rubberduck/zero_g/sfx_18b_CC0_by_rubberduck.wav
CC0 by RubberDuck
https://opengameart.org/users/rubberduck
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_breath/151215__owlstorm__female-breathing-heavily-2_CC0_by_OwlStorm[1].wav
CC0 by OwlStorm (Ashe Kirk / Owlish Media)
https://freesound.org/people/OwlStorm/sounds/151215/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_breath/151215__owlstorm__female-breathing-heavily-2_CC0_by_OwlStorm[2].wav
CC0 by OwlStorm (Ashe Kirk / Owlish Media)
https://freesound.org/people/OwlStorm/sounds/151215/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_breath/151215__owlstorm__female-breathing-heavily-2_CC0_by_OwlStorm[3].wav
CC0 by OwlStorm (Ashe Kirk / Owlish Media)
https://freesound.org/people/OwlStorm/sounds/151215/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_breath/151215__owlstorm__female-breathing-heavily-2_CC0_by_OwlStorm[4].wav
CC0 by OwlStorm (Ashe Kirk / Owlish Media)
https://freesound.org/people/OwlStorm/sounds/151215/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_breath/151215__owlstorm__female-breathing-heavily-2_CC0_by_OwlStorm[5].wav
CC0 by OwlStorm (Ashe Kirk / Owlish Media)
https://freesound.org/people/OwlStorm/sounds/151215/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_giggle/235165__reitanna__giggle4.wav
CC0 by Reitanna
https://freesound.org/people/Reitanna/sounds/235165/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_giggle/235166__reitanna__giggle3.wav
CC0 by Reitanna
https://freesound.org/people/Reitanna/sounds/235166/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_giggle/235167__reitanna__giggle2.wav
CC0 by Reitanna
https://freesound.org/people/Reitanna/sounds/235167/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_giggle/235168__reitanna__giggle.wav
CC0 by Reitanna
https://freesound.org/people/Reitanna/sounds/235168/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_giggle/242905__reitanna__giggle.wav
CC0 by Reitanna
https://freesound.org/people/Reitanna/sounds/242905/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_giggle/252218__reitanna__cute-giggle.wav
CC0 by Reitanna
https://freesound.org/people/Reitanna/sounds/252218/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_giggle/252234__reitanna__small-giggle.wav
CC0 by Reitanna
https://freesound.org/people/Reitanna/sounds/252234/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_giggle/343902__reitanna__nervous-giggle.wav
CC0 by Reitanna
https://freesound.org/people/Reitanna/sounds/343902/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_giggle/343984__reitanna__giggle10.wav
CC0 by Reitanna
https://freesound.org/people/Reitanna/sounds/343984/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_giggle/343992__reitanna__giggle9.wav
CC0 by Reitanna
https://freesound.org/people/Reitanna/sounds/343992/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_giggle/351170__reitanna__quick-giggle.wav
CC0 by Reitanna
https://freesound.org/people/Reitanna/sounds/351170/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_giggle/635014__singger__giggle.wav
CC0 by Singger
https://freesound.org/people/Singger/sounds/635014/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_grunt/218908__martian__female-grunts-breaths_CC0_by_martian[1].wav
CC0 by martian
https://freesound.org/people/martian/sounds/218908/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_grunt/218908__martian__female-grunts-breaths_CC0_by_martian[2].wav
CC0 by martian
https://freesound.org/people/martian/sounds/218908/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_grunt/218908__martian__female-grunts-breaths_CC0_by_martian[3].wav
CC0 by martian
https://freesound.org/people/martian/sounds/218908/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_grunt/218908__martian__female-grunts-breaths_CC0_by_martian[4].wav
CC0 by martian
https://freesound.org/people/martian/sounds/218908/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_grunt/218908__martian__female-grunts-breaths_CC0_by_martian[5].wav
CC0 by martian
https://freesound.org/people/martian/sounds/218908/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_grunt/218908__martian__female-grunts-breaths_CC0_by_martian[6].wav
CC0 by martian
https://freesound.org/people/martian/sounds/218908/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_grunt/218908__martian__female-grunts-breaths_CC0_by_martian[7].wav
CC0 by martian
https://freesound.org/people/martian/sounds/218908/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_grunt/218908__martian__female-grunts-breaths_CC0_by_martian[8].wav
CC0 by martian
https://freesound.org/people/martian/sounds/218908/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_grunt/218908__martian__female-grunts-breaths_CC0_by_martian[9].wav
CC0 by martian
https://freesound.org/people/martian/sounds/218908/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_grunt/218908__martian__female-grunts-breaths_CC0_by_martian[10].wav
CC0 by martian
https://freesound.org/people/martian/sounds/218908/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_grunt/218908__martian__female-grunts-breaths_CC0_by_martian[11].wav
CC0 by martian
https://freesound.org/people/martian/sounds/218908/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_grunt/218908__martian__female-grunts-breaths_CC0_by_martian[12].wav
CC0 by martian
https://freesound.org/people/martian/sounds/218908/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_hurt/218190__madamvicious__girl-taking-damage_CC0_by_MadamVicious[1].wav
CC0 by MadamVicious
https://freesound.org/people/MadamVicious/sounds/218190/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_hurt/218190__madamvicious__girl-taking-damage_CC0_by_MadamVicious[2].wav
CC0 by MadamVicious
https://freesound.org/people/MadamVicious/sounds/218190/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_hurt/218190__madamvicious__girl-taking-damage_CC0_by_MadamVicious[3].wav
CC0 by MadamVicious
https://freesound.org/people/MadamVicious/sounds/218190/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_hurt/218190__madamvicious__girl-taking-damage_CC0_by_MadamVicious[4].wav
CC0 by MadamVicious
https://freesound.org/people/MadamVicious/sounds/218190/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_hurt/218190__madamvicious__girl-taking-damage_CC0_by_MadamVicious[5].wav
CC0 by MadamVicious
https://freesound.org/people/MadamVicious/sounds/218190/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_hurt/218190__madamvicious__girl-taking-damage_CC0_by_MadamVicious[6].wav
CC0 by MadamVicious
https://freesound.org/people/MadamVicious/sounds/218190/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_hurt/218190__madamvicious__girl-taking-damage_CC0_by_MadamVicious[7].wav
CC0 by MadamVicious
https://freesound.org/people/MadamVicious/sounds/218190/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_hurt/218190__madamvicious__girl-taking-damage_CC0_by_MadamVicious[8].wav
CC0 by MadamVicious
https://freesound.org/people/MadamVicious/sounds/218190/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_hurt/218190__madamvicious__girl-taking-damage_CC0_by_MadamVicious[9].wav
CC0 by MadamVicious
https://freesound.org/people/MadamVicious/sounds/218190/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_surprise/405203__drotzruhn__gasp_CC-BY_by_drotzruhn[1].wav
CC-BY 4.0 by drotzruhn
https://freesound.org/people/drotzruhn/sounds/405203/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_surprise/405203__drotzruhn__gasp_CC-BY_by_drotzruhn[2].wav
CC-BY 4.0 by drotzruhn
https://freesound.org/people/drotzruhn/sounds/405203/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_surprise/405203__drotzruhn__gasp_CC-BY_by_drotzruhn[3].wav
CC-BY 4.0 by drotzruhn
https://freesound.org/people/drotzruhn/sounds/405203/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_surprise/405203__drotzruhn__gasp_CC-BY_by_drotzruhn[4].wav
CC-BY 4.0 by drotzruhn
https://freesound.org/people/drotzruhn/sounds/405203/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_surprise/405203__drotzruhn__gasp_CC-BY_by_drotzruhn[5].wav
CC-BY 4.0 by drotzruhn
https://freesound.org/people/drotzruhn/sounds/405203/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_surprise/405203__drotzruhn__gasp_CC-BY_by_drotzruhn[6].wav
CC-BY 4.0 by drotzruhn
https://freesound.org/people/drotzruhn/sounds/405203/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_surprise/405203__drotzruhn__gasp_CC-BY_by_drotzruhn[7].wav
CC-BY 4.0 by drotzruhn
https://freesound.org/people/drotzruhn/sounds/405203/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_surprise/405203__drotzruhn__gasp_CC-BY_by_drotzruhn[8].wav
CC-BY 4.0 by drotzruhn
https://freesound.org/people/drotzruhn/sounds/405203/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_surprise/405203__drotzruhn__gasp_CC-BY_by_drotzruhn[9].wav
CC-BY 4.0 by drotzruhn
https://freesound.org/people/drotzruhn/sounds/405203/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_surprise/467348__nicklas3799__female-gasp_CC-BY_by_nicklas3799.wav
CC-BY 3.0 by nicklas3799
https://freesound.org/people/nicklas3799/sounds/467348/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

/data/packages/base/voice_female/female_surprise/536756__egomassive__gaspf_CC-BY_by_Montblanc_Candies.wav
CC-BY 3.0 by Montblanc Candies (edited by egomassive)
https://freesound.org/people/egomassive/sounds/536756/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

### Halloween package

/data/packages/halloween/items/paperdoll/jack_o_lantern_CC0_by_HorrorPen_modified_by_EugeneLoza.png
CC0 by HorrorPen
https://openclipart.org/detail/164977/halloween-pumpkin
Modified by EugeneLoza
Changes licensed under CC0

/data/packages/halloween/sounds/556419__colorscrimsontears__ghost-wails_CC0_by_colorsCrimsonTears[loop].wav
CC0 by colorsCrimsonTears
https://freesound.org/people/colorsCrimsonTears/sounds/556419/
Cropped, trimmed, normalized by EugeneLoza
Changes licensed under CC0

# These are large-scale tasks to handle before the game can be considered Alpha:

## Mechanics

* Character UI : WIP
* Skills & classes
* Interaction points, action points
* Poses & species : WIP
* ~~Attachment slots (items attached to / stored inside others)~~ Canceled
* External attachments (leashes, anchors)
* Consumable items, potrients

## Maps and render

* Upgrade to renderer
* Animated monsters
* Animated player characters
* Bosses / boss-levels
* Missions
* Building settlement
* Crafting : WIP

## Story and special mechanics

* Player character AI behavior
* Player character out-of-control
* Cutscenes subsystem
* Fractal context: story scripting system
* Monologue UI
* Juna's portrait & body
* Juna's tutorial
* First encounter
* Second encounter

# Optional goals / Beta:

## Mechanics

* Better (more honest) AI
* Sensory orgnas
* ~~Dirt & bathing~~ Canceled
* Smell
* Addictions
* Cooking
* Challenges

## Maps and render

* New map generator logic
* Map tileset editor
* Support for multitileset maps
* ~~Water + wet mechanics~~ Canceled
* More interactive environment, using environment in combat
* Environmental puzzles

## Story and special mechanics

* New game plus
* New game plus plus

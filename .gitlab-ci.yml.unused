# ----------------------------------------------------------------------------
# GitLab CI/CD configuration file to build this application
# using latest Castle Game Engine snapshot.
#
# See https://docs.gitlab.com/ee/ci/yaml/index.html for concise syntax reference of this YAML.
#
# This does *not* use Castle Game Engine Docker image by default
# for the simple reason that on GitLab "shared runner"
# ( https://docs.gitlab.com/ee/ci/runners/runners_scope.html#shared-runners )
# there's not enough disk space.
# But if you want to play with your own runner, uncomment and extend the code below
# using kambi/castle-engine-cloud-builds-tools:cge-none.
# ----------------------------------------------------------------------------

stages:
  - build

# ----------------------------------------------------------------------------

build-windows:
  stage: build
  tags:
    - saas-windows-medium-amd64
  before_script:
    # Install Lazarus (Windows installer includes also FPC) using Chocolatey,
    # https://community.chocolatey.org/packages/lazarus .
    # Shared Windows runners on GitLab include Chocolatey,
    # see https://docs.gitlab.com/ee/development/windows.html and
    # https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/gcp/windows-containers/blob/main/cookbooks/preinstalled-software/README.md
    - choco install lazarus zip -y

    # Place Lazarus and FPC on $PATH
    - $Env:Path += ";c:\lazarus;C:\lazarus\fpc\3.2.2\bin\x86_64-win64;C:\ProgramData\chocolatey\lib\zip\tools"

    # Check everything looks good
    - lazbuild --version
    - fpc -iV
    - echo "Project dir is:" $Env:CI_PROJECT_DIR

    # Get latest CGE and compile the build tool.
    - git clone --depth 1 --single-branch --branch snapshot https://github.com/castle-engine/castle-engine/
    - $Env:CASTLE_ENGINE_PATH = "$Env:CI_PROJECT_DIR\castle-engine"
    - cd $Env:CASTLE_ENGINE_PATH
    - ./tools/build-tool/castle-engine_compile.ps1
    - $Env:Path += ";$Env:CASTLE_ENGINE_PATH\tools\build-tool"
    - castle-engine --version
  script:
    # Main job: actually build the project.
    - cd $Env:CI_PROJECT_DIR
    - ./scripts/version.ps1
    - castle-engine package --os=win64 --cpu=x86_64 --verbose
  artifacts:
    paths:
      - "*-win64-x86_64.zip"
    expire_in: 1 week

# ----------------------------------------------------------------------------
# Commented out version for Linux build
# 400 free CI minutes / month is too little, with build times 10+ minutes, it's only around 30 builds
# Not making Linux builds doesn't double the number but still saves up 1-2 minutes therefore allowing for a couple more Windows builds
# Maybe GitLab can add more minutes to free and open source project, but I haven't had the time to investigate it yet

#build-linux:
#  stage: build
#  tags:
#    - saas-linux-small-amd64
#  before_script:
#    # Unfortunately, installing FPC by simple "apt-get install fpc"
#    # is not good on a GitLab shared runner.
#    # Reason: These runners have old version of FPC 3.0.4 from Debian stable,
#    # while CGE requires FPC >= 3.2.0.
#    # See https://castle-engine.io/supported_compilers.php .
#    # - apt-get update && apt-get --no-install-recommends -y install fpc

#    # Download latest FPC and install it.
#    - wget 'https://sourceforge.net/projects/lazarus/files/Lazarus%20Linux%20amd64%20DEB/Lazarus%202.2.2/fpc-laz_3.2.2-210709_amd64.deb/download' --output-document='fpc-laz_3.2.2-210709_amd64.deb'
#    - apt install './fpc-laz_3.2.2-210709_amd64.deb'
#    # Not necessary for now:
#    #- wget 'https://sourceforge.net/projects/lazarus/files/Lazarus%20Linux%20amd64%20DEB/Lazarus%202.2.2/fpc-src_3.2.2-210709_amd64.deb/download' --output-document='fpc-src_3.2.2-210709_amd64.deb'
#    #- wget 'https://sourceforge.net/projects/lazarus/files/Lazarus%20Linux%20amd64%20DEB/Lazarus%202.2.2/lazarus-project_2.2.2-0_amd64.deb/download' --output-document='lazarus-project_2.2.2-0_amd64.deb'

#    # Check everything looks good
#    #- lazbuild --version # we do not install lazbuild as not necessary
#    - fpc -iV
#    - echo "Project dir is:" $CI_PROJECT_DIR

#    # Install library dependencies for compiling CGE applications.
#    # See https://castle-engine.io/compiling_from_source.php .
#    - apt-get update && apt-get --no-install-recommends -y install libgtk2.0-dev libgl-dev

#    # Get latest CGE and compile the build tool.
#    - git clone --depth 1 --single-branch --branch snapshot https://github.com/castle-engine/castle-engine/
#    - export CASTLE_ENGINE_PATH=$CI_PROJECT_DIR/castle-engine
#    - cd $CASTLE_ENGINE_PATH
#    - ./tools/build-tool/castle-engine_compile.sh
#    - export PATH="$PATH:$CASTLE_ENGINE_PATH/tools/build-tool"
#    - castle-engine --version
#  script:
#    # Main job: actually build the project.
#    - cd $CI_PROJECT_DIR
#    - version.sh -nightly
#    - castle-engine package --os=linux --cpu=x86_64 --verbose
#  artifacts:
#    paths:
#      - "*-linux-x86_64.tar.gz"
#    expire_in: 1 week

# ----------------------------------------------------------------------------
# Commented out version using Docker image
# (will fail on free GitLab CI/CD due to lack of disk space).
#
# build-windows:
#   image: kambi/castle-engine-cloud-builds-tools:cge-none
#   stage: build
#   script:
#     - castle-engine package --os=win64 --cpu=x86_64 --verbose
#   artifacts:
#     paths:
#       - "*-win64-x86_64.zip"
#
# build-linux:
#   image: kambi/castle-engine-cloud-builds-tools:cge-none
#   stage: build
#   script:
#     - castle-engine package --os=linux --cpu=x86_64 --verbose
#   artifacts:
#     paths:
#       - "*-linux-x86_64.tar.gz"
#
# build-android:
#   image: kambi/castle-engine-cloud-builds-tools:cge-none
#   stage: build
#   script:
#     - castle-engine package --target=android --verbose
#   artifacts:
#     paths:
#       - "*.apk"

#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

rm -f *.log
./scripts/purgecore.sh

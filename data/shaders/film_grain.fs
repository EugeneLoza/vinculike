uniform sampler2D grainTexture;
uniform float rndx1;
uniform float rndy1;
uniform float rndx2;
uniform float rndy2;

const float strength1 = 0.335589238579123;
const float strength2 = 0.232139567203509;
const float unit = 1.0 - strength1 - strength2;

void main (void)
{
  gl_FragColor = screenf_get_original_color() * (unit +
     strength1 * texture2D(grainTexture, vec2(screenf_x() / 512.0 + rndx1, screenf_y() / 512.0 + rndy1)) +
     strength2 * texture2D(grainTexture, vec2(screenf_x() / 512.0 + rndx2, screenf_y() / 512.0 + rndy2))
  );
}

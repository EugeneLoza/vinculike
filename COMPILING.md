﻿This game is written in Free Pascal, powered by [Castle Game Engine](https://castle-engine.io/) - it's free and open source. Download it and follow the instructions to [install and set up the environment](https://castle-engine.io/install).

This version requires at least 7.0-alpha Castle Game Engine version to compile. The most convenient way is to use [prebuilt snapshots](https://castle-engine.io/download). Alternatively you can download and build the Engine from [sources](https://github.com/castle-engine/castle-engine/).

After you have a working copy of Castle Engine and FPC compiler to make your build you can use either of the following ways:

- Use menu item _"Compile"_ in [CGE editor](https://castle-engine.io/manual_editor.php).

- Use [CGE command-line build tool](https://castle-engine.io/build_tool). Run `castle-engine compile` in this directory.

- Use [Lazarus](https://www.lazarus-ide.org/). Open  `vinculike_standalone.lpi` in Lazarus and compile / run. Make sure to first register [CGE Lazarus packages](https://castle-engine.io/install#_fpc_and_lazarus).

### FPC Version

Note that the project cannot be built with stable version of FPC 3.2.2 due to a bug which is fixed in current unstable "trunk" version 3.3.1. This refers both to local builds (the most convenient way to get trunk version of FPC+Lazarus is through [FPCUpDeluxe](https://github.com/LongDirtyAnimAlf/fpcupdeluxe)) and builds through Docker image (choose an image with proper FPC version, as of date of the writing: `cge-none-fpc331`).

#### Note about internal errors

When changing the game code sometimes (depending on stars alignment - can be often, can be rare) the compiler starts throwing "internal errors", "compilation raised internally" or some weird error messages that seem to point at unrelated code fragment.

This happens as a result of compiler confusing "cached" version of compiled units and failing to properly handle the situation. To fix this problem you can use "Clean up and Build" in "Run" menu in Lazarus. However, this will also rebuild Castle Game Engine, so it's better only to clean up only game code.

There are `purge.sh` and `purgewin.bat` scripts designed to do that in one click. The first one is for Unix-based systems, the second one is for Windows.

## Linux

You need both normal and development versions of the libraries required for Castle Game Engine to run properly. These are (Debian/Ubuntu package reference):

* libopenal1
* libopenal-dev
* libpng16-16
* libpng-dev
* zlib1g
* zlib1g-dev
* libvorbis0a
* libvorbis-dev
* libfreetype6
* libfreetype6-dev

You will also need dev version of OpenGL drivers for your video card. Usually it is libgl1-mesa-dev.

Note: the libraries versions may change in newer versions of the operation systems and not every library has a meta-package.

## Windows

Specific DLL libraries (32 bit / 64 bit) are required in case of Windows. You can find those e.g. in `tools\build-tool\data\external_libraries` folder of your Castle Game Engine installation.

The DLLs must be placed in the exe folder or otherwise be available at your PATH.

Note that Castle Engine build-tool (used as `castle-engine compile` or `castle-engine package`) automatically adds those to the game folder, so copying them requires only in case you are building the project through other means, e.g. Lazarus.

## Docker images

Note that the instructions above are meant for Desktop environment (Windows, Linux and most likely MAC, however the latter wasn't tested). Building for Android while follows a very similar pattern, for now requires additional steps (setting up cross-compiler) which in my experience can be quite complicated.

A good option to make builds of the game on all target platforms (Windows, Linux, Android) are [Docker images of Castle Game Engine](https://castle-engine.io/docker). This is an alternative way of installing Castle Engine which this way comes with a fully pre-set-up environment (even with its own integrated OS :)). Learning to use Docker is not trivial, however it can pay off if you need to make release-quality builds and in my experience this is a much easier way to make Android builds than setting up Android NDK+SDK + FPC crosscompiler.

Note that Docker images contain a Debian operation system and all the necessary tools and therefore are large. Currently the image has size of approximately 10 Gb. Make sure you have this space on your system drive, moving Docker images to a different drive is possible but complicated.

Once in a while you should update Docker image by

`docker pull kambi/castle-engine-cloud-builds-tools:cge-none-fpc331`

Don't forget to delete old images to avoid wasting drive space, check Docker documentation on how to do that.

### Using Docker scripts

Some shell scripts in `scripts` folder contain commands to set up the environment and make a build with `cge-none` Docker image. Usually you only want to use (maybe modified) `dockerbuild.sh` which will produce release builds for Windows, Linux and Android.

First of all you need to adjust the script to your local folders arrangement and point it both to Castle Game Engine and your project.

You need to adjust the following lines in `dockerbuild.sh` and `dockerstart.sh` (optionally in `dockercache.sh` too):

`export PROJECT_PATH=GIT/vinculike`

`export CASTLE_ENGINE_PATH=/home/castle_game_engine`

to match the folders where your project and Castle Game Engine are. You can also adjust other build instructions there, e.g. add or remove build targets (e.g. remove Linux builds or add Win32 build).

Before running the script you need to start Docker container by 

`docker run -d -i --name=cge -v <<FOLDER>>:/home/ kambi/castle-engine-cloud-builds-tools:cge-none-fpc331 bash`

where instead of <<FOLDER>> put your local working folder which will become "home", you need to be able to access both Castle Game Engine installation and your project from this folder. The lines changed in `dockerbuild.sh` above will be relative to this folder.

After this execute:

`docker container exec cge sh /home/<<project folder related to FOLDER>>/dockerbuild.sh`

This will take a dozen or two minutes and if everything is successful will produce the corresponding builds packaged.

After all builds and operations on the container finished call:

`docker container stop cge`

to stop the Docker container.

#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

# The folders below are system-specific, you'll need to adjust them for your setup
export PROJECT_PATH=GIT/vinculike
export CASTLE_ENGINE_PATH=/home/GIT/castle-engine

export FPCLAZARUS_VERSION=3.3.1
export PATH=/usr/local/fpclazarus/${FPCLAZARUS_VERSION}/fpc/bin/:${PATH}
FPCLAZARUS_REAL_VERSION=`fpc -iV`
echo 'Real FPC version:' ${FPCLAZARUS_REAL_VERSION}
export FPCDIR=/usr/local/fpclazarus/${FPCLAZARUS_VERSION}/fpc/lib/fpc/${FPCLAZARUS_REAL_VERSION}/

export HOME=/home/
echo 'Building CGE build tool:'
cd "$CASTLE_ENGINE_PATH"
rm -f ./tools/build-tool/castle-engine
./tools/build-tool/castle-engine_compile.sh
cp tools/build-tool/castle-engine /usr/local/bin/
castle-engine --version

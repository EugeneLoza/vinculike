#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

find -type d -name 'backup' -prune -exec rm -rf {} \;
rm -f *.dbg
rm -f vinculike
rm -f vinculike.exe
rm -rf castle-engine-output/standalone
rm -f *.res
rm -rf dockerbuild

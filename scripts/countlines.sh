#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

#!/bin/bash
script_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$script_path"
cd ..

find -type f -regex '\(.*\.inc\)\|\(.*\.pas\)' -exec sed -n '$=' {} +

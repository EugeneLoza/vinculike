#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

export PROJECT_PATH=GIT/vinculike
export CASTLE_ENGINE_PATH=/home/GIT/castle-engine

rm -Rf ./home/${CASTLE_ENGINE_PATH}/tools/build-tool/castle-engine-output/ # to rebuild from scratch

./home/${PROJECT_PATH}/scripts/dockerstart.sh

echo 'Generating CGE cache for Android:'
castle-engine cache --target android
echo 'Generating CGE cache for Win64:'
castle-engine cache --os=win64 --cpu=x86_64
echo 'Generating CGE cache for Linux:'
castle-engine cache --os=linux --cpu=x86_64
echo 'Done.'

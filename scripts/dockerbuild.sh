#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

export PROJECT_PATH=GIT/vinculike
export CASTLE_ENGINE_PATH=/home/GIT/castle-engine

./home/${PROJECT_PATH}/scripts/dockerstart.sh

(cd /home/${PROJECT_PATH}/; ./scripts/purgecore.sh; mkdir dockerbuild)

echo 'Building Android:'
castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/dockerbuild package --target=android --mode=release --package-format=android-apk --verbose >/home/${PROJECT_PATH}/build_android.log
mv /home/${PROJECT_PATH}/dockerbuild/*.apk /home/${PROJECT_PATH}/
echo 'Building Debian64:'
castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/dockerbuild package --os=linux --cpu=x86_64 --mode=release --package-format=deb --verbose >/home/${PROJECT_PATH}/build_deb.log
mv /home/${PROJECT_PATH}/dockerbuild/*.deb /home/${PROJECT_PATH}/
echo 'Building Linux64:'
castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/dockerbuild package --os=linux --cpu=x86_64 --mode=release --verbose >/home/${PROJECT_PATH}/build_linux.log
mv /home/${PROJECT_PATH}/dockerbuild/*.tar.gz /home/${PROJECT_PATH}/
#echo 'Building Win32:'
#castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/dockerbuild package --os=win32 --cpu=i386 --mode=release --verbose >/home/${PROJECT_PATH}/build_win32.log
#mv /home/${PROJECT_PATH}/dockerbuild/*.zip /home/${PROJECT_PATH}/
echo 'Building Win64:'
castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/dockerbuild package --os=win64 --cpu=x86_64 --mode=release --verbose >/home/${PROJECT_PATH}/build_win64.log
mv /home/${PROJECT_PATH}/dockerbuild/*.zip /home/${PROJECT_PATH}/

(cd /home/${PROJECT_PATH}/; ./scripts/purgecore.sh; rm -Rf dockerbuild)
echo 'Done.'

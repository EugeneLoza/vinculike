#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

find -type f -name '*.lpr' -exec sed -i -- "s/-2023/-2024/g" {} +
find -type f -name '*.pas' -exec sed -i -- "s/-2023/-2024/g" {} +
find -type f -name '*.inc' -exec sed -i -- "s/-2023/-2024/g" {} +
find -type f -name 'LICENSE' -exec sed -i -- "s/-2023/-2024/g" {} +

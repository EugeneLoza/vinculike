#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

find -type f -name '*.lpr' -exec sed -i "s/[ \t]*$//" {} +
find -type f -name '*.pas' -exec sed -i "s/[ \t]*$//" {} +
find -type f -name '*.inc' -exec sed -i "s/[ \t]*$//" {} +

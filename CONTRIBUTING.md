# CONTRIBUTION GUIDELINES AND CODE CONVENTIONS

The code of the game is free and open source is available at https://gitlab.com/EugeneLoza/vinculike under GPLv3 license unless explicitly noted otherwise.

Please, use https://gitlab.com/EugeneLoza/vinculike/-/issues to submit a bug report. Also try to stick to the rule "one report - one issue", if you have discovered several problems: create several issues, even if those are related.

To contribute code or art, create a fork of the repository. Then create a branch for the feature. After implementing the fix or new feature - create a Pull Request to the main repository.

## Where to start?

Please have a look at ["Good first issue" label](https://gitlab.com/EugeneLoza/vinculike/-/issues/?label_name%5B%5D=Good%20first%20issue). It doesn't mean the issues are "easy" but it means that implementing those doesn't require in-depth knowledge of the existing code and the changes will most likely be "localized". If in doubt, don't hesitate to ask. The issues are often created without proper explanation of what and how to do but often a lot of additional information can be easily provided.

## Contribute code

Please, follow [Castle Game Engine coding conventions](https://castle-engine.io/coding_conventions).

There are some minor differences in code style:

* Always use CAPITALS for compiler directives and CamelCase for `$DEFINE` values. E.g. `{$IFDEF DesktopVairable}...{$ELSE}...{$ENDIF}`
* Use extended names for compiler directives. E.g. not `{$I somefile.inc}` but `{$INCLUDE somefile.inc}`. Otherwise add a comment on what the compiler directive does.
* Always put `{$INCLUDE compilerconfig.inc}` at the beginning of every unit. Even if it doesn't use any compiler options.
* It's fine (and even advised) to use c-style operators such as "+=". However, be careful as this operation will not work properly with properties that have a getter/setter.
* Prefer longer and explanatory names, better if unique, for entities unless it is a local/nested variable/routine. E.g. `SourceData` instead of `Src`. E.g. you can easily find `SourceStringDictionary` in the code, wherever it is used, but looking for a specific `Source` variable over hundreds of units is not something easily done.
* Don't forget to properly comment your code, especially in case it does something non-obvious.
* Prefer American English. E.g. "behavior", not "behaviour".
* Put all sources into `code` folder. Choose or create an appropriate sub-folder.
* Prefer explanatory names for commits and add a valid reference to issues (if required). E.g. not "commit 8", but "make Update global, fixes https://gitlab.com/EugeneLoza/vinculike/-/issues/1234567890".
* Try to avoid large and many-fold commits, but split them into small commits, each addressing a single issue.
* Please use more specific data types, e.g. `UInt32` instead of `Word`, unless the actual bitness is not important, e.g. for a loop variables.

## Project organization

* **code** - most of the game's code files.
* **data** - game data files, such as audio, graphical and other assets.
* **GFX** - sources for some of the assets. Unfortunately due to limitations on the storage space in a GIT repository some of the assets sources which are unlikely to be reused are not included, however some of those (>5Gb) are available if necessary.
* **scripts** - some (mostly Docker-related) scripts to make semi-automatic builds or automate other tasks

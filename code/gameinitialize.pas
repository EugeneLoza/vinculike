{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Initialize game window, load data and set up UI (views)
  everything starts here }
unit GameInitialize;

{$INCLUDE compilerconfig.inc}

interface

{ Apply some game-wise settings, such as Fullscreen mode, global music volume, etc. }
procedure ApplySettings;
implementation

uses SysUtils, Classes,
  CastleLog, CastleApplicationProperties, CastleWindow,
  CastleSoundEngine, CastleGlUtils
  {$region 'Castle Initialization Uses'}
  // The content here may be automatically updated by CGE editor.
  , GameViewGame
  , GameViewContentWarning
  , GameViewMainMenu
  , GameViewOptions
  , GameViewConfirmResetProgress
  , GameViewConfirmDeleteSave
  , GameViewConfirmNewRun
  , GameViewCredits
  , GameViewEndGame
  , GameViewVinculopedia
  {$endregion 'Castle Initialization Uses'},
  GameConfiguration, GameApparelSlots, GameRandom, GameMap, GameNamesGenerator,
  GameSerializableData, GameSerializableObject, GameSimpleSerializableObject,
  GameCachedImages, GameFonts, GameSounds,
  GameColors, GameStats, GameGarbageCollector,
  GameWindow, GameLoadData, GameDifficultyLevel,
  GameEnchantmentAbstract, GameBodyPresetsDatabase,
  GameLoadSerializedClasses; // WARNING: GameLoadSerializedClasses is critical here, as it includes all serializables

procedure ApplySettings;
begin
  Window.FullScreen := Configuration.FullScreen;
  SoundEngine.Volume := Configuration.MasterVolume;
  SoundEngine.LoopingChannel[0].Volume := Configuration.MusicVolume;
  ApplicationProperties.LimitFPS := Configuration.LimitFps;
end;

procedure ApplicationInitialize;
begin
  { load settings }
  Window.Container.LoadSettings('castle-data:/CastleSettings.xml');
  LoadGameConfiguration;
  ApplySettings;

  { Initialize global stuff }
  InitSerializableObjects;
  InitRandom;
  InitGarbageCollector;
  InitApparelSlots;

  { load game data }
  InitializeColors;
  InitNamesGenerator;
  InitStats;
  LoadDifficultyLevels;
  LoadCachedImages;
  InitEnchantments;
  InitializeTheme; // maybe temporarily

  InitMap;
  LoadFonts;

  LoadGameData;

  { initialize game viewes (UI states) }
  {$region 'Castle State Creation'}
  // The content here may be automatically updated by CGE editor.
  ViewGame := TViewGame.Create(Application);
  ViewContentWarning := TViewContentWarning.Create(Application);
  ViewMainMenu := TViewMainMenu.Create(Application);
  ViewOptions := TViewOptions.Create(Application);
  ViewConfirmResetProgress := TViewConfirmResetProgress.Create(Application);
  ViewConfirmDeleteSave := TViewConfirmDeleteSave.Create(Application);
  ViewConfirmNewRun := TViewConfirmNewRun.Create(Application);
  ViewCredits := TViewCredits.Create(Application);
  ViewEndGame := TViewEndGame.Create(Application);
  ViewVinculopedia := TViewVinculopedia.Create(Application);
  {$endregion 'Castle State Creation'}

  { Set default view (the first screen shown to the Player) }
  if Configuration.AskContentWarning or Configuration.AlwaysAskContentWarning then
    Window.Container.View := ViewContentWarning
  else
    Window.Container.View := ViewMainMenu;
end;

procedure ApplicationFinalize;
begin
  FreeMap;
  FreeGameData;
  FreeDifficultyLevels;
  FreeCachedImages;
  FreeFonts;
  FreeApparelSlots;
  FreeStats;
  FreeGarbageCollector;
  FreeEnchantments;
  FreeBodyPresets;
  FreeSerializableData;
  FreeSerializableObjects;
  FreeSimpleSerializableObjects;
  FreeGameConfiguration;
  FreeRandom;
end;

initialization
  WriteLnLog('----------------------------------------------------');
  WriteLnLog(ApplicationProperties.Caption + ' ' + ApplicationProperties.Version);
  WriteLnLog('Copyright (C) 2022-2024 Yevhen Loza');
  WriteLnLog('This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.');
  WriteLnLog('This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.');
  WriteLnLog('You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.');
  WriteLnLog('----------------------------------------------------');

  // log additional information about video drivers, let's keep it in release too
  LogGLInformationVerbose := true;

  Application.OnInitialize := @ApplicationInitialize;

  InitializeWindow;

  Application.MainWindow := Window;

finalization
  ApplicationFinalize;
end.

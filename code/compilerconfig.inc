{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{------------------------- Compiler settings -------------------------------}

{$SMARTLINK ON} // Enable smart-linking

{$MODE ObjFPC} // FreePascal style code
{$H+} // AnsiStrings
{$J-} // non-writeable constants
{$COPERATORS ON} // allow += style operators, I like them
{$GOTO OFF} // Disallow goto and label
{$EXTENDEDSYNTAX ON} // Allow discarding function result
{$INTERFACES CORBA}
{$MINFPCONSTPREC 32} // By default constants are Single precision, see https://www.freepascal.org/docs-html/prog/progsu52.html#x59-580001.2.52 and https://forum.lazarus.freepascal.org/index.php?topic=64889

{$WARN 2005 off : Comment level $1 found}

{$IFDEF ANDROID} {$DEFINE Mobile} {$ENDIF}
{$IFDEF CASTLE_IOS} {$DEFINE Mobile} {$ENDIF}
{$IFDEF Mobile} {$DEFINE GLIndexesShort} {$DEFINE OpenGLES} {$ENDIF}

{$MACRO ON}

{---------------------------- Game specific --------------------------------}

{$IFDEF DEBUG}
{$DEFINE SafeTypecast}
{$ENDIF}

{$DEFINE ValidateData}

{ Otherwise we are using distance-pathfinding (technically same Dijkstra, but
  algorithm that takes into account "cost" of traversal)
  But there are problems with it:
  1. We don't know when to stop looking for the path, as some path in the next
     iterations may be more optimal. This seriously hurts performance. Maybe will
     eventually need to switch to A*
  2. There's a bug that instead of going in a straight line, the character
     sometimes moves in a wavy pattern. Maybe due to #1. I couldn't figure out
     the real reason though.
  3. Even in a perfect case, this one seems to be a bit less optimal than basic
     Dijkstra. I've seen a few noticeable lags.
  4. Dijkstra map uses less memory (8 bit), unlike distance map (32 bit).
  Moreover:
  5. There is no good gamedesign reason to introduce different cost for tiles
     while it's a welcome bonus, it may be not worth extra work if it doesn't
     work out of the box or hurts performance. Maybe that could be a fun thing
     to experiment with in Vinculike 2 if it'll ever happen.
  Note that Dijkstra implementation is actually quite hacky, accounts for some
  weird cases and optimizations, but in the end it works well in contrast to
  distance-based pathfinding.
  So, by defining this directive we "go back" to original pathfinding
  implementation. At least for now. }

{$DEFINE DijkstraPathfinding}


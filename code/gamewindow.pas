{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Reference to game window and some utils to handle those }
unit GameWindow;

{$INCLUDE compilerconfig.inc}

interface

uses
  CastleWindow;

var
  { Game window }
  Window: TCastleWindow;

{ Create Window and set up basic parameters
  includes setting up splash screen}
procedure InitializeWindow;
{ Set up theme elements, for now: scrollbars}
procedure InitializeTheme;
implementation
uses
  SysUtils,
  CastleKeysMouse, CastleConfig, CastleColors, CastleUiControls,
  GameSplash, GameSaveGame, GameSounds, GameCachedImages;

procedure WindowClose(Container: TUIContainer);
begin
  {  We should not save the game on mobile
     as we don't control the shutdown sequence (it's handled by OS)
     we may end up app killed externally before we finished saving the game file
     resuesulting in corrupted save.
     technically, I've seen apps that manage to save a lot of data on shutdown
     but I would've risk it for a rare but potentially severe bug }
  {$IFNDEF Mobile}
  SaveGame;
  {$ENDIF}
  Window.Close;
end;

procedure InitializeWindow;
begin
  { Splash screen }
  Theme.LoadingBackgroundColor := HexToColor('13120e');
  Theme.ImagesPersistent[tiLoading].Image := Splash;
  Theme.ImagesPersistent[tiLoading].OwnsImage := false;
  Theme.ImagesPersistent[tiLoading].SmoothScaling := true;
  Theme.LoadingUIScaling := usEncloseReferenceSize;
  Theme.LoadingUIReferenceWidth := 1920;
  Theme.LoadingUIReferenceHeight := 1080;

  { Prepare to open the game window }
  Window := TCastleWindow.Create(Application);
  Window.FpsShowOnCaption := {$IFDEF Debug}true{$ELSE}false{$ENDIF};
  Window.ParseParameters; // allows to control window size / fullscreen on the command-line

  {$IFNDEF Mobile}
    Window.Height := Application.ScreenHeight * 4 div 5;
    Window.Width := Window.Height * 1920 div 1080;
  {$ENDIF}
  //Window.AlphaBits := 8;
  Window.OnCloseQuery := @WindowClose;
  Window.Container.BackgroundEnable := false;
end;

procedure InitializeTheme;
begin
  Theme.ScrollBarWidth := 16;
  Theme.ImagesPersistent[tiScrollbarFrame].Image := LoadRgba('castle-data:/ui/kodiakgraphics/scrollbar_background.png');
  Theme.ImagesPersistent[tiScrollbarFrame].OwnsImage := false;
  Theme.ImagesPersistent[tiScrollbarFrame].SmoothScaling := true;
  Theme.ImagesPersistent[tiScrollbarFrame].ProtectedSides.Top := 2;
  Theme.ImagesPersistent[tiScrollbarFrame].ProtectedSides.Bottom := 2;
  Theme.ImagesPersistent[tiScrollbarFrame].ProtectedSides.Left := 2;
  Theme.ImagesPersistent[tiScrollbarFrame].ProtectedSides.Right := 2;
  Theme.ImagesPersistent[tiScrollbarSlider].Image := LoadRgba('castle-data:/ui/kodiakgraphics/scrollbar_slider.png');
  Theme.ImagesPersistent[tiScrollbarSlider].OwnsImage := false;
  Theme.ImagesPersistent[tiScrollbarSlider].SmoothScaling := true;
  Theme.ImagesPersistent[tiScrollbarSlider].ProtectedSides.Top := 4;
  Theme.ImagesPersistent[tiScrollbarSlider].ProtectedSides.Bottom := 4;
  Theme.ImagesPersistent[tiScrollbarSlider].ProtectedSides.Left := 4;
  Theme.ImagesPersistent[tiScrollbarSlider].ProtectedSides.Right := 4;
  {
  Theme.ImagesPersistent[tiButtonNormal].Image := LoadRgba('castle-data:/ui/kodiakgraphics/inventory_button_light.png');
  Theme.ImagesPersistent[tiButtonNormal].OwnsImage := false;
  Theme.ImagesPersistent[tiButtonNormal].ProtectedSides.AllSides := 15;
  Theme.ImagesPersistent[tiButtonFocused].Image := LoadRgba('castle-data:/ui/kodiakgraphics/inventory_button_light_focus.png');
  Theme.ImagesPersistent[tiButtonFocused].OwnsImage := false;
  Theme.ImagesPersistent[tiButtonFocused].ProtectedSides.AllSides := 15;
  Theme.ImagesPersistent[tiButtonPressed].Image := LoadRgba('castle-data:/ui/kodiakgraphics/inventory_button_light_press.png');
  Theme.ImagesPersistent[tiButtonPressed].OwnsImage := false;
  Theme.ImagesPersistent[tiButtonPressed].ProtectedSides.AllSides := 15;
  Theme.ImagesPersistent[tiButtonDisabled].Image := LoadRgba('castle-data:/ui/kodiakgraphics/inventory_button_light_disabled.png');
  Theme.ImagesPersistent[tiButtonDisabled].OwnsImage := false;
  Theme.ImagesPersistent[tiButtonDisabled].ProtectedSides.AllSides := 15;
  }
end;

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ The primary view of the game - most of Player's interaction with the world happens here
  Handles main game loop, initialization of game and map, user input,
  and a lot of features related to "CurrentCharacter" - the player's character that is currently being controlled
  TODO : split this unit and extract some features into sub-units }
unit GameViewGame;

{$INCLUDE compilerconfig.inc}

//{$DEFINE discrete_zoom}

interface

uses
  SysUtils, Classes,
  CastleVectors, CastleComponentSerialize, CastleColors,
  CastleUIControls, CastleControls, CastleKeysMouse, CastleTimeUtils,
  CastleUiShaker, CastleScrollViewBottom, CastleBigButton,
  GameViewAbstract,
  GameRandom, GameMap, GameMapInterface, GamePlayerCharacter, GameItemsDatabase,
  GameApparelSlots, GameMonster, GameDebugUi, GameScreenEffect, GamePaperDoll,
  GameInventoryItem, GameMapItem,
  GamePauseUi, GameMissionUi, GameCharacterScreen, GamePopupItem;

const
  MouseScrollZoomSensitivity = Single(0.1);
  MaxZoomTiles = 2 * 16;
  //DefaultZoomTiles = 2 * VisibleRange;
  MinZoomTiles = 256;
  CompactInventoryWidth = Single(145.0);

type
  { Main view of the game. handles core loop and most UI }
  TViewGame = class(TViewAbstract)
  { Status effects displayed in the quick UI }
  private const MaxStatusEffectsDisplayed = 7;
  strict private
    { Container for "global" screen effects:
      Shaders that affect all other UI elements
      Uses a special in-game class TScreenEffect as a wrapper around TCastleScreenEffecst }
    ScreenEffect: TScreenEffect;
    UiShakerMap: TCastleUiShaker;
    { Handles map displaying, scaling and reacting to clicks }
    MapInterface: TMapInterface;
    { Stat bars, "depleted stats" bars and stat values labels }
    HealthBar, StaminaBar, WillBar, MagicBar, XpBar: TCastleImageControl;
    HealthBarBackground, StaminaBarBackground, WillBarBackground, MagicBarBackground, XpBarBackground: TCastleImageControl;
    HealthAntiBar, StaminaAntiBar, WillAntiBar, MagicAntiBar, XpBarSlider: TCastleImageControl;
    HealthLabel, StaminaLabel, WillLabel, MagicLabel, XpBarLabel: TCastleLabel;

    LabelNormalDamage, LabelStealthDamage: TCastleLabel;
    ImageTopCovered, ImageTopNude, ImageTopNudeCensored, ImageTopVisible, ImageTopTouch: TCastleImageControl;
    ImageBottomCovered, ImageBottomNude, ImageBottomNudeCensored, ImageBottomVisible, ImageBottomTouch: TCastleImageControl;
    VerticalGroupOverallStats: TCastleVerticalGroup;
    { Find a monster or trap at coordinates }
    function GetMonster(const AX, AY: Single; const IsTrap: Boolean): TMonster;
    { Find an active/captured player character at coordinates }
    function GetPlayer(const AX, AY: Single; const IsCaptured: Boolean): TPlayerCharacter;
    { Show-hide character screen }
    procedure ToggleCharacterScreen(Sender: TObject);
    { Show-hide item pop-up }
    procedure TogglePopupItem(Sender: TObject);
    { Show pop-up to select destination from settlement }
    procedure ShowMissionUi;
    { Temporary: hide all pop-ups }
    procedure HideOtherPauseUis;
    { Try stealth attack into coordinates
      If a monster is found at the destination and is unsuspecting,
      will initiate a stealth attack and return true
      returns false if no stealth attack started }
    function TryStealthAttack(const ClickX, ClickY: Single): Boolean;
    { Calculate which margin should be at current zoom level in tiles
      Always returns at least "1" }
    function ClickAdditionalMargin: Single;
    { Perform short click on the map }
    procedure MapClick(const ClickX, ClickY: Single);
    { Perform long/right click on the map }
    procedure MapLongClick(const ClickX, ClickY: Single);
    { Perform swipe action on the map }
    procedure MapSwipe(const ClickX, ClickY: Single);
  strict private
    { Click callback on "equip item" action }
    procedure ClickEquipItem(Sender: TObject);
    { Click callback to repair item action }
    procedure ClickRepairItem(Sender: TObject); // NO LONGER USED ANYWHERE
    { Click callback to unequip item action }
    procedure ClickUnequipItem(Sender: TObject);
  public
    procedure EquipItem(const MapItem: TMapItem);
    procedure RepairItem(const MapItem: TMapItem);
    procedure UnequipItem(const Item: TInventoryItem);
    procedure IdentifyItem(const MapItem: TMapItem);
  public
    { Current character's paperdoll }
    PaperDoll: TPaperDoll;
    function InternalMapInterface: TMapInterface;
  strict private
    { Toggle between full and compact inventory view}
    ButtonFullInventoryShow, ButtonFullInventoryHide: TCastleButton;
    { Activate character UI }
    ButtonCharacterScreen: TCastleButton;
    { UI shaker containing the character paperdoll }
    UiShakerCharacter: TCastleUiShaker;
    { UI group containing inventory/ground items buttons }
    InventoryGroup: TCastleUserInterface;
    { UI group containing character's UIs
      Maybe temporary - todo; right now only used for scaling other UI elements }
    CharacterUi: TCastleUserInterface;
    { Layout groups for items buttons }
    VerticalGroupGround, VerticalGroupEquipped: TCastleVerticalGroup;
    ButtonMagic: TCastleButton;
    VerticalGroupMagic: TCastleVerticalGroup;
    VerticalGroupStatusEffects: TCastleVerticalGroup;
    { Scroll view for items on the ground }
    ScrollViewBottomGround: TCastleScrollViewBottom;
    procedure ClickMagic(Sender: TObject);
    procedure ToggleInventoryMode(Sender: TObject);
    { Updates inventory buttons and character's paperdoll }
    procedure DrawInventory;
    { Update inventor buttons for items on the ground }
    procedure DrawMapItemsInventory;
    { Displays a log line about captured characters on the current level }
    procedure ShowReportOnCapturedCharactersOnThisLevel;
    function GetInventoryWidth: Single;
  strict private
    { If this is the first frame of the game (after coming from main menu) }
    FirstFrame: Boolean;
    { There is a bunch of problems connected to the game not updating different
      objects properly if set to pause before they have updated once, or
      if the situation has changed as the game is set to Pause (e.g. the character
      got blindfolded or inverse - but the map didn't update) }
    WorkaroundUpdateOnceAfterPause: Boolean;
    // TEMPORARY - Todo: move and act
    ExitMapCounter: Single;
    { Time flow speed }
    function TimeSpeed: Single;
  protected
    procedure BeforeScreenshot; override;
    procedure AfterScreenshot; override;
  strict private
    { Container for log }
    PlayerLogVerticalGroup: TCastleVerticalGroup;
    { Precalculated values of maximum and minimum zoom levels
      based on screen resolution (window size) }
    MaxZoom, MinZoom: Single;
    { If user is holding zoom-in-out button }
    ShouldZoomIn, ShouldZoomOut: Boolean;
    { Overlay that covers the screen while the characters is restring/unconscious/incapacitated }
    RestingOverlay: TCastleUserInterface;
    LabelResting: TCastleLabel;
  public
    procedure DoZoomIn;
    procedure DoZoomOut;
    procedure DoReleaseZoom;
    procedure DoZoom(const Value: Single);
  strict private
    { if character is in danger (monsters nearby or chasing the character) }
    function CharacterNotInDanger: Boolean;
    { if any aggressive monster is in direct view }
    function CharacterSeesMonsters: Boolean;
    { if the current character is resting:
      warning, this is rather broken logic: (TODO)
      1. it relies on character action (not extensible )
      2. it doesn't support multiple characters }
    function IsResting: Boolean;
    procedure PauseGame;
    procedure TogglePause;
    procedure StopResting;
    procedure SetActivePause;
    procedure ClickPause(Sender: TObject);
  public
    procedure StartResting;
    { Wakes up the current character:
      through Ui interaction or event in game (like monster attack) }
    function WakeUp(const ForceWakeUp: Boolean; const ActivePause: Boolean): Boolean;
    { if the game is currently paused }
    function IsPause: Boolean;
    { unlike menu pause, allows to drag map and give orders }
    function IsActivePause: Boolean;
    { recover from pause mode and set up UI for action }
    procedure UnPauseGame;
    procedure ClickCamp(Sender: TObject);
  strict private
    StealthDangerMeter: TCastleImageControl;
    FastForwardIcon, PauseIcon: TCastleImageControl;
    LabelTimeAcceleration: TCastleLabel;
    ButtonStealth: TCastleButton;
    LabelStealth: TCastleLabel;
    LabelTime: TCastleLabel;
    procedure ClickStealth(Sender: TObject);
  strict private
    class var DelayedUpdatePosition: Boolean;
    class var DelayedUpdateInventory: Boolean;
    class var FFullInventory: Boolean;
    procedure DoUpdatePosition;
    procedure DoUpdateInventory;
  strict private
    { UIs overlay shown during the pause }
    PauseUi: TPauseUi;
    { UI overlay shown for character screen }
    CharacterScreen: TCharacterScreen;
    PopupItem: TPopupItem;
    MissionUi: TMissionUi;
  public
    { Asks UI to update next frame.
      Only if Sender is CurrentCharacter.
      Will always ask to invalidate if Sender is nil }
    class procedure InvalidatePosition(Sender: TObject);
    class procedure InvalidateInventory(Sender: TObject);
    class function FullInventory: Boolean;
  public
    {
      note: should be public, because StateOptions calls it when necessary }
    procedure UpdateScreenEffect;
    { Time flow speed is high enough for collisions to behave in a buggy way }
    function BuggyLargeTimeFlowSpeed: Boolean;
  strict private
    FLastSoundTime: TTimerResult;
    FLastSoundPriority: Integer;
  public
    procedure SoundMonsterInvestigate(const SoundName: String = 'monster_investigate');
    procedure SoundMonsterAttack(const SoundName: String = 'monster_attack');
    procedure SoundMonsterDie(const SoundName: String);
  public
    { If the run is ending:
      this frame we will show some end-game UI }
    ShouldEndGame: Boolean;
    { If should Reset all monsters to idle (safely, after their actions have updated) }
    MonstersToIdlePending: Boolean;
    { The character that is currently controlled by the player }
    CurrentCharacter: TPlayerCharacter;
    {
      As I always have problems finiding the info and neither
      https://wiki.freepascal.org/Format_function
      nor
      https://www.freepascal.org/docs-html/rtl/sysutils/format.html
      seem to be anywhere near useful, and don't contain useful examples
      So the regular ones are:
      %s - string
      %d - integer
      %.1n - floating-point 10.3 (".1" - how many symbols after the dot. NOTE: it doesn't seem to round the number correctly! e.g. 10.5 can be rounded to 11 or 10 depending on stars alignment)
      TODO: replace for Context system
    }
    procedure AddLog(const AMessage: String; const Args: array of Const; const AColor: TCastleColor); // some better way to send logs, like "global log" (e.g. when an actor blacks out) and "verbose log" which makes sense only for this character
    procedure ShakeCharacter;
    procedure ShakeMap;
    { ask to reset all monsters to idle at the end of this frame }
    procedure ScheduleMonstersToIdle;
    { Stop all shakers (in case the situation changed abruptly, e.g. character teleported) }
    procedure StopShakers;
    { Schedulec to show end-game in the end of this game }
    procedure ScheduleEndGame;
  strict private
    DirectInputActive: Boolean;
    PathfindingActive: Boolean;
    ButtonPathfinding, ButtonNoPathfinding: TCastleBigButton;
    procedure TurnPathfindingOff(Sender: TObject);
    procedure TurnPathfindingOn(Sender: TObject);
  public
    function PathfindingDisabled: Boolean;
  public
    { Label showing FPS }
    LabelFps: TCastleLabel;
    { UI for debugging (cheating) features }
    DebugUi: TDebugUi;
    { Create a newbie }
    function NewPlayerCharacter: TPlayerCharacter;
    { Start a new map with depth +1 from current
      Todo : more genetic to support missions and special maps}
    procedure NewMap(const StartMusic: Boolean = true);
    { Return to the settlement and init everything necessary to start the new run }
    procedure StartNewRun(const StartMusic: Boolean = true);
    procedure StartNewGame;
    { Start music depending on the current map }
    procedure StartMusicForMap;
    { Prepare player characters position on the map }
    procedure TeleportAllCharactersToEntrance;
    procedure CraftItems;
  public
    { when the game starts, when the player enters from main menu }
    procedure Start; override;
    procedure Resize; override;
    procedure GLContextOpen; override;
    { Before rendering the next frame }
    procedure BeforeRender; override;
    { every frame: main loop of the game is here }
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
    function Press(const Event: TInputPressRelease): Boolean; override;
    function Release(const Event: TInputPressRelease): Boolean; override;
    constructor Create(AOwner: TComponent); override;
  end;

var
  ViewGame: TViewGame;

implementation
uses
  TempStamps,
  CastleMobileButton,
  GameSpellEffectApplyStatusEffect, GameStatusEffect, GameEnchantmentChangeMaxHealthPerSecond, GameSpellEffectAbstract, GameThemedButton, //temporary
  GameConfiguration, GameSerializableObject, GameGarbageCollector,
  GameActor, GameFonts, GameInventory, GameMath, GameCachedImages,
  GameSounds, GameTranslation, GameMarkAbstract, GameUiUtils, GameMapTypes,
  TempData, GameVibrate, GameParticle, GameSpell,
  GameActionPlayerEquipItem, GameActionPlayerUnequipItem, GameActionIdle,
  GameActionPlayerStruggleUnequipItem, GameActionPlayerFixItem, GameActionPlayerIdentifyMapItem,
  GameActionPlayerRest, GameActionPlayerUnconscious,
  GameActionStealthAttack, GameActionMoveAbstract, GameActionRoll,
  GameMonsterData, GameMonstersDatabase,
  GameSaveGame, GameLog, GameColors,
  GameViewEndGame, GameTilesetMap, GameInventoryButton, GameStats, GameDifficultyLevel;

constructor TViewGame.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewgame.castle-user-interface';
  DesignPreload := true;
end;

function TViewGame.NewPlayerCharacter: TPlayerCharacter;
begin
  LogVerbose('TViewGame.NewPlayerCharacter');
  Result := TPlayerCharacter.Create;
  Result.Data := TempPlayerCharacter;
  Result.Reset;
  LogVerbose('character prepared');

  Result.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['heart_collar']));
  Result.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['sandals']));
  Result.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['shirt']));
  Result.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['skirt']));
  Result.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['patches_top']));
  Result.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['patch_bottom']));
  Result.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['stick']));
  LogVerbose('items equipped');
  Result.Blueprint.RandomBody(Result);
  LogVerbose('body created');

  Map.PlayerCharactersList.Add(Result);
end;

procedure TViewGame.StartNewRun(const StartMusic: Boolean);
var
  PlayerCharacter, LastExhaustedChar: TPlayerCharacter;
  AvailableCharacters: TPlayerCharactersList;
  SettlementPopulation: Integer;
begin
  LogVerbose('TViewGame.StartNewRun');
  // resting
  for PlayerCharacter in Map.PlayerCharactersList do
    if PlayerCharacter.AtMap = 0 then
      Dec(PlayerCharacter.Exhausted); // can go negative, maybe ok.

  LogVerbose('Exhausted state of characters updated');
  SettlementPopulation := 0;
  AvailableCharacters := TPlayerCharactersList.Create(false);
  LastExhaustedChar := nil;
  for PlayerCharacter in Map.PlayerCharactersList do
  begin
    if PlayerCharacter.IsAvailableForDungeon then
      AvailableCharacters.Add(PlayerCharacter)
    else
    if not PlayerCharacter.IsCaptured and (PlayerCharacter.AtMap = 0) then
    begin
      LastExhaustedChar := PlayerCharacter;
      Inc(SettlementPopulation);
    end;
  end;

  LogVerbose('Population counted');
  LogVerbose('AvailableCharacters.Count = %d', [AvailableCharacters.Count]);
  { Note: currently we have 4 lines of information following the story
    minimal number of log lines (set in options) is 10
    this way we have 6 lines for the story max }
  if AvailableCharacters.Count = 0 then
  begin
    CurrentCharacter := NewPlayerCharacter;
    if SettlementPopulation > 0 then
    begin
      // 7 lines... argh... can't squeeze it more...
      ShowLog('%s: "You had to turn back, I tried to warn you..."', [LastExhaustedChar.Data.DisplayName], ColorLogEnterMap);
      ShowLog('%s: "I didn''t hear. I only saw you waving hands and..."', [CurrentCharacter.Data.DisplayName], ColorLogEnterMap);
      ShowLog('%s: "Do I look like I can shout? Anyway it''s too late now."', [LastExhaustedChar.Data.DisplayName], ColorLogEnterMap);
      ShowLog('%s: "There is a magic barrier that lets only in, but not out."', [LastExhaustedChar.Data.DisplayName], ColorLogEnterMap);
      ShowLog('%s: "So... what can I do?"', [CurrentCharacter.Data.DisplayName], ColorLogEnterMap);
      ShowLog('%s: "Not much. Try that dungeon over there."', [LastExhaustedChar.Data.DisplayName], ColorLogEnterMap);
      ShowLog('%s: "Not something you''d want to experience, but not as bad."', [LastExhaustedChar.Data.DisplayName], ColorLogEnterMap);
    end else
    begin
      ShowLog('"There was a settlement on the map here", %s could only', [CurrentCharacter.Data.DisplayName], ColorLogEnterMap);
      ShowLog('whisper helplessly staring at the ruins abandoned long ago.', [CurrentCharacter.Data.DisplayName], ColorLogEnterMap);
      ShowLog('With trembling hands she reached for the blister of violets.', [CurrentCharacter.Data.DisplayName], ColorLogEnterMap);
      ShowLog('When the drug kicked in, %s calmed down. She was still alive.', [CurrentCharacter.Data.DisplayName], ColorLogEnterMap);
      ShowLog('A night''s sleep helped her recover the strength to move on', [CurrentCharacter.Data.DisplayName], ColorLogEnterMap);
      ShowLog('but her attempt was stopped by an invisible wall. What now?', [CurrentCharacter.Data.DisplayName], ColorLogEnterMap);
    end;
  end else
  begin
    CurrentCharacter := AvailableCharacters[Rnd.Random(AvailableCharacters.Count)];
    if AvailableCharacters.Count = 1 then
    begin
      ShowLog('%s senses a familiar funny feeling', [CurrentCharacter.Data.DisplayName], ColorLogEnterMap);
      ShowLog('She is neither afraid nor worried. Even a bit excited', [CurrentCharacter.Data.DisplayName], ColorLogEnterMap);
      ShowLog('Remembering everything that happened to her down there', [CurrentCharacter.Data.DisplayName], ColorLogEnterMap);
      ShowLog('%s knows that she can do it again.', [CurrentCharacter.Data.DisplayName], ColorLogEnterMap);
    end else
    begin
      ShowLog('%s senses a familiar eerie feeling', [CurrentCharacter.Data.DisplayName], ColorLogEnterMap);
      ShowLog('Confidence and determination, she barely ever had before', [CurrentCharacter.Data.DisplayName], ColorLogEnterMap);
      ShowLog('Maybe it''s her turn to go down today?', [CurrentCharacter.Data.DisplayName], ColorLogEnterMap);
    end;
  end;

  LogVerbose('Settlement is ready');

  FreeAndNil(AvailableCharacters);
  Map.CurrentDepth := -1;

  UnPauseGame;

  NewMap(StartMusic);
end;

procedure TViewGame.StartNewGame;
var
  I: Integer;
  Character: TPlayerCharacter;
begin
  LogVerbose('TViewGame.StartNewGame');
  // Create initial characters
  for I := 1 to 7 do
  begin
    Character := TPlayerCharacter.Create;
    Character.Data := TempPlayerCharacter;
    Character.Blueprint.RandomBody(Character);
    Character.AtMap := I * 5 + Rnd.Random(3) - 1;
    Character.IsCaptured := true;
    Character.IsStranger := true;
    Character.Experience.Xp := Character.Experience.ExperienceToLevel(Character.AtMap + Rnd.Random(3));
    Character.Experience.Xp += Sqrt(Character.Experience.Xp) * Rnd.Random;
    Character.Experience.XpAccumulatedInThisRun := (4 + 4 * Rnd.Random) * Sqrt(Character.Experience.Xp);
    Character.Reset;
    Map.PlayerCharactersList.Add(Character);
  end;
  StartNewRun;
  LocalStats.MaxStat('game-started', 1);
end;

procedure TViewGame.StartMusicForMap;
var
  M: TMonster;
  TotalMonstersHp: Single;
begin
  if Map.CurrentDepth = 0 then
  begin
    if Map.CharactersOnThisLevel.Count >= 7 then
      Music('settlement_full')
    else
    if Map.CharactersOnThisLevel.Count >= 4 then
      Music('settlement_half_empty')
    else
      Music('settlement_empty');
  end else
  begin
    TotalMonstersHp := 0;
    for M in Map.MonstersList do
      if M.CanAct and M.Aggressive then
        TotalMonstersHp += M.MaxHealth;
    LogNormal('TotalMonstersHp: %d', [Round(TotalMonstersHp)]);
    if map.CurrentDepth <= 10 - 2 + Sqrt(CurrentCharacter.Experience.Level) then // newbie comes to dungeon lvl.10 at lvl.4, so "hard" theme will start playing from lvl.10; for lvl.49 hero = from lvl.15
      Music('dungeon_easy')
    else
      Music('dungeon_hard');
  end;
end;

procedure TViewGame.TeleportAllCharactersToEntrance;
var
  SX, SY: Int16;
  PlayerCharacter: TPlayerCharacter;
begin
  for PlayerCharacter in Map.PlayerCharactersList do
    PlayerCharacter.ResetLeashSafe;

  //TODO: This is safeguard, but doesn't feel like the core of the problem is fixed
  for PlayerCharacter in Map.PlayerCharactersList do
    if not (PlayerCharacter.CurrentAction is TActionIdle) then
    begin
      if PlayerCharacter.CurrentAction <> nil then
        LogWarning('%s had action %s. Resetting to idle', [PlayerCharacter.Data.DisplayName, PlayerCharacter.CurrentAction.ClassName])
      else
        LogWarning('%s had action NIL. Resetting to idle', [PlayerCharacter.Data.DisplayName]);
      PlayerCharacter.CurrentAction := TActionIdle.NewAction(PlayerCharacter);
      PlayerCharacter.CurrentAction.Start;
    end;

  for PlayerCharacter in Map.CharactersOnThisLevel do
    if not PlayerCharacter.IsCaptured and (PlayerCharacter.AtMap = Map.CurrentDepth) then
      if Map.CurrentDepth <> 0 then
        PlayerCharacter.Teleport(Map.EntranceX, Map.EntranceY)
      else
      begin
        // it's settlement: teleport characters around randomly
        repeat
          SX := Rnd.Random(Map.SizeX);
          SY := Rnd.Random(Map.SizeY);
        until Map.PassableTiles[PredPlayerColliderSize][SX + Map.SizeX * SY] and (Sqr(SX - Map.ExitX) + Sqr(SY - Map.ExitY) > Sqr(5));
        PlayerCharacter.Teleport(SX, SY);
        PlayerCharacter.Reset;
      end;
  //Another workaround that doesn't feel right - TODO
  for PlayerCharacter in Map.PlayerCharactersList do
    if (PlayerCharacter.LastTileX <= 0) or (PlayerCharacter.LastTileY <=0) or (PlayerCharacter.LastTileX >= Map.PredSizeX) or (PlayerCharacter.LastTileY >= Map.PredSizeY) then
    begin
      LogWarning('%s had invalid coordinates %d,%d for this map size %d,%d. This shouldn''t cause any trouble.', [PlayerCharacter.Data.DisplayName, PlayerCharacter.LastTileX, PlayerCharacter.LastTileY, Map.SizeX, Map.SizeY]);
      //Any valid point on the map - just to make the validation happy.
      PlayerCharacter.Teleport(Map.EntranceX, Map.EntranceY);
    end;
end;

procedure TViewGame.CraftItems;
var
  I: Integer;
begin
  for I := 0 to Pred(Map.CharactersOnThisLevel.Count) do
    Map.CharactersOnThisLevel[I].TemporaryCraftItem;
end;

procedure TViewGame.Start;
begin
  inherited;
  LogNormal('TViewGame.Start');
  FLastSoundTime := Timer;
  FLastSoundPriority := -1;

  ScreenEffect := TScreenEffect.Create(FreeAtStop);
  ScreenEffect.Inject(Self);

  ShouldEndGame := false;

  RestingOverlay := DesignedComponent('RestingOverlay') as TCastleUserInterface;
  LabelResting := DesignedComponent('LabelResting') as TCastleLabel;
  LabelResting.CustomFont := FontBenderBold150;

  ButtonStealth := DesignedComponent('ButtonStealth') as TCastleButton;
  ButtonStealth.OnClick := @ClickStealth;
  LabelStealth := DesignedComponent('LabelStealth') as TCastleLabel;
  LabelStealth.CustomFont := FontSonianoNumbers30;
  LabelTime := DesignedComponent('LabelTime') as TCastleLabel;
  LabelTime.CustomFont := FontSonianoNumbers30;
  StealthDangerMeter := DesignedComponent('StealthDangerMeter') as TCastleImageControl;
  FastForwardIcon := DesignedComponent('FastForwardIcon') as TCastleImageControl;
  LabelTimeAcceleration := DesignedComponent('LabelTimeAcceleration') as TCastleLabel;
  LabelTimeAcceleration.CustomFont := FontBender20;
  PauseIcon := DesignedComponent('PauseIcon') as TCastleImageControl;

  (DesignedComponent('ButtonPause') as TCastleBigButton).OnClick := @ClickPause;

  CharacterUi := DesignedComponent('CharacterUi') as TCastleUserInterface;
  HealthBar := DesignedComponent('HealthBar') as TCastleImageControl;
  StaminaBar := DesignedComponent('StaminaBar') as TCastleImageControl;
  WillBar := DesignedComponent('WillBar') as TCastleImageControl;
  MagicBar := DesignedComponent('MagicBar') as TCastleImageControl;
  XpBar := DesignedComponent('XpBar') as TCastleImageControl;
  HealthAntiBar := DesignedComponent('HealthAntiBar') as TCastleImageControl;
  StaminaAntiBar := DesignedComponent('StaminaAntiBar') as TCastleImageControl;
  WillAntiBar := DesignedComponent('WillAntiBar') as TCastleImageControl;
  MagicAntiBar := DesignedComponent('MagicAntiBar') as TCastleImageControl;
  XpBarSlider := DesignedComponent('XpBarSlider') as TCastleImageControl;
  HealthBarBackground := DesignedComponent('HealthBarBackground') as TCastleImageControl;
  StaminaBarBackground := DesignedComponent('StaminaBarBackground') as TCastleImageControl;
  WillBarBackground := DesignedComponent('WillBarBackground') as TCastleImageControl;
  MagicBarBackground := DesignedComponent('MagicBarBackground') as TCastleImageControl;
  XpBarBackground := DesignedComponent('XpBarBackground') as TCastleImageControl;
  HealthLabel := DesignedComponent('HealthLabel') as TCastleLabel;
  HealthLabel.CustomFont := FontSonianoNumbers20;
  StaminaLabel := DesignedComponent('StaminaLabel') as TCastleLabel;
  StaminaLabel.CustomFont := FontSonianoNumbers20;
  WillLabel := DesignedComponent('WillLabel') as TCastleLabel;
  WillLabel.CustomFont := FontSonianoNumbers20;
  MagicLabel := DesignedComponent('MagicLabel') as TCastleLabel;
  MagicLabel.CustomFont := FontSonianoNumbers20;
  XpBarLabel := DesignedComponent('XpBarLabel') as TCastleLabel;
  XpBarLabel.CustomFont := FontSonianoNumbers20;

  LabelNormalDamage := DesignedComponent('LabelNormalDamage') as TCastleLabel;
  LabelStealthDamage := DesignedComponent('LabelStealthDamage') as TCastleLabel;

  ImageTopCovered := DesignedComponent('ImageTopCovered') as TCastleImageControl;
  ImageTopNude := DesignedComponent('ImageTopNude') as TCastleImageControl;
  ImageTopNudeCensored := DesignedComponent('ImageTopNudeCensored') as TCastleImageControl;
  ImageTopVisible := DesignedComponent('ImageTopVisible') as TCastleImageControl;
  ImageTopTouch := DesignedComponent('ImageTopTouch') as TCastleImageControl;
  ImageBottomCovered := DesignedComponent('ImageBottomCovered') as TCastleImageControl;
  ImageBottomNude := DesignedComponent('ImageBottomNude') as TCastleImageControl;
  ImageBottomNudeCensored := DesignedComponent('ImageBottomNudeCensored') as TCastleImageControl;
  ImageBottomVisible := DesignedComponent('ImageBottomVisible') as TCastleImageControl;
  ImageBottomTouch := DesignedComponent('ImageBottomTouch') as TCastleImageControl;
  VerticalGroupOverallStats := DesignedComponent('VerticalGroupOverallStats') as TCastleVerticalGroup;

  LabelFps := DesignedComponent('LabelFps') as TCastleLabel;
  LabelFps.Exists := false;
  DebugUi := TDebugUi.Create(FreeAtStop);
  DebugUi.Parse(DesignedComponent('DebugUi') as TCastleDesign);

  PauseUi := TPauseUi.Create(FreeAtStop);
  PauseUi.Parse(DesignedComponent('PauseUi') as TCastleDesign);

  MissionUi := TMissionUi.Create(FreeAtStop);
  MissionUi.Parse(DesignedComponent('MissionUi') as TCastleDesign);

  VerticalGroupGround := DesignedComponent('VerticalGroupGround') as TCastleVerticalGroup;
  VerticalGroupEquipped := DesignedComponent('VerticalGroupEquipped') as TCastleVerticalGroup;
  VerticalGroupStatusEffects := DesignedComponent('VerticalGroupStatusEffects') as TCastleVerticalGroup;
  ScrollViewBottomGround := DesignedComponent('ScrollViewBottomGround') as TCastleScrollViewBottom;

  ButtonMagic := DesignedComponent('ButtonMagic') as TCastleButton;
  ButtonMagic.OnClick := @ClickMagic;
  VerticalGroupMagic := DesignedComponent('VerticalGroupMagic') as TCastleVerticalGroup;
  VerticalGroupMagic.Exists := false;

  ButtonFullInventoryShow := DesignedComponent('ButtonFullInventoryShow') as TCastleButton;
  ButtonFullInventoryShow.OnClick := @ToggleInventoryMode;
  ButtonFullInventoryHide := DesignedComponent('ButtonFullInventoryHide') as TCastleButton;
  ButtonFullInventoryHide.OnClick := @ToggleInventoryMode;
  ButtonFullInventoryHide.Exists := false;

  ButtonCharacterScreen := DesignedComponent('ButtonCharacterScreen') as TCastleButton;
  ButtonCharacterScreen.OnClick := @ToggleCharacterScreen;

  ButtonPathfinding := DesignedComponent('ButtonPathfinding') as TCastleBigButton;
  ButtonPathfinding.OnClick := @TurnPathfindingOff;
  ButtonNoPathfinding := DesignedComponent('ButtonNoPathfinding') as TCastleBigButton;
  ButtonNoPathfinding.OnClick := @TurnPathfindingOn;
  PathfindingActive := true;

  UiShakerCharacter := DesignedComponent('UiShakerCharacter') as TCastleUiShaker;
  PaperDoll := TPaperDoll.Create(FreeAtStop);
  UiShakerCharacter.InsertFront(PaperDoll);

  //note: character screen needs paper doll reference, so should be created after
  CharacterScreen := TCharacterScreen.Create(FreeAtStop);
  CharacterScreen.Parse(DesignedComponent('CharacterScreen') as TCastleDesign);

  PopupItem := TPopupItem.Create(FreeAtStop);
  PopupItem.Parse(DesignedComponent('PopupItem') as TCastleDesign);

  InventoryGroup := DesignedComponent('InventoryGroup') as TCastleUserInterface;

  PlayerLogVerticalGroup := DesignedComponent('PlayerLogVerticalGroup') as TCastleVerticalGroup;

  // Create Map Interface

  UiShakerMap := DesignedComponent('UiShakerMap') as TCastleUiShaker;
  MapInterface := TMapInterface.Create(FreeAtStop);
  MapInterface.FullSize := true;
  //MapInterface.Zoom := 1.0; We set it in FirstFrame
  MapInterface.OnMapClick := @MapClick;
  MapInterface.OnMapLongClick := @MapLongClick;
  MapInterface.OnMapSwipe := @MapSwipe;
  UiShakerMap.InsertBack(MapInterface);

  LogVerbose('Clearing references');
  ClearObjectReferences; {NOTE: while it should be ok here... maybe a better place like in Map.Load and Map.Init would be more appropriate?}
  LogVerbose('Checking savegame');
  if SaveGameExists then
  begin
    try
      LogVerbose('Trying to load save game');
      LoadGame;
    except
      on E: Exception do
      begin
        ShowError('THERE WAS AN UNHANDLED EXCEPTION LOADING THE GAME.');
        ShowError('%s:"%s"', [E.ClassName, E.Message]);
        ShowError('Unfortunately things like that may happen in the prototype.');
        ShowError('For now there is no way we can recover from this bug.');
        ShowError('So, the broken save was deleted and a new game has started.');
        StartNewRun;
        Exit;
      end;
    end;
    LogVerbose('Load game finished successfully');
    StartMusicForMap;
    MapInterface.TeleportTo(CurrentCharacter.CenterX, CurrentCharacter.CenterY);
    if Map.CurrentDepth = 0 then
      ShowLog('Game loaded. Current map: Settlement', [], ColorLogEnterMap)
    else
      ShowLog('Game loaded. Current dungeon level: %d', [Map.CurrentDepth], ColorLogEnterMap);
  end else
    StartNewGame;
  LogVerbose('Game is ready');

  if not CharacterNotInDanger then
  begin
    ShowLog('There are monsters nearby! The game is set to pause', [], ColorLogEnterMap);
    SetActivePause;
  end else
    UnPauseGame; // this will also properly set Resting overlay

  {$WARNING in case of crash at load game - this will not happen}
  ShowReportOnCapturedCharactersOnThisLevel;

  FirstFrame := true;
  FFullInventory := false;
  InvalidateInventory(nil);
  InvalidatePosition(nil);
end;

procedure TViewGame.NewMap(const StartMusic: Boolean);
var
  MapArea: Integer;
  MinMapSide: Integer;
  MapSizeX, MapSizeY: Int16;
  GenerationStart: TTimerResult;
begin
  LogVerbose('TViewGame.NewMap');
  GenerationStart := Timer;

  MonstersToIdlePending := false;

  if Map.CurrentDepth < Map.MaxDungeonDepth then // temporary
    Inc(Map.CurrentDepth);
  CurrentCharacter.AtMap := Map.CurrentDepth;

  // movemap has insignificant but noticeable lags when calculating path on 512x512
  // frustrating lags on 1024x1024

  if Map.CurrentDepth = 0 then
  begin
    Map.Init(41, 41);
    Map.GenerateStamp(TownStamp);

    Map.GenerateMonsters(-1);
    Map.CacheCharactersOnThisLevel;
    TeleportAllCharactersToEntrance;
    CraftItems;
    if Map.CharactersOnThisLevel.Count > 1 then
      Map.ShowPassableMap; // TODO: this is a workaround for the bug that Move doesn't like to work with unexplored map for player character, no idea why though. However the overall fix should be good, or rather the correct fix should be: save the map
    Map.PositionCapturedCharacters; // It is needed to cache some related stuff, like the longest route on the map
    //Map.GenerateItems(2 + Rnd.Random(5));
    Map.ClearGenerationCache;

    Map.EnemySpawnTime := Single.MaxValue;
  end else
  begin
    case Map.CurrentDepth of
      1..7: MapArea := 1024 * Map.CurrentDepth;
      else
        MapArea := 7168 + Map.CurrentDepth * 128;
    end;
    MinMapSide := 32 + Round(Sqrt(Map.CurrentDepth));
    MapSizeX := MinMapSide + Rnd.Random(MapArea div MinMapSide - MinMapSide);
    MapSizeY := Trunc(MapArea / MapSizeX);
    Map.Init(MapSizeX, MapSizeY);

    if (MapSizeX > 50) and (MapSizeY > 50) then
    begin
      if (Rnd.Random < 0.2) and (MapSizeX > 70) and (MapSizeY > 70) then
        Map.GenerateStamp(Twins12x12)
      else
      if (Rnd.Random < 0.2) and (MapSizeX > 70) and (MapSizeY > 70) then
        Map.GenerateStamp(Basic13x13)
      else
      if (Rnd.Random < 0.2) and (MapSizeX > 70) and (MapSizeY > 70) then
         Map.GenerateStamp(ThickWalls12x12)
      else
      if (Rnd.Random < 0.3) and (MapSizeX > 60) and (MapSizeY > 60) then
        Map.GenerateStamp(Circles12x12)
      else
      if (Rnd.Random < 0.3) and (MapSizeX > 60) and (MapSizeY > 60) then
        Map.GenerateStamp(Basic11x11)
      else
      if (Rnd.Random < 0.2) then
        Map.GenerateStamp(Triangle11x8)
      else
      if Rnd.Random < 0.4 then
        Map.GenerateStamp(Basic10x10)
      else
      if Rnd.Random < 0.7 then
        Map.GenerateStamp(Basic8x8)
      else
      if Rnd.Random < 0.7 then
        Map.GenerateStamp(Basic6x6)
      else
      if Rnd.Random < 0.7 then
        Map.GenerateStamp(Boxes11x11)
      else
      if Rnd.Random < 0.4 then
        Map.GenerateStamp(Basic4x4)
      else
      if Rnd.Random < 0.5 then
        Map.GenerateStamp(Cage12x12)
      else
      if Rnd.Random < 0.5 then
        Map.GenerateStamp(Cage6x6)
      else
      if Rnd.Random < 0.7 then
        Map.GenerateStamp(Checkers4x4)
      else
        Map.GenerateStamp(Checkers3x3);
    end else
    begin
      if Rnd.Random < 0.5 then
        Map.GenerateStamp(Basic8x8)
      else
      if (Rnd.Random < 0.2) and (Map.SizeX > 50) then
        Map.GenerateStamp(Triangle11x8)
      else
      if Rnd.Random < 0.6 then
        Map.GenerateStamp(Basic6x6)
      else
      if Rnd.Random < 0.7 then
        Map.GenerateStamp(Boxes11x11)
      else
      if Rnd.Random < 0.4 then
        Map.GenerateStamp(Basic4x4)
      else
      if Rnd.Random < 0.5 then
        Map.GenerateStamp(Cage6x6)
      else
      if Rnd.Random < 0.7 then
        Map.GenerateStamp(Checkers4x4)
      else
        Map.GenerateStamp(Checkers3x3);
    end;

    case Map.CurrentDepth of
      1: Map.GenerateMonsters(1);
      2: Map.GenerateMonsters(3);
      3: Map.GenerateMonsters(7);
      4: Map.GenerateMonsters(11);
      else
        Map.GenerateMonsters(Difficulty.MonstersNumberMultiplier * (Rnd.Random(Trunc(Map.FreeArea / 2048.0)) + Trunc(Map.FreeArea / 2048.0) + Trunc(1.15 * Map.CurrentDepth) + Rnd.Random(Map.CurrentDepth div 2)));
    end;

    LogNormal('Map generated in %.1n seconds', [GenerationStart.ElapsedTime]);

    Map.CacheCharactersOnThisLevel;
    TeleportAllCharactersToEntrance;
    Map.PositionCapturedCharacters;
    Map.GenerateItems(1 + Round(Difficulty.ItemsNumberMultiplier * Map.MonstersList.Count) + Rnd.Random(Map.MonstersList.Count div 2)); { 1/3+1/3/2=50% too little; 1/2+1/4=75% too much; 1/3+1/4 = 7/12 =58% }
    Map.GenerateTraps(Rnd.Random(Map.MonstersList.Count div 2));
    Map.ClearGenerationCache;

    ShowLog('Entering abandoned mine level %d.', [Map.CurrentDepth], ColorLogEnterMap);
    Map.EnemySpawnTime := Difficulty.FirstPatrolSpawnTime;
  end;

  ShowReportOnCapturedCharactersOnThisLevel;

  LogNormal('Map ready in %.1n seconds', [GenerationStart.ElapsedTime]);

  if StartMusic then
    StartMusicForMap;

  MapInterface.TeleportTo(CurrentCharacter.CenterX, CurrentCharacter.CenterY);

  HideOtherPauseUis;
  InvalidateInventory(nil);
  InvalidatePosition(nil);

  if Map.CurrentDepth > 1 then
    SaveGame;
end;

procedure TViewGame.UpdateScreenEffect;
begin
  if Active then
  begin
    ScreenEffect.Enable;
    //MapEffect.Enable;
  end;
end;

function TViewGame.BuggyLargeTimeFlowSpeed: Boolean;
begin
  Exit(TimeSpeed > 5.0);
end;

procedure TViewGame.SoundMonsterInvestigate(const SoundName: String = 'monster_investigate');
begin
  if CurrentCharacter.Unsuspecting or IsPause or not CurrentCharacter.CurrentAction.CanStop or Map.MonstersAttacking then
    Exit;
  if (FLastSoundTime.ElapsedTime > 1.0) or (FLastSoundPriority < 0) then
  begin
    Sound(SoundName);
    FLastSoundPriority := 0;
    FLastSoundTime := Timer;
  end;
end;

procedure TViewGame.SoundMonsterAttack(const SoundName: String = 'monster_attack');
begin
  if CurrentCharacter.Unsuspecting or IsPause or not CurrentCharacter.CurrentAction.CanStop or Map.MonstersAttacking then
    Exit;
  if (FLastSoundTime.ElapsedTime > 1.0) or (FLastSoundPriority < 1) then
  begin
    Sound(SoundName);
    FLastSoundPriority := 1;
    FLastSoundTime := Timer;
  end;
end;

procedure TViewGame.SoundMonsterDie(const SoundName: String);
begin
  Sound(SoundName);
end;

procedure TViewGame.DoZoomIn;
begin
  ShouldZoomIn := true;
  ShouldZoomOut := false;
end;

procedure TViewGame.DoZoomOut;
begin
  ShouldZoomIn := false;
  ShouldZoomOut := true;
end;

procedure TViewGame.DoReleaseZoom;
begin
  ShouldZoomIn := false;
  ShouldZoomOut := false;
end;

procedure TViewGame.DoZoom(const Value: Single);
begin
  if ShouldZoomIn then
  begin
    {$IFDEF discrete_zoom}
    if MapInterface.Zoom > MaxZoom then Exit;
    if MapInterface.Zoom >= 1 then
      MapInterface.Zoom += 1
    else
      MapInterface.Zoom := 1.0 / (Trunc(1 / MapInterface.Zoom) - 1);
    ShouldZoomIn := false;
    {$ELSE}
    MapInterface.Zoom *= 1 + Value;
    if MapInterface.Zoom > MaxZoom then
      MapInterface.Zoom := MaxZoom;
    {$ENDIF}
  end;
  PauseUi.ButtonZoomIn.Enabled := not IsResting and (MapInterface.Zoom < MaxZoom - 1e-5);
  if ShouldZoomOut then
  begin
    {$IFDEF discrete_zoom}
    if MapInterface.Zoom < MinZoom then Exit;
    if MapInterface.Zoom > 1 then
      MapInterface.Zoom -= 1
    else
      MapInterface.Zoom := 1.0 / (Trunc(1 / MapInterface.Zoom) + 1);
    ShouldZoomOut := false;
    {$ELSE}
    MapInterface.Zoom *= 1 - Value;
    if MapInterface.Zoom < MinZoom then
      MapInterface.Zoom := MinZoom;
    {$ENDIF}
  end;
  PauseUi.ButtonZoomOut.Enabled := not IsResting and (MapInterface.Zoom > MinZoom + 1e-5);
end;

procedure TViewGame.Resize;
begin
  inherited Resize;
  // Container.Width/Height in real device pixels
  MaxZoom := Container.PixelsHeight / RenderTileSize / MaxZoomTiles;
  MinZoom := Container.PixelsHeight / RenderTileSize / MinZoomTiles;
  // Self.Width/Height in unscaled pixels
  CharacterUi.Height := Container.UnscaledHeight;
  CharacterUi.Width := 384.0 * Container.UnscaledHeight / 1080.0;
  HealthBarBackground.Width := CharacterUi.Width - 12 * Container.UnscaledHeight / 1080.0;
  StaminaBarBackground.Width := HealthBarBackground.Width;
  WillBarBackground.Width := HealthBarBackground.Width;
  MagicBarBackground.Width := HealthBarBackground.Width;
  XpBarBackground.Width := HealthBarBackground.Width;
  UiShakerMap.Width := Self.EffectiveWidth - CompactInventoryWidth;
  MapInterface.GameplayAreaWidth := CharacterUi.EffectiveRect.Left;
  UiShakerMap.Height := Self.EffectiveHeight;
  InvalidateInventory(nil);
  InvalidatePosition(nil);
  CharacterScreen.SetWidth(CharacterUi.EffectiveRect.Left);
  PopupItem.SetWidth(CharacterUi.EffectiveRect.Left);
  MissionUi.SetWidth(CharacterUi.EffectiveRect.Left);
end;

procedure TViewGame.GLContextOpen;
begin
  inherited GLContextOpen;
  // otherwise we may lose Paperdolls on Android
  InvalidateInventory(nil);
  InvalidatePosition(nil);
end;

function TViewGame.GetMonster(const AX, AY: Single; const IsTrap: Boolean): TMonster;
var
  M: TMonster;
  DistanceSqr, D: Single;
begin
  DistanceSqr := Single.MaxValue;
  Result := nil;
  for M in Map.MonstersList do
    if M.CanAct and (M.MonsterData.Trap = IsTrap) and (M.IsVisible or M.IsHearable) then
    begin
      D := M.DistanceToSqr(CurrentCharacter);
      if (
           (D < DistanceSqr) or ((Result <> nil) and (not Result.Aggressive))
         ) and (M.DistanceTo(AX, AY) <= M.HalfSize + ClickAdditionalMargin) then
      begin
        if CurrentCharacter.CanInteractWith(M) then
        begin
          DistanceSqr := D;
          Result := M;
        end;
      end;
    end;
end;

function TViewGame.GetPlayer(const AX, AY: Single; const IsCaptured: Boolean): TPlayerCharacter;
var
  P: TPlayerCharacter;
  Distance, D: Single;
begin
  Distance := Single.MaxValue;
  Result := nil;
  for P in Map.CharactersOnThisLevel do
    if (P.IsCaptured = IsCaptured) and P.IsVisible then
    begin
      D := P.DistanceTo(AX, AY);
      if D < Distance then
      begin
        Distance := D;
        Result := P;
      end;
    end;
  if (Result <> nil) and (Distance > Result.HalfSize + ClickAdditionalMargin) then
    Result := nil;
end;

procedure TViewGame.ToggleCharacterScreen(Sender: TObject);
begin
  HideOtherPauseUis;
  Sound('character_screen_show');
  CharacterScreen.SetWidth(CharacterUi.EffectiveRect.Left);
  CharacterScreen.Show;
end;

procedure TViewGame.TogglePopupItem(Sender: TObject);
begin
  HideOtherPauseUis;
  Sound('menu_button');
  if Sender is TMapItemButton then // not sure about that
  begin
    PopupItem.MapItem := TMapItemButton(Sender).MapItem;
    PopupItem.InventoryItem := TMapItemButton(Sender).MapItem.Item;
  end else
  begin
    PopupItem.MapItem := nil;
    PopupItem.InventoryItem := (Sender as TInventoryButton).Item;
  end;
  PopupItem.SetWidth(CharacterUi.EffectiveRect.Left);
  PopupItem.Show;
end;

procedure TViewGame.ShowMissionUi;
begin
  HideOtherPauseUis;
  MissionUi.SetWidth(CharacterUi.EffectiveRect.Left);
  MissionUi.Show;
end;

procedure TViewGame.HideOtherPauseUis;
begin
  if PopupItem.Exists then
    PopupItem.Hide;
  if CharacterScreen.Exists then
    CharacterScreen.Hide;
  if MissionUi.Exists then
    MissionUi.Hide;
  if PauseUi.Exists then
    PauseUi.Hide;
end;

procedure TViewGame.ToggleInventoryMode(Sender: TObject);
begin
  FFullInventory := not FFullInventory;
  if FFullInventory then
    Sound('full_inventory_open')
  else
    Sound('full_inventory_close');
  ButtonFullInventoryShow.Exists := not FFullInventory;
  ButtonFullInventoryHide.Exists := FFullInventory;
  InvalidateInventory(nil);
end;

type
  TButtonSpell = class(TCastleUserInterface)
  strict private
    Spell: TSpell;
    procedure ClickCast(Sender: TObject);
  public
    //Button: TCastleMobileButton;
    procedure Setup(const ASpell: TSpell);
  end;

procedure TButtonSpell.ClickCast(Sender: TObject);
begin
  // todo: hide parent - or rather: ?
  //ViewGame.HideOtherPauseUis;
  ViewGame.UnPauseGame;
  Spell.Cast; // will also deduce the price; we've checked it when creating the menu and disabled the button of the spell cannot be cast
end;

procedure TButtonSpell.Setup(const ASpell: TSpell);
var
  Button: TCastleMobileButton;
begin
  Spell := ASpell;
  Width := 400;
  Height := 70;

  Button := TCastleMobileButton.Create(Self);
  Button.SetTheme('inventory_button_light');
  Button.CustomFont := FontBender40;
  Button.CustomTextColorUse := true;
  Button.CustomTextColor := ColorDefault;
  Button.EnableParentDragging := true;
  Button.Height := Self.Height;
  Button.Width := Self.Width;
  Button.OnClick := @ClickCast;
  Button.Caption := Spell.DisplayName + '(' + Spell.Price.ToString + ')'

  // auto size? false
  //ButtonFavorite
end;

procedure TViewGame.ClickMagic(Sender: TObject);
var
  ButtonSpell: TButtonSpell;
  I: Integer;
begin
  Sound('menu_button'); // magic_button
  ViewGame.HideOtherPauseUis;
  VerticalGroupMagic.Exists := not VerticalGroupMagic.Exists; // todo: something smarter
  VerticalGroupMagic.ClearAndFreeControls;
  // Todo: favorite spells
  for I := 0 to Pred(CurrentCharacter.Inventory.Spells.Count) do
  begin
    ButtonSpell := TButtonSpell.Create(VerticalGroupMagic);
    ButtonSpell.Setup(CurrentCharacter.Inventory.Spells[i]);
    VerticalGroupMagic.InsertFront(ButtonSpell);
  end;
end;

function TViewGame.TryStealthAttack(const ClickX, ClickY: Single): Boolean;
const
  SqrStealthAttackRange = Sqr(15 - 1); // = roll range - 1 ; monster visibility: 23 max; 5 min -> 8 -> 11 -> 14 (player can have 2 items equipped for a stealth attack)
var
  Monster, SelectedMonster: TMonster;
begin
  if (Sqr(CurrentCharacter.X - ClickX) + Sqr(CurrentCharacter.Y - ClickY) < SqrStealthAttackRange * 2) then
  begin
    SelectedMonster := nil;
    for Monster in Map.MonstersList do
      if Monster.CanAct and not Monster.MonsterData.Trap and not Monster.MonsterData.Chest and Monster.Unsuspecting and
        Monster.IsVisible and Monster.IsHere(ClickX, ClickY, ClickAdditionalMargin) and
        (Sqr(CurrentCharacter.CenterX - Monster.CenterX) + Sqr(CurrentCharacter.CenterY - Monster.CenterY) <= SqrStealthAttackRange) then
          SelectedMonster := Monster;
    if SelectedMonster <> nil then
    begin
      //if Map.Ray(CurrentCharacter.PredSize, CurrentCharacter.CenterX, CurrentCharacter.CenterY, SelectedMonster.CenterX, SelectedMonster.CenterY) then // I have no idea why this is broken
      if CurrentCharacter.LineOfSight(SelectedMonster) then
      begin
        CurrentCharacter.CurrentAction := TActionStealthAttack.NewAction(CurrentCharacter);
        TActionStealthAttack(CurrentCharacter.CurrentAction).Target := SelectedMonster;
        CurrentCharacter.CurrentAction.Start;
        Exit(true);
      end else
      begin
        ShowLog(GetTranslation('PlayerStealthAttackImpossible'), [CurrentCharacter.Data.DisplayName, SelectedMonster.Data.DisplayName], ColorLogAttack);
      end;
    end;
  end;
  Exit(false);
end;

function TViewGame.ClickAdditionalMargin: Single;
var
  ClickSize: Single;
begin
  ClickSize := Configuration.ClickMargin * RenderRect.Height; // in device pixels
  Result := 1.0 + ClickSize / (RenderTileSize * MapInterface.Zoom);
end;

procedure TViewGame.MapClick(const ClickX, ClickY: Single);
var
  MoveToX, MoveToY: Single;
  SelectedMonster: TMonster;
begin
  if not CurrentCharacter.CurrentAction.CanStop then
    Exit;

  if WakeUp(false, false) then
    Exit;

  MoveToX := ClickX - CurrentCharacter.HalfSize + 0.5; // I have absolutely no idea why 0.5
  MoveToY := ClickY - CurrentCharacter.HalfSize + 0.5;
  if (mkShift in Container.Pressed.Modifiers) then
    MapSwipe(ClickX, ClickY)
  else
  begin
    SelectedMonster := GetMonster(ClickX, ClickY, false);
    if (SelectedMonster <> nil) and (SelectedMonster.IsHearable or SelectedMonster.IsVisible) then
    begin
      if not TryStealthAttack(ClickX, ClickY) then
        CurrentCharacter.MoveAndAct(SelectedMonster, PlayerActionAttack);
    end else
      CurrentCharacter.MoveTo(MoveToX, MoveToY);
  end;
end;

procedure TViewGame.MapLongClick(const ClickX, ClickY: Single);
var
  Item: TMapItem;
  Monster: TMonster;
  NothingFound: Boolean;
  SelectedMonster: TMonster;
  SelectedPlayer: TPlayerCharacter;

  function ItemName: String;
  begin
    if Item.Item.HasStochasticEnchantments then
      Exit('*' + Item.Item.Data.DisplayName + '*')
    else
      Exit(Item.Item.Data.DisplayName)
  end;

begin
  if not CurrentCharacter.CurrentAction.CanStop then
    Exit;

  if not (CurrentCharacter.CurrentAction is TActionMoveAbstract) or IsPause then
  begin
    if WakeUp(false, false) then
      Exit;

    SelectedMonster := GetMonster(ClickX, ClickY, true);
    if SelectedMonster <> nil then
    begin
      if CurrentCharacter.Inventory.CanUseHands then
        CurrentCharacter.MoveAndAct(SelectedMonster, PlayerActionDisarm)
      else
        ShowLog('%s can''t disarm the trap with her own hands restrained', [CurrentCharacter.Data.DisplayName], ColorLogBondage);
      Exit;
    end;

    if TryStealthAttack(ClickX, ClickY) then
      Exit;

    SelectedPlayer := GetPlayer(ClickX, ClickY, true);
    if SelectedPlayer <> nil then
    begin
      if CurrentCharacter.Inventory.CanUseHands then
        CurrentCharacter.MoveAndAct(SelectedPlayer, PlayerActionRescue)
      else
        ShowLog('%s can''t rescue anyone with her hands restrained', [CurrentCharacter.Data.DisplayName], ColorLogBondage);
      Exit;
    end;
  end;

  //  if Map.PassablePlayerTiles[Round(ClickX) + Map.SizeX * Round(ClickY)]

  NothingFound := true;
  for Item in Map.MapItemsList do
    if Item.IsHere(ClickX, ClickY, ClickAdditionalMargin) and Item.IsVisible then
    begin
      NothingFound := false;
      if Item.Item.Broken then
        ShowLog(GetTranslation('MapItemBrokenHereLog'), [ItemName], ColorLogMapItemBroken)
      else
      if Item.Item.Durability > Item.Item.MaxDurability / 3 then
        ShowLog(GetTranslation('MapItemHereLog'), [ItemName, Round(Item.Item.Durability), Round(Item.Item.MaxDurability)], ColorLogMapItemGood)
      else
        ShowLog(GetTranslation('MapItemHereLog'), [ItemName, Round(Item.Item.Durability), Round(Item.Item.MaxDurability)], ColorLogMapItemDamaged)
    end;

  for Monster in Map.MonstersList do
    if Monster.CanAct and Monster.IsHere(ClickX, ClickY, ClickAdditionalMargin) and Monster.IsVisible then
    begin
      NothingFound := false;
      if Monster.Health < Monster.MaxHealth then
        ShowLog(GetTranslation('MonsterWoundedHereLog'), [Monster.Data.DisplayName, Round(Monster.Health)], ColorLogMapMonster)
      else
        ShowLog(GetTranslation('MonsterHealthyHereLog'), [Monster.Data.DisplayName, Round(Monster.Health)], ColorLogMapMonster);
    end;

  if not (CurrentCharacter.CurrentAction is TActionMoveAbstract) or IsPause then
    if NothingFound then
      MapClick(ClickX, ClickY);
end;

procedure TViewGame.MapSwipe(const ClickX, ClickY: Single);
var
  RollVector: TVector2;
  MoveToX, MoveToY: Single;
begin
  {if not CurrentCharacter.CurrentAction.CanStop then
    Exit; --------- not sure about swipes. Those can be too useful to block}

  if CurrentCharacter.Immobilized and not CurrentCharacter.CurrentAction.CanStop {to show logs if any} then
    Exit; // for now manually 2 actions cannot be canceled by rolling TODO: CanStopForced

  if WakeUp(false, false) then
    Exit;

  MoveToX := ClickX - CurrentCharacter.HalfSize + 0.5; // I have absolutely no idea why 0.5
  MoveToY := ClickY - CurrentCharacter.HalfSize + 0.5;
  if (CurrentCharacter.Stamina > 0) then
  begin
    RollVector := CurrentCharacter.Data.RollRange * Vector2(MoveToX - CurrentCharacter.X, MoveToY - CurrentCharacter.Y).Normalize;
    CurrentCharacter.RollTo(CurrentCharacter.X + RollVector.X, CurrentCharacter.Y + RollVector.Y);
    CurrentCharacter.DegradeStamina(CurrentCharacter.GetRollCost);
  end else
  begin
    CurrentCharacter.MoveTo(MoveToX, MoveToY);
    ShowLog('Not enough stamina for dash roll', [], ColorLogNotEnoughStamina);
  end;
end;

procedure TViewGame.TurnPathfindingOff(Sender: TObject);
begin
  Sound('menu_button');
  PathfindingActive := false;
end;

procedure TViewGame.TurnPathfindingOn(Sender: TObject);
begin
  Sound('menu_button');
  PathfindingActive := true;
end;

procedure TViewGame.ClickRepairItem(Sender: TObject);
begin
  if not CurrentCharacter.CurrentAction.CanStop then
    Exit;

  if DelayedUpdateInventory or DelayedUpdatePosition then
  begin
    ShowError('Tried to ClickRepairItem but inventory is not ready');
    Exit;
  end;

  RepairItem((Sender as TMapItemButton).MapItem);
end;

procedure TViewGame.RepairItem(const MapItem: TMapItem);
begin
  WakeUp(false, false);
  if MapItem.Item.ItemData.CanBeRepaired then
  begin
    if not CurrentCharacter.Inventory.CanUseHands then
    begin
      ShowLog('%s can''t repair %s with her hands restrained', [CurrentCharacter.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogBondage);
      Exit;
    end;
    if (MapItem.InteractingActorReferenceID <> 0) and (MapItem.InteractingActorReferenceID <> CurrentCharacter.ReferenceId) then
    begin
      if ObjectByReferenceId(MapItem.InteractingActorReferenceID) is TActor then
        ShowLog('Cannot repair %s, %s is using it.', [MapItem.Item.Data.DisplayName, TActor(ObjectByReferenceId(MapItem.InteractingActorReferenceID)).Data.DisplayName], ColorLogMapItemBroken)
      else
        ShowLog('Cannot repair %s, someone else is using it.', [MapItem.Item.Data.DisplayName], ColorLogMapItemBroken);
      Exit;
    end;
    if not (CurrentCharacter.CurrentAction is TActionPlayerFixItem) or (TActionPlayerFixItem(CurrentCharacter.CurrentAction).MapItem <> MapItem) then
    begin
      CurrentCharacter.CurrentAction := TActionPlayerFixItem.NewAction(CurrentCharacter);
      TActionPlayerFixItem(CurrentCharacter.CurrentAction).MapItem := MapItem;
      CurrentCharacter.CurrentAction.Start;
    end;
  end else
    ShowLog(GetTranslation('ActorItemCannotBeRepaired'), [MapItem.Item.Data.DisplayName], ColorLogMapItemBroken);
end;

procedure TViewGame.ClickEquipItem(Sender: TObject);
begin
  if not CurrentCharacter.CurrentAction.CanStop then
    Exit;

  if WakeUp(false, false) then
    Exit;

  if DelayedUpdateInventory or DelayedUpdatePosition then
  begin
    ShowError('Tried to ClickEquipItem but inventory is not ready');
    Exit;
  end;

  EquipItem((Sender as TMapItemButton).MapItem);
end;

procedure TViewGame.EquipItem(const MapItem: TMapItem);
begin
  WakeUp(false, false);
  if not MapItem.Item.Broken then
  begin
    if not CurrentCharacter.Inventory.CanUseHands then
    begin
      ShowLog('%s can''t equip %s with her hands restrained', [CurrentCharacter.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogBondage);
      Exit;
    end;
    if (MapItem.InteractingActorReferenceID <> 0) and (MapItem.InteractingActorReferenceID <> CurrentCharacter.ReferenceId) then
    begin
      if ObjectByReferenceId(MapItem.InteractingActorReferenceID) is TActor then
        ShowLog('Cannot equip %s, %s is using it.', [MapItem.Item.Data.DisplayName, TActor(ObjectByReferenceId(MapItem.InteractingActorReferenceID)).Data.DisplayName], ColorLogMapItemBroken)
      else
        ShowLog('Cannot equip %s, someone else is using it.', [MapItem.Item.Data.DisplayName], ColorLogMapItemBroken);
      Exit;
    end;
    case CurrentCharacter.Inventory.CanEquipItem(MapItem.Item) of
      eiOk: ;// do nothing;
      eiSlotRestrained:
        begin
          ShowLog(GetTranslation('ActorCannotEquipItemLog'), [CurrentCharacter.Data.DisplayName, MapItem.Item.Data.DisplayName, CurrentCharacter.Inventory.RestraintInTheWay(MapItem.Item).Data.DisplayName], ColorLogBondage);
          Exit;
        end;
      else
        begin
          // TODO: more explanation
          ShowLog('Can''t equip', [], ColorLogBondage);
          Exit;
        end;
    end;
    if not (CurrentCharacter.CurrentAction is TActionPlayerEquipItem) or (TActionPlayerEquipItem(CurrentCharacter.CurrentAction).MapItem <> MapItem) then
    begin
      CurrentCharacter.CurrentAction := TActionPlayerEquipItem.NewAction(CurrentCharacter);
      TActionPlayerEquipItem(CurrentCharacter.CurrentAction).MapItem := MapItem;
      CurrentCharacter.CurrentAction.Start;
    end;
  end else
  begin
    ShowLog(GetTranslation('ActorCannotEquipBrokenItemLog'), [MapItem.Item.Data.DisplayName], ColorLogMapItemBroken);
    RepairItem(MapItem);
  end;
end;

procedure TViewGame.IdentifyItem(const MapItem: TMapItem);
begin
  WakeUp(false, false);
  if not CurrentCharacter.Inventory.CanUseHands then
  begin
    ShowLog('%s can''t identify %s with her hands restrained', [CurrentCharacter.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogBondage);
    Exit;
  end;
  if (MapItem.InteractingActorReferenceID <> 0) and (MapItem.InteractingActorReferenceID <> CurrentCharacter.ReferenceId) then
  begin
    if ObjectByReferenceId(MapItem.InteractingActorReferenceID) is TActor then
      ShowLog('Cannot identify %s, %s is using it.', [MapItem.Item.Data.DisplayName, TActor(ObjectByReferenceId(MapItem.InteractingActorReferenceID)).Data.DisplayName], ColorLogMapItemBroken)
    else
      ShowLog('Cannot identify %s, someone else is using it.', [MapItem.Item.Data.DisplayName], ColorLogMapItemBroken);
    Exit;
  end;
  if not (CurrentCharacter.CurrentAction is TActionPlayerIdentifyMapItem) or (TActionPlayerIdentifyMapItem(CurrentCharacter.CurrentAction).MapItem <> MapItem) then
  begin
    CurrentCharacter.CurrentAction := TActionPlayerIdentifyMapItem.NewAction(CurrentCharacter);
    TActionPlayerIdentifyMapItem(CurrentCharacter.CurrentAction).MapItem := MapItem;
    CurrentCharacter.CurrentAction.Start;
  end;
end;

procedure TViewGame.ClickStealth(Sender: TObject);
begin
  Configuration.VisualizeNoise := not Configuration.VisualizeNoise;
  Configuration.Save;
end;

function TViewGame.CharacterNotInDanger: Boolean;
begin
  Exit(not Map.MonstersAttacking and Map.NoMonstersNearby(CurrentCharacter.LastTileX, CurrentCharacter.LastTileY, 3) and Map.NoMarksNearby(CurrentCharacter, 3));
end;

function TViewGame.CharacterSeesMonsters: Boolean;
begin
  Exit(Map.MonsterSeen(CurrentCharacter.LastTileX, CurrentCharacter.LastTileY, CurrentCharacter.VisibleRange));
end;

function TViewGame.IsResting: Boolean;
begin
  Exit(
    (CurrentCharacter.CurrentAction is TActionPlayerRest) or
    (CurrentCharacter.CurrentAction is TActionPlayerUnconscious)
  );
end;

function TViewGame.IsPause: Boolean;
begin
  Exit((container.FrontView <> Self) or PauseUi.Exists or CharacterScreen.Exists or MissionUi.Exists or PopupItem.Exists);
end;

procedure TViewGame.ClickPause(Sender: TObject);
begin
  if IsPause then
  begin
    Sound('unpause_game');
    UnPauseGame;
  end else
  begin
    Sound('pause_game');
    PauseGame;
  end;
end;

procedure TViewGame.ClickCamp(Sender: TObject);
begin
  if not CurrentCharacter.CurrentAction.CanStop then
    Exit;

  if IsResting then
  begin
    Sound('stop_rest');
    StopResting;
    if CharacterSeesMonsters then
    begin
      ShowLog('%s wakes up in danger! The game is set to pause', [CurrentCharacter.Data.DisplayName], ColorDefault);
      SetActivePause;
    end;
  end else
  begin
    if CharacterSeesMonsters then
    begin
      ShowLog('%s feels danger and can''t let her guard down', [CurrentCharacter.Data.DisplayName], ColorDefault);
      Exit;
    end;
    Sound('start_rest');
    StartResting;
  end;
end;

procedure TViewGame.PauseGame;
begin
  HideOtherPauseUis;
  PauseUi.Show;
  RestingOverlay.Exists := IsResting;
  WorkaroundUpdateOnceAfterPause := true;
end;

procedure TViewGame.UnPauseGame;
begin
  PauseUi.Hide;
  //ButtonCamp.Exists := IsResting; //TODO: maybe try to have a copy of camp button in normal game during resting?
  RestingOverlay.Exists := IsResting;
  if (CurrentCharacter.CurrentAction is TActionPlayerRest) then
    LabelResting.Caption := 'Resting...'
  else
  if (CurrentCharacter.CurrentAction is TActionPlayerUnconscious) then
    LabelResting.Caption := 'Unconscious.';
end;

procedure TViewGame.TogglePause;
begin
  if not CharacterScreen.Exists then
  begin
    if not PopupItem.Exists then
    begin
      if PauseUi.Exists then
      begin
        Sound('unpause_game');
        UnPauseGame;
      end else
      begin
        Sound('pause_game');
        PauseGame;
      end;
    end else
    begin
      Sound('menu_button');
      PopupItem.Hide;
    end;
  end else
  begin
    Sound('character_screen_hide');
    CharacterScreen.Hide;
  end;
end;

procedure TViewGame.StartResting;
begin
  CurrentCharacter.CurrentAction := TActionPlayerRest.NewAction(CurrentCharacter);
  CurrentCharacter.CurrentAction.Start;
  if IsPause then
    UnPauseGame;
end;

procedure TViewGame.StopResting;
begin
  CurrentCharacter.CurrentAction := TActionIdle.NewAction(CurrentCharacter);
  CurrentCharacter.CurrentAction.Start;
  UnPauseGame;
end;

procedure TViewGame.SetActivePause;
begin
  Configuration.PartyActive := false;
  Configuration.PauseMenuActive := false;
  PauseGame;
end;

function TViewGame.WakeUp(const ForceWakeUp: Boolean;
  const ActivePause: Boolean): Boolean;
begin
  if not IsResting then
  begin
    { TODO: Make a proper fix (not yet sure how)
      So, the problem is that if the character action is forced to change
      (e.g. stun or knockback or reset to idle [grabber, tickling slime, bug shooter, stun shooter, etc.])
      then IsResting = false and we just exit this procedure
      BUT the TimeSpeed is still set to resting speed
      and UI overlay is still intact
      Here we just detect the time speed and allow "wake-up" as regular }
    if RestingOverlay.Exists then
      LogWarning('The character was not resting, but UI seems to have been in resting mode. Forcing UI reset.')
    else
    if TimeSpeed > 1.1 then
    begin
      if not CharacterNotInDanger then
      begin
        ShowLog('Non-resting character received a wake-up event in danger, but time scale was %.1n. Setting active pause.', [TimeSpeed], ColorDefault);
        SetActivePause;
      end;
      CurrentCharacter.CurrentAction := TActionIdle.NewAction(CurrentCharacter);
      CurrentCharacter.CurrentAction.Start;
      Exit(false);
    end else
      Exit(false);
  end;

  Result := true; // an attempt was made, input should be discarded regardless of the result
  if ForceWakeUp or CurrentCharacter.CurrentAction.CanStop then
  begin
    StopResting;
    if ActivePause then
    begin
      ShowLog('%s wakes up in danger! The game is set to pause', [CurrentCharacter.Data.DisplayName], ColorDefault);
      SetActivePause;
    end;
  end;
end;

function TViewGame.IsActivePause: Boolean;
begin
  Exit(IsPause and not Configuration.PauseMenuActive and not Configuration.PartyActive);
end;

procedure TViewGame.ClickUnequipItem(Sender: TObject);
begin
  if not CurrentCharacter.CurrentAction.CanStop then
    Exit;

  if WakeUp(false, false) then
    Exit;

  if DelayedUpdateInventory then
  begin
    ShowError('Tried to ClickUnequipItem but inventory is not ready');
    Exit;
  end;

  UnequipItem((Sender as TInventoryButton).Item);
end;

procedure TViewGame.UnequipItem(const Item: TInventoryItem);
begin
  WakeUp(false, false);
  if not (esWeapon in Item.Data.EquipSlots) and not CurrentCharacter.Inventory.CanUseHands then // Todo: more general
  begin
    ShowLog('%s can''t unequip %s with her hands restrained', [CurrentCharacter.Data.DisplayName, Item.Data.DisplayName], ColorLogBondage);
    Exit;
  end;

  if Item.Data.Id = 'metal_collar' then // Todo: more general
  begin
    ShowLog('%s gropes the %s around her neck', [CurrentCharacter.Data.DisplayName, Item.Data.DisplayName], ColorLogBondage);
    ShowLog('It doesn''t take a degree in chemistry to say it''s made from solid titanium', [], ColorLogBondage);
    ShowLog('She has no idea who or how put it on, but one thing is sure: it won''t go off', [], ColorLogBondage);
    ShowLog('At least it''s light enough and doesn''t restrain movements', [], ColorLogBondage);
    ShowLog('A large symbol "∀" is embossed on the %s''s surface', [Item.Data.DisplayName], ColorLogBondage);
    Exit;
  end;

  if Item.IsBondage(CurrentCharacter.Inventory) then
  begin
    if CurrentCharacter.Stamina < 0 then // Todo: more general
    begin
      ShowLog('%s is completely exhausted and cannot struggle', [CurrentCharacter.Data.DisplayName], ColorLogBondage);
      Exit;
    end;
    CurrentCharacter.CurrentAction := TActionPlayerStruggleUnequipItem.NewAction(CurrentCharacter);
    TActionPlayerStruggleUnequipItem(CurrentCharacter.CurrentAction).ItemSlot := Item.ItemData.MainSlot;
    CurrentCharacter.CurrentAction.Start;
  end else
  begin
    CurrentCharacter.CurrentAction := TActionPlayerUnequipItem.NewAction(CurrentCharacter);
    TActionPlayerUnequipItem(CurrentCharacter.CurrentAction).ItemSlot := Item.ItemData.MainSlot;
    CurrentCharacter.CurrentAction.Start;
  end;
end;

function TViewGame.InternalMapInterface: TMapInterface;
begin
  Exit(MapInterface);
end;

procedure TViewGame.DrawInventory;
var
  E: TApparelSlot;
  I: Integer;
  Button: TInventoryButtonAbstract;
  Image: TCastleImageControl;
  InventoryButtonsHeight, FreeHeight: Single;
begin
  {$WARNING make sure asking for inventory.update twice a frame won't mess anything up }
  CurrentCharacter.Inventory.Update(0); // to make sure caches regenerate properly

  InventoryGroup.Translation := Vector2(-(EffectiveRect.Width - CharacterUi.EffectiveRect.Left), InventoryGroup.Translation.Y);
  InventoryGroup.Width := GetInventoryWidth;
  ScrollViewBottomGround.Width := InventoryGroup.Width;
  ScrollViewBottomGround.ScrollBarWidth := 11;
  VerticalGroupEquipped.ClearAndFreeControls;
  for E in GlobalUiApparelSlotsList do
    if E in CurrentCharacter.Blueprint.EquipmentSlots then
    begin
      if CurrentCharacter.Inventory.Apparel[E] <> nil then
      begin
        if CurrentCharacter.Inventory.Apparel[E].Data.MainSlot = E then
        begin
          Button := TInventoryButton.Create(VerticalGroupEquipped);
          Button.OnLeftClick := @ClickUnequipItem;
          Button.OnRightClick := @TogglePopupItem;
          Button.OnLongClick := @TogglePopupItem;
          TInventoryButton(Button).Item := CurrentCharacter.Inventory.Equipped[E];
        end else
          continue; // this item either was or will be inserted as a different equipment slot
      end else
      begin
        if (E = esLeash) or (E = esGag) then // leash or gag are seen only when equipped
          Continue;
        Button := TEmptyInventoryButton.Create(VerticalGroupEquipped);
        TEmptyInventoryButton(Button).Slot := E;
      end;
      Button.Setup;
      Button.Width := InventoryGroup.EffectiveWidth;
      VerticalGroupEquipped.InsertBack(Button);
    end;

  // Ok, I'm stupid, but I have to start somehow. Let's try this way:
  InventoryButtonsHeight := 0;
  for I := 0 to Pred(VerticalGroupEquipped.ControlsCount) do
    InventoryButtonsHeight += (VerticalGroupEquipped.Controls[I] as TInventoryButtonAbstract).Height;// - 20; //hacking
  FreeHeight := EffectiveHeight;// - 20 * VerticalGroupEquipped.ControlsCount; //hacking
  for I := 0 to Pred(VerticalGroupEquipped.ControlsCount) do
    (VerticalGroupEquipped.Controls[I] as TInventoryButtonAbstract).Height :=
      FreeHeight / InventoryButtonsHeight * (VerticalGroupEquipped.Controls[I] as TInventoryButtonAbstract).Height;// + 20;
  // ShowLog('%.0n / %.0n', [FreeHeight, InventoryButtonsHeight], ColorDefault);

  { STATUS EFFECTS }

  VerticalGroupStatusEffects.ClearAndFreeControls;
  // TODO: sort effects by severity
  for I := 0 to MinInteger(Pred(CurrentCharacter.Inventory.StatusEffects.Count), MaxStatusEffectsDisplayed) do
  begin
    Image := TCastleImageControl.Create(VerticalGroupStatusEffects);
    if I < MaxStatusEffectsDisplayed then
      Image.DrawableImage := CurrentCharacter.Inventory.StatusEffects[I].Effect.Image
    else
      Image.DrawableImage := StatusEffectsOverflowIcon;
    Image.OwnsDrawableImage := false;
    Image.Width := 30;
    Image.Height := 30;
    Image.Stretch := true;
    if I < MaxStatusEffectsDisplayed then
      VerticalGroupStatusEffects.InsertFront(Image)
    else
      VerticalGroupStatusEffects.InsertBack(Image); //we want "more effects" icon on top, not at bottom
  end;

  { SPELLS }

  if CurrentCharacter.Inventory.Spells.Count > 0 then
  begin
    ButtonMagic.Exists := true;
    // hot cast last spell? adjust icon?
  end else
    ButtonMagic.Exists := false;

  { COVER STATUS }
  ImageTopCovered.Exists := CurrentCharacter.Inventory.TopCovered > 0;
  //ImageTopCovered.Color is always green
  ImageTopNude.Exists := (CurrentCharacter.Inventory.TopCovered = 0) and not Configuration.Censored; // TODO: Censored in screenshots
  if (CurrentCharacter.Inventory.EmbarrasmentMultiplier < 0.5) and (CurrentCharacter.Inventory.TouchMultiplier < 0.5) then
    ImageTopNude.Color := ColorUiBodyResistant
  else
    ImageTopNude.Color := ColorUiBodyVulnerable;
  ImageTopNudeCensored.Exists := (CurrentCharacter.Inventory.TopCovered = 0) and Configuration.Censored;
  ImageTopNudeCensored.Color := ImageTopNude.Color;
  ImageTopVisible.Exists := CurrentCharacter.Inventory.TopCovered = 0;
  if CurrentCharacter.Inventory.EmbarrasmentMultiplier < 0.5 then
    ImageTopVisible.Color := ColorUiNudityResist
  else
    ImageTopVisible.Color := ColorUiNudityVulnerable;
  ImageTopTouch.Exists := CurrentCharacter.Inventory.TopCovered = 0;
  if CurrentCharacter.Inventory.TouchMultiplier < 0.5 then
    ImageTopTouch.Color := ColorUiNudityResist
  else
    ImageTopTouch.Color := ColorUiNudityVulnerable;

  ImageBottomCovered.Exists := CurrentCharacter.Inventory.BottomCovered > 0;
  //ImageBottomCovered.Color is always green
  ImageBottomNude.Exists := (CurrentCharacter.Inventory.BottomCovered = 0) and not Configuration.Censored; // TODO: Censored in screenshots
  ImageBottomNude.Color := ImageTopNude.Color;
  ImageBottomNudeCensored.Exists := (CurrentCharacter.Inventory.BottomCovered = 0) and Configuration.Censored;
  ImageBottomNudeCensored.Color := ImageTopNude.Color;
  ImageBottomVisible.Exists := CurrentCharacter.Inventory.BottomCovered = 0;
  ImageBottomVisible.Color := ImageTopVisible.Color;
  ImageBottomTouch.Exists := CurrentCharacter.Inventory.BottomCovered = 0;
  ImageBottomTouch.Color := ImageTopTouch.Color;
end;

procedure TViewGame.DrawMapItemsInventory;
var
  MapItem: TMapItem;
  Button: TMapItemButton;
begin
  InventoryGroup.Translation := Vector2(-(EffectiveRect.Width - CharacterUi.EffectiveRect.Left), InventoryGroup.Translation.Y);
  InventoryGroup.Width := GetInventoryWidth;
  ScrollViewBottomGround.Width := InventoryGroup.Width;
  VerticalGroupGround.ClearAndFreeControls;
  for MapItem in Map.MapItemsList do
    if MapItem.CollidesInt(CurrentCharacter, 1) then
    begin
      Button := TMapItemButton.Create(VerticalGroupGround);
      Button.MapItem := MapItem;
      Button.OnLeftClick := @ClickEquipItem;
      Button.OnMiddleClick := @ClickRepairItem;
      Button.OnRightClick := @TogglePopupItem;
      Button.OnLongClick := @TogglePopupItem;
      Button.Setup;
      Button.Width := InventoryGroup.EffectiveWidth;
      VerticalGroupGround.InsertBack(Button);
    end;
  ScrollViewBottomGround.Scroll := ScrollViewBottomGround.Scroll; // Workaround to clamp the value
end;

procedure TViewGame.ShowReportOnCapturedCharactersOnThisLevel;
var
  CaptivesCount: Integer;
begin
  CaptivesCount := Map.CapturedCharactersAtLevel;
  case CaptivesCount of
    0: ; // show nothing
    1: ShowLog('There is a character imprisoned on this level. Find and long-press/right-click to rescue her.', [], ColorLogCapturedCharacterHere);
    else
      ShowLog('There are %d characters imprisoned on this level. Find and long-press/right-click to rescue them.', [CaptivesCount], ColorLogCapturedCharacterHere);
  end;
end;

function TViewGame.GetInventoryWidth: Single;
begin
  if FullInventory then
    Exit(EffectiveRect.Width - CharacterUi.EffectiveRect.Left)
  else
    Exit(CompactInventoryWidth);
end;

function TViewGame.TimeSpeed: Single;
begin
  if IsPause then
    Exit(0.0)
  else
  if CurrentCharacter.CurrentAction = nil then
  begin
    ShowError('TViewGame.TimeSpeed: CurrentCharacter.CurrentAction = nil');
    Exit(1.0);
  end else
  if CurrentCharacter.Unsuspecting or CharacterNotInDanger then
    Exit(CurrentCharacter.CurrentAction.ProposedTimeSpeed)
  else
    Exit(1.0);
end;

procedure TViewGame.BeforeScreenshot;
begin
  inherited;
  DoUpdateInventory; // will also call DoUpdatePosition;
end;

procedure TViewGame.AfterScreenshot;
begin
  inherited;
  DoUpdateInventory; // will also call DoUpdatePosition;
end;

class procedure TViewGame.InvalidateInventory(Sender: TObject);
begin
  if (Sender <> nil) and not (Sender is TPlayerCharacter) then
  begin
    ShowError('Cannot invalidate class %s, expected TPlayerCharacter', [Sender.ClassName]);
    DelayedUpdateInventory := true;
    Exit;
  end;
  if (Sender = ViewGame.CurrentCharacter) or (Sender = nil) then
    DelayedUpdateInventory := true;
end;

class function TViewGame.FullInventory: Boolean;
begin
  Exit(FFullInventory);
end;

class procedure TViewGame.InvalidatePosition(Sender: TObject);
begin
  if (Sender <> nil) and not (Sender is TPlayerCharacter) then
  begin
    ShowError('Cannot invalidate class %s, expected TPlayerCharacter', [Sender.ClassName]);
    DelayedUpdatePosition := true;
    Exit;
  end;
  if (Sender = ViewGame.CurrentCharacter) or (Sender = nil) then
    DelayedUpdatePosition := true;
end;

procedure TViewGame.DoUpdateInventory;
var
  P: TPlayerCharacter;
begin
  DelayedUpdateInventory := false;
  try
    for P in Map.CharactersOnThisLevel do
      P.Inventory.PaperDollSprite.UpdateToInventory; // TODO: optimize, we often need to update just one
    PaperDoll.UpdateToInventory;
  except
    ShowError('Exception during PaperDoll/Sprite.UpdateToInventory.');
  end;

  try
    DrawInventory;
  except
    ShowError('Exception during DrawInventory.');
  end;

  DoUpdatePosition; // TODO: Optimize, we don't always need this here

  if PauseUi.Exists then
    PauseUi.Show;
end;
procedure TViewGame.DoUpdatePosition;
begin
  DelayedUpdatePosition := false;
  DrawMapItemsInventory;
end;

procedure TViewGame.AddLog(const AMessage: String; const Args: array of const;
  const AColor: TCastleColor);
var
  NormalLabel, ShadowLabel: TCastleLabel;
  Removed: TCastleUserInterface;
begin
  try
    ShadowLabel := TCastleLabel.Create(PlayerLogVerticalGroup);
    ShadowLabel.Caption := Format(AMessage, Args);
    ShadowLabel.Color := CastleColors.Black;
    ShadowLabel.CustomFont := FontBender20;
    NormalLabel := TCastleLabel.Create(ShadowLabel);
    NormalLabel.Caption := ShadowLabel.Caption;
    NormalLabel.Color := AColor;
    NormalLabel.Translation := Vector2(1, -1);
    NormalLabel.CustomFont := FontBender20;
    ShadowLabel.InsertFront(NormalLabel);
    PlayerLogVerticalGroup.InsertFront(ShadowLabel);
    while PlayerLogVerticalGroup.ControlsCount > Configuration.LogLength do
    begin
      Removed := PlayerLogVerticalGroup.Controls[0];
      PlayerLogVerticalGroup.RemoveControl(Removed);
      FreeAndNil(Removed);
    end;
  except
    on E: Exception do
    begin
      ShowError('Exception while trying to show log/error message %s: %s', [E.ClassName, E.Message]);
      LogNormal('Original message was: ' + AMessage);
      // raise; -- try to survive this one? There is no point in raising exception if only message broke?
    end;
  end;
end;

procedure TViewGame.ShakeCharacter;
begin
  if Configuration.ShakeScreen then
    UiShakerCharacter.Shake(30, 0.5);
  Vibrate(200);
end;

procedure TViewGame.ShakeMap;
begin
  if Configuration.ShakeScreen then
    UiShakerMap.Shake(30, 0.5);
  Vibrate(100);
end;

procedure TViewGame.ScheduleMonstersToIdle;
begin
  MonstersToIdlePending := true;
end;

procedure TViewGame.StopShakers;
begin
  UiShakerCharacter.StopShake;
  UiShakerMap.StopShake;
end;

procedure TViewGame.ScheduleEndGame;
begin
  ShouldEndGame := true;
  // make sure no longer existing items won't receive update - doesn't feel clean, maybe TODO?
  VerticalGroupEquipped.ClearAndFreeControls;
  VerticalGroupGround.ClearAndFreeControls;
end;

function TViewGame.PathfindingDisabled: Boolean;
begin
  Exit(CurrentCharacter.Blindfolded or CurrentCharacter.Inventory.LeashHeld or not PathfindingActive or DirectInputActive);
end;

procedure TViewGame.BeforeRender;

  procedure DoFirstFrame;
  begin
    MapInterface.Zoom := Container.PixelsHeight / RenderTileSize / 2.0 / TPlayerCharacter.DefaultVisionRange;
  end;

var
  Ui: TCastleUserInterface;
begin
  inherited BeforeRender;

  if FirstFrame then
  begin
    DoFirstFrame;
    FirstFrame := False;
  end;

  if DelayedUpdateInventory then
    DoUpdateInventory
  else
  if DelayedUpdatePosition then
    DoUpdatePosition;
  for Ui in VerticalGroupEquipped do
    if Ui is TInventoryButton then
      TInventoryButton(Ui).UpdateCaption;
  for Ui in VerticalGroupGround do
    if Ui is TInventoryButton then
      TInventoryButton(Ui).UpdateCaption;
end;

procedure TViewGame.Update(const SecondsPassed: Single; var HandleInput: Boolean);
const
  SecondsWarning = 12;
  NextMapTimeout = 3;
var
  LX, LY: Int16;
  PhysicalSeconds: Single;
  M: TMonster;
  P: TParticle;
  PlayerChar: TPlayerCharacter;
  Mark: TMarkAbstract;
  TimeI: Integer;

  procedure SpawnPatrolMonsters;
  var
    AMonster: TMonster;
    MonsterData: TMonsterData;
    MonstersToSpawn: Integer;
    SpawnVacuumCleaner: Boolean;

    procedure DoSpawnPatrol;
    var
      I: Integer;
      SX, SY: Int16;
      GoX, GoY: Int16;
      GoToX, GoToY: Int16;
    begin
      repeat
        GoX := Rnd.Random(Map.SizeX);
        GoY := Rnd.Random(Map.SizeY);
      until Map.PassableTiles[Pred(MonsterData.Size)][GoX + Map.SizeX * GoY] and ((Map.Visible[GoX + Map.SizeX * GoY] = 0) or (Rnd.Random < 0.05));

      for I := 0 to MonstersToSpawn do
      begin
        if MonsterData.CuriousAi then
          repeat
            GoX := Rnd.Random(Map.SizeX);
            GoY := Rnd.Random(Map.SizeY);
          until Map.PassableTiles[Pred(MonsterData.Size)][GoX + Map.SizeX * GoY];

        AMonster := TMonster.Create;
        AMonster.Data := MonsterData;
        AMonster.Reset;
        repeat
          SX := Map.ExitX - AMonster.PredSize + Rnd.Random(3 + AMonster.PredSize * 2);
          SY := Map.ExitY - AMonster.PredSize + Rnd.Random(3 + AMonster.PredSize * 2);
        until (SX > 0) and (SY > 0) and (SX < Map.SizeX - AMonster.Size) and (SY < Map.SizeY - AMonster.Size) and Map.PassableTiles[AMonster.PredSize][SX + Map.SizeX * SY];
        AMonster.Teleport(SX, SY);
        AMonster.Ai.Guard := false;
        repeat
          GoToX := GoX + Rnd.RandomSign;
          GoToY := GoY + Rnd.RandomSign;
        until Map.PassableTiles[AMonster.PredSize][GoToX + Map.SizeX * GoToY];
        AMonster.MoveTo(GoToX + Rnd.Random, GoToY + Rnd.Random);
        AMonster.Ai.Timeout := (AMonster.CurrentAction as TActionMoveAbstract).RemainingTime;
        if AMonster.MonsterData.VacuumCleaner then
          Map.MonstersList.Insert(0, AMonster)
        else
          Map.MonstersList.Add(AMonster);
      end;
    end;

    function GetMonsterInPatrol(const IsRare: Boolean): TMonsterData;
    var
      M: Integer;
      ML: TMonstersDataList;
    begin
      ML := TMonstersDataList.Create(false);
      for M := 0 to Pred(Map.PatrolList.Count) do
        if Map.PatrolList[M].Rare = IsRare then
          ML.Add(Map.PatrolList[M]);
      if ML.Count > 0 then
        Result := ML[Rnd.Random(ML.Count)]
      else
        Result := nil;
      FreeAndNil(ML);
    end;

  begin
    SpawnVacuumCleaner := (Map.MapItemsList.Count > 3) and (Rnd.Random < 0.2);
    if SpawnVacuumCleaner then
      for AMonster in Map.MonstersList do
        if AMonster.CanAct and AMonster.MonsterData.VacuumCleaner then
          SpawnVacuumCleaner := false;

    if SpawnVacuumCleaner then
    begin
      MonsterData := VacuumCleanersData[Rnd.Random(VacuumCleanersData.Count)];
      MonstersToSpawn := 0;
      DoSpawnPatrol;
    end;

    //spawn regular monsters
    if Map.PatrolList.Count = 0 then
    begin
      ShowError('Cannot find any monster to spawn patrol at level %d', [Map.CurrentDepth]);
      Exit;
    end;

    MonsterData := GetMonsterInPatrol(false);
    if MonsterData <> nil then
    begin
      MonstersToSpawn := 1 + Rnd.Random(Round(Sqrt(Map.CurrentDepth / MonsterData.Danger)) + 1); // 2 - 3 on lvl.1 2 - 8 on lvl.25
      DoSpawnPatrol;
    end;

    if Rnd.Random < 0.3 then
    begin
      MonsterData := GetMonsterInPatrol(true);
      if MonsterData <> nil then
      begin
        MonstersToSpawn := 0 + Rnd.Random(Round(Sqrt(Map.CurrentDepth / MonsterData.Danger / 3)));
        DoSpawnPatrol;
      end;
    end;
  end;

  procedure UpdateDangerSensor;
  var
    MAttacking: Boolean;
  begin
    MAttacking := Map.MonstersAttacking;
    StealthDangerMeter.Exists := MAttacking or CharacterSeesMonsters;
    if MAttacking then
      StealthDangerMeter.Color := ColorStealthDanger
    else
      StealthDangerMeter.Color := ColorStealthWarning;
  end;

  procedure TryMoveByDirectInput;
  var
    DDX, DDY: Integer;
    SelectedMonster: TMonster;
    RollVector: TVector2;

    procedure MoveCharToDelta(const DX, DY: Integer);
    begin
      DDX += DX;
      DDY += DY;
    end;

  begin
    DirectInputActive := false;
    if (not CurrentCharacter.Immobilized) and (not CurrentCharacter.Unsuspecting) and CurrentCharacter.CurrentAction.CanStop and not (CurrentCharacter.CurrentAction is TActionRoll) then
    begin
      DDX := 0; DDY := 0;
      if Container.Pressed[keyW] or Container.Pressed[keyArrowUp] or Container.Pressed[keyNumpad8] then MoveCharToDelta(0, 1);
      if Container.Pressed[keyA] or Container.Pressed[keyArrowLeft] or Container.Pressed[keyNumpad4] then MoveCharToDelta(-1, 0);
      if Container.Pressed[keyS] or Container.Pressed[keyArrowDown] or Container.Pressed[keyNumpad2] then MoveCharToDelta(0, -1);
      if Container.Pressed[keyD] or Container.Pressed[keyArrowRight] or Container.Pressed[keyNumpad6] then MoveCharToDelta(1, 0);
      if (DDX <> 0) or (DDY <> 0) then
      begin
        DirectInputActive := true;
        if Container.Pressed[keySpace] and (CurrentCharacter.Stamina > 0) then
        begin
          RollVector := CurrentCharacter.Data.RollRange * Vector2(DDX, DDY).Normalize;
          CurrentCharacter.RollTo(CurrentCharacter.CenterX + RollVector.X, CurrentCharacter.CenterY + RollVector.Y);
          CurrentCharacter.DegradeStamina(CurrentCharacter.GetRollCost);
        end else
        if Container.Pressed[keyShift] then
          CurrentCharacter.MoveTo(CurrentCharacter.X + DDX, CurrentCharacter.Y + DDY)
        else
        begin
          SelectedMonster := GetMonster(CurrentCharacter.CenterX + 3 * DDX, CurrentCharacter.CenterY + 3 * DDY, false);
          if SelectedMonster = nil then
            CurrentCharacter.MoveTo(CurrentCharacter.X + DDX, CurrentCharacter.Y + DDY)
          else
            CurrentCharacter.MoveAndAct(SelectedMonster, PlayerActionAttack);
        end;
      end;
    end;
  end;

begin
  inherited;

  DoZoom(SecondsPassed);

  PhysicalSeconds := SecondsPassed * TimeSpeed;
  if TimeSpeed > 1.1 then
  begin
    FastForwardIcon.Exists := true;
    LabelTimeAcceleration.Caption := Round(TimeSpeed).ToString + 'x';
  end else
    FastForwardIcon.Exists := false;
  PauseIcon.Exists := TimeSpeed = 0;

  Map.TimeSpentOnTheMap += PhysicalSeconds;

  UpdateDangerSensor;

  HealthBar.ClipLine := Vector3(-1, 0, CurrentCharacter.Health / CurrentCharacter.PlayerCharacterData.MaxHealth);
  HealthAntiBar.ClipLine := Vector3(1, 0, -CurrentCharacter.MaxHealth / CurrentCharacter.PlayerCharacterData.MaxHealth);
  HealthLabel.Caption := Round(CurrentCharacter.Health).ToString;
  StaminaBar.ClipLine := Vector3(-1, 0, CurrentCharacter.Stamina / CurrentCharacter.PlayerCharacterData.MaxStamina);
  StaminaAntiBar.ClipLine := Vector3(1, 0, -CurrentCharacter.MaxStamina / CurrentCharacter.PlayerCharacterData.MaxStamina);
  StaminaLabel.Caption := Round(CurrentCharacter.Stamina).ToString;
  WillBar.ClipLine := Vector3(-1, 0, CurrentCharacter.Will / CurrentCharacter.PlayerCharacterData.MaxWill);
  WillAntiBar.ClipLine := Vector3(1, 0, -CurrentCharacter.MaxWill / CurrentCharacter.PlayerCharacterData.MaxWill);
  WillLabel.Caption := Round(CurrentCharacter.Will).ToString;
  if (CurrentCharacter.PlayerCharacterData.MaxMagic > 0) and (CurrentCharacter.Inventory.Spells.Count > 0) then
  begin
    ButtonMagic.Enabled := CurrentCharacter.Magic > 0;
    MagicBarBackground.Exists := true;
    MagicBar.ClipLine := Vector3(-1, 0, CurrentCharacter.Magic / CurrentCharacter.PlayerCharacterData.MaxMagic);
    MagicAntiBar.ClipLine := Vector3(1, 0, -CurrentCharacter.MaxMagic / CurrentCharacter.PlayerCharacterData.MaxMagic);
    MagicLabel.Caption := Round(CurrentCharacter.Magic).ToString;
  end else
    MagicBarBackground.Exists := false;

  XpBar.ClipLine := Vector3(-1, 0, CurrentCharacter.Experience.ExperienceFractionToNextLevel);
  XpBarSlider.Translation := Vector2((XpBarBackground.EffectiveRect.Width - XpBarSlider.EffectiveRect.Width) * CurrentCharacter.Experience.ExperienceFractionToNextLevel, 0);
  XpBarLabel.Caption := CurrentCharacter.Experience.Level.ToString;

  LabelStealth.Caption := Round(CurrentCharacter.GetNoise).ToString;

  //todo: cache to optimize?
  LabelNormalDamage.Caption := (Round(10 * CurrentCharacter.Inventory.GetDamage) / 10).ToString;
  LabelStealthDamage.Caption := (Round(10 * CurrentCharacter.Inventory.GetStealthDamage) / 10).ToString;

  //todo: cache to optimize?
  ButtonNoPathfinding.Exists := PathfindingDisabled;
  ButtonNoPathfinding.Enabled := not (CurrentCharacter.Blindfolded or CurrentCharacter.Inventory.LeashHeld);
  ButtonPathfinding.Exists := not ButtonNoPathfinding.Exists;

  VerticalGroupStatusEffects.Exists := not PaperDoll.Nude;
  VerticalGroupOverallStats.Exists := not PaperDoll.Nude;

  LabelTime.Exists := Configuration.ShowClock;
  if LabelTime.Exists then
    LabelTime.Caption := FormatDateTime('hh:nn', Now);

  if not Self.IsPause or WorkaroundUpdateOnceAfterPause then
  begin
    WorkaroundUpdateOnceAfterPause := false;
    LX := CurrentCharacter.LastTileX; // Save last current character's coordinates to see if they've changed
    LY := CurrentCharacter.LastTileY;

    for PlayerChar in Map.CharactersOnThisLevel do
      //if PlayerChar.CanAct --- will not "capture" in Update TODO
      PlayerChar.Update(PhysicalSeconds); // no need to try...except, everything is wrapped inside :)

    MapInterface.TargetX := CurrentCharacter.CenterX;
    MapInterface.TargetY := CurrentCharacter.CenterY;

    for M in Map.MonstersList do
      if M.CanAct then
        // no need to try...except, everything is wrapped inside :)
        M.Update(PhysicalSeconds)
      else
        if not (M.CurrentAction is TActionIdle) then
          M.DieSafe;

    {$WARNING Something tries to break marks if action wasn't freed, and makes "invalid pointers" below}
    GarbageCollector.Clear;

    for Mark in Map.MarksList do // NOTE: marks can "delete themselves from MarksList" at least in case of error
      try
        Mark.Update(PhysicalSeconds);
      except
        on E: TMarkQuitEarly do
        begin
          LogNormal('Workaround %s:%s', [E.ClassName, E.Message]); // note: Mark.ClassName no longer accessible because Mark is most likely a dangling pointer now
        end;
        on E: Exception do
        begin
          try
            ShowError('%s:"%s" during Mark.Update (%s).', [E.ClassName, E.Message, Mark.ClassName]);
          except
            ShowError('Exception while trying to report exception in TViewGame.Update->Mark.Update');
          end;
        end;
      end;

    // temporary?
    // Note: must come after monsters.update and marks.update because they can change player character's position
    if (LX <> CurrentCharacter.LastTileX) or (LY <> CurrentCharacter.LastTileY) then
      InvalidatePosition(CurrentCharacter);

    // show warning about incoming patrol
    if (Map.EnemySpawnTime < SecondsWarning) and (CurrentCharacter.DistanceTo(Map.ExitX, Map.ExitY) < 30) and
       MapInterface.ExitVisible and not IsResting then
    begin
      for TimeI := 0 to SecondsWarning div 2 do
        if (Map.EnemySpawnTime > 1 + 2 * TimeI) and (Map.EnemySpawnTime - PhysicalSeconds <= 1 + 2 * TimeI) then
        begin
          NewParticle(Map.ExitX + 1.5, Map.ExitY, 'WARNING', ColorParticleSpawn);
          break;
        end;
    end;
    Map.EnemySpawnTime -= PhysicalSeconds;
    if Map.EnemySpawnTime < 0 then
    begin
      Map.EnemySpawnTime := Difficulty.EnemyPatrolSpawnTime;
      SpawnPatrolMonsters;
    end;

    if (CurrentCharacter.LastTileX >= Map.ExitX - 1) and (CurrentCharacter.LastTileX <= Map.ExitX + 1) and
       (CurrentCharacter.LastTileY >= Map.ExitY - 1) and (CurrentCharacter.LastTileY <= Map.ExitY + 1) and
       (CurrentCharacter.CurrentAction is TActionIdle) then
    begin
      if Map.CurrentDepth = 0 then
        ShowMissionUi
      else
      begin
        for TimeI := 0 to Pred(NextMapTimeout) do
          if (ExitMapCounter + PhysicalSeconds > TimeI) and (ExitMapCounter <= TimeI) then
          begin
            NewParticle(Map.ExitX + 1.5, Map.ExitY, 'GO DOWN ' + IntToStr(NextMapTimeout - TimeI), ColorParticleExit);
            break;
          end;
        ExitMapCounter += PhysicalSeconds;
        if ExitMapCounter > NextMapTimeout then
        begin
          Sound('going_down');
          NewMap;
        end;
      end;
    end else
      ExitMapCounter := 0;

    TryMoveByDirectInput;
  end;

  // not sure - maybe better if it'd go into if not IsPause
  Map.UpdateVisible;

  // notifications should update regardless of Pause
  for P in Map.ParticlesList do
    //try..except - no need for now, very simple inside
    P.Update(SecondsPassed);

  LabelFps.Exists := Configuration.ShowFps;
  if LabelFps.Exists then
    LabelFps.Caption := Format('%d', [Round(Container.Fps.RealFps)]);

  if MonstersToIdlePending then
  begin
    MonstersToIdlePending := false;
    Map.MonstersToIdle;
  end;

  if ShouldEndGame then
  begin
    Container.PushView(ViewEndGame);
    StartNewRun(false); // workaround that new map music will overwrite game-over music
    SaveGame;
    ShouldEndGame := false;
  end;

  if (SecondsSinceLastSave > 30) and (CurrentCharacter.CurrentAction is TActionIdle) then
    SaveGame;
end;

function TViewGame.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;

  if Result then
    Exit;

  if Event.IsKey(keyPlus) or Event.IsKey(keyEqual) or Event.IsKey(keyNumpadPlus) then
    DoZoomIn;
  if Event.IsKey(keyMinus) or Event.IsKey(keyReserved_95) or Event.IsKey(keyNumpadMinus) then
    DoZoomOut;
  if Event.EventType = itMouseWheel then
    if Event.MouseWheelScroll > 0 then
    begin
      DoZoomIn;
      DoZoom(Event.MouseWheelScroll * MouseScrollZoomSensitivity);
      DoReleaseZoom;
    end else
    if Event.MouseWheelScroll < 0 then // avoid changing anything if = 0
    begin
      DoZoomOut;
      DoZoom(-Event.MouseWheelScroll * MouseScrollZoomSensitivity);
      DoReleaseZoom;
    end;

  if Event.IsKey(keyEscape) then
    if PauseUi.Exists or (not IsPause) then
      TogglePause
    else
    if CharacterUi.Exists then
      TogglePause;

  if Event.IsKey(keyF1) then
    DebugUi.Toggle;

  if Event.IsKey(keyF4) then
  begin
    if CurrentCharacter.Inventory.Spells.Count = 0 then
    begin
      CurrentCharacter.Inventory.Spells.Add(TSpell.Create);
      CurrentCharacter.Inventory.Spells[0].Price := 50;
      CurrentCharacter.Inventory.Spells[0].Parent := CurrentCharacter;
      CurrentCharacter.Inventory.Spells[0].Effects.Add(TSpellEffectApplyStatusEffect.Create);
      with CurrentCharacter.Inventory.Spells[0].Effects[0] as TSpellEffectApplyStatusEffect do
      begin
        Parent := CurrentCharacter;
        SpellTarget := stSelf;
        Effect := TStatusEffect.Create;
        Effect.Timeout := 60;
        Effect.Effect := TEnchantmentChangeMaxHealthPerSecond.Create;
        Effect.Effect.Strength := 1;
      end;
    end;
    InvalidateInventory(CurrentCharacter);
  end;
end;

function TViewGame.Release(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;

  if Event.IsKey(keyPlus) or Event.IsKey(keyEqual) or Event.IsKey(keyNumpadPlus) or
     Event.IsKey(keyMinus) or Event.IsKey(keyReserved_95) or Event.IsKey(keyNumpadMinus) then
    DoReleaseZoom;
end;

end.

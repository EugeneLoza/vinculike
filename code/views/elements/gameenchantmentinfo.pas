{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameEnchantmentInfo;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleControls, CastleUiControls,
  GameEnchantmentAbstract, GameStatusEffect;

type
  TEnchantmentInfoAbstract = class abstract(TCastleUserInterface)
  protected
    HorizontalGroup: TCastleHorizontalGroup;
    LabelDescription: TCastleLabel;
    Icon: TCastleImageControl;
  public
    procedure SetWidth(const NewWidth: Single); virtual;
    constructor Create(AOwner: TComponent); override;
  end;

type
  TEnchantmentInfo = class(TEnchantmentInfoAbstract)
  public
    procedure FillIn(const AEnchantment: TEnchantmentAbstract);
  end;

type
  TStatusEffectInfo = class(TEnchantmentInfoAbstract)
  public
    procedure FillIn(const AStatusEffect: TStatusEffect);
  end;

implementation
uses
  CastleComponentSerialize,
  CastleColors, CastleRectangles, CastleVectors, CastleImages,
  GameFonts, GameColors, GameCachedImages,
  GameViewGame;

{ TEnchantmentInfoAbstract }

procedure TEnchantmentInfoAbstract.SetWidth(const NewWidth: Single);
begin
  LabelDescription.MaxWidth := NewWidth - Icon.EffectiveRect.Width - HorizontalGroup.Spacing;

  Width := NewWidth;
  Height := LabelDescription.EffectiveRect.Height;
end;

constructor TEnchantmentInfoAbstract.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  HorizontalGroup := TCastleHorizontalGroup.Create(Self);
  HorizontalGroup.Alignment := vpMiddle;
  HorizontalGroup.Spacing := 10;
  InsertFront(HorizontalGroup);

  Icon := TCastleImageControl.Create(HorizontalGroup);
  Icon.Stretch := true;
  Icon.Width := 30;
  Icon.Height := 30;
  HorizontalGroup.InsertFront(Icon);

  LabelDescription := TCastleLabel.Create(HorizontalGroup);
  LabelDescription.CustomFont := FontBender40;
  LabelDescription.Alignment := hpLeft;
  HorizontalGroup.InsertFront(LabelDescription);
end;

{ TEnchantmentInfo ---------------------------------------}

procedure TEnchantmentInfo.FillIn(const AEnchantment: TEnchantmentAbstract);
begin
  LabelDescription.Caption := Format(AEnchantment.Description, []);
  if not AEnchantment.IsStochastic then
  begin
    Icon.DrawableImage := AEnchantment.Image;
    Icon.OwnsDrawableImage := false; // needs to be set later after Create, as by default there's something there
    if ViewGame.CurrentCharacter.Inventory.EnchantmentRequirementsMet(AEnchantment) then
    begin
      if AEnchantment.IsPositive then
        LabelDescription.Color := ColorUiEnchantmentDeterministicPositive
      else
        LabelDescription.Color := ColorUiEnchantmentDeterministicNegative;
    end else
      LabelDescription.Color := ColorUiEnchantmentInactive;
  end else
  if AEnchantment.Identified then
  begin
    Icon.DrawableImage := AEnchantment.Image;
    Icon.OwnsDrawableImage := false; // needs to be set later after Create, as by default there's something there
    if ViewGame.CurrentCharacter.Inventory.EnchantmentRequirementsMet(AEnchantment) then
    begin
      if AEnchantment.IsPositive then
        LabelDescription.Color := ColorUiEnchantmentStochasticPositive
      else
        LabelDescription.Color := ColorUiEnchantmentStochasticNegative;
    end else
      LabelDescription.Color := ColorUiEnchantmentInactive;
  end else
  begin
    Icon.DrawableImage := StatusEffectUnidentified;
    Icon.OwnsDrawableImage := false; // needs to be set later after Create, as by default there's something there
    LabelDescription.Caption := '????????';
    LabelDescription.Color := ColorUiEnchantmentUnidentified;
  end;
end;

{ TStatusEffectInfo -------------------------------------}

procedure TStatusEffectInfo.FillIn(const AStatusEffect: TStatusEffect);
begin
  LabelDescription.Caption := Format(AStatusEffect.Description, []);
  Icon.DrawableImage := AStatusEffect.Effect.Image;
  Icon.OwnsDrawableImage := false; // needs to be set later after Create, as by default there's something there
  if ViewGame.CurrentCharacter.Inventory.EnchantmentRequirementsMet(AStatusEffect.Effect) then
  begin
    if AStatusEffect.Effect.IsPositive then
      LabelDescription.Color := ColorUiStatusEffectPositive
    else
      LabelDescription.Color := ColorUiStatusEffectNegative;
  end else
    LabelDescription.Color := ColorUiEnchantmentInactive;
end;

end.


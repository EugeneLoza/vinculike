{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameBodypartEdit;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleControls, CastleUiControls, CastleKeysMouse,
  CastleMobileButton, CastleSlider,
  GameApparelSlots;

type
  TCharacterEditAbstract = class abstract(TCastleUserInterface)
  protected const
    BodypartEditHeight = Single(60);
  protected
    HorizontalGroup: TCastleHorizontalGroup;
    CaptionLabel: TCastleLabel;
    procedure SetupButton(const AButton: TCastleMobileButton);
    procedure UpdateCaptions;
  public
    ParentScreen: TObject;
    procedure SetWidth(const NewWidth: Single); virtual;
    procedure UpdateValues; virtual; abstract;
    constructor Create(AOwner: TComponent); override;
  end;

type
  TBodypartEdit = class(TCharacterEditAbstract)
  strict private
    ButtonPrevious, ButtonNext, ButtonRandom: TCastleMobileButton;
  public
    ApparelSlot: TApparelSlot;
    procedure ClickRandom(Sender: TObject);
    procedure ClickNext(Sender: TObject);
    procedure ClickPrevious(Sender: TObject);
    //procedure SetWidth(const NewWidth: Single); override;
    procedure UpdateValues; override;
    constructor Create(AOwner: TComponent); override;
  end;

type
  TBodypartColor = class(TCharacterEditAbstract)
  strict private
    ButtonRandom: TCastleMobileButton;
    SliderHue, SliderSaturation, SliderValue: TCastleSlider;
    ImageHueBackground, ImageSaturationBackground, ImageValueBackground: TCastleImageControl;
    procedure ClickRandom(Sender: TObject);
    procedure SetHue(Sender: TObject);
    procedure SetSaturation(Sender: TObject);
    procedure SetValue(Sender: TObject);
   public
    ColorTag: String;
    procedure SetWidth(const NewWidth: Single); override;
    procedure UpdateValues; override;
    constructor Create(AOwner: TComponent); override;
  end;

type
  TBodypartBoolean = class(TCharacterEditAbstract)
  strict private
    ButtonToggle: TCastleMobileButton;
    procedure ClickToggle(Sender: TObject);
  public
    Caption: String;
    Value: ^Boolean;
    procedure UpdateValues; override;
    constructor Create(AOwner: TComponent); override;
  end;

type
  TNameEdit = class(TCharacterEditAbstract)
  strict private
    NameEdit: TCastleEdit;
    ButtonRandom: TCastleMobileButton;
    procedure ClickRandom(Sender: TObject);
    procedure NameChanged(Sender: TObject);
    procedure NamePressed(const Sender: TCastleUserInterface;
      const Event: TInputPressRelease; var Handled: Boolean);
  public
    procedure UpdateValues; override;
    constructor Create(AOwner: TComponent); override;
  end;

type
  TSelectBodyPreset = class(TCharacterEditAbstract)
  strict private
    SelectedPreset: Integer; // we don't remember it between re-creations of the UI
    LabelName: TCastleLabel;
    ButtonPrevious, ButtonNext, ButtonApply, ButtonConfirm, ButtonSave: TCastleMobileButton;
    procedure ClickConfirm(Sender: TObject);
    procedure ClickApply(Sender: TObject);
    procedure ClickNext(Sender: TObject);
    procedure ClickPrevious(Sender: TObject);
    procedure ClickSave(Sender: TObject);
  public
    procedure UpdateValues; override;
    constructor Create(AOwner: TComponent); override;
  end;

implementation
uses
  CastleComponentSerialize,
  CastleColors, CastleRectangles, CastleVectors, CastleImages,
  GameFonts, GameSounds, GameColors,
  GameViewGame, GameNamesGenerator, GameInventory, GameBodyPresetsDatabase,
  GameBodyPart, GameItemsDatabase, GameItemDataAbstract, GameThemedButton,
  GameCharacterScreen, GameEnumUtils,
  GameCachedImages, GameRandom;

procedure TCharacterEditAbstract.SetupButton(const AButton: TCastleMobileButton);
begin
  AButton.SetTheme('inventory_button_light');
  AButton.CustomFont := FontBender40;
  AButton.CustomTextColorUse := true;
  AButton.CustomTextColor := ColorDefault;
  AButton.EnableParentDragging := true;
  AButton.Height := BodypartEditHeight;
end;

procedure TCharacterEditAbstract.UpdateCaptions;
begin
  (ParentScreen as TCharacterScreen).UpdateCaptions;
end;

procedure TCharacterEditAbstract.SetWidth(const NewWidth: Single);
begin
  Width := NewWidth;
end;

constructor TCharacterEditAbstract.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Height := BodypartEditHeight;

  HorizontalGroup := TCastleHorizontalGroup.Create(Self);
  HorizontalGroup.Alignment := vpMiddle;
  InsertFront(HorizontalGroup);

  CaptionLabel := TCastleLabel.Create(Self);
  CaptionLabel.CustomFont := FontBender40;
  CaptionLabel.Color := ColorDefault;
  CaptionLabel.AutoSize := false;
  CaptionLabel.Width := 400;
  CaptionLabel.Height := BodypartEditHeight;
  CaptionLabel.Alignment := hpRight;
  HorizontalGroup.InsertFront(CaptionLabel);
end;

{ TBodypartEdit ---------------------------------------------------- }

procedure TBodypartEdit.ClickRandom(Sender: TObject);
var
  PreviousBodypart: TItemDataAbstract;
  BodyPartsList: TItemsDataAbstractList;
  PreviousIndex, BodyPartIndex: SizeInt;
begin
  Sound('menu_button');
  PreviousBodypart := ViewGame.CurrentCharacter.Inventory.Apparel[ApparelSlot].Data;
  ViewGame.CurrentCharacter.Inventory.DestroyEquippedItemSafe(ApparelSlot);
  BodyPartsList := GetItemsForSlot(ApparelSlot);
  if BodyPartsList.Count = 0 then
    raise Exception.CreateFmt('No items for slot %s in %s', [specialize EnumToStr<TApparelSlot>(ApparelSlot), Self.ClassName]);
  PreviousIndex := BodyPartsList.IndexOf(PreviousBodypart);
  repeat
    BodyPartIndex := Rnd.Random(BodyPartsList.Count);
  until (BodyPartsList.Count = 1) or (PreviousIndex <> BodyPartIndex);
  ViewGame.CurrentCharacter.Inventory.EquipApparel(TBodyPart.NewBodyPart(BodyPartsList[BodyPartIndex]));
  FreeAndNil(BodyPartsList);
  UpdateCaptions;
end;

procedure TBodypartEdit.ClickNext(Sender: TObject);
var
  PreviousBodypart: TItemDataAbstract;
  BodyPartsList: TItemsDataAbstractList;
  BodyPartIndex: SizeInt;
begin
  Sound('menu_button');
  PreviousBodypart := ViewGame.CurrentCharacter.Inventory.Apparel[ApparelSlot].Data;
  ViewGame.CurrentCharacter.Inventory.DestroyEquippedItemSafe(ApparelSlot);
  BodyPartsList := GetItemsForSlot(ApparelSlot);
  if BodyPartsList.Count = 0 then
    raise Exception.CreateFmt('No items for slot %s in %s', [specialize EnumToStr<TApparelSlot>(ApparelSlot), Self.ClassName]);
  BodyPartIndex := BodyPartsList.IndexOf(PreviousBodypart);
  Inc(BodyPartIndex);
  if BodyPartIndex >= BodyPartsList.Count then
    BodyPartIndex := 0;
  ViewGame.CurrentCharacter.Inventory.EquipApparel(TBodyPart.NewBodyPart(BodyPartsList[BodyPartIndex]));
  FreeAndNil(BodyPartsList);
  UpdateCaptions;
end;

procedure TBodypartEdit.ClickPrevious(Sender: TObject);
var
  PreviousBodypart: TItemDataAbstract;
  BodyPartsList: TItemsDataAbstractList;
  BodyPartIndex: SizeInt;
begin
  Sound('menu_button');
  PreviousBodypart := ViewGame.CurrentCharacter.Inventory.Apparel[ApparelSlot].Data;
  ViewGame.CurrentCharacter.Inventory.DestroyEquippedItemSafe(ApparelSlot);
  BodyPartsList := GetItemsForSlot(ApparelSlot);
  if BodyPartsList.Count = 0 then
    raise Exception.CreateFmt('No items for slot %s in %s', [specialize EnumToStr<TApparelSlot>(ApparelSlot), Self.ClassName]);
  BodyPartIndex := BodyPartsList.IndexOf(PreviousBodypart);
  Dec(BodyPartIndex);
  if BodyPartIndex < 0 then
    BodyPartIndex := Pred(BodyPartsList.Count);
  ViewGame.CurrentCharacter.Inventory.EquipApparel(TBodyPart.NewBodyPart(BodyPartsList[BodyPartIndex]));
  FreeAndNil(BodyPartsList);
  UpdateCaptions;
end;

procedure TBodypartEdit.UpdateValues;
begin
  CaptionLabel.Caption := BodySlotToHumanReadableString(ApparelSlot) + ' : ';
  ButtonRandom.Caption := ViewGame.CurrentCharacter.Inventory.Apparel[ApparelSlot].Data.DisplayName;
end;

constructor TBodypartEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  ButtonPrevious := TCastleMobileButton.Create(Self);
  SetupButton(ButtonPrevious);
  ButtonPrevious.OnClick := @ClickPrevious;
  ButtonPrevious.Width := BodypartEditHeight;
  ButtonPrevious.Caption := '<<';
  HorizontalGroup.InsertFront(ButtonPrevious);

  ButtonRandom := TCastleMobileButton.Create(Self);
  SetupButton(ButtonRandom);
  ButtonRandom.OnClick := @ClickRandom;
  ButtonRandom.Width := 400;
  HorizontalGroup.InsertFront(ButtonRandom);

  ButtonNext := TCastleMobileButton.Create(Self);
  SetupButton(ButtonNext);
  ButtonNext.OnClick := @ClickNext;
  ButtonNext.Width := BodypartEditHeight;
  ButtonNext.Caption := '>>';
  HorizontalGroup.InsertFront(ButtonNext);
end;

{ TBodypartColor ---------------------------------------------------- }

procedure TBodypartColor.ClickRandom(Sender: TObject);
var
  OldColor: String;
begin
  Sound('menu_button');
  OldColor := ColorToHex(ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag]);
  repeat
    ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag] := ViewGame.CurrentCharacter.Blueprint.ColorTags[ColorTag][Rnd.Random(ViewGame.CurrentCharacter.Blueprint.ColorTags[ColorTag].Count)];
  until (ViewGame.CurrentCharacter.Blueprint.ColorTags[ColorTag].Count = 1) or (OldColor <> ColorToHex(ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag]));
  ViewGame.InvalidateInventory(nil);
  UpdateCaptions;
end;

procedure TBodypartColor.SetHue(Sender: TObject);
var
  Hsv: TVector3;
begin
  Hsv := RgbToHsv(ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag].XYZ);
  Hsv.X := SliderHue.Value * 6.0; // Hue is in 0..6
  if Hsv.X = 6.0 then
    Hsv.X -= SingleEpsilon;
  ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag] := Vector4(HsvToRgb(Hsv), 1.0);
  ViewGame.InvalidateInventory(nil);
  UpdateCaptions;
end;

procedure TBodypartColor.SetSaturation(Sender: TObject);
var
  Hsv: TVector3;
begin
  Hsv := RgbToHsv(ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag].XYZ);
  Hsv.Y := SliderSaturation.Value;
  ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag] := Vector4(HsvToRgb(Hsv), 1.0);
  ViewGame.InvalidateInventory(nil);
  UpdateCaptions;
end;

procedure TBodypartColor.SetValue(Sender: TObject);
var
  Hsv: TVector3;
begin
  Hsv := RgbToHsv(ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag].XYZ);
  Hsv.Z := SliderValue.Value;
  ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag] := Vector4(HsvToRgb(Hsv), 1.0);
  ViewGame.InvalidateInventory(nil);
  UpdateCaptions;
end;

procedure TBodypartColor.SetWidth(const NewWidth: Single);

  function WhiteBox(const ParentControl: TCastleUserInterface): TRgbImage;
  begin
    Result := TRgbImage.Create(Round(ParentControl.Width) - 8, Round(ParentControl.Height) - 8);
    Result.Clear(Vector4Byte(255, 255, 255, 255));
  end;

begin
  inherited SetWidth(NewWidth);
  ImageHueBackground.Width := (Width - ButtonRandom.Width - CaptionLabel.Width - 27.0 - 3.0 * 10.0) / 3.0;
  SliderHue.Width := ImageHueBackground.Width;
  SliderHue.HandleImage.Image.Image := WhiteBox(SliderHue.HandleImage);
  ImageSaturationBackground.Width := ImageHueBackground.Width;
  SliderSaturation.Width := ImageSaturationBackground.Width;
  SliderSaturation.HandleImage.Image.Image := WhiteBox(SliderSaturation.HandleImage);
  ImageValueBackground.Width := ImageHueBackground.Width;
  SliderValue.Width := ImageValueBackground.Width;
  SliderValue.HandleImage.Image.Image := WhiteBox(SliderValue.HandleImage);
end;

procedure TBodypartColor.UpdateValues;

  function HueImage: TRgbImage;
  var
    I: Integer;
    P: PVector3Byte;
    Scale: Single;
    Hsv, Rgb: TVector3;
  begin
    Result := TRgbImage.Create(Trunc(ImageHueBackground.Width), 1);
    P := Result.RawPixels;
    Scale := 6.0 / Single(Result.Width - 1);
    Hsv := Vector3(0, SliderSaturation.Value, SliderValue.Value);
    for I := 0 to Result.Width - 1 do
    begin
      Hsv.X := I * Scale;
      Rgb := HsvToRgb(Hsv);
      P^ := Vector3Byte(Round(255 * Rgb.x), Round(255 * Rgb.y), Round(255 * Rgb.z));
      Inc(P);
    end;
  end;

  function SaturationImage: TRgbImage;
  var
    I: Integer;
    P: PVector3Byte;
    Scale: Single;
    Hsv, Rgb: TVector3;
  begin
    Result := TRgbImage.Create(Trunc(ImageSaturationBackground.Width), 1);
    P := Result.RawPixels;
    Scale := 1.0 / Single(Result.Width - 1);
    Hsv := Vector3(SliderHue.Value * 6.0, 0, SliderValue.Value);
    for I := 0 to Result.Width - 1 do
    begin
      Hsv.Y := I * Scale;
      Rgb := HsvToRgb(Hsv);
      P^ := Vector3Byte(Round(255 * Rgb.x), Round(255 * Rgb.y), Round(255 * Rgb.z));
      Inc(P);
    end;
  end;

  function ValueImage: TRgbImage;
  var
    I: Integer;
    P: PVector3Byte;
    Scale: Single;
    Hsv, Rgb: TVector3;
  begin
    Result := TRgbImage.Create(Trunc(ImageValueBackground.Width), 1);
    P := Result.RawPixels;
    Scale := 1.0 / Single(Result.Width - 1);
    Hsv := Vector3(SliderHue.Value * 6.0, SliderSaturation.Value, 0);
    for I := 0 to Result.Width - 1 do
    begin
      Hsv.Z := I * Scale;
      Rgb := HsvToRgb(Hsv);
      P^ := Vector3Byte(Round(255 * Rgb.x), Round(255 * Rgb.y), Round(255 * Rgb.z));
      Inc(P);
    end;
  end;

begin
  CaptionLabel.Caption := ColorTag + ' : ';
  ButtonRandom.Caption := ColorToHex(ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag]);
  SliderHue.Value := RgbToHsv(ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag].Xyz).X / 6.0; // Hue is 0..6
  SliderHue.HandleImage.Image.Color := ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag];
  ImageHueBackground.Image := HueImage;
  SliderSaturation.Value := RgbToHsv(ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag].Xyz).Y;
  SliderSaturation.HandleImage.Image.Color := ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag];
  ImageSaturationBackground.Image := SaturationImage;
  SliderValue.Value := RgbToHsv(ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag].Xyz).Z;
  SliderValue.HandleImage.Image.Color := ViewGame.CurrentCharacter.Inventory.BodyColor[ColorTag];
  ImageValueBackground.Image := ValueImage;
end;

constructor TBodypartColor.Create(AOwner: TComponent);

  procedure MakeSlider(var ImageBackground: TCastleImageControl; var Slider: TCastleSlider);
  var
    Filler: TCastleUserInterface;
  begin
    Filler := TCastleUserInterface.Create(HorizontalGroup);
    Filler.Width := 10;
    Filler.Height := 10;
    HorizontalGroup.InsertFront(Filler);

    ImageBackground := TCastleImageControl.Create(Self);
    ImageBackground.Stretch := true;
    ImageBackground.Height := BodypartEditHeight / 3.0;
    HorizontalGroup.InsertFront(ImageBackground);

    Slider := TCastleSlider.Create(ImageBackground);
    Slider.HandleImage.CustomBackgroundNormal.Image := LoadRgba('castle-data:/ui/kodiakgraphics/scrollbar_slider.png');
    Slider.HandleImage.CustomBackgroundNormal.OwnsImage := false;
    Slider.HandleImage.CustomBackgroundNormal.ProtectedSides.Top := 4;
    Slider.HandleImage.CustomBackgroundNormal.ProtectedSides.Bottom := 4;
    Slider.HandleImage.CustomBackgroundNormal.ProtectedSides.Left := 4;
    Slider.HandleImage.CustomBackgroundNormal.ProtectedSides.Right := 4;
    Slider.HandleImage.Height := ImageBackground.Height * 1.1;
    Slider.HandleImage.Width := Slider.HandleImage.Height;
    Slider.HandleImage.CustomBackground := true;
    Slider.HandleImage.AutoSize := false;
    Slider.Height := ImageBackground.Height * 1.1;
    Slider.Vertical := false;
    ImageBackground.InsertFront(Slider);
  end;

begin
  inherited Create(AOwner);
  ButtonRandom := TCastleMobileButton.Create(Self);
  SetupButton(ButtonRandom);
  ButtonRandom.OnClick := @ClickRandom;
  ButtonRandom.Width := 200;
  HorizontalGroup.InsertFront(ButtonRandom);

  MakeSlider(ImageHueBackground, SliderHue);
  SliderHue.OnChanged := @SetHue;
  MakeSlider(ImageSaturationBackground, SliderSaturation);
  SliderSaturation.OnChanged := @SetSaturation;
  MakeSlider(ImageValueBackground, SliderValue);
  SliderValue.OnChanged := @SetValue;
end;

{ TBodypartBoolean ---------------------------------------------------- }

procedure TBodypartBoolean.ClickToggle(Sender: TObject);
begin
  Sound('menu_button');
  Value^ := not Value^;
  ViewGame.InvalidateInventory(nil);
  UpdateCaptions;
end;

procedure TBodypartBoolean.UpdateValues;

  function OnOff(const AValue: Boolean): String; // todo make StringUtils or similar
  begin
    if AValue then
      Exit('ON')
    else
      Exit('OFF');
  end;

begin
  CaptionLabel.Caption := Caption;
  ButtonToggle.Caption := OnOff(Value^);
end;

constructor TBodypartBoolean.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  ButtonToggle := TCastleMobileButton.Create(Self);
  SetupButton(ButtonToggle);
  ButtonToggle.OnClick := @ClickToggle;
  ButtonToggle.Width := 100;
  HorizontalGroup.InsertFront(ButtonToggle);

  CaptionLabel.Width := 500;
end;

{ TNameEdit ---------------------------------------------------- }

procedure TNameEdit.ClickRandom(Sender: TObject);
begin
  ViewGame.CurrentCharacter.PlayerCharacterData.DisplayName := GenerateFemaleName;
  ViewGame.InvalidateInventory(nil);
  UpdateCaptions;
end;

procedure TNameEdit.NameChanged(Sender: TObject);
begin
  if NameEdit.Text <> '' then
    ViewGame.CurrentCharacter.PlayerCharacterData.DisplayName := NameEdit.Text;
  if PlayerNameNotExists(NameEdit.Text, ViewGame.CurrentCharacter) then
  begin
    NameEdit.FocusedColor := White;
    NameEdit.UnfocusedColor := ColorDefault;
  end else
  begin
    NameEdit.FocusedColor := ColorError;
    NameEdit.UnfocusedColor := ColorError;
  end;
  ViewGame.InvalidateInventory(nil);
end;

procedure TNameEdit.NamePressed(const Sender: TCastleUserInterface;
  const Event: TInputPressRelease; var Handled: Boolean);
begin
  if Container <> nil then
  begin
    if Event.IsKey(keyEnter) then
      Container.ForceCaptureInput := nil
    else
      Container.ForceCaptureInput := NameEdit; // Workaround problem that edit works only when mouse hovers over it
  end;
end;

procedure TNameEdit.UpdateValues;
begin
  NameEdit.Text := ViewGame.CurrentCharacter.PlayerCharacterData.DisplayName;
end;

constructor TNameEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  NameEdit := TCastleEdit.Create(Self);
  NameEdit.Width := 300;
  NameEdit.AutoSizeHeight := false;
  NameEdit.Height := BodypartEditHeight - 8;
  NameEdit.OnChange := @NameChanged;
  NameEdit.OnPress := @NamePressed;
  NameEdit.BackgroundColor := HexToColor('131313');
  NameEdit.FocusedColor := ColorDefault; // White;
  NameEdit.UnfocusedColor := ColorDefault;
  NameEdit.MaxLength := MaxNameSymbols;
  NameEdit.AutoOnScreenKeyboard := true; // Show keyboard on Android
  NameEdit.CustomFont := FontBender40;
  HorizontalGroup.InsertFront(NameEdit);

  ButtonRandom := TCastleMobileButton.Create(Self);
  SetupButton(ButtonRandom);
  ButtonRandom.OnClick := @ClickRandom;
  ButtonRandom.Width := 200;
  ButtonRandom.Caption := 'Random';
  HorizontalGroup.InsertFront(ButtonRandom);

  CaptionLabel.Caption := 'Name : ';
end;

{ TSelectBodyPreset ------------------------------------ }

procedure TSelectBodyPreset.ClickPrevious(Sender: TObject);
begin
  Dec(SelectedPreset);
  if SelectedPreset < 0 then
    SelectedPreset := Pred(BodyPresets.Count); // always at least one preset exists
  UpdateCaptions;
end;

procedure TSelectBodyPreset.ClickSave(Sender: TObject);
var
  Preset: TBodyPreset;
begin
  Preset := ViewGame.CurrentCharacter.Inventory.CloneBody;
  BodyPresets.Add(Preset);
  SaveLocalPresets;
  UpdateValues;
end;

procedure TSelectBodyPreset.ClickNext(Sender: TObject);
begin
  Inc(SelectedPreset);
  if SelectedPreset >= BodyPresets.Count then
    SelectedPreset := 0;
  UpdateCaptions;
end;

procedure TSelectBodyPreset.ClickConfirm(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Inventory.AssignBody(BodyPresets[SelectedPreset]);
  UpdateCaptions;
end;

procedure TSelectBodyPreset.ClickApply(Sender: TObject);
begin
  ButtonConfirm.Exists := true;
  ButtonApply.Exists := false;
end;

procedure TSelectBodyPreset.UpdateValues;
begin
  LabelName.Caption := BodyPresets[SelectedPreset].DisplayName;
  ButtonSave.Caption := 'Save ' + ViewGame.CurrentCharacter.Data.DisplayName;
  ButtonConfirm.Exists := false;
  ButtonApply.Exists := true;
  ButtonPrevious.Enabled := BodyPresets.Count > 1;
  ButtonNext.Enabled := BodyPresets.Count > 1;
end;

constructor TSelectBodyPreset.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  // we guarantee that the first item of BodyPresets is Random body
  SelectedPreset := 0;

  ButtonPrevious := TCastleMobileButton.Create(Self);
  SetupButton(ButtonPrevious);
  ButtonPrevious.OnClick := @ClickPrevious;
  ButtonPrevious.Width := BodypartEditHeight;
  ButtonPrevious.Caption := '<<';
  HorizontalGroup.InsertFront(ButtonPrevious);

  LabelName := TCastleLabel.Create(Self);
  LabelName.CustomFont := FontBender40;
  LabelName.Color := ColorDefault;
  LabelName.AutoSize := false;
  LabelName.Width := 300;
  LabelName.Height := BodypartEditHeight;
  LabelName.Alignment := hpMiddle;
  HorizontalGroup.InsertFront(LabelName);

  ButtonNext := TCastleMobileButton.Create(Self);
  SetupButton(ButtonNext);
  ButtonNext.OnClick := @ClickNext;
  ButtonNext.Width := BodypartEditHeight;
  ButtonNext.Caption := '>>';
  HorizontalGroup.InsertFront(ButtonNext);

  ButtonApply := TCastleMobileButton.Create(Self);
  SetupButton(ButtonApply);
  ButtonApply.OnClick := @ClickApply;
  ButtonApply.Width := 170;
  ButtonApply.Caption := 'Apply';
  HorizontalGroup.InsertFront(ButtonApply);

  ButtonConfirm := TCastleMobileButton.Create(Self);
  SetupButton(ButtonConfirm);
  ButtonConfirm.OnClick := @ClickConfirm;
  ButtonConfirm.Width := 170;
  ButtonConfirm.Caption := 'Confirm';
  HorizontalGroup.InsertFront(ButtonConfirm);

  ButtonSave := TCastleMobileButton.Create(Self);
  SetupButton(ButtonSave);
  ButtonSave.OnClick := @ClickSave;
  ButtonSave.Width := 370;
  HorizontalGroup.InsertFront(ButtonSave);

  CaptionLabel.Caption := 'Body Preset : ';
end;

end.


{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameMissionUi;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleControls, CastleUiControls;

type
  TMissionUi = class(TComponent) // CastleClassUtils -> TCastleComponent
  strict private
    ParentDesign: TCastleDesign;
    procedure ClickCharacter(Sender: TObject);
    procedure ClickGoToDungeonLevel(Sender: TObject);
    procedure FillCharacters;
    procedure FillDestinations;
  public
    function Exists: Boolean;
  public
    procedure Show;
    procedure Hide;
    procedure SetWidth(const NewWidth: Single);
    procedure Parse(const AParentDesign: TCastleDesign);
  end;

implementation
uses
  CastleComponentSerialize,
  CastleMobileButton,
  GameFonts, GamePlayerCharacter, GameMap, GameColors, GameMath,
  GameViewGame, GameUiUtils, GameSounds, GameSerializableObject,
  GameThemedButton;

procedure TMissionUi.Parse(const AParentDesign: TCastleDesign);
begin
  ParentDesign := AParentDesign;
  ParentDesign.Exists := false;

  //(ParentDesign.DesignedComponent('ButtonPauseMenu') as TCastleBigButton).OnClick := @ClickPauseMenu;
  //(ParentDesign.DesignedComponent('ButtonPauseMenu') as TCastleBigButton).CustomFont := FontSoniano90;

  (ParentDesign.DesignedComponent('LabelTitle') as TCastleLabel).CustomFont := FontSoniano90;

  Hide;
end;

procedure TMissionUi.FillCharacters;
var
  VerticalGroupCharacters: TCastleVerticalGroup;
  CharGroup: TCastleHorizontalGroup;
  PlayerCharacter: TPlayerCharacter;
  Button: TCastleMobileButton;
  CharLabel: TCastleLabel;
begin
  VerticalGroupCharacters := ParentDesign.DesignedComponent('VerticalGroupCharacters') as TCastleVerticalGroup;
  VerticalGroupCharacters.ClearAndFreeControls;
  for PlayerCharacter in Map.PlayerCharactersList do
    if not PlayerCharacter.IsCaptured then
    begin
      CharGroup := TCastleHorizontalGroup.Create(VerticalGroupCharacters);
      CharGroup.Spacing := 20;
      VerticalGroupCharacters.InsertFront(CharGroup);

      Button := TCastleMobileButton.Create(VerticalGroupCharacters);
      if PlayerCharacter = ViewGame.CurrentCharacter then
        Button.SetTheme('inventory_button_light_selected')
      else
        Button.SetTheme('inventory_button_light');
      Button.Width := 450;
      Button.Height := 70;
      Button.Html := true;
      Button.CustomTextColorUse := true;
      if PlayerCharacter.IsAvailableForDungeon then
      begin
        Button.CustomTextColor := ColorUiCharacterReady;
        Button.Enabled := true;
      end else
      begin
        Button.CustomTextColor := ColorUiCharacterAway;
        Button.Enabled := false;
      end;
      Button.EnableParentDragging := true;
      Button.Caption := Format('%s (lvl.%d)', [PlayerCharacter.Data.DisplayName, PlayerCharacter.Experience.Level]);
      Button.Tag := PlayerCharacter.ReferenceId;
      Button.OnClick := @ClickCharacter;

      CharGroup.InsertFront(Button);

      CharLabel := TCastleLabel.Create(CharGroup);
      CharLabel.Color := ColorWhite;
      if PlayerCharacter = ViewGame.CurrentCharacter then
        CharLabel.Caption := 'Ready for action!'
      else
        CharLabel.Caption := 'Crafting';

      CharGroup.InsertFront(CharLabel);
    end;
end;

procedure TMissionUi.ClickGoToDungeonLevel(Sender: TObject);
begin
  Hide;
  Map.CurrentDepth := (Sender as TCastleMobileButton).Tag;
  if Map.CurrentDepth = 0 then
    Sound('going_down')
  else
    Sound('teleport_to_location');
  ViewGame.NewMap(true);
end;

procedure TMissionUi.ClickCharacter(Sender: TObject);
begin
  Sound('menu_button');
  ViewGame.CurrentCharacter := ObjectByReferenceId((Sender as TCastleButton).Tag) as TPlayerCharacter;
  ViewGame.InvalidateInventory(ViewGame.CurrentCharacter);
  ViewGame.InvalidatePosition(ViewGame.CurrentCharacter);
  FillCharacters;
end;

procedure TMissionUi.FillDestinations;
var
  VerticalGroupDestinations: TCastleVerticalGroup;
  Button: TCastleMobileButton;
  CharLabel: TCastleLabel;
  I: Integer;
  P: TPlayerCharacter;
begin
  VerticalGroupDestinations := ParentDesign.DesignedComponent('VerticalGroupDestinations') as TCastleVerticalGroup;
  VerticalGroupDestinations.ClearAndFreeControls;
  for I := 1 to MaxInteger(1, Map.DeepestLevel) do
  begin
    if I in Map.UnlockedDestinations then
    begin
      Button := TCastleMobileButton.Create(VerticalGroupDestinations);
      Button.SetTheme('inventory_button_light');
      Button.EnableParentDragging := true;
      Button.CustomTextColorUse := true;
      Button.CustomTextColor := ColorDefault;
      Button.Caption := 'Dungeon lvl.' + I.ToString;
      Button.Width := 300;
      Button.Height := 70;
      Button.Tag := I - 1;
      Button.OnClick := @ClickGoToDungeonLevel;
      VerticalGroupDestinations.InsertFront(Button);
    end;
    for P in Map.PlayerCharactersList do
      if (not P.IsStranger) and (P.AtMap = I) then
      begin
        CharLabel := TCastleLabel.Create(VerticalGroupDestinations);
        CharLabel.Caption := I.ToString + ' : ' + P.Data.DisplayName;
        CharLabel.Color := ColorUiCharacterCaptured;
        VerticalGroupDestinations.InsertFront(CharLabel);
      end;
  end;
end;

function TMissionUi.Exists: Boolean;
begin
  Exit(ParentDesign.Exists);
end;

procedure TMissionUi.Show;
begin
  FillCharacters;
  FillDestinations;
  ParentDesign.Exists := true;
end;

procedure TMissionUi.Hide;
begin
  ParentDesign.Exists := false;
end;

procedure TMissionUi.SetWidth(const NewWidth: Single);
begin
  (ParentDesign.DesignedComponent('Content') as TCastleUserInterface).Width := NewWidth;
end;

end.


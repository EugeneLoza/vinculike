{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameCharacterScreen;
{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleControls, CastleUiControls,
  GameApparelSlots;

type
  TCharacterScreenPage = (cpBody, cpEnchantments);

type
  TCharacterScreen = class(TComponent) // CastleClassUtils -> TCastleComponent
  strict private
    Content: TCastleUserInterface;
    ParentDesign: TCastleDesign;
    ScrollView: TCastleScrollView;
    VerticalGroup: TCastleVerticalGroup;
    CurrentPage: TCharacterScreenPage;
    procedure ClickBody(Sender: TObject);
    procedure ClickEnchantments(Sender: TObject);
    procedure ClickReturn(Sender: TObject);
    procedure InjectBodyPage;
    procedure InjectEnchantmentsPage;
  public
    function Exists: Boolean;
  public
    procedure SetWidth(const NewWidth: Single);
    procedure Show;
    procedure Hide;
    procedure Parse(const AParentDesign: TCastleDesign);
    procedure UpdateCaptions;
  end;

implementation
uses
  CastleComponentSerialize,
  CastleColors,
  GameFonts, GameSounds, GameColors,
  GameViewGame, GameUiUtils,
  GameItemsDatabase,
  GameBodypartEdit, GameEnchantmentInfo;

{----------------------------------------------------------------------------------------}

procedure TCharacterScreen.Parse(const AParentDesign: TCastleDesign);
begin
  ParentDesign := AParentDesign;
  ParentDesign.Exists := false;

  Content := (ParentDesign.DesignedComponent('Content') as TCastleUserInterface);

  VerticalGroup := ParentDesign.DesignedComponent('VerticalGroup') as TCastleVerticalGroup;
  ScrollView := ParentDesign.DesignedComponent('ScrollView') as TCastleScrollView;

  (ParentDesign.DesignedComponent('ButtonReturn') as TCastleButton).OnClick := @ClickReturn;
  (ParentDesign.DesignedComponent('ButtonReturn') as TCastleButton).CustomFont := FontSoniano90;
  (ParentDesign.DesignedComponent('ButtonBody') as TCastleButton).OnClick := @ClickBody;
  (ParentDesign.DesignedComponent('ButtonBody') as TCastleButton).CustomFont := FontSoniano90;
  (ParentDesign.DesignedComponent('ButtonEnchantments') as TCastleButton).OnClick := @ClickEnchantments;
  (ParentDesign.DesignedComponent('ButtonEnchantments') as TCastleButton).CustomFont := FontSoniano90;

  Hide;
  CurrentPage := cpBody;
end;

procedure TCharacterScreen.UpdateCaptions;
var
  I: Integer;
begin
  for I := 0 to Pred(VerticalGroup.ControlsCount) do
    if VerticalGroup.Controls[I] is TCharacterEditAbstract then // otherwise exception?
      TCharacterEditAbstract(VerticalGroup.Controls[I]).UpdateValues;
end;

procedure TCharacterScreen.ClickReturn(Sender: TObject);
begin
  Sound('character_screen_hide');
  Hide;
end;

procedure TCharacterScreen.ClickBody(Sender: TObject);
begin
  if CurrentPage <> cpBody then
  begin
    Sound('menu_button');
    InjectBodyPage;
  end;
end;

procedure TCharacterScreen.ClickEnchantments(Sender: TObject);
begin
  if CurrentPage <> cpEnchantments then
  begin
    Sound('menu_button');
    InjectEnchantmentsPage;
  end;
end;

procedure TCharacterScreen.InjectBodyPage;
var
  NameEdit: TNameEdit;
  SelectBodyPreset: TSelectBodyPreset;
  BodypartEdit: TBodypartEdit;
  BodypartColor: TBodypartColor;
  BodypartBoolean: TBodypartBoolean;
  A: TApparelSlot;
  C: String;
  I: Integer;
begin
  CurrentPage := cpBody;
  if ParentDesign.Container <> nil then
    ParentDesign.Container.ForceCaptureInput := nil; // if we were forced-focused on name edit - unfocus

  ViewGame.PaperDoll.Nude := true;
  ViewGame.InvalidateInventory(nil);

  VerticalGroup.ClearAndFreeControls;

  NameEdit := TNameEdit.Create(VerticalGroup);
  NameEdit.ParentScreen := Self;
  VerticalGroup.InsertFront(NameEdit);

  SelectBodyPreset := TSelectBodyPreset.Create(VerticalGroup);
  SelectBodyPreset.ParentScreen := Self;
  VerticalGroup.InsertFront(SelectBodyPreset);

  for A in ViewGame.CurrentCharacter.Blueprint.BodySlots do
  begin
    BodypartEdit := TBodypartEdit.Create(VerticalGroup);
    BodypartEdit.ApparelSlot := A;
    BodypartEdit.ParentScreen := Self;
    VerticalGroup.InsertFront(BodypartEdit);
  end;

  for C in ViewGame.CurrentCharacter.Blueprint.ColorTags.Keys do
  begin
    BodypartColor := TBodypartColor.Create(VerticalGroup);
    BodypartColor.ColorTag := C;
    BodypartColor.ParentScreen := Self;
    VerticalGroup.InsertFront(BodypartColor);
  end;

  BodypartBoolean := TBodypartBoolean.Create(VerticalGroup);
  BodypartBoolean.Caption := 'Censored : ';
  BodypartBoolean.Value := @ViewGame.CurrentCharacter.Inventory.Censored;
  BodypartBoolean.ParentScreen := Self;
  VerticalGroup.InsertFront(BodypartBoolean);

  BodypartBoolean := TBodypartBoolean.Create(VerticalGroup);
  BodypartBoolean.Caption := 'Censored in screenshots : ';
  BodypartBoolean.Value := @ViewGame.CurrentCharacter.Inventory.CensoredInScreenshots;
  BodypartBoolean.ParentScreen := Self;
  VerticalGroup.InsertFront(BodypartBoolean);

  for I := 0 to Pred(VerticalGroup.ControlsCount) do
    if VerticalGroup.Controls[I] is TCharacterEditAbstract then // otherwise exception?
      TCharacterEditAbstract(VerticalGroup.Controls[I]).SetWidth(Content.Width);

  UpdateCaptions;
end;

procedure TCharacterScreen.InjectEnchantmentsPage;
var
  E: TApparelSlot;
  I: Integer;
  L: TCastleLabel;
  StatusEffectInfo: TStatusEffectInfo;
  EnchantmentInfo: TEnchantmentInfo;
  Count: Integer;
begin
  CurrentPage := cpEnchantments;
  if ParentDesign.Container <> nil then
    ParentDesign.Container.ForceCaptureInput := nil; // if we were forced-focused on name edit - unfocus

  ViewGame.PaperDoll.Nude := false;
  ViewGame.InvalidateInventory(nil);

  VerticalGroup.ClearAndFreeControls;

  Count := 0;
  // temporary? Try summarize same enchantments?
  for I := 0 to Pred(ViewGame.CurrentCharacter.Inventory.StatusEffects.Count) do
  begin
    StatusEffectInfo := TStatusEffectInfo.Create(VerticalGroup);
    StatusEffectInfo.FillIn(ViewGame.CurrentCharacter.Inventory.StatusEffects[I]);
    StatusEffectInfo.SetWidth(Content.Width - ScrollView.EffectiveScrollBarWidth - 10);
    VerticalGroup.InsertFront(StatusEffectInfo);
    Inc(Count);
  end;

  for E in ViewGame.CurrentCharacter.Blueprint.EquipmentSlots do
    if (ViewGame.CurrentCharacter.Inventory.Apparel[E] <> nil) and (ViewGame.CurrentCharacter.Inventory.Apparel[E].Data.MainSlot = E) then
      for I := 0 to Pred(ViewGame.CurrentCharacter.Inventory.Equipped[E].Enchantments.Count) do
      begin
        EnchantmentInfo := TEnchantmentInfo.Create(VerticalGroup);
        EnchantmentInfo.FillIn(ViewGame.CurrentCharacter.Inventory.Equipped[E].Enchantments[I]);
        EnchantmentInfo.SetWidth(Content.Width - ScrollView.EffectiveScrollBarWidth - 10);
        VerticalGroup.InsertFront(EnchantmentInfo);
        Inc(Count);
      end;

  if Count = 0 then
  begin
    L := TCastleLabel.Create(VerticalGroup);
    L.CustomFont := FontBender40;
    L.Caption := 'No Active Effects';
    L.Color := ColorWhite;
    VerticalGroup.InsertFront(L);
  end;
end;

function TCharacterScreen.Exists: Boolean;
begin
  Exit(ParentDesign.Exists);
end;

{$PUSH}
{$WARN 6018 off : Unreachable code}
procedure TCharacterScreen.SetWidth(const NewWidth: Single);
var
  I: Integer;
begin
  Content.Width := NewWidth;
  //(ParentDesign.DesignedComponent('ScrollViewBodyFeatures') as TCastleUserInterface).Width := NewWidth;
  // TODO:
  case CurrentPage of
    cpBody:
      begin
        for I := 0 to Pred(VerticalGroup.ControlsCount) do
          if VerticalGroup.Controls[I] is TCharacterEditAbstract then // otherwise exception?
            TCharacterEditAbstract(VerticalGroup.Controls[I]).SetWidth(NewWidth - ScrollView.EffectiveScrollBarWidth - 10);
      end;
    cpEnchantments:
      begin
        for I := 0 to Pred(VerticalGroup.ControlsCount) do
          if VerticalGroup.Controls[I] is TEnchantmentInfoAbstract then // otherwise exception?
            TEnchantmentInfoAbstract(VerticalGroup.Controls[I]).SetWidth(NewWidth - ScrollView.EffectiveScrollBarWidth - 10);
      end;
    else
      raise Exception.Create('Unexpected page in TCharacterScreen.SetWidth');
  end;
end;

procedure TCharacterScreen.Show;
begin
  case CurrentPage of
    cpBody: InjectBodyPage;
    cpEnchantments: InjectEnchantmentsPage;
    else
      raise Exception.Create('Unexpected page in TCharacterScreen.Show');
  end;
  ParentDesign.Exists := true;
end;
{$POP}

procedure TCharacterScreen.Hide;
begin
  if ParentDesign.Container <> nil then
    ParentDesign.Container.ForceCaptureInput := nil; // if we were forced-focused on name edit - unfocus
  VerticalGroup.ClearAndFreeControls;
  ParentDesign.Exists := false;
  // note: paper doll is created before character screen
  ViewGame.PaperDoll.Nude := false;
  ViewGame.InvalidateInventory(nil);
end;

end.


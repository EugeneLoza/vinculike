{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GamePopupItem;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleControls, CastleUiControls,
  GameMapItem, GameInventoryItem;

type
  TPopupItem = class(TComponent) // CastleClassUtils -> TCastleComponent
  strict private
    ButtonVinculopedia, ButtonRepair, ButtonEquip, ButtonUnequip, ButtonIdentify: TCastleButton;
    LabelRepair, LabelEquip, LabelUnequip, LabelIdentify: TCastleLabel;
    LabelHeader: TCastleLabel;
    ImageItemIcon: TCastleImageControl;
    GroupFooterEquipped, GroupFooterGround: TCastleUserInterface;
    ScrollViewItemFeatures: TCastleScrollView;
    Content: TCastleUserInterface;
    ParentDesign: TCastleDesign;
    VerticalGroupItemFeatures: TCastleVerticalGroup;
    procedure ClickEquip(Sender: TObject);
    procedure ClickIdentify(Sender: TObject);
    procedure ClickRepair(Sender: TObject);
    procedure ClickReturn(Sender: TObject);
    procedure ClickUnequip(Sender: TObject);
    procedure ClickVinculopedia(Sender: TObject);
    procedure FillInValues;
  public
    MapItem: TMapItem;
    InventoryItem: TInventoryItem;
  public
    function Exists: Boolean;
    procedure SetWidth(const NewWidth: Single);
    procedure Show;
    procedure Hide;
    procedure Parse(const AParentDesign: TCastleDesign);
  end;

implementation
uses
  CastleComponentSerialize,
  GameFonts, GameSounds, GameColors, GameApparelSlots, GameStats, GameLeash, GameActor,
  GameViewGame, GameViewVinculopedia, GameUiUtils, GameInventory, GameEnchantmentInfo;

{----------------------------------------------------------------------------------------}

procedure TPopupItem.Parse(const AParentDesign: TCastleDesign);
begin
  ParentDesign := AParentDesign;
  ParentDesign.Exists := false;

  Content := (ParentDesign.DesignedComponent('Content') as TCastleUserInterface);

  VerticalGroupItemFeatures := ParentDesign.DesignedComponent('VerticalGroupItemFeatures') as TCastleVerticalGroup;

  (ParentDesign.DesignedComponent('ButtonClose') as TCastleButton).OnClick := @ClickReturn;
  (ParentDesign.DesignedComponent('ButtonClose') as TCastleButton).CustomFont := FontSoniano50;

  ButtonVinculopedia := ParentDesign.DesignedComponent('ButtonVinculopedia') as TCastleButton;
  ButtonVinculopedia.OnClick := @ClickVinculopedia;
  ButtonVinculopedia.CustomFont := FontSoniano50;

  ButtonRepair := ParentDesign.DesignedComponent('ButtonRepair') as TCastleButton;
  ButtonRepair.OnClick := @ClickRepair;
  LabelRepair := ParentDesign.DesignedComponent('LabelRepair') as TCastleLabel;

  ButtonEquip := ParentDesign.DesignedComponent('ButtonEquip') as TCastleButton;
  ButtonEquip.OnClick := @ClickEquip;
  LabelEquip := ParentDesign.DesignedComponent('LabelEquip') as TCastleLabel;

  ButtonUnequip := ParentDesign.DesignedComponent('ButtonUnequip') as TCastleButton;
  ButtonUnequip.OnClick := @ClickUnequip;
  LabelUnequip := ParentDesign.DesignedComponent('LabelUnequip') as TCastleLabel;

  ButtonIdentify := ParentDesign.DesignedComponent('ButtonIdentify') as TCastleButton;
  ButtonIdentify.OnClick := @ClickIdentify;
  LabelIdentify := ParentDesign.DesignedComponent('LabelIdentify') as TCastleLabel;

  LabelHeader := ParentDesign.DesignedComponent('LabelHeader') as TCastleLabel;
  LabelHeader.CustomFont := FontBender40;

  ImageItemIcon := ParentDesign.DesignedComponent('ImageItemIcon') as TCastleImageControl;

  GroupFooterEquipped := ParentDesign.DesignedComponent('GroupFooterEquipped') as TCastleUserInterface;
  GroupFooterGround := ParentDesign.DesignedComponent('GroupFooterGround') as TCastleUserInterface;
  ScrollViewItemFeatures := ParentDesign.DesignedComponent('ScrollViewItemFeatures') as TCastleScrollView;

  Hide;
end;

procedure TPopupItem.ClickReturn(Sender: TObject);
begin
  Sound('menu_button');
  Hide;
end;

procedure TPopupItem.ClickUnequip(Sender: TObject);
begin
  Sound('menu_button');
  ViewGame.UnequipItem(InventoryItem);
  Hide;
end;

procedure TPopupItem.ClickVinculopedia(Sender: TObject);
begin
  Sound('menu_button');
  ViewVinculopedia.OpenItem := InventoryItem.Data.Id;
  ParentDesign.Container.PushView(ViewVinculopedia);
  Hide;
end;

procedure TPopupItem.ClickRepair(Sender: TObject);
begin
  Sound('menu_button');
  ViewGame.RepairItem(MapItem);
  Hide;
end;

procedure TPopupItem.ClickEquip(Sender: TObject);
begin
  Sound('menu_button');
  ViewGame.EquipItem(MapItem);
  Hide;
end;

procedure TPopupItem.ClickIdentify(Sender: TObject);
begin
  Sound('menu_button');
  ViewGame.IdentifyItem(MapItem);
  Hide;
end;

procedure TPopupItem.FillInValues;
var
  I: Integer;
  L: TCastleLabel;
  EnchantmentInfo: TEnchantmentInfo;
  HasUnidentifiedEnchantments: Boolean;
begin
  GroupFooterEquipped.Exists := MapItem = nil;
  GroupFooterGround.Exists := MapItem <> nil;

  ImageItemIcon.DrawableImage := InventoryItem.ItemData.MapImageDrawable;
  ImageItemIcon.OwnsDrawableImage := false;

  VerticalGroupItemFeatures.ClearAndFreeControls;

  L := TCastleLabel.Create(VerticalGroupItemFeatures);
  L.CustomFont := FontBender40;
  if InventoryItem.ItemData.Indestructible then
    L.Caption := 'Indestructible'
  else
  if InventoryItem.Broken then
    L.Caption := Format('Durability: %.1n/%.1n (broken)', [InventoryItem.Durability, InventoryItem.MaxDurability])
  else
    L.Caption := Format('Durability: %.1n/%.1n (%d%%)', [InventoryItem.Durability, InventoryItem.MaxDurability, Round(100 * InventoryItem.Durability / InventoryItem.MaxDurability)]);
  L.Color := ColorDefault;
  VerticalGroupItemFeatures.InsertFront(L);

  if (esWeapon in InventoryItem.Data.EquipSlots) and (InventoryItem.ItemData.Damage > 0) then
  begin
    L := TCastleLabel.Create(VerticalGroupItemFeatures);
    L.CustomFont := FontBender40;
    L.Caption := Format('Damage: %.1n', [InventoryItem.ItemData.Damage]);
    L.Color := ColorDefault;
    VerticalGroupItemFeatures.InsertFront(L);
  end;

  if InventoryItem is TLeash then
  begin
    L := TCastleLabel.Create(VerticalGroupItemFeatures);
    L.CustomFont := FontBender40;
    L.Caption := Format('Leash length: %.1n tiles', [TLeash(InventoryItem).LeashLength]);
    L.Color := ColorDefault;
    VerticalGroupItemFeatures.InsertFront(L);

    L := TCastleLabel.Create(VerticalGroupItemFeatures);
    L.CustomFont := FontBender40;
    if TLeash(InventoryItem).LeashHolder = nil then
    begin
      L.Caption := Format('Leash''s loose end is dangling freely', []);
      L.Color := ColorDefault;
    end else
    begin
      L.Caption := Format('Leash is attached to %s', [(TLeash(InventoryItem).LeashHolder as TActor).Data.DisplayName]);
      L.Color := ColorUiItemBondage;
    end;
    VerticalGroupItemFeatures.InsertFront(L);
  end;

  if InventoryItem.ItemData.ProtectionTop > 0 then
  begin
    L := TCastleLabel.Create(VerticalGroupItemFeatures);
    L.CustomFont := FontBender40;
    L.Caption := Format('Protection (top): %d%%', [Round(100 * InventoryItem.ItemData.ProtectionTop)]);
    L.Color := ColorDefault;
    VerticalGroupItemFeatures.InsertFront(L);
  end;
  if InventoryItem.ItemData.ProtectionBottom > 0 then
  begin
    L := TCastleLabel.Create(VerticalGroupItemFeatures);
    L.CustomFont := FontBender40;
    L.Caption := Format('Protection (bottom): %d%%', [Round(100 * InventoryItem.ItemData.ProtectionBottom)]);
    L.Color := ColorDefault;
    VerticalGroupItemFeatures.InsertFront(L);
  end;

  if InventoryItem.ItemData.Noise > 0 then
  begin
    L := TCastleLabel.Create(VerticalGroupItemFeatures);
    L.CustomFont := FontBender40;
    L.Caption := Format('Noisiness: %.1n', [InventoryItem.ItemData.Noise]);
    L.Color := ColorDefault;
    VerticalGroupItemFeatures.InsertFront(L);
  end;

  if InventoryItem.TimeWorn > 0 then
  begin
    L := TCastleLabel.Create(VerticalGroupItemFeatures);
    L.CustomFont := FontBender40;
    if InventoryItem.TimeWorn < 100 then
      L.Caption := Format('Time worn: %d seconds', [Round(InventoryItem.TimeWorn)])
    else
      L.Caption := Format('Time worn: %d minutes', [Round(InventoryItem.TimeWorn / 60)]);
    L.Color := ColorDefault;
    VerticalGroupItemFeatures.InsertFront(L);
  end;

  if InventoryItem.ItemData.WardrobeMalfunctionChance > 0 then
  begin
    L := TCastleLabel.Create(VerticalGroupItemFeatures);
    L.CustomFont := FontBender40;
    L.Caption := Format('Wardrobe malfunction chance: %.1n%%', [100 * InventoryItem.ItemData.WardrobeMalfunctionChance]);
    L.Color := ColorDefault;
    VerticalGroupItemFeatures.InsertFront(L);
  end;

  HasUnidentifiedEnchantments := false;
  if (InventoryItem.Enchantments <> nil) and (InventoryItem.Enchantments.Count > 0) then
  begin
    if InventoryItem.HasDeterministicEnchantments then
    begin
      L := TCastleLabel.Create(VerticalGroupItemFeatures);
      L.CustomFont := FontSoniano50;
      L.Color := ColorWhite;
      L.AutoSize := false;
      L.Width := ScrollViewItemFeatures.EffectiveWidth;
      L.Alignment := hpMiddle;
      L.Caption := 'Deterministic Enchantments';
      VerticalGroupItemFeatures.InsertFront(L);
      for I := 0 to Pred(InventoryItem.Enchantments.Count) do
        if not InventoryItem.Enchantments[I].IsStochastic then
        begin
          EnchantmentInfo := TEnchantmentInfo.Create(VerticalGroupItemFeatures);
          EnchantmentInfo.FillIn(InventoryItem.Enchantments[I]);
          EnchantmentInfo.SetWidth(ScrollViewItemFeatures.EffectiveWidth - ScrollViewItemFeatures.EffectiveScrollBarWidth - 10);
          VerticalGroupItemFeatures.InsertFront(EnchantmentInfo);
        end;
    end;
    if InventoryItem.HasStochasticEnchantments then
    begin
      L := TCastleLabel.Create(VerticalGroupItemFeatures);
      L.CustomFont := FontSoniano50;
      L.Color := ColorWhite;
      L.Caption := 'Stochastic Enchantments';
      L.AutoSize := false;
      L.Width := ScrollViewItemFeatures.EffectiveWidth;
      L.Alignment := hpMiddle;
      VerticalGroupItemFeatures.InsertFront(L);
      for I := 0 to Pred(InventoryItem.Enchantments.Count) do
        if InventoryItem.Enchantments[I].IsStochastic then
        begin
          EnchantmentInfo := TEnchantmentInfo.Create(VerticalGroupItemFeatures);
          EnchantmentInfo.FillIn(InventoryItem.Enchantments[I]);
          EnchantmentInfo.SetWidth(ScrollViewItemFeatures.EffectiveWidth - ScrollViewItemFeatures.EffectiveScrollBarWidth - 10);
          VerticalGroupItemFeatures.InsertFront(EnchantmentInfo);
          if not InventoryItem.Enchantments[I].Identified then
            HasUnidentifiedEnchantments := true;
        end;
    end;
  end;

  LabelHeader.Caption := InventoryItem.Data.DisplayName;
  if InventoryItem.Broken then
    LabelHeader.Color := ColorUiItemBroken
  else
  if InventoryItem.IsKnownBondage then
    LabelHeader.Color := ColorUiItemBondage
  else
    LabelHeader.Color := ColorDefault;

  LocalStats.IncStat('equipped_' + InventoryItem.Data.Id);

  ButtonRepair.Exists := not InventoryItem.ItemData.Indestructible;
  ButtonRepair.Enabled := ViewGame.CurrentCharacter.CurrentAction.CanStop;
  if InventoryItem.ItemData.CanBeRepaired then
  begin
    if InventoryItem.Durability < InventoryItem.MaxDurability - 0.5 then
    begin
      if ViewGame.CurrentCharacter.Inventory.CanUseHands then
      begin
        LabelRepair.Caption := 'Repair';
        LabelRepair.CustomFont := FontSoniano50;
      end else
      begin
        ButtonRepair.Enabled := false;
        LabelRepair.Caption := 'Can''t repair, hands restrained';
        LabelRepair.CustomFont := FontBender20;
      end;
    end else
    begin
      ButtonRepair.Enabled := false;
      LabelRepair.Caption := 'No need to repair';
      LabelRepair.CustomFont := FontBender20;
    end;
  end else
  begin
    ButtonRepair.Enabled := false;
    LabelRepair.Caption := 'Can''t be repaired';
    LabelRepair.CustomFont := FontBender20;
  end;

  ButtonEquip.Enabled := ViewGame.CurrentCharacter.CurrentAction.CanStop;
  if not InventoryItem.Broken then
  begin
    if ViewGame.CurrentCharacter.Inventory.CanUseHands then
    begin
      case ViewGame.CurrentCharacter.Inventory.CanEquipItem(InventoryItem) of
        eiOk:
          begin
            LabelEquip.Caption := 'Equip';
            LabelEquip.CustomFont := FontSoniano50;
          end;
        eiSlotRestrained:
          begin
            ButtonEquip.Enabled := false;
            LabelEquip.Caption := Format('Can''t equip, %s is in the way', [ViewGame.CurrentCharacter.Inventory.RestraintInTheWay(InventoryItem).Data.DisplayName]);
            LabelEquip.CustomFont := FontBender20;
          end;
        else
          begin
            ButtonEquip.Enabled := false;
            // todo, explain the reason better
            LabelEquip.Caption := Format('Can''t equip', []);
            LabelEquip.CustomFont := FontBender20;
          end;
      end;
    end else
    begin
      ButtonEquip.Enabled := false;
      LabelEquip.Caption := 'Can''t equip, hands restrained';
      LabelEquip.CustomFont := FontBender20;
    end;
  end else
  begin
    ButtonEquip.Enabled := false;
    LabelEquip.Caption := 'Can''t equip, item broken';
    LabelEquip.CustomFont := FontBender20;
  end;

  ButtonUnequip.Enabled := ViewGame.CurrentCharacter.CurrentAction.CanStop;
  if ViewGame.CurrentCharacter.Inventory.CanUseHands then
  begin
    if InventoryItem.IsBondage(ViewGame.CurrentCharacter.Inventory) then
    begin
      if ViewGame.CurrentCharacter.Stamina > 0 then
      begin
        LabelUnequip.Caption := 'Struggle';
        LabelUnequip.CustomFont := FontSoniano50;
      end else
      begin
        ButtonUnequip.Enabled := false;
        LabelUnequip.Caption := 'Can''t striggle, exhausted';
        LabelUnequip.CustomFont := FontBender20;
      end;
    end else
    begin
      LabelUnequip.Caption := 'Unequip';
      LabelUnequip.CustomFont := FontSoniano50;
    end;
  end else
  begin
    ButtonUnequip.Enabled := false;
    LabelUnequip.Caption := 'Can''t unequip, hands restrained';
    LabelUnequip.CustomFont := FontBender20;
  end;

  ButtonIdentify.Exists := HasUnidentifiedEnchantments;
  ButtonIdentify.Enabled := ViewGame.CurrentCharacter.CurrentAction.CanStop;
  if ViewGame.CurrentCharacter.Inventory.CanUseHands then
  begin
    LabelIdentify.Caption := 'Identify';
    LabelIdentify.CustomFont := FontSoniano50;
  end else
  begin
    ButtonIdentify.Enabled := false;
    LabelIdentify.Caption := 'Can''t identify, hands restrained';
    LabelIdentify.CustomFont := FontBender20;
  end;

  ButtonVinculopedia.Exists := InventoryItem.ItemData.HasVinculopediaArticle;
end;

function TPopupItem.Exists: Boolean;
begin
  Exit(ParentDesign.Exists);
end;

procedure TPopupItem.SetWidth(const NewWidth: Single);
begin
  Content.Width := NewWidth;
end;

procedure TPopupItem.Show;
begin
  FillInValues;
  ParentDesign.Exists := true;
  ViewGame.InvalidateInventory(nil);
end;

procedure TPopupItem.Hide;
begin
  VerticalGroupItemFeatures.ClearAndFreeControls;
  ParentDesign.Exists := false;
end;

end.


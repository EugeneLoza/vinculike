{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GamePauseUi;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleControls, CastleUiControls, CastleKeysMouse, CastleTimeUtils, CastleBigButton;

type
  TPauseUi = class(TComponent) // CastleClassUtils -> TCastleComponent
  strict private
    ParentDesign: TCastleDesign;
    DebugModeClickCount: Integer;
    DebugModeClickTimer: TTimerResult;
    procedure ClickCharacter(Sender: TObject);
    procedure ClickDebugMode(Sender: TObject);
    procedure ClickExitToMenu(Sender: TObject);
    procedure ClickVinculopedia(Sender: TObject);
    procedure ClickOptions(Sender: TObject);
    procedure ClickParty(Sender: TObject);
    procedure ClickPauseMenu(Sender: TObject);
    procedure ClickResume(Sender: TObject);
    procedure FillCharacters;
    procedure PressZoomIn(const Sender: TCastleUserInterface;
      const Event: TInputPressRelease; var Handled: Boolean);
    procedure PressZoomOut(const Sender: TCastleUserInterface;
      const Event: TInputPressRelease; var Handled: Boolean);
    procedure ReleaseZoom(const Sender: TCastleUserInterface;
      const Event: TInputPressRelease; var Handled: Boolean);
    procedure SetComponentsActive;
  public
    //TODO: make those somehow a bit more self-contained, it's not good to have them public
    ButtonZoomIn, ButtonZoomOut: TCastleBigButton;
  public
    function Exists: Boolean;
  public
    procedure Show;
    procedure Hide;
    procedure Parse(const AParentDesign: TCastleDesign);
  end;

implementation
uses
  CastleComponentSerialize,
  CastleMobileButton,
  GameSerializableObject,
  GameFonts, GameSounds, GameSaveGame, GamePlayerCharacter, GameMap, GameLog, GameColors,
  GameViewOptions, GameViewGame, GameViewMainMenu, GameViewVinculopedia, GameUiUtils,
  GameThemedButton, GameConfiguration;

procedure TPauseUi.Parse(const AParentDesign: TCastleDesign);
begin
  ParentDesign := AParentDesign;
  ParentDesign.Exists := false;
  (ParentDesign.DesignedComponent('ButtonResume') as TCastleButton).OnClick := @ClickResume;
  (ParentDesign.DesignedComponent('ButtonResume') as TCastleButton).CustomFont := FontSoniano90;
  (ParentDesign.DesignedComponent('ButtonOptions') as TCastleButton).OnClick := @ClickOptions;
  (ParentDesign.DesignedComponent('ButtonOptions') as TCastleButton).CustomFont := FontSoniano90;
  (ParentDesign.DesignedComponent('ButtonVinculopedia') as TCastleButton).OnClick := @ClickVinculopedia;
  (ParentDesign.DesignedComponent('ButtonVinculopedia') as TCastleButton).CustomFont := FontSoniano90;
  (ParentDesign.DesignedComponent('ButtonMenu') as TCastleButton).OnClick := @ClickExitToMenu;
  (ParentDesign.DesignedComponent('ButtonMenu') as TCastleButton).CustomFont := FontSoniano90;

  (ParentDesign.DesignedComponent('ButtonParty') as TCastleBigButton).OnClick := @ClickParty;
  (ParentDesign.DesignedComponent('ButtonParty') as TCastleBigButton).CustomFont := FontSoniano90;
  (ParentDesign.DesignedComponent('ButtonPauseMenu') as TCastleBigButton).OnClick := @ClickPauseMenu;
  (ParentDesign.DesignedComponent('ButtonPauseMenu') as TCastleBigButton).CustomFont := FontSoniano90;

  (ParentDesign.DesignedComponent('ButtonPause') as TCastleBigButton).OnClick := @ClickResume;
  (ParentDesign.DesignedComponent('ButtonCamp') as TCastleBigButton).OnClick := @ViewGame.ClickCamp;

  (ParentDesign.DesignedComponent('LabelPause') as TCastleLabel).CustomFont := FontBender90;

  (ParentDesign.DesignedComponent('ButtonDebugMode') as TCastleButton).OnClick := @ClickDebugMode;

  ButtonZoomIn := ParentDesign.DesignedComponent('ButtonZoomIn') as TCastleBigButton;
  ButtonZoomIn.OnPress := @PressZoomIn;
  ButtonZoomIn.OnRelease := @ReleaseZoom;
  ButtonZoomOut := ParentDesign.DesignedComponent('ButtonZoomOut') as TCastleBigButton;
  ButtonZoomOut.OnPress := @PressZoomOut;
  ButtonZoomOut.OnRelease := @ReleaseZoom;

  Hide;
end;

procedure TPauseUi.ClickCharacter(Sender: TObject);
begin
  Sound('menu_button');
  ViewGame.CurrentCharacter := ObjectByReferenceId((Sender as TCastleButton).Tag) as TPlayerCharacter;
  ViewGame.InvalidateInventory(ViewGame.CurrentCharacter);
  ViewGame.InvalidatePosition(ViewGame.CurrentCharacter);
  ShowLog('SELECTED CHARACTER: %s', [ViewGame.CurrentCharacter.Data.DisplayName], ColorLogLevelUp);
  // TODO: remove this
  ShowLog('%s current level: %d', [ViewGame.CurrentCharacter.Data.DisplayName, ViewGame.CurrentCharacter.Experience.Level], ColorLogExperience);
  ShowLog('Experience: %d; To next level: %d', [Round(ViewGame.CurrentCharacter.Experience.Xp), Round(ViewGame.CurrentCharacter.Experience.ExperienceToNextLevel)], ColorLogExperience);
  ////////////////////////////////TUIState.Pop(Self);
  ViewGame.UnpauseGame;
end;

procedure TPauseUi.ClickDebugMode(Sender: TObject);
const
  EnterDebugTime = 4;
begin
  if DebugModeClickTimer.ElapsedTime < EnterDebugTime then
  begin
    Inc(DebugModeClickCount);
    if DebugModeClickCount >= 5 then
    begin
      ViewGame.DebugUi.Toggle;
      DebugModeClickTimer := Timer;
      DebugModeClickCount := 0;
    end;
  end else
  begin
    DebugModeClickTimer := Timer;
    DebugModeClickCount := 0;
  end;
end;

procedure TPauseUi.ClickExitToMenu(Sender: TObject);
begin
  Sound('menu_button');
  SaveGame;
  Map.Clear;
  ParentDesign.Container.View := ViewMainMenu;
end;

procedure TPauseUi.ClickVinculopedia(Sender: TObject);
begin
  Sound('menu_button');
  ParentDesign.Container.PushView(ViewVinculopedia);
end;

procedure TPauseUi.ClickOptions(Sender: TObject);
begin
  Sound('menu_button');
  ParentDesign.Container.PushView(ViewOptions);
end;

procedure TPauseUi.ClickParty(Sender: TObject);
begin
  Configuration.PartyActive := not Configuration.PartyActive;
  if Configuration.PartyActive then
    Sound('menu_button')
  else
    Sound('menu_back');
  SetComponentsActive;
end;

procedure TPauseUi.ClickPauseMenu(Sender: TObject);
begin
  Configuration.PauseMenuActive := not Configuration.PauseMenuActive;
  if Configuration.PauseMenuActive then
    Sound('menu_button')
  else
    Sound('menu_back');
  SetComponentsActive;
end;

procedure TPauseUi.ClickResume(Sender: TObject);
begin
  Sound('menu_back');
  ViewGame.UnpauseGame;
end;

procedure TPauseUi.FillCharacters;
var
  VerticalGroupCharacters: TCastleVerticalGroup;
  PlayerCharacter: TPlayerCharacter;
  Button: TCastleMobileButton;
begin
  VerticalGroupCharacters := ParentDesign.DesignedComponent('VerticalGroupCharacters') as TCastleVerticalGroup;
  VerticalGroupCharacters.ClearAndFreeControls;
  for PlayerCharacter in Map.PlayerCharactersList do
  if not PlayerCharacter.IsStranger then
    begin
      Button := TCastleMobileButton.Create(VerticalGroupCharacters);
      if PlayerCharacter = ViewGame.CurrentCharacter then
        Button.SetTheme('inventory_button_light_selected')
      else
        Button.SetTheme('inventory_button_light');
      Button.Width := 800;
      Button.Height := 100;
      Button.Html := true;
      Button.CustomTextColorUse := true;
      Button.EnableParentDragging := true;
      if not PlayerCharacter.IsCaptured and (PlayerCharacter.AtMap = Map.CurrentDepth) then
      begin
        Button.Enabled := true;
        Button.CustomTextColor := ColorUiCharacterReady;
        if PlayerCharacter = ViewGame.CurrentCharacter then
          Button.Caption := Format('%s (lvl.%d) - ACTIVE (depth %d)', [PlayerCharacter.Data.DisplayName, PlayerCharacter.Experience.Level, PlayerCharacter.AtMap])
        else
        if PlayerCharacter.AtMap = 0 then
        begin
          if PlayerCharacter.IsAvailableForDungeon then
            Button.Caption := Format('%s (lvl.%d) - READY (in settlement)', [PlayerCharacter.Data.DisplayName, PlayerCharacter.Experience.Level])
          else
          begin
            Button.Enabled := false;
            Button.CustomTextColor := ColorUiCharacterAway;
            Button.Caption := Format('%s (lvl.%d) - RECOVERING', [PlayerCharacter.Data.DisplayName, PlayerCharacter.Experience.Level]);
          end;
        end else
          Button.Caption := Format('%s (lvl.%d) - READY (depth %d)', [PlayerCharacter.Data.DisplayName, PlayerCharacter.Experience.Level, PlayerCharacter.AtMap]);
        Button.Tag := PlayerCharacter.ReferenceId;
        Button.OnClick := @ClickCharacter;
      end else
      if PlayerCharacter.IsCaptured then
      begin
        Button.Enabled := false;
        Button.CustomTextColor := ColorUiCharacterCaptured;
        if PlayerCharacter.AtMap = Map.CurrentDepth then
          Button.Caption := Format('%s (lvl.%d) captured at this level', [PlayerCharacter.Data.DisplayName, PlayerCharacter.Experience.Level])
        else
          Button.Caption := Format('%s (lvl.%d) captured at level %d', [PlayerCharacter.Data.DisplayName, PlayerCharacter.Experience.Level, PlayerCharacter.AtMap]);
      end else
      begin
        Button.Enabled := false;
        if PlayerCharacter.AtMap = 0 then
        begin
          Button.CustomTextColor := ColorUiCharacterAway;
          Button.Caption := Format('%s (lvl.%d) in the settlement', [PlayerCharacter.Data.DisplayName, PlayerCharacter.Experience.Level]);
        end else
        begin
          Button.Caption := Format('%s (lvl.%d) away at lvl %d', [PlayerCharacter.Data.DisplayName, PlayerCharacter.Experience.Level, PlayerCharacter.AtMap]);
          Button.CustomTextColor := ColorError; // it's an error for now!
        end;
      end;
      VerticalGroupCharacters.InsertFront(Button);
    end;
end;

procedure TPauseUi.PressZoomIn(const Sender: TCastleUserInterface;
  const Event: TInputPressRelease; var Handled: Boolean);
begin
  if not ButtonZoomIn.Enabled then Exit;
  if Event.EventType = itMouseButton then
    ViewGame.DoZoomIn;
end;

procedure TPauseUi.PressZoomOut(const Sender: TCastleUserInterface;
  const Event: TInputPressRelease; var Handled: Boolean);
begin
  if not ButtonZoomOut.Enabled then Exit;
  if Event.EventType = itMouseButton then
    ViewGame.DoZoomOut;
end;

procedure TPauseUi.ReleaseZoom(const Sender: TCastleUserInterface;
  const Event: TInputPressRelease; var Handled: Boolean);
begin
  if Event.EventType = itMouseButton then
    ViewGame.DoReleaseZoom;
end;

procedure TPauseUi.SetComponentsActive;
begin
  (ParentDesign.DesignedComponent('ScrollViewCharacters') as TCastleUserInterface).Exists := Configuration.PartyActive;
  (ParentDesign.DesignedComponent('VerticalGroupPause') as TCastleUserInterface).Exists := Configuration.PauseMenuActive;
  (ParentDesign.DesignedComponent('Shade') as TCastleUserInterface).Exists := Configuration.PartyActive or Configuration.PauseMenuActive;
end;

function TPauseUi.Exists: Boolean;
begin
  Exit(ParentDesign.Exists);
end;

procedure TPauseUi.Show;
begin
  DebugModeClickTimer := Timer;
  DebugModeClickCount := 0;

  FillCharacters;
  SetComponentsActive;
  ParentDesign.Exists := true;
end;

procedure TPauseUi.Hide;
begin
  ParentDesign.Exists := false;
end;

end.


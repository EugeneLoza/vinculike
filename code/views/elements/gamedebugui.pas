{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameDebugUi;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleControls;

type
  TDebugUi = class(TComponent) // CastleClassUtils -> TCastleComponent
  strict private
    ParentDesign: TCastleDesign;
    VerticalGroupObjects: TCastleVerticalGroup;
    procedure ClickAddItemMenu(Sender: TObject);
    procedure ClickAddMonsterMenu(Sender: TObject);
    procedure ClickAddNewbie(Sender: TObject);
    procedure ClickAddTrapMenu(Sender: TObject);
    procedure ClickDungeonInfo(Sender: TObject);
    procedure ClickGoToMap(Sender: TObject);
    procedure ClickGoToMapMenu(Sender: TObject);
    procedure ClickCaptureHealth(Sender: TObject);
    procedure ClickCaptureMental(Sender: TObject);
    procedure ClickCaptureTied(Sender: TObject);
    procedure ClickCaptureGlue(Sender: TObject);
    procedure ClickDropAllItems(Sender: TObject);
    procedure ClickHit10(Sender: TObject);
    procedure ClickHit100(Sender: TObject);
    procedure ClickHitStamina100(Sender: TObject);
    procedure ClickHitWill100(Sender: TObject);
    procedure ClickItemButton(Sender: TObject);
    procedure ClickKillAllMonsters(Sender: TObject);
    procedure ClickLevelUp(Sender: TObject);
    procedure ClickAddRandomItem(Sender: TObject);
    procedure ClickClose(Sender: TObject);
    procedure ClickLogSaveGame(Sender: TObject);
    procedure ClickMonsterButton(Sender: TObject);
    procedure ClickRandomBody(Sender: TObject);
    procedure ClickResetCharacter(Sender: TObject);
    procedure ClickShowMap(Sender: TObject);
    procedure ClickSpawnPatrol(Sender: TObject);
    procedure ClickTeleport(Sender: TObject);
    procedure ClickVisualizeColliders(Sender: TObject);
    procedure ClickVisualizeExtrema(Sender: TObject);
    procedure ClickVisualizeMonsterOrigins(Sender: TObject);
    procedure ClickVisualizePlayerOrigins(Sender: TObject);
  public
    procedure Toggle;
    procedure Parse(const AParentDesign: TCastleDesign);
  end;

implementation
uses
  CastleComponentSerialize, CastleLog, CastleColors,
  GameMap, GameMapTypes, GameRandom, GameMapInterface,
  GamePlayerCharacter, GameMonster, GameActorData, GameMonstersDatabase,
  GameItemsDatabase, GameLeash, GameInventoryItem, GameItemData, GameItemDataAbstract, GameApparelSlots,
  GameViewGame, GameViewEndGame, GameUiUtils, GameSaveGame;

procedure TDebugUi.ClickLevelUp(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Experience.AddExperience(ViewGame.CurrentCharacter.Experience.ExperienceToNextLevel + 1);
end;

procedure TDebugUi.ClickCaptureHealth(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Reset; // heal completely
  ViewGame.CurrentCharacter.Health := -1;
  ViewGame.CurrentCharacter.GetCapturedSafe;
  ViewEndGame.EndGameKind := egHealth;
  ViewGame.ScheduleEndGame;
end;

procedure TDebugUi.ClickAddNewbie(Sender: TObject);
var
  Newbie: TPlayerCharacter;
begin
  Newbie := ViewGame.NewPlayerCharacter;
  Newbie.Teleport(Map.EntranceX, Map.EntranceY); // it will reset the character to idle + will teleport to entrance (but character will remain at map 0)
  Map.CacheCharactersOnThisLevel;
  ViewGame.InvalidateInventory(nil);
  ViewGame.InvalidatePosition(nil);
end;

procedure TDebugUi.ClickAddItemMenu(Sender: TObject);
var
  I: TItemData;
  S: String;
  B: TCastleButton;
  Lst: TStringList;
begin
  VerticalGroupObjects.ClearAndFreeControls;
  Lst := TStringList.Create;
  Lst.Sorted := true;
  for I in ItemsDataList do // just to sort them
    Lst.Add(I.Id);
  for S in Lst do
  begin
    B := TCastleButton.Create(VerticalGroupObjects);
    B.Caption := S;
    B.EnableParentDragging := true;
    B.OnClick := @ClickItemButton;
    VerticalGroupObjects.InsertFront(B);
  end;
  FreeAndNil(Lst);
end;

procedure TDebugUi.ClickAddMonsterMenu(Sender: TObject);
var
  M: TActorData;
  S: String;
  B: TCastleButton;
  Lst: TStringList;
begin
  VerticalGroupObjects.ClearAndFreeControls;
  Lst := TStringList.Create;
  Lst.Sorted := true;
  for M in MonstersData do // just to sort them
    Lst.Add(M.Id);
  for S in Lst do
  begin
    B := TCastleButton.Create(VerticalGroupObjects);
    B.Caption := S;
    B.EnableParentDragging := true;
    B.OnClick := @ClickMonsterButton;
    VerticalGroupObjects.InsertFront(B);
  end;
  FreeAndNil(Lst);
end;

procedure TDebugUi.ClickAddTrapMenu(Sender: TObject);
var
  M: TActorData;
  S: String;
  B: TCastleButton;
  Lst: TStringList;
begin
  VerticalGroupObjects.ClearAndFreeControls;
  Lst := TStringList.Create;
  Lst.Sorted := true;
  for M in TrapsData do // just to sort them
    Lst.Add(M.Id);
  for S in Lst do
  begin
    B := TCastleButton.Create(VerticalGroupObjects);
    B.Caption := S;
    B.EnableParentDragging := true;
    B.OnClick := @ClickMonsterButton;
    VerticalGroupObjects.InsertFront(B);
  end;
  FreeAndNil(Lst);
end;

procedure TDebugUi.ClickDungeonInfo(Sender: TObject);
var
  M: TMonster;
  M1: TActorData;
  S: String;
  L: TCastleLabel;
  B: TCastleButton;
  Lst: TStringList;
begin
  VerticalGroupObjects.ClearAndFreeControls;
  Lst := TStringList.Create;
  Lst.Sorted := true;
  Lst.Duplicates := dupIgnore;
  for M in Map.MonstersList do // just to sort them
    Lst.Add('Spawn: ' + M.Data.Id);
  for M1 in Map.PatrolList do // just to sort them
    Lst.Add('Patrol: ' + M1.Id);

  B := TCastleButton.Create(VerticalGroupObjects);
  B.Caption := 'Restart Current Map';
  B.Tag := Map.CurrentDepth;
  B.OnClick := @ClickGoToMap;
  VerticalGroupObjects.InsertFront(B);

  for S in Lst do
  begin
    L := TCastleLabel.Create(VerticalGroupObjects);
    L.Caption := S;
    L.Color := White;
    VerticalGroupObjects.InsertFront(L);
  end;
  FreeAndNil(Lst);
end;

procedure TDebugUi.ClickGoToMap(Sender: TObject);
begin
  Map.CurrentDepth := (Sender as TCastleButton).Tag - 1;
  VerticalGroupObjects.ClearAndFreeControls; // close this menu so that "restart this map" will make sense
  ViewGame.NewMap;
end;

procedure TDebugUi.ClickGoToMapMenu(Sender: TObject);
var
  I: Integer;
  B: TCastleButton;

  procedure AddMapButton(const MapNumber: Integer; const MapName: String);
  begin
    B := TCastleButton.Create(VerticalGroupObjects);
    B.Caption := MapName;
    B.Tag := MapNumber;
    B.EnableParentDragging := true;
    B.OnClick := @ClickGoToMap;
    if (B.Caption.Length < 5) then
    begin
      B.AutoSizeWidth := false;
      B.Width := 200;
    end;
    VerticalGroupObjects.InsertFront(B);
  end;

begin
  VerticalGroupObjects.ClearAndFreeControls;
  AddMapButton(0, 'Settlement');
  AddMapButton(Map.CurrentDepth, 'Restart Current Map (' + Map.CurrentDepth.ToString + ')');
  for I in [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,40,50,60,70] do
    AddMapButton(I, I.ToString);
end;

procedure TDebugUi.ClickCaptureMental(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Reset; // heal completely
  ViewGame.CurrentCharacter.Will := -1;
  ViewGame.CurrentCharacter.GetCapturedSafe;
  ViewEndGame.EndGameKind := egMental;
  ViewGame.ScheduleEndGame;
end;

procedure TDebugUi.ClickCaptureTied(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Reset; // heal completely
  ViewGame.CurrentCharacter.GetCapturedSafe;
  ViewEndGame.EndGameKind := egTiedUp;
  ViewGame.ScheduleEndGame;
end;

procedure TDebugUi.ClickCaptureGlue(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Reset; // heal completely
  ViewGame.CurrentCharacter.GetCapturedSafe;
  ViewEndGame.EndGameKind := egGlued;
  ViewGame.ScheduleEndGame;
end;

procedure TDebugUi.ClickDropAllItems(Sender: TObject);
var
  E: TApparelSlot;
begin
  for E in ViewGame.CurrentCharacter.Blueprint.EquipmentSlots do
    ViewGame.CurrentCharacter.Inventory.UnequipAndDrop(E, false);
end;

procedure TDebugUi.ClickHit10(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Hit(10);
end;

procedure TDebugUi.ClickHit100(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Hit(100);
end;

procedure TDebugUi.ClickHitStamina100(Sender: TObject);
begin
  ViewGame.CurrentCharacter.HitStamina(100);
end;

procedure TDebugUi.ClickHitWill100(Sender: TObject);
begin
  ViewGame.CurrentCharacter.HitWill(100);
end;

procedure TDebugUi.ClickItemButton(Sender: TObject);
var
  AItem: TInventoryItem;
  AItemData: TItemDataAbstract;
begin
  AItemData := ItemsDataDictionary[(Sender as TCastleButton).Caption];
  if esLeash in AItemData.EquipSlots then
    AItem := TLeash.NewLeash(AItemData)
  else
    AItem := TInventoryItem.NewItem(AItemData);
  ViewGame.CurrentCharacter.Inventory.EquipItem(AItem);
end;

procedure TDebugUi.ClickMonsterButton(Sender: TObject);
var
  M: TMonster;
  TX, TY: Int16;
begin
  M := TMonster.Create;
  M.Data := MonstersDataDictionary[(Sender as TCastleButton).Caption];
  M.Reset;
  repeat
    TX := Rnd.Random(Map.SizeX);
    TY := Rnd.Random(Map.SizeY);
  until Map.PassableTiles[M.PredSize][TX + Map.SizeX * TY];
  M.Teleport(TX, TY);
  Map.MonstersList.Add(M);
end;

procedure TDebugUi.ClickKillAllMonsters(Sender: TObject);
var
  M: TMonster;
begin
  for M in Map.MonstersList do
    if M.CanAct then
      M.Hit(10000);
end;

procedure TDebugUi.ClickAddRandomItem(Sender: TObject);
var
  AItemData: TItemDataAbstract;
begin
  repeat
    AItemData := ItemsDataList[Rnd.Random(ItemsDataList.Count)];
  until not (esLeash in AItemData.EquipSlots); // and can fit blueprint
  ViewGame.CurrentCharacter.Inventory.EquipItem(TInventoryItem.NewItem(AItemData));
end;

procedure TDebugUi.ClickClose(Sender: TObject);
begin
  VerticalGroupObjects.ClearAndFreeControls;
  ParentDesign.Exists := false;
end;

procedure TDebugUi.ClickLogSaveGame(Sender: TObject);
begin
  WriteLnLog('================================= SAVE GAME START =================================');
  WriteLnLog(SaveAsString);
  WriteLnLog('================================== SAVE GAME END ==================================');
end;

procedure TDebugUi.ClickRandomBody(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Blueprint.RandomBody(ViewGame.CurrentCharacter);
end;

procedure TDebugUi.ClickResetCharacter(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Reset;
end;

procedure TDebugUi.ClickShowMap(Sender: TObject);
begin
  Map.ShowAllMap;
end;

procedure TDebugUi.ClickSpawnPatrol(Sender: TObject);
begin
  Map.EnemySpawnTime := 0;
end;

procedure TDebugUi.ClickTeleport(Sender: TObject);
begin
  ViewGame.CurrentCharacter.TeleportToUnknown(0.5);
  ViewGame.ScheduleMonstersToIdle;  //Map.MonstersToIdle; // we can't do that here, it'll free the parent action and everything goes BOOM
end;

procedure TDebugUi.ClickVisualizeColliders(Sender: TObject);
begin
  if ViewGame.InternalMapInterface.DebugNavgrid < 0 then
    ViewGame.InternalMapInterface.DebugNavgrid := PredMaxColliderSize
  else
    Dec(ViewGame.InternalMapInterface.DebugNavgrid);

  if ViewGame.InternalMapInterface.DebugNavgrid >= 0 then
    (ParentDesign.DesignedComponent('ButtonVisualizeColliders') as TCastleButton).Caption := 'Collider: ' + IntToStr(ViewGame.InternalMapInterface.DebugNavgrid + 1)
  else
    (ParentDesign.DesignedComponent('ButtonVisualizeColliders') as TCastleButton).Caption := 'Collider: OFF';
end;

procedure TDebugUi.ClickVisualizeExtrema(Sender: TObject);
begin
  ViewGame.InternalMapInterface.DebugExtrema := not ViewGame.InternalMapInterface.DebugExtrema;
end;

procedure TDebugUi.ClickVisualizeMonsterOrigins(Sender: TObject);
begin
  ViewGame.InternalMapInterface.DebugMonsterOrigins := not ViewGame.InternalMapInterface.DebugMonsterOrigins;
end;

procedure TDebugUi.ClickVisualizePlayerOrigins(Sender: TObject);
begin
  ViewGame.InternalMapInterface.DebugPlayerOrigins := not ViewGame.InternalMapInterface.DebugPlayerOrigins;
end;

procedure TDebugUi.Toggle;
begin
  if ParentDesign.Exists then
    ClickClose(nil)
  else
    ParentDesign.Exists := true;
end;

procedure TDebugUi.Parse(const AParentDesign: TCastleDesign);
begin
  ParentDesign := AParentDesign;
  ParentDesign.Exists := false;

  VerticalGroupObjects := ParentDesign.DesignedComponent('VerticalGroupObjects') as TCastleVerticalGroup;

  (ParentDesign.DesignedComponent('ButtonClose') as TCastleButton).OnClick := @ClickClose;
  (ParentDesign.DesignedComponent('ButtonShowMap') as TCastleButton).OnClick := @ClickShowMap;
  (ParentDesign.DesignedComponent('ButtonSpawnPatrol') as TCastleButton).OnClick := @ClickSpawnPatrol;
  (ParentDesign.DesignedComponent('ButtonAddMonsterMenu') as TCastleButton).OnClick := @ClickAddMonsterMenu;
  (ParentDesign.DesignedComponent('ButtonAddTrapMenu') as TCastleButton).OnClick := @ClickAddTrapMenu;
  (ParentDesign.DesignedComponent('ButtonKillAllMonsters') as TCastleButton).OnClick := @ClickKillAllMonsters;
  (ParentDesign.DesignedComponent('ButtonGoToMap') as TCastleButton).OnClick := @ClickGoToMapMenu;
  (ParentDesign.DesignedComponent('ButtonResetCharacter') as TCastleButton).OnClick := @ClickResetCharacter;
  (ParentDesign.DesignedComponent('ButtonHitPlayer10') as TCastleButton).OnClick := @ClickHit10;
  (ParentDesign.DesignedComponent('ButtonHitPlayer100') as TCastleButton).OnClick := @ClickHit100;
  (ParentDesign.DesignedComponent('ButtonHitStamina100') as TCastleButton).OnClick := @ClickHitStamina100;
  (ParentDesign.DesignedComponent('ButtonHitWill100') as TCastleButton).OnClick := @ClickHitWill100;
  (ParentDesign.DesignedComponent('ButtonAddRandomItem') as TCastleButton).OnClick := @ClickAddRandomItem;
  (ParentDesign.DesignedComponent('ButtonAddItemMenu') as TCastleButton).OnClick := @ClickAddItemMenu;
  (ParentDesign.DesignedComponent('ButtonDropAllItems') as TCastleButton).OnClick := @ClickDropAllItems;
  (ParentDesign.DesignedComponent('ButtonTeleport') as TCastleButton).OnClick := @ClickTeleport;
  (ParentDesign.DesignedComponent('ButtonLevelUp') as TCastleButton).OnClick := @ClickLevelUp;
  (ParentDesign.DesignedComponent('ButtonLogSaveGame') as TCastleButton).OnClick := @ClickLogSaveGame;
  (ParentDesign.DesignedComponent('ButtonDungeonInfo') as TCastleButton).OnClick := @ClickDungeonInfo;

  (ParentDesign.DesignedComponent('ButtonRandomBody') as TCastleButton).OnClick := @ClickRandomBody; // not used anymore and disabled

  (ParentDesign.DesignedComponent('ButtonVisualizeExtrema') as TCastleButton).OnClick := @ClickVisualizeExtrema;
  (ParentDesign.DesignedComponent('ButtonVisualizeColliders') as TCastleButton).OnClick := @ClickVisualizeColliders;
  (ParentDesign.DesignedComponent('ButtonVisualizePlayerOrigins') as TCastleButton).OnClick := @ClickVisualizePlayerOrigins;
  (ParentDesign.DesignedComponent('ButtonVisualizeMonsterOrigins') as TCastleButton).OnClick := @ClickVisualizeMonsterOrigins;

  (ParentDesign.DesignedComponent('ButtonAddNewbie') as TCastleButton).OnClick := @ClickAddNewbie;
  (ParentDesign.DesignedComponent('ButtonCaptureHealth') as TCastleButton).OnClick := @ClickCaptureHealth;
  (ParentDesign.DesignedComponent('ButtonCaptureMental') as TCastleButton).OnClick := @ClickCaptureMental;
  (ParentDesign.DesignedComponent('ButtonCaptureTied') as TCastleButton).OnClick := @ClickCaptureTied;
  (ParentDesign.DesignedComponent('ButtonCaptureGlued') as TCastleButton).OnClick := @ClickCaptureGlue;
end;

end.


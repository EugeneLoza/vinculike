{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Displays "are you sure you want to start a new run" dialogue
  currently it's a debug feature: will capture current chatter, clean up some potential problems and start a new run }
unit GameViewConfirmNewRun;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleUiControls, CastleKeysMouse,
  GameViewAbstract;

type
  TViewConfirmNewRun = class(TViewAbstract)
  strict private
    procedure ClickReset(Sender: TObject);
    procedure ClickReturn(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    function Press(const Event: TInputPressRelease): Boolean; override;
  end;

var
  ViewConfirmNewRun: TViewConfirmNewRun;

implementation
uses
  CastleControls, CastleXmlUtils,
  GameFonts, GameSounds, GameScreenEffect, GameSaveGame,
  GameViewOptions, GameViewMainMenu;

constructor TViewConfirmNewRun.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewconfirmnewrun.castle-user-interface';
  DesignPreload := false;
end;

procedure TViewConfirmNewRun.Start;
begin
  inherited;

  TScreenEffect.Create(FreeAtStop).Inject(Self);

  (DesignedComponent('ButtonReset') as TCastleButton).OnClick := @ClickReset;
  (DesignedComponent('ButtonReset') as TCastleButton).CustomFont := FontSoniano90;
  (DesignedComponent('ButtonCancel') as TCastleButton).OnClick := @ClickReturn;
  (DesignedComponent('ButtonCancel') as TCastleButton).CustomFont := FontSoniano90;

  (DesignedComponent('LabelWarning') as TCastleLabel).CustomFont := FontBenderBold150;
  (DesignedComponent('LabelAreYouSure') as TCastleLabel).CustomFont := FontBender90;
end;

function TViewConfirmNewRun.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;

  if Event.IsKey(keyEscape) then
    ClickReturn(Self);
end;

procedure TViewConfirmNewRun.ClickReset(Sender: TObject);
begin
  Sound('menu_quit');
  SaveGameResetCurrentCharacter;
  Container.View := ViewMainMenu;
end;

procedure TViewConfirmNewRun.ClickReturn(Sender: TObject);
begin
  Sound('menu_back');
  Container.View := ViewMainMenu;
  Container.PushView(ViewOptions);
end;

end.


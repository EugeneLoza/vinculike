{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Displays "are you sure you want to delete the save game" dialogue }
unit GameViewConfirmDeleteSave;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleUiControls, CastleKeysMouse,
  GameViewAbstract;

type
  TViewConfirmDeleteSave = class(TViewAbstract)
  strict private
    procedure ClickReset(Sender: TObject);
    procedure ClickReturn(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    function Press(const Event: TInputPressRelease): Boolean; override;
  end;

var
  ViewConfirmDeleteSave: TViewConfirmDeleteSave;

implementation
uses
  CastleControls, CastleWindow,
  GameFonts, GameSounds, GameSaveGame, GameScreenEffect,
  GameViewOptions, GameViewMainMenu;

constructor TViewConfirmDeleteSave.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewconfirmdeletesave.castle-user-interface';
  DesignPreload := false;
end;

procedure TViewConfirmDeleteSave.Start;
begin
  inherited;

  TScreenEffect.Create(FreeAtStop).Inject(Self);

  (DesignedComponent('ButtonReset') as TCastleButton).OnClick := @ClickReset;
  (DesignedComponent('ButtonReset') as TCastleButton).CustomFont := FontSoniano90;
  (DesignedComponent('ButtonCancel') as TCastleButton).OnClick := @ClickReturn;
  (DesignedComponent('ButtonCancel') as TCastleButton).CustomFont := FontSoniano90;

  (DesignedComponent('LabelWarning') as TCastleLabel).CustomFont := FontBenderBold150;
  (DesignedComponent('LabelAreYouSure') as TCastleLabel).CustomFont := FontBender90;
end;

function TViewConfirmDeleteSave.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;

  if Event.IsKey(keyEscape) then
    ClickReturn(Self);
end;

procedure TViewConfirmDeleteSave.ClickReset(Sender: TObject);
begin
  Sound('menu_quit');
  DeleteSaveGame;
  Container.View := ViewMainMenu;
end;

procedure TViewConfirmDeleteSave.ClickReturn(Sender: TObject);
begin
  Sound('menu_back');
  Container.View := ViewMainMenu;
  Container.PushView(ViewOptions);
end;

end.


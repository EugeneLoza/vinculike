{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Show "18+ content warning" screen }
unit GameViewContentWarning;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleUiControls,
  GameViewAbstract;

type
  TViewContentWarning = class(TViewAbstract)
  strict private
    procedure ClickQuit(Sender: TObject);
    procedure ClickPlay(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
  end;

var
  ViewContentWarning: TViewContentWarning;

implementation
uses
  CastleControls, CastleWindow, CastleApplicationProperties,
  GameConfiguration, GameViewMainMenu, GameFonts, GameSounds, GameScreenEffect;

constructor TViewContentWarning.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewcontentwarning.castle-user-interface';
  DesignPreload := false;
end;

procedure TViewContentWarning.Start;
begin
  inherited;

  TScreenEffect.Create(FreeAtStop).Inject(Self);

  (DesignedComponent('ButtonQuit') as TCastleButton).OnClick := @ClickQuit;
  (DesignedComponent('ButtonQuit') as TCastleButton).Exists := not ApplicationProperties.TouchDevice;
  (DesignedComponent('ButtonQuit') as TCastleButton).CustomFont := FontSoniano90;
  (DesignedComponent('ButtonStart') as TCastleButton).OnClick := @ClickPlay;
  (DesignedComponent('ButtonStart') as TCastleButton).CustomFont := FontSoniano90;
  (DesignedComponent('CheckboxDoNotAskAgain') as TCastleCheckBox).Checked := not Configuration.AlwaysAskContentWarning;
  (DesignedComponent('CheckboxDoNotAskAgain') as TCastleCheckBox).CustomFont := FontBender40;

  (DesignedComponent('LabelCaution') as TCastleLabel).CustomFont := FontBenderBold150;
  (DesignedComponent('LabelMainText') as TCastleLabel).CustomFont := FontBender90;
end;

procedure TViewContentWarning.ClickQuit(Sender: TObject);
begin
  Sound('menu_quit');
  Application.MainWindow.Close;
end;

procedure TViewContentWarning.ClickPlay(Sender: TObject);
begin
  Sound('menu_button');
  { note: we set those to exactly the same value =
    checked: do not ask again, and do not ask always
    unchecked: ask again and ask always
    the difference will be only in "state" of the checkbox when the next time the view is displayed
    and the values are different only in their default state: ask, but don't ask always }
  Configuration.AskContentWarning := not not (DesignedComponent('CheckboxDoNotAskAgain') as TCastleCheckbox).Checked;
  Configuration.AlwaysAskContentWarning := Configuration.AskContentWarning;
  Configuration.Save;
  Container.View := ViewMainMenu;
end;

end.


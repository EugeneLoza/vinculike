{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Temporary view to display the end-game story,
  also handles "how" the character was captured (affects only story) }
unit GameViewEndGame;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleUiControls, CastleKeysMouse, CastleControls, CastleTimeUtils,
  GameViewAbstract;

const
  EndGameTimeout = Single(2.0);

type
  TEndGameKind = (egHealth, egMental, egTiedUp, egGlued);

type
  TViewEndGame = class(TViewAbstract)
  strict private
    LabelReturn: TCastleLabel;
    Timeout: TTimerResult;
    procedure ClickReturn(Sender: TObject);
  public
    EndGameKind: TEndGameKind;
    constructor Create(AOwner: TComponent); override;
    procedure Update(const SecondsPassed: Single;
      var HandleInput: Boolean); override;
    procedure Start; override;
    function Press(const Event: TInputPressRelease): Boolean; override;
  end;

var
  ViewEndGame: TViewEndGame;

implementation
uses
  CastleWindow,
  GameFonts, GameSounds, GameLog, GameRandom, GameScreenEffect,
  GameViewGame;

constructor TViewEndGame.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewendgame.castle-user-interface';
  DesignPreload := false;
end;

{$PUSH} // PUSH-POP work only on a whole method, they don't work locally for some reason
procedure TViewEndGame.Start;
var
  L: TCastleLabel;
begin
  inherited;
  InterceptInput := true;

  TScreenEffect.Create(FreeAtStop).Inject(Self);
  Music('captured');

  Sound('game_over');

  Timeout := Timer;

  (DesignedComponent('ButtonReturn') as TCastleButton).OnClick := @ClickReturn;

  (DesignedComponent('LabelEndGame') as TCastleLabel).CustomFont := FontBender90;

  L := DesignedComponent('LabelEndGame') as TCastleLabel;
  {$WARN 6018 OFF}
  case EndGameKind of
    egMental:
      case Rnd.Random(2) of
        0: L.Caption := Format('Barely able to understand what was going on around her, %s only helplessly felt being dragged somewhere. The sensation of cold metal cuffs locking around her wrists and ankles immediately returned %s to her senses... But it was too late to struggle. %s still did, as hard as she could.', [ViewGame.CurrentCharacter.Data.DisplayName, ViewGame.CurrentCharacter.Data.DisplayName, ViewGame.CurrentCharacter.Data.DisplayName]);
        1: L.Caption := Format('%s surrendered. She couldn''t take it anymore, and monsters seem to have understood and accepted that. Only when a metal yoke locked up around her neck and hands, she thought "what next?" But what was about to come was no longer in her control.', [ViewGame.CurrentCharacter.Data.DisplayName]);
      end;
    egTiedUp:
      case Rnd.Random(3) of
        0: L.Caption := Format('Tied up from hands to feet %s could no longer resist... or even move. She still was trying to prevent monsters from adding more and more layers of rope over her or at least crawl away. All of this was futile though and her fate was literally sealed in tight loops.', [ViewGame.CurrentCharacter.Data.DisplayName]);
        1: L.Caption := Format('The outcome of the fight was clear when %s''s hands were tied behind her back. Feet, elbows and knees soon followed until she was pulled into a tight hogtie, making sure she''ll stay put. A mask was forced onto %s''s head, completely blocking sight, smell and hearing. Soon her now hypersensitive body was completely prepared for long-term storage.', [ViewGame.CurrentCharacter.Data.DisplayName, ViewGame.CurrentCharacter.Data.DisplayName]);
        2: L.Caption := Format('%s lost balance and fell. Entangled in ropes, she couldn''t get up quickly and was only desperately jolting trying to keep the monster at distance. One missed kick was enough, and now she was played with like a toy as the slime was pulling and twisting ropes around her with agility and precision. The ropes squeezed her tighter as finally %s was lifted off the ground into suspension.', [ViewGame.CurrentCharacter.Data.DisplayName, ViewGame.CurrentCharacter.Data.DisplayName]);
      end;
    egHealth:
      case Rnd.Random(4) of
        0: L.Caption := Format('She regained consciousness naked in complete darkness and silence. As fragments of what has happened returned to her mind %s tried to jump on her feet, but her hands immediately pulled her back to the ground. Panic blocked her thoughts as %s was desperately trying to struggle limbs out of chains or at least pull down the blindfold which was just a centimeter out of her reach...', [ViewGame.CurrentCharacter.Data.DisplayName, ViewGame.CurrentCharacter.Data.DisplayName]);
        1: L.Caption := Format('%s woke up. Or did she? There was no difference between the dream and reality. She tried to move but her limbs didn''t respond. Or did they? %s realized she was suspended in some viscous substance that perfectly matched the temperature of her body, depriving her of any sense of touch. She quickly lost track of time and space, and even of her own identity, submerging into chaos of phantom sensations her isolated brain was desperately throwing at her.', [ViewGame.CurrentCharacter.Data.DisplayName, ViewGame.CurrentCharacter.Data.DisplayName, ViewGame.CurrentCharacter.Data.DisplayName]);
        2: L.Caption := Format('When %s came to her senses, she was compacted in embryo pose inside of a tiny cage. After some struggling she finally managed to squeeze her arms to reach her face, but only to find that the mask deafening and blindfolding her was too tight and too strong to remove with bare hands. %s tried to at least shift into a bit more comfortable pose, but to no avail. For how long will it last? She didn''t know and could only hope that dull pain in the back won''t get much worse.', [ViewGame.CurrentCharacter.Data.DisplayName, ViewGame.CurrentCharacter.Data.DisplayName]);
        3: L.Caption := Format('%s felt weightless... Suddenly she woke up to find herself floating a couple of meters above the ground. She started fiercely floundering trying to squirm out, but didn''t move an inch, only started slowly spinning around her hip. %s has heard of this kind of zero-g traps and was perfectly aware there was no way she could move her center of mass without help, yet it didn''t hold her from trying until giving up completely exhausted and disoriented.', [ViewGame.CurrentCharacter.Data.DisplayName, ViewGame.CurrentCharacter.Data.DisplayName]);
      end;
    egGlued:
      case Rnd.Random(3) of
        0: L.Caption := Format('As %s was still struggling to push the attacker off her, she suddenly felt unable to move at all. The weight was lifted off but her body was stuck in an inconvenient asymmetric pose inside a now solid plastic-like semi-transparent shell. All her strength was enough only to slightly flex the construction before it forced her back into the original pose %s was about to spend many hours or even days in...', [ViewGame.CurrentCharacter.Data.DisplayName, ViewGame.CurrentCharacter.Data.DisplayName]);
        1: L.Caption := Format('The glue suddenly solidified. %s was stuck in a very uncomfortable pose with her arms and legs still stretched in efforts of the recent struggle. Now she couldn''t even turn her head, able only to breathe and throw scared glances right and left trying not to think of what will happen to her next. And the wait was about to be a long one.', [ViewGame.CurrentCharacter.Data.DisplayName]);
        2: L.Caption := Format('The opponent finally pinned %s to the ground gaining a grip of her limbs and twisting them painfully. Suddenly it all stopped. %s tried to pull her hands and feet into a more comfortable position but to no avail. The sticky glue now became a solid transparent shell around her body holding it tight in an unnatural and extremely inconvenient pose. In panic %s significantly underestimated for how long she''ll be able to withstand the growing pain.', [ViewGame.CurrentCharacter.Data.DisplayName, ViewGame.CurrentCharacter.Data.DisplayName, ViewGame.CurrentCharacter.Data.DisplayName]);
      end;
    else
      ShowError('Unexpected end game state!');
  end;

  LabelReturn := DesignedComponent('LabelReturn') as TCastleLabel;
  LabelReturn.CustomFont := FontBender40;
  LabelReturn.Exists := false;
end;
{$POP}

procedure TViewEndGame.Update(const SecondsPassed: Single;
  var HandleInput: Boolean);
begin
  inherited;

  LabelReturn.Exists := Timeout.ElapsedTime > EndGameTimeout;
end;

function TViewEndGame.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;

  if Event.IsKey(keyEscape) then
    ClickReturn(Self);
end;

procedure TViewEndGame.ClickReturn(Sender: TObject);
begin
  if Timeout.ElapsedTime > EndGameTimeout then
  begin
    ViewGame.StartMusicForMap;
    Sound('menu_back');
    //TUiState.Current := StateMainMenu;
    Container.PopView(Self);
    ViewGame.UnPauseGame;
  end;
end;

end.


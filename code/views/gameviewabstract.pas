{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameViewAbstract;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  CastleUiControls, CastleKeysMouse;

type
  TViewAbstract = class(TCastleView)
  strict private
    procedure Screenshot;
  protected
    procedure BeforeScreenshot; virtual;
    procedure AfterScreenshot; virtual;
  public
    function Press(const Event: TInputPressRelease): Boolean; override;
  end;

var
  ScreenshotNow: Boolean;

implementation
uses
  GameConfiguration, GameWindow, GameSounds;

procedure TViewAbstract.Screenshot;
begin
  //Window.Invalidate; ---- not needed, Container.SaveScreen will call Render
  //Application.ProcessAllMessages; ---- not needed, Container.SaveScreen will call Render and no need in Update - we "update" everything in BeforeScreenshot
  BeforeScreenshot;
  Container.SaveScreen('screenshot_vinculike_' + FormatDateTime('yyyy-mm-dd-hh-mm-ss-zzz', Now) + '.png'); // also calls Render
  AfterScreenshot;
end;

procedure TViewAbstract.BeforeScreenshot;
begin
  // Force finish all pending tasks
  ScreenshotNow := true;
end;

procedure TViewAbstract.AfterScreenshot;
begin
  ScreenshotNow := false;
end;

function TViewAbstract.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited Press(Event);

  if Result then
    Exit;

  {$IFNDEF Mobile} // keyboard shortcuts don't work on mobile
  if Event.IsKey(keyF11) or (Event.IsKey(KeyEnter) and (mkAlt in Event.ModifiersDown)) then
  begin
    Window.FullScreen := not Window.FullScreen;
    Configuration.FullScreen := Window.FullScreen;
    Result := true;
  end;

  if Event.IsKey(keyF12) or Event.IsKey(keyPrintScreen) then
  begin
    Sound('screenshot');
    Screenshot;
    Result := true;
  end;
  {$ENDIF}
end;

end.


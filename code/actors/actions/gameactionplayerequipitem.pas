{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerEquipItem;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameSimpleSerializableObject,
  GameMapItem,
  GameActionAbstract;

type
  TActionPlayerEquipItem = class(TActionAbstract)
  public const
    Duration = Single(1);
  strict private
    Phase: Single;
    procedure Perform;
  public
    // WARNING!!! If different characters may use this in parallel - they may perform different actions on the same item, resulting in hard-to-find bugs
    MapItem: TMapItem;
  public
    procedure Save(const Element: TDOMElement); override;
    procedure AfterDeserealization; override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function ProposedTimeSpeed: Single; override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    procedure Stop; override;
  end;

  TActionPlayerEquipItemData = class(TActionAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameMap, GameLog, GameColors, GameEnchantmentAbstract,
  GameActor, GamePlayerCharacter, GameSounds, GameTranslation, GameApparelSlots;

{$DEFINE DataClass:=TActionPlayerEquipItemData}
{$INCLUDE actiontypecasts.inc}

procedure TActionPlayerEquipItem.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
  Element.AttributeSet('MapItem', MapItem.ReferenceId);
end;

procedure TActionPlayerEquipItem.AfterDeserealization;
begin
  inherited AfterDeserealization;
  //TODO: MapItem
end;

procedure TActionPlayerEquipItem.Load(const Element: TDOMElement);
begin
  inherited;
  Phase := Element.AttributeSingle('Phase');
  MapItem := ObjectByReferenceId(Element.AttributeQWord('MapItem')) as TMapItem; // MapItems are deserealized before Player characters, so this should be fine ====== TODO: Move into AfterDeserealization
end;

function TActionPlayerEquipItem.NoiseMultiplier: Single;
begin
  Exit(1.2);
end;

function TActionPlayerEquipItem.NoiseAddition: Single;
begin
  try
    {will crash if MapItem or MapItem.Item have been externally altered}
    Exit(MapItem.Item.ItemData.Noise * 2); // will result in 2x multiplier, item isn't in list yet
  except
    ShowError('Error in %s.NoiseAddition, action is no longer possible', [Self.ClassName]);
    ActionFinished;
    Exit(0.0);
  end;
end;

function TActionPlayerEquipItem.ProposedTimeSpeed: Single;
begin
  Exit(4.0);
end;

procedure TActionPlayerEquipItem.Start;
begin
  inherited Start;
  ShowLog(GetTranslation('ActorStartsEquippingItemLog'), [ParentPlayer.Data.DisplayName, MapItem.Item.Data.DisplayName, TActionPlayerEquipItem.Duration], ColorLogActionStart);
  MapItem.InteractingActorReferenceID := ParentActor.ReferenceId;
end;

procedure TActionPlayerEquipItem.Perform;
var
  E: TEnchantmentAbstract;
  A: TApparelSlot;
  SomethingIdentified: Boolean;
begin
  // if during equip - a bondage item got equipped or inventory unexpectedly changed otherwise
  for A in MapItem.Item.Data.EquipSlots do
    if (ParentPlayer.Inventory.Equipped[A] <> nil) and ParentPlayer.Inventory.Bondage[A] then
    begin
      ShowLog('%s is restrained and can no longer equip %s', [ParentActor.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogCancel);
      MapItem.InteractingActorReferenceID := 0;
      MapItem := nil;
      ActionFinished;
      Exit;
    end;

  ParentPlayer.Inventory.EquipItem(MapItem.Item);

  SomethingIdentified := false;
  if MapItem.Item.Enchantments <> nil then
    for E in MapItem.Item.Enchantments do
      if not E.Identified and E.EquipIdentifies and ParentPlayer.Inventory.EnchantmentRequirementsMet(E) then
      begin
        E.Identified := true;
        SomethingIdentified := true;
      end;

  if SomethingIdentified then
    ShowLog('Item enchantments became apparent', [], ColorUiItemStochastic);

  if MapItem.Item.IsBondage(ParentPlayer.Inventory) then
  begin
    ShowLog(GetTranslation('ActorEquipedBondageItemLog1'), [ParentActor.Data.DisplayName, MapItem.Item.Data.DisplayName, EquipSlotsToHumanReadableString(MapItem.Item.Data.EquipSlots)], ColorLogBondage);
    ShowLog(GetTranslation('ActorEquipedBondageItemLog2'), [ParentActor.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogBondage);
  end else
    ShowLog(GetTranslation('ActorEquipsItemLog'), [ParentActor.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogInventory);

  Sound(MapItem.Item.ItemData.SoundEquip);
  MapItem.Item := nil;
  Map.MapItemsList.Remove(MapItem);
  MapItem := nil;
end;

procedure TActionPlayerEquipItem.Update(const SecondsPassed: Single);
begin
  Phase += SecondsPassed;
  if Phase > Duration then
  begin
    Perform;
    ActionFinished;
    Exit;
  end
end;

class function TActionPlayerEquipItem.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionPlayerEquipItem).Phase := 0;
end;

procedure TActionPlayerEquipItem.Stop;
begin
  inherited;
  if MapItem <> nil then
  begin
    ShowLog(GetTranslation('ActorCancelEquipItemLog'), [ParentActor.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogCancel);
    MapItem.InteractingActorReferenceID := 0;
    MapItem := nil;
  end;
end;

{ TActionPlayerEquipItemData -------------------------------}

function TActionPlayerEquipItemData.Action: TActionClass;
begin
  Exit(TActionPlayerEquipItem);
end;

initialization
  RegisterSimpleSerializableObject(TActionPlayerEquipItem);
  RegisterSerializableData(TActionPlayerEquipItemData);

end.

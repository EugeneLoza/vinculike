{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionTrapMarkTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  GameActionAbstract, GameActionMarkTarget;

type
  TActionTrapMarkTarget = class(TActionMarkTarget)
  protected
    procedure MarkFinished; override;
  public
    function NoiseMultiplier: Single; override;
    procedure Start; override;
  end;

  TActionTrapMarkTargetData = class(TActionMarkTargetData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  GameSimpleSerializableObject, GameSerializableData,
  GameActor, GameLog, GameTranslation, GameColors, GameSounds;

{$DEFINE DataClass:=TActionTrapMarkTargetData}
{$INCLUDE actiontypecasts.inc}

function TActionTrapMarkTarget.NoiseMultiplier: Single;
begin
  //Result := inherited NoiseMultiplier;
  Exit(1.0);
end;

procedure TActionTrapMarkTarget.Start;
begin
  inherited Start;
  Sound('trap_trigger');
  ShowLog(GetTranslation('TrapTriggered'), [TargetActor.Data.DisplayName], ColorLogTrap);
end;

procedure TActionTrapMarkTarget.MarkFinished;
begin
  ParentActor.Health := -1;
  ParentActor.ForceResetToIdle;
  inherited;
end;

{ TActionTrapMarkTargetData -------------------------------}

function TActionTrapMarkTargetData.Action: TActionClass;
begin
  Exit(TActionTrapMarkTarget);
end;

initialization
  RegisterSimpleSerializableObject(TActionTrapMarkTarget);
  RegisterSerializableData(TActionTrapMarkTargetData);

end.


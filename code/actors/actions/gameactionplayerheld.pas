{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerHeld;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameActionAbstract, GameActionPlayerStunnedAbstract;

type
  TActionPlayerHeld = class(TActionPlayerStunnedAbstract)
  protected
    procedure CannotStopStunned; override;
    procedure StopStunned; override;
    procedure Load(const Element: TDOMElement); override;
  public
    HeldBy: UInt64;
    Resistance: Single;
    procedure Save(const Element: TDOMElement); override;
  end;

  TActionPlayerHeldData = class(TActionAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GamePlayerCharacter, GameLog, GameColors;

{$DEFINE DataClass:=TActionPlayerHeldData}
{$INCLUDE actiontypecasts.inc}

procedure TActionPlayerHeld.CannotStopStunned;
begin
  ShowLog('%s desperately struggles but the monster holds her too tight', [ParentPlayer.Data.DisplayName], ColorLogNotEnoughStamina);
end;

procedure TActionPlayerHeld.StopStunned;
begin
  //ShowLog('%s finally manages to break free', [ParentPlayer.Data.DisplayName], ColorLogCancel);
  // nothing, let the holding opponent care for logs, as this message is shown every time player is grabbed again
  // Yes, this is not safe, let's see if it'll work
end;

procedure TActionPlayerHeld.Load(const Element: TDOMElement);
begin
  inherited Load(Element);
  HeldBy := Element.AttributeQWord('HeldBy');
  Resistance := Element.AttributeSingleDef('Resistance', 0);
end;

procedure TActionPlayerHeld.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('HeldBy', HeldBy);
  Element.AttributeSet('Resistance', Resistance);
end;

{ TActionPlayerHeldData -------------------------------}

function TActionPlayerHeldData.Action: TActionClass;
begin
  Exit(TActionPlayerHeld);
end;

initialization
  RegisterSimpleSerializableObject(TActionPlayerHeld);
  RegisterSerializableData(TActionPlayerHeldData);

end.


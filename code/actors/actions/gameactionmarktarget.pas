{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionMarkTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  GameActionAbstract, GameMarkAbstract, GameActionOnTarget,
  GameUnlockableEntry;

type
  TActionMarkTarget = class(TActionOnTarget)
  strict private
    Phase: Single;
    IsWarmUp: Boolean;
    Mark: TMarkAbstract;
    procedure Perform;
  protected
    procedure MarkFinished; virtual;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function CanStop: Boolean; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    destructor Destroy; override;
  end;

  TActionMarkTargetData = class(TActionOnTargetData)
  public
    function Description: TEntriesList; override;
    function Action: TActionClass; override;
  end;

implementation
uses
  GameSimpleSerializableObject, GameSerializableData,
  GameActor, GameMonsterData, GameMap, GameRandom, GameDifficultyLevel;

{$DEFINE DataClass:=TActionMarkTargetData}
{$INCLUDE actiontypecasts.inc}

procedure TActionMarkTarget.MarkFinished;
begin
  Mark := nil;
  {if not TargetActor.CanAct then
  begin
    OnFinished;
    Exit;
  end; } // This causes a SIGSEGV --- WHY???????????????
end;

function TActionMarkTarget.NoiseMultiplier: Single;
begin
  Exit(1.1);
end;

function TActionMarkTarget.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionMarkTarget.CanStop: Boolean;
begin
  Exit(IsWarmUp);
end;

procedure TActionMarkTarget.Perform;
const
  MaxRetryCount = Integer(1000);
var
  RetryCount: Integer;
  SX, SY: Single;
begin
  if ParentActor.Data is TMonsterData then
  begin
    Mark := ActionData.MarkData.Mark.Create as TMarkTargetAbstract;
    TMarkTargetAbstract(Mark).Target := TargetActor;

    Mark.Parent := ParentActor;
    Mark.Data := ActionData.MarkData;
    Mark.SetSize(Mark.Data.Size);

    RetryCount := 0;
    repeat
      repeat
        SX := TargetActor.CenterX + (Rnd.Random - 0.5) * (TargetActor.Data.Size + Mark.Size);
        SY := TargetActor.CenterY + (Rnd.Random - 0.5) * (TargetActor.Data.Size + Mark.Size);
      until (SX - Mark.HalfSize > 0.5) and (SY - Mark.HalfSize > 0.5) and (SX + Mark.HalfSize < Map.SizeX - 0.5) and (SY + Mark.HalfSize < Map.SizeY - 0.5);
      Inc(RetryCount);
    until ((ParentActor.DistanceToSqr(SX, SY) <= Mark.Data.SqrRange) and (TargetActor.DistanceToSqr(SX, SY) <= Sqr(Mark.HalfSize))) or (RetryCount > MaxRetryCount);
    Mark.MoveCenterTo(SX, SY);
    Mark.OnFinished := @MarkFinished;
    Map.MarksList.Add(Mark);
  end {else
  begin
    Weapon := TPlayerCharacter(Parent).Equipped[esWeapon];
    if Weapon <> nil then
    begin
      Sound(Weapon.Data.SoundAttack);
      PlayerDamage := Weapon.Data.Damage;
      TPlayerCharacter(Parent).DamageItem(Weapon, 1 + Rnd.Random * 2);
    end else
    begin
      Sound('hit_punch');
      PlayerDamage := 5;
    end;
    if Parent = StateGame.CurrentCharacter then
      StateGame.PlayerLog(ActorAttacksTargetLog, [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, PlayerDamage], CastleColors.White);
    TargetActor.Hit(PlayerDamage);
    (TargetActor as TMonster).ChasePlayer;
  end};
end;

procedure TActionMarkTarget.Update(const SecondsPassed: Single);
begin
  inherited;

  if not TargetActor.CanAct then //CanBeInteractedWith : TODO
  begin
    ActionFinished;
    Exit;
  end;

  Phase += SecondsPassed;
  if IsWarmUp and (Phase >= ActionData.WarmUpTime * Difficulty.MonsterAttackDelayMultiplier) then //TODO: multiplier only for monsters
  begin
    { Ok, so this is VERY broken here
      We receive Perform before the Mark gone nil
      Especially if the SecondsPassed > ActionData.WarmUpTime + ActionData.CoolDownTime
      Which throws multiple marks, which freed at unexpected moments
      Here the workaround introduces another bug - enemy skips attacks,
      Still better than crashing :D }
    if Mark = nil then
      Perform;
    Phase -= ActionData.WarmUpTime * Difficulty.MonsterAttackDelayMultiplier;
    IsWarmUp := false;
  end else
  if not IsWarmUp and (Phase >= ActionData.CoolDownTime * Difficulty.MonsterAttackDelayMultiplier) then //TODO: multiplier only for monsters
  begin
    Phase -= ActionData.CoolDownTime * Difficulty.MonsterAttackDelayMultiplier;
    IsWarmUp := true;
  end;
end;

class function TActionMarkTarget.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionMarkTarget).Phase := 0;
  (Result as TActionMarkTarget).IsWarmUp := true;
end;

destructor TActionMarkTarget.Destroy;
begin
  if Mark <> nil then
  begin
    Mark.OnFinished := nil;
    Mark.EndAction;
  end;
  inherited Destroy;
end;

{ TActionMarkTargetData -------------------------------}

function TActionMarkTargetData.Description: TEntriesList;
var
  ActionEntries: TEntriesList;
begin
  Result := inherited Description;
  ActionEntries := MarkData.Description;
  Result.AddRange(ActionEntries);
  FreeAndNil(ActionEntries);
end;

function TActionMarkTargetData.Action: TActionClass;
begin
  Exit(TActionMarkTarget);
end;

initialization
  RegisterSimpleSerializableObject(TActionMarkTarget);
  RegisterSerializableData(TActionMarkTargetData);

end.


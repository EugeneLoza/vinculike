{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerStunnedAbstract;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameActionAbstract;

type
  TActionPlayerStunnedAbstract = class abstract(TActionAbstract)
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function CanStop: Boolean; override;
    function Immobilized: Boolean; override;
  protected
    procedure CannotStopStunned; virtual; abstract;
    procedure StopStunned; virtual; abstract;
  public
    Phase: Single;
    Duration: Single;
  public
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    class function NewAction(const AParent: TObject; const StunDuration: Single): TActionAbstract;
    procedure Stop; override;
  end;

  TActionPlayerStunnedAbstractData = class(TActionAbstractData)
  public
  end;

implementation
uses
  CastleXmlUtils,
  GamePlayerCharacter;

{$DEFINE DataClass:=TActionPlayerStunnedAbstractData}
{$INCLUDE actiontypecasts.inc}

procedure TActionPlayerStunnedAbstract.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
end;

procedure TActionPlayerStunnedAbstract.Load(const Element: TDOMElement);
begin
  inherited Load(Element);
  Phase := Element.AttributeSingle('Phase');
end;

function TActionPlayerStunnedAbstract.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionPlayerStunnedAbstract.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionPlayerStunnedAbstract.CanStop: Boolean;
begin
  // don't use inherited
  Result := false;
  CannotStopStunned;
end;

function TActionPlayerStunnedAbstract.Immobilized: Boolean;
begin
  Exit(true);
end;

procedure TActionPlayerStunnedAbstract.Update(const SecondsPassed: Single);
begin
  //inherited; // abstract
  Phase += SecondsPassed;
  if Phase > Duration then
  begin
    ActionFinished;
    Exit;
  end
end;

class function TActionPlayerStunnedAbstract.NewAction(
  const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionPlayerStunnedAbstract).Phase := 0;
end;

class function TActionPlayerStunnedAbstract.NewAction(
  const AParent: TObject; const StunDuration: Single): TActionAbstract;
begin
  Result := NewAction(AParent);
  (Result as TActionPlayerStunnedAbstract).Phase := 0;
  (Result as TActionPlayerStunnedAbstract).Duration := StunDuration;
end;

procedure TActionPlayerStunnedAbstract.Stop;
begin
  inherited;
  StopStunned;
end;

{ TActionPlayerStunnedAbstractData -------------------------------}

// nothing

end.


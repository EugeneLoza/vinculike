{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Basic monster ai, can walk around randomly ot curiously, guard/soft-guard,
  alert other monsters of player character presence and attack }
unit GameAiBasic;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  GameAiAbstract, GameAiSingleActionAbstract,
  GameUnlockableEntry;

type
  TAiBasic = class(TAiSingleActionAbstract)
  strict private
    IdleWait: Single;
    IdleTimeout: Single;
  public
    procedure Update(const SecondsPassed: Single); override;
  end;

  TAiBasicData = class(TAiSingleActionAbstractData)
  protected
    //procedure Validate; override;
    //procedure Read(const Element: TDOMElement); override;
  public
    // Curious: Boolean; ---> CanGuard: Boolean;
    function Description: TEntriesList; override;
    function Ai: TAiClass; override;
  end;

implementation
uses
  GameSimpleSerializableObject, GameSerializableData,
  GameRandom, GameMap,  GameMonster, GameActor,
  GameActionIdle, GameActionMoveAbstract, GameActionMoveAndActTarget,
  GameVinculopediaEntryHeader, GameVinculopediaEntryText;

{$DEFINE DataClass:=TAiBasicData}
{$INCLUDE aitypecasts.inc}

procedure TAiBasic.Update(const SecondsPassed: Single);

  procedure DoIdle;
  begin
    if IdleWait = 0 then
    begin
      if Guard or SoftGuard then
        IdleTimeout := (Rnd.Random + 0.5) * 6.0
      else
      if ParentMonster.MonsterData.CuriousAi then
        IdleTimeout := (Rnd.Random + 0.5) * 13.0 * (1 + 120.0 / (Map.TimeSpentOnTheMap + 120.0))
      else
        IdleTimeout := (Rnd.Random + 0.5) * 8.0 * (1 + 120.0 / (Map.TimeSpentOnTheMap + 120.0));
    end;
    IdleWait += SecondsPassed;
    AiFlee := false;
    AiState := asIdle;
    if IdleWait > IdleTimeout then
    begin
      if Guard or SoftGuard then
        DoReturnToGuard
      else
      if ParentMonster.MonsterData.CuriousAi then
        DoWalkCurious
      else
        DoWalkRandomly;
    end;
  end;

  procedure DoAlert;
  var
    SqrDistanceToPlayer: Single;
  begin
    SqrDistanceToPlayer := Sqr(ParentMonster.CurrentTarget.CenterX - ParentMonster.CenterX) + Sqr(ParentMonster.CurrentTarget.CenterY - ParentMonster.CenterY);
    // if player is close enough attack and alert others to join attack
    if not (ParentActor.CurrentAction is TActionMoveAndActTarget) and (SqrDistanceToPlayer < Sqr(ParentMonster.VisibilityRange / 2)) and ParentMonster.LineOfSight(ParentMonster.CurrentTarget) then
      DoChasePlayerAndAlertNearbyToChase
    else
    // if player is away but visible, investigate and alert otherst to investigate
    if (SqrDistanceToPlayer < Sqr(ParentMonster.VisibilityRange)) and ParentMonster.LineOfSight(ParentMonster.CurrentTarget) then
      DoInvestigateNoiseAndAlertNearbyToInvestigate
    else
    if not (ParentMonster.CurrentAction is TActionMoveAndActTarget) and (SqrDistanceToPlayer < Sqr(ParentMonster.VisibilityRange / 3)) then
      InvestigateNoise(ParentMonster.CurrentTarget.CenterX, ParentMonster.CurrentTarget.CenterY)
    else
    // if Player is not visible, then go idlea after timeout
    if not (ParentMonster.CurrentAction is TActionIdle) then
    begin
      Timeout -= SecondsPassed;
      if Timeout < 0 then
      begin
        ParentMonster.CurrentAction := TActionIdle.NewAction(ParentActor);
        ParentMonster.CurrentAction.Start;
        AiState := asIdle;
      end;
    end;
  end;

begin
  if AiFlee and not (ParentActor.CurrentAction is TActionMoveAbstract) and not (ParentActor.CurrentAction is TActionIdle) then
    DoFlee;
  if not AiFlee then
    DoAlert;
  if ParentActor.CurrentAction is TActionIdle then
    DoIdle
  else
    IdleWait := 0;
end;

{ TAiBasicData ------------------------------------------ }

function TAiBasicData.Description: TEntriesList;
begin
  //Result := inherited Description; it's nil
  Result := ActionData.Description;
  //If curiousAI
  Result.Insert(0, NewEntryText('Generic monster that will walk around aimlessly. If gets suspicious will investigate the source of the noise. If spots the character or is alerted by monsters nearby will try to close in and attack the target.', '', 0));
  Result.Insert(0, NewEntryText('In attack mode will chase the character for 10 seconds before giving up.', '', 0));
  Result.Insert(0, NewEntryHeader('Behavior:', '', 0));
end;

function TAiBasicData.Ai: TAiClass;
begin
  Exit(TAiBasic);
end;

initialization
  RegisterSimpleSerializableObject(TAiBasic);
  RegisterSerializableData(TAiBasicData);

end.


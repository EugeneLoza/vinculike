{$IFDEF SafeTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$DEFINE AiData:=(Data as DataClass)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$DEFINE AiData:=DataClass(Data)}
{$ENDIF}

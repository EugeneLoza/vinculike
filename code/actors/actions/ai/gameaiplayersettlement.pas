{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Dummy player character behavior in the settlement:
  walk around randomly, sometimes go and "greet" others
  except current character
  Todo: bug, sometimes characters can decide to
  "run around in circles" chasing each other trying to greet and creating a loop }
unit GameAiPlayerSettlement;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameAiAbstract;

type
  TAiPlayerSettlement = class(TAiAbstract)
  private
    IdleWait: Single;
    IdleTimeout: Single;
  public
    procedure OnHit(const ATimeout: Single); override;
    procedure ChasePlayer(const ATimeout: Single); override;
    procedure InvestigateNoise(const AX, AY: Single); override;
  public
    procedure Update(const SecondsPassed: Single); override;
  end;

  TAiPlayerSettlementData = class(TAiAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
   function Ai: TAiClass; override;
  end;

implementation
uses
  GameSimpleSerializableObject, GameSerializableData,
  GameActor, GameMap, GameActionIdle,
  TempData,
  GameViewGame, GameRandom;

{$DEFINE DataClass:=TAiPlayerSettlementData}
{$INCLUDE aitypecasts.inc}

procedure TAiPlayerSettlement.OnHit(const ATimeout: Single);
begin
  // do nothing
end;

procedure TAiPlayerSettlement.ChasePlayer(const ATimeout: Single);
begin
  // do nothing
end;

procedure TAiPlayerSettlement.InvestigateNoise(const AX, AY: Single);
begin
  // do nothing
end;

procedure TAiPlayerSettlement.Update(const SecondsPassed: Single);

  procedure DoWalkRandomlyAvoidExit;
  var
    TX, TY: Int16;
  begin
    repeat
      TX := Rnd.Random(Map.SizeX);
      TY := Rnd.Random(Map.SizeY);
    until Map.PassableTiles[ParentActor.PredSize][TX + Map.SizeX * TY] and (Sqr(TX - Map.ExitX) + Sqr(TY - Map.ExitY) > Sqr(4));
    ParentActor.MoveTo(TX, TY);
    IdleTimeout := Rnd.Random * 10 + 2;
    IdleWait := 0;
  end;

  procedure GoAndChat;
  var
    Target: TActor;
  begin
    repeat
      Target := Map.CharactersOnThisLevel[Rnd.Random(Map.CharactersOnThisLevel.Count)];
    until (Target <> Parent) and (Target <> ViewGame.CurrentCharacter);
    ParentActor.MoveAndAct(Target, PlayerActionChat);
    IdleWait := 0;
  end;

begin
  if ParentActor.CurrentAction is TActionIdle then
  begin
    IdleWait += SecondsPassed;
    if IdleTimeout = 0 then
      IdleTimeout := Rnd.Random * 12;
    // TODO: for some reason AI players also use PlayerPassableTiles instead of PassableTiles - resulting in errors.
    if IdleWait > IdleTimeout then
      if (Map.CharactersOnThisLevel.Count < 4) or Rnd.RandomBoolean then
        DoWalkRandomlyAvoidExit
      else
        GoAndChat;
  end;
end;

{ TAiPlayerSettlementData ------------------------------------------ }

procedure TAiPlayerSettlementData.Validate;
begin
  //do nothing
end;

procedure TAiPlayerSettlementData.Read(const Element: TDOMElement);
begin
  //do nothing
end;

function TAiPlayerSettlementData.Ai: TAiClass;
begin
  Exit(TAiPlayerSettlement);
end;

initialization
  RegisterSimpleSerializableObject(TAiPlayerSettlement);
  RegisterSerializableData(TAiPlayerSettlementData);

end.


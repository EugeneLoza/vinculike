{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Doesn't do anything until attacked,
  won't alert other monsters and will fight back if touched }
unit GameAiMimic;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  GameAiAbstract, GameAiSingleActionAbstract,
  GameUnlockableEntry;

type
  TAiMimic = class(TAiSingleActionAbstract)
  public
    procedure ChasePlayer(const ATimeout: Single); override;
    procedure InvestigateNoise(const AX, AY: Single); override;
  public
    procedure Update(const SecondsPassed: Single); override;
  end;

  TAiMimicData = class(TAiSingleActionAbstractData)
  protected
    //procedure Validate; override;
    //procedure Read(const Element: TDOMElement); override;
  public
    // Curious: Boolean; ---> CanGuard: Boolean;
    function Description: TEntriesList; override;
    function Ai: TAiClass; override;
  end;

implementation
uses
  GameSimpleSerializableObject, GameSerializableData,
  GameMonster, GameActor,
  GameActionIdle,
  GameVinculopediaEntryHeader, GameVinculopediaEntryText;

{$DEFINE DataClass:=TAiMimicData}
{$INCLUDE aitypecasts.inc}

procedure TAiMimic.ChasePlayer(const ATimeout: Single);
begin
  // Do nothing
end;

procedure TAiMimic.InvestigateNoise(const AX, AY: Single);
begin
  // Do nothing
end;

procedure TAiMimic.Update(const SecondsPassed: Single);
begin
  if not (ParentActor.CurrentAction is TActionIdle) then
  begin
    // Mimic attacks the target - try timeouting it
    Timeout -= SecondsPassed / 2;
    if Timeout < 0 then
    begin
      ParentMonster.CurrentAction := TActionIdle.NewAction(ParentActor);
      ParentMonster.CurrentAction.Start;
      AiState := asIdle;
    end;
  end;
end;

{ TAiMimicData ------------------------------------------ }

function TAiMimicData.Description: TEntriesList;
begin
  //Result := inherited Description; it's nil
  Result := ActionData.Description;
  Result.Insert(0, NewEntryText('Mimics remain passive until the character tries to interact with it. When activated will pursue the target until killed or the character teleports away.', '', 0));
  Result.Insert(0, NewEntryHeader('Behavior:', '', 0));
end;

function TAiMimicData.Ai: TAiClass;
begin
  Exit(TAiMimic);
end;

initialization
  RegisterSimpleSerializableObject(TAiMimic);
  RegisterSerializableData(TAiMimicData);

end.


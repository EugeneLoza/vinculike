{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ The most common monster ai that will actively attack the target:
  will come to action.range and execute the (single) action
  Currently it is the most default behavior for normal hostile monsters }
unit GameAiSingleActionAbstract;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameAiAbstract, GameActionAbstract;

type
  TAiSingleActionAbstract = class abstract(TAiAbstract)
  strict private
    procedure ChasePlayerCore(const ATimeout: Single);
  public
    procedure OnHit(const ATimeout: Single); override;
    procedure ChasePlayer(const ATimeout: Single); override;
    procedure InvestigateNoise(const AX, AY: Single); override;
  end;

  TAiSingleActionAbstractData = class abstract(TAiAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    ActionData: TActionAbstractData;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameLog, GameActor, GameMonster, GameActionMoveAbstract, GameMap, GameRandom,
  GameViewGame;

{$DEFINE DataClass:=TAiSingleActionAbstractData}
{$INCLUDE aitypecasts.inc}

procedure TAiSingleActionAbstract.ChasePlayerCore(const ATimeout: Single);
begin
  if not ParentActor.CanAct then
  begin
    ShowError('%s received ChasePlayer but it is dead.', [ParentActor.Data.Id]);
    Exit;
  end;
  SoftGuard := false;
  AiState := asAttack;
  ParentActor.MoveAndAct(ParentMonster.CurrentTarget, AiData.ActionData);
  Timeout := ATimeout;
end;

procedure TAiSingleActionAbstract.OnHit(const ATimeout: Single);
begin
  ChasePlayerCore(ATimeout);
end;

procedure TAiSingleActionAbstract.ChasePlayer(const ATimeout: Single);
begin
  if (AiState <> asAttack) and not ParentMonster.MonsterData.IsInvisible {and ParentActor.IsVisible} then
    ViewGame.SoundMonsterAttack;
  ChasePlayerCore(ATimeout);
end;

procedure TAiSingleActionAbstract.InvestigateNoise(const AX, AY: Single);
const
  MaxRetryCount = 100;
var
  SX, SY: Single;
  RetryCount: Integer;
begin
  if not ParentActor.CanAct then
  begin
    ShowError('%s received InvestigateNoise but it is dead.', [ParentActor.Data.Id]);
    Exit;
  end;
  if ParentActor.Unsuspecting and not AiFlee then // Todo: something smarter here
  begin
    if (AiState = asIdle) and not ParentMonster.MonsterData.IsInvisible {and ParentActor.IsVisible} then
      ViewGame.SoundMonsterInvestigate;
    AiState := asInvestigate;
    RetryCount := 0;
    repeat
      Inc(RetryCount);
      SX := AX + 2.0 * Rnd.Random - 1.0;
      SY := AY + 2.0 * Rnd.Random - 1.0;
    until ((Trunc(SX) > 0) and (Trunc(SY) > 0) and (Trunc(SX) < Map.PredSizeX) and (Trunc(SY) < Map.PredSizeY) and Map.PassableTiles[ParentActor.PredSize][Trunc(SX) + Map.SizeX * Trunc(SY)]) or (RetryCount > MaxRetryCount);
    if RetryCount > MaxRetryCount then
      ParentActor.MoveTo(AX, AY)
    else
      ParentActor.MoveTo(SX, SY);
    Timeout := (ParentActor.CurrentAction as TActionMoveAbstract).RemainingTime;
  end;
end;

{ TAiSingleActionAbstractData ------------------------------------------ }

procedure TAiSingleActionAbstractData.Validate;
begin
  //do nothing
end;

procedure TAiSingleActionAbstractData.Read(const Element: TDOMElement);
begin
  ActionData := TActionAbstractData.ReadClass(FBaseUrl, Element.Child('Action')) as TActionAbstractData;
end;

destructor TAiSingleActionAbstractData.Destroy;
begin
  FreeAndNil(ActionData);
  inherited Destroy;
end;

end.


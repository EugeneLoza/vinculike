{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Vacuum cleaner ignores player character and moves from item to item,
  picking those up and selecting next nearest target
  will attack only if attacked
  if no items left to pick up, will walk around randomly}
unit GameAiVacuumCleaner;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  GameAiAbstract, GameAiSingleActionAbstract,
  GameUnlockableEntry;

type
  TAiVacuumCleaner = class(TAiSingleActionAbstract)
  strict private const AdditionalMargin = Integer(1);
  public
    procedure ChasePlayer(const ATimeout: Single); override;
    procedure InvestigateNoise(const AX, AY: Single); override;
    { OnHit governs attacking player character }
  public
    procedure Update(const SecondsPassed: Single); override;
  end;

  TAiVacuumCleanerData = class(TAiSingleActionAbstractData)
  protected
    //procedure Validate; override;
    //procedure Read(const Element: TDOMElement); override;
  public
    function Description: TEntriesList; override;
    function Ai: TAiClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData, GameMath,
  GameViewGame, GameMap, GameMapTypes, GameActor, GameMonster, GameMapItem,
  GameActionIdle, GameActionMoveAbstract, GameLog,
  GameVinculopediaEntryHeader, GameVinculopediaEntryText;

{$DEFINE DataClass:=TAiVacuumCleanerData}
{$INCLUDE aitypecasts.inc}

procedure TAiVacuumCleaner.ChasePlayer(const ATimeout: Single);
begin
  // Do nothing
end;

procedure TAiVacuumCleaner.InvestigateNoise(const AX, AY: Single);
begin
  // Do nothing
end;

procedure TAiVacuumCleaner.Update(const SecondsPassed: Single);
var
  I: Integer;
  Item: TMapItem;
  LastTargetDist: TDistanceQuant;
  DebugTargetFound: Boolean;
  TargetX, TargetY: Integer;
  Dist: TDistanceMapArray;

  procedure TryFindTheNearestSpotForThisItem;

    procedure TrySelectMoveTarget(const ToX, ToY: Integer);
    var
      NewTarget: SizeInt;
    begin
      NewTarget := ToX + ToY * Map.SizeX;
      if Map.PassableTiles[ParentActor.PredSize][NewTarget] and (Dist[NewTarget] < LastTargetDist) then
      begin
        LastTargetDist := Dist[NewTarget];
        TargetX := ToX;
        TargetY := ToY;
        DebugTargetFound := true;
      end;
    end;

  var
    IX, IY: Integer;
  begin
    for IX := MaxInteger(Item.LastTileX - ParentActor.PredSize - AdditionalMargin, 1) to Item.LastTileX + AdditionalMargin do
      for IY := MaxInteger(Item.LastTileY - ParentActor.PredSize - AdditionalMargin, 1) to Item.LastTileY + AdditionalMargin do
        TrySelectMoveTarget(IX, IY);
  end;

begin
  // TODO: all characters (or can't sleep when teamed missions altogether?)
  if ParentActor.Collides(ViewGame.CurrentCharacter, 1) then
    ViewGame.WakeUp(false, false);
  // Warning: VacuumCleaner doesn't timeout, so will chase the player forever until teleported, maybe good
  if ParentActor.CurrentAction is TActionIdle then
  begin
    //pick up all items
    I := 0;
    while I < Map.MapItemsList.Count do
    begin
      if ParentActor.Collides(Map.MapItemsList[I], AdditionalMargin) and (Map.MapItemsList[I].InteractingActorReferenceID = 0) then
      begin
        ParentMonster.Loot.Add(Map.MapItemsList[I].Item);
        Map.MapItemsList[I].Item := nil;
        Map.MapItemsList.Delete(I);
      end else
        Inc(I);
    end;
    if ParentActor.Collides(ViewGame.CurrentCharacter, 1) then
      ViewGame.InvalidatePosition(ViewGame.CurrentCharacter);
    //choose what to do next
    if Map.MapItemsList.Count > 0 then
    begin
      AiState := asIdle;
      Dist := Map.DistanceMap(ParentActor, true);
      LastTargetDist := High(TDistanceQuant);
      DebugTargetFound := false;
      for Item in Map.MapItemsList do
        TryFindTheNearestSpotForThisItem;
      if DebugTargetFound then
        ParentActor.MoveTo(TargetX, TargetY)
      else
      begin
        ShowError('There are items on the map, but Vacuum cleaner failed to reach any of them');
        DoWalkRandomly;
      end;
      //MoveAndAct(); for now aimed at TActor derivatives, so unusable, yet
    end else
      DoWalkRandomly;
  end else
  if not (ParentActor.CurrentAction is TActionMoveAbstract) then
  begin
    // Vacuum cleaner attacks the target - try timeouting it
    Timeout -= SecondsPassed / 2;
    if Timeout < 0 then
    begin
      ParentMonster.CurrentAction := TActionIdle.NewAction(ParentActor);
      ParentMonster.CurrentAction.Start;
      AiState := asIdle;
    end;
  end;
end;

{ TAiVacuumCleanerData ------------------------------------------ }

function TAiVacuumCleanerData.Description: TEntriesList;
begin
  //Result := inherited Description; it's nil
  Result := ActionData.Description;
  Result.Insert(0, NewEntryText('Looks for and picks up items around the map. Is not interesed in items in chests or equipped ones. Non-aggressive until provoked.', '', 0));
  Result.Insert(0, NewEntryHeader('Behavior:', '', 0));
end;

function TAiVacuumCleanerData.Ai: TAiClass;
begin
  Exit(TAiVacuumCleaner);
end;

initialization
  RegisterSimpleSerializableObject(TAiVacuumCleaner);
  RegisterSerializableData(TAiVacuumCleanerData);

end.


{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Doesn't do anything at all }
unit GameAiNone;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameAiAbstract;

type
  TAiNone = class(TAiAbstract)
  public
    procedure OnHit(const ATimeout: Single); override;
    procedure ChasePlayer(const ATimeout: Single); override;
    procedure InvestigateNoise(const AX, AY: Single); override;
  public
    procedure Update(const SecondsPassed: Single); override;
  end;

  TAiNoneData = class(TAiAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
   function Ai: TAiClass; override;
  end;

implementation
uses
  GameSimpleSerializableObject, GameSerializableData;

procedure TAiNone.OnHit(const ATimeout: Single);
begin
  // do nothing
end;

procedure TAiNone.ChasePlayer(const ATimeout: Single);
begin
  // do nothing
end;

procedure TAiNone.InvestigateNoise(const AX, AY: Single);
begin
  // do nothing
end;

procedure TAiNone.Update(const SecondsPassed: Single);
begin
  // do nothing
end;

{ TAiNoneData ------------------------------------------ }

procedure TAiNoneData.Validate;
begin
  //do nothing
end;

procedure TAiNoneData.Read(const Element: TDOMElement);
begin
  //do nothing
end;

function TAiNoneData.Ai: TAiClass;
begin
  Exit(TAiNone);
end;

initialization
  RegisterSimpleSerializableObject(TAiNone);
  RegisterSerializableData(TAiNoneData);

end.


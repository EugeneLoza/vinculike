{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionOnTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameActionAbstract;

type
  TActionOnTarget = class abstract(TActionAbstract)
  strict private
    TargetReferenceId: UInt64;
  public
    Data: TObject;
    Target: TObject;
  public
    procedure Save(const Element: TDOMElement); override;
    procedure AfterDeserealization; override;
  protected
    procedure Load(const Element: TDOMElement); override;
  end;

  TActionOnTargetData = class abstract(TActionAbstractData)
    // nothing special here for now. Maybe valid targets types?
  end;

implementation
uses
  GameSerializableObject,
  CastleXmlUtils;

procedure TActionOnTarget.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Target', (Target as TSerializableObject).ReferenceId);
  //Element.AttributeSet('Data', Data.Id);
end;

procedure TActionOnTarget.AfterDeserealization;
begin
  inherited AfterDeserealization;
  Target := ObjectByReferenceId(TargetReferenceId);
end;

procedure TActionOnTarget.Load(const Element: TDOMElement);
begin
  inherited;
  TargetReferenceId := Element.AttributeQWord('Target'); // we cannot get the reference just now, as the target may be lower in load order than Parent of this Action. We'll try to recover the Target at the nearest Update
  Target := nil; // paranoid
  //Data := SomeActionDataContainer[Element.AttributeString('Data', Data.Id)];
  raise EActionLoadNotImplemented.Create('Loading TActionOnTarget is not implemented yet as the action data is not serialized!');
end;

end.


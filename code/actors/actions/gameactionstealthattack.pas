{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionStealthAttack;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  CastleVectors,
  GameSimpleSerializableObject,
  GameActor, GameActionAbstract, GameActionRoll;

type
  TActionStealthAttack = class(TActionRoll)
  strict private
    procedure Perform;
  public
    Target: TActor;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    function CanStop: Boolean; override;
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single); override;
  end;

  TActionStealthAttackData = class(TActionRollData)
  public
    function Action: TActionClass; override;
  end;


implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameTranslation, GamePlayerCharacter,
  GameApparelSlots, GameMonster, GameMath;

{$DEFINE DataClass:=TActionStealthAttackData}
{$INCLUDE actiontypecasts.inc}

procedure TActionStealthAttack.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Target', Target.ReferenceId);
end;

procedure TActionStealthAttack.Load(const Element: TDOMElement);
begin
  inherited;
  Target := ObjectByReferenceId(Element.AttributeQWord('Target')) as TActor; // TMonsters are deserealized before Player characters, so this should be fine
end;

function TActionStealthAttack.CanStop: Boolean;
begin
  //inherited - no need;
  Exit(false); // cannot stop stealth attack until it hits the target, then it'll stop by itself
end;

function TActionStealthAttack.NoiseMultiplier: Single;
begin
  //Result := inherited NoiseMultiplier;
  Exit(0.9);
end;

function TActionStealthAttack.NoiseAddition: Single;
begin
  if ParentPlayer.Inventory.Equipped[esWeapon] <> nil then
    Exit(ParentPlayer.Inventory.GetItemNoiseSafe(esWeapon))
  else
    Exit(0.5);
end;

procedure TActionStealthAttack.Start;
begin
  inherited Start;
  MoveVector := Vector2(Target.CenterX - ParentActor.CenterX, Target.CenterY - ParentActor.CenterY);
  MoveVectorNormalized := MoveVector.Normalize;
  MoveVector := MoveVectorNormalized * MinSingle(ParentPlayer.PlayerCharacterData.RollRange, MoveVector.Length + ParentActor.Size + Target.Size);
end;

procedure TActionStealthAttack.Perform;
var
  TotalDamage: Single;
begin
  TotalDamage := ParentPlayer.Inventory.GetStealthDamage;
  ShowLog(GetTranslation('PlayerStealthAttack'), [ParentActor.Data.DisplayName, Target.Data.DisplayName, TotalDamage], ColorLogAttack);
  Target.Hit(TotalDamage);
  ParentPlayer.Inventory.PlayAttackSound;
  ParentActor.DamageWeapon;
  if Target.CanAct and (Parent is TPlayerCharacter) then
  begin
    (Target as TMonster).Ai.AiFlee := false;
    (Target as TMonster).Ai.OnHit(15);
  end;
  if not Target.CanAct and (Parent is TPlayerCharacter) then
    ParentPlayer.Experience.AddExperience((Target as TMonster).MonsterData.XP);
  Target := nil;
end;

procedure TActionStealthAttack.Update(const SecondsPassed: Single);
begin
  if (Target <> nil) and (Target.Health > 0) then
  begin
    if Target.Collides(ParentActor, 0) then
    begin
      Perform;
      ActionFinished;
      Exit;
    end;
  end; //else just keep rolling?
  inherited;
end;

{ TActionStealthAttackData -------------------------------}

function TActionStealthAttackData.Action: TActionClass;
begin
  Exit(TActionStealthAttack);
end;

initialization
  RegisterSimpleSerializableObject(TActionStealthAttack);
  RegisterSerializableData(TActionStealthAttackData);

end.


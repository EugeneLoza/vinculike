{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionAlarm;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameActionAbstract, GameActionAlarmAbstract,
  GameUnlockableEntry;

type
  { Action that unconditionally alerts monsters within a given range
    males sense for traps, for monster action use AlarmFlee }
  TActionAlarm = class(TActionAlarmAbstract)
  protected
    procedure Perform; override;
  public
    function CanStop: Boolean; override;
  end;

  TActionAlarmData = class(TActionAlarmAbstractData)
  public
    function Description: TEntriesList; override;
    function Action: TActionClass; override;
  end;

implementation
uses
  GameSimpleSerializableObject, GameSerializableData,
  GameActor, GameStats,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TActionAlarmData}
{$INCLUDE actiontypecasts.inc}

function TActionAlarm.CanStop: Boolean;
begin
  Exit(true);
end;

procedure TActionAlarm.Perform;
begin
  inherited Perform;
  LocalStats.IncStat(Data.ClassName);
  ParentActor.Health := -1;
  ParentActor.ForceResetToIdle;
end;

{ TActionAlarmData --------------------------------------- }

function TActionAlarmData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('EFFECT: Will alert all monsters within radius %.0n of the character''s current position.', [Sqrt(SqrAlarmRange)]),
    Classname, 1));
end;

function TActionAlarmData.Action: TActionClass;
begin
  Exit(TActionAlarm);
end;

initialization
  RegisterSimpleSerializableObject(TActionAlarm);
  RegisterSerializableData(TActionAlarmData);

end.


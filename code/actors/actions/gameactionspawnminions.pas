{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionSpawnMinions;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameActionAbstract, GameActionOnTarget, GameMonsterData,
  GameUnlockableEntry;

type
  TActionSpawnMinions = class(TActionOnTarget) // TODO: Not on target?
  strict private
    IsWarmUp: Boolean;
    Phase: Single;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  strict private
    procedure Perform;
  public
    function CanStop: Boolean; override;
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    procedure Update(const SecondsPassed: Single); override;
  end;

  TActionSpawnMinionsData = class(TActionOnTargetData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    SpawnList: TMonstersDataList;
    function Description: TEntriesList; override;
    function Action: TActionClass; override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData, CastleStringUtils,
  GameMap, GameRandom, GameActor, GameMonster, GameMonstersDatabase,
  GameSounds, GameLog, GameColors, GameStats, GameDifficultyLevel,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TActionSpawnMinionsData}
{$INCLUDE actiontypecasts.inc}

procedure TActionSpawnMinions.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
  Element.AttributeSet('IsWarmUp', IsWarmUp);
end;

procedure TActionSpawnMinions.Load(const Element: TDOMElement);
begin
  inherited Load(Element);
  Phase := Element.AttributeSingle('Phase');
  IsWarmUp := Element.AttributeBoolean('IsWarmUp');
end;

procedure TActionSpawnMinions.Perform;
var
  I: Integer;
  AMonster: TMonster;
  MonsterKind: TMonsterData;
  SX, SY: Int16;
begin
  Sound(ActionData.HitSound);
  MonsterKind := ActionData.SpawnList[Rnd.Random(ActionData.SpawnList.Count)];
  for I := 0 to 1 + Rnd.Random(Round(Sqrt(Map.CurrentDepth / MonsterKind.Danger / 2)) + 1) do // 2 - 2 on lvl.1 2 - 6 on lvl.25
  begin
    AMonster := TMonster.Create;
    AMonster.Data := MonsterKind;
    AMonster.Reset;
    repeat
      SX := ParentActor.LastTileX - (3 + Map.CurrentDepth div 3) + Rnd.Random(3 + (3 + Map.CurrentDepth div 3) * 2);
      SY := ParentActor.LastTileY - (3 + Map.CurrentDepth div 3) + Rnd.Random(3 + (3 + Map.CurrentDepth div 3) * 2);
    until (SX > 0) and (SY > 0) and (SX < Map.PredSizeX - AMonster.PredSize) and (SY < Map.PredSizeY - AMonster.PredSize) and Map.PassableTiles[AMonster.PredSize][SX + Map.SizeX * SY];
    AMonster.Teleport(SX, SY);
    AMonster.Ai.Guard := false;
    AMonster.Ai.ChasePlayer(30);
    Map.MonstersList.Add(AMonster);
  end;
  LocalStats.IncStat(Data.ClassName);
  ShowLog('%s shivers and several smaller objects detach from it', [ParentActor.Data.DisplayName], ColorLogTrap);
  ParentActor.Particle('SPAWN', ColorParticleSpawn);
end;

function TActionSpawnMinions.CanStop: Boolean;
begin
  Exit(IsWarmUp);
end;

function TActionSpawnMinions.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionSpawnMinions.NoiseAddition: Single;
begin
  Exit(0.0);
end;

class function TActionSpawnMinions.NewAction(
  const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionSpawnMinions).Phase := 0;
  (Result as TActionSpawnMinions).IsWarmUp := true;
end;

procedure TActionSpawnMinions.Update(const SecondsPassed: Single);
begin
  if not ParentActor.CanAct then
  begin
    ActionFinished;
    Exit;
  end;

  Phase += SecondsPassed;
  if IsWarmUp and (Phase >= ActionData.WarmUpTime * Difficulty.MonsterAttackDelayMultiplier) then
  begin
    Phase -= ActionData.WarmUpTime * Difficulty.MonsterAttackDelayMultiplier;
    IsWarmUp := false;
    Perform;
  end else
  if not IsWarmUp and (Phase >= ActionData.CoolDownTime * Difficulty.MonsterAttackDelayMultiplier) then
  begin
    IsWarmUp := true;
    Phase -= ActionData.CoolDownTime * Difficulty.MonsterAttackDelayMultiplier;
  end;

  inherited;
end;

{ TActionSpawnMinionsData -------------------------------}

procedure TActionSpawnMinionsData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if SpawnList.Count = 0 then
    raise EDataValidationError.CreateFmt('Cannot spawn minions: SpawnList is empty in %s', [Self.ClassName]);
end;

procedure TActionSpawnMinionsData.Read(const Element: TDOMElement);
var
  SlotStringList: TCastleStringList;
  S: String;
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');

  SpawnList := TMonstersDataList.Create(false);
  SlotStringList := CreateTokens(Element.AttributeString('SpawnList'), [',']);
  for S in SlotStringList do
    if MonstersDataDictionary.ContainsKey(S) then
      SpawnList.Add(MonstersDataDictionary[S])
    else
      raise Exception.CreateFmt('Monster "%s" not found for action %s', [S, Self.ClassName]); {$WARNING this depends on monsters initialization order, needs to go into post-processing"}
  FreeAndNil(SlotStringList);
end;

function TActionSpawnMinionsData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('Can spawn several (at least two) lesser monsters.', []),
    Classname, 1));
end;

function TActionSpawnMinionsData.Action: TActionClass;
begin
  Exit(TActionSpawnMinions);
end;

destructor TActionSpawnMinionsData.Destroy;
begin
  FreeAndNil(SpawnList);
  inherited Destroy;
end;

initialization
  RegisterSimpleSerializableObject(TActionSpawnMinions);
  RegisterSerializableData(TActionSpawnMinionsData);

end.


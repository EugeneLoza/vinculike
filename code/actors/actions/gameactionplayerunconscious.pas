{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerUnconscious;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameActionAbstract;

type
  TActionPlayerUnconscious = class(TActionAbstract)
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function ProposedTimeSpeed: Single; override;
    function CanStop: Boolean; override;
  public
    procedure Update(const SecondsPassed: Single); override;
    procedure Stop; override;
  end;

  TActionPlayerUnconsciousData = class(TActionAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GamePlayerCharacter, GameLog, GameColors, GameEnchantmentDeepSleep;

{$DEFINE DataClass:=TActionPlayerUnconsciousData}
{$INCLUDE actiontypecasts.inc}

function TActionPlayerUnconscious.NoiseMultiplier: Single;
begin
  Exit(0.7);
end;

function TActionPlayerUnconscious.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionPlayerUnconscious.ProposedTimeSpeed: Single;
begin
  Exit(30.0);
end;

function TActionPlayerUnconscious.CanStop: Boolean;
begin
  // don't use inherited
  Result := ParentPlayer.Stamina >= ParentPlayer.Inventory.FindEffectAdditive(TEnchantmentDeepSleep) * ParentPlayer.PlayerCharacterData.MaxStamina;
  if not Result then
    ShowLog('%s is unconscious and cannot wake up', [ParentPlayer.Data.DisplayName], ColorLogNotEnoughStamina);
end;

procedure TActionPlayerUnconscious.Update(const SecondsPassed: Single);
begin
  //inherited;
  ParentPlayer.RegenerateMaxStats(SecondsPassed, 1.5);
end;

procedure TActionPlayerUnconscious.Stop;
begin
  inherited;
  ShowLog('%s jumps up trying to remember where she is and what has happened', [ParentPlayer.Data.DisplayName], ColorLogCancel);
end;

{ TActionPlayerUnconsciousData -------------------------------}

function TActionPlayerUnconsciousData.Action: TActionClass;
begin
  Exit(TActionPlayerUnconscious);
end;

initialization
  RegisterSimpleSerializableObject(TActionPlayerUnconscious);
  RegisterSerializableData(TActionPlayerUnconsciousData);

end.


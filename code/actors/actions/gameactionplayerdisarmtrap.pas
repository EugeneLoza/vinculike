{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerDisarmTrap;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameSimpleSerializableObject,
  GameMonster,
  GameActionAbstract, GameActionOnTarget;

const
  TrapDisarmedXp = 5;

type
  TActionPlayerDisarmTrap = class(TActionOnTarget)
  public const
    Duration = Single(1.5);
  strict private
    Phase: Single;
    procedure Perform;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function ProposedTimeSpeed: Single; override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    procedure Stop; override;
  end;

  TActionPlayerDisarmTrapData = class(TActionOnTargetData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors,
  GameActor, GamePlayerCharacter, GameSounds, GameTranslation,
  GameStats;

{$DEFINE DataClass:=TActionPlayerDisarmTrapData}
{$INCLUDE actiontypecasts.inc}

procedure TActionPlayerDisarmTrap.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
end;

procedure TActionPlayerDisarmTrap.Load(const Element: TDOMElement);
begin
  inherited;
  Phase := Element.AttributeSingle('Phase');
  Target := ObjectByReferenceId(Element.AttributeQWord('Target')) as TMonster; // TMonsters are deserealized before Player characters, so this should be fine
end;

function TActionPlayerDisarmTrap.NoiseMultiplier: Single;
begin
  Exit(1.2);
end;

function TActionPlayerDisarmTrap.NoiseAddition: Single;
begin
  Exit(2.0);
end;

function TActionPlayerDisarmTrap.ProposedTimeSpeed: Single;
begin
  Exit(4.0);
end;

procedure TActionPlayerDisarmTrap.Start;
begin
  inherited Start;
  ShowLog(GetTranslation('PlayerActionDisarmTrapStart'), [ParentActor.Data.DisplayName, TargetMonster.Data.DisplayName, TActionPlayerDisarmTrap.Duration], ColorLogActionStart);
end;

procedure TActionPlayerDisarmTrap.Perform;
begin
  ShowLog(GetTranslation('PlayerActionDisarmTrapSuccess'), [ParentActor.Data.DisplayName, TargetMonster.Data.DisplayName], ColorLogTrapDisarmed);
  Sound('trap_trigger'); // TODO: Temporary
  ParentPlayer.Experience.AddExperience(TrapDisarmedXp);
  // TODO: fail chance
  TargetMonster.Health := -1;
  TargetMonster.ForceResetToIdle; // make sure action discarded properly if any
  LocalStats.IncStat('killed_' + TargetMonster.Data.Id);
  Target := nil;
end;

procedure TActionPlayerDisarmTrap.Update(const SecondsPassed: Single);
begin
  Phase += SecondsPassed;
  if Phase > Duration then
  begin
    Perform;
    ActionFinished;
    Exit;
  end
end;

class function TActionPlayerDisarmTrap.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionPlayerDisarmTrap).Phase := 0;
end;

procedure TActionPlayerDisarmTrap.Stop;
begin
  inherited;
  if Target <> nil then
    ShowLog(GetTranslation('PlayerActionDisarmTrapCancel'), [ParentActor.Data.DisplayName, TargetMonster.Data.DisplayName], ColorLogCancel);
end;

{ TActionPlayerDisarmTrapData -------------------------------}

function TActionPlayerDisarmTrapData.Action: TActionClass;
begin
  Exit(TActionPlayerDisarmTrap);
end;

initialization
  RegisterSimpleSerializableObject(TActionPlayerDisarmTrap);
  RegisterSerializableData(TActionPlayerDisarmTrapData);

end.

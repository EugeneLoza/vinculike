{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionMoveGhost;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  CastleVectors,
  GameSimpleSerializableObject,
  GameMapTypes,
  GameActionMoveAbstract, GameActionAbstract;

type
  TActionMoveGhost = class(TActionMoveAbstract)
  strict private
    DestinationX, DestinationY, OriginX, OriginY: Single;
  public
    MoveVector, MoveVectorNormalized: TVector2;
    MoveVectorLength, MovePhase: Single;
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function RemainingDistance: Single; override;
    function RemainingTime: Single; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    procedure MoveTo(const ToX, ToY: Single); override;
  public
    procedure Save(const Element: TDOMElement); override;
    procedure AfterDeserealization; override;
  protected
    procedure Load(const Element: TDOMElement); override;
  end;

  TActionMoveGhostData = class(TActionMoveAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableData,
  GameActor, GameMap, GameViewGame;

{$DEFINE DataClass:=TActionMoveGhostData}
{$INCLUDE actiontypecasts.inc}

function TActionMoveGhost.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionMoveGhost.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionMoveGhost.RemainingDistance: Single;
begin
  Exit(MoveVectorLength);
end;

function TActionMoveGhost.RemainingTime: Single;
begin
  Exit(MoveVectorLength / ParentActor.Data.MovementSpeed);
end;

procedure TActionMoveGhost.Update(const SecondsPassed: Single);
var
  Diff: Single;
begin
  Diff := SecondsPassed * ParentActor.GetSpeed;
  // todo: make sure step no bigger than a tile?
  MovePhase += diff;
  ParentActor.HurtFeet(diff * FeetDamagePerTile);
  if MovePhase < MoveVectorLength then
  begin
    ParentActor.MoveMeTo(OriginX + MoveVectorNormalized.X * MovePhase, OriginY + MoveVectorNormalized.Y * MovePhase);
    ParentActor.UpdateVisible;
  end else
  begin
    ParentActor.MoveMeTo(DestinationX, DestinationY);
    ParentActor.UpdateVisible;
    ActionFinished;
    Exit;
  end;
end;

class function TActionMoveGhost.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionMoveGhost).MovePhase := 0;
end;

procedure TActionMoveGhost.MoveTo(const ToX, ToY: Single);
var
  ToXInt, ToYInt, GotoX, GotoY: Int16;
begin
  DestinationX := ToX;
  DestinationY := ToY;
  OriginX := ParentActor.X;
  OriginY := ParentActor.Y;

  ToXInt := Round(ToX);
  ToYInt := Round(ToY);
  Map.GetNearestPassableTile(ParentActor.PredSize, Parent <> ViewGame.CurrentCharacter, ToXInt, ToYInt, GotoX, GotoY);
  DestinationX := GotoX;// + Frac(ToX);
  DestinationY := GotoY;// + Frac(ToY);

  MoveVector := Vector2(DestinationX - OriginX, DestinationY - OriginY);
  MoveVectorLength := MoveVector.Length;
  MoveVectorNormalized := MoveVector / MoveVectorLength;
end;

procedure TActionMoveGhost.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  // To avoid painful saving everything we save... only destination.
  Element.AttributeSet('DestinationX', DestinationX);
  Element.AttributeSet('DestinationY', DestinationY);
end;

procedure TActionMoveGhost.AfterDeserealization;
begin
  inherited AfterDeserealization;
  MoveTo(DestinationX, DestinationY); // TODO: Maybe better save waypoints?
end;

procedure TActionMoveGhost.Load(const Element: TDOMElement);
begin
  inherited;
  DestinationX := Element.AttributeSingle('DestinationX');
  DestinationY := Element.AttributeSingle('DestinationY');
end;

{ TActionMoveGhostData -------------------------------}

function TActionMoveGhostData.Action: TActionClass;
begin
  Exit(TActionMoveGhost);
end;

initialization
  RegisterSimpleSerializableObject(TActionMoveGhost);
  RegisterSerializableData(TActionMoveGhostData);

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionAlarmAbstract;

{$INCLUDE compilerconfig.inc}

interface

uses
  Classes, DOM,
  GameActionOnTarget;

type
  TActionAlarmAbstract = class abstract(TActionOnTarget) // TODO: not OnTarget
  protected
    procedure Perform; virtual;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Update(const SecondsPassed: Single); override;
  end;

  TActionAlarmAbstractData = class abstract(TActionOnTargetData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    SqrAlarmRange: Single;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableData,
  GameSounds, GameMap, GameColors, GameMonster, GameActor, GameLog;

{$DEFINE DataClass:=TActionAlarmAbstractData}
{$INCLUDE actiontypecasts.inc}

function TActionAlarmAbstract.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionAlarmAbstract.NoiseAddition: Single;
begin
  Exit(0.0);
end;

procedure TActionAlarmAbstract.Perform;
var
  AMonster: TMonster;
begin
  Sound(ActionData.HitSound);
  ShowLog('%s emits a loud noise.', [ParentActor.Data.DisplayName], ColorLogTrap);
  ParentActor.Particle('ALARM', ColorParticleSpawn);
  for AMonster in Map.MonstersList do
    if AMonster.CanAct and (Sqr(AMonster.X - ParentActor.X) + Sqr(AMonster.Y - ParentActor.Y) <= ActionData.SqrAlarmRange) then
      AMonster.Ai.InvestigateNoise(ParentActor.CenterX, ParentActor.CenterY);
end;

procedure TActionAlarmAbstract.Update(const SecondsPassed: Single);
begin
  //inherited: no inherited because no "on-target" TODO

  if not ParentActor.CanAct then
  begin
    ActionFinished;
    Exit;
  end;

  Perform;
end;

{ TActionAlarmAbstractData ----------------------------------------------------- }

procedure TActionAlarmAbstractData.Validate;
begin
  inherited Validate;
  if SqrAlarmRange <= 0 then
    raise EDataValidationError.CreateFmt('SqrAlarmRange %n <= 0 in %s', [SqrAlarmRange, Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TActionAlarmAbstractData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  SqrAlarmRange := Sqr(Element.AttributeSingle('AlarmRange'));
  HitSound := Element.AttributeString('HitSound');
end;

initialization
end.


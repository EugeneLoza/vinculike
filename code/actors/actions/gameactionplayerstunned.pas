{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerStunned;

{$INCLUDE compilerconfig.inc}

interface

uses
  GameActionAbstract, GameActionPlayerStunnedAbstract;

type
  TActionPlayerStunned = class(TActionPlayerStunnedAbstract)
  protected
    procedure CannotStopStunned; override;
    procedure StopStunned; override;
  end;

  TActionPlayerStunnedData = class(TActionAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  GameSimpleSerializableObject, GameSerializableData,
  GamePlayerCharacter, GameLog, GameColors;

{$DEFINE DataClass:=TActionPlayerStunnedData}
{$INCLUDE actiontypecasts.inc}

procedure TActionPlayerStunned.CannotStopStunned;
begin
  ShowLog('%s is stunned and cannot move', [ParentPlayer.Data.DisplayName], ColorLogNotEnoughStamina);
end;

procedure TActionPlayerStunned.StopStunned;
begin
  ShowLog('%s regains control of her limbs', [ParentPlayer.Data.DisplayName], ColorLogCancel);
end;

{ TActionPlayerStunnedData -------------------------------}

function TActionPlayerStunnedData.Action: TActionClass;
begin
  Exit(TActionPlayerStunned);
end;

initialization
  RegisterSimpleSerializableObject(TActionPlayerStunned);
  RegisterSerializableData(TActionPlayerStunnedData);

end.


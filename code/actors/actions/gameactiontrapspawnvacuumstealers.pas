{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionTrapSpawnVacuumStealers;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameActionAbstract, GameActionOnTarget,
  GameActorData,
  GameUnlockableEntry;

type
  TActionTrapSpawnVacuumStealers = class(TActionOnTarget)
  strict private
    procedure Perform;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Update(const SecondsPassed: Single); override;
  end;

  TActionTrapSpawnVacuumStealersData = class(TActionOnTargetData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    function Description: TEntriesList; override;
    function Action: TActionClass; override;
  public
    MonsterData: TActorData;
    SuspensionDuration: Single;
    FallDamage: Single;
    HitSound: String;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GameApparelSlots, GameActionPlayerZeroG,
  GamePlayerCharacter, GameMonster, GameMonstersDatabase,
  GameMap, GameRandom, GameActor, GameMapItem,
  GameSounds, GameLog, GameColors, GameStats,
  GameViewGame,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TActionTrapSpawnVacuumStealersData}
{$INCLUDE actiontypecasts.inc}

procedure TActionTrapSpawnVacuumStealers.Perform;
var
  I: Integer;
  E: TApparelSlot;
  Count: Integer;
  AMonster: TMonster;
  SX, SY: Int16;
begin
  LocalStats.IncStat(Data.ClassName);

  Count := 0;
  for E in TargetPlayer.Blueprint.EquipmentSlots do
    if (TargetPlayer.Inventory.Equipped[E] <> nil) and not TargetPlayer.Inventory.Bondage[E] then
    begin
      repeat
        repeat
          SX := TargetActor.LastTileX + Rnd.Random(32) - 16;
        until (SX > 0) and (SX < Map.PredSizeX);
        repeat
          SY := TargetActor.LastTileY + Rnd.Random(32) - 16;
        until (SY > 0) and (SY < Map.PredSizeY);
      until Map.Ray(0, TargetActor.LastTileX, TargetActor.LastTileY, SX, SY);
      TMapItem.DropItem(SX, SY, TargetPlayer.Inventory.UnequipAndReturn(E, true), false);
      Inc(Count);
    end;

  TargetPlayer.CurrentAction := TActionPlayerZeroG.NewAction(TargetPlayer, ActionData.SuspensionDuration);
  TActionPlayerZeroG(TargetPlayer.CurrentAction).FallDamage := ActionData.FallDamage;
  TargetPlayer.CurrentAction.Start;
  ViewGame.UnPauseGame;
  Sound(ActionData.HitSound);

  if Count > 0 then
  begin
    ShowLog('A whirlwind lifts %s off the ground (%.1ns), tearing all items off her', [TargetActor.Data.DisplayName, ActionData.SuspensionDuration], ColorLogTrap);
    ShowLog('Out of nowhere a few monsters rush to pick those up', [TargetActor.Data.DisplayName, ActionData.SuspensionDuration], ColorLogTrap);
    TargetActor.Particle('SPAWN', ColorParticleSpawn);

    for I := 0 to Pred(Count) do
    begin
      AMonster := TMonster.Create;
      AMonster.Data := ActionData.MonsterData;
      AMonster.Reset;
      repeat
        SX := TargetActor.LastTileX - (3 + Map.CurrentDepth div 3) + Rnd.Random(3 + (3 + Map.CurrentDepth div 3) * 2);
        SY := TargetActor.LastTileY - (3 + Map.CurrentDepth div 3) + Rnd.Random(3 + (3 + Map.CurrentDepth div 3) * 2);
      until (SX > 0) and (SY > 0) and (SX < Map.PredSizeX - AMonster.PredSize) and (SY < Map.PredSizeY - AMonster.PredSize) and Map.PassableTiles[AMonster.PredSize][SX + Map.SizeX * SY];
      AMonster.Teleport(SX, SY);
      AMonster.Ai.Guard := false;
      Map.MonstersList.Add(AMonster);
    end;
  end else
    ShowLog('A whirlwind briefly lifts %s off the ground (%.1ns)', [TargetActor.Data.DisplayName, ActionData.SuspensionDuration], ColorLogTrap);
end;

function TActionTrapSpawnVacuumStealers.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionTrapSpawnVacuumStealers.NoiseAddition: Single;
begin
  Exit(0.0);
end;

procedure TActionTrapSpawnVacuumStealers.Update(const SecondsPassed: Single);
begin
  if not ParentActor.CanAct then
  begin
    ActionFinished;
    Exit;
  end;

  Perform;

  ParentActor.Health := -1;
  ParentActor.ForceResetToIdle;
  inherited;
end;

{ TActionTrapSpawnVacuumStealersData -------------------------------}

procedure TActionTrapSpawnVacuumStealersData.Validate;
begin
  inherited Validate;
  if SuspensionDuration < 0 then
    raise EDataValidationError.CreateFmt('SuspensionDuration < 0 in %s', [Self.ClassName]);
  if FallDamage < 0 then
    raise EDataValidationError.CreateFmt('FallDamage < 0 in %s', [Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TActionTrapSpawnVacuumStealersData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  MonsterData := MonstersDataDictionary[Element.AttributeString('MonsterData')];
  SuspensionDuration := Element.AttributeFloat('SuspensionDuration');
  FallDamage := Element.AttributeFloat('FallDamage');
  HitSound := Element.AttributeString('HitSound');
end;

function TActionTrapSpawnVacuumStealersData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('EFFECT: Strips all non-bondage items off the target and scatters them on the ground around, suspending the heroine for a fraction of a second. Meanwhile releases several Vacuum Stealers who will try to pick the dropped item and run away to collect other items if any on the dungeon floor.', []),
    Classname, 1));
end;

function TActionTrapSpawnVacuumStealersData.Action: TActionClass;
begin
  Exit(TActionTrapSpawnVacuumStealers);
end;

initialization
  RegisterSimpleSerializableObject(TActionTrapSpawnVacuumStealers);
  RegisterSerializableData(TActionTrapSpawnVacuumStealersData);

end.


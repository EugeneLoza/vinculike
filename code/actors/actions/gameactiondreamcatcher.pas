{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionDreamCatcher;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameActionOnTarget, GameActionAbstract, GameStatusEffect,
  GameUnlockableEntry;

type
  { Monster flees target if target is active
    if target is sleeping will undress and equip lethargic blindfold }
  TActionDreamCatcher = class(TActionOnTarget)
  protected
    procedure Perform; virtual;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Update(const SecondsPassed: Single); override;
  end;

  TActionDreamCatcherData = class(TActionOnTargetData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    FallAsleepSound: String;
    KeepAsleepSound: String;
    AntifreezeMonster1Chance: Single;
    AntifreezeMonster2Chance: Single;
    AntifreezeMonster1: String;
    AntifreezeMonster2: String;
    HealingNude: Single;
    HealingDressed: Single;
    StatusEffects: TStatusEffectsList;
    function Action: TActionClass; override;
    function Description: TEntriesList; override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GameSounds, GameColors, GameActor, GameLog, GameItemsDatabase, GameItemData, GameInventoryItem,
  GameApparelSlots, GamePlayerCharacter, GameMonster, GameStats, GameRandom, GameMonstersDatabase, GameMap,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TActionDreamCatcherData}
{$INCLUDE actiontypecasts.inc}

function TActionDreamCatcher.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionDreamCatcher.NoiseAddition: Single;
begin
  Exit(0.0);
end;

procedure TActionDreamCatcher.Perform;

  procedure SpawnMonster(const MonsterId: String);
  var
    AMonster: TMonster;
    SX, SY: Int16;
  begin
    AMonster := TMonster.Create;
    AMonster.Data := MonstersDataDictionary[MonsterId]; // TODO: move that into ActionData
    AMonster.Reset;
    repeat
      SX := Rnd.Random(Map.SizeX - 2) + 1;
      SY := Rnd.Random(Map.SizeY - 2) + 1;
    until (TargetPlayer.DistanceTo(SX, SY) > Map.SizeX / 4) and Map.PassableTiles[AMonster.PredSize][SX + Map.SizeX * SY];
    AMonster.Teleport(SX, SY);
    AMonster.Ai.Guard := false;
    Map.MonstersList.Add(AMonster);
  end;

var
  E: TApparelSlot;
  I: Integer;
begin
  if TargetPlayer.Unsuspecting then
  begin
    LocalStats.IncStat(Data.ClassName);
    if TargetPlayer.Blindfolded then
    begin
      Sound(ActionData.KeepAsleepSound);
      ShowLog('%s makes sure %s is sleeping sweet and tight', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogStaminaExhausted);
      ShowLog('And gives a healing kiss on her forehead before leaving', [], ColorLogStaminaExhausted);
      if TargetPlayer.Inventory.Nude then
      begin
        TargetPlayer.HealMaxHealth(TargetPlayer.MaxHealth * ActionData.HealingNude);
        TargetPlayer.HealMaxWill(TargetPlayer.MaxWill * ActionData.HealingNude);
      end else
      begin
        TargetPlayer.HealMaxHealth(TargetPlayer.MaxHealth * ActionData.HealingDressed);
        TargetPlayer.HealMaxWill(TargetPlayer.MaxWill * ActionData.HealingDressed);
      end;
      { "Antifreeze monsters" - sometimes spawn some nasty thing to avoid
        a situation when the PC cannot wake up becuase being constantly reset
        to negative stamina, but no monsters in the patrols
        can actually force-wake-up, so we add a couple here with a low chance }
      if Rnd.Random < ActionData.AntifreezeMonster1Chance then
        SpawnMonster(ActionData.AntifreezeMonster1)
      else
      if Rnd.Random < ActionData.AntifreezeMonster2Chance then
        SpawnMonster(ActionData.AntifreezeMonster2);
    end else
    begin
      Sound(ActionData.FallAsleepSound);
      for E in TargetPlayer.Blueprint.EquipmentSlots do
        if (TargetPlayer.Inventory.Equipped[E] <> nil) and (not TargetPlayer.Inventory.Bondage[E]) then
          TargetPlayer.Inventory.UnequipAndDrop(E, false);
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['blindfold_no_wake_up'] as TItemData));
      ShowLog('%s has a wonderful dream she doesn''t want to end', [TargetPlayer.Data.DisplayName], ColorLogStaminaExhausted);
      ShowLog('She knows it''s not real, but it''s so calm and peaceful', [TargetPlayer.Data.DisplayName], ColorLogStaminaExhausted);
      if TargetPlayer.Inventory.Nude then
      begin
        ShowLog('Only now she realizes how little she needs from life', [TargetPlayer.Data.DisplayName], ColorLogStaminaExhausted);
        ShowLog('Not even clothes or anything, nothing matters any longer', [TargetPlayer.Data.DisplayName], ColorLogStaminaExhausted);
      end else
      begin
        ShowLog('She pulls on an imaginary blanket that embraces her so warm and soft', [TargetPlayer.Data.DisplayName], ColorLogStaminaExhausted);
        ShowLog('And rolls in a more comfortable pose smiling to herself', [TargetPlayer.Data.DisplayName], ColorLogStaminaExhausted);
      end;
      ShowLog('No worries or struggles, it''s time to rest, it''s time to sleep', [TargetPlayer.Data.DisplayName], ColorLogStaminaExhausted);
      ShowLog('And never wake up...', [TargetPlayer.Data.DisplayName], ColorLogStaminaExhausted);
    end;
    for I := 0 to Pred(ActionData.StatusEffects.Count) do
      TargetPlayer.Inventory.AddStatusEffectNoStack(ActionData.StatusEffects[I].Clone);
  end else
    ShowLog('%s flees', [ParentActor.Data.DisplayName], ColorLogTickle);
  ParentMonster.Ai.AiFlee := true;
end;

procedure TActionDreamCatcher.Update(const SecondsPassed: Single);
begin
  //inherited: no inherited because no "on-target" TODO

  if not ParentActor.CanAct then
  begin
    ActionFinished;
    Exit;
  end;

  Perform;
end;

{ TActionDreamCatcherData ----------------------------------------------------- }

procedure TActionDreamCatcherData.Validate;
var
  I: Integer;
begin
  inherited Validate;
  if not SoundExists(FallAsleepSound) then
    raise EDataValidationError.CreateFmt('Invalid FallAsleepSound = "%s" in %s', [FallAsleepSound, Self.ClassName]);
  if not SoundExists(KeepAsleepSound) then
    raise EDataValidationError.CreateFmt('Invalid KeepAsleepSound = "%s" in %s', [KeepAsleepSound, Self.ClassName]);
  if AntifreezeMonster1Chance < 0 then
    raise EDataValidationError.CreateFmt('AntifreezeMonster1Chance < 0 in %s', [Self.ClassName]);
  if AntifreezeMonster1 = '' then
    raise EDataValidationError.CreateFmt('Empty AntifreezeMonster1 in %s', [Self.ClassName]);
  if AntifreezeMonster2Chance < 0 then
    raise EDataValidationError.CreateFmt('AntifreezeMonster2Chance < 0 in %s', [Self.ClassName]);
  if AntifreezeMonster2 = '' then
    raise EDataValidationError.CreateFmt('Empty AntifreezeMonster2 in %s', [Self.ClassName]);
  if HealingNude < 0 then
    raise EDataValidationError.CreateFmt('HealingNude < 0 in %s', [Self.ClassName]);
  if HealingDressed < 0 then
    raise EDataValidationError.CreateFmt('HealingDressed < 0 in %s', [Self.ClassName]);
  if HealingNude > 1 then
    raise EDataValidationError.CreateFmt('HealingNude > 1 in %s', [Self.ClassName]);
  if HealingDressed > 1 then
    raise EDataValidationError.CreateFmt('HealingDressed > 1 in %s', [Self.ClassName]);
  if StatusEffects.Count = 0 then
    raise EDataValidationError.CreateFmt('StatusEffects.Count = 0 in %s', [Self.ClassName]);
  for I := 0 to Pred(StatusEffects.Count) do
    if not StatusEffects[I].Effect.CanBeStatusEffect then
      raise EDataValidationError.CreateFmt('StatusEffects[%d] as %s cannot be status effect in %s', [I, StatusEffects[I].Effect.ClassName, Self.ClassName]);
end;

procedure TActionDreamCatcherData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  FallAsleepSound := Element.AttributeString('FallAsleepSound');
  KeepAsleepSound := Element.AttributeString('KeepAsleepSound');

  AntifreezeMonster1Chance := Element.AttributeSingle('AntifreezeMonster1Chance');
  AntifreezeMonster1 := Element.AttributeString('AntifreezeMonster1');
  AntifreezeMonster2Chance := Element.AttributeSingle('AntifreezeMonster2Chance');
  AntifreezeMonster2 := Element.AttributeString('AntifreezeMonster2');

  HealingNude := Element.AttributeSingle('HealingNude');
  HealingDressed := Element.AttributeSingle('HealingDressed');

  StatusEffects := LoadStatusEffectsList(Element);
end;

function TActionDreamCatcherData.Action: TActionClass;
begin
  Exit(TActionDreamCatcher);
end;

function TActionDreamCatcherData.Description: TEntriesList;
begin
  Result := inherited Description;
    Result.Add(
      NewEntryText(
        Format('ATTACK: Will only attack in sleep and flees otherwise. If finds the heroine sleeping will unequip all items and equip a blindfold which will prevent her from waking up until completley rested or attacked.', []),
      Classname, 1));
end;

destructor TActionDreamCatcherData.Destroy;
begin
  FreeAndNil(StatusEffects);
  inherited Destroy;
end;

initialization
  RegisterSimpleSerializableObject(TActionDreamCatcher);
  RegisterSerializableData(TActionDreamCatcherData);
end.


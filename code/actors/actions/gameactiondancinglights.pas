{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionDancingLights;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameActionAbstract, GameActionOnTarget, GameApparelSlots,
  GameUnlockableEntry;

type
  { Action that has different effects depending on amount of monsters
    attacking the target }
  TActionDancingLights = class(TActionOnTarget)
  strict private const
    EffectDistanceSqr = Sqr(5);
  strict private
    IsWarmUp: Boolean;
    Phase: Single;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  strict private
    class var GroupCounter: Integer;
    function GroupSize: Integer;
    procedure Perform;
  public
    function CanStop: Boolean; override;
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    procedure Update(const SecondsPassed: Single); override;
  end;

  TActionDancingLightsData = class(TActionOnTargetData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    Damage: Single;
    DisrobeSlots: TApparelSlotsList;
    function Description: TEntriesList; override;
    function Action: TActionClass; override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GameMap, GameRandom, GameActor, GameMonster, GameStats,
  GameActionPlayerStunned, GamePlayerCharacter, GameEnumUtils,
  GameSounds, GameLog, GameColors, GameViewGame, GameDifficultyLevel,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TActionDancingLightsData}
{$INCLUDE actiontypecasts.inc}

procedure TActionDancingLights.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
  Element.AttributeSet('IsWarmUp', IsWarmUp);
end;

procedure TActionDancingLights.Load(const Element: TDOMElement);
begin
  inherited Load(Element);
  Phase := Element.AttributeSingle('Phase');
  IsWarmUp := Element.AttributeBoolean('IsWarmUp');
end;

function TActionDancingLights.GroupSize: Integer;
var
  M: TMonster;
begin
  Result := 0;
  for M in Map.MonstersList do
    if M.CanAct and (M.CurrentAction.ClassType = ParentActor.CurrentAction.ClassType) and (M.DistanceToSqr(TargetPlayer) < EffectDistanceSqr) then
      Inc(Result);
end;

procedure TActionDancingLights.Perform;

  procedure StunTarget(const Duration: Single);
  begin
    LocalStats.IncStat(Data.ClassName + '_two');
    ViewGame.WakeUp(true, true);
    if Rnd.Random > TargetPlayer.ResistStun then
    begin
      TargetActor.CurrentAction := TActionPlayerStunned.NewAction(TargetActor, Duration);
      TargetActor.CurrentAction.Start;
      TargetPlayer.AddResistStun;
      ShowLog('Suddenly all %s''s muscles spasm and become stiff (%.1n seconds)', [TargetActor.Data.DisplayName, Duration], ColorLogNotEnoughStamina);
      TargetPlayer.Particle('STUN', ColorParticlePlayerStunned);
      Sound(ActionData.HitSound);
    end else
      ShowLog('%s''s muscles spasm but she manages not to faint', [TargetActor.Data.DisplayName], ColorLogNotEnoughStamina);
  end;

  procedure DisrobeTarget;
  var
    E: TApparelSlot;
  begin
    LocalStats.IncStat(Data.ClassName + '_two');
    repeat
      E := ActionData.DisrobeSlots.Random;
    until TargetPlayer.Inventory.RemovableItem[E];
    ShowLog('The lights around %s flash and her %s falls off', [TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogNotEnoughStamina);
    TargetPlayer.Particle('STRIP', ColorParticlePlayerStunned);
    TargetPlayer.Inventory.UnequipAndDrop(E, true);
    ViewGame.WakeUp(true, true);
    Sound(ActionData.HitSound);
  end;

  procedure DisarmTarget;
  begin
    LocalStats.IncStat(Data.ClassName + '_three');
    ShowLog('%s feels extremely weak for a moment and lets %s drop to the ground', [TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName], ColorLogNotEnoughStamina);
    TargetPlayer.Inventory.UnequipAndDrop(esWeapon, true);
    ViewGame.WakeUp(true, true);
    TargetPlayer.Particle('DISARM', ColorParticlePlayerStunned);
    Sound(ActionData.HitSound);
  end;

  procedure FatigueTarget(const Damage: Single);
  begin
    LocalStats.IncStat(Data.ClassName + '_four');
    ShowLog('%s suddenly feels weird and tired', [TargetActor.Data.DisplayName], ColorLogNotEnoughStamina);
    TargetPlayer.HitStamina(Damage);
    ViewGame.WakeUp(true, true);
    Sound(ActionData.HitSound);
  end;

  procedure FearTarget(const Damage: Single);
  begin
    LocalStats.IncStat(Data.ClassName + '_five');
    ShowLog('%s has a panic attack', [TargetActor.Data.DisplayName], ColorLogNotEnoughStamina);
    TargetPlayer.HitWill(Damage);
    ViewGame.WakeUp(true, true);
    Sound(ActionData.HitSound);
  end;

  function TargetHasRemovableItems: Boolean;
  var
    I: Integer;
  begin
    for I := 0 to Pred(ActionData.DisrobeSlots.Count) do
      if TargetPlayer.Inventory.RemovableItem[ActionData.DisrobeSlots[I]] then
        Exit(true);
    Exit(false);
  end;

begin
  LocalStats.IncStat(Data.ClassName);

  if GroupSize = 1 then
  begin
    // Do nothing
    ShowLog('%s orbits around %s without touching her', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTrap);
    LocalStats.IncStat(Data.ClassName + '_idle');
  end else
  if GroupSize >= 2 then
  begin
    if not TargetActor.Immobilized and (Rnd.Random < 0.5) then
      StunTarget(0.25 * GroupSize)
    else
    if TargetHasRemovableItems and (Rnd.Random < 0.5) then
      DisrobeTarget
    else
    if (GroupSize >= 3) and TargetActor.Immobilized and (TargetPlayer.Inventory.Equipped[esWeapon] <> nil) and (not TargetPlayer.Inventory.Bondage[esWeapon]) and (Rnd.Random < 0.5) then
      DisarmTarget
    else
    if (GroupSize >= 4) and (Rnd.Random < 0.5) then
      FatigueTarget(ActionData.Damage * 2 * GroupSize)
    else
    if (TargetPlayer.Inventory.EquippedClothesRemovable = 0) and TargetActor.Immobilized and (GroupSize >= 5) and (Rnd.Random < 0.5) then
      FearTarget(ActionData.Damage * GroupSize)
    else
    begin
      ShowLog('The lights dance around %s in mesmerizing patterns', [TargetActor.Data.DisplayName], ColorLogTrap);
      LocalStats.IncStat(Data.ClassName + '_noharm');
    end;
  end else
    raise Exception.CreateFmt('Unexpected GroupSize in TActionDancingLights.Perform: %d', [GroupSize]);
end;

function TActionDancingLights.CanStop: Boolean;
begin
  Exit(IsWarmUp);
end;

function TActionDancingLights.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionDancingLights.NoiseAddition: Single;
begin
  Exit(0.0);
end;

class function TActionDancingLights.NewAction(
  const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionDancingLights).Phase := 0;
  (Result as TActionDancingLights).IsWarmUp := true;
end;

procedure TActionDancingLights.Update(const SecondsPassed: Single);

  procedure TeleportAround;
  var
    OriginalX, OriginalY: Int16;
    NewX, NewY: Int16;
    SizeSum: Integer;
  begin
    OriginalX := Trunc(TargetActor.CenterX);
    OriginalY := Trunc(TargetActor.CenterY);
    SizeSum := ParentActor.Size + TargetActor.Size;
    repeat
      repeat
        NewX := OriginalX + Rnd.Random(2 * SizeSum) - SizeSum;
        NewY := OriginalY + Rnd.Random(2 * SizeSum) - SizeSum;
      until (NewX >= 1) and (NewX <= Map.PredSizeX) and (NewY >= 1) and (NewY <= Map.PredSizeY) and Map.PassableTiles[ParentActor.PredSize][NewX + Map.SizeX * NewY];
      ParentActor.MoveMeTo(NewX + Rnd.Random - 0.5, NewY + Rnd.Random - 0.5);
    until ParentActor.DistanceToSqr(TargetActor) < EffectDistanceSqr;
  end;

begin
  if not ParentActor.CanAct then
  begin
    ActionFinished;
    Exit;
  end;

  if Rnd.Random < 0.01 then
    TeleportAround;

  Phase += SecondsPassed;
  if IsWarmUp and (Phase >= ActionData.WarmUpTime * Difficulty.MonsterAttackDelayMultiplier) then
  begin
    Phase -= ActionData.WarmUpTime * Difficulty.MonsterAttackDelayMultiplier;
    IsWarmUp := false;
    Inc(GroupCounter);
    if GroupCounter >= GroupSize then // TODO: CacheGroupSize
    begin
      GroupCounter -= GroupSize;
      Perform;
    end;
  end else
  if not IsWarmUp and (Phase >= ActionData.CoolDownTime * Difficulty.MonsterAttackDelayMultiplier) then
  begin
    IsWarmUp := true;
    Phase -= ActionData.CoolDownTime * Difficulty.MonsterAttackDelayMultiplier;
  end;

  inherited;
end;

{ TActionDancingLightsData -------------------------------}

procedure TActionDancingLightsData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if Damage <= 0 then
    raise EDataValidationError.CreateFmt('Damage %n <= 0 in %s', [Damage, Self.ClassName]);
  if DisrobeSlots.Count = 0 then
    raise EDataValidationError.CreateFmt('DisrobeSlots.Count = 0 in %s', [Self.ClassName]);
end;

procedure TActionDancingLightsData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  Damage := Element.AttributeSingle('Damage');
  DisrobeSlots := specialize StrToEnumsList<TApparelSlot>(Element.AttributeString('DisrobeSlots'));
end;

function TActionDancingLightsData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('The attack pattern depends on the group size of monsters of the same kind.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('If there is only one attacker, it''s pretty much harmless and will not do any damage or undesirable effects, except maybe attracting other monsters.', []),
    Classname + '_idle', 1));
  Result.Add(
    NewEntryText(
      Format('If there are two and more - they have 50% of stunning the target for 1/4 second per each group memeber. Alternatively they can undress the target.', []),
    Classname + '_two', 1));
  Result.Add(
    NewEntryText(
      Format('In case of 3+ attackers and target is immobilized - the character can get disarmed.', []),
    Classname + '_three', 1));
  Result.Add(
    NewEntryText(
      Format('4 and more opponents can cause a serious stamina damage, scaled to group size.', []),
    Classname + '_four', 1));
  Result.Add(
    NewEntryText(
      Format('And finally group of 5+ attackers can inflict a severe willpower damage if target doesn''t have normal clothes on and is stunned.', []),
    Classname + '_five', 1));
  Result.Add(
    NewEntryText(
      Format('There''s always a chance they will not harm the target though.', []),
    Classname + '_noharm', 1));
end;

function TActionDancingLightsData.Action: TActionClass;
begin
  Exit(TActionDancingLights);
end;

destructor TActionDancingLightsData.Destroy;
begin
  FreeAndNil(DisrobeSlots);
  inherited Destroy;
end;

initialization
  RegisterSimpleSerializableObject(TActionDancingLights);
  RegisterSerializableData(TActionDancingLightsData);

end.


{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerRandomChat;

{$INCLUDE compilerconfig.inc}

interface

uses
  GameActionAbstract, GameActionOnTarget;

type
  TActionPlayerRandomChat = class(TActionOnTarget)
  strict private
    procedure Perform;
  public
    procedure Update(const SecondsPassed: Single); override;
  end;

  TActionPlayerRandomChatData = class(TActionOnTargetData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  GameSimpleSerializableObject, GameSerializableData,
  GameLog, GameColors,
  GameActor,
  GameRandom;

{$DEFINE DataClass:=TActionPlayerRandomChatData}
{$INCLUDE actiontypecasts.inc}

procedure TActionPlayerRandomChat.Perform;
var
  Message: String;
begin
  case Rnd.Random(10) of
    0: Message := 'Hi!';
    1: Message := 'Hey!';
    2: Message := 'Hello!';
    3: Message := 'How r u?';
    4: Message := 'Good day!';
    5: Message := 'All good?';
    6: Message := 'Greetings!';
    7: Message := 'Stay safe!';
    8: Message := 'Who''s next?';
    9: Message := 'Hello there!';
    else
    begin
      ShowError('Unexpected message in RandomChat');
      Message := 'Huh..?';
    end;
  end;
  ParentActor.Particle(Message, ColorParticleChat);
end;

procedure TActionPlayerRandomChat.Update(const SecondsPassed: Single);
begin
  Perform;
  ActionFinished;
end;

{ TActionPlayerRandomChatData -------------------------------}

function TActionPlayerRandomChatData.Action: TActionClass;
begin
  Exit(TActionPlayerRandomChat);
end;

initialization
  RegisterSimpleSerializableObject(TActionPlayerRandomChat);
  RegisterSerializableData(TActionPlayerRandomChatData);

end.

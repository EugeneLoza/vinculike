{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerIdentifyMapItem;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameSimpleSerializableObject,
  GameMapItem,
  GameActionAbstract;

type
  TActionPlayerIdentifyMapItem = class(TActionAbstract)
  public const
    DurationBase = Single(10);
    MinDuration = Single(5);
  strict private
    AccumulatedXP: Single;
    Phase: Single;
    IdentifiedCount: Integer;
  public
    // WARNING!!! If different characters may use this in parallel - they may perform different actions on the same item, resulting in hard-to-find bugs
    MapItem: TMapItem;
  public
    procedure Save(const Element: TDOMElement); override;
    procedure AfterDeserealization; override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function ProposedTimeSpeed: Single; override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    procedure Stop; override;
  end;

  TActionPlayerIdentifyMapItemData = class(TActionAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameEnchantmentAbstract,
  GameActor, GamePlayerCharacter, GameSounds, GameRandom;

{$DEFINE DataClass:=TActionPlayerIdentifyMapItemData}
{$INCLUDE actiontypecasts.inc}

procedure TActionPlayerIdentifyMapItem.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
  Element.AttributeSet('AccumulatedXP', AccumulatedXP);
  Element.AttributeSet('IdentifiedCount', IdentifiedCount);
  Element.AttributeSet('MapItem', MapItem.ReferenceId);
end;

procedure TActionPlayerIdentifyMapItem.AfterDeserealization;
begin
  inherited AfterDeserealization;
  //TODO: MapItem
end;

procedure TActionPlayerIdentifyMapItem.Load(const Element: TDOMElement);
begin
  inherited;
  Phase := Element.AttributeSingle('Phase');
  AccumulatedXP := Element.AttributeSingle('AccumulatedXP');
  IdentifiedCount := Element.AttributeInteger('IdentifiedCount');
  MapItem := ObjectByReferenceId(Element.AttributeQWord('MapItem')) as TMapItem; // MapItems are deserealized before Player characters, so this should be fine ====== TODO: Move into AfterDeserealization
end;

function TActionPlayerIdentifyMapItem.NoiseMultiplier: Single;
begin
  Exit(1.2);
end;

function TActionPlayerIdentifyMapItem.NoiseAddition: Single;
begin
  try
    {will crash if MapItem or MapItem.Item have been externally altered}
    Exit(MapItem.Item.ItemData.Noise * 2);
  except
    ShowError('Error in %s.NoiseAddition, action is no longer possible', [Self.ClassName]);
    ActionFinished;
    Exit(0.0);
  end;
end;

function TActionPlayerIdentifyMapItem.ProposedTimeSpeed: Single;
begin
  Exit(5.0);
end;

procedure TActionPlayerIdentifyMapItem.Start;
begin
  inherited Start;
  ShowLog('%s attempts at identifying %s', [ParentPlayer.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogActionStart);
  MapItem.InteractingActorReferenceID := ParentActor.ReferenceId;
  IdentifiedCount := 0;
end;

procedure TActionPlayerIdentifyMapItem.Update(const SecondsPassed: Single);
var
  E: TEnchantmentAbstract;
  EverythingIdentified: Boolean;
  CountCannotBeIdentified: Integer;
  IdentificationStrength: Single;
begin
  // if during equip - a bondage item got equipped or inventory unexpectedly changed otherwise
  if not ParentPlayer.Inventory.CanUseHands then
  begin
    ShowLog('%s is restrained and can no longer identify %s', [ParentActor.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogCancel);
    MapItem.InteractingActorReferenceID := 0;
    MapItem := nil;
    ActionFinished;
    Exit;
  end;

  Phase += SecondsPassed;
  if Phase > MinDuration then
  begin
    EverythingIdentified := true;
    CountCannotBeIdentified := 0;
    for E in MapItem.Item.Enchantments do
      if not E.Identified then
      begin
        IdentificationStrength := E.IdentificationThreshold / Sqrt(ParentPlayer.Experience.Level + Sqrt(MapItem.Item.TimeWorn));
        if IdentificationStrength < 1 then
        begin
          EverythingIdentified := false;
          if Phase - MinDuration > IdentificationStrength * DurationBase then
          begin
            Phase -= IdentificationStrength * DurationBase;
            E.Identified := true;
            if E.IsPositive then
              ShowLog('%s: %s', [MapItem.Item.Data.DisplayName, Format(E.Description, [])], ColorUiEnchantmentStochasticPositive)
            else
              ShowLog('%s: %s', [MapItem.Item.Data.DisplayName, Format(E.Description, [])], ColorUiEnchantmentStochasticNegative);
            Inc(IdentifiedCount);
            if E.IdentificationThreshold > 0 then
              AccumulatedXP += 8 * E.IdentificationThreshold; // IdentificationThreshold is Sqrt(CurrentDepth) on avarage; so level 20 is 4 // 4x8=32 per enchantment (item can contain many enchantments)
          end;
        end else
          Inc(CountCannotBeIdentified);
      end;

    if EverythingIdentified then
    begin
      if IdentifiedCount > 0 then
      begin
        if CountCannotBeIdentified = 0 then
          ShowLog('%s identified all enchantments on %s', [ParentActor.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogInventory)
        else
          ShowLog('%s identified %d out of %d unknown enchantments on %s', [ParentActor.Data.DisplayName, IdentifiedCount, IdentifiedCount + CountCannotBeIdentified, MapItem.Item.Data.DisplayName], ColorLogInventory);
        if AccumulatedXP > 1 then
          ParentPlayer.Experience.AddExperience(AccumulatedXP);
        Sound(MapItem.Item.ItemData.SoundRepair);
      end else
        ShowLog('%s failed to identify any enchantments on %s', [ParentActor.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorUiItemStochasticDamaged);
      MapItem.InteractingActorReferenceID := 0;
      MapItem := nil;
      ActionFinished;
      Exit;
    end;
  end
end;

class function TActionPlayerIdentifyMapItem.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionPlayerIdentifyMapItem).Phase := 0;
end;

procedure TActionPlayerIdentifyMapItem.Stop;
begin
  inherited;
  if MapItem <> nil then
  begin
    if AccumulatedXP > 1 then
      ParentPlayer.Experience.AddExperience(AccumulatedXP);
    ShowLog('%s gives up trying to identify %s', [ParentActor.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogCancel);
    MapItem.InteractingActorReferenceID := 0;
    MapItem := nil;
  end;
end;

{ TActionPlayerIdentifyMapItemData -------------------------------}

function TActionPlayerIdentifyMapItemData.Action: TActionClass;
begin
  Exit(TActionPlayerIdentifyMapItem);
end;

initialization
  RegisterSimpleSerializableObject(TActionPlayerIdentifyMapItem);
  RegisterSerializableData(TActionPlayerIdentifyMapItemData);

end.

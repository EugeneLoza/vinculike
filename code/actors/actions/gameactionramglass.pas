{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionRamGlass;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameActionAbstract, GameActionOnTarget, GameActionRoll, GameItemData,
  GameUnlockableEntry;

type
  TActionRamGlass = class(TActionOnTarget)
  strict private
    Phase: Single;
    SubActionRoll: TActionRoll;
    procedure FinishRoll;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function CanStop: Boolean; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    destructor Destroy; override;
  end;

  TActionRamGlassData = class(TActionOnTargetData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    RollWarmUp: Single;
    GlassTopItem, GlassBottomItem: TItemData;
    function Description: TEntriesList; override;
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleVectors, CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData, GameGarbageCollector,
  GameActor, GamePlayerCharacter, GameActionKnockback, GameApparelSlots, GameMonster,
  GameInventoryItem, GameItemsDatabase,
  GameViewGame, GameLog, GameColors, GameRandom, GameStats, GameSounds, GameDifficultyLevel,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TActionRamGlassData}
{$INCLUDE actiontypecasts.inc}

procedure TActionRamGlass.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
  if SubActionRoll <> nil then // TODO: It most likely will crash Loading --- but can happen as soon as action started but didn't receive Update yet.
    SubActionRoll.Save(Element.CreateChild('SubActionRoll'));
end;

procedure TActionRamGlass.Load(const Element: TDOMElement);
var
  SubActionRollElement: TDOMElement;
begin
  inherited Load(Element);
  Phase := Element.AttributeSingle('Phase');
  SubActionRollElement := Element.ChildElement('SubActionRoll', false);
  if SubActionRollElement <> nil then
  begin
    SubActionRoll := TActionAbstract.LoadClass(SubActionRollElement) as TActionRoll;
    SubActionRoll.OnActionFinished := @FinishRoll;
  end else
    SubActionRoll := nil;
end;

function TActionRamGlass.NoiseMultiplier: Single;
begin
  Exit(1.1);
end;

function TActionRamGlass.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionRamGlass.CanStop: Boolean;
begin
  Exit(Phase < ActionData.RollWarmUp * Difficulty.MonsterAttackDelayMultiplier / 10); // "/10" because otherwise it won't be as dumb as it should be
end;

procedure TActionRamGlass.FinishRoll;
begin
  if SubActionRoll <> nil then
    SubActionRoll.Stop;
  GarbageCollector.AddAndNil(SubActionRoll);
  Phase := 0;
end;

procedure TActionRamGlass.Update(const SecondsPassed: Single);
var
  E: TApparelSlot;
begin
  inherited;

  if not TargetPlayer.CanAct then
  begin
    ActionFinished;
    Exit;
  end;

  Phase += SecondsPassed;
  if (SubActionRoll = nil) and (Phase >= ActionData.RollWarmUp * Difficulty.MonsterAttackDelayMultiplier) then
  begin
    SubActionRoll := TActionRoll.NewAction(Parent) as TActionRoll;
    TActionRoll(SubActionRoll).MoveVector := Vector2(TargetPlayer.CenterX - ParentActor.CenterX, TargetPlayer.CenterY - ParentActor.CenterY);
    TActionRoll(SubActionRoll).MoveVectorNormalized := TActionRoll(SubActionRoll).MoveVector.Normalize;
    TActionRoll(SubActionRoll).MoveVector := 15 * TActionRoll(SubActionRoll).MoveVectorNormalized;
    SubActionRoll.Start;
    SubActionRoll.OnActionFinished := @FinishRoll;
  end;
  if SubActionRoll <> nil then
  begin
    if ParentActor.Collides(TargetPlayer, 0) or ViewGame.BuggyLargeTimeFlowSpeed then
    begin
      Phase := 0;
      LocalStats.IncStat(Data.ClassName);
      SubActionRoll.Stop;
      GarbageCollector.AddAndNil(SubActionRoll);

      if TargetPlayer.Inventory.HasItem(ActionData.GlassTopItem) and TargetPlayer.Inventory.HasItem(ActionData.GlassBottomItem) then
      begin
        //reinforce both
        TargetPlayer.Inventory.Equipped[ActionData.GlassTopItem.MainSlot].MaxDurability += Sqrt(Rnd.Random) * ActionData.GlassTopItem.Durability;
        TargetPlayer.Inventory.Equipped[ActionData.GlassTopItem.MainSlot].Durability := (TargetPlayer.Inventory.Equipped[ActionData.GlassTopItem.MainSlot].Durability + TargetPlayer.Inventory.Equipped[ActionData.GlassTopItem.MainSlot].MaxDurability) / 2.0;
        TargetPlayer.Inventory.Equipped[ActionData.GlassBottomItem.MainSlot].MaxDurability += Sqrt(Rnd.Random) * ActionData.GlassBottomItem.Durability;
        TargetPlayer.Inventory.Equipped[ActionData.GlassBottomItem.MainSlot].Durability := (TargetPlayer.Inventory.Equipped[ActionData.GlassBottomItem.MainSlot].Durability + TargetPlayer.Inventory.Equipped[ActionData.GlassBottomItem.MainSlot].MaxDurability) / 2.0;
        Sound(ActionData.GlassTopItem.SoundRepair);
        ParentActor.Health := -1;
        ShowLog('%s splashes all over %s''s body adding another layer to her encasing', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogBondage);
      end else
      if not TargetPlayer.Inventory.HasItem(ActionData.GlassTopItem) and (TargetPlayer.Inventory.HasItem(ActionData.GlassBottomItem) or Rnd.RandomBoolean) then
      begin
        if TargetPlayer.Inventory.TopCovered > 0 then
        begin
          repeat
            E := TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItemOrBondage;
          until TargetPlayer.Inventory.Equipped[E].ItemData.CoversTop;
          ShowLog('%s brutally rams into %s, knocking off her %s', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].ItemData.DisplayName], ColorLogItemSteal);
          Sound(ActionData.HitSound);
          Sound(TargetPlayer.Inventory.Equipped[E].ItemData.SoundUnequip);
          TargetPlayer.Inventory.DisintegrateOrDrop(E, true);
        end else
        begin
          for E in ActionData.GlassTopItem.EquipSlots do
            TargetPlayer.Inventory.DisintegrateOrDrop(E, true);
          TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ActionData.GlassTopItem));
          Sound(ActionData.GlassTopItem.SoundEquip);
          ShowLog('%s splashes all over %s'' upper body becoming a transparent casing around her', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogBondage);
          ParentMonster.Health := -1;
        end;
      end else
      if not TargetPlayer.Inventory.HasItem(ActionData.GlassBottomItem) then
      begin
        if TargetPlayer.Inventory.BottomCovered > 0 then
        begin
          repeat
            E := TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItemOrBondage;
          until TargetPlayer.Inventory.Equipped[E].ItemData.CoversBottom;
          ShowLog('%s brutally rams into %s, knocking off her %s', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].ItemData.DisplayName], ColorLogItemSteal);
          Sound(ActionData.HitSound);
          Sound(TargetPlayer.Inventory.Equipped[E].ItemData.SoundUnequip);
          TargetPlayer.Inventory.DisintegrateOrDrop(E, true);
        end else
        begin
          for E in ActionData.GlassBottomItem.EquipSlots do
            TargetPlayer.Inventory.DisintegrateOrDrop(E, true);
          TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ActionData.GlassBottomItem));
          Sound(ActionData.GlassBottomItem.SoundEquip);
          ShowLog('%s splashes all over %s'' lower body becoming a transparent casing around her', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogBondage);
          ParentMonster.Health := -1;
        end;
      end else
      begin
        ShowError('Unexpected situation in GameActionRamGlass');
        ParentMonster.Ai.AiFlee := true;
      end;
      // Push target (in all cases)

      if Rnd.Random > TargetPlayer.ResistKnockback then
      begin
        TargetPlayer.CurrentAction := TActionKnockback.NewAction(Target);
        TActionKnockback(TargetPlayer.CurrentAction).MoveVector := Vector2(TargetPlayer.CenterX - ParentActor.CenterX, TargetPlayer.CenterY - ParentActor.CenterY);
        TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized := TActionKnockback(TargetPlayer.CurrentAction).MoveVector.Normalize;
        TActionKnockback(TargetPlayer.CurrentAction).MoveVector := 20 * TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized;
        TargetPlayer.CurrentAction.Start;
        TargetPlayer.AddResistKnockback;
        ViewGame.ShakeMap;
      end else
        ShowLog('%s manages to hold the ground', [TargetActor.Data.DisplayName], ColorLogStaminaDamage);
    end else
      SubActionRoll.Update(SecondsPassed);
  end;
end;

class function TActionRamGlass.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionRamGlass).Phase := 0;
end;

destructor TActionRamGlass.Destroy;
begin
  GarbageCollector.FreeNowAndNil(SubActionRoll);
  inherited Destroy;
end;

{ TActionRamGlassData -------------------------------}

procedure TActionRamGlassData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if RollWarmUp < 0 then
    raise EDataValidationError.CreateFmt('RollWarmUp must be >= 0 in %s', [ClassName]);
end;

procedure TActionRamGlassData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  RollWarmUp := Element.AttributeSingle('RollWarmUp');

  GlassTopItem := ItemsDataDictionary[Element.AttributeString('GlassTopItem')] as TItemData;
  GlassBottomItem := ItemsDataDictionary[Element.AttributeString('GlassBottomItem')] as TItemData;
end;

function TActionRamGlassData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will try to come into contact with the target and splash itself around it. If target has equipped items in slot it was intended to take (upper or lower body), will first knock those off. If all necessary slots were free, will encase upper or lower body into a sturdy flexible transparent casing. If target is already wrapped in one, will add another layer on top of it.', []),
    Classname, 1));
end;

function TActionRamGlassData.Action: TActionClass;
begin
  Exit(TActionRamGlass);
end;

initialization
  RegisterSimpleSerializableObject(TActionRamGlass);
  RegisterSerializableData(TActionRamGlassData);

end.


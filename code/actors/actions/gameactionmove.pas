{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionMove;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  CastleVectors,
  GameSimpleSerializableObject,
  GameMapTypes,
  GameActionMoveAbstract, GameActionAbstract;

type
  TActionMove = class(TActionMoveAbstract)
  strict private
    DestinationX, DestinationY: Single; // used only to LoadGame -> AfterDeserealization
  public
    CurrentWaypoint: Integer;
    Waypoints: TFloatCoordList;
    MoveVector, MoveVectorNormalized: TVector2;
    MovePhase: Single;
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function RemainingDistance: Single; override;
    function RemainingTime: Single; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    procedure MoveTo(const ToX, ToY: Single); override;
  public
    procedure Save(const Element: TDOMElement); override;
    procedure AfterDeserealization; override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    destructor Destroy; override;
  end;

  TActionMoveData = class(TActionMoveAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableData,
  GameActor, GameMonster, GameMap, GameLog, GameViewGame;

{$DEFINE DataClass:=TActionMoveData}
{$INCLUDE actiontypecasts.inc}

function TActionMove.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionMove.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionMove.RemainingDistance: Single;
begin
  Exit(1.5 * CurrentWaypoint);
end;

function TActionMove.RemainingTime: Single;
begin
  Exit(1.5 * CurrentWaypoint / ParentActor.Data.MovementSpeed);
end;

procedure TActionMove.Update(const SecondsPassed: Single);
var
  V: TVector2;
begin
  MovePhase += SecondsPassed * ParentActor.GetSpeed;
  while MovePhase > MoveVector.Length do
  begin
    MovePhase -= MoveVector.Length;
    Dec(CurrentWaypoint);
    ParentActor.HurtFeet(MoveVector.Length * FeetDamagePerTile);
    ParentActor.MoveMeTo(Waypoints[CurrentWaypoint][0], Waypoints[CurrentWaypoint][1]);
    if not Map.CanMove(ParentActor.LastTile) then
      ShowError('Nav grid error in TActionMove.Update: %d,%d is not walkable', [ParentActor.LastTileX, ParentActor.LastTileY]);

    ParentActor.UpdateVisible;
    if CurrentWaypoint = 0 then // Arrived at destination
    begin
      FreeAndNil(Waypoints);
      MovePhase := -1;
      ActionFinished;
      Exit; // it may be not the last iteration of While loop
    end else
    begin
      MoveVector := Vector2(Waypoints[Pred(CurrentWaypoint)][0] - Waypoints[CurrentWaypoint][0], Waypoints[Pred(CurrentWaypoint)][1] - Waypoints[CurrentWaypoint][1]);
      MoveVectorNormalized := MoveVector.Normalize;
    end;
  end;
  V := MovePhase * MoveVectorNormalized;
  ParentActor.MoveMeToSafe(Waypoints[CurrentWaypoint][0] + V.X, Waypoints[CurrentWaypoint][1] + V.Y);
end;

class function TActionMove.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionMove).CurrentWaypoint := 0;
  (Result as TActionMove).MovePhase := 0;
end;

procedure TActionMove.MoveTo(const ToX, ToY: Single);
var
  ToXInt, ToYInt, GotoX, GotoY: Int16;
  {$IFNDEF DijkstraPathfinding}
  Seed: TIntCoordList;
  {$ENDIF}
begin
  if not Map.PassableTiles[ParentActor.PredSize][ParentActor.lastTile] then
    ShowError('Last tile is not walkable in TActionMove.MoveTo: %d,%d', [ParentActor.LastTileX, ParentActor.LastTileY]);

  ToXInt := Round(ToX); // note, not TRUNC, because we want "nearest" tile
  ToYInt := Round(ToY);
  Map.GetNearestPassableTile(ParentActor.PredSize, Parent <> ViewGame.CurrentCharacter, ToXInt, ToYInt, GotoX, GotoY);

  {$IFNDEF DijkstraPathfinding}
  Seed := TIntCoordList.Create;
  Seed.Add(IntCoord(ParentActor.LastTileX, ParentActor.LastTileY));
  {$ENDIF}
  if (ToXInt = GotoX) and (ToYInt = GotoY) then
    Waypoints := Map.GetWaypoints(
      {$IFDEF DijkstraPathfinding}
      Map.GenerateDijkstra(ParentActor.PredSize, Parent <> ViewGame.CurrentCharacter,
      ParentActor.LastTileX, ParentActor.LastTileY, GotoX, GotoY),
      {$ELSE}
      Map.DistanceMap(ParentActor.PredSize, Parent <> ViewGame.CurrentCharacter, Seed, true, GotoX, GotoY),
      {$ENDIF}
      ParentActor.X, ParentActor.Y, ToX, ToY, ParentActor.LastTileX, ParentActor.LastTileY
    )
  else
    Waypoints := Map.GetWaypoints(
      {$IFDEF DijkstraPathfinding}
      Map.GenerateDijkstra(ParentActor.PredSize,Parent <> ViewGame.CurrentCharacter,
      ParentActor.LastTileX, ParentActor.LastTileY, GotoX, GotoY),
      {$ELSE}
      Map.DistanceMap(ParentActor.PredSize, Parent <> ViewGame.CurrentCharacter, Seed, true, GotoX, GotoY),
      {$ENDIF}
      ParentActor.X, ParentActor.Y, GotoX, GotoY, ParentActor.LastTileX, ParentActor.LastTileY
    );
  {$IFNDEF DijkstraPathfinding}
  FreeAndNil(Seed);
  {$ENDIF}
  CurrentWaypoint := Pred(Waypoints.Count);
  {if Waypoints = nil then
  begin
    // "magic" value to report "no way to reach target"
    OnFinished;
    Exit;
  end;}

  //DebugLog(Waypoints.Count.ToString);
  // Map.GetWaypoints is guaranteed to give at least 2 waypoints!
  // Waypoint[0] === (X,Y)
  MoveVector := Vector2(Waypoints[Pred(CurrentWaypoint)][0] - ParentActor.X, Waypoints[Pred(CurrentWaypoint)][1] - ParentActor.Y);
  MoveVectorNormalized := MoveVector.Normalize;
end;

procedure TActionMove.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  // To avoid painful saving everything we save... only destination.
  Element.AttributeSet('DestinationX', Waypoints[Pred(Waypoints.Count)][0]);
  Element.AttributeSet('DestinationY', Waypoints[Pred(Waypoints.Count)][1]);
end;

procedure TActionMove.AfterDeserealization;
begin
  inherited AfterDeserealization;
  MoveTo(DestinationX, DestinationY); // TODO: Maybe better save waypoints?
end;

procedure TActionMove.Load(const Element: TDOMElement);
begin
  inherited;
  DestinationX := Element.AttributeSingle('DestinationX');
  DestinationY := Element.AttributeSingle('DestinationY');
end;

destructor TActionMove.Destroy;
begin
  FreeAndNil(Waypoints);
  inherited Destroy;
end;

{ TActionMoveData -------------------------------}

function TActionMoveData.Action: TActionClass;
begin
  Exit(TActionMove);
end;

initialization
  RegisterSimpleSerializableObject(TActionMove);
  RegisterSerializableData(TActionMoveData);

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionRoll;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  CastleVectors,
  GameSimpleSerializableObject,
  GameActionAbstract;

type
  TActionRoll = class(TActionAbstract)
  public
    MovePhase: Single;
    MoveVector, MoveVectorNormalized: TVector2;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    function CanStop: Boolean; override;
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
  end;

  { This is an internal action and doesn't need data
    But as some others inherit from it, maybe it'll be useful as parent }
  TActionRollData = class(TActionAbstractData)
  public
    function Action: TActionClass; override;
  end;


implementation
uses
  CastleXmlUtils,
  GameSerializableData,
  GamePlayerCharacter, GameActor;

{$DEFINE DataClass:=TActionRollData}
{$INCLUDE actiontypecasts.inc}

procedure TActionRoll.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('MovePhase', MovePhase);
  Element.AttributeSet('MoveVector', MoveVector);
end;

procedure TActionRoll.Load(const Element: TDOMElement);
begin
  inherited;
  MovePhase := Element.AttributeSingle('MovePhase');
  MoveVector := Element.AttributeVector2('MoveVector');
  MoveVectorNormalized := MoveVector.Normalize;
end;

function TActionRoll.CanStop: Boolean;
begin
  //inherited - no need;
  //Exit(MovePhase > MoveVector.Length * 0.8);
  Exit(true);
end;

function TActionRoll.NoiseMultiplier: Single;
begin
  Exit(1.2);
end;

function TActionRoll.NoiseAddition: Single;
begin
  Exit(0.0);
end;

procedure TActionRoll.Update(const SecondsPassed: Single);
const
  MinDeltaSeconds = Single(0.01);
var
  RemainingSeconds: Single;
  DeltaSeconds: Single;
  OldTile: SizeInt;
begin
  (* TODO: BUG
     if SecondsPassed is large enough we can "jump over a tile".
     This may be very bad for navmesh and can even lead to a crash due to navmesh error *)
  RemainingSeconds := SecondsPassed;
  while RemainingSeconds > 0 do
  begin
    if RemainingSeconds > MinDeltaSeconds then
    begin
      DeltaSeconds := MinDeltaSeconds;
      RemainingSeconds -= MinDeltaSeconds;
    end else
    begin
      DeltaSeconds := RemainingSeconds;
      RemainingSeconds := 0;
    end;

    OldTile := ParentActor.LastTile;

    MovePhase += DeltaSeconds * ParentActor.Data.RollSpeed; // bare value without modifiers? TODO: think it over

    if ParentActor.MoveMeToDelta(DeltaSeconds * ParentActor.GetRollSpeed, MoveVectorNormalized.X, MoveVectorNormalized.Y) then
      if OldTile <> ParentActor.LastTile then
        ParentActor.UpdateVisible;

    if MovePhase > MoveVector.Length then
    begin
      ActionFinished;
      Exit;
    end;
  end;
end;

class function TActionRoll.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionRoll).MovePhase := 0;
  if AParent is TPlayerCharacter then
    TPlayerCharacter(AParent).Inventory.WardrobeMalfunction;
end;

{ TActionRollData ------------------------------ }

function TActionRollData.Action: TActionClass;
begin
  Exit(TActionRoll);
end;

initialization
  RegisterSimpleSerializableObject(TActionRoll);
  RegisterSerializableData(TActionRollData);

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionMoveAbstract;

{$INCLUDE compilerconfig.inc}

interface

uses
  GameActionAbstract;

type
  TActionMoveAbstract = class abstract(TActionAbstract)
  protected
    const FeetDamagePerTile = 2 / 100; {2 durability damage per 100 tiles}
  public
  { Very rough approximation of remaining distance and remaining time of the route
    It's based ONLY on number of waypoints to pass and considers waypoints do diagonal movement
    I.e. this _should_ be max of the proper values
    It's used only for monster's AI and doesn't need higher accuracy for now }
    function RemainingDistance: Single; virtual; abstract;
    function RemainingTime: Single; virtual; abstract;
    procedure MoveTo(const ToX, ToY: Single); virtual; abstract;
    procedure MoveTo(const Actor: TObject);
  end;
  TActionMoveClass = class of TActionMoveAbstract;

  TActionMoveAbstractData = class(TActionAbstractData)
  end;
  //TActionMoveDataClass = class of TActionMoveAbstractData;

implementation
uses
  GameActor;

procedure TActionMoveAbstract.MoveTo(const Actor: TObject);
begin
  MoveTo(TActor(Actor).CenterX, TActor(Actor).CenterY);
end;

end.


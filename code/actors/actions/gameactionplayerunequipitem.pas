{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerUnequipItem;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameSimpleSerializableObject,
  GameApparelSlots,
  GameActionAbstract;

type
  TActionPlayerUnequipItem = class(TActionAbstract)
  public const
    Duration = 0.2;
  strict private
    Phase: Single;
    procedure Perform;
  public
    // WARNING!!! item may be force-unequipped/force-equipped here!, resulting in hard-to-find bugs
    ItemSlot: TApparelSlot;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function ProposedTimeSpeed: Single; override;
    procedure Start; override;
    procedure Stop; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
  end;

  TActionPlayerUnequipItemData = class(TActionAbstractData)
  public
    function Action: TActionClass; override;
  end;


implementation
uses
  CastleXmlUtils,
  GameSerializableData,
  GameActor, GamePlayerCharacter, GameSounds, GameTranslation, GameLog, GameColors,
  GameEnumUtils;

{$DEFINE DataClass:=TActionPlayerUnequipItemData}
{$INCLUDE actiontypecasts.inc}

procedure TActionPlayerUnequipItem.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
  Element.AttributeSet('ItemSlot', specialize EnumToStr<TApparelSlot>(ItemSlot));
end;

procedure TActionPlayerUnequipItem.Load(const Element: TDOMElement);
begin
  inherited;
  Phase := Element.AttributeSingle('Phase');
  ItemSlot := specialize StrToEnum<TApparelSlot>(Element.AttributeString('ItemSlot'));
end;

function TActionPlayerUnequipItem.NoiseMultiplier: Single;
begin
  Exit(1.1);
end;

function TActionPlayerUnequipItem.NoiseAddition: Single;
begin
  Exit(ParentPlayer.Inventory.GetItemNoiseSafe(ItemSlot));
end;

function TActionPlayerUnequipItem.ProposedTimeSpeed: Single;
begin
  Exit(4.0);
end;

procedure TActionPlayerUnequipItem.Start;
begin
  inherited Start;
  ShowLog(GetTranslation('ActorStartsUnequippingItemLog'), [ParentActor.Data.DisplayName, ParentPlayer.Inventory.Equipped[ItemSlot].Data.DisplayName, TActionPlayerUnequipItem.Duration], ColorLogActionStart);
end;

procedure TActionPlayerUnequipItem.Stop;
begin
  inherited;
  if ParentPlayer.Inventory.Equipped[ItemSlot] <> nil then
    ShowLog(GetTranslation('ActorCancelUnequipItemLog'), [ParentActor.Data.DisplayName, ParentPlayer.Inventory.Equipped[ItemSlot].Data.DisplayName], ColorLogCancel);
end;

procedure TActionPlayerUnequipItem.Perform;
begin
  ShowLog(GetTranslation('ActorUnequipsItemLog'), [ParentActor.Data.DisplayName, ParentPlayer.Inventory.Equipped[ItemSlot].Data.DisplayName], ColorLogUnequip);
  Sound(ParentPlayer.Inventory.Equipped[ItemSlot].ItemData.SoundUnequip);
  ParentPlayer.Inventory.UnequipAndDrop(ItemSlot, false);
end;

procedure TActionPlayerUnequipItem.Update(const SecondsPassed: Single);
begin
  inherited;
  Phase += SecondsPassed;
  // if during unequip - a bondage item got equipped or inventory unexpectedly changed otherwise
  if (ParentPlayer.Inventory.Equipped[ItemSlot] = nil) or ParentPlayer.Inventory.Bondage[ItemSlot] then
  begin
    ShowLog('%s can no longer unequip the item', [ParentActor.Data.DisplayName], ColorLogCancel);
    ActionFinished;
    Exit;
  end;
  if Phase > Duration then
  begin
    Perform;
    ActionFinished;
    Exit;
  end
end;

class function TActionPlayerUnequipItem.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionPlayerUnequipItem).Phase := 0;
end;

{ TActionPlayerUnequipItemData -------------------------------}

function TActionPlayerUnequipItemData.Action: TActionClass;
begin
  Exit(TActionPlayerUnequipItem);
end;

initialization
  RegisterSimpleSerializableObject(TActionPlayerUnequipItem);
  RegisterSerializableData(TActionPlayerUnequipItemData);

end.

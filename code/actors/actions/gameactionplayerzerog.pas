{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerZeroG;

{$INCLUDE compilerconfig.inc}

interface

uses
  GameActionAbstract, GameActionPlayerStunnedAbstract;

type
  TActionPlayerZeroG = class(TActionPlayerStunnedAbstract)
  protected
    procedure CannotStopStunned; override;
    procedure StopStunned; override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
  public
    FallDamage: Single;
  end;

  TActionPlayerZeroGData = class(TActionAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  GameSimpleSerializableObject, GameSerializableData,
  GamePlayerCharacter, GameLog, GameColors;

{$DEFINE DataClass:=TActionPlayerZeroGData}
{$INCLUDE actiontypecasts.inc}

procedure TActionPlayerZeroG.CannotStopStunned;
begin
  ShowLog('%s waves her limbs in panic but can''t move an inch', [ParentPlayer.Data.DisplayName], ColorLogNotEnoughStamina);
end;

procedure TActionPlayerZeroG.StopStunned;
begin
  ShowLog('%s suddenly collapses on the ground hurting herself by %.1n', [ParentPlayer.Data.DisplayName, FallDamage], ColorLogCancel);
  ParentPlayer.Hit(FallDamage);
end;

function TActionPlayerZeroG.NoiseMultiplier: Single;
begin
  Exit(1.6);
end;

function TActionPlayerZeroG.NoiseAddition: Single;
begin
  Exit(10.0);
end;

{ TActionPlayerZeroGData -------------------------------}

function TActionPlayerZeroGData.Action: TActionClass;
begin
  Exit(TActionPlayerZeroG);
end;

initialization
  RegisterSimpleSerializableObject(TActionPlayerZeroG);
  RegisterSerializableData(TActionPlayerZeroGData);

end.


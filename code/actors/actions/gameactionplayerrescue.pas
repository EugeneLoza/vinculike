{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerRescue;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameSimpleSerializableObject,
  GameActionAbstract, GameActionOnTarget;

const
  XpPerRescue = 50;
  XpPerDungeonLevel = 10;
  XpPerRescueeLevel = 5;

type
  TActionPlayerRescue = class(TActionOnTarget)
  public const
    Duration = Single(7);
  strict private
    NextProgressSound: Single;
    Phase: Single;
    procedure Perform;
    function TargetName: String;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    procedure Stop; override;
  end;

  TActionPlayerRescueData = class(TActionOnTargetData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableData,
  GameLog, GameColors, GameMath,
  GameActor, GamePlayerCharacter, GameSounds, GameTranslation,
  GameInventoryItem, GameItemsDatabase, GameMap, GameRandom;

{$DEFINE DataClass:=TActionPlayerRescueData}
{$INCLUDE actiontypecasts.inc}

procedure TActionPlayerRescue.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
end;

procedure TActionPlayerRescue.Load(const Element: TDOMElement);
begin
  inherited;
  Phase := Element.AttributeSingle('Phase');
end;

function TActionPlayerRescue.NoiseMultiplier: Single;
begin
  Exit(1.5);
end;

function TActionPlayerRescue.NoiseAddition: Single;
begin
  Exit(48.0);
end;

procedure TActionPlayerRescue.Perform;
var
  RemainingCaptured: Integer;
begin
  if TargetPlayer.IsStranger then
  begin
    ShowLog('%s finally manages to unlock all the straps and locks', [ParentActor.Data.DisplayName], ColorLogCharacterRescued);
    ShowLog('The tight mask around the face was the trickiest one, having a bunch', [], ColorLogCharacterRescued);
    ShowLog('of tubes attached to it most likely going directly into stomach and lungs', [], ColorLogCharacterRescued);
    ShowLog('How long has the poor girl been here? %s can''t get any meaningful reply', [ParentActor.Data.DisplayName], ColorLogCharacterRescued);
    ShowLog('from the delirious naked prisoner, except for her name: %s', [TargetPlayer.Data.DisplayName], ColorLogCharacterRescued);
    if Map.UnlockedCharacters = 1 then
    begin
      ShowLog('Wondering of how to get her out, %s noticees a flashing gem on the rescuee''s collar', [ParentActor.Data.DisplayName], ColorLogCharacterRescued);
      ShowLog('As she touches it, %s disappears with a bright flash', [TargetPlayer.Data.DisplayName], ColorLogCharacterRescued);
      ShowLog('What the hell is going on here?', [], ColorLogCharacterRescued);
    end else
    begin
      ShowLog('%s presses the sparkling cyan gem on rescuee''s collar', [ParentActor.Data.DisplayName], ColorLogCharacterRescued);
      ShowLog('And with a flash %s disappears, teleported hopefully somewhere safer', [TargetPlayer.Data.DisplayName], ColorLogCharacterRescued);
    end;
  end else
  begin
    ShowLog('After struggling with locks and chains %s eventually manages to free %s', [ParentActor.Data.DisplayName, TargetName], ColorLogCharacterRescued);
    ShowLog('The only thing on her naked body is a tight collar with an encrusted cyan gem', [], ColorLogCharacterRescued);
    ShowLog('When the last binds fall, the jewel starts pulsing with magic light', [], ColorLogCharacterRescued);
    ShowLog('As %s touches it, with a bright flash %s disappears teleported to the settlement to recover', [ParentActor.Data.DisplayName, TargetName], ColorLogCharacterRescued);
  end;
  Sound('rescue_success'); // TODO: Temporary
  ParentPlayer.Experience.AddExperience(XpPerRescue + MinSingle(Sqrt(TargetPlayer.Experience.XpAccumulatedInThisRun) * 5, XpPerRescueeLevel * TargetPlayer.Experience.Level + XpPerDungeonLevel * Map.CurrentDepth));
  TargetPlayer.Experience.XpAccumulatedInThisRun := 0;

  TargetPlayer.IsCaptured := false;
  TargetPlayer.IsStranger := false;
  TargetPlayer.AtMap := 0;
  TargetPlayer.Reset;
  { TODO: this is a workaround. The problem is that after the player character got captured
    the same frame another attack may hit and equip some bondage item which will not be
    disintegrated in GetCaptured.
    Most likely this should be "fixed" by having the captured character have a specific restraints set
    when generating on level, so this workaround should be ok as temporary system }
  TargetPlayer.Inventory.UnequipAndDisintegrateEverything;
  TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['metal_collar']));
  TargetPlayer.Exhausted := 2; // TODO: maybe something better here
  Target := nil;
  // TODO: This may cause "Collection Modified" bug, as most likely this action happens in PlayerCharacter.Update
  Map.CacheCharactersOnThisLevel;
  RemainingCaptured := Map.CapturedCharactersAtLevel;
  if RemainingCaptured = 1 then
    ShowLog('There is one more character imprisoned on this level', [RemainingCaptured], ColorLogCapturedCharacterHere)
  else
  if RemainingCaptured > 1 then
    ShowLog('There are %d characters remain imprisoned on this level', [RemainingCaptured], ColorLogCapturedCharacterHere);
end;

function TActionPlayerRescue.TargetName: String;
begin
  if TargetPlayer.IsStranger then
    Exit('the stranger')
  else
    Exit(TargetPlayer.Data.DisplayName);
end;

procedure TActionPlayerRescue.Start;
begin
  inherited Start;
  NextProgressSound := 0;
  ShowLog('%s tries to free %s of her restraints (%.1n seconds)', [ParentActor.Data.DisplayName, TargetName, TActionPlayerRescue.Duration], ColorLogActionStart);
end;

procedure TActionPlayerRescue.Update(const SecondsPassed: Single);
begin
  Phase += SecondsPassed;
  if Phase > NextProgressSound then
  begin
    Sound('rescue_progress'); // TODO: Temporary
    NextProgressSound += Rnd.Random;
  end;
  if Phase > Duration then
  begin
    Perform;
    ActionFinished;
    Exit;
  end
end;

class function TActionPlayerRescue.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionPlayerRescue).Phase := 0;
end;

procedure TActionPlayerRescue.Stop;
begin
  inherited;
  if (Target <> nil)then
    ShowLog('%s gives up untying %s', [ParentActor.Data.DisplayName, TargetName], ColorLogCancel);
end;

{ TActionPlayerRescueData -------------------------------}

function TActionPlayerRescueData.Action: TActionClass;
begin
  Exit(TActionPlayerRescue);
end;

initialization
  RegisterSimpleSerializableObject(TActionPlayerRescue);
  RegisterSerializableData(TActionPlayerRescueData);

end.

{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionShootTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameProjectileAbstract,
  GameActionAbstract, GameActionOnTarget,
  GameUnlockableEntry;

type
  TActionShootTarget = class(TActionOnTarget)
  strict private
    Phase: Single;
    ProjectileNumber: Integer;
    procedure Perform;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function CanStop: Boolean; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
  end;

  TActionShootTargetData = class(TActionOnTargetData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    ShotDelay: Single;
    Projectiles: Integer;
    RandomAngle: Single;
    function Description: TEntriesList; override;
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleVectors, CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GameActor, GameMap, GameDifficultyLevel, GameValidated, GameRandom;

{$DEFINE DataClass:=TActionShootTargetData}
{$INCLUDE actiontypecasts.inc}

function TActionShootTarget.NoiseMultiplier: Single;
begin
  Exit(1.1);
end;

function TActionShootTarget.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionShootTarget.CanStop: Boolean;
begin
  Exit(ProjectileNumber = 0);
end;

procedure TActionShootTarget.Perform;
var
  Projectile: TProjectileAbstract;
  ThisAngle: Single;
begin
  Projectile := ActionData.MarkData.Mark.Create as TProjectileAbstract;

  Projectile.Parent := ParentActor;
  Projectile.Data := ActionData.MarkData;
  Projectile.SetSize(Projectile.Data.Size);
  Projectile.MoveVector := Vector2(TargetActor.CenterX - ParentActor.CenterX, TargetActor.CenterY - ParentActor.CenterY).Normalize;
  if ActionData.RandomAngle > 0 then
  begin
    ThisAngle := ActionData.RandomAngle * 2 * (Rnd.Random - 0.5);
    Projectile.MoveVector := RotatePoint2D(Projectile.MoveVector, ThisAngle);
  end;
  Projectile.MoveCenterTo(ParentActor);
  Map.MarksList.Add(Projectile);
end;

procedure TActionShootTarget.Update(const SecondsPassed: Single);
begin
  inherited;

  if not TargetActor.CanAct then //CanBeInteractedWith : TODO
  begin
    ActionFinished;
    Exit;
  end;

  Phase += SecondsPassed;
  if (ProjectileNumber = 0) and (Phase >= ActionData.WarmUpTime * Difficulty.MonsterAttackDelayMultiplier) then //TODO: multiplier only for monsters
  begin
    Phase -= ActionData.WarmUpTime * Difficulty.MonsterAttackDelayMultiplier;
    ProjectileNumber := 1;
  end else
  if (ProjectileNumber > 0) and (ProjectileNumber <= ActionData.Projectiles) and (Phase >= ActionData.ShotDelay * Difficulty.MonsterAttackDelayMultiplier) then
  begin
    Inc(ProjectileNumber);
    Phase -= ActionData.ShotDelay * Difficulty.MonsterAttackDelayMultiplier;
    Perform;
  end else
  if (ProjectileNumber > ActionData.Projectiles) and (Phase >= ActionData.CoolDownTime * Difficulty.MonsterAttackDelayMultiplier) then //TODO: multiplier only for monsters
  begin
    Phase -= ActionData.CoolDownTime * Difficulty.MonsterAttackDelayMultiplier;
    ProjectileNumber := 0;
  end;
end;

class function TActionShootTarget.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionShootTarget).Phase := 0;
  (Result as TActionShootTarget).ProjectileNumber := 0;
end;

{ TActionShootTargetData -------------------------------}

procedure TActionShootTargetData.Validate;
begin
  inherited Validate;
  if Projectiles <= 0 then
    raise EDataValidationError.CreateFmt('Projectiles <= 0 in %s', [ClassName]);
  if ShotDelay < 0 then
    raise EDataValidationError.CreateFmt('ShotDelay <= 0 in %s', [ClassName]);
  if RandomAngle < 0 then
    raise EDataValidationError.CreateFmt('RandomAngle < 0 in %s', [ClassName]);
end;

procedure TActionShootTargetData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  ShotDelay := Element.AttributeSingle('ShotDelay');
  Projectiles := Element.AttributeInteger('Projectiles');
  RandomAngle := Element.AttributeSingleDef('RandomAngle', 0);
end;

function TActionShootTargetData.Description: TEntriesList;
var
  ActionEntries: TEntriesList;
begin
  Result := inherited Description;
  ActionEntries := MarkData.Description;
  Result.AddRange(ActionEntries);
  FreeAndNil(ActionEntries);
end;

function TActionShootTargetData.Action: TActionClass;
begin
  Exit(TActionShootTarget);
end;

initialization
  RegisterSimpleSerializableObject(TActionShootTarget);
  RegisterSerializableData(TActionShootTargetData);

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameProjectileBug;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, Classes, DOM,
  CastleUtils,
  GameProjectileBugSimple, GameMarkAbstract, GameActor,
  GameUnlockableEntry;

type
  TProjectileBug = class(TProjectileBugSimple)
  strict private
    procedure TeleportToTarget;
  public
    procedure HitWall; override;
    procedure HitTarget(const TargetsHit: array of TActor); override;
  end;

  TProjectileBugData = class(TProjectileBugSimpleData)
  protected
    function CanBeIndependent: Boolean; override; // cannot be independent, because needs to affect the Parent
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    MinTeleportRange: Single;
    MaxTeleportRange: Single;
    TeleportChanceMiss: Single;
    TeleportChanceHit: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameMap, GameMapTypes,
  GameViewGame, GameLog, GameColors, GameRandom, GameStats,
  GameVinculopediaEntryText;

{$IFDEF SafeTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE MarkData:=(Data as TProjectileBugData)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE MarkData:=TProjectileBugData(Data)}
{$ENDIF}

{$DEFINE TargetPlayer:=ViewGame.CurrentCharacter}
//TODO: it's not trivial to get rid of it here

procedure TProjectileBug.TeleportToTarget;
const
  RetryCount = Integer(1000);
var
  Dist: TDistanceMapArray;
  ToX, ToY: Int16;
  Count: Integer;
begin
  if not ParentActor.CanAct then // TODO: unsafe?
    Exit;

  Dist := Map.DistanceMap(TargetPlayer, true);
  Count := 0;
  repeat
    Inc(Count);
    repeat // TODO: Optimize
      ToX := Rnd.Random(Map.SizeX);
      ToY := Rnd.Random(Map.SizeY);
    until (Dist[ToX + Map.SizeX * ToY] > MarkData.MinTeleportRange) and (Dist[ToX + Map.SizeX * ToY] <= MarkData.MaxTeleportRange) and Map.PassableTiles[ParentActor.PredSize][ToX + Map.SizeX * ToY]
  until TargetPlayer.LineOfSightMore(ToX + ParentActor.Size div 2, ToY + ParentActor.Size div 2) or (Count > RetryCount);
  LocalStats.IncStat('TProjectileBug_teleport');
  ShowLog('%s blinks and changes position', [ParentActor.Data.DisplayName], ColorLogTickle);
  // TODO: Teleport without resetting action to Idle (and thus making Self nil here)
  ParentActor.MoveMeTo(ToX, ToY);
end;

procedure TProjectileBug.HitWall;
begin
  inherited;
  if Rnd.Random < MarkData.TeleportChanceMiss then
    TeleportToTarget;
end;

procedure TProjectileBug.HitTarget(const TargetsHit: array of TActor);
begin
  inherited;
  if Rnd.Random < MarkData.TeleportChanceHit then
    TeleportToTarget;
end;

{ TProjectileBugData ----------------------------- }

procedure TProjectileBugData.Validate;
begin
  inherited Validate;
  if MinTeleportRange <= 0 then
    raise EDataValidationError.CreateFmt('MinTeleportRange <= 0 in %s', [Self.ClassName]);
  if MaxTeleportRange <= 0 then
    raise EDataValidationError.CreateFmt('MaxTeleportRange <= 0 in %s', [Self.ClassName]);
  if TeleportChanceMiss < 0 then
    raise EDataValidationError.CreateFmt('TeleportChanceMiss < 0 in %s', [Self.ClassName]);
  if TeleportChanceHit < 0 then
    raise EDataValidationError.CreateFmt('TeleportChanceHit < 0 in %s', [Self.ClassName]);
end;

procedure TProjectileBugData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  MinTeleportRange := Element.AttributeSingle('MinTeleportRange');
  MaxTeleportRange := Element.AttributeSingle('MaxTeleportRange');
  TeleportChanceMiss := Element.AttributeSingle('TeleportChanceMiss');
  TeleportChanceHit := Element.AttributeSingle('TeleportChanceHit');
end;

function TProjectileBugData.CanBeIndependent: Boolean;
begin
  Exit(false);
end;

function TProjectileBugData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('Will also teleport closer to target - with some chance if hit and always if misses.', []),
    'TProjectileBug_teleport', 1));
end;

function TProjectileBugData.Mark: TMarkClass;
begin
  Exit(TProjectileBug);
end;

initialization
  RegisterSerializableObject(TProjectileBug);
  RegisterSerializableData(TProjectileBugData);

end.


{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Will steal an item and give it to the nearest monster
  if character is nude will deal willpower Damage
  if character isn't moving them won't do anything }
unit GameProjectileEye;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  CastleUtils,
  GameProjectileAbstract, GameMarkAbstract, GameActor,
  GameUnlockableEntry;

type
  TProjectileEye = class(TProjectileAbstract)
  public
    procedure HitWall; override;
    procedure HitTarget(const TargetsHit: array of TActor); override;
  end;

  TProjectileEyeData = class(TProjectileAbstractData)
  protected
    function CanBeIndependent: Boolean; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    WillDamage: Single;
    StealSound: String;
    ScareSound: String;
    MissSound: String;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameInventoryItem, GameMapItem, GamePlayerCharacter,
  GameSounds,
  GameViewGame, GameLog, GameColors, GameStats, GameMonster, {GameMap,}
  GameVinculopediaEntryText;

{$IFDEF SafeTypecast}
{$DEFINE MarkData:=(Data as TProjectileEyeData)}
{$ELSE}
{$DEFINE MarkData:=TProjectileEyeData(Data)}
{$ENDIF}

procedure TProjectileEye.HitWall;
begin
  //todo: spawn explosion
end;

procedure TProjectileEye.HitTarget(const TargetsHit: array of TActor);
var
  ItemStolen: TInventoryItem;
  TargetPlayer: TPlayerCharacter;
  M: TMonster;
begin
  LocalStats.IncStat('TProjectileEye');
  TargetPlayer := TargetsHit[0] as TPlayerCharacter;
  if TargetPlayer.IsPassive then
  begin
    ViewGame.WakeUp(true, true);
    LocalStats.IncStat('TProjectileEye_passive');
    ShowLog('As %s doesn''t act aggressively the flying eye seems to not notice her', [TargetPlayer.Data.DisplayName], ColorDefault);
    Sound(MarkData.MissSound);
  end else
  begin
    if (TargetPlayer.Inventory.EquippedSlotsRemovable > 0) then
    begin
      ViewGame.WakeUp(true, true);
      ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem, true);
      M := TMonster(GetNearestMonster);
      if M <> nil then // at least one active monster on the map
      begin
        if M.IsVisible then // Map.Ray(0, TargetPlayer.CenterX, TargetPlayer.CenterY, M.CenterX, M.CenterY)
          ShowLog('%s suddenly notices her %s disappearing inside nearby %s''s husk', [TargetPlayer.Data.DisplayName, ItemStolen.Data.DisplayName, M.Data.DisplayName], ColorLogItemSteal)
        else
          ShowLog('%s suddenly notices her %s disappear', [TargetPlayer.Data.DisplayName, ItemStolen.Data.DisplayName], ColorLogItemSteal);
        M.Loot.Add(ItemStolen);
      end else
      begin
        ShowLog('%s''s %s falls to the ground', [TargetPlayer.Data.DisplayName, ItemStolen.Data.DisplayName], ColorLogItemSteal);
        TMapItem.DropItem(LastTileX, LastTileY, ItemStolen, true);
      end;
      Sound(MarkData.StealSound);
    end else
    begin
      ViewGame.WakeUp(true, true);
      ShowLog('The flying eye stares at %s and shivers run down her spine', [TargetPlayer.Data.DisplayName], ColorLogWillDamage);
      TargetPlayer.HitWill(MarkData.WillDamage);
      Sound(MarkData.ScareSound);
    end;
  end;
end;

{ TProjectileEyeData ----------------------------- }

function TProjectileEyeData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

procedure TProjectileEyeData.Validate;
begin
  inherited Validate;
  if WillDamage <= 0 then
    raise EDataValidationError.CreateFmt('WillDamage <= 0 in %s', [Self.ClassName]);
  if not SoundExists(StealSound) then
    raise EDataValidationError.CreateFmt('Invalid StealSound = "%s" in %s', [StealSound, Self.ClassName]);
  if not SoundExists(ScareSound) then
    raise EDataValidationError.CreateFmt('Invalid ScareSound = "%s" in %s', [ScareSound, Self.ClassName]);
  if not SoundExists(MissSound) then
    raise EDataValidationError.CreateFmt('Invalid MissSound = "%s" in %s', [MissSound, Self.ClassName]);
end;

procedure TProjectileEyeData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  WillDamage := Element.AttributeSingle('WillDamage');
  StealSound := Element.AttributeString('StealSound');
  ScareSound := Element.AttributeString('ScareSound');
  MissSound := Element.AttributeString('MissSound');
end;

function TProjectileEyeData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will launch a barrage of ghosting flying eyes at the target that will steal a random item and give it to the nearest monster. In case the target has no items to steal will deal %.1n willpower damage.', [WillDamage]),
    'TProjectileEye', 1));
  Result.Add(
    NewEntryText(
      Format('If the target is passive will not affect the target in any way.', []),
    'TProjectileEye_passive', 1));
end;

function TProjectileEyeData.Mark: TMarkClass;
begin
  Exit(TProjectileEye);
end;

initialization
  RegisterSerializableObject(TProjectileEye);
  RegisterSerializableData(TProjectileEyeData);

end.


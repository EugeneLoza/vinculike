{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Will suspend the character in immobilized state }
unit GameMarkZeroG;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract,
  GameUnlockableEntry;

type
  TMarkMarkZeroG = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkMarkZeroGData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    FallDamage: Single;
    SuspensionDuration: Single;
    HitSound: String;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameSounds,
  GameViewGame, GameActor, GameStats,
  GamePlayerCharacter, GameActionPlayerZeroG,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkMarkZeroGData}
{$INCLUDE marktypecasts.inc}

procedure TMarkMarkZeroG.Perform;
begin
  Sound(MarkData.HitSound);
  LocalStats.IncStat(Data.ClassName);
  ShowLog('%s suddenly feels as ground escapes from under her feet', [TargetActor.Data.DisplayName], ColorLogTrap);
  TargetPlayer.CurrentAction := TActionPlayerZeroG.NewAction(TargetPlayer, MarkData.SuspensionDuration);
  TActionPlayerZeroG(TargetPlayer.CurrentAction).FallDamage := MarkData.FallDamage;
  TargetPlayer.CurrentAction.Start;
  ViewGame.UnPauseGame;
end;

{ TMarkMarkZeroGData ----------------------------- }

procedure TMarkMarkZeroGData.Validate;
begin
  inherited Validate;
  if SuspensionDuration < 0 then
    raise EDataValidationError.CreateFmt('SuspensionDuration < 0 in %s', [Self.ClassName]);
  if FallDamage < 0 then
    raise EDataValidationError.CreateFmt('FallDamage < 0 in %s', [Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TMarkMarkZeroGData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  SuspensionDuration := Element.AttributeSingle('SuspensionDuration');
  FallDamage := Element.AttributeSingle('FallDamage');
  HitSound := Element.AttributeString('HitSound');
end;

function TMarkMarkZeroGData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('EFFECT: Forces target float above the ground for %.1n seconds unable to move or resist. After releasing deals %.1n damage. Loud noise the trap makes can attract monsters.', [SuspensionDuration, FallDamage]),
    Classname, 1));
end;

function TMarkMarkZeroGData.Mark: TMarkClass;
begin
  Exit(TMarkMarkZeroG);
end;

initialization
  RegisterSerializableObject(TMarkMarkZeroG);
  RegisterSerializableData(TMarkMarkZeroGData);

end.


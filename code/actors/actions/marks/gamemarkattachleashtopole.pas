{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkAttachLeashToPole;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract,
  GameItemData, GameMonsterData,
  GameUnlockableEntry;

type
  TMarkAttachLeashToPole = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkAttachLeashToPoleData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    LeashData, CollarData: TItemData;
    PoleData: TMonsterData;
    HitSound: String;
    LeashLength: Single;
    LeashBoost: Single;
    NoCollarStaminaDamage: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
    function CanBeIndependent: Boolean; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameInventoryItem,
  GameViewGame, GameSounds, GameActor, GameLeash, GameMonstersDatabase, GameMap,
  GameItemsDatabase, GameApparelSlots, GamePlayerCharacter, GameMonster, GameStats,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkAttachLeashToPoleData}
{$INCLUDE marktypecasts.inc}

procedure TMarkAttachLeashToPole.Perform;
var
  Leash: TLeash;
  Pole: TMonster;
begin
  LocalStats.IncStat(Data.ClassName);

  if not TargetPlayer.Inventory.Bondage[esNeck] then
  begin
    if TargetPlayer.Unsuspecting then // sleep attack: equip collar, parent is already fleeing, so will have to wait for next opportunity for the leash
    begin
      LocalStats.IncStat(Data.ClassName + '_sleep');
      ShowLog('%s feels as something appears over her neck', [TargetActor.Data.DisplayName], ColorLogTrap);
      Sound(MarkData.CollarData.SoundEquip);
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(MarkData.CollarData));
    end else
    begin
      Sound(MarkData.HitSound);
      ShowLog('%s slips across %s''s neck and she barely avoids getting caught in the loop', [MarkData.LeashData.DisplayName, TargetActor.Data.DisplayName], ColorLogTrap);
      TargetPlayer.HitStamina(MarkData.NoCollarStaminaDamage);
    end;
    Exit;
  end;

  if (TargetPlayer.Inventory.Apparel[esLeash] <> nil) and ((TargetPlayer.Inventory.Apparel[esLeash] as TLeash).LeashHolder <> nil) then
  begin
    if not TargetPlayer.Unsuspecting then
    begin
      Sound(MarkData.HitSound);
      ShowLog('Nothing seems to happen', [], ColorLogTrap);
    end;
    Exit;
  end;

  Sound(MarkData.HitSound);
  if TargetPlayer.Inventory.Apparel[esLeash] = nil then
  begin
    Leash := TLeash.NewLeash(MarkData.LeashData);
    TargetPlayer.Inventory.EquipItem(Leash);
    TargetPlayer.Inventory.ReinforceItem(esLeash);
    Leash.LeashLength := MarkData.LeashLength;
    Sound(MarkData.LeashData.SoundEquip);
    ShowLog('A sturdy %s snaps to %s''s %s', [Leash.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Inventory.Apparel[esNeck].Data.DisplayName], ColorLogBondage);
  end else
  begin
    Leash := (TargetPlayer.Inventory.Apparel[esLeash] as TLeash);
    Leash.MaxDurability := (1 - MarkData.LeashBoost) * Leash.MaxDurability + MarkData.LeashBoost * Leash.ItemData.Durability;
    Leash.Durability := (1 - MarkData.LeashBoost) * Leash.Durability + MarkData.LeashBoost * Leash.MaxDurability;
    // don't adjust leash length
    ShowLog('The %s''s around %s''s suddenly comes into motion', [Leash.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondage);
  end;

  Pole := TMonster.Create(true);
  Pole.Data := MarkData.PoleData;
  Pole.Reset;
  Pole.TeleportCenter(ParentActor.CenterX, ParentActor.CenterY); // teleports to MARK, not mark thrower
  Map.MonstersList.Add(Pole);
  Leash.LeashHolder := Pole;

  ShowLog('The loose end is now attached to a %s', [Pole.Data.DisplayName], ColorLogBondage);
  TargetPlayer.Particle('LEASH', ColorParticlePlayerBound);

  //ViewGame.WakeUp(true, true);
end;

{ TMarkAttachLeashToPoleData ----------------------------- }

procedure TMarkAttachLeashToPoleData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if LeashData = nil then
    raise EDataValidationError.CreateFmt('LeashData = nil in %s', [Self.ClassName]);
  if CollarData = nil then
    raise EDataValidationError.CreateFmt('CollarData = nil in %s', [Self.ClassName]);
  if PoleData = nil then
    raise EDataValidationError.CreateFmt('PoleData = nil in %s', [Self.ClassName]);
  if LeashLength <= 1 then
    raise EDataValidationError.CreateFmt('LeashLength <= 1 in %s', [Self.ClassName]);
  if LeashLength >= 32 then
    raise EDataValidationError.CreateFmt('LeashLength >= 32 in %s', [Self.ClassName]);
  if LeashBoost <= 0.0 then
    raise EDataValidationError.CreateFmt('LeashBoost <= 0.0 in %s', [Self.ClassName]);
  if LeashBoost > 1.0 then
    raise EDataValidationError.CreateFmt('LeashBoost > 1.0 in %s', [Self.ClassName]);
  if NoCollarStaminaDamage < 0 then
    raise EDataValidationError.CreateFmt('NoCollarStaminaDamage < 0 in %s', [Self.ClassName]);
end;

procedure TMarkAttachLeashToPoleData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  LeashData := ItemsDataDictionary[Element.AttributeString('LeashData')] as TItemData; // tood: post-processing
  CollarData := ItemsDataDictionary[Element.AttributeString('CollarData')] as TItemData; // tood: post-processing
  PoleData := MonstersDataDictionary[Element.AttributeString('PoleData')] as TMonsterData; // tood: post-processing
  LeashLength := Element.AttributeSingle('LeashLength');
  LeashBoost := Element.AttributeSingle('LeashBoost');
  NoCollarStaminaDamage := Element.AttributeSingle('NoCollarStaminaDamage');
end;

function TMarkAttachLeashToPoleData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('EFFECT: Will attempt to attach a %s to heroine''s collar. The other end will get attached to an extremely durable but otherwise harmless %s. If heroine wears no collar the attack will fail. If the heroine already wears a leash, it will get reinforced.', [LeashData.DisplayName, PoleData.DisplayName]),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('In case the target is unsuspecting (e.g. unconscious) and doesn''t have a collar on will equip a %s.', [CollarData.DisplayName]),
    Classname + '_sleep', 1));
end;

function TMarkAttachLeashToPoleData.Mark: TMarkClass;
begin
  Exit(TMarkAttachLeashToPole);
end;

function TMarkAttachLeashToPoleData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

initialization
  RegisterSerializableObject(TMarkAttachLeashToPole);
  RegisterSerializableData(TMarkAttachLeashToPoleData);

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkPinchBlindfold;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameMarkAbstract, GameItemData, GameApparelSlots,
  GameUnlockableEntry;

type
  TMarkPinchBlindfold = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkPinchBlindfoldData = class(TMarkAbstractData)
  public const
    AttackSlots = [esBottomUnder, esBottomOver, esTopOverOver, esTopUnder];
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Blindfold: TItemData;
    NudeTopDamage: Single;
    NudeBottomDamage: Single;
    StealSound: String;
    HitSound: String;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameSounds, GameMap,
  GameViewGame,
  GameItemsDatabase, GameInventoryItem,
  GamePlayerCharacter, GameMonster,
  GameStats, GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkPinchBlindfoldData}
{$INCLUDE marktypecasts.inc}

procedure TMarkPinchBlindfold.Perform;
var
  ItemStolen: TInventoryItem;
  E: TApparelSlot;
begin
  if TargetPlayer.Unsuspecting then
  begin
    if (TargetPlayer.Inventory.Equipped[esGlasses] <> nil) and not (TargetPlayer.Inventory.Bondage[esGlasses]) then
    begin
      if TargetPlayer.Blindfolded then //non-bondage blindfold
      begin
        ShowLog('%s feels her nose itching, but it''s not enough to wake her up', [TargetPlayer.Data.DisplayName], ColorLogItemSteal)
      end else
      begin
        ShowLog('%s silently removes %s from %s''s %s', [ParentMonster.Data.DisplayName, TargetPlayer.Inventory.Equipped[esGlasses].Data.DisplayName, TargetPlayer.Data.DisplayName, EquipSlotsToHumanReadableString(TargetPlayer.Inventory.Equipped[esGlasses].Data.EquipSlots)], ColorLogItemSteal);
        Sound(TargetPlayer.Inventory.Equipped[esGlasses].ItemData.SoundUnequip);
        TargetPlayer.Inventory.UnequipAndDrop(esGlasses, false);
      end;
    end;
    if TargetPlayer.Inventory.Equipped[esGlasses] = nil then
    begin
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(MarkData.Blindfold));
      TargetPlayer.Inventory.ReinforceItem(esGlasses);
      Sound(MarkData.Blindfold.SoundEquip);
      Map.ForgetVisible(1200);
      ShowLog('%s carefully puts %s on %s''s %s and leaves her to sweet dreams', [ParentMonster.Data.DisplayName, MarkData.Blindfold.DisplayName, TargetPlayer.Data.DisplayName, EquipSlotsToHumanReadableString(MarkData.Blindfold.EquipSlots)], ColorLogBondage);
    end else
    begin
      TargetPlayer.Inventory.ReinforceItem(esGlasses);
      Sound(MarkData.Blindfold.SoundEquip);
      ShowLog('%s secures %s around %s''s %s and vanishes in shadows', [ParentMonster.Data.DisplayName, TargetPlayer.Inventory.Equipped[esGlasses].Data.DisplayName, TargetPlayer.Data.DisplayName, EquipSlotsToHumanReadableString(TargetPlayer.Inventory.Equipped[esGlasses].Data.EquipSlots)], ColorLogBondage);
    end;
    LocalStats.IncStat(Data.ClassName + '_sleep');
    ParentMonster.Ai.AiFlee := true;
    Exit;
  end;
  //ViewGame.WakeUp(true, true); -- never wakes up
  LocalStats.IncStat(Data.ClassName);

  if TargetPlayer.Inventory.Equipped[esGlasses] = nil then
  begin
    ShowLog('%s catches %s''s head and wraps %s around her %s blindfolding and disorienting her', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName, MarkData.Blindfold.DisplayName, EquipSlotsToHumanReadableString(MarkData.Blindfold.EquipSlots)], ColorLogBondage);
    TargetPlayer.Particle('BOUND', ColorParticlePlayerBound);
    TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(MarkData.Blindfold));
    TargetPlayer.PlaySurpriseSound;
    Sound(MarkData.Blindfold.SoundEquip);
    ViewGame.ShakeMap;
    Map.ForgetVisible(1200);
  end else
  begin
    if TargetPlayer.Blindfolded then
    begin
      if TargetPlayer.Inventory.Nude then
        TryCatchLeash;
      if Rnd.RandomBoolean then
      begin
        // attack top
        if TargetPlayer.Inventory.TopCoveredRemovable > 0 then
        begin
          repeat
            E := TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem;
          until (TargetPlayer.Inventory.Equipped[E].ItemData.CoversTop) and not (TargetPlayer.Inventory.Bondage[E]);
          ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(E, true);
          ParentMonster.Loot.Add(ItemStolen);
          Sound(MarkData.StealSound);
          ShowLog('%s snatches %s from %s''s %s as she stumbles blindly', [ParentMonster.Data.DisplayName, ItemStolen.Data.DisplayName, TargetPlayer.Data.DisplayName, EquipSlotsToHumanReadableString(ItemStolen.Data.EquipSlots)], ColorLogItemSteal);
          TargetPlayer.Particle('STRIP', ColorParticlePlayerDisrobed);
        end else
        begin
          if TargetPlayer.Inventory.TopCovered = 0 then
          begin
            ShowLog('%s pinches %s''s nipple making her squeak in surprise', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogWillDamage);
            TargetPlayer.HitWill(MarkData.NudeTopDamage);
            Sound(MarkData.HitSound);
            TargetPlayer.PlaySurpriseSound;
          end else
            ShowLog('%s tries to pinch %s''s nipple, but can''t reach it', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogWillDamage);
        end;
      end else
      begin
        // attack bottom
        if TargetPlayer.Inventory.BottomCoveredRemovable > 0 then
        begin
          repeat
            E := TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem;
          until (TargetPlayer.Inventory.Equipped[E].ItemData.CoversBottom) and not (TargetPlayer.Inventory.Bondage[E]);
          ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(E, true);
          ParentMonster.Loot.Add(ItemStolen);
          Sound(MarkData.StealSound);
          ShowLog('%s seizes %s from %s''s %s as she stumbles blindly', [ParentMonster.Data.DisplayName, ItemStolen.Data.DisplayName, TargetPlayer.Data.DisplayName, EquipSlotsToHumanReadableString(ItemStolen.Data.EquipSlots)], ColorLogItemSteal);
          TargetPlayer.Particle('STRIP', ColorParticlePlayerDisrobed);
        end else
        begin
          if TargetPlayer.Inventory.BottomCovered = 0 then
          begin
            ShowLog('%s pinches %s''s nethers forcing her to shriek', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogWillDamage);
            TargetPlayer.HitWill(MarkData.NudeBottomDamage);
            Sound(MarkData.HitSound);
            TargetPlayer.PlaySurpriseSound;
          end else
            ShowLog('%s tries to pinch %s''s bottom, but can''t reach it', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogWillDamage);
        end;
      end;
    end else
    begin
      ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(esGlasses, true);
      ParentMonster.Loot.Add(ItemStolen);
      Sound(MarkData.StealSound);
      ShowLog('%s easily steals %s from %s''s %s', [ParentMonster.Data.DisplayName, ItemStolen.Data.DisplayName, TargetPlayer.Data.DisplayName, EquipSlotsToHumanReadableString(ItemStolen.Data.EquipSlots)], ColorLogItemSteal);
      TargetPlayer.Particle('STRIP', ColorParticlePlayerDisrobed);
    end;
  end;
end;

{ TMarkPinchBlindfoldData ----------------------------- }

procedure TMarkPinchBlindfoldData.Validate;
begin
  inherited Validate;
  if not (esGlasses in Blindfold.EquipSlots) then
    raise EDataValidationError.CreateFmt('Blindfold item "%s" in %s does not have esGlasses slot', [Blindfold.Id, Self.ClassName]);
  if Blindfold.NumberOfSlots > 1 then
    raise EDataValidationError.CreateFmt('Blindfold item "%s" in %s has too many equip slots, expected: 1', [Blindfold.Id, Self.ClassName]);
  if NudeTopDamage <= 0 then
    raise EDataValidationError.CreateFmt('NudeTopDamage <= 0 in %s', [Self.ClassName]);
  if NudeBottomDamage <= 0 then
    raise EDataValidationError.CreateFmt('NudeBottomDamage <= 0 in %s', [Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if not SoundExists(StealSound) then
    raise EDataValidationError.CreateFmt('Invalid StealSound = "%s" in %s', [StealSound, Self.ClassName]);
end;

procedure TMarkPinchBlindfoldData.Read(const Element: TDOMElement);
var
  BlindfoldString: String;
begin
  inherited Read(Element);
  BlindfoldString := Element.AttributeString('Blindfold');
  if ItemsDataDictionary.ContainsKey(BlindfoldString) then
    Blindfold := ItemsDataDictionary[BlindfoldString] as TItemData
  else
    raise Exception.CreateFmt('Blindfold item "%s" not found in %s', [BlindfoldString, ClassName]);
  NudeTopDamage := Element.AttributeSingle('NudeTopDamage');
  NudeBottomDamage := Element.AttributeSingle('NudeBottomDamage');
  StealSound := Element.AttributeString('StealSound');
  HitSound := Element.AttributeString('HitSound');
end;

function TMarkPinchBlindfoldData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('Will first of all attack eyes/glasses slot. If there are glasses (or any other normal accessory item) will steal it. If there is none: will equip a blindfold there.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('If the character is blindfolded, will choose to attack top or bottom. If the target is covered by clothes then will steal those. If none - will pinch nipples for %.1n willpower damage or nethers for %.1n willpower damage.', [NudeTopDamage, NudeBottomDamage]),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('If breasts or bottoms are covered by a restraint item, the pinching attack will fail.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('If finds the character asleep, will steal glasses if any, equip a blindfold and leave the heroine to her dreams.', []),
    Classname + '_sleep', 1));
end;

function TMarkPinchBlindfoldData.Mark: TMarkClass;
begin
  Exit(TMarkPinchBlindfold);
end;

initialization
  RegisterSerializableObject(TMarkPinchBlindfold);
  RegisterSerializableData(TMarkPinchBlindfoldData);

end.


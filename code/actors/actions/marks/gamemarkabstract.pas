{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkAbstract;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, Classes, Generics.Collections, DOM,
  CastleUtils, CastleGlImages,
  GameSerializableData,
  GamePositionedObject, GameApparelSlots, GameInventoryItem,
  GameUnlockableEntry;

type
  TMarkQuitEarly = class(Exception);

type
  TMarkAbstractData = class; // forward
  TMarkAbstract = class abstract(TPositionedObject)
  protected // MACROS
    function GetNearestMonster: TObject;
  public
    Parent: TObject; // TODO: Parenless marks
    Phase: Single;
    OnFinished: TSimpleNotifyEvent; // TODO: Parentless marks
    Data: TMarkAbstractData;
    function Progress: Single;
    procedure Update(SecondsPassed: Single); virtual;
    procedure EndAction;
  public const Signature = 'mark';
  end;
  TMarkClass = class of TMarkAbstract;
  TMarksList = specialize TObjectList<TMarkAbstract>;

  // todo: move into a new unit
  TMarkTargetAbstract = class abstract(TMarkAbstract)
  protected
    TargetIdleCounter: Single;
    function TargetWasResisting: Boolean;
    procedure Perform; virtual; abstract;
    procedure Miss; virtual;
  protected // MACROS
    procedure TryCatchLeash;
    procedure TryReleaseLeash;
    procedure DragTarget;
    { Variation of TPlayerCharacter.UnequipAndDrop but drops at monster's position }
    function UnequipAndDrop(const E: TApparelSlot; const Forced: Boolean): TInventoryItem;
  public
    Target: TObject;
    procedure Update(SecondsPassed: Single); override;
  end;

  TMarkAbstractData = class abstract(TSerializableData)
  protected
    { If this mark can exist without parent
      If not, then parent must take care of mark
      Otherwise it can be "left alone"
      Currently only for validating Independent parameter from data }
    function CanBeIndependent: Boolean; virtual; // TODO: abstract;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Duration: Single;
    Size: Byte;
    Range: Single;
    SqrRange: Single;
    Image: TDrawableImage;
    SqueezeHorizontal: Boolean;
    { Independent marks persist after parent changed action or died
      WARNING: Not all actions can be independent }
    Independent: Boolean;
    function Description: TEntriesList; virtual;
    function Mark: TMarkClass; virtual; abstract;
  end;

implementation
uses
  CastleXmlUtils, CastleUriUtils,
  GameCachedImages, GameMap, GameLog, GameActionIdle, GameActor, GameDifficultyLevel,
  GameActionAbstract, GameMonster, GameLeash, GameMapItem,
  GamePlayerCharacter, GameColors, GameSounds;

type
  EParentActorDead = class(Exception);

{$DEFINE DataClass:=TMarkAbstractData}
{$INCLUDE marktypecasts.inc}

procedure TMarkAbstract.EndAction;
begin
  if Assigned(OnFinished) then
    OnFinished;
  OnFinished := nil;
  Map.MarksList.Remove(Self); // this will also free Self!
end;

function TMarkAbstract.GetNearestMonster: TObject;
var
  D, D1: Single;
  M: TMonster;
begin
  Result := nil;
  D := Single.MaxValue;
  for M in Map.MonstersList do
    if M.CanAct and M.Aggressive then
    begin
      D1 := M.DistanceToSqr(Self);
      if D1 < D then
      begin
        Result := M;
        D := D1;
      end;
    end;
end;

function TMarkAbstract.Progress: Single;
begin
  Exit(Phase / (Data.Duration * Difficulty.MonsterAttackDelayMultiplier)); // TODO: multiplier only for monsters
end;

procedure TMarkAbstract.Update(SecondsPassed: Single);
begin
  Phase += SecondsPassed;

  if (not Data.Independent) and (not ParentActor.CanAct) then
  begin
    EndAction;
    // This is not an error state, but a way to self-destruct this mark, even if parent didn't explicitly ask it to
    // No error message will be shown, it's for internal use only
    // This is a workaround, that will most likely not get fixed
    raise TMarkQuitEarly.Create('Non-independent mark received an update event, but its parent is no longer active'); // we can't call ClassName here because the instance is broken
  end;
end;

{ TMarkTargetAbstract ---------------------------------------------}

function TMarkTargetAbstract.TargetWasResisting: Boolean;
begin
  Exit(TargetIdleCounter < Data.Duration * 0.9);
end;

procedure TMarkTargetAbstract.Miss;
begin
  // do nothing by default; maybe play miss sound?
end;

procedure TMarkTargetAbstract.TryCatchLeash;
begin
  if (TargetPlayer.Inventory.Apparel[esLeash] <> nil) and ((TargetPlayer.Inventory.Equipped[esLeash] as TLeash).LeashHolder = nil) then
  begin
    //if TargetPlayer.ResistMultiplier;
    ShowLog('%s catches the loose end of %s and holds it tight', [ParentActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[esLeash].Data.DisplayName], ColorLogBondage);
    (TargetPlayer.Inventory.Equipped[esLeash] as TLeash).LeashHolder := ParentActor;
    TargetPlayer.Particle('LEASH', ColorParticlePlayerBound);
  end;
end;

procedure TMarkTargetAbstract.TryReleaseLeash;
begin
  if (TargetPlayer.Inventory.Apparel[esLeash] <> nil) and ((TargetPlayer.Inventory.Equipped[esLeash] as TLeash).LeashHolder = Parent) then
  begin
    //if TargetPlayer.ResistMultiplier;
    ShowLog('%s lets go off %s''s leash', [ParentActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[esLeash].Data.DisplayName], ColorLogBondageItemSave);
    (TargetPlayer.Inventory.Equipped[esLeash] as TLeash).ReleaseLeash;
  end;
end;

procedure TMarkTargetAbstract.DragTarget;
var
  ToX, ToY: Single;

  procedure FindNearestPassableTile;
  var
    Range: Integer;
    DX, DY: Integer;
    ToXInt, ToYInt: Int16;
  begin
    Range := 1;
    ToXInt := Trunc(ToX);
    ToYInt := Trunc(ToY);
    if not Map.PassableTiles[TargetActor.PredSize][ToXInt + Map.SizeX * ToYInt] then
    begin
      repeat
        for DX := -Range to Range do
          for DY := -Range to Range do
            if (ToXInt + DX > 0) and (ToXInt + DX < Map.PredSizeX) and
               (ToYInt + DY > 0) and (ToYInt + DY < Map.PredSizeY) and
               Map.PassableTiles[TargetActor.PredSize][ToXInt + DX + Map.SizeX * (ToYInt + DY)] then
            begin
              ToX := ToXInt + DX;
              ToY := ToYInt + DY;
              Exit;
            end;
        Inc(Range);
      until true; // Exit when found
    end;
  end;

begin
  // lower-bottom
  ToX := ParentActor.X + ParentActor.HalfSize - TargetActor.HalfSize;
  ToY := ParentActor.Y + ParentActor.HalfSize - TargetActor.HalfSize;
  FindNearestPassableTile;
  //TargetActor.ActionFinished;
  TargetPlayer.PlayGruntSound;
  TargetActor.Teleport(ToX, ToY);
end;

function TMarkTargetAbstract.UnequipAndDrop(const E: TApparelSlot;
  const Forced: Boolean): TInventoryItem;
begin
  Result := TargetPlayer.Inventory.UnequipAndReturn(E, Forced);
  if Result <> nil then
  begin
    if Forced then
      Sound(Result.ItemData.SoundUnequip);
    TMapItem.DropItem(ParentActor.LastTileX + ParentActor.Size div 2, ParentActor.LastTileY + ParentActor.Size div 2, Result, false);
  end;
end;

procedure TMarkTargetAbstract.Update(SecondsPassed: Single);
begin
  inherited;

  if TargetActor.IsPassive then
    TargetIdleCounter += SecondsPassed;

  if Phase > Data.Duration * Difficulty.MonsterAttackDelayMultiplier then // TODO: multiplier only for monsters
  begin
    if Sqr(TargetActor.CenterX - CenterX) + Sqr(TargetActor.CenterY - CenterY) <= Sqr(HalfSize + TargetActor.HalfSize) then
      Perform
    else
      Miss;
    EndAction;
    Exit;
  end;
end;

{ TMarkAbstractData ---------------------------------}

procedure TMarkAbstractData.Validate;
begin
  // inherited -- parent is abstract
  if Duration <= 0 then
    raise EDataValidationError.CreateFmt('Duration <= 0 in %s', [Self.ClassName]);
  if Duration <= 0 then
    raise EDataValidationError.CreateFmt('Size <= 0 in %s', [Self.ClassName]);
  if Range <= 0 then
    raise EDataValidationError.CreateFmt('Range <= 0 in %s', [Self.ClassName]);
  if SqrRange <= 0 then
    raise EDataValidationError.CreateFmt('SqrRange <= 0 in %s', [Self.ClassName]);
  if Image = nil then
    raise EDataValidationError.CreateFmt('Image = nil in %s', [Self.ClassName]);
  if not Mark.InheritsFrom(TMarkAbstract) then
    raise EDataValidationError.CreateFmt('not Mark.InheritsFrom(TMarkAbstract) in %s', [Self.ClassName]);
  if Independent and not CanBeIndependent then
    raise EDataValidationError.CreateFmt('Independent and not CanBeIndependent in %s', [Self.ClassName]);
end;

procedure TMarkAbstractData.Read(const Element: TDOMElement);
begin
  Duration := Element.AttributeSingle('Duration') - TimeoutsCorrection;
  Size := Element.AttributeInteger('Size');
  Range := Element.AttributeSingle('Range');
  SqrRange := Sqr(Range);
  Image := LoadDrawable(CombineURI(FBaseUrl, Element.AttributeString('Image')));
  SqueezeHorizontal := Element.AttributeBooleanDef('SqueezeHorizontal', true);
  Independent := Element.AttributeBooleanDef('Independent', false); // TODO
end;

function TMarkAbstractData.Description: TEntriesList;
begin
  Exit(TEntriesList.Create(false)); // Ownership will be of Vinculopedia page
end;

function TMarkAbstractData.CanBeIndependent: Boolean;
begin
  Exit(false);
end;

end.


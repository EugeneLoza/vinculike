{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkUndressAndTeleportTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  GameApparelSlots, GameMarkAbstract,
  GameUnlockableEntry;

type
  TMarkUndressAndTeleportTarget = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  public
    procedure Update(SecondsPassed: Single); override;
  end;

  TMarkUndressAndTeleportTargetData = class(TMarkAbstractData)
  public const
    AttackSlots = [esBottomOver, esBottomUnder, esTopUnder, esTopOver, esTopOverOver];
    MinTeleportDistanceFraction = Single(0.5);
    DisorientationAmount = 2 * 25 * 25;
  public
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameTranslation,
  GameViewGame, GameMap,
  GamePlayerCharacter, GameActor, GameStats,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkUndressAndTeleportTargetData}
{$INCLUDE marktypecasts.inc}

procedure TMarkUndressAndTeleportTarget.Perform;
var
  E: TApparelSlot;
  Count: Integer;
begin
  Count := 0;
  for E in MarkData.AttackSlots do
    if (TargetPlayer.Inventory.Apparel[E] <> nil) and not TargetPlayer.Inventory.Bondage[E] then
    begin
      Inc(Count);
      TargetPlayer.Inventory.UnequipAndDrop(E, true);
    end;

  LocalStats.IncStat('TMarkUndressAndTeleportTarget');
  TargetPlayer.TeleportToUnknown(MarkData.MinTeleportDistanceFraction);
  ViewGame.ScheduleMonstersToIdle;  //Map.MonstersToIdle; // we can't do that here, it'll free the parent action and everything goes BOOM
  Map.ForgetVisible(MarkData.DisorientationAmount);

  ViewGame.ShakeCharacter;
  if Count > 0 then
    ShowLog(GetTranslation('TrapTeleport'), [TargetPlayer.Data.DisplayName], ColorLogTeleport)
  else
    ShowLog(GetTranslation('TrapTeleportWasNaked'), [TargetPlayer.Data.DisplayName], ColorLogTeleport);
  TargetPlayer.Particle('TELEPORT', ColorParticlePlayerTeleport);
end;

procedure TMarkUndressAndTeleportTarget.Update(SecondsPassed: Single);
begin
  inherited;
  if Phase < -1 then
    Exit; // we already show error in Inherited

  if Phase > Data.Duration then
  begin
    if Sqr(TargetActor.CenterX - CenterX) + Sqr(TargetActor.CenterY - CenterY) <= Sqr(HalfSize + TargetActor.HalfSize) then
      Perform;
    EndAction;
    Exit;
  end;
end;

{ TMarkUndressAndTeleportTargetData ----------------------------- }

function TMarkUndressAndTeleportTargetData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('EFFECT: Will remove all clothes except restraints and teleport the character to a random place on the map.', []),
    'TMarkUndressAndTeleportTarget', 1)); // not ClassName because should be "shared" with the child
end;

function TMarkUndressAndTeleportTargetData.Mark: TMarkClass;
begin
  Exit(TMarkUndressAndTeleportTarget);
end;

initialization
  RegisterSerializableObject(TMarkUndressAndTeleportTarget);
  RegisterSerializableData(TMarkUndressAndTeleportTargetData);

end.


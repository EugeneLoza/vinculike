{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkDice;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract,
  GameUnlockableEntry;

type
  TMarkDice = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
    //procedure Miss; override;
  end;

  TMarkDiceData = class(TMarkAbstractData)
  protected
    function CanBeIndependent: Boolean; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  public
    HitSound, MissSound, CriticalMissSound, CriticalHitSound: String;
    //NextMark: TMarkAbstractData;
    DiceRolls: Integer;
    DiceSides: Integer;
    TargetEvasion: Integer;
    StunDuration: Single;
  {public
    destructor Destroy; override;}
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameSounds,
  GamePlayerCharacter, GameMonster, GameActor, GameViewGame,
  GameActionPlayerStunned, GameTranslation,
  GameStats, GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkDiceData}
{$INCLUDE marktypecasts.inc}

procedure TMarkDice.Perform;
var
  I: Integer;
  TotalDamage: Integer;
  AccuracyRoll: Integer;
begin
  ViewGame.WakeUp(true, true);
  LocalStats.IncStat(Data.ClassName);
  TotalDamage := 0;
  for I := 1 to MarkData.DiceRolls do
    Inc(TotalDamage, 1 + Rnd.Random(MarkData.DiceSides));
  AccuracyRoll := 1 + Rnd.Random(20);
  ShowLog('%s rolls %dd%d for %d damage and 1d20 for %d accuracy against %d evasion', [ParentActor.Data.DisplayName, MarkData.DiceRolls, MarkData.DiceSides, TotalDamage, AccuracyRoll, MarkData.TargetEvasion], ColorLogAbsorbedDamage);
  if AccuracyRoll = 1 then
  begin
    ShowLog('Critical failure! Clumsy %s hits itself for %d', [ParentActor.Data.DisplayName, TotalDamage], ColorLogAttack);
    Sound(MarkData.CriticalMissSound);
    if ParentActor.Health > TotalDamage then
      ParentActor.Hit(TotalDamage)
    else
    begin
      ShowLog(GetTranslation('MonsterDiesLog'), [ParentMonster.Data.DisplayName], ColorLogMonsterDies);
      TargetPlayer.Experience.AddExperience(ParentMonster.MonsterData.XP);
      ParentActor.Health := -1; // Ugly way to die
    end;
  end else
  if (AccuracyRoll >= 2) and (AccuracyRoll <= MarkData.TargetEvasion) then
  begin
    ShowLog('Miss! %s evades the attack', [TargetActor.Data.DisplayName], ColorLogAttack);
    Sound(MarkData.MissSound);
  end else
  if (AccuracyRoll > MarkData.TargetEvasion) and (AccuracyRoll <= 19) then
  begin
    TargetPlayer.Hit(TotalDamage);
    Sound(MarkData.HitSound);
  end else
  if AccuracyRoll = 20 then
  begin
    TargetActor.Hit(TotalDamage);
    ShowLog('Critical success! %s is stunned (%.1ns)', [TargetActor.Data.DisplayName, MarkData.StunDuration], ColorLogAttack);
    // Cannot be resisted
    TargetActor.CurrentAction := TActionPlayerStunned.NewAction(TargetActor, MarkData.StunDuration);
    TargetActor.CurrentAction.Start;
    TargetPlayer.Particle('STUN', ColorParticlePlayerStunned);
    TargetPlayer.PlayHurtSound;
    Sound(MarkData.HitSound);
  end else
  begin
    ShowError('Unexpected AccuracyRoll value: %d', [AccuracyRoll]);
    TargetActor.Hit(TotalDamage);
  end;
end;

{procedure TMarkDice.Miss;
begin
  inherited Miss;
  if MarkData.NextMark <> nil then
  begin
    // TODO: we can't do anything here because this is not an Independent mark!
  end;
end; }

{ TMarkDiceData ----------------------------- }

function TMarkDiceData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

procedure TMarkDiceData.Validate;
begin
  inherited Validate;
  if DiceRolls <= 0 then
    raise EDataValidationError.CreateFmt('DiceRolls <= 0 in %s', [Self.ClassName]);
  if DiceSides <= 1 then
    raise EDataValidationError.CreateFmt('DiceSides <= 1 in %s', [Self.ClassName]);
  if (TargetEvasion <= 1) or (TargetEvasion >= 20) then
    raise EDataValidationError.CreateFmt('(TargetEvasion <= 1) or (TargetEvasion >= 20) in %s', [Self.ClassName]);
  if StunDuration <= 0 then
    raise EDataValidationError.CreateFmt('StunDuration <= 0 in %s', [Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if not SoundExists(MissSound) then
    raise EDataValidationError.CreateFmt('Invalid MissSound = "%s" in %s', [MissSound, Self.ClassName]);
  if not SoundExists(CriticalHitSound) then
    raise EDataValidationError.CreateFmt('Invalid CriticalHitSound = "%s" in %s', [CriticalHitSound, Self.ClassName]);
  if not SoundExists(CriticalMissSound) then
    raise EDataValidationError.CreateFmt('Invalid CriticalMissSound = "%s" in %s', [CriticalMissSound, Self.ClassName]);
end;

procedure TMarkDiceData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  //NextMark := TMarkAbstractData.ReadClass(Element.Child('NextMark', false)) as TMarkAbstractData; // will return nil if no Mark is present
  DiceRolls := Element.AttributeInteger('DiceRolls');
  DiceSides := Element.AttributeInteger('DiceSides');
  TargetEvasion := Element.AttributeInteger('TargetEvasion');
  StunDuration := Element.AttributeSingle('StunDuration');
  HitSound := Element.AttributeString('HitSound');
  MissSound := Element.AttributeString('MissSound');
  CriticalHitSound := Element.AttributeString('CriticaHitSound');
  CriticalMissSound := Element.AttributeString('CriticalMissSound');
end;

function TMarkDiceData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('Rolls 6d6 dice for damage and 1d20 for accuracy. If accuracy exceeds heroine''s evasion (4), will land a hit. Rolling 1 for accuracy is a critical failure (will hurt self instead), rolling 20 for accuracy is a critical success (damage will hit and stun the target).', []),
    Classname, 1))
end;

function TMarkDiceData.Mark: TMarkClass;
begin
  Exit(TMarkDice);
end;

{destructor TMarkDiceData.Destroy;
begin
  FreeAndNil(NextMark);
  inherited Destroy;
end;}

initialization
  RegisterSerializableObject(TMarkDice);
  RegisterSerializableData(TMarkDiceData);

end.


{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Applies one or more status effects on hit }
unit GameProjectileStatusEffect;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  CastleUtils,
  GameProjectileAbstract, GameMarkAbstract, GameActor, GameStatusEffect,
  GameUnlockableEntry;

type
  TProjectileStatusEffect = class(TProjectileAbstract)
  public
    procedure HitWall; override;
    procedure HitTarget(const TargetsHit: array of TActor); override;
  end;
  TProjectileStatusEffectData = class(TProjectileAbstractData)
  protected
    function CanBeIndependent: Boolean; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    StatusEffects: TStatusEffectsList;
    HitSound: String;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameSounds, GameStats, GamePlayerCharacter, GameLog, GameColors, GameViewGame,
  GameVinculopediaEntryText;

{$IFDEF SafeTypecast}
{$DEFINE MarkData:=(Data as TProjectileStatusEffectData)}
{$ELSE}
{$DEFINE MarkData:=TProjectileStatusEffectData(Data)}
{$ENDIF}

procedure TProjectileStatusEffect.HitWall;
begin
  //todo: spawn explosion
end;

procedure TProjectileStatusEffect.HitTarget(const TargetsHit: array of TActor);
var
  I: Integer;
begin
  LocalStats.IncStat(Data.ClassName);
  for I := 0 to Pred(MarkData.StatusEffects.Count) do
    (TargetsHit[0] as TPlayerCharacter).Inventory.AddStatusEffectNoStack(MarkData.StatusEffects[I].Clone);
  ShowLog('%s suddenly feels weird (%d status effects applied)', [TargetsHit[0].Data.DisplayName, MarkData.StatusEffects.Count], ColorLogStaminaDamage);
  Sound(MarkData.HitSound);
  ViewGame.WakeUp(true, true);
end;

{ TProjectileStatusEffectData ----------------------------- }

function TProjectileStatusEffectData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

procedure TProjectileStatusEffectData.Validate;
var
  I: Integer;
begin
  inherited Validate;
  if StatusEffects.Count = 0 then
    raise EDataValidationError.CreateFmt('StatusEffects.Count = 0 in %s', [Self.ClassName]);
  for I := 0 to Pred(StatusEffects.Count) do
    if not StatusEffects[I].Effect.CanBeStatusEffect then
      raise EDataValidationError.CreateFmt('StatusEffects[%d] as %s cannot be status effect in %s', [I, StatusEffects[I].Effect.ClassName, Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TProjectileStatusEffectData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  StatusEffects := LoadStatusEffectsList(Element);
end;

function TProjectileStatusEffectData.Description: TEntriesList;
var
  I: Integer;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will apply %d status effects to the target:', [StatusEffects.Count]),
    Classname, 1));
  for I := 0 to Pred(StatusEffects.Count) do
    Result.Add(
      NewEntryText(
        StatusEffects[I].Description,
      Classname, 1));
end;

function TProjectileStatusEffectData.Mark: TMarkClass;
begin
  Exit(TProjectileStatusEffect);
end;

destructor TProjectileStatusEffectData.Destroy;
begin
  FreeAndNil(StatusEffects);
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TProjectileStatusEffect);
  RegisterSerializableData(TProjectileStatusEffectData);

end.


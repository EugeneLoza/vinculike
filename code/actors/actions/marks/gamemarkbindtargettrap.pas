{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkBindTargetTrap;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameMarkBindTarget, GameMarkAbstract;

type
  TMarkBindTargetTrap = class(TMarkBindTarget)
  protected
    procedure InteractAllTiedUp; override;
  end;

  TMarkBindTargetTrapData = class(TMarkBindTargetData)
  public const
    TrapStaminaDamage = 200;
  public
    //description : no need, it's idential to parent, except it doesn't capture when restrain target, which will never happen in this case
    function Mark: TMarkClass; override;
  end;

implementation
uses
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameActor,
  GameViewGame, GamePlayerCharacter, GameActionIdle;

{$DEFINE DataClass:=TMarkBindTargetTrapData}
{$INCLUDE marktypecasts.inc}

procedure TMarkBindTargetTrap.InteractAllTiedUp;
begin
  // inherited - completely different behavior
  ShowLog('The straps pin %s to the ground', [TargetActor.Data.DisplayName], ColorLogBondageItemSave);
  ShowLog('With a lot of effort she still manages to stand up quickly', [TargetActor.Data.DisplayName], ColorLogBondageItemSave);
  ViewGame.ShakeCharacter;
  TargetPlayer.HitStamina(MarkData.TrapStaminaDamage);
  TargetPlayer.CurrentAction := TActionIdle.Create;
  TargetPlayer.CurrentAction.Start;
end;

{ TMarkBindTargetTrapData ----------------------------- }

function TMarkBindTargetTrapData.Mark: TMarkClass;
begin
  //inherited
  Exit(TMarkBindTargetTrap);
end;

initialization
  RegisterSerializableObject(TMarkBindTargetTrap);
  RegisterSerializableData(TMarkBindTargetTrapData);

end.


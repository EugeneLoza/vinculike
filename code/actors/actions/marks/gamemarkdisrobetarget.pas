{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkDisrobeTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameApparelSlots, GameMarkAbstract,
  GameUnlockableEntry;

type
  TMarkDisrobeTarget = class(TMarkTargetAbstract)
  private const
    DisrobingSlots = [esBottomOver, esBottomUnder, esTopUnder, esTopOver, esTopOverOver];
  protected
    procedure Perform; override;
  end;

  TMarkDisrobeTargetData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public const
    MinTeleportDistanceFraction = Single(0.5);
    //HitNakedWillDamage = 50;
    //HitNakedWillDamagePercent = 0.67;
    HitNakedStaminaDamage = 50;
  public
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameTranslation, GameInventoryItem, GameRandom, GameMap, GameMapTypes,
  GameViewGame, GameMapItem,
  GamePlayerCharacter, GameStats,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkDisrobeTargetData}
{$INCLUDE marktypecasts.inc}

procedure TMarkDisrobeTarget.Perform;
const
  MaxRetryCount = Integer(10000);
var
  E: TApparelSlot;
  ItemStolen: TInventoryItem;
  AX, AY: Int16;
  Count: Integer;
  RetryCount: Integer;

  DistanceSeed: TIntCoordList;
  DistanceMap: TDistanceMapArray;
  MaxDistance: TDistanceQuant;
begin
  Count := 0;
  DistanceSeed := TIntCoordList.Create;
  // TODO: cycle all active players
  TargetPlayer.AddSeed(DistanceSeed);
  DistanceMap := Map.DistanceMap(TargetPlayer.PredSize, true, DistanceSeed);
  MaxDistance := Map.MaxDistance(DistanceMap);
  FreeAndNil(DistanceSeed);

  RetryCount := 0;
  repeat
    repeat
      AX := Rnd.Random(Map.SizeX);
      AY := Rnd.Random(Map.SizeY);
    until Map.CanMove(AX + Map.SizeX * AY) and (DistanceMap[AX + Map.SizeX * AY] > MaxDistance * MarkData.MinTeleportDistanceFraction);
    Inc(RetryCount);
  until (Map.Visible[AX + Map.SizeX * AY] = 0) or (RetryCount > MaxRetryCount);

  DistanceMap := nil;
  for E in DisrobingSlots do
    if (TargetPlayer.Inventory.Equipped[E] <> nil) and not TargetPlayer.Inventory.Bondage[E] then
    begin
      Inc(Count);
      ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(E, true);
      if ItemStolen <> nil then
        TMapItem.DropItem(AX, AY, ItemStolen, false);
    end;
  ViewGame.ShakeCharacter;
  if Count > 0 then
  begin
    LocalStats.IncStat(Data.ClassName);
    ShowLog(GetTranslation('TrapDisrobeTarget'), [TargetPlayer.Data.DisplayName], ColorLogTeleport);
    TargetPlayer.Particle('DISROBED', ColorParticlePlayerDisrobed);
  end else
  begin
    LocalStats.IncStat(Data.ClassName + '_naked');
    {ShowLog(GetTranslation('TrapDisrobeTargetWasNaked'), [TargetPlayer.Data.DisplayName], ColorLogTeleport);
    TargetPlayer.DegradeWill(Min(TargetPlayer.Will / 2, MarkData.HitNakedWillDamage));
    TargetPlayer.Will := TargetPlayer.Will * (1 - MarkData.HitNakedWillDamagePercent);
    TargetPlayer.Particle('DIZZY', ColorParticlePlayerDisrobed);
    ViewGame.ShakeMap;}
    ShowLog('Pink fumes burst around %s, she gasps some in surprise and coughs', [TargetPlayer.Data.DisplayName], ColorLogStaminaDamage);
    TargetPlayer.HitStamina(MarkData.HitNakedStaminaDamage);
  end;
end;

{ TMarkDisrobeTargetData ----------------------------- }

procedure TMarkDisrobeTargetData.Validate;
begin
  inherited Validate;
end;

procedure TMarkDisrobeTargetData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
end;

function TMarkDisrobeTargetData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('EFFECT: Unequips all items except restraints and teleports them to a random spot on map.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('If hits a nude target will deal a minor stamina damage.', []),
    Classname + '_naked', 1));
end;

function TMarkDisrobeTargetData.Mark: TMarkClass;
begin
  Exit(TMarkDisrobeTarget);
end;

initialization
  RegisterSerializableObject(TMarkDisrobeTarget);
  RegisterSerializableData(TMarkDisrobeTargetData);

end.


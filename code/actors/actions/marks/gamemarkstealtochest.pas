{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkStealToChest;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameMarkAbstract,
  GameUnlockableEntry;

type
  TMarkStealToChest = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkStealToChestData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
    function CanBeIndependent: Boolean; override;
  public
    HitSound: String;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameSounds, GameMap,
  GameViewGame, GameApparelSlots,
  GameItemData, GameItemsDatabase, GameInventoryItem, GameMapItem,
  GamePlayerCharacter, GameActor, GameMonster, GameMonstersDatabase,
  GameStats, GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkStealToChestData}
{$INCLUDE marktypecasts.inc}

procedure TMarkStealToChest.Perform;
var
  ItemStolen: TInventoryItem;
  ItemGift: TItemData;
  Durability, MaxDurability: Single;
  Wristbands, Anklets: TApparelSlot;

  function NewChest: TMonster;
  const
    MaxRetryCount = 1000;
  var
    Extremum: SizeInt;
    RetryCount: Integer;

    function NothingCollides(const TX, TY: Int16): Boolean;
    var
      A: TActor;
    begin
      for A in Map.MonstersList do
        if A.CanAct and A.CollidesInt(TX, TY, 1) then
          Exit(false);
      for A in Map.PlayerCharactersList do
        if A.CanAct and A.CollidesInt(TX, TY, 1) then
          Exit(false);
      Exit(true);
    end;

  begin
    Result := TMonster.Create;
    Result.Data := ChestsData[Rnd.Random(ChestsData.Count)];
    RetryCount := 0;
    repeat
      Extremum := Rnd.Random(Map.Extrema.Count);
      Inc(RetryCount);
    until NothingCollides(Map.Extrema[Extremum].X, Map.Extrema[Extremum].Y) or (RetryCount > MaxRetryCount);
    Result.Reset;
    Result.Teleport(Map.Extrema[Extremum].X, Map.Extrema[Extremum].Y);
    Map.MonstersList.Insert(0, Result);
  end;

  procedure AddItemToExistingChestOrCreateThreeNew;
  var
    M: TMonster;
    List: TMonstersList;
  begin
    List := TMonstersList.Create(false);
    for M in Map.MonstersList do
      if M.CanAct and M.MonsterData.Chest and not M.MonsterData.Mimic then
        List.Add(M);
    if List.Count > 0 then
    begin
      List[Rnd.Random(List.Count)].Loot.Add(ItemStolen);
      FreeAndNil(List);
      Exit;
    end;
    FreeAndNil(List);
    NewChest;
    NewChest;
    NewChest.Loot.Add(ItemStolen); // no need to randomize here
  end;

begin
  LocalStats.IncStat(Data.ClassName);
  if TargetPlayer.Inventory.HasItem('nude_wristbands') and TargetPlayer.Inventory.HasItem('nude_anklets') then
  begin
    LocalStats.IncStat(Data.ClassName + '_link');
    ShowLog('Before %s can react she realizes that her wristbands and anklets are now interlocked', [TargetPlayer.Data.DisplayName], ColorLogBondage);
    TargetPlayer.Particle('BOUND', ColorParticlePlayerBound);
    Wristbands := ItemsDataDictionary['nude_wristbands'].MainSlot;
    Anklets := ItemsDataDictionary['nude_anklets'].MainSlot;
    Durability := TargetPlayer.Inventory.Equipped[Wristbands].Durability;
    MaxDurability := TargetPlayer.Inventory.Equipped[Wristbands].MaxDurability;
    Durability += TargetPlayer.Inventory.Equipped[Anklets].Durability;
    MaxDurability += TargetPlayer.Inventory.Equipped[Anklets].MaxDurability;
    TargetPlayer.Inventory.UnequipAndDisintegrate(Wristbands, false);
    TargetPlayer.Inventory.UnequipAndDisintegrate(Anklets, false);
    TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['nude_wristbands_linked']));
    TargetPlayer.Inventory.Equipped[Wristbands].Durability := Durability;
    TargetPlayer.Inventory.Equipped[Wristbands].MaxDurability := MaxDurability;
    Sound('lock_manackles');
    ViewGame.WakeUp(true, false);
    //link
  end else
  if TargetPlayer.Inventory.EquippedSlotsRemovable > 0 then
  begin
    if not TargetPlayer.Unsuspecting then
    begin
      ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem, true);
      Sound(MarkData.HitSound);
      ShowLog('%s suddenly notices her %s disappears', [TargetPlayer.Data.DisplayName, ItemStolen.Data.DisplayName], ColorLogItemSteal);
      TargetPlayer.Particle('STEAL', ColorParticleItemStolen);
    end else
    begin
      ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem, false);
      Sound(ItemStolen.ItemData.SoundUnequip);
      ShowLog('%s feels something is missing, but can''t figure out if it''s a dream or not', [TargetPlayer.Data.DisplayName], ColorLogItemSteal);
    end;
    AddItemToExistingChestOrCreateThreeNew;
  end else
  begin
    if TargetPlayer.Unsuspecting then
    begin
      LocalStats.IncStat(Data.ClassName + '_gift');
      repeat
        ItemGift := ItemsDataList[Rnd.Random(ItemsDataList.Count)];
      until ItemGift.StartSpawningAtDepth <= Map.CurrentDepth + 10;
      TMapItem.DropItem(TargetPlayer.LastTileX, TargetPlayer.LastTileY, TInventoryItem.NewItem(ItemGift), false);
      ShowLog('%s has a weird dream of a tooth fairy or something; nothing lewd, just weird', [TargetPlayer.Data.DisplayName], ColorLogItemSteal)
    end else
      ShowLog('%s feels strange, but nothing seems to happen', [TargetPlayer.Data.DisplayName], ColorLogItemSteal)
  end;
end;

{ TMarkStealToChestData ----------------------------- }

procedure TMarkStealToChestData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TMarkStealToChestData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
end;

function TMarkStealToChestData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

function TMarkStealToChestData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('If the mark hits, it will steal any non-restraint item that the character has equipped and teleport it away into any chest on the map. If there are no chests left will create a new chest, and additionally two more empty ones around the map.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('In case the character has Nude Wristbands and Nude Anklets on, will link those extremely slowing down the character and preventing from fighting or doing any other complex operations', []),
    Classname + '_link', 1));
  Result.Add(
    NewEntryText(
      Format('If discovers a heroine sleeping without any items equipped will leave a gift and retreat.', []),
    Classname + '_gift', 1));
end;

function TMarkStealToChestData.Mark: TMarkClass;
begin
  Exit(TMarkStealToChest);
end;

initialization
  RegisterSerializableObject(TMarkStealToChest);
  RegisterSerializableData(TMarkStealToChestData);

end.


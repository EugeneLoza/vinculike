{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Whip will deal small damage to dressed target and high damage if hits unequipped
  has a chance to unequip an
  has a chance to stun character on hit empty slot }
unit GameMarkWhip;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract, GameApparelSlots,
  GameUnlockableEntry;

type
  TMarkWhip = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkWhipData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    DressedDamage: Single;
    ItemDamage: Single;
    NudeDamage: Single;
    StunDuration: Single;
    StunChance: Single;
    AttackSlots: TApparelSlotsSet;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameStats, GameSounds, GamePlayerCharacter, GameLog, GameColors,
  GameActionPlayerStunned, GameViewGame, GameRandom, GameMonster,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkWhipData}
{$INCLUDE marktypecasts.inc}

procedure TMarkWhip.Perform;
var
  E: TApparelSlot;
begin
  LocalStats.IncStat(Data.ClassName);

  repeat
    E := TargetPlayer.Inventory.GetRandomClothesSlot;
  until E in MarkData.AttackSlots;

  Sound(MarkData.HitSound);
  if TargetPlayer.Inventory.Equipped[E] = nil then
  begin
    if (Rnd.Random < MarkData.StunChance) and (Rnd.Random > TargetPlayer.ResistStun) then
    begin
      TargetPlayer.PlayHurtSound;
      TargetPlayer.CurrentAction := TActionPlayerStunned.NewAction(TargetPlayer, MarkData.StunDuration);
      TargetPlayer.CurrentAction.Start;
      TargetPlayer.AddResistStun;
      ShowLog('%s lands a precise hit at %s''s %s and her muscles spasm in pain (%.1n seconds)', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName, EquipmentSlotToHumanReadableString(E), MarkData.StunDuration], ColorLogUnabsorbedDamage);
      TargetPlayer.Particle('STUN', ColorParticlePlayerStunned);
      TryCatchLeash;
    end;
    TargetPlayer.HitHealth(MarkData.NudeDamage);
  end else
  begin
    if not TargetPlayer.Inventory.Bondage[E] then
    begin
      if Rnd.Random > Sqrt(Sqrt(TargetPlayer.ResistMultiplier)) then
      begin
        ShowLog('An accurately landed whip blow knocks %s off %s''s %s', [TargetPlayer.Inventory.Equipped[E].Data.Displayname, TargetPlayer.Data.DisplayName, EquipSlotsToHumanReadableString(TargetPlayer.Inventory.Equipped[E].Data.EquipSlots)], ColorLogUnabsorbedDamage);
        TargetPlayer.Inventory.UnequipAndDrop(E, true);
        TargetPlayer.Particle('STRIP', ColorParticlePlayerDisrobed);
      end else
        TargetPlayer.Inventory.DamageItem(E, MarkData.ItemDamage);
    end;
    TargetPlayer.Hit(MarkData.DressedDamage);
  end;
  ViewGame.WakeUp(true, true);
end;

{ TMarkWhipData ----------------------------- }

procedure TMarkWhipData.Validate;
begin
  inherited Validate;
  if DressedDamage < 0 then
    raise EDataValidationError.CreateFmt('DressedDamage < 0 in %s', [Self.ClassName]);
  if ItemDamage < 0 then
    raise EDataValidationError.CreateFmt('ItemDamage < 0 in %s', [Self.ClassName]);
  if NudeDamage < 0 then
    raise EDataValidationError.CreateFmt('NudeDamage < 0 in %s', [Self.ClassName]);
  if StunDuration < 0 then
    raise EDataValidationError.CreateFmt('StunDuration < 0 in %s', [Self.ClassName]);
  if StunChance < 0 then
    raise EDataValidationError.CreateFmt('StunChance < 0 in %s', [Self.ClassName]);
  if AttackSlots = [] then
    raise EDataValidationError.CreateFmt('AttackSlots = [] in %s', [Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TMarkWhipData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  DressedDamage := Element.AttributeSingle('DressedDamage');
  ItemDamage := Element.AttributeSingle('ItemDamage');
  NudeDamage := Element.AttributeSingle('NudeDamage');
  StunDuration := Element.AttributeSingle('StunDuration');
  StunChance := Element.AttributeSingle('StunChance');
  AttackSlots := [esWeapon, esTopOverOver, esTopOver, esTopUnder, esBottomUnder, esBottomOver];
end;

function TMarkWhipData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will whip the target causing %.1n damage and damaging the hit item by %.1n. If the attacked slot was empty will inflict %.1n damage and paralyze target for %.1n seconds with %d%% chance.', [DressedDamage, ItemDamage, NudeDamage, StunDuration, Round(100*StunChance)]),
    Classname, 1));
end;

function TMarkWhipData.Mark: TMarkClass;
begin
  Exit(TMarkWhip);
end;

initialization
  RegisterSerializableObject(TMarkWhip);
  RegisterSerializableData(TMarkWhipData);

end.


{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkForceEquipAllItems;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract, GameItemData,
  GameUnlockableEntry;

type
  TMarkForceEquipAllItems = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkForceEquipAllItemsData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    BondageItemsList: TItemsDataList;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils, CastleStringUtils,
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameSounds, GameApparelSlots,
  GameViewGame,
  GameItemsDatabase, GameInventoryItem, GamePlayerCharacter, GameStats,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkForceEquipAllItemsData}
{$INCLUDE marktypecasts.inc}

procedure TMarkForceEquipAllItems.Perform;
var
  E: TApparelSlot;
  ItemData: TItemData;
  CanEquip: Boolean;
  Count: Integer;
begin
  LocalStats.IncStat(MarkData.ClassName);

  //unequip everything in the way
  for ItemData in MarkData.BondageItemsList do
    for E in ItemData.EquipSlots do
      if (TargetPlayer.Inventory.Apparel[E] <> nil) and not TargetPlayer.Inventory.Bondage[E] then
      begin
        // show log? TargetPlayer.Inventory.Apparel[E];
        TargetPlayer.Inventory.DisintegrateOrDrop(E, true);
      end;

  // Equip items
  Count := 0;
  for ItemData in MarkData.BondageItemsList do
  begin
    CanEquip := true;
    for E in ItemData.EquipSlots do
      if (TargetPlayer.Inventory.Apparel[E] <> nil) then
      begin
        CanEquip := false;
        break;
      end;
    if CanEquip then
    begin
      Sound(ItemData.SoundEquip);
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemData));
      ShowLog('%s appears out of nowhere and clings to %s''s body', [ItemData.DisplayName, TargetPlayer.Data.DisplayName], ColorLogBondage);
      Inc(Count);
    end;
  end;

  if Count > 0 then
  begin
    ViewGame.ShakeCharacter;
    TargetPlayer.PlaySurpriseSound;
    TargetPlayer.Particle('BOUND', ColorParticlePlayerBound);
  end else
  begin
    // note: but may have unequipped some items
    ShowLog('Luckily nothing serious happens', [], ColorLogBondageItemSave);
  end;

  ViewGame.WakeUp(true, true);
end;

{ TMarkForceEquipAllItemsData ----------------------------- }

procedure TMarkForceEquipAllItemsData.Validate;
begin
  inherited Validate;
  if BondageItemsList.Count = 0 then
    raise EDataValidationError.CreateFmt('%s has no bondage items', [Self.ClassName]);
end;

procedure TMarkForceEquipAllItemsData.Read(const Element: TDOMElement);
var
  ItemsStringList: TCastleStringList;
  S: String;
begin
  inherited Read(Element);
  BondageItemsList := TItemsDataList.Create(false);
  ItemsStringList := CreateTokens(Element.AttributeString('BondageItemsList'), [',']);
  for S in ItemsStringList do
    if ItemsDataDictionary.ContainsKey(S) then
      BondageItemsList.Add(ItemsDataDictionary[S] as TItemData)
    else
      raise Exception.CreateFmt('Item "%s" not found for action %s', [S, Self.ClassName]);
  FreeAndNil(ItemsStringList);
end;

function TMarkForceEquipAllItemsData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will unequip clothes and replace them with runes that debuff the character.', []),
    Self.ClassName, 1));
end;

function TMarkForceEquipAllItemsData.Mark: TMarkClass;
begin
  Exit(TMarkForceEquipAllItems);
end;

destructor TMarkForceEquipAllItemsData.Destroy;
begin
  FreeAndNil(BondageItemsList);
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TMarkForceEquipAllItems);
  RegisterSerializableData(TMarkForceEquipAllItemsData);

end.


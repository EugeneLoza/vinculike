{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkMummify;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract, GameApparelSlots, GameItemData,
  GameUnlockableEntry;

type
  TMarkMummify = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkMummifyData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    MissSound: String;
    StaminaDamage: Single;
    AttackSlots: TApparelSlotsSet;
    SleepingBag, SleepingBagLocked: TItemData;
    WrappingsTop, WrappingsBottom, WrappingsHead: TItemData;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameStats, GameSounds, GamePlayerCharacter, GameLog, GameColors,
  GameRandom, GameMonster,
  GameItemsDatabase, GameInventoryItem,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkMummifyData}
{$INCLUDE marktypecasts.inc}

procedure TMarkMummify.Perform;
var
  ItemStolen: TInventoryItem;
  NewItem: TInventoryItem;
  HasTop, HasBottom, HasHead: Boolean;

  function CanEquip(Slots: TApparelSlotsSet): Boolean;
  var
    E: TApparelSlot;
  begin
    for E in Slots do
      if TargetPlayer.Inventory.Equipped[E] <> nil then
        Exit(false);
    Exit(true);
  end;

var
  E: TApparelSlot;
  StrippableSlots: TApparelSlotsSet;
begin
  HasTop := TargetPlayer.Inventory.HasItem(MarkData.WrappingsTop);
  HasBottom := TargetPlayer.Inventory.HasItem(MarkData.WrappingsBottom);
  HasHead := TargetPlayer.Inventory.HasItem(MarkData.WrappingsHead);

  // Attack sleeping target
  if TargetPlayer.Unsuspecting then
  begin
    LocalStats.IncStat(Data.ClassName + '_sleep');
    if TargetPlayer.Inventory.HasItem(MarkData.SleepingBag) then
    begin
      ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(MarkData.SleepingBag.MainSlot, false);
      NewItem := TInventoryItem.NewItem(MarkData.SleepingBagLocked);
      NewItem.Durability := ItemStolen.Durability;
      NewItem.MaxDurability := ItemStolen.MaxDurability;
      Sound(NewItem.ItemData.SoundEquip);
      FreeAndNil(ItemStolen);
      TargetPlayer.Inventory.EquipItem(NewItem);
      TargetPlayer.Inventory.ReinforceItem(MarkData.SleepingBagLocked.MainSlot);
      ShowLog('%s feels a bit too cozy, but that makes her sleep only deeper', [TargetPlayer.Data.DisplayName], ColorLogBondage);
    end; {else
      ShowLog('%s has a weird dream of being wrapped up in bandages, but it''s just a dream', [TargetPlayer.Data.DisplayName], ColorLogSleep);}
    ParentMonster.Ai.AiFlee := true;
    Exit;
  end;

  // Attack normal
  if HasHead and HasTop and HasBottom then
  begin
    ShowLog('%s is happy with %s''s current look and wanders off', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorDefault);
    ParentMonster.Ai.AiFlee := true;
    Exit;
  end;

  LocalStats.IncStat(Data.ClassName);
  StrippableSlots := [];
  for E in MarkData.AttackSlots do
    if (TargetPlayer.Inventory.Equipped[E] <> nil) and (not TargetPlayer.Inventory.Bondage[E]) then
      StrippableSlots += [E];

  if CanEquip(MarkData.WrappingsBottom.EquipSlots) then
  begin
    TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(MarkData.WrappingsBottom));
    Sound(MarkData.WrappingsBottom.SoundEquip);
    ShowLog('%s sends %s spinning and in a blink of an eye rolls her legs in bandages', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogBondage);
    TargetPlayer.Particle('BOUND', ColorParticlePlayerBound);
  end else
  if CanEquip(MarkData.WrappingsTop.EquipSlots) then
  begin
    TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(MarkData.WrappingsTop));
    Sound(MarkData.WrappingsTop.SoundEquip);
    ShowLog('%s pushes %s off balance and swiftly wraps her arms and torso in tight stripes', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogBondage);
    TargetPlayer.Particle('BOUND', ColorParticlePlayerBound);
  end else
  if CanEquip(MarkData.WrappingsHead.EquipSlots) then
  begin
    TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(MarkData.WrappingsHead));
    Sound(MarkData.WrappingsHead.SoundEquip);
    ShowLog('%s dances around %s and in a moment seals her head in tight ribbon weave', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogBondage);
    TargetPlayer.Particle('BOUND', ColorParticlePlayerBound);
  end else
  if StrippableSlots <> [] then
  begin
    if StrippableSlots * TargetPlayer.Blueprint.EquipmentSlots = [] then
    begin
      ShowError('No strippable slots in GameMarkMummify');
      Exit;
    end;
    repeat
      E := TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem;
    until E in StrippableSlots;
    if Rnd.Random > Sqrt(TargetPlayer.ResistMultiplier) then
    begin
      ShowLog('Before %s can react %s pulls her %s off', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogBondage);
      UnequipAndDrop(E, true);
      TargetPlayer.Particle('STRIP', ColorParticlePlayerDisrobed);
    end else
    begin
      ShowLog('%s tries to remove %s''s %s but she manages to wriggle out', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogBondageItemSave);
      Sound(MarkData.MissSound);
      TargetPlayer.HitStamina(MarkData.StaminaDamage);
    end;
  end else
  begin
    if HasHead or HasTop or HasBottom then
      ShowLog('%s can''t do much more and retreats', [ParentMonster.Data.DisplayName], ColorDefault)
    else
      ShowLog('%s doesn''t want to mess with %s''s outfit and withdraws', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorDefault);
    ParentMonster.Ai.AiFlee := true;
    Exit;
  end;

end;

{ TMarkMummifyData ----------------------------- }

procedure TMarkMummifyData.Validate;
begin
  inherited Validate;
  if StaminaDamage < 0 then
    raise EDataValidationError.CreateFmt('StaminaDamage < 0 in %s', [Self.ClassName]);
  if not SoundExists(MissSound) then
    raise EDataValidationError.CreateFmt('Invalid MissSound = "%s" in %s', [MissSound, Self.ClassName]);
end;

procedure TMarkMummifyData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  MissSound := Element.AttributeString('MissSound');
  StaminaDamage := Element.AttributeSingle('StaminaDamage');

  SleepingBag := ItemsDataDictionary['sleeping_bag'] as TItemData;
  SleepingBagLocked := ItemsDataDictionary['sleeping_bag_locked'] as TItemData;
  WrappingsBottom := ItemsDataDictionary['mummy_wraps_bottom'] as TItemData;
  WrappingsTop := ItemsDataDictionary['mummy_wraps_top'] as TItemData;
  WrappingsHead := ItemsDataDictionary['mummy_wraps_head'] as TItemData;
  AttackSlots := WrappingsBottom.EquipSlots + WrappingsTop.EquipSlots + WrappingsHead.EquipSlots;
end;

function TMarkMummifyData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will try to wrap target into paper stripes, starting from feet and up. Will try to remove any clothes worn in upper wear slots, but will be pretty useless against restraints.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('If encounters the heroine sleeping in a sleeping bag will lock the sleeping bag in tight straps making getting out of it hard.', []),
    Classname + '_sleep', 1));
end;

function TMarkMummifyData.Mark: TMarkClass;
begin
  Exit(TMarkMummify);
end;

initialization
  RegisterSerializableObject(TMarkMummify);
  RegisterSerializableData(TMarkMummifyData);

end.


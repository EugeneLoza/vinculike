{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkAnestheticTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract,
  GameUnlockableEntry;

type
  TMarkAnestheticTarget = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkAnestheticTargetData = class(TMarkAbstractData)
  public const
    StaminaBlackhole = 100;
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    StaminaDamage: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors,
  GameViewGame, GameActor, GameStats, GameSounds,
  GamePlayerCharacter, GameActionPlayerUnconscious,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkAnestheticTargetData}
{$INCLUDE marktypecasts.inc}

procedure TMarkAnestheticTarget.Perform;
begin
  TargetPlayer.DegradeStamina(MarkData.StaminaDamage);
  TargetPlayer.Stamina := -MarkData.StaminaBlackhole;
  Sound(MarkData.HitSound);
  LocalStats.IncStat(Data.ClassName);
  ShowLog('%s smells anesthetic and immediately exhales, but it''s too late and her consciousness fades into blackness', [TargetActor.Data.DisplayName], ColorLogTrap);
  TargetPlayer.CurrentAction := TActionPlayerUnconscious.NewAction(TargetPlayer);
  TargetPlayer.CurrentAction.Start;
  ViewGame.UnPauseGame;
end;

{ TMarkAnestheticTargetData ----------------------------- }

procedure TMarkAnestheticTargetData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if StaminaDamage <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamage %n <= 0 in %s', [StaminaDamage, Self.ClassName]);
end;

procedure TMarkAnestheticTargetData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  StaminaDamage := Element.AttributeSingle('StaminaDamage');
end;

function TMarkAnestheticTargetData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('EFFECT: Forces target unconscious and drains stamina. Target can''t wake up voluntariliy until stamina recovers.', []),
    Classname, 1));
end;

function TMarkAnestheticTargetData.Mark: TMarkClass;
begin
  Exit(TMarkAnestheticTarget);
end;

initialization
  RegisterSerializableObject(TMarkAnestheticTarget);
  RegisterSerializableData(TMarkAnestheticTargetData);

end.


{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkEquipSelf;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract,
  GameItemData,
  GameUnlockableEntry;

type
  TMarkEquipSelf = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkEquipSelfData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    ItemData: TItemData;
    HitSound, MissSound: String;
    StaminaDamageResist: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameInventoryItem,
  GameViewGame, GameSounds, GameActor,
  GameItemsDatabase, GameApparelSlots, GamePlayerCharacter, GameMonster, GameStats,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkEquipSelfData}
{$INCLUDE marktypecasts.inc}

procedure TMarkEquipSelf.Perform;
var
  E: TApparelSlot;
  ItemStolen: TInventoryItem;
  Item: TInventoryItem;
begin
  for E in MarkData.ItemData.EquipSlots do
    if TargetPlayer.Inventory.Bondage[E] then
    begin
      ParentMonster.Ai.AiFlee := true;
      if not TargetPlayer.Unsuspecting then
        ShowLog('%s doesn''t seem to be able to do anything and retreats', [ParentActor.Data.DisplayName], ColorDefault);
      Exit;
    end;

  LocalStats.IncStat(Data.ClassName);
  if TargetPlayer.Unsuspecting or TargetPlayer.Immobilized or (Rnd.Random > TargetPlayer.ResistMultiplier) then
  begin
    Sound(MarkData.HitSound);
    // failed to resist - strip or equip self
    for E in MarkData.ItemData.EquipSlots do
      if TargetPlayer.Inventory.Apparel[E] <> nil then // it's not bondage
      begin
        ItemStolen := UnequipAndDrop(E, true);
        ShowLog('%s skillfully pulls %s off %s''s %s', [ParentActor.Data.DisplayName, ItemStolen.Data.DisplayName, TargetActor.Data.DisplayName, EquipSlotsToHumanReadableString(ItemStolen.Data.EquipSlots)], ColorLogItemSteal);
        TargetPlayer.Particle('STRIP', ColorParticlePlayerDisrobed);
        ViewGame.WakeUp(true, true);
        Exit;
      end;
    // if nothing to undress: equip self

    ShowLog('Before %s can dodge %s attaches itself to her body', [TargetActor.Data.DisplayName, ParentActor.Data.DisplayName], ColorLogTickleBondage);
    TargetPlayer.Particle('BOUND', ColorParticlePlayerBound);
    ViewGame.ShakeMap;
    Item := TInventoryItem.NewItem(MarkData.ItemData);
    Item.MaxDurability := (Item.MaxDurability + Item.ItemData.Durability) / 2.0;
    Item.Durability := Item.MaxDurability * (ParentActor.Health / ParentActor.Data.MaxHealth) * (1.0 - 0.1 * Rnd.Random); // MaxDurability is still random
    Sound(MarkData.ItemData.SoundEquip);
    TargetPlayer.Inventory.EquipItem(Item);
    ParentActor.Health := -1;
    // We cannot Parent.Die as it'll reset this action, and EndAction will crash next (TODO)
    // Also note we don't want to drop loot here? As living clothes will drop itself
  end else
  begin
    Sound(MarkData.MissSound);
    ShowLog('%s makes an advance but %s manages to fend it off', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogStaminaDamage);
    TargetPlayer.HitStamina(MarkData.StaminaDamageResist);
  end;
  ViewGame.WakeUp(true, true);
end;

{ TMarkEquipSelfData ----------------------------- }

procedure TMarkEquipSelfData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if not SoundExists(MissSound) then
    raise EDataValidationError.CreateFmt('Invalid MissSound = "%s" in %s', [MissSound, Self.ClassName]);
  if StaminaDamageResist <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamageResist %n <= 0 in %s', [StaminaDamageResist, Self.ClassName]);
  if ItemData = nil then
    raise EDataValidationError.CreateFmt('ItemData = nil in %s', [Self.ClassName]);
end;

procedure TMarkEquipSelfData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  MissSound := Element.AttributeString('MissSound');
  StaminaDamageResist := Element.AttributeSingle('StaminaDamageResist');
  ItemData := ItemsDataDictionary[Element.AttributeString('ItemData')] as TItemData; // tood: post-processing
end;

function TMarkEquipSelfData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will try to strip target off items preventing from itself to get equipped - and when done, will attach itself to the target.', []),
    Classname, 1));
end;

function TMarkEquipSelfData.Mark: TMarkClass;
begin
  Exit(TMarkEquipSelf);
end;

initialization
  RegisterSerializableObject(TMarkEquipSelf);
  RegisterSerializableData(TMarkEquipSelfData);

end.


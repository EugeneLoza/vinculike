{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkRepairAndStealIntoContainer;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameApparelSlots, GameMarkAbstract, GameMonsterData,
  GameUnlockableEntry;

type
  TMarkRepairAndStealIntoContainer = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  public
    procedure Update(SecondsPassed: Single); override;
  end;

  TMarkRepairAndStealIntoContainerData = class(TMarkAbstractData)
  public
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    AttackSlots: TApparelSlotsSet;
    ContainerMonsterData: TMonsterData;
    StunDuration: Single;
    RepairQuality: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameMonster,
  GameViewGame, GameMap, GameInventoryItem,
  GamePlayerCharacter, GameActor, GameStats, GameActionPlayerStunned,
  GameMonstersDatabase, GameSounds,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkRepairAndStealIntoContainerData}
{$INCLUDE marktypecasts.inc}

procedure TMarkRepairAndStealIntoContainer.Perform;
var
  E: TApparelSlot;
  Container: TMonster;
  ItemStolen: TInventoryItem;
begin
  LocalStats.IncStat(MarkData.ClassName);
  Container := nil;
  for E in MarkData.AttackSlots do
    if (TargetPlayer.Inventory.Apparel[E] <> nil) and not TargetPlayer.Inventory.Bondage[E] then
    begin
      if Container = nil then
      begin
        Container := TMonster.Create;
        Container.Data := MarkData.ContainerMonsterData;
        Container.Reset;
        Container.TeleportCenter(ParentActor.CenterX, ParentActor.CenterY);
        Container.Ai.Guard := false;
        Container.Ai.SoftGuard := false;
        Container.Ai.AiFlee := true;
        Map.MonstersList.Add(Container);
      end;
      ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(E, true);
      ItemStolen.MaxDurability := (1 - MarkData.RepairQuality) * ItemStolen.MaxDurability + MarkData.RepairQuality * ItemStolen.ItemData.Durability;
      ItemStolen.Durability := (ItemStolen.Durability + ItemStolen.MaxDurability) / 2;
      Container.Health += ItemStolen.Durability / 2;
      Sound(ItemStolen.ItemData.SoundUnequip);
      Container.Loot.Add(ItemStolen);
    end;

  if Container <> nil then
  begin
    if TargetPlayer = ViewGame.CurrentCharacter then
      ViewGame.ShakeCharacter;
    ShowLog('%s suddenly feels a spasm in her lungs barely able to move (stun %.1ns)', [TargetPlayer.Data.DisplayName, MarkData.StunDuration], ColorLogItemSteal);
    ShowLog('In an eyeblink %s is stripped of %d of her items', [TargetPlayer.Data.DisplayName, Container.Loot.Count], ColorLogItemSteal);
    ShowLog('which get repaired and tightly packaged into %s', [Container.Data.DisplayName], ColorLogItemSteal);
    // Cannot be resisted
    TargetPlayer.CurrentAction := TActionPlayerStunned.NewAction(TargetActor, MarkData.StunDuration);
    TargetPlayer.CurrentAction.Start;
    TargetPlayer.Particle('STUN', ColorParticlePlayerStunned);
  end else
    ShowLog('Luckily nothing seems to have happened', [], ColorDefault);
end;

procedure TMarkRepairAndStealIntoContainer.Update(SecondsPassed: Single);
begin
  inherited;
  if Phase < -1 then
    Exit; // we already show error in Inherited

  if Phase > Data.Duration then
  begin
    if Sqr(TargetActor.CenterX - CenterX) + Sqr(TargetActor.CenterY - CenterY) <= Sqr(HalfSize + TargetActor.HalfSize) then
      Perform;
    EndAction;
    Exit;
  end;
end;

{ TMarkRepairAndStealIntoContainerData ----------------------------- }

procedure TMarkRepairAndStealIntoContainerData.Validate;
begin
  inherited Validate;
  if AttackSlots = [] then
    raise EDataValidationError.CreateFmt('AttackSlots = [] in %s', [Self.ClassName]);
  if ContainerMonsterData = nil then
    raise EDataValidationError.CreateFmt('ContainerMonsterData = nil in %s', [Self.ClassName]);
  if StunDuration <= 0 then
    raise EDataValidationError.CreateFmt('StunDuration <= 0 in %s', [Self.ClassName]);
  if RepairQuality <= 0 then
    raise EDataValidationError.CreateFmt('RepairQuality <= 0 in %s', [Self.ClassName]);
  if RepairQuality > 1 then
    raise EDataValidationError.CreateFmt('RepairQuality > 1 in %s', [Self.ClassName]);
end;

procedure TMarkRepairAndStealIntoContainerData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  AttackSlots := ApparelSlotsStringToSlotsSet(Element.AttributeString('AttackSlots'));
  ContainerMonsterData := MonstersDataDictionary[Element.AttributeString('ContainerMonsterData')];
  StunDuration := Element.AttributeSingle('StunDuration');
  RepairQuality := Element.AttributeSingle('RepairQuality');
end;

function TMarkRepairAndStealIntoContainerData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('EFFECT: Will steal all the items it can get its hands on and package those into a %s.', [ContainerMonsterData.DisplayName]),
    ClassName, 1));
end;

function TMarkRepairAndStealIntoContainerData.Mark: TMarkClass;
begin
  Exit(TMarkRepairAndStealIntoContainer);
end;

initialization
  RegisterSerializableObject(TMarkRepairAndStealIntoContainer);
  RegisterSerializableData(TMarkRepairAndStealIntoContainerData);

end.


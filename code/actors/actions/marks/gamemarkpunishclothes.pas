{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkPunishClothes;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract, GameApparelSlots, GameStatusEffect,
  GameUnlockableEntry;

type
  TMarkPunishClothes = class(TMarkTargetAbstract)
  strict private const
    StardustSlots = [esWeapon, esTopOverOver, esTopOver, esTopUnder, esBottomOver, esBottomUnder, esFeet];
  strict private
    function GetRandomEquipSlotForStardust: TApparelSlot;
    procedure EquipStardust(E: TApparelSlot);
    procedure InteractWithNudeTarget;
    procedure InteractWithSleepingNudeTarget;
    procedure InteractWithSleepingDressedTarget;
  protected
    procedure Perform; override;
    procedure Miss; override;
  end;

  TMarkPunishClothesData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    Damage: Single;
    StaminaDamage: Single;
    SugarSweetHealingTiny: Single;
    SugarSweetHealingBig: Single;
    DressedStaminaSleepDamage: Single;
    StatusEffects: TStatusEffectsList;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameEnumUtils,
  GameSounds, GameItemData, GameItemsDatabase,
  GameInventoryItem, GamePlayerCharacter, GameMonster, GameActor, GameStats,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkPunishClothesData}
{$INCLUDE marktypecasts.inc}

function TMarkPunishClothes.GetRandomEquipSlotForStardust: TApparelSlot;
begin
  repeat
    Result := TApparelSlot(Rnd.Random(Ord(High(TApparelSlot)) + 1));
  until Result in StardustSlots;
end;

procedure TMarkPunishClothes.EquipStardust(E: TApparelSlot);
var
  Stardust: TItemData;
begin
  // TODO: list as action data
  case E of
    esWeapon: Stardust := ItemsDataDictionary['stardust_hands'] as TItemData;
    esTopOverOver: Stardust := ItemsDataDictionary['stardust_top_overover'] as TItemData;
    esTopOver: Stardust := ItemsDataDictionary['stardust_top_over'] as TItemData;
    esTopUnder: Stardust := ItemsDataDictionary['stardust_top_under'] as TItemData;
    esBottomOver: Stardust := ItemsDataDictionary['stardust_bottom_over'] as TItemData;
    esBottomUnder: Stardust := ItemsDataDictionary['stardust_bottom_under'] as TItemData;
    esFeet: Stardust := ItemsDataDictionary['stardust_feet'] as TItemData;
    else
      raise Exception.CreateFmt('Unexpected equip slot %s', [specialize EnumToStr<TApparelSlot>(E)]);
  end;
  TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(Stardust));
end;

procedure TMarkPunishClothes.InteractWithNudeTarget;
begin
  // Character is awake
  if not TargetPlayer.Unsuspecting then
  begin
    LocalStats.IncStat('TMarkPunishClothes_nudeawake');
    ShowLog('%s retreats', [ParentMonster.Data.DisplayName], ColorLogTwinkleStrip)
  end else
  begin
    // Character is sleeping
    InteractWithSleepingNudeTarget;
  end;
  ParentMonster.Ai.AiFlee := true;
end;

procedure TMarkPunishClothes.InteractWithSleepingNudeTarget;
var
  CanEquipDust: Boolean;
  EquipSlot: TApparelSlot;
  I: Integer;
begin
  CanEquipDust := false;
  for EquipSlot in StardustSlots do
    if TargetPlayer.Inventory.Equipped[EquipSlot] = nil then
    begin
      CanEquipDust := true;
      break;
    end;
  if CanEquipDust then
  begin
    LocalStats.IncStat('TMarkPunishClothes_nudeasleep');
    repeat
      EquipSlot := GetRandomEquipSlotForStardust;
    until TargetPlayer.Inventory.Equipped[EquipSlot] = nil;
    Sound('stardust'); // TODO
    EquipStardust(EquipSlot);
    ShowLog('The sun, the sand, the ocean and the breeze. Everything in such harmony', [], ColorLogTwinkleHurt);
    ShowLog('Why is everybody looking at her? How has she forgotten to wear any clothes to the beach?', [], ColorLogTwinkleHurt);
    ShowLog('%s tries to cover her nudity, but her hands are heavy as lead', [TargetPlayer.Data.DisplayName], ColorLogTwinkleHurt);
    ShowLog('She feels a pleasant tingling on her %s and sweet taste of sugar on her lips.', [EquipmentSlotToHumanReadableString(EquipSlot)], ColorLogTwinkleHurt);
    ShowLog('It''s only a dream. Another silly dream', [], ColorLogTwinkleHurt);
    // heal the heroine
    for I := 0 to Pred(MarkData.StatusEffects.Count) do
      TargetPlayer.Inventory.AddStatusEffectNoStack(MarkData.StatusEffects[I].Clone);
    TargetPlayer.HealMaxHealth(TargetPlayer.PlayerCharacterData.MaxHealth * MarkData.SugarSweetHealingBig);
    TargetPlayer.HealMaxWill(TargetPlayer.PlayerCharacterData.MaxWill * MarkData.SugarSweetHealingBig);
  end;
end;

procedure TMarkPunishClothes.InteractWithSleepingDressedTarget;
var
  I: Integer;
  EquipSlot: TApparelSlot;
  AvailableSlots: TApparelSlotsList;
begin
  // if we come here, we have top or bottom covered
  AvailableSlots := TApparelSlotsList.Create;
  for EquipSlot in StardustSlots do
    if (TargetPlayer.Inventory.Equipped[EquipSlot] = nil) or not TargetPlayer.Inventory.Bondage[EquipSlot] then
    begin
      // make sure to unequip clothes that cover something, others can stay for now, stardust may unequip them too
      if (TargetPlayer.Inventory.Equipped[EquipSlot] <> nil) and (TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.CoversBottom or TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.CoversTop) then
        TargetPlayer.Inventory.UnequipAndDrop(EquipSlot, false);
      AvailableSlots.Add(EquipSlot);
    end;
  if AvailableSlots.Count > 0 then
  begin
    for I := 0 to Rnd.Random(AvailableSlots.Count - 1) do // Will leave at least one slot free, but will add at least one
    begin
      EquipSlot := AvailableSlots[Rnd.Random(AvailableSlots.Count)];
      EquipStardust(EquipSlot);
      AvailableSlots.Remove(EquipSlot);
    end;
  end;
  FreeAndNil(AvailableSlots);

  Sound('stardust'); // TODO
  if not TargetPlayer.Inventory.Nude then
  begin
    // could not unequip some covering items - maybe because those are bondage items? Now it'll hurt badly.
    LocalStats.IncStat('TMarkPunishClothes_dressedasleepunremovable');
    ShowLog('%s dreams of a strong breeze from the ocean trying to pull her clothes off', [TargetPlayer.Data.DisplayName], ColorLogTwinkleHurt);
    ShowLog('She feels an urge to help, but they stuck tight to her body', [], ColorLogTwinkleHurt);
    ShowLog('Suddenly a violent gust blasts her face blinding her and forcing itself into her mouth', [], ColorLogTwinkleHurt);
    ShowLog('%s tries to scream but can''t even breathe as sugar sweet powder blocks her throat', [TargetPlayer.Data.DisplayName], ColorLogTwinkleHurt);
    ShowLog('After a few minutes of panic struggle in agony she passes out from asphyxiation', [], ColorLogTwinkleHurt);
    for EquipSlot in StardustSlots do
      if (TargetPlayer.Inventory.Equipped[EquipSlot] <> nil) and ((TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.CoversBottom) or (TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.CoversTop)) then
      begin
        TargetPlayer.Inventory.UnequipAndDisintegrate(EquipSlot, true);
        EquipStardust(EquipSlot);
      end;
    ShowLog('It all stops as suddenly as started, leaving %s unconscious, hurt and exhausted', [TargetPlayer.Data.DisplayName], ColorLogTwinkleHurt);
    TargetPlayer.DegradeStamina(TargetPlayer.PlayerCharacterData.MaxStamina + MarkData.DressedStaminaSleepDamage);
    TargetPlayer.HitHealth(TargetPlayer.Health / 2);
    TargetPlayer.HitWill(TargetPlayer.Will / 2);
    for I := 0 to Pred(MarkData.StatusEffects.Count) do
      TargetPlayer.Inventory.AddStatusEffectNoStack(MarkData.StatusEffects[I].Clone);
  end else
  begin
    LocalStats.IncStat('TMarkPunishClothes_dressedasleep');
    ShowLog('%s has a beautiful dream of lying nude on a beach in a hot sunny day', [TargetPlayer.Data.DisplayName], ColorLogTwinkleStrip);
    ShowLog('Why is she naked? She doesn''t remember taking her clothes off', [], ColorLogTwinkleStrip);
    ShowLog('Yet it feels so good as calm soft breeze tingles her sensitive parts', [], ColorLogTwinkleStrip);
    ShowLog('There are others around eying her body, but %s is too tired and sleepy to care', [TargetPlayer.Data.DisplayName], ColorLogTwinkleStrip);
    ShowLog('Soon the delightful sensation fades away leaving sweet grains of sugar on her lips', [], ColorLogTwinkleStrip);
    // heal the heroine
    TargetPlayer.DegradeStamina(MarkData.DressedStaminaSleepDamage);
    TargetPlayer.HealMaxHealth(TargetPlayer.PlayerCharacterData.MaxHealth * MarkData.SugarSweetHealingTiny);
    TargetPlayer.HealMaxWill(TargetPlayer.PlayerCharacterData.MaxWill * MarkData.SugarSweetHealingTiny);
    for I := 0 to Pred(MarkData.StatusEffects.Count) do
      TargetPlayer.Inventory.AddStatusEffectNoStack(MarkData.StatusEffects[I].Clone);
  end;
  ParentMonster.Ai.AiFlee := true;
end;

procedure TMarkPunishClothes.Perform;
var
  EquipSlot: TApparelSlot;
  TargetBodyHalf: TBodyHalf;
begin
  if TargetPlayer.Inventory.Nude then
  begin
    InteractWithNudeTarget;
    Exit;
  end else
  if TargetPlayer.Unsuspecting then
  begin
    InteractWithSleepingDressedTarget;
    Exit;
  end;

  LocalStats.IncStat('TMarkPunishClothes');
  TargetBodyHalf := TBodyHalf(Rnd.Random(Ord(High(TBodyHalf)) + 1));
  if (TargetPlayer.Inventory.TopCovered > 0) and (TargetPlayer.Inventory.BottomCovered > 0) then
    //nothing, use random from above
  else
  if TargetPlayer.Inventory.TopCovered > 0 then // will prioritize upper body
    TargetBodyHalf := bhTop
  else
    TargetBodyHalf := bhBottom;

  Sound(MarkData.HitSound);
  TargetPlayer.HitSpecific(MarkData.Damage, TargetBodyHalf);
  if TargetBodyHalf = bhTop then
    ShowLog('%s breath spasms as her breasts are hit by electric charge', [TargetPlayer.Data.DisplayName], ColorLogTwinkleHurt)
  else
    ShowLog('%s nearly fell zapped in her nethers by electricity', [TargetPlayer.Data.DisplayName], ColorLogTwinkleHurt);
  TargetPlayer.HitStamina(MarkData.StaminaDamage);

  if (Rnd.Random > TargetPlayer.ResistMultiplier) or TargetActor.Immobilized then
  begin
    EquipSlot := GetRandomEquipSlotForStardust;
    if TargetPlayer.Inventory.Equipped[EquipSlot] = nil then
    begin
      EquipStardust(EquipSlot);
      Sound(TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.SoundEquip);
      ShowLog('%s showers weakened %s with shiny particles that sick fast to her %s', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName, EquipmentSlotToHumanReadableString(EquipSlot)], ColorLogTwinkleStrip);
      TargetPlayer.Particle('SHINY', ColorParticleTwinkle);
    end;
  end;

  if TargetPlayer.Inventory.Nude then
  begin
    ShowLog('%s is satisfied with the result and stops attacking %s', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogTwinkleStrip);
    ParentMonster.Ai.AiFlee := true;
  end;
end;

procedure TMarkPunishClothes.Miss;
begin
  inherited;
  if TargetPlayer.Inventory.Nude then
    InteractWithNudeTarget;
end;

{ TMarkPunishClothesData ----------------------------- }

procedure TMarkPunishClothesData.Validate;
var
  I: Integer;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if Damage <= 0 then
    raise EDataValidationError.CreateFmt('Damage %n <= 0 in %s', [Damage, Self.ClassName]);
  if StaminaDamage <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamage %n <= 0 in %s', [StaminaDamage, Self.ClassName]);
  if SugarSweetHealingTiny < 0 then
    raise EDataValidationError.CreateFmt('SugarSweetHealingTiny < 0 in %s', [Self.ClassName]);
  if SugarSweetHealingBig < 0 then
    raise EDataValidationError.CreateFmt('SugarSweetHealingBig < 0 in %s', [Self.ClassName]);
  if SugarSweetHealingTiny > 1 then
    raise EDataValidationError.CreateFmt('SugarSweetHealingTiny > 1 in %s', [Self.ClassName]);
  if SugarSweetHealingBig > 1 then
    raise EDataValidationError.CreateFmt('SugarSweetHealingBig > 1 in %s', [Self.ClassName]);
  if DressedStaminaSleepDamage < 0 then
    raise EDataValidationError.CreateFmt('DressedStaminaSleepDamage < 0 in %s', [Self.ClassName]);
  if StatusEffects.Count = 0 then
    raise EDataValidationError.CreateFmt('StatusEffects.Count = 0 in %s', [Self.ClassName]);
  for I := 0 to Pred(StatusEffects.Count) do
    if not StatusEffects[I].Effect.CanBeStatusEffect then
      raise EDataValidationError.CreateFmt('StatusEffects[%d] as %s cannot be status effect in %s', [I, StatusEffects[I].Effect.ClassName, Self.ClassName]);
end;

procedure TMarkPunishClothesData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  Damage := Element.AttributeSingle('Damage');
  StaminaDamage := Element.AttributeSingle('StaminaDamage');
  SugarSweetHealingTiny := Element.AttributeSingle('SugarSweetHealingTiny');
  SugarSweetHealingBig := Element.AttributeSingle('SugarSweetHealingBig');
  DressedStaminaSleepDamage := Element.AttributeSingle('DressedStaminaSleepDamage');
  StatusEffects := LoadStatusEffectsList(Element);
end;

function TMarkPunishClothesData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: If the target has top or bottom covered, will zap with electricity dealing low health damage but high stamina damage.', []),
    'TMarkPunishClothes', 1));
  Result.Add(
    NewEntryText(
      Format('If the target is nude (exposes both top and bottom) will retreat.', []),
    'TMarkPunishClothes_nudeawake', 1));
  Result.Add(
    NewEntryText(
      Format('Encountering a nude sleeping target will give a "little gift" in form of a small health and willpower healing. Will force the target into deep sleep though unable to wake up for some time.', []),
    'TMarkPunishClothes_nudeasleep', 1));
  Result.Add(
    NewEntryText(
      Format('Encountering a dressed target will undress her also givin a tiny gift of healing in the process.', []),
    'TMarkPunishClothes_dressedasleep', 1));
  Result.Add(
    NewEntryText(
      Format('If the target is wearing unremovable restraints that cover private bits, will still remove those and cause a severe health, willpower and stamina damage.', []),
    'TMarkPunishClothes_dressedasleepunremovable', 1));
end;

function TMarkPunishClothesData.Mark: TMarkClass;
begin
  Exit(TMarkPunishClothes);
end;

destructor TMarkPunishClothesData.Destroy;
begin
  FreeAndNil(StatusEffects);
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TMarkPunishClothes);
  RegisterSerializableData(TMarkPunishClothesData);

end.


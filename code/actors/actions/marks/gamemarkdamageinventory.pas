{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkDamageInventory;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameApparelSlots, GameMarkAbstract,
  GameUnlockableEntry;

type
  { Very similar to TMarkDamageInventoryTrap but
    1. Damages only one item, not all
    2. Has different log messages
    3. Has sleep attack special treatment
    Hypothetically can be merged with the latter
    having logs as variables and some other action parameters exposed }
  TMarkDamageInventory = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkDamageInventoryData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public const
  public
    HitSound: String;
    HitHealthSound: String;
    AttackSlots: TApparelSlotsList;
    ItemDamageFraction: Single;
    HealthDamage: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameSounds, GameStats, GameActor, GameEnumUtils,
  GamePlayerCharacter, GameMath, GameViewGame,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkDamageInventoryData}
{$INCLUDE marktypecasts.inc}

procedure TMarkDamageInventory.Perform;
var
  E: TApparelSlot;
  ItemsToDamage: TApparelSlotsList;
  ItemDamage: Single;
begin
  LocalStats.IncStat(Data.ClassName);

  ItemsToDamage := TApparelSlotsList.Create;
  for E in MarkData.AttackSlots do
    if (TargetPlayer.Inventory.Apparel[E] <> nil) and (TargetPlayer.Inventory.Equipped[E].ItemData.MainSlot = E) then // Bondage items included!
      ItemsToDamage.Add(E);
  if ItemsToDamage.Count > 0 then
  begin
    Sound(MarkData.HitSound);
    E := ItemsToDamage.Random;
    ItemDamage := MaxSingle(TargetPlayer.Inventory.Equipped[E].Durability * MarkData.ItemDamageFraction, 1);
    ShowLog('%s grabs %s and tries to break it, damaging by %.1n', [ParentActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName, ItemDamage], ColorLogAbsorbDamage);
    TargetPlayer.Inventory.DamageItem(E, ItemDamage);
  end else
  begin
    Sound(MarkData.HitHealthSound);
    ShowLog('%s swings around %s as if trying to catch an inexistent item', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogStaminaDamage);
    TargetPlayer.Hit(MarkData.HealthDamage);
  end;
  ViewGame.WakeUp(true, true);
  FreeAndNil(ItemsToDamage);
end;

{ TMarkDamageInventoryData ----------------------------- }

procedure TMarkDamageInventoryData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if not SoundExists(HitHealthSound) then
    raise EDataValidationError.CreateFmt('Invalid HitHealthSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TMarkDamageInventoryData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  HitHealthSound := Element.AttributeString('HitHealthSound');
  AttackSlots := specialize StrToEnumsList<TApparelSlot>(Element.AttributeString('AttackSlots'));
  ItemDamageFraction := Element.AttributeSingle('ItemDamageFraction');
  HealthDamage := Element.AttributeSingle('HealthDamage');
end;

function TMarkDamageInventoryData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('EFFECT: Damages most large equipped items for %d%% of their durability. Nude target will suffer %.1n stamina damage without any other harm.', [Round(100 * ItemDamageFraction), HealthDamage]),
    Classname, 1));
end;

function TMarkDamageInventoryData.Mark: TMarkClass;
begin
  Exit(TMarkDamageInventory);
end;

destructor TMarkDamageInventoryData.Destroy;
begin
  FreeAndNil(AttackSlots);
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TMarkDamageInventory);
  RegisterSerializableData(TMarkDamageInventoryData);

end.


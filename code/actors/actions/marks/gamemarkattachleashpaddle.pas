{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkAttachLeashPaddle;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract,
  GameItemData,
  GameUnlockableEntry;

type
  TMarkMarkAttachLeashPaddle = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkMarkAttachLeashPaddleData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    LeashData, CollarData: TItemData;
    HitSound: String;
    LeashLength: Single;
    NearRange: Single;
    { When fails to equip collar or leash }
    StaminaDamage: Single;
    KickDamage: Single;
    DragStaminaDamage: Single;
    KickLength: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils, CastleVectors,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameInventoryItem, GameApparelSlots,
  GameViewGame, GameSounds, GameActor, GameLeash,
  GameItemsDatabase,  GamePlayerCharacter, GameStats, GameActionKnockback,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkMarkAttachLeashPaddleData}
{$INCLUDE marktypecasts.inc}

procedure TMarkMarkAttachLeashPaddle.Perform;
var
  Leash: TLeash;
  Collar: TInventoryItem;
  E: TApparelSlot;
begin
  LocalStats.IncStat(Data.ClassName);
  Sound(MarkData.HitSound);

  // if target close - attach collar/leash, and kick away; also if sleeping - to avoid waking up too early
  if (TargetPlayer.DistanceToSqr(ParentActor) < Sqr(MarkData.NearRange)) or TargetPlayer.Unsuspecting then
  begin
    // if target has non-restraint in collar, try remove it
    if (TargetPlayer.Inventory.Apparel[esNeck] <> nil) and (not TargetPlayer.Inventory.Bondage[esNeck]) then
    begin
      if (Rnd.Random > TargetPlayer.ResistMultiplier) or TargetPlayer.Unsuspecting or TargetPlayer.Immobilized then
      begin
        ShowLog('%s pulls %s from %s''s neck and casts it aside', [ParentActor.Data.DisplayName, TargetPlayer.Inventory.Apparel[esNeck].Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondage);
        UnequipAndDrop(esNeck, not TargetPlayer.Unsuspecting);
      end else
      begin
        ShowLog('%s tries to remove %s from %s''s neck but she manages to dodge', [ParentActor.Data.DisplayName, TargetPlayer.Inventory.Apparel[esNeck].Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondageItemSave);
        TargetPlayer.HitStamina(MarkData.StaminaDamage);
      end;
      Exit;
    end else
    // try equip collar
    if TargetPlayer.Inventory.Apparel[esNeck] = nil then
    begin
      if (Rnd.Random > TargetPlayer.ResistMultiplier) or TargetPlayer.Unsuspecting or TargetPlayer.Immobilized then
      begin
        Collar := TInventoryItem.NewItem(MarkData.CollarData);
        TargetPlayer.Inventory.EquipItem(Collar);
        Sound(MarkData.CollarData.SoundEquip);
        ShowLog('%s zips a tight %s over %s''s neck', [ParentActor.Data.DisplayName, Collar.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondage);
        TargetPlayer.Particle('BOUND', ColorParticlePlayerBound);
      end else
      begin
        ShowLog('%s tries to zip a collar on, but %s manages to break free before it locks tight', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondageItemSave);
        TargetPlayer.HitStamina(MarkData.StaminaDamage);
      end;
      Exit; // will not wake up, will not attach leash, will not kick
    end;

    if TargetPlayer.Inventory.Apparel[esLeash] = nil then
    begin
      if (Rnd.Random > TargetPlayer.ResistMultiplier) or TargetPlayer.Unsuspecting or TargetPlayer.Immobilized then
      begin
        Leash := TLeash.NewLeash(MarkData.LeashData);
        TargetPlayer.Inventory.EquipItem(Leash);
        Leash.LeashLength := MarkData.LeashLength;
        Sound(MarkData.LeashData.SoundEquip);
        ShowLog('%s attaches %s to %s''s %s and holds it ready to play harsh', [ParentActor.Data.DisplayName, Leash.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Inventory.Apparel[esNeck].Data.DisplayName], ColorLogBondage);
        Leash.LeashHolder := Parent;
        TargetPlayer.Particle('LEASH', ColorParticlePlayerBound);
      end else
      begin
        ShowLog('%s tries to leash %s, but she manages to pull her collar ring free', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondageItemSave);
        TargetPlayer.HitStamina(MarkData.StaminaDamage);
      end;
    end else
    // leash is attached
    begin
      Leash := (TargetPlayer.Inventory.Apparel[esLeash] as TLeash);
      if Leash.LeashHolder = nil then
      begin
        Leash.LeashHolder := ParentActor;
        ShowLog('%s catches the loose end of %s', [ParentActor.Data.DisplayName, Leash.Data.DisplayName], ColorLogBondage);
        if Abs(Leash.LeashLength - MarkData.LeashLength) > 0.5 then
        begin
          Leash.LeashLength := MarkData.LeashLength;
          Sound(Leash.ItemData.SoundRepair);
          ShowLog('and tweaks its length for most fun ball-and-paddle fun', [ParentActor.Data.DisplayName, Leash.Data.DisplayName], ColorLogBondage);
        end;
        TargetPlayer.Particle('LEASH', ColorParticlePlayerBound);
      end else
      begin
        // TODO: chance to resist knockback
        E := TargetPlayer.Blueprint.EquipmentSlotsList.Random;
        if (TargetPlayer.Inventory.Apparel[E] <> nil) and (not TargetPlayer.Inventory.Bondage[E]) then
        begin
          ShowLog('%s kicks %s hard knocking %s off her %s', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Apparel[E].Data.DisplayName, EquipSlotsToHumanReadableString(TargetPlayer.Inventory.Apparel[E].Data.EquipSlots)], ColorLogStaminaDamage);
          TargetPlayer.Inventory.DisintegrateOrDrop(E, true);
        end else
          ShowLog('%s slams %s sending her flying away', [ParentActor.Data.DisplayName, Leash.Data.DisplayName], ColorLogStaminaDamage);

        if Rnd.Random > TargetPlayer.ResistKnockback then
        begin
          TargetPlayer.CurrentAction := TActionKnockback.NewAction(Target);
          TargetPlayer.PlayHurtSound;
          TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized := Vector2(Rnd.Random - 0.5, Rnd.Random - 0.5).Normalize; // kicking in random directions
          TActionKnockback(TargetPlayer.CurrentAction).MoveVector := MarkData.KickLength * TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized;
          TargetPlayer.CurrentAction.Start;
          TargetPlayer.Hit(MarkData.KickDamage);
          TargetPlayer.AddResistKnockback;
          ViewGame.ShakeMap;
        end else
          ShowLog('%s manages to hold the ground', [TargetActor.Data.DisplayName], ColorLogStaminaDamage);
      end;
    end;
  end else
  // Target is far away and not sleeping - drag closer
  begin
    ShowLog('%s extends unnaturally far away and pulls %s', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogStaminaDamage);
    TargetPlayer.HitStamina(MarkData.DragStaminaDamage);
    DragTarget;
    TryCatchLeash;
  end;

  ViewGame.WakeUp(true, true);
end;

{ TMarkMarkAttachLeashPaddleData ----------------------------- }

procedure TMarkMarkAttachLeashPaddleData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if LeashData = nil then
    raise EDataValidationError.CreateFmt('LeashData = nil in %s', [Self.ClassName]);
  if CollarData = nil then
    raise EDataValidationError.CreateFmt('CollarData = nil in %s', [Self.ClassName]);
  if LeashLength <= 1 then
    raise EDataValidationError.CreateFmt('LeashLength <= 1 in %s', [Self.ClassName]);
  if NearRange < 3 then
    raise EDataValidationError.CreateFmt('NearRange < 3 in %s', [Self.ClassName]);
  if NearRange > LeashLength then
    raise EDataValidationError.CreateFmt('NearRange > LeashLength in %s', [Self.ClassName]);
  if LeashLength >= 32 then
    raise EDataValidationError.CreateFmt('LeashLength >= 32 in %s', [Self.ClassName]);
  if StaminaDamage <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamage <= 0 in %s', [Self.ClassName]);
  if KickDamage < 0 then
    raise EDataValidationError.CreateFmt('KickDamage < 0 in %s', [Self.ClassName]);
  if DragStaminaDamage < 0 then
    raise EDataValidationError.CreateFmt('DragStaminaDamage < 0 in %s', [Self.ClassName]);
  if KickLength <= 0 then
    raise EDataValidationError.CreateFmt('KickLength <= 0 in %s', [Self.ClassName]);
end;

procedure TMarkMarkAttachLeashPaddleData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  LeashData := ItemsDataDictionary[Element.AttributeString('LeashData')] as TItemData; // tood: post-processing
  CollarData := ItemsDataDictionary[Element.AttributeString('CollarData')] as TItemData; // tood: post-processing
  LeashLength := Element.AttributeSingle('LeashLength');
  NearRange := Element.AttributeSingle('NearRange');
  StaminaDamage := Element.AttributeSingle('StaminaDamage');
  KickDamage := Element.AttributeSingle('KickDamage');
  DragStaminaDamage := Element.AttributeSingle('DragStaminaDamage');
  KickLength := Element.AttributeSingle('KickLength');
end;

function TMarkMarkAttachLeashPaddleData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will attempt to attach %s to heroine''s collar. If there is no collar - will try to equip %s. If the leash is loose, will catch it and hold. After that will attempt to use the leash to play ball-and-paddle with the heroine, kicking her away and pulling back - until whether the leash or heroine breaks.', [LeashData.DisplayName, CollarData.DisplayName]),
    Classname, 1));
end;

function TMarkMarkAttachLeashPaddleData.Mark: TMarkClass;
begin
  Exit(TMarkMarkAttachLeashPaddle);
end;

initialization
  RegisterSerializableObject(TMarkMarkAttachLeashPaddle);
  RegisterSerializableData(TMarkMarkAttachLeashPaddleData);

end.


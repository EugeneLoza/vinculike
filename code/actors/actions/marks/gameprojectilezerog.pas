{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Will suspend the player character in zero g state (immobilized) if hits
  if hits a target suspended in zero g, will knock off all equipped items
  and force TActionKnockback}
unit GameProjectileZeroG;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  CastleUtils,
  GameProjectileAbstract, GameMarkAbstract, GameActor,
  GameUnlockableEntry;

type
  TProjectileZeroG = class(TProjectileAbstract)
  public
    procedure HitWall; override;
    procedure HitTarget(const TargetsHit: array of TActor); override;
  end;

  TProjectileZeroGData = class(TProjectileAbstractData)
  protected
    function CanBeIndependent: Boolean; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    SlamSound: String;
    SuspensionDuration: Single;
    FallDamage: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameSounds, GamePlayerCharacter, GameApparelSlots, GameActionPlayerZeroG, GameActionKnockback,
  GameViewGame, GameLog, GameColors,
  GameStats, GameVinculopediaEntryText;

{$IFDEF SafeTypecast}
{$DEFINE MarkData:=(Data as TProjectileZeroGData)}
{$ELSE}
{$DEFINE MarkData:=TProjectileZeroGData(Data)}
{$ENDIF}

procedure TProjectileZeroG.HitWall;
begin
  //todo: spawn explosion
end;

procedure TProjectileZeroG.HitTarget(const TargetsHit: array of TActor);
const
  StunSlots = [esNeck, esFeet, esBottomUnder, esBottomOver, esTopUnder, esTopOver, esTopOverOver];
var
  TargetPlayer: TPlayerCharacter;
  E: TApparelSlot;
  Count: Integer;
begin
  LocalStats.IncStat(Data.ClassName);
  TargetPlayer := TargetsHit[0] as TPlayerCharacter;
  ViewGame.WakeUp(true, true);
  if TargetPlayer.CurrentAction is TActionPlayerZeroG then
  begin
    Count := 0;
    for E in TargetPlayer.Blueprint.EquipmentSlots do
      if (TargetPlayer.Inventory.Equipped[E] <> nil) and (not TargetPlayer.Inventory.Bondage[E]) then
      begin
        TargetPlayer.Inventory.UnequipAndDrop(E, true);
        Inc(Count);
      end;
    if Count = 0 then
      ShowLog('Helplessly suspended %s is thrown with a violent force', [TargetPlayer.Data.DisplayName], ColorLogItemSteal)
    else
      ShowLog('Another projectile knocks all equipment items off %s and sends her flying across', [TargetPlayer.Data.DisplayName], ColorLogItemSteal);

    // cannot be resisted
    TargetPlayer.CurrentAction := TActionKnockback.NewAction(TargetPlayer);
    TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized := MoveVector.Normalize;
    TActionKnockback(TargetPlayer.CurrentAction).MoveVector := 20 * TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized;
    TargetPlayer.CurrentAction.Start;
    ViewGame.ShakeMap;
    Sound(MarkData.SlamSound);
  end else
  begin
    ShowLog('%s suddenly loses ground under her feet', [TargetPlayer.Data.DisplayName], ColorLogItemSteal);
    TargetPlayer.CurrentAction := TActionPlayerZeroG.NewAction(TargetPlayer, MarkData.SuspensionDuration);
    TActionPlayerZeroG(TargetPlayer.CurrentAction).FallDamage := MarkData.FallDamage;
    TargetPlayer.CurrentAction.Start;
    ViewGame.UnPauseGame;
    Sound(MarkData.HitSound);
  end;
end;

{ TProjectileZeroGData ----------------------------- }

function TProjectileZeroGData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

procedure TProjectileZeroGData.Validate;
begin
  inherited Validate;
  if SuspensionDuration < 0 then
    raise EDataValidationError.CreateFmt('SuspensionDuration < 0 in %s', [Self.ClassName]);
  if FallDamage < 0 then
    raise EDataValidationError.CreateFmt('FallDamage < 0 in %s', [Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if not SoundExists(SlamSound) then
    raise EDataValidationError.CreateFmt('Invalid SlamSound = "%s" in %s', [SlamSound, Self.ClassName]);
end;

procedure TProjectileZeroGData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  SuspensionDuration := Element.AttributeSingle('SuspensionDuration');
  FallDamage := Element.AttributeSingle('FallDamage');
  HitSound := Element.AttributeString('HitSound');
  SlamSound := Element.AttributeString('SlamSound');
end;

function TProjectileZeroGData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Suspends the heroine in zero gravity for a short time. If hits again, will knock off all equipped non-restraint items and kick her away.', []),
    Classname, 1));
end;

function TProjectileZeroGData.Mark: TMarkClass;
begin
  Exit(TProjectileZeroG);
end;

initialization
  RegisterSerializableObject(TProjectileZeroG);
  RegisterSerializableData(TProjectileZeroGData);

end.


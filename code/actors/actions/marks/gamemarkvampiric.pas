{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkVampiric;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract, GameActor,
  GameUnlockableEntry;

type
  TMarkVampiric = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkVampiricData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    Damage: Single;
    VampiricMultiplier: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameStats, GameSounds, GameLog, GameColors,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkVampiricData}
{$INCLUDE marktypecasts.inc}

procedure TMarkVampiric.Perform;
var
  OldTargetHealth, VampiricHealing: Single;
begin
  LocalStats.IncStat(Data.ClassName);
  OldTargetHealth := TargetActor.Health;
  TargetActor.Hit(MarkData.Damage); // will also wake up
  VampiricHealing := (OldTargetHealth - TargetActor.Health) * MarkData.VampiricMultiplier;
  ParentActor.Health += VampiricHealing;
  if VampiricHealing > 0.05 then
    ShowLog('%s receives %.1n health points from sadistic satisfaction', [ParentActor.Data.DisplayName, VampiricHealing], ColorLogVampiric);
  Sound(MarkData.HitSound);
  ParentActor.DamageWeapon;
end;

{ TMarkVampiricData ----------------------------- }

procedure TMarkVampiricData.Validate;
begin
  inherited Validate;
  if Damage <= 0 then
    raise EDataValidationError.CreateFmt('Damage %n <= 0 in %s', [Damage, Self.ClassName]);
  if VampiricMultiplier <= 0 then
    raise EDataValidationError.CreateFmt('VampiricMultiplier %n <= 0 in %s', [VampiricMultiplier, Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TMarkVampiricData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  Damage := Element.AttributeSingle('Damage');
  VampiricMultiplier := Element.AttributeSingle('VampiricMultiplier');
end;

function TMarkVampiricData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will hurt the target for %.1n damage and convert the %d%% of damage dealt to the body into own health buff. The effect can be resisted with any damage absorption, e.g. clothes.', [Damage, Round(VampiricMultiplier * 100)]),
    Classname, 1));
end;

function TMarkVampiricData.Mark: TMarkClass;
begin
  Exit(TMarkVampiric);
end;

initialization
  RegisterSerializableObject(TMarkVampiric);
  RegisterSerializableData(TMarkVampiricData);

end.


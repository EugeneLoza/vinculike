{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkBreakClothes;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameApparelSlots, GameMarkAbstract,
  GameUnlockableEntry;

type
  TMarkBreakClothes = class(TMarkTargetAbstract)
  strict private const
    DisrobingSlots = [esFeet, esBottomOver, esBottomUnder, esTopUnder, esTopOver, esTopOverOver];
  protected
    procedure Perform; override;
  end;

  TMarkBreakClothesData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public const
    //HitNakedDamage = 50;
    //HitNakedDamagePercent = 0.5;
    HitNakedStaminaDamage = 25;
  public
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors,
  GameViewGame,
  GameMapItem, GameInventoryItem,
  GamePlayerCharacter, GameStats,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkBreakClothesData}
{$INCLUDE marktypecasts.inc}

procedure TMarkBreakClothes.Perform;
var
  E: TApparelSlot;
  ItemStolen: TInventoryItem;
  Count: Integer;
  BondageCount: Integer;
begin
  Count := 0;
  BondageCount := 0;
  for E in DisrobingSlots do
    if TargetPlayer.Inventory.Equipped[E] <> nil then
    begin
      if (not TargetPlayer.Inventory.Bondage[E]) and TargetPlayer.Inventory.Equipped[E].ItemData.CanBeRepaired then
      begin
        Inc(Count);
        if TargetPlayer.Inventory.Equipped[E].ItemData.CanBeRepaired then
        begin
          ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(E, true);
          if ItemStolen <> nil then
          begin
            ItemStolen.Broken := true;
            TMapItem.DropItem(TargetPlayer.LastTileX + TargetPlayer.PredSize div 2, TargetPlayer.LastTileY + TargetPlayer.PredSize div 2, ItemStolen, false);
          end;
        end;
      end else
        Inc(BondageCount);
    end;
  if Count > 0 then
  begin
    LocalStats.IncStat(Data.ClassName);
    ShowLog('Tiny sharp hooks catch %s''s clothes and pull them off without hurting her', [TargetPlayer.Data.DisplayName], ColorLogItemBreak);
    TargetPlayer.Particle('DISROBED', ColorParticlePlayerDisrobed);
    ViewGame.ShakeCharacter;
  end else
  begin
    LocalStats.IncStat(Data.ClassName + '_nude');
    {if BondageCount < 4 then
      ShowLog('Dozens of sharp hooks slide over %s''s nude skin leaving shallow but very painful scratches', [TargetPlayer.Data.DisplayName], ColorLogUnabsorbedDamage)
    else
      ShowLog('Sharp hooks slide over %s''s restraints leaving painful scratches on uncovered areas', [TargetPlayer.Data.DisplayName], ColorLogUnabsorbedDamage);
    TargetPlayer.DegradeHealth(Min(TargetPlayer.Health / 2, MarkData.HitNakedDamage));
    TargetPlayer.Health := TargetPlayer.Health * (1 - MarkData.HitNakedDamagePercent);
    ViewGame.ShakeMap;}
    if TargetPlayer.Inventory.Nude then
      ShowLog('Dozens of sharp hooks slide over %s''s nude skin without hurting her', [TargetPlayer.Data.DisplayName], ColorLogNaked)
    else
    begin
      if BondageCount = 0 then // this should be impossible
        ShowError('%s is not nude, but doesn''t have clothes and bondage items', [TargetPlayer.Data.DisplayName]);
      // WARNING: if the character had only unrepairable clothes on then this message will be misleading, but let it stay so for now
      ShowLog('Sharp hooks stick to %s''s restraints and she struggles to pull free', [TargetPlayer.Data.DisplayName], ColorLogStaminaDamage);
      TargetPlayer.HitStamina(BondageCount * MarkData.HitNakedStaminaDamage);
    end;
  end;
end;

{ TMarkBreakClothesData ----------------------------- }

procedure TMarkBreakClothesData.Validate;
begin
  inherited Validate;
end;

procedure TMarkBreakClothesData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
end;

function TMarkBreakClothesData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('EFFECT: Will unequip and apply "broken" status to all equipped items.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('Mostly harmless against nude target, unless heavily restrained.', []),
    Classname + '_nude', 1));
end;

function TMarkBreakClothesData.Mark: TMarkClass;
begin
  Exit(TMarkBreakClothes);
end;

initialization
  RegisterSerializableObject(TMarkBreakClothes);
  RegisterSerializableData(TMarkBreakClothesData);

end.


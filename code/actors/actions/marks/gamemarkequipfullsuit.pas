{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkEquipFullSuit;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract,
  GameInventoryItem,
  GameUnlockableEntry;

type
  TMarkEquipFullSuit = class(TMarkTargetAbstract)
  strict private
    procedure DoDrop(const ItemStolen: TInventoryItem);
  protected
    procedure Perform; override;
  end;

  TMarkEquipFullSuitData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    StaminaDamageDressed: Single;
    StaminaDamageNude: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors,
  GameViewGame, GameSounds, GameMapItem, GameActor,
  GameItemData, GameItemsDatabase, GameApparelSlots, GamePlayerCharacter, GameMonster, GameStats,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkEquipFullSuitData}
{$INCLUDE marktypecasts.inc}

procedure TMarkEquipFullSuit.DoDrop(const ItemStolen: TInventoryItem);
begin
  Sound(ItemStolen.ItemData.SoundUnequip);
  TMapItem.DropItem(ParentMonster.LastTileX + ParentMonster.Size div 2, ParentMonster.LastTileY + ParentMonster.Size div 2, ItemStolen, false);
end;

procedure TMarkEquipFullSuit.Perform;
var
  FullSuit: TItemData;

  function GetRemovableItem(const Forced: Boolean): TInventoryItem;
  var
    E: TApparelSlot;
  begin
    for E in FullSuit.EquipSlots do
      if (TargetPlayer.Inventory.Equipped[E] <> nil) and not TargetPlayer.Inventory.Bondage[E] then
        Exit(TargetPlayer.Inventory.UnequipAndReturn(E, Forced));
    Exit(nil);
  end;
  function DestroyAllRemainingItems: Boolean;
  var
    E: TApparelSlot;
  begin
    Result := false;
    for E in FullSuit.EquipSlots do
      if TargetPlayer.Inventory.Equipped[E] <> nil then
      begin
        Result := true;
        TargetPlayer.Inventory.DisintegrateOrDrop(E, false);
      end;
  end;

  function TryReinforce(const ItemName: String): Boolean;
  var
    ItemToReinforce: TItemData;
  begin
    ItemToReinforce := ItemsDataDictionary[ItemName] as TItemData;
    if TargetPlayer.Inventory.HasItem(ItemToReinforce) then
    begin
      LocalStats.IncStat(Data.ClassName + '_reinforce');
      TargetPlayer.Inventory.ReinforceItem(ItemToReinforce.MainSlot);
      ShowLog('As %s is already wearing a %s, %s patches it up and retreats', [TargetActor.Data.DisplayName, ItemToReinforce.DisplayName, ParentActor.Data.DisplayName], ColorLogTickleBondage);
      Sound(ItemToReinforce.SoundRepair);
      ParentMonster.Ai.AiFlee := true;
      Exit(true);
    end;
    Exit(false);
  end;

  function TryReinforceOrLock(const ItemName, ItemLockedName: String): Boolean;
  var
    Catsuit, CatsuitLocked: TItemData;
    CatsuitAdded, CatsuitRemoved: TInventoryItem;
    E: TApparelSlot;
  begin
    Catsuit := ItemsDataDictionary[ItemName] as TItemData;
    CatsuitLocked := ItemsDataDictionary[ItemLockedName] as TItemData;
    if TargetPlayer.Inventory.HasItem(Catsuit) then
    begin
      LocalStats.IncStat(Data.ClassName + '_reinforce_catsuit');
      TargetPlayer.Inventory.ReinforceItem(Catsuit.MainSlot);
      if Rnd.Random < 0.2 then
      begin
        ShowLog('%s wraps around %s''s body squeezing her tightly', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickleBondage);
        CatsuitRemoved := TargetPlayer.Inventory.UnequipAndReturn(Catsuit.MainSlot, false);
        CatsuitAdded := TInventoryItem.NewItem(CatsuitLocked);
        CatsuitAdded.Durability := CatsuitRemoved.Durability;
        CatsuitAdded.MaxDurability := CatsuitRemoved.MaxDurability;
        for E in CatsuitAdded.Data.EquipSlots do
          if TargetPlayer.Inventory.Equipped[E] <> nil then
          begin
            ShowLog('%s is in the way, so off it goes', [TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogItemSteal);
            TargetPlayer.Inventory.DisintegrateOrDrop(E, false);
          end;
        TargetPlayer.HitStamina(MarkData.StaminaDamageDressed);
        ShowLog('Before she can react all holes on her %s are sealed solid', [Catsuit.DisplayName], ColorLogTickleBondage);
        ShowLog('There isn''t any hope to take %s off without breaking', [CatsuitLocked.DisplayName], ColorLogTickleBondage);
        TargetPlayer.Particle('BOUND', ColorParticlePlayerBound);
        FreeAndNil(CatsuitRemoved);
        TargetPlayer.Inventory.EquipItem(CatsuitAdded);
        Sound(CatsuitLocked.SoundEquip);
      end else
      begin
        ShowLog('%s apparently approves %s''s feel of style and repairs %s on her', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, Catsuit.DisplayName], ColorLogInventory);
        Sound(Catsuit.SoundRepair);
      end;
      ParentMonster.Ai.AiFlee := true;
      Exit(true);
    end;
    Exit(false);
  end;

  function TryRepair(const ItemName, ItemRepairedName: String): Boolean;
  var
    Catsuit, CatsuitRepaired: TItemData;
    CatsuitAdded, CatsuitRemoved: TInventoryItem;
    E: TApparelSlot;
  begin
    Catsuit := ItemsDataDictionary[ItemName] as TItemData;
    CatsuitRepaired := ItemsDataDictionary[ItemRepairedName] as TItemData;
    if TargetPlayer.Inventory.HasItem(Catsuit) then
    begin
      LocalStats.IncStat(Data.ClassName + '_reinforce_catsuit');
      ShowLog('%s makes an unexpected dash to %s and wraps around her', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogInventory);
      CatsuitRemoved := TargetPlayer.Inventory.UnequipAndReturn(Catsuit.MainSlot, false);
      CatsuitAdded := TInventoryItem.NewItem(CatsuitRepaired);
      CatsuitAdded.Durability := CatsuitRemoved.Durability;
      CatsuitAdded.MaxDurability := CatsuitRemoved.MaxDurability;
      for E in CatsuitAdded.Data.EquipSlots do
        if TargetPlayer.Inventory.Equipped[E] <> nil then
        begin
          ShowLog('%s is in the way, so off it goes', [TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogItemSteal);
          TargetPlayer.Inventory.DisintegrateOrDrop(E, false);
        end;
      TargetPlayer.HitStamina(MarkData.StaminaDamageDressed);
      ShowLog('However after a short struggle lets her go and retreats', [], ColorLogInventory);
      ShowLog('Only a few seconds later she notices %s is patched and repaired', [CatsuitRepaired.DisplayName], ColorLogInventory);
      FreeAndNil(CatsuitRemoved);
      TargetPlayer.Inventory.EquipItem(CatsuitAdded);
      TargetPlayer.Inventory.ReinforceItem(CatsuitAdded.Data.MainSlot);
      Sound(CatsuitRepaired.SoundRepair);
      ParentMonster.Ai.AiFlee := true;
      Exit(true);
    end;
    Exit(false);
  end;

var
  WakeUpChance, ResistChance: Single;
  ItemStolen: TInventoryItem;
  Item: TInventoryItem;
begin
  // If same already equipped, reinforce and flee
  if TryReinforce('full_suit') or TryReinforce('catsuit_tight_locked') or TryReinforce('catsuit_loose_locked') then
    Exit
  else
  if TryReinforceOrLock('catsuit_tight_full', 'catsuit_tight_locked') or TryReinforceOrLock('catsuit_loose_full', 'catsuit_loose_locked') then
    Exit
  else
  if TryRepair('catsuit_tight_broken', 'catsuit_tight_half') or TryRepair('catsuit_tight_half', 'catsuit_tight_full') or TryRepair('catsuit_loose_broken', 'catsuit_loose_full') then
    Exit;

  FullSuit := ItemsDataDictionary['full_suit'] as TItemData;
  ItemStolen := GetRemovableItem(not TargetPlayer.Unsuspecting);
  if ItemStolen <> nil then
  begin
    if TargetPlayer.Unsuspecting then
    begin
      LocalStats.IncStat(Data.ClassName + '_sleeping');
      WakeUpChance := 0.5 * TargetPlayer.ResistMultiplier;
      if WakeUpChance < 0.2 then
        WakeUpChance := 0.2;
      if Rnd.Random < WakeUpChance then
      begin
        ShowLog('%s feels %s being pulled off her %s', [TargetActor.Data.DisplayName, ItemStolen.Data.DisplayName, EquipSlotsToHumanReadableString(ItemStolen.Data.EquipSlots)], ColorLogItemSteal);
        TargetPlayer.Particle('STRIP', ColorParticlePlayerDisrobed);
        Sound(MarkData.HitSound);
        ViewGame.WakeUp(true, true);
      end else
        ShowLog('%s carefully removes %s from %s without waking her up', [ParentActor.Data.DisplayName, ItemStolen.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogItemSteal);
    end else
    begin
      LocalStats.IncStat(Data.ClassName + '_undress');
      ShowLog('%s catches %s and pulls off her %s before she manages to break free', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, ItemStolen.Data.DisplayName], ColorLogItemSteal);
      TargetPlayer.Particle('STRIP', ColorParticlePlayerDisrobed);
      Sound(MarkData.HitSound);
      TargetPlayer.HitStamina(MarkData.StaminaDamageDressed);
    end;
    DoDrop(ItemStolen);
  end else
  begin
    LocalStats.IncStat(Data.ClassName + '_equipself');
    ResistChance := TargetPlayer.ResistMultiplier;
    if ResistChance < 0.3 then
      ResistChance := 0.3;
    if TargetActor.Immobilized then
      ResistChance := 0;
    if not TargetPlayer.Inventory.CanUseHands or TargetPlayer.Unsuspecting then
      ResistChance := ResistChance / 2;
    if Rnd.Random < ResistChance then
    begin
      ViewGame.WakeUp(true, true);
      ShowLog('%s tries to wrap around %s and she barely struggles out', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondageItemSave);
      TargetPlayer.HitStamina(MarkData.StaminaDamageNude);
      Sound(MarkData.HitSound);
    end else
    begin
      ViewGame.WakeUp(true, false);
      if DestroyAllRemainingItems then
      begin
        ShowLog('With surprising ease %s gets rid of restraints on %s', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickleBondage);
        ShowLog('And before she gets a chance to escape, embraces her %s', [EquipSlotsToHumanReadableString(FullSuit.EquipSlots)], ColorLogTickleBondage);
      end else
        ShowLog('%s wraps around %s''s exposed %s and tightens itself before she can react', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, EquipSlotsToHumanReadableString(FullSuit.EquipSlots)], ColorLogTickleBondage);
      TargetPlayer.Particle('BOUND', ColorParticlePlayerBound);
      ViewGame.ShakeMap;
      Item := TInventoryItem.NewItem(FullSuit);
      Item.Durability := Item.MaxDurability * (ParentActor.Health / ParentActor.Data.MaxHealth) * (1 - 0.1 * Rnd.Random); // MaxDurability is still random
      Sound(FullSuit.SoundEquip);
      TargetPlayer.Inventory.EquipItem(Item);
      ParentActor.Health := -1;
      // We cannot Parent.Die as it'll reset this action, and EndAction will crash next (TODO)
      // Also note we don't want to drop loot here? As living clothes will drop itself
      Exit;
    end;
  end;
end;

{ TMarkEquipFullSuitData ----------------------------- }

procedure TMarkEquipFullSuitData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if StaminaDamageDressed <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamageDressed %n <= 0 in %s', [StaminaDamageDressed, Self.ClassName]);
  if StaminaDamageNude <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamageNude %n <= 0 in %s', [StaminaDamageNude, Self.ClassName]);
end;

procedure TMarkEquipFullSuitData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  StaminaDamageDressed := Element.AttributeSingle('StaminaDamageDressed');
  StaminaDamageNude := Element.AttributeSingle('StaminaDamageNude');
end;

function TMarkEquipFullSuitData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: If target has some clothes preventing from equipping self over it the monster will remove those, causing stamina damage in the process.', []),
    Classname + '_undress', 1));
  Result.Add(
    NewEntryText(
      Format('If the target is sleeping the undressing will be much easier for the attacker.', []),
    Classname + '_sleeping', 1));
  Result.Add(
    NewEntryText(
      Format('If the target is ready to be wrapped into the living suit, it will try to equip self over it. Success depends on how much stamina the target has left, and in case of failure will deal high stamina damage. If succeeds, will remove all restraints on target and force equip self over it.', []),
    Classname + '_equipself', 1));
  Result.Add(
    NewEntryText(
      Format('If the target is already wearing a Full Suit, will instead patch it up and retreat.', []),
    ClassName + '_reinforce', 1));
  Result.Add(
    NewEntryText(
      Format('Can also from time to time be helfpul and repair a catsuit that cannot be repaired in any other way. However be careful, can also lock it and make unremovable without breaking.', []),
    ClassName + '_reinforce_catsuit', 1));
end;

function TMarkEquipFullSuitData.Mark: TMarkClass;
begin
  Exit(TMarkEquipFullSuit);
end;

initialization
  RegisterSerializableObject(TMarkEquipFullSuit);
  RegisterSerializableData(TMarkEquipFullSuitData);

end.


{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkCatchAndHold;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract, GameApparelSlots,
  GameUnlockableEntry;

type
  TMarkCatchAndHold = class(TMarkTargetAbstract)
  private const
    AttackSlots = [esBottomOver, esBottomUnder, esTopOver, esTopUnder, esTopOverOver];
  protected
    procedure Perform; override;
  end;

  TMarkCatchAndHoldData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public const
    MaxTeleportRange = 29;
  public
    HitSound: String;
    MissSound: String;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameSounds,
  GameViewGame, GameActor, GameActionPlayerHeld,
  GamePlayerCharacter, GameStats,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkCatchAndHoldData}
{$INCLUDE marktypecasts.inc}

procedure TMarkCatchAndHold.Perform;
var
  E: TApparelSlot;
begin
  LocalStats.IncStat(Data.ClassName);

  ViewGame.WakeUp(true, true);

  if TargetPlayer.CurrentAction is TActionPlayerHeld then
  begin
    // target is already held, don't catch her again - drain stamina instead
    ShowLog('%s makes sure %s won''t get a chance to pull her limbs free', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondage);
    TargetPlayer.HitStamina(25 + TargetPlayer.MaxStamina * 0.02);
    Sound(MarkData.HitSound);
  end else
  begin
    repeat
      E := TargetPlayer.Inventory.GetRandomClothesSlot;
    until E in AttackSlots;

    if TargetPlayer.Inventory.Apparel[E] <> nil then
    begin
      ShowLog('%s catches %s by her %s and holds tight', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogBondage);
      TargetPlayer.CurrentAction := TActionPlayerHeld.NewAction(TargetPlayer, 2.0);
      TActionPlayerHeld(TargetPlayer.CurrentAction).HeldBy := ParentActor.ReferenceId;
      TargetPlayer.Particle('HOLD', ColorParticlePlayerStunned);
      TargetPlayer.CurrentAction.Start;
      Sound(MarkData.HitSound);
    end else
    if TargetPlayer.Immobilized then
    begin
      ShowLog('%s grabs helpless %s and squeezes tight with its many fingers', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondage);
      TargetPlayer.CurrentAction := TActionPlayerHeld.NewAction(TargetPlayer, 2.0);
      TargetPlayer.Particle('HOLD', ColorParticlePlayerStunned);
      TActionPlayerHeld(TargetPlayer.CurrentAction).HeldBy := ParentActor.ReferenceId;
      TargetPlayer.CurrentAction.Start;
      Sound(MarkData.HitSound);
    end else
    begin
      if Rnd.Random < 0.5 then
      begin
        ShowLog('%s tries to grab %s''s %s but she manages to slip out', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, EquipmentSlotToHumanReadableString(E)], ColorLogInventory);
        TargetPlayer.HitStamina(12 + TargetPlayer.MaxStamina * 0.01);
        Sound(MarkData.MissSound);
      end else
      begin
        ShowLog('%s grabs %s''s %s and wraps her in tight embrace', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, EquipmentSlotToHumanReadableString(E)], ColorLogBondage);
        TargetPlayer.Particle('HOLD', ColorParticlePlayerStunned);
        TargetPlayer.CurrentAction := TActionPlayerHeld.NewAction(TargetPlayer, 2.0);
        TActionPlayerHeld(TargetPlayer.CurrentAction).HeldBy := ParentActor.ReferenceId;
        TargetPlayer.HitStamina(25 + TargetPlayer.MaxStamina * 0.01);
        TargetPlayer.CurrentAction.Start;
        Sound(MarkData.HitSound);
      end;
    end;
  end;
end;

{ TMarkCatchAndHoldData ----------------------------- }

procedure TMarkCatchAndHoldData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if not SoundExists(MissSound) then
    raise EDataValidationError.CreateFmt('Invalid MissSound = "%s" in %s', [MissSound, Self.ClassName]);
end;

procedure TMarkCatchAndHoldData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  MissSound := Element.AttributeString('MissSound');
end;

function TMarkCatchAndHoldData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will attempt to catch and immobilize the target for some time. If catches by clothes or restraints, will always succeed, otherwise target has 50%% chance to slip out.', []),
    Classname, 1));
end;

function TMarkCatchAndHoldData.Mark: TMarkClass;
begin
  Exit(TMarkCatchAndHold);
end;

initialization
  RegisterSerializableObject(TMarkCatchAndHold);
  RegisterSerializableData(TMarkCatchAndHoldData);

end.


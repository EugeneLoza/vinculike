{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkDrainWillpower;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract, GameActor,
  GameUnlockableEntry;

type
  TMarkDrainWillpower = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkDrainWillpowerData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound, SleepAttackSound: String;
    DamageDressed, DamageNude, SleepDamage: Single;
    WillToHealthRatio: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameStats, GameSounds, GameLog, GameColors, GameMonster, GamePlayerCharacter,
  GameEnchantmentWalkNudeWillpowerDamageMultiplier,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkDrainWillpowerData}
{$INCLUDE marktypecasts.inc}

procedure TMarkDrainWillpower.Perform;
var
  OldTargetWill, VampiricHealing: Single;
  NudeMultiplier: Single;
begin

  OldTargetWill := TargetPlayer.Will;
  if TargetActor.Unsuspecting then
  begin
    LocalStats.IncStat(Data.ClassName + '_sleep');
    ShowLog('%s has an obscure nightmare', [TargetActor.Data.DisplayName], ColorLogWillDamage);
    if TargetPlayer.Will > MarkData.SleepDamage * 2 then
      TargetPlayer.HitWill(MarkData.SleepDamage)
    else
      TargetPlayer.HitWill(TargetPlayer.Will / 2);
    ParentMonster.Ai.AiFlee := true;
    Sound(MarkData.SleepAttackSound);
  end else
  begin
    LocalStats.IncStat(Data.ClassName);
    if TargetPlayer.Inventory.Nude then
    begin
      NudeMultiplier := TargetPlayer.Inventory.FindEffectMultiplier(TEnchantmentWalkNudeWillpowerDamageMultiplier);
      if NudeMultiplier > 0.5 then
        ShowLog('%s suddenly feels extremely exposed and vulnerable', [TargetActor.Data.DisplayName], ColorLogVampiric)
      else
        ShowLog('%s stares at %s with unblinking photosensors but she manages to keep her cool', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogVampiric);
      TargetPlayer.HitWill(MarkData.DamageNude * NudeMultiplier);
    end else
    begin
      ShowLog('%s has a brief but extremely unsettling hallucination', [TargetActor.Data.DisplayName], ColorLogVampiric);
      TargetPlayer.HitWill(MarkData.DamageDressed);
    end;
    Sound(MarkData.HitSound);
  end;
  VampiricHealing := (OldTargetWill - TargetPlayer.Will) * MarkData.WillToHealthRatio;
  if VampiricHealing > 0.05 then
  begin
    ParentActor.Health += VampiricHealing;
    ShowLog('%s converts collected fear into %.1n health points', [ParentActor.Data.DisplayName, VampiricHealing], ColorLogWillDamage);
  end;
end;

{ TMarkDrainWillpowerData ----------------------------- }

procedure TMarkDrainWillpowerData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if not SoundExists(SleepAttackSound) then
    raise EDataValidationError.CreateFmt('Invalid SleepAttackSound = "%s" in %s', [SleepAttackSound, Self.ClassName]);
  if DamageDressed <= 0 then
    raise EDataValidationError.CreateFmt('DamageDressed %n <= 0 in %s', [DamageDressed, Self.ClassName]);
  if DamageNude <= 0 then
    raise EDataValidationError.CreateFmt('DamageNude %n <= 0 in %s', [DamageNude, Self.ClassName]);
  if SleepDamage <= 0 then
    raise EDataValidationError.CreateFmt('SleepDamage %n <= 0 in %s', [SleepDamage, Self.ClassName]);
  if WillToHealthRatio <= 0 then
    raise EDataValidationError.CreateFmt('WillToHealthRatio %n <= 0 in %s', [WillToHealthRatio, Self.ClassName]);
end;

procedure TMarkDrainWillpowerData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  SleepAttackSound := Element.AttributeString('SleepAttackSound');
  DamageDressed := Element.AttributeSingle('DamageDressed');
  DamageNude := Element.AttributeSingle('DamageNude');
  SleepDamage := Element.AttributeSingle('SleepDamage');
  WillToHealthRatio := Element.AttributeSingle('WillToHealthRatio');
end;

function TMarkDrainWillpowerData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will drain %.1n willpower from the target and convert it into own health buff. If the target is nude, will take %.1n willpower damage, which is affected by "willpower damage from walking nude" enchantment.', [DamageDressed, DamageNude]),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('Attack in sleep will deal %.1n willpower damage and the monster will retreat.', [SleepDamage]),
    Classname + '_sleep', 1));
end;

function TMarkDrainWillpowerData.Mark: TMarkClass;
begin
  Exit(TMarkDrainWillpower);
end;

initialization
  RegisterSerializableObject(TMarkDrainWillpower);
  RegisterSerializableData(TMarkDrainWillpowerData);

end.


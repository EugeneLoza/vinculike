{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Hits the Target}
unit GameProjectileHurt;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  CastleUtils,
  GameProjectileAbstract, GameMarkAbstract, GameActor,
  GameUnlockableEntry;

type
  TProjectileHurt = class(TProjectileAbstract)
  public
    procedure HitWall; override;
    procedure HitTarget(const TargetsHit: array of TActor); override;
  end;
  TProjectileHurtData = class(TProjectileAbstractData)
  protected
    function CanBeIndependent: Boolean; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Damage: Single;
    HitSound: String;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameSounds, GameStats,
  GameVinculopediaEntryText;

{$IFDEF SafeTypecast}
{$DEFINE MarkData:=(Data as TProjectileHurtData)}
{$ELSE}
{$DEFINE MarkData:=TProjectileHurtData(Data)}
{$ENDIF}

procedure TProjectileHurt.HitWall;
begin
  //todo: spawn explosion
end;

procedure TProjectileHurt.HitTarget(const TargetsHit: array of TActor);
begin
  LocalStats.IncStat(Data.ClassName);
  TargetsHit[0].Hit(MarkData.Damage);
  Sound(MarkData.HitSound);
end;

{ TProjectileHurtData ----------------------------- }

function TProjectileHurtData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

procedure TProjectileHurtData.Validate;
begin
  inherited Validate;
  if Damage <= 0 then
    raise EDataValidationError.CreateFmt('Damage <= 0 in %s', [Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TProjectileHurtData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  Damage := Element.AttributeSingle('Damage');
  HitSound := Element.AttributeString('HitSound');
end;

function TProjectileHurtData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: The projectile will hurt the target for %.1n upon impact.', [Damage]),
    Classname, 1));
end;

function TProjectileHurtData.Mark: TMarkClass;
begin
  Exit(TProjectileHurt);
end;

initialization
  RegisterSerializableObject(TProjectileHurt);
  RegisterSerializableData(TProjectileHurtData);

end.


{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkRuneThief;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract, GameItemData,
  GameUnlockableEntry;

type
  TMarkRuneThief = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkRuneThiefData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    StealSound: String;
    RuneSound: String;
    StunDuration: Single;
    SleepingRuneHealing: Single;
    BondageItemsList: TItemsDataList;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils, CastleStringUtils,
  GameSerializableObject, GameSerializableData,
  GameMonster, GameActionPlayerStunned,
  GameLog, GameColors, GameSounds, GameApparelSlots,
  GameViewGame,
  GameItemsDatabase, GameInventoryItem, GamePlayerCharacter, GameActor, GameStats,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkRuneThiefData}
{$INCLUDE marktypecasts.inc}

procedure TMarkRuneThief.Perform;

  procedure TryReinforceRunes;
  var
    ItemData: TItemData;
  begin
    for ItemData in MarkData.BondageItemsList do
      if TargetPlayer.Inventory.HasItem(ItemData) then
        TargetPlayer.Inventory.ReinforceItem(ItemData.MainSlot);
  end;

  function HasAllRunes: Boolean;
  var
    ItemData: TItemData;
  begin
    for ItemData in MarkData.BondageItemsList do
      if not TargetPlayer.Inventory.HasItem(ItemData) then
        Exit(false);
    Exit(true);
  end;

  function TryStealItem(const Forced: Boolean): Boolean;
  var
    E: TApparelSlot;
    ItemData: TItemData;
    ItemStolen: TInventoryItem;
  begin
    for ItemData in MarkData.BondageItemsList do
      for E in ItemData.EquipSlots do
        if (TargetPlayer.Inventory.Apparel[E] <> nil) and not TargetPlayer.Inventory.Bondage[E] then
        begin
          ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(E, Forced);
          ParentMonster.Loot.Add(ItemStolen);
          if TargetPlayer.Unsuspecting then
            ShowLog('%s feels something''s missing', [TargetPlayer.Data.DisplayName], ColorLogItemSteal)
          else
          begin
            ShowLog('%s''s %s disappears inside %s''s husk', [TargetPlayer.Data.DisplayName, ItemStolen.Data.DisplayName, ParentActor.Data.DisplayName], ColorLogItemSteal);
            Sound(MarkData.StealSound);
          end;
          TargetPlayer.Particle('STEAL', ColorParticleItemStolen);
          Exit(true);
        end;
    Exit(false);
  end;

  function TryEquipRune: Boolean;
  var
    E: TApparelSlot;
    ItemData: TItemData;
    CanEquip: Boolean;
  begin
    for ItemData in MarkData.BondageItemsList do
    begin
      CanEquip := true;
      for E in ItemData.EquipSlots do
        if (TargetPlayer.Inventory.Apparel[E] <> nil) then
        begin
          CanEquip := false;
          break;
        end;
      if CanEquip then
      begin
        TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemData));
        TryReinforceRunes;
        if TargetPlayer.Unsuspecting then
        begin
          LocalStats.IncStat(MarkData.ClassName + '_healing');
          TargetPlayer.HealMaxWill(MarkData.SleepingRuneHealing);
          ShowLog('A strange warm feeling passes through %s''s body', [TargetPlayer.Data.DisplayName], ColorLogBondageItemSave);
          ShowLog('Or was it her soul that was warmed up?', [TargetPlayer.Data.DisplayName], ColorLogBondageItemSave);
        end else
        begin
          ShowLog('%s touches %s and a %s appears on her skin', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, ItemData.DisplayName], ColorLogBondage);
          TargetPlayer.PlaySurpriseSound;
          ViewGame.ShakeCharacter;
          TargetPlayer.Particle('BOUND', ColorParticlePlayerBound);
          Sound(MarkData.RuneSound);
        end;
        Sound(ItemData.SoundEquip);
        Exit(true);
      end;
    end;
    Exit(false);
  end;

begin
  if TargetPlayer.Unsuspecting then
  begin
    LocalStats.IncStat(MarkData.ClassName + '_sleeping');
    if not TryStealItem(false) then
      TryEquipRune;
    ParentMonster.Ai.AiFlee := true;
    Exit;
  end else
  begin
    LocalStats.IncStat(MarkData.ClassName);
    if not TryStealItem(true) then
    if not TryEquipRune then
    begin
      TryReinforceRunes;
      if HasAllRunes then
      begin
        ShowLog('With the farewell touch runes on %s''s skin flash stunning her (%.1ns)', [TargetActor.Data.DisplayName, MarkData.StunDuration], ColorLogStaminaDamage);
        // cannot be resisted
        TargetActor.CurrentAction := TActionPlayerStunned.NewAction(TargetActor, MarkData.StunDuration);
        TargetActor.CurrentAction.Start;
        TargetPlayer.Particle('STUN', ColorParticlePlayerStunned);
      end else
      begin
        ShowLog('%s doesn''t seem to be able to do anything more and flees', [ParentMonster.Data.DisplayName], ColorLogItemSteal);
      end;
      ParentMonster.Ai.AiFlee := true;
    end;
    ViewGame.WakeUp(true, true);
  end;
end;

{ TMarkRuneThiefData ----------------------------- }

procedure TMarkRuneThiefData.Validate;
begin
  inherited Validate;
  if not SoundExists(StealSound) then
    raise EDataValidationError.CreateFmt('Invalid StealSound = "%s" in %s', [StealSound, Self.ClassName]);
  if not SoundExists(RuneSound) then
    raise EDataValidationError.CreateFmt('Invalid RuneSound = "%s" in %s', [RuneSound, Self.ClassName]);
  if StunDuration <= 0 then
    raise EDataValidationError.CreateFmt('StunDuration %n <= 0 in %s', [StunDuration, Self.ClassName]);
  if SleepingRuneHealing <= 0 then
    raise EDataValidationError.CreateFmt('SleepingRuneHealing %n <= 0 in %s', [SleepingRuneHealing, Self.ClassName]);
  if BondageItemsList.Count = 0 then
    raise EDataValidationError.CreateFmt('%s has no bondage items', [Self.ClassName]);
end;

procedure TMarkRuneThiefData.Read(const Element: TDOMElement);
var
  ItemsStringList: TCastleStringList;
  S: String;
begin
  inherited Read(Element);
  StealSound := Element.AttributeString('StealSound');
  RuneSound := Element.AttributeString('RuneSound');
  StunDuration := Element.AttributeSingle('StunDuration');
  SleepingRuneHealing := Element.AttributeSingle('SleepingRuneHealing');

  BondageItemsList := TItemsDataList.Create(false);
  ItemsStringList := CreateTokens(Element.AttributeString('BondageItemsList'), [',']);
  for S in ItemsStringList do
    if ItemsDataDictionary.ContainsKey(S) then
      BondageItemsList.Add(ItemsDataDictionary[S] as TItemData)
    else
      raise Exception.CreateFmt('Item "%s" not found for action %s', [S, Self.ClassName]);
  FreeAndNil(ItemsStringList);
end;

function TMarkRuneThiefData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will steal clothes and replace them with runes that debuff the character.', []),
    Self.ClassName, 1));
  Result.Add(
    NewEntryText(
      Format('In sleep will steal items silently and will apply runes without waking up the character.', []),
    Self.ClassName + '_sleeping', 1));
  Result.Add(
    NewEntryText(
      Format('If the character is sleeping - in addition to applying runes to nude body will also provide some small willpower healing.', []),
    Self.ClassName + '_healing', 1));
end;

function TMarkRuneThiefData.Mark: TMarkClass;
begin
  Exit(TMarkRuneThief);
end;

destructor TMarkRuneThiefData.Destroy;
begin
  FreeAndNil(BondageItemsList);
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TMarkRuneThief);
  RegisterSerializableData(TMarkRuneThiefData);

end.


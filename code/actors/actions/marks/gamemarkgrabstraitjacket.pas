{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkGrabStraitjacket;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract, GameActor, GameApparelSlots,
  GameUnlockableEntry;

type
  TMarkGrabStraitjacket = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkGrabStraitjacketData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public const
    AttackSlots = [
      esTopOver, esTopOverOver, esWeapon, // obviously slots of straitjacket; maybe add automatically or validate
      esBottomOver, esFeet
    ];
  public
    SoundSleepAttack: String;
    WakeUpHealthDamage: Single;
    UndressStaminaDamage: Single;
    CollarAttackChance: Single;
    CollarStaminaDamage: Single;
    CollarHealthDamage: Single;
    CollarWillDamage: Single;
    AttackStaminaDamage: Single;
    AttackWillDamage: Single;
    StraitjacketEquipStaminaDamage: Single;
    PullStaminaDamage: Single;
    PushDistance: Single;
    StraitjacketEquipPushDistance: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleVectors, CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GamePlayerCharacter, GameItemsDatabase, GameItemData, GameInventoryItem, GameSounds,
  GameActionKnockback,
  GameLog, GameViewGame, GameColors, GameStats, GameRandom,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkGrabStraitjacketData}
{$INCLUDE marktypecasts.inc}

procedure TMarkGrabStraitjacket.Perform;
var
  E: TApparelSlot;
  Straitjacket: TItemData;
  Count: Integer;

  procedure PushTarget(const Distance: Single);
  begin
    // cannot be resisted
    TargetPlayer.PlayGruntSound;
    TargetPlayer.CurrentAction := TActionKnockback.NewAction(Target);
    TActionKnockback(TargetPlayer.CurrentAction).MoveVector := Vector2(TargetPlayer.CenterX - ParentActor.CenterX, TargetPlayer.CenterY - ParentActor.CenterY);
    TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized := TActionKnockback(TargetPlayer.CurrentAction).MoveVector.Normalize;
    TActionKnockback(TargetPlayer.CurrentAction).MoveVector := Distance * TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized;
    TargetPlayer.CurrentAction.Start;
    ViewGame.ShakeMap;
  end;

begin
  StraitJacket := ItemsDataDictionary['straitjacket'] as TItemData;

  // attack sleeping target
  if TargetActor.Unsuspecting then
  begin
    LocalStats.IncStat(Data.ClassName + '_sleep');
    ShowLog('%s rudely pokes %s', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickle);
    ViewGame.WakeUp(false, true);
    Sound(MarkData.SoundSleepAttack);
    if TargetActor.Unsuspecting then // failed to wake up normally
    begin
      ShowLog('As she isn''t responding, %s is annoyed and impatient', [ParentActor.Data.DisplayName], ColorLogBondage);
      if TargetPlayer.Inventory.HasItem(Straitjacket) then
      begin
        TargetPlayer.Inventory.ReinforceItem(Straitjacket.MainSlot);
        Sound(Straitjacket.SoundRepair);
        ShowLog('It checks if straightjacket sits right and is locked properly', [], ColorLogBondage);
        ShowLog('And then lightly but painfully smacks %s into face', [TargetActor.Data.DisplayName], ColorLogBondage);
      end else
      begin
        Count := 0;
        for E in StraitJacket.EquipSlots do
          if TargetPlayer.Inventory.Equipped[E] <> nil then
          begin
            Inc(Count);
            TargetPlayer.Inventory.DisintegrateOrDrop(E, false);
          end;
        TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(Straitjacket));
        Sound(Straitjacket.SoundEquip);
        if Count > 0 then
          ShowLog('It strips, forces %s into a straitjacket and slaps her in face', [TargetActor.Data.DisplayName], ColorLogBondage)
        else
          ShowLog('It wraps %s into a straitjacket and violently shakes her', [TargetActor.Data.DisplayName], ColorLogBondage);
        TargetPlayer.Particle('BOUND', ColorParticlePlayerBound);
      end;
      TryCatchLeash;
      TargetPlayer.HitHealth(MarkData.WakeUpHealthDamage);
      ViewGame.WakeUp(true, true);
    end;
  end else

  // attack on close range - do nasties
  if (TargetActor.DistanceTo(ParentActor) < MarkData.Range / 2) or TargetActor.Immobilized then
  begin
    Count := 0;
    for E in MarkData.AttackSlots do
      if (TargetPlayer.Inventory.Equipped[E] <> nil) and (not TargetPlayer.Inventory.Bondage[E]) then
        Inc(Count);

    // Strip attack slots
    if (Count > 0) and (not TargetActor.Immobilized) then
    begin
      LocalStats.IncStat(Data.ClassName);
      repeat
        E := TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem;
      until E in MarkData.AttackSlots;
      ShowLog('%s grabs %s forcefully pulls %s off her %s and kicks away', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName, EquipSlotsToHumanReadableString(TargetPlayer.Inventory.Equipped[E].Data.EquipSlots)], ColorLogItemSteal);
      TargetPlayer.Inventory.UnequipAndDrop(E, true);
      TargetPlayer.Particle('STRIP', ColorParticlePlayerDisrobed);
      PushTarget(MarkData.PushDistance);
      TargetPlayer.HitStamina(MarkData.UndressStaminaDamage);
    end else
    // Count = 0 or Immobilized
    begin
      LocalStats.IncStat(Data.ClassName + '_straitjacket');
      // has straitjacket - play with target
      if TargetPlayer.Inventory.HasItem(Straitjacket) then
      begin
        if (Rnd.Random < MarkData.CollarAttackChance) and TargetPlayer.Inventory.HasItem('metal_collar') then
        begin
          ShowLog('%s catches helpless %s by her collar ring and raises her above the ground', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondage);
          ShowLog('She gasps and wriggles helplessly until finally manages to jostle the attacker away', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondage);
          PushTarget(MarkData.PushDistance);
          TargetPlayer.HitStamina(MarkData.CollarStaminaDamage);
          TargetPlayer.HitHealth(MarkData.CollarHealthDamage);
          TargetPlayer.HitWill(MarkData.CollarWillDamage);
        end else
        begin
          ShowLog('%s grabs %s by straightjacket and tosses her around like a toy', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondage);
          DragTarget;
          TryCatchLeash;
          TargetPlayer.HitStamina(MarkData.AttackStaminaDamage);
          TargetPlayer.HitWill(MarkData.AttackWillDamage);
        end;
      end else
      // doesn't have straitjacket yet - try equip
      begin
        if (Rnd.Random > TargetPlayer.ResistMultiplier) or TargetActor.Immobilized then
        // failed to resist
        begin
          // strip everything that's remaining (most likely restraints, but can be everything else if immobilized)
          Count := 0;
          for E in StraitJacket.EquipSlots do
            if TargetPlayer.Inventory.Equipped[E] <> nil then
            begin
              Inc(Count);
              TargetPlayer.Inventory.DisintegrateOrDrop(E, false);
            end;
          // and put straitjacket on
          TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(Straitjacket));
          Sound(Straitjacket.SoundEquip);
          TargetPlayer.Particle('BOUND', ColorParticlePlayerHurt);
          if TargetActor.Immobilized then
          begin
            ShowLog('%s makes a good use of incapacitated %s forcing her into subdued pose', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondage);
            if Count > 0 then
              ShowLog('After getting rid of all items that were in the way', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondage);
            ShowLog('Quickly locks the straitjacket on her now fully immobilized arms', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondage);
          end else
          begin
            ShowLog('%s fully subdues %s to her knees and forces her hands behind her back', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondage);
            if Count > 0 then
              ShowLog('With surprising ease gettring rid of her restraints', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondage);
            ShowLog('Zips the straitjacket on and pushes her off', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondage);
            PushTarget(MarkData.StraitjacketEquipPushDistance);
            TryReleaseLeash;
            TargetPlayer.HitStamina(MarkData.StraitjacketEquipStaminaDamage);
          end;
        end else
        // resist getting bound
        begin
          ShowLog('%s grabs %s''s hands and twists painfully behind her back', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondageItemSave);
          ShowLog('Only by sheer luck she manages to pull them out before the straightjacket locks tight', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondageItemSave);
          DragTarget;
          TryCatchLeash;
          TargetPlayer.HitStamina(MarkData.StraitjacketEquipStaminaDamage * 2);
        end;
      end;
    end;
  end else

  // attack on long range - pull closer
  begin
    ShowLog('%s catches %s and pulls her closer', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickle);
    DragTarget;
    TryCatchLeash;
    TargetPlayer.HitStamina(MarkData.PullStaminaDamage);
  end;
end;

{ TMarkGrabStraitjacketData ----------------------------- }

procedure TMarkGrabStraitjacketData.Validate;
begin
  inherited Validate;
  if not SoundExists(SoundSleepAttack) then
    raise EDataValidationError.CreateFmt('Invalid SoundSleepAttack = "%s" in %s', [SoundSleepAttack, Self.ClassName]);
  if WakeUpHealthDamage <= 0 then
    raise EDataValidationError.CreateFmt('WakeUpHealthDamage %n <= 0 in %s', [WakeUpHealthDamage, Self.ClassName]);
  if UndressStaminaDamage <= 0 then
    raise EDataValidationError.CreateFmt('UndressStaminaDamage %n <= 0 in %s', [UndressStaminaDamage, Self.ClassName]);
  if CollarAttackChance < 0 then
    raise EDataValidationError.CreateFmt('CollarAttackChance %n < 0 in %s', [CollarAttackChance, Self.ClassName]);
  if CollarStaminaDamage <= 0 then
    raise EDataValidationError.CreateFmt('CollarStaminaDamage %n <= 0 in %s', [CollarStaminaDamage, Self.ClassName]);
  if CollarHealthDamage <= 0 then
    raise EDataValidationError.CreateFmt('CollarHealthDamage %n <= 0 in %s', [CollarHealthDamage, Self.ClassName]);
  if CollarWillDamage <= 0 then
    raise EDataValidationError.CreateFmt('CollarWillDamage %n <= 0 in %s', [CollarWillDamage, Self.ClassName]);
  if AttackStaminaDamage <= 0 then
    raise EDataValidationError.CreateFmt('AttackStaminaDamage %n <= 0 in %s', [AttackStaminaDamage, Self.ClassName]);
  if AttackWillDamage <= 0 then
    raise EDataValidationError.CreateFmt('AttackWillDamage %n <= 0 in %s', [AttackWillDamage, Self.ClassName]);
  if StraitjacketEquipStaminaDamage <= 0 then
    raise EDataValidationError.CreateFmt('StraitjacketEquipStaminaDamage %n <= 0 in %s', [StraitjacketEquipStaminaDamage, Self.ClassName]);
  if PullStaminaDamage <= 0 then
    raise EDataValidationError.CreateFmt('PullStaminaDamage %n <= 0 in %s', [PullStaminaDamage, Self.ClassName]);
  if PushDistance <= 0 then
    raise EDataValidationError.CreateFmt('PushDistance %n <= 0 in %s', [PushDistance, Self.ClassName]);
  if StraitjacketEquipPushDistance <= 0 then
    raise EDataValidationError.CreateFmt('StraitjacketEquipPushDistance %n <= 0 in %s', [StraitjacketEquipPushDistance, Self.ClassName]);
end;

procedure TMarkGrabStraitjacketData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  SoundSleepAttack := Element.AttributeString('SoundSleepAttack');
  WakeUpHealthDamage := Element.AttributeSingle('WakeUpHealthDamage');
  UndressStaminaDamage := Element.AttributeSingle('UndressStaminaDamage');
  CollarAttackChance := Element.AttributeSingle('CollarAttackChance');
  CollarStaminaDamage := Element.AttributeSingle('CollarStaminaDamage');
  CollarHealthDamage := Element.AttributeSingle('CollarHealthDamage');
  CollarWillDamage := Element.AttributeSingle('CollarWillDamage');
  AttackStaminaDamage := Element.AttributeSingle('AttackStaminaDamage');
  AttackWillDamage := Element.AttributeSingle('AttackWillDamage');
  StraitjacketEquipStaminaDamage := Element.AttributeSingle('StraitjacketEquipStaminaDamage');
  PullStaminaDamage := Element.AttributeSingle('PullStaminaDamage');
  PushDistance := Element.AttributeSingle('PushDistance');
  StraitjacketEquipPushDistance := Element.AttributeSingle('StraitjacketEquipPushDistance');
end;

function TMarkGrabStraitjacketData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will pull the target closer and if close enough - will undress item-by-item and push away.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('If the heroine is fully undressed or immobilized then will zip a straitjacket on her and toss her around subduing and draining stamina.', []),
    Classname + '_straitjacket', 1));
  Result.Add(
    NewEntryText(
      Format('Will not attack the heroine in sleep but will wake up instead. Unless she can''t wake up for some reason (e.g. too exhausted) then will lock her into straitjacket out of irritation.', []),
    Classname + '_sleep', 1));
end;

function TMarkGrabStraitjacketData.Mark: TMarkClass;
begin
  Exit(TMarkGrabStraitjacket);
end;

initialization
  RegisterSerializableObject(TMarkGrabStraitjacket);
  RegisterSerializableData(TMarkGrabStraitjacketData);

end.


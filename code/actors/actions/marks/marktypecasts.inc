{$IFDEF SafeTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE MarkData:=(Data as DataClass)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE MarkData:=DataClass(Data)}
{$ENDIF}


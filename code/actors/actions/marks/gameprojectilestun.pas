{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Drains stamina or stuns target if hits not occupied inventory slot }
unit GameProjectileStun;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  CastleUtils,
  GameProjectileAbstract, GameMarkAbstract, GameActor,
  GameUnlockableEntry;

type
  TProjectileStun = class(TProjectileAbstract)
  public
    procedure HitWall; override;
    procedure HitTarget(const TargetsHit: array of TActor); override;
  end;
  TProjectileStunData = class(TProjectileAbstractData)
  protected
    function CanBeIndependent: Boolean; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    StunDuration: Single;
    StunChance: Single;
    StaminaDamage: Single;
    ClothesDamage: Single;
    HitSound: String;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameSounds, GamePlayerCharacter, GameApparelSlots, GameActionPlayerStunned,
  GameViewGame, GameLog, GameColors, GameRandom, GameStats,
  GameVinculopediaEntryText;

{$IFDEF SafeTypecast}
{$DEFINE MarkData:=(Data as TProjectileStunData)}
{$ELSE}
{$DEFINE MarkData:=TProjectileStunData(Data)}
{$ENDIF}

procedure TProjectileStun.HitWall;
begin
  //todo: spawn explosion
end;

procedure TProjectileStun.HitTarget(const TargetsHit: array of TActor);
const
  StunSlots = [esNeck, esFeet, esBottomUnder, esBottomOver, esTopUnder, esTopOver, esTopOverOver];
var
  TargetPlayer: TPlayerCharacter;
  E: TApparelSlot;
begin
  TargetPlayer := TargetsHit[0] as TPlayerCharacter;
  ViewGame.WakeUp(true, true);
  repeat
    E := TargetPlayer.Inventory.GetRandomClothesSlot;
  until E in StunSlots;
  if TargetPlayer.Inventory.Equipped[E] = nil then
  begin
    LocalStats.IncStat(Data.ClassName);
    if (Rnd.Random < MarkData.StunChance) and (Rnd.Random > TargetPlayer.ResistStun) then
    begin
      TargetPlayer.CurrentAction := TActionPlayerStunned.NewAction(TargetPlayer, MarkData.StunDuration);
      TargetPlayer.CurrentAction.Start;
      TargetPlayer.AddResistStun;
      ShowLog('Hit by a stun needle in her %s %s''s muscles spasm (%.1n seconds)', [EquipmentSlotToHumanReadableString(E), TargetPlayer.Data.DisplayName, MarkData.StunDuration], ColorLogNotEnoughStamina);
      TargetPlayer.Particle('STUN', ColorParticlePlayerStunned);
    end else
    begin
      ShowLog('A feathered needle leaves a scratch on %s''s %s. She suddenly feels tired.', [TargetPlayer.Data.DisplayName, EquipmentSlotToHumanReadableString(E)], ColorLogNotEnoughStamina);
      TargetPlayer.HitStamina(MarkData.StaminaDamage);
    end;
  end else
  begin
    LocalStats.IncStat(Data.ClassName + '_clothes');
    ShowLog('A feathered needle slides along %s leaving a harmless droplet of tranquilizer on it', [TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogAbsorbDamage);
    TargetPlayer.Inventory.DamageItem(E, MarkData.ClothesDamage);
  end;
  Sound(MarkData.HitSound);
end;

{ TProjectileStunData ----------------------------- }

function TProjectileStunData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

procedure TProjectileStunData.Validate;
begin
  inherited Validate;
  if ClothesDamage < 0 then
    raise EDataValidationError.CreateFmt('ClothesDamage < 0 in %s', [Self.ClassName]);
  if StaminaDamage < 0 then
    raise EDataValidationError.CreateFmt('StaminaDamage < 0 in %s', [Self.ClassName]);
  if StunDuration <= 0 then
    raise EDataValidationError.CreateFmt('StunDuration <= 0 in %s', [Self.ClassName]);
  if StunChance < 0 then
    raise EDataValidationError.CreateFmt('StunChance < 0 in %s', [Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TProjectileStunData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  ClothesDamage := Element.AttributeSingle('ClothesDamage');
  StaminaDamage := Element.AttributeSingle('StaminaDamage');
  StunDuration := Element.AttributeSingle('StunDuration');
  StunChance := Element.AttributeSingle('StunChance');
  HitSound := Element.AttributeString('HitSound');
end;

function TProjectileStunData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Anesthetic needles can cause brief muscle spasm or drain stamina if those hit skin.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('As the needle is very short, rather a prick, hitting an article of clothes or restraints will cause no harm.', []),
    Classname + '_clothes', 1));
end;

function TProjectileStunData.Mark: TMarkClass;
begin
  Exit(TProjectileStun);
end;

initialization
  RegisterSerializableObject(TProjectileStun);
  RegisterSerializableData(TProjectileStunData);

end.


{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkCraftFromStamina;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract, GameItemData,
  GameUnlockableEntry;

type
  TMarkCraftFromStamina = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkCraftFromStaminaData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
    function CanBeIndependent: Boolean; override;
  public
    HitSound, SleepHitSound: String;
    StaminaDamage, NudeStaminaDamage, SleepStaminaDamage: Single;
    UnconsciousStaminaThreshold: Single;
    CraftMultiplier, NudeCraftMultiplier, SleepCraftMultiplier: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameSounds,
  GameApparelSlots,
  GameItemsDatabase, GameInventoryItem, GameMap,
  GamePlayerCharacter, GameActionPlayerUnconscious, GameMonster,
  GameViewGame, GameRandom, GameStats, GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkCraftFromStaminaData}
{$INCLUDE marktypecasts.inc}

procedure TMarkCraftFromStamina.Perform;
var
  CraftedItemData: TItemData = nil;

  procedure CraftItem(const Multiplier: Single);
  var
    AItem: TInventoryItem;
  begin
    repeat
      CraftedItemData := ItemsDataList[Rnd.Random(ItemsDataList.Count)];
    until (not CraftedItemData.Bondage) and (CraftedItemData.StartSpawningAtDepth <= Map.CurrentDepth * Multiplier);
    AItem := TInventoryItem.NewItem(CraftedItemData);
    AItem.AddRandomEnchantments(Multiplier * 0.5, Multiplier * Sqrt(Map.CurrentDepth));
    ParentMonster.Loot.Add(AItem);
  end;

var
  StaminaDamage: Single;
begin
  if TargetPlayer.Unsuspecting then
  begin
    if TargetPlayer.Stamina > 0 then
    begin
      LocalStats.IncStat(Data.ClassName + '_sleep');
      CraftItem(MarkData.SleepCraftMultiplier);
      Sound(MarkData.SleepHitSound);
      ShowLog('%s suddenly feels exhausted in her sleep', [TargetPlayer.Data.DisplayName], ColorLogStaminaDamage);
      // no report on crafted item
      TargetPlayer.HitStamina(MarkData.SleepStaminaDamage);
    end;
    ParentMonster.Ai.AiFlee := true;
  end else
  begin
    LocalStats.IncStat(Data.ClassName);
    Sound(MarkData.HitSound);

    if TargetPlayer.Inventory.Nude then
      StaminaDamage := MarkData.NudeStaminaDamage
    else
      StaminaDamage := MarkData.StaminaDamage;

    if TargetPlayer.Stamina > 0 then
    begin
      if TargetPlayer.Inventory.Nude then
        CraftItem(MarkData.NudeCraftMultiplier)
      else
        CraftItem(MarkData.CraftMultiplier);
      ShowLog('%s drains strength out of %s and turns it into %s', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName, CraftedItemData.DisplayName], ColorLogStaminaDamage);
    end;
    // no else - will just show "lost stamina" log from HitStamina below

    TargetPlayer.HitStamina(StaminaDamage);

    if TargetPlayer.Stamina < MarkData.UnconsciousStaminaThreshold then
    begin
      ShowLog('%s collapses from unnatural exhaustion', [TargetPlayer.Data.DisplayName], ColorLogStaminaExhausted);
      TargetPlayer.CurrentAction := TActionPlayerUnconscious.NewAction(TargetPlayer);
      TargetPlayer.CurrentAction.Start;
      ViewGame.UnPauseGame; // to properly set the UI
      ParentMonster.Ai.AiFlee := true;
    end;
  end;
end;

{ TMarkCraftFromStaminaData ----------------------------- }

procedure TMarkCraftFromStaminaData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if not SoundExists(SleepHitSound) then
    raise EDataValidationError.CreateFmt('Invalid SleepHitSound = "%s" in %s', [SleepHitSound, Self.ClassName]);
  if StaminaDamage <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamage <= 0 in %s', [Self.ClassName]);
  if NudeStaminaDamage <= 0 then
    raise EDataValidationError.CreateFmt('NudeStaminaDamage <= 0 in %s', [Self.ClassName]);
  if SleepStaminaDamage <= 0 then
    raise EDataValidationError.CreateFmt('SleepStaminaDamage <= 0 in %s', [Self.ClassName]);
  // UnconsciousStaminaThreshold not validated, can be anything
  if CraftMultiplier <= 0 then
    raise EDataValidationError.CreateFmt('CraftMultiplier <= 0 in %s', [Self.ClassName]);
  if NudeCraftMultiplier <= 0 then
    raise EDataValidationError.CreateFmt('NudeCraftMultiplier <= 0 in %s', [Self.ClassName]);
  if SleepCraftMultiplier <= 0 then
    raise EDataValidationError.CreateFmt('SleepCraftMultiplier <= 0 in %s', [Self.ClassName]);
end;

procedure TMarkCraftFromStaminaData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  SleepHitSound := Element.AttributeString('SleepHitSound');
  StaminaDamage := Element.AttributeSingle('StaminaDamage');
  NudeStaminaDamage := Element.AttributeSingle('NudeStaminaDamage');
  SleepStaminaDamage := Element.AttributeSingle('SleepStaminaDamage');
  UnconsciousStaminaThreshold := Element.AttributeSingle('UnconsciousStaminaThreshold');
  CraftMultiplier := Element.AttributeSingle('CraftMultiplier');
  NudeCraftMultiplier := Element.AttributeSingle('NudeCraftMultiplier');
  SleepCraftMultiplier := Element.AttributeSingle('SleepCraftMultiplier');
end;

function TMarkCraftFromStaminaData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

function TMarkCraftFromStaminaData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will drain %.1n stamina (%.1n if target is nude) and turn it into a new item. Will not craft anything if target''s stamina falls below zero. And when stamina falls too low, will knock the target unconscious and retreat.', [StaminaDamage, NudeStaminaDamage]),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('If target is unsuspecting, will inflict %.1n stamina damage and retreat, crafted item has a chance to be of higher quality.', [SleepStaminaDamage]),
    Classname + '_sleep', 1));
end;

function TMarkCraftFromStaminaData.Mark: TMarkClass;
begin
  Exit(TMarkCraftFromStamina);
end;

initialization
  RegisterSerializableObject(TMarkCraftFromStamina);
  RegisterSerializableData(TMarkCraftFromStaminaData);

end.


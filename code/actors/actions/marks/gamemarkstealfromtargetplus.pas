{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkStealFromTargetPlus;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  GameMarkStealFromTarget, GameMarkAbstract,
  GameUnlockableEntry;

type
  TMarkStealFromTargetPlus = class(TMarkStealFromTarget)
  strict private
    procedure TeleportToSafety;
  protected
    procedure Perform; override;
  end;

  TMarkStealFromTargetPlusData = class(TMarkStealFromTargetData)
  public
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors,
  GameMap, GameMapTypes, GameActor,
  GameMonster, GamePlayerCharacter, GameActionIdle, GameStats,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkStealFromTargetPlusData}
{$INCLUDE marktypecasts.inc}

procedure TMarkStealFromTargetPlus.TeleportToSafety;
var
  TX, TY: Int16;
  DistMap: TDistanceMapArray;
  MaxDistance: TDistanceQuant;
  P: TPlayerCharacter;
begin
  LocalStats.IncStat(Data.ClassName);
  ParentActor.Particle('TELEPORT', ColorParticleItemStolen);
  DistMap := Map.DistanceMap(Target, true);
  MaxDistance := Map.MaxDistance(DistMap);
  repeat
    TX := Rnd.Random(Map.SizeX);
    TY := Rnd.Random(Map.SizeY);
  until Map.PassableTiles[ParentActor.PredSize][TX + Map.SizeX * TY] and (DistMap[TX + Map.SizeX * TY] > MaxDistance div 2);
  DistMap := nil;
  ShowLog('%s teleports away', [ParentActor.Data.DisplayName], ColorLogItemSteal);
  ParentActor.MoveMeTo(TX, TY);
  // TODO: more generic
  for P in Map.CharactersOnThisLevel do
    if P.CanAct and (P.GetActionTarget = Parent) then
    begin
      P.CurrentAction := TActionIdle.NewAction(P);
      P.CurrentAction.Start;
    end;
end;

procedure TMarkStealFromTargetPlus.Perform;
begin
  inherited;
  if ParentMonster.Ai.AiFlee then
  begin
    //ParentMonster.AiFlee := false; // otherwise the action doesn't reset to idle :) TODO
    TeleportToSafety;
  end;
end;

{ TMarkStealFromTargetPlusData ----------------------------- }

function TMarkStealFromTargetPlusData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('Will teleport away after successfully snatching an item or few.', []),
    Classname, 1));
end;

function TMarkStealFromTargetPlusData.Mark: TMarkClass;
begin
  Exit(TMarkStealFromTargetPlus);
end;

initialization
  RegisterSerializableObject(TMarkStealFromTargetPlus);
  RegisterSerializableData(TMarkStealFromTargetPlusData);

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkMedic;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract, GameApparelSlots,
  GameUnlockableEntry;

type
  TMarkMedic = class(TMarkTargetAbstract)
  strict private
    const InteractionSlots = [esFeet, esBottomUnder, esBottomOver, esTopUnder, esTopOver, esTopOverOver, esAmulet];
  strict private
    function TryReinforceBondage(const AItemName: String): Boolean;
    procedure InteractWithSleepingTarget;
  protected
    procedure Perform; override;
  end;

  TMarkMedicData = class(TMarkAbstractData)
  public const
    StaminaBlackhole = -200;
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HealthDamageOver: Single;
    HealthDamageOverOver: Single;
    StaminaDamageOverOver: Single;
    HealthDamageTopUnder: Single;
    WillpowerDamageTopUnder: Single;
    WillpowerDamageBottomUnder: Single;
    HealthDamageFeet: Single;
    StaminaDamageFeet: Single;
    WillpowerDamageFeet: Single;
    HealthDamageNeck: Single;
    StaminaDamageNeck: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameActor, GameMath, GameEnumUtils,
  GameViewGame, GameSounds, GameItemData, GameItemsDatabase,
  GameInventoryItem, GamePlayerCharacter, GameMonster, GameStats,
  GameActionPlayerUnconscious,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkMedicData}
{$INCLUDE marktypecasts.inc}

function TMarkMedic.TryReinforceBondage(const AItemName: String): Boolean;
var
  A: TApparelSlot;
begin
  if TargetPlayer.Inventory.ShouldMaximizeDurability(AItemName) then
  begin
    ShowLog('%s makes sure %s is locked tight and secure', [ParentActor.Data.DisplayName, ItemsDataDictionary[AItemName].DisplayName], ColorLogTickleBondage);
    TargetPlayer.Inventory.ReinforceItem(ItemsDataDictionary[AItemName].MainSlot);
    Sound((ItemsDataDictionary[AItemName] as TItemData).SoundEquip);
    Exit(true);
  end else
  begin
    for A in ItemsDataDictionary[AItemName].EquipSlots do
      if TargetPlayer.Inventory.Apparel[A] <> nil then
        if TargetPlayer.Inventory.Equipped[A].Data.Id = 'medical_rack_full' then
          Exit(false) // this is a workaround, and I really don't like it
        else
        if TargetPlayer.Inventory.Equipped[A].Data.Id = AItemName then
          Exit(false) // this is a workaround, and I really don't like it
        else
        if TargetPlayer.Inventory.Bondage[A] then // "cannot be dropped" todo
          TargetPlayer.Inventory.DisintegrateOrDrop(A, false)
        else
          TargetPlayer.Inventory.DisintegrateOrDrop(A, true); // this shouldn't happen?
    Exit(false);
  end;
end;

procedure TMarkMedic.InteractWithSleepingTarget;
var
  E: TApparelSlot;
  MedicalRack: TItemData;
  SomethingUnequipped: Boolean;
begin
  ShowLog('One tiny drop of anesthetic on %s''s nose makes sure she won''t make any trouble while getting prepared', [TargetPlayer.Data.DisplayName], ColorLogTwinkleStrip);
  MedicalRack := ItemsDataDictionary['medical_rack_full'] as TItemData;

  LocalStats.IncStat('TMarkMedic_sleeping');
  // If character has medical rack already equipped
  if (TargetPlayer.Inventory.Apparel[MedicalRack.MainSlot] <> nil) and (TargetPlayer.Inventory.Equipped[MedicalRack.MainSlot].Data = MedicalRack) then
  begin
    // Note: here we don't unequip anything, so virtually the character may have had underwear on
    // However, as equipping medical rack always paired with complete stripping
    // And medical rack prevents from equipping anything (using hands)
    // The only additional items here may be applied bondage by yellow slime
    ShowLog('%s is already properly rigged up for a medical checkup', [TargetPlayer.Data.DisplayName], ColorLogTickleBondage);
    ShowLog('%s only briefly makes sure that no locks or straps on %s are loose', [ParentMonster.Data.DisplayName, TargetPlayer.Inventory.Equipped[MedicalRack.MainSlot].Data.DisplayName], ColorLogTickleBondage);
    TargetPlayer.Inventory.ReinforceItem(MedicalRack.MainSlot);
    Exit;
  end;

  SomethingUnequipped := false;
  // Unequip clothes that cover something
  for E in TargetPlayer.Blueprint.EquipmentSlots do
    if (TargetPlayer.Inventory.Apparel[E] <> nil) and (TargetPlayer.Inventory.Equipped[E].ItemData.CoversTop or TargetPlayer.Inventory.Equipped[E].ItemData.CoversBottom) then
    begin
      TargetPlayer.Inventory.DisintegrateOrDrop(E, false);
      SomethingUnequipped := true;
    end;
  // Free up space for medical rack (will also remove bondage items, including bars)
  for E in MedicalRack.EquipSlots do
    if (TargetPlayer.Inventory.Apparel[E] <> nil) and (TargetPlayer.Inventory.Equipped[E].Data <> MedicalRack) then
    begin
      SomethingUnequipped := true;
      TargetPlayer.Inventory.DisintegrateOrDrop(E, false);
    end;

  TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(MedicalRack));
  Sound(MedicalRack.SoundEquip);
  if SomethingUnequipped then
    ShowLog('Stripped stark naked %s feels something''s wrong but is unable to wake up', [TargetPlayer.Data.DisplayName], ColorLogTwinkleStrip);
  ShowLog('A complicated portable rack is tedious to properly set up and tighten all the straps', [], ColorLogTickleBondage);
  ShowLog('But %s is not capable of interfering and %s takes its time to make sure she''s all locked up', [TargetPlayer.Data.DisplayName, ParentActor.Data.DisplayName], ColorLogTickleBondage);
end;

procedure TMarkMedic.Perform;
var
  EquipSlot: TApparelSlot;

  procedure DisarmAndRestrainHands;
  begin
    LocalStats.IncStat('TMarkMedic_restrain');
    if TargetPlayer.Inventory.Apparel[esWeapon] = nil then
    begin
      // if hands are free - apply wrists bar
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['medical_rack_hands']));
      ShowLog('%s swiftly locks %s around %s''s wrists', [ParentMonster.Data.DisplayName, TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogTickleBondage);
      TargetPlayer.Particle('BOUND', ColorParticleTickleBondage);
      Sound(TargetPlayer.Inventory.Equipped[esWeapon].ItemData.SoundEquip);
    end else
    if TargetPlayer.Inventory.Bondage[esWeapon] then // but player character can use hands
    begin
      TargetPlayer.Inventory.DisintegrateOrDrop(esWeapon, false);
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['medical_rack_hands']));
      ShowLog('While %s''s hands are already restrained, %s decides that %s will be more convenient to work with', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName, TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName], ColorLogTickleBondage);
      TargetPlayer.Particle('BOUND', ColorParticleTickleBondage);
      Sound(TargetPlayer.Inventory.Equipped[esWeapon].ItemData.SoundEquip);
    end else
    begin
      ShowLog('Effortlessly catching %s''s wrist %s taps a painful pressure point and makes her drop %s', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName, TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName], ColorLogTickleStrip);
      UnequipAndDrop(esWeapon, true);
      TargetPlayer.Particle('DISARM', ColorParticleTickleStrip);
    end;
  end;

  function RestrainTarget: Boolean;
  begin
    case EquipSlot of
      esFeet:
        begin
          if TryReinforceBondage('medical_rack_feet') then
            Exit(true);
          if TargetPlayer.Inventory.Apparel[EquipSlot] = nil then
          begin
            ShowLog('With her hands immobilized %s cannot prevent her feet from following', [TargetPlayer.Data.DisplayName], ColorLogTickleBondage);
            TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['medical_rack_feet']));
            TargetPlayer.Particle('BOUND', ColorParticleTickleBondage);
            Sound(TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.SoundEquip);
            Exit(true);
          end;
        end;
      esBottomOver:
        begin
          if TryReinforceBondage('medical_rack_legs') then
            Exit(true);
          if TargetPlayer.Inventory.Apparel[EquipSlot] = nil then
          begin
            ShowLog('Holding %s''s hands firmly %s easily fastens straps around her shins', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogTickleBondage);
            TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['medical_rack_legs']));
            TargetPlayer.Particle('BOUND', ColorParticleTickleBondage);
            Sound(TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.SoundEquip);
            Exit(true);
          end;
        end;
      esTopOver,esTopOverOver:
        begin
          if TryReinforceBondage('medical_rack_shoulders') then
            Exit(true);
          if TargetPlayer.Inventory.Apparel[EquipSlot] = nil then
          begin
            ShowLog('With %s''s hands out of the way %s link-locks her shoulders with a bar meeting little resistance', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogTickleBondage);
            TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['medical_rack_shoulders']));
            TargetPlayer.Particle('BOUND', ColorParticleTickleBondage);
            Sound(TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.SoundEquip);
            Exit(true);
          end;
        end;
      else
      begin
        // do nothing
      end;
    end;
    Exit(false);
  end;

  procedure CureTarget;
  begin
    LocalStats.IncStat('TMarkMedic_cure');
    TargetPlayer.MaxHealth += 1; // we guarantee that MaxHealth + 1 <= Data.MaxHealth
    TargetPlayer.Particle('CURED', ColorParticleTickleBondage);
    ViewGame.WakeUp(true, true);
    case EquipSlot of
      esTopOver, esBottomOver:
        begin
          ShowLog('%s shrieks in pain as %s sprays antiseptic on her scratches', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogTickle);
          TargetPlayer.Health -= MarkData.HealthDamageOver;
          TargetPlayer.PlayGruntSound;
        end;
      esTopOverOver:
        begin
          ShowLog('%s gasps and chokes as having forced her mouth wide open %s applies oinment inside her throat', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogTickle);
          TargetPlayer.Health -= MarkData.HealthDamageOverOver;
          TargetPlayer.HitStamina(MarkData.StaminaDamageOverOver);
          TargetPlayer.PlayGruntSound;
        end;
      esTopUnder:
        begin
          ShowLog('%s helplessly blushes as %s oils and massages her breasts', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogTickle);
          TargetPlayer.Health -= MarkData.HealthDamageTopUnder;
          TargetPlayer.HitWill(MarkData.WillpowerDamageTopUnder);
          TargetPlayer.PlaySurpriseSound;
        end;
      esBottomUnder:
        begin
          ShowLog('%s squeaks from embarassment as %s applies some salve to inside of her nethers', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogTickle);
          TargetPlayer.HitWill(MarkData.WillpowerDamageBottomUnder);
          TargetPlayer.PlaySurpriseSound;
        end;
      esFeet:
        begin
          ShowLog('%s involuntarily giggles as %s acupunctures her soles', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogTickle);
          TargetPlayer.Health -= MarkData.HealthDamageFeet;
          TargetPlayer.HitStamina(MarkData.StaminaDamageFeet);
          TargetPlayer.HitWill(MarkData.WillpowerDamageFeet);
          TargetPlayer.PlayGiggleSound;
        end;
      esAmulet{esNeck}:
        begin
          ShowLog('%s groans in pain as %s pulls her neck up until it crackles', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogTickle);
          TargetPlayer.Health -= MarkData.HealthDamageNeck;
          TargetPlayer.DegradeStamina(MarkData.StaminaDamageNeck);
          TargetPlayer.PlayGruntSound;
        end;
      else
        raise Exception.CreateFmt('Unexpected equipment slot: %s', [specialize EnumToStr<TApparelSlot>(EquipSlot)]);
        //esWeapon - impossible here
    end;
  end;

  procedure UndressBeforeVoluntaryCheckup;
  var
    Count: Integer;
    Disarmed: Boolean;
  begin
    LocalStats.IncStat('TMarkMedic_voluntary');
    if (TargetPlayer.Inventory.Apparel[esWeapon] <> nil) and not TargetPlayer.Inventory.Bondage[esWeapon] then
    begin
      Disarmed := true;
      TargetPlayer.Inventory.UnequipAndDrop(esWeapon, true);
    end else
      Disarmed := false;
    Count := 0;
    for EquipSlot in TargetPlayer.Blueprint.ClothesSlots do
      if (TargetPlayer.Inventory.Apparel[EquipSlot] <> nil) and
         (not TargetPlayer.Inventory.Bondage[EquipSlot] or TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.CoversTop or
          TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.CoversBottom) then
          begin
            TargetPlayer.Inventory.UnequipAndDrop(EquipSlot, true);
            Inc(Count);
          end;
    if Disarmed and (Count > 0) then
      ShowLog('Before %s can react, she is disarmed and undressed for the checkup', [TargetActor.Data.DisplayName], ColorParticleTickleStrip)
    else
    if Disarmed then
      ShowLog('Before %s can react, she is disarmed and stands ready for the checkup', [TargetActor.Data.DisplayName], ColorParticleTickleStrip)
    else
    if Count > 0 then
      ShowLog('Before %s can react, she is stripped naked for the checkup', [TargetActor.Data.DisplayName], ColorParticleTickleStrip);
    if Disarmed or (Count > 0) then
    begin
      TargetPlayer.Particle('STRIP', ColorParticlePlayerDisrobed);
      ViewGame.ShakeMap;
    end;
  end;

begin
  if TargetPlayer.Unsuspecting then
    InteractWithSleepingTarget;
  // and proceed as normal

  // if Player character doesn't have restrictive bondage on hands, try add some or disarm
  if TargetWasResisting and TargetPlayer.Inventory.CanUseHands then
    DisarmAndRestrainHands
  // if hands are properly immobilized - act further
  else
  begin
    if not TargetWasResisting then
      UndressBeforeVoluntaryCheckup
    else
      TryCatchLeash;

    if ((TargetPlayer.Inventory.Apparel[esWeapon] = nil) or (TargetPlayer.Inventory.Apparel[esFeet] = nil) or
       (TargetPlayer.Inventory.Apparel[esBottomOver] = nil) or (TargetPlayer.Inventory.Apparel[esTopOver] = nil))
       and not TargetWasResisting then
      ShowLog('As %s is not resisting, %s is not provoked to restrain her', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogInventory);

    // check if target needs "healing"
    if TargetPlayer.MaxHealth >= TargetPlayer.PlayerCharacterData.MaxHealth - 1 then
    begin
      ShowLog('%s quickly checks up on %s''s health and recedes satisfied', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogTickle);
      ParentMonster.Ai.AiFlee := true;
      TryReleaseLeash;
      Exit;
    end;
    // if target needs +1 maxHealth:

    repeat
      EquipSlot := TApparelSlot(Rnd.Random(Ord(High(TApparelSlot)) + 1));
    until EquipSlot in InteractionSlots;

    // if clothes equipped - strip
    if (TargetPlayer.Inventory.Apparel[EquipSlot] <> nil) and (not TargetPlayer.Inventory.Bondage[EquipSlot] or
      TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.CoversTop or TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.CoversBottom) then
    begin
      ShowLog('%s is in the way, so off it goes', [TargetPlayer.Inventory.Equipped[EquipSlot].Data.DisplayName], ColorLogTickleStrip);
      UnequipAndDrop(EquipSlot, true);
      TargetPlayer.Particle('STRIP', ColorParticleTickleStrip);
    end else
    // if slot is empty or bondage
    begin
      // restrain character or replace bondage items if they're weaker than expected (we guarantee it's a bondage item here)
      if TargetWasResisting and RestrainTarget then
        Exit;
      // If we didn't Exit yet - apply treatment
      CureTarget;
    end;
  end;

  if (TargetPlayer.Health <= 0) or (TargetPlayer.Will <= 0) then
  begin
    ShowLog('Acknowledging %s is on the verge of blacking out %s finally leaves her alone', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogInventory);
    if (TargetPlayer.Health <= 0) then
      TargetPlayer.Health := 1;
    if (TargetPlayer.Will <= 0) then
      TargetPlayer.Will := 1;
    if TargetWasResisting then
    begin
      ShowLog('But only after forcing a sleeping pill into her throat', [], ColorLogBondage);
      if TargetPlayer.Stamina > MarkData.StaminaBlackhole then
        TargetPlayer.Stamina := MarkData.StaminaBlackhole;
      TargetPlayer.CurrentAction := TActionPlayerUnconscious.NewAction(TargetPlayer);
      TargetPlayer.CurrentAction.Start;
      ViewGame.UnPauseGame;
    end;
    ParentMonster.Ai.AiFlee := true;
    TryReleaseLeash;
    Exit;
  end;

  if not TargetWasResisting then
    if (Rnd.Random < PowerSingle(1.0 - TargetPlayer.Health / TargetPlayer.MaxHealth, 6.0)) or  //1% chance at 50%, 53% chance at 90 %
       (Rnd.Random < PowerSingle(1.0 - TargetPlayer.Will / TargetPlayer.MaxWill, 6.0)) then
    begin
      ShowLog('Given %s''s good behavior %s decides to give her a break and pause the procedures', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogInventory);
      ParentMonster.Ai.AiFlee := true;
      TryReleaseLeash;
      Exit;
    end;
end;

{ TMarkMedicData ----------------------------- }

procedure TMarkMedicData.Validate;
begin
  inherited Validate;
end;

procedure TMarkMedicData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HealthDamageOver := Element.AttributeSingle('HealthDamageOver');
  HealthDamageOverOver := Element.AttributeSingle('HealthDamageOverOver');
  StaminaDamageOverOver := Element.AttributeSingle('StaminaDamageOverOver');
  HealthDamageTopUnder := Element.AttributeSingle('HealthDamageTopUnder');
  WillpowerDamageTopUnder := Element.AttributeSingle('WillpowerDamageTopUnder');
  WillpowerDamageBottomUnder := Element.AttributeSingle('WillpowerDamageBottomUnder');
  HealthDamageFeet := Element.AttributeSingle('HealthDamageFeet');
  StaminaDamageFeet := Element.AttributeSingle('StaminaDamageFeet');
  WillpowerDamageFeet := Element.AttributeSingle('WillpowerDamageFeet');
  HealthDamageNeck := Element.AttributeSingle('HealthDamageNeck');
  StaminaDamageNeck := Element.AttributeSingle('StaminaDamageNeck');
end;

function TMarkMedicData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: If the target is resisting will disarm the target, then proceed to undress and lock target in more strict restraints.', []),
    'TMarkMedic_restrain', 1));
  Result.Add(
    NewEntryText(
      Format('If the target''s hands are properly restrained will proceed to cure the target. Each attack will heal 1 max health, however, hurting other stats or current value of health.', []),
    'TMarkMedic_cure', 1));
  Result.Add(
    NewEntryText(
      Format('If the target is healthy will retreat. Unlocking the restraints is not provisioned.', []),
    'TMarkMedic_cure', 1));
  Result.Add(
    NewEntryText(
      Format('SLEEP ATTACK: A sleeping target will get restrained in an extremely robust and restraining medical rack. ', []),
    'TMarkMedic_sleeping', 1));
  Result.Add(
    NewEntryText(
      Format('VOLUNTARY TREATMENT: If the target is not resisting (stands perfectly still not doing anything else) will not apply restraints, will however undress and disarm target if anything is equipped.', []),
    'TMarkMedic_voluntary', 1));
end;

function TMarkMedicData.Mark: TMarkClass;
begin
  Exit(TMarkMedic);
end;

initialization
  RegisterSerializableObject(TMarkMedic);
  RegisterSerializableData(TMarkMedicData);

end.


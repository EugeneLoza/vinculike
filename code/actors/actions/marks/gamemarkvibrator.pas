{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ a mark thrown by equipped bottom vibrator item }
unit GameMarkVibrator;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract,
  GameUnlockableEntry;

type
  TMarkVibrator = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkVibratorData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
    function CanBeIndependent: Boolean; override;
  public
    HitSound: String;
    Damage: Single;
    Stun: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameStats, GameSounds, GamePlayerCharacter, GameLog, GameColors,
  GameActionPlayerStunned, GameViewGame, GameRandom,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkVibratorData}
{$INCLUDE marktypecasts.inc}

procedure TMarkVibrator.Perform;
begin
  LocalStats.IncStat(Data.ClassName);
  ShowLog('Suddenly the vibrator goes crazy, dragging %s to the edge', [TargetPlayer.Data.DisplayName], ColorLogStaminaDamage);
  TargetPlayer.HitWill(MarkData.Damage * TargetPlayer.Will);
  ViewGame.WakeUp(true, false);
  if (Rnd.Random < TargetPlayer.Will / TargetPlayer.PlayerCharacterData.MaxWill) or (Rnd.Random < TargetPlayer.ResistStun) then
    ShowLog('She barely manages to endure and not get too distracted', [], ColorLogBondageItemSave)
  else
  begin
    TargetPlayer.CurrentAction := TActionPlayerStunned.NewAction(TargetPlayer, MarkData.Stun);
    TargetPlayer.CurrentAction.Start;
    TargetPlayer.AddResistStun;
    TargetPlayer.Particle('STUN', ColorParticlePlayerStunned);
    TargetPlayer.PlayGruntSound;
    ShowLog('And then suddenly stops, leaving her barely able to think or act (paralyzed %.1n s)', [MarkData.Stun], ColorLogStaminaDamage);
  end;
  Sound(MarkData.HitSound);
end;

{ TMarkVibratorData ----------------------------- }

procedure TMarkVibratorData.Validate;
begin
  inherited Validate;
  if Damage <= 0 then
    raise EDataValidationError.CreateFmt('Damage %n <= 0 in %s', [Damage, Self.ClassName]);
  if Stun <= 0 then
    raise EDataValidationError.CreateFmt('Stun %n <= 0 in %s', [Damage, Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TMarkVibratorData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  Damage := Element.AttributeSingle('Damage');
  Stun := Element.AttributeSingle('Stun');
end;

function TMarkVibratorData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

function TMarkVibratorData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('Turns on at full throttle and brings character to an edge', [Damage]),
    Classname, 1));
end;

function TMarkVibratorData.Mark: TMarkClass;
begin
  Exit(TMarkVibrator);
end;

initialization
  RegisterSerializableObject(TMarkVibrator);
  RegisterSerializableData(TMarkVibratorData);

end.


{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkDamageInventoryTrap;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameApparelSlots, GameMarkAbstract,
  GameUnlockableEntry;

type
  TMarkDamageInventoryTrap = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkDamageInventoryTrapData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public const
  public
    HitSound: String;
    AttackSlots: TApparelSlotsList;
    ItemDamageFraction: Single;
    StaminaDamage: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameSounds, GameStats, GameMath, GameEnumUtils,
  GameViewGame, GamePlayerCharacter,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkDamageInventoryTrapData}
{$INCLUDE marktypecasts.inc}

procedure TMarkDamageInventoryTrap.Perform;
var
  E: TApparelSlot;
  ItemDamage: Single;
  Count: Integer;
begin
  LocalStats.IncStat(Data.ClassName);
  Count := 0;
  Sound(MarkData.HitSound);
  for E in MarkData.AttackSlots do
    if (TargetPlayer.Inventory.Apparel[E] <> nil) and (TargetPlayer.Inventory.Equipped[E].ItemData.MainSlot = E) then // Bondage items included!
    begin
      Inc(Count);
      ItemDamage := MaxSingle(TargetPlayer.Inventory.Equipped[E].Durability * MarkData.ItemDamageFraction, 1);
      ShowLog('%s is damaged by %.1n', [TargetPlayer.Inventory.Equipped[E].Data.DisplayName, ItemDamage], ColorLogAbsorbDamage);
      TargetPlayer.Inventory.DamageItem(E, ItemDamage);
    end;
  if Count > 0 then
  begin
    ViewGame.ShakeCharacter;
    ShowLog('Thin belts of sandpaper slide along %s''s items damaging them', [TargetPlayer.Data.DisplayName], ColorLogAbsorbDamage);
  end else
  begin
    ShowLog('Thin bands of sandpaper wrap around %s and she struggles out', [TargetPlayer.Data.DisplayName], ColorLogStaminaDamage);
    TargetPlayer.HitStamina(MarkData.StaminaDamage);
  end;
  ViewGame.WakeUp(true, true);
end;

{ TMarkDamageInventoryTrapData ----------------------------- }

procedure TMarkDamageInventoryTrapData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TMarkDamageInventoryTrapData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  AttackSlots := specialize StrToEnumsList<TApparelSlot>(Element.AttributeString('AttackSlots'));
  ItemDamageFraction := Element.AttributeSingle('ItemDamageFraction');
  StaminaDamage := Element.AttributeSingle('StaminaDamage');
end;

function TMarkDamageInventoryTrapData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('EFFECT: Damages most large equipped items for %d%% of their durability. Nude target will suffer %.1n stamina damage without any other harm.', [Round(100 * ItemDamageFraction), StaminaDamage]),
    Classname, 1));
end;

function TMarkDamageInventoryTrapData.Mark: TMarkClass;
begin
  Exit(TMarkDamageInventoryTrap);
end;

destructor TMarkDamageInventoryTrapData.Destroy;
begin
  FreeAndNil(AttackSlots);
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TMarkDamageInventoryTrap);
  RegisterSerializableData(TMarkDamageInventoryTrapData);

end.


{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Does damaged and transfers fraction of it to the nearest monster }
unit GameProjectileVampiric;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  CastleUtils,
  GameProjectileAbstract, GameMarkAbstract, GameActor,
  GameUnlockableEntry;

type
  TProjectileVampiric = class(TProjectileAbstract)
  public
    procedure HitWall; override;
    procedure HitTarget(const TargetsHit: array of TActor); override;
  end;

  TProjectileVampiricData = class(TProjectileAbstractData)
  protected
    function CanBeIndependent: Boolean; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Damage: Single;
    HitSound: String;
    HealingMultiplier: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GamePlayerCharacter,
  GameSounds,
  GameLog, GameColors, GameStats, GameMonster, {GameMap,}
  GameVinculopediaEntryText;

{$IFDEF SafeTypecast}
{$DEFINE MarkData:=(Data as TProjectileVampiricData)}
{$ELSE}
{$DEFINE MarkData:=TProjectileVampiricData(Data)}
{$ENDIF}

procedure TProjectileVampiric.HitWall;
begin
  //todo: spawn explosion
end;

procedure TProjectileVampiric.HitTarget(const TargetsHit: array of TActor);
var
  TargetPlayer: TPlayerCharacter;
  OldHealth, MonsterHealing: Single;
  M: TMonster;
begin
  LocalStats.IncStat('TProjectileVampiric');
  TargetPlayer := TargetsHit[0] as TPlayerCharacter;
  OldHealth := TargetPlayer.Health;
  TargetPlayer.Hit(MarkData.Damage); // will wake up

  MonsterHealing := MarkData.HealingMultiplier * (OldHealth - TargetPlayer.Health);
  M := TMonster(GetNearestMonster);
  if (M <> nil) and (MonsterHealing > 0.1) then
  begin
    if M.IsVisible then // Map.Ray(0, TargetPlayer.CenterX, TargetPlayer.CenterY, M.CenterX, M.CenterY)
      ShowLog('%.1n health points transferred to %s', [MonsterHealing, M.Data.DisplayName], ColorLogMapMonster);
    M.Health += MonsterHealing;
  end;
end;

{ TProjectileVampiricData ----------------------------- }

function TProjectileVampiricData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

procedure TProjectileVampiricData.Validate;
begin
  inherited Validate;
  if Damage <= 0 then
    raise EDataValidationError.CreateFmt('Damage <= 0 in %s', [Self.ClassName]);
  if HealingMultiplier <= 0 then
    raise EDataValidationError.CreateFmt('HealingMultiplier <= 0 in %s', [Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TProjectileVampiricData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  Damage := Element.AttributeSingle('Damage');
  HealingMultiplier := Element.AttributeSingle('HealingMultiplier');
  HitSound := Element.AttributeString('HitSound');
end;

function TProjectileVampiricData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will hit the target for %.1n and transfer damage dealt to body to the nearest monster.', [Damage]),
    'TProjectileVampiric', 1));
end;

function TProjectileVampiricData.Mark: TMarkClass;
begin
  Exit(TProjectileVampiric);
end;

initialization
  RegisterSerializableObject(TProjectileVampiric);
  RegisterSerializableData(TProjectileVampiricData);

end.


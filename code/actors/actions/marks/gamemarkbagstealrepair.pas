{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkBagStealRepair;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract, GameItemData,
  GameUnlockableEntry;

type
  TMarkBagStealRepair = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkBagStealRepairData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
    function CanBeIndependent: Boolean; override;
  public
    HitSound: String;
    ZapSound: String;
    RepairToHealthMultiplier: Single;
    StunDuration: Single;
    StunStaminaDamage: Single;
    BlindfoldItem, MittensItem: TItemData;
    RuneOfObedience: TItemData;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameSounds,
  GameApparelSlots,
  GameItemsDatabase, GameInventoryItem,
  GamePlayerCharacter, GameActionPlayerStunned, GameMonster,
  GameStats, GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkBagStealRepairData}
{$INCLUDE marktypecasts.inc}

procedure TMarkBagStealRepair.Perform;
var
  ItemStolen: TInventoryItem;
  OldItemDurability, HealthBuff: Single;
  E: TApparelSlot;

  procedure RepairItemAndHeal;
  begin
    Sound(MarkData.HitSound);
    ItemStolen.MaxDurability := (ItemStolen.MaxDurability + ItemStolen.ItemData.Durability) / 2.0;
    OldItemDurability := ItemStolen.Durability;
    ItemStolen.Durability := (ItemStolen.Durability + ItemStolen.MaxDurability) / 2.0;
    HealthBuff := (ItemStolen.Durability - OldItemDurability) * MarkData.RepairToHealthMultiplier;
    ParentMonster.Health += HealthBuff;
    ParentMonster.Loot.Add(ItemStolen);
    ShowLog('Quickly repairs it and gains %.1n health points as a reward for the minigame.', [HealthBuff], ColorLogItemSteal)
  end;

begin
  LocalStats.IncStat(Data.ClassName);
  if TargetPlayer.Unsuspecting then
  begin
    if TargetPlayer.Inventory.EquippedSlotsRemovable > 0 then
    begin
      ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem, false);
      Sound(ItemStolen.ItemData.SoundUnequip);
      ShowLog('Making sure not to wake %s up %s snatches %s', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName, ItemStolen.Data.DisplayName], ColorLogItemSteal);
      RepairItemAndHeal;
    end else
      ShowLog('With nothing left to steal %s retreats', [ParentMonster.Data.DisplayName], ColorDefault);
    ParentMonster.Ai.AiFlee := true;
  end else
  if not TargetWasResisting then
  begin
    LocalStats.IncStat(Data.ClassName + '_voluntary');
    if TargetPlayer.Inventory.EquippedSlotsRemovable > 0 then
    begin
      //steal item
      if (TargetPlayer.Inventory.Equipped[esWeapon] <> nil) and (not TargetPlayer.Inventory.Bondage[esWeapon]) then
        E := esWeapon
      else
        E := TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem;
      ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(E, true);
      Sound(ItemStolen.ItemData.SoundUnequip);
      ShowLog('As %s is not resisting, %s takes her %s without asking', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName, ItemStolen.Data.DisplayName], ColorLogItemSteal);
      RepairItemAndHeal;
    end else
    begin
      if TargetPlayer.Inventory.Equipped[MarkData.RuneOfObedience.MainSlot] <> nil then
        ShowLog('Satisfied with %s''s inventory state %s retreats', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogItemSteal)
      else begin
        ShowLog('%s feels oddly tame and obedient', [TargetPlayer.Data.DisplayName], ColorLogBondage);
        TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(MarkData.RuneOfObedience));
        TargetPlayer.Inventory.ReinforceItem(MarkData.RuneOfObedience.MainSlot);
      end;
      ParentMonster.Ai.AiFlee := true;
    end;
  end else
  if TargetPlayer.Blindfolded and not TargetPlayer.Inventory.CanUseHands then
  begin
    if TargetPlayer.Inventory.EquippedSlotsRemovable > 0 then
    begin
      //steal item
      ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem, true);
      Sound(ItemStolen.ItemData.SoundUnequip);
      ShowLog('Taking advantage of disarmed and disoriented %s, %s steals her %s', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName, ItemStolen.Data.DisplayName], ColorLogItemSteal);
      RepairItemAndHeal;
    end else
    begin
      Sound(MarkData.ZapSound);
      TargetPlayer.PlayHurtSound;
      ShowLog('%s stuns %s with electric shock to flee safely (%.1n seconds)', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName, MarkData.StunDuration], ColorLogStaminaDamage);
      TargetPlayer.HitStamina(MarkData.StunStaminaDamage);
      TargetPlayer.PlayHurtSound;
      // cannot be resisted
      TargetPlayer.CurrentAction := TActionPlayerStunned.NewAction(TargetPlayer, MarkData.StunDuration);
      TargetPlayer.CurrentAction.Start;
      TargetPlayer.Particle('STUN', ColorParticleTickle);
      ParentMonster.Ai.AiFlee := true;
    end;
  end else
  if not TargetPlayer.Blindfolded then
  begin
    if TargetPlayer.Inventory.Equipped[esGlasses] <> nil then
    begin
      //steal glasses
      ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(esGlasses, true);
      Sound(ItemStolen.ItemData.SoundUnequip);
      ShowLog('%s impudently grabs %s from %s''s %s', [ParentMonster.Data.DisplayName, ItemStolen.Data.DisplayName, TargetPlayer.Data.DisplayName, EquipSlotsToHumanReadableString(ItemStolen.Data.EquipSlots)], ColorLogItemSteal);
      RepairItemAndHeal;
    end else
    begin
      //equip blindfold
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(MarkData.BlindfoldItem));
      Sound(MarkData.BlindfoldItem.SoundEquip);
      TargetPlayer.PlaySurpriseSound;
      ShowLog('%s suddenly throws and secures %s on %s''s head', [ParentMonster.Data.DisplayName, MarkData.BlindfoldItem.DisplayName, TargetPlayer.Data.DisplayName], ColorLogBondage)
    end;
  end else
  begin
    if TargetPlayer.Inventory.Equipped[esWeapon] <> nil then
    begin
      //steal weapon
      ItemStolen := TargetPlayer.Inventory.UnequipAndReturn(esWeapon, true);
      Sound(ItemStolen.ItemData.SoundUnequip);
      ShowLog('%s easily steals %s from disoriented %s', [ParentMonster.Data.DisplayName, ItemStolen.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogItemSteal);
      RepairItemAndHeal;
    end else
    begin
      //equip mittens
      Sound(MarkData.MittensItem.SoundEquip);
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(MarkData.MittensItem));
      TargetPlayer.PlaySurpriseSound;
      ShowLog('%s catches %s''s hands and wraps them in %s', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName, MarkData.MittensItem.DisplayName], ColorLogBondage)
    end;
  end;
end;

{ TMarkBagStealRepairData ----------------------------- }

procedure TMarkBagStealRepairData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if not SoundExists(ZapSound) then
    raise EDataValidationError.CreateFmt('Invalid ZapSound = "%s" in %s', [ZapSound, Self.ClassName]);
  if RepairToHealthMultiplier <= 0 then
    raise EDataValidationError.CreateFmt('RepairToHealthMultiplier <= 0 in %s', [Self.ClassName]);
  if StunDuration <= 0 then
    raise EDataValidationError.CreateFmt('StunDuration <= 0 in %s', [Self.ClassName]);
  if StunStaminaDamage <= 0 then
    raise EDataValidationError.CreateFmt('StunStaminaDamage <= 0 in %s', [Self.ClassName]);
end;

procedure TMarkBagStealRepairData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  ZapSound := Element.AttributeString('ZapSound');
  RepairToHealthMultiplier := Element.AttributeSingle('RepairToHealthMultiplier');
  StunDuration := Element.AttributeSingle('StunDuration');
  StunStaminaDamage := Element.AttributeSingle('StunStaminaDamage');

  BlindfoldItem := ItemsDataDictionary[Element.AttributeString('BlindfoldItem')] as TItemData;
  MittensItem := ItemsDataDictionary[Element.AttributeString('MittensItem')] as TItemData;
  RuneOfObedience := ItemsDataDictionary[Element.AttributeString('RuneOfObedience')] as TItemData;
end;

function TMarkBagStealRepairData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

function TMarkBagStealRepairData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will first try to steal glasses, then will blindfold the target. Next will try to steal weapon, after which will equip mittens on hands. After target is blindfolded and disarmed, will steal all non-bondage items. Finally, when there is nothing left to steal, will stun the target and flee.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('Every stolen item will get a quality repair. Monster will receive a significant health bonus based on how much the item was repaired - higher bonus for more worn off items.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('Will not restrain a heroine if she is not resisting. But will put a Rune of Obedience on her for a short time to prevent from backstabbing.', []),
    Classname + '_voluntary', 1));
end;

function TMarkBagStealRepairData.Mark: TMarkClass;
begin
  Exit(TMarkBagStealRepair);
end;

initialization
  RegisterSerializableObject(TMarkBagStealRepair);
  RegisterSerializableData(TMarkBagStealRepairData);

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkHurtTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract, GameActor,
  GameUnlockableEntry;

type
  TMarkHurtTarget = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkHurtTargetData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    Damage: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameStats, GameSounds, GameLog, GameColors,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkHurtTargetData}
{$INCLUDE marktypecasts.inc}

procedure TMarkHurtTarget.Perform;
begin
  LocalStats.IncStat(Data.ClassName);
  TargetActor.Hit(MarkData.Damage);
  Sound(MarkData.HitSound);
  ParentActor.DamageWeapon;
end;

{ TMarkHurtTargetData ----------------------------- }

procedure TMarkHurtTargetData.Validate;
begin
  inherited Validate;
  if Damage <= 0 then
    raise EDataValidationError.CreateFmt('Damage %n <= 0 in %s', [Damage, Self.ClassName]);
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
end;

procedure TMarkHurtTargetData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  Damage := Element.AttributeSingle('Damage');
end;

function TMarkHurtTargetData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will hurt the target for %.1n damage.', [Damage]),
    Classname, 1));
end;

function TMarkHurtTargetData.Mark: TMarkClass;
begin
  Exit(TMarkHurtTarget);
end;

initialization
  RegisterSerializableObject(TMarkHurtTarget);
  RegisterSerializableData(TMarkHurtTargetData);

end.


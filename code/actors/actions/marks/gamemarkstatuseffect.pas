{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkStatusEffect;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract, GameStatusEffect,
  GameUnlockableEntry;

type
  TMarkStatusEffect = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkStatusEffectData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    StatusEffects: TStatusEffectsList;
    MinCount, MaxCount: Integer;
    SleepAttackMultiplier: Integer;
    Damage: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameStats, GameSounds, GameLog, GameColors, GameRandom,
  GamePlayerCharacter, GameActor, GameMonster,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkStatusEffectData}
{$INCLUDE marktypecasts.inc}

procedure TMarkStatusEffect.Perform;
var
  SEToChooseFrom, SEChosen: TStatusEffectsList;
  I, K, Count, Mult: Integer;
begin
  SEToChooseFrom := TStatusEffectsList.Create(false);
  for I := 0 to Pred(MarkData.StatusEffects.Count) do
    SEToChooseFrom.Add(MarkData.StatusEffects[I]);
  SEChosen := TStatusEffectsList.Create(false);
  Count := MarkData.MinCount + Rnd.Random(MarkData.MaxCount - MarkData.MinCount + 1);
  for I := 0 to Pred(Count) do
  begin
    K := Rnd.Random(SEToChooseFrom.Count);
    SEChosen.Add(SEToChooseFrom[K]);
    SEToChooseFrom.Delete(K);
  end;
  FreeAndNil(SEToChooseFrom);
  for I := 0 to Pred(SEChosen.Count) do
  begin
    if TargetActor.Unsuspecting then
      Mult := MarkData.SleepAttackMultiplier
    else
      Mult := 1;
    // will stack the durability
    for K := 1 to Mult do
      TargetPlayer.Inventory.AddStatusEffectNoStack(SEChosen[I].Clone);
  end;
  FreeAndNil(SEChosen);
  if TargetActor.Unsuspecting then
  begin
    LocalStats.IncStat(Data.ClassName + '_sleep');
    ShowLog('%s dreams of being bitten by a mosquito', [TargetActor.Data.DisplayName], ColorLogStaminaDamage);
    ParentMonster.Ai.AiFlee := true;
  end else
  begin
    LocalStats.IncStat(Data.ClassName);
    if TargetPlayer.Inventory.PartiallyOrFullyNude then
      ShowLog('%s pierces %s''s skin with a tiny needle', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogStaminaDamage)
    else
      ShowLog('%s thrusts a tiny needle between seams of %s''s clothes', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogStaminaDamage);
    ShowLog('and she feels a droplet of liquid injected (%d staus effects applied)', [Count], ColorLogStaminaDamage);
    TargetPlayer.HitHealth(MarkData.Damage);
  end;
  Sound(MarkData.HitSound);
  //ViewGame.WakeUp(true, true); // doesn't wake up
end;

{ TMarkStatusEffectData ----------------------------- }

procedure TMarkStatusEffectData.Validate;
var
  I: Integer;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if MaxCount <= 0 then
    raise EDataValidationError.CreateFmt('MaxCount <= 0 in %s', [Self.ClassName]);
  if MinCount <= 0 then
    raise EDataValidationError.CreateFmt('MinCount <= 0 in %s', [Self.ClassName]);
  if SleepAttackMultiplier <= 0 then
    raise EDataValidationError.CreateFmt('SleepAttackMultiplier <= 0 in %s', [Self.ClassName]);
  if MinCount > MaxCount then
    raise EDataValidationError.CreateFmt('MinCount > MaxCount in %s', [Self.ClassName]);
  if StatusEffects.Count < MaxCount then
    raise EDataValidationError.CreateFmt('StatusEffects.Count < MaxCount in %s', [Self.ClassName]);
  for I := 0 to Pred(StatusEffects.Count) do
    if not StatusEffects[I].Effect.CanBeStatusEffect then
      raise EDataValidationError.CreateFmt('StatusEffects[%d] as %s cannot be status effect in %s', [I, StatusEffects[I].Effect.ClassName, Self.ClassName]);
end;

procedure TMarkStatusEffectData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  StatusEffects := LoadStatusEffectsList(Element);
  MinCount := Element.AttributeInteger('MinCount');
  MaxCount := Element.AttributeInteger('MaxCount');
  SleepAttackMultiplier := Element.AttributeInteger('SleepAttackMultiplier');
  Damage := Element.AttributeSingle('Damage');
end;

function TMarkStatusEffectData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will randommly apply %d-%d out of %d possible status effects.', [MinCount, MaxCount, StatusEffects.Count]),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('If the target is asleep, will inject the status effects at %dx duration and flee.', [SleepAttackMultiplier]),
    Classname + '_sleep', 1));
end;

function TMarkStatusEffectData.Mark: TMarkClass;
begin
  Exit(TMarkStatusEffect);
end;

destructor TMarkStatusEffectData.Destroy;
begin
  FreeAndNil(StatusEffects);
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TMarkStatusEffect);
  RegisterSerializableData(TMarkStatusEffectData);

end.


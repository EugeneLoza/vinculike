{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkAttachLeash;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameMarkAbstract,
  GameItemData,
  GameUnlockableEntry;

type
  TMarkMarkAttachLeash = class(TMarkTargetAbstract)
  protected
    procedure Perform; override;
  end;

  TMarkMarkAttachLeashData = class(TMarkAbstractData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    LeashData, CollarData: TItemData;
    HitSound: String;
    LeashLength: Single;
    LeashBoost: Single;
    StaminaDamage: Single;
    DragStaminaDamage: Single;
    LeashShorteningOwner: Single;
    ObedienceWillHealing: Single;
    ObedienceDuration: Single;
    function Description: TEntriesList; override;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameInventoryItem, GameApparelSlots,
  GameViewGame, GameSounds, GameActor, GameLeash,
  GameStatusEffect, GameEnchantmentCannotInteractWithActor,
  GameItemsDatabase, GamePlayerCharacter, GameStats,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TMarkMarkAttachLeashData}
{$INCLUDE marktypecasts.inc}

procedure TMarkMarkAttachLeash.Perform;

  procedure TryMakeObedient;
  var
    SE: TStatusEffect;
    Enchantment: TEnchantmentCannotInteractWithActor;
  begin
    if TargetPlayer.CanInteractWith(ParentActor) then
    begin
      SE := TStatusEffect.Create;
      SE.Timeout := MarkData.ObedienceDuration;
      Enchantment := TEnchantmentCannotInteractWithActor.Create;
      Enchantment.Strength := 1;
      Enchantment.ActorReferenceId := ParentActor.ReferenceId;
      SE.Effect := Enchantment;
      TargetPlayer.Inventory.AddStatusEffect(SE);
      ShowLog('She definitely won''t dare attacking the leash holder (%.1n seconds)', [MarkData.ObedienceDuration], ColorLogTickle);
    end;
  end;

var
  Leash: TLeash;
  Collar: TInventoryItem;
  OldWill: Single;
begin
  Sound(MarkData.HitSound);

  LocalStats.IncStat(Data.ClassName);

  // if target has non-restraint in collar, try remove it
  if (TargetPlayer.Inventory.Apparel[esNeck] <> nil) and (not TargetPlayer.Inventory.Bondage[esNeck]) then
  begin
    if (Rnd.Random > TargetPlayer.ResistMultiplier) or TargetPlayer.Unsuspecting or TargetPlayer.Immobilized then
    begin
      ShowLog('%s pulls %s from %s''s neck and casts it aside', [ParentActor.Data.DisplayName, TargetPlayer.Inventory.Apparel[esNeck].Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondage);
      UnequipAndDrop(esNeck, not TargetPlayer.Unsuspecting);
    end else
    begin
      ShowLog('%s tries to remove %s from %s''s neck but she manages to dodge', [ParentActor.Data.DisplayName, TargetPlayer.Inventory.Apparel[esNeck].Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondageItemSave);
      TargetPlayer.HitStamina(MarkData.StaminaDamage);
    end;
    Exit;
  end else
  // if target doesn't have a collar - equip one
  if TargetPlayer.Inventory.Apparel[esNeck] = nil then
  begin
    if (Rnd.Random > TargetPlayer.ResistMultiplier) or TargetPlayer.Unsuspecting or TargetPlayer.Immobilized then
    begin
      Collar := TInventoryItem.NewItem(MarkData.CollarData);
      TargetPlayer.Inventory.EquipItem(Collar);
      Sound(MarkData.CollarData.SoundEquip);
      ShowLog('%s locks %s over %s''s neck', [ParentActor.Data.DisplayName, Collar.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondage);
      TargetPlayer.Particle('BOUND', ColorParticlePlayerBound);
    end else
    begin
      ShowLog('%s tries to collar %s, but she slips out at the last moment', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondageItemSave);
      TargetPlayer.HitStamina(MarkData.StaminaDamage);
    end;
    Exit; // will not wake up
  end;

  // at this point we guarantee that character wears some collar

  if TargetPlayer.Inventory.Apparel[esLeash] = nil then
  begin
    if (Rnd.Random > TargetPlayer.ResistMultiplier) or TargetPlayer.Unsuspecting or TargetPlayer.Immobilized then
    begin
      Leash := TLeash.NewLeash(MarkData.LeashData);
      TargetPlayer.Inventory.EquipItem(Leash);
      Leash.LeashLength := MarkData.LeashLength;
      Sound(MarkData.LeashData.SoundEquip);
      ShowLog('%s skillfully snaps %s to %s''s %s', [ParentActor.Data.DisplayName, Leash.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Inventory.Apparel[esNeck].Data.DisplayName], ColorLogBondage);
      if TargetPlayer.Unsuspecting or TargetPlayer.Immobilized then
      begin
        ShowLog('and holds the loose end tightly', [], ColorLogBondage);
        Leash.LeashHolder := Parent;
        TargetPlayer.Particle('LEASH', ColorParticlePlayerBound);
      end;
    end else
    begin
      ShowLog('%s tries to leash %s, but she slips out at the last moment', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondageItemSave);
      TargetPlayer.HitStamina(MarkData.StaminaDamage);
    end;
  end else
  begin
    Leash := (TargetPlayer.Inventory.Apparel[esLeash] as TLeash);
    if Leash.LeashHolder <> nil then
    begin
      if Leash.LeashHolder <> Parent then
      begin
        Leash.MaxDurability := (1 - MarkData.LeashBoost) * Leash.MaxDurability + MarkData.LeashBoost * Leash.ItemData.Durability;
        Leash.Durability := (1 - MarkData.LeashBoost) * Leash.Durability + MarkData.LeashBoost * Leash.MaxDurability;
        Sound(TargetPlayer.Inventory.Equipped[esLeash].ItemData.SoundRepair);
        ShowLog('%s makes sure %s is attached tight and secure', [ParentActor.Data.DisplayName, Leash.Data.DisplayName], ColorLogBondage);
      end else
      // this is leash owner
      if (TargetPlayer.DistanceToSqr(ParentActor) < Sqr(ParentActor.Size)) then
      begin
        if not TargetPlayer.Inventory.Equipped[esNeck].ItemData.Indestructible then
        begin
          TargetPlayer.Inventory.ReinforceItem(esNeck);
          Sound(TargetPlayer.Inventory.Equipped[esNeck].ItemData.SoundRepair);
          ShowLog('%s checks if %s''s %s sits tight and secure', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[esNeck].Data.DisplayName], ColorLogBondage);
        end else
        begin
          Leash.MaxDurability := (1 - MarkData.LeashBoost) * Leash.MaxDurability + MarkData.LeashBoost * Leash.ItemData.Durability;
          Leash.Durability := (1 - MarkData.LeashBoost) * Leash.Durability + MarkData.LeashBoost * Leash.MaxDurability;
          Sound(TargetPlayer.Inventory.Equipped[esLeash].ItemData.SoundRepair);
          ShowLog('%s inspects %s and makes sure it''s attached well', [ParentActor.Data.DisplayName, Leash.Data.DisplayName], ColorLogBondage);
        end;

        //obedient healing
        if not TargetWasResisting then
        begin
          OldWill := TargetPlayer.Will;
          TargetPlayer.HealWill(Rnd.Random * MarkData.ObedienceWillHealing);
          ShowLog('%s feels oddly obedient, and ... happy about that', [TargetActor.Data.DisplayName], ColorLogBondageItemSave);
          TryMakeObedient;
          if TargetPlayer.Will - OldWill > 1 then
          begin
            LocalStats.IncStat(Data.ClassName + '_healing');
            ShowLog('%.1n willpower healed', [TargetPlayer.Will - OldWill], ColorLogBondageItemSave);
          end;
        end;
      end else
      begin
        DragTarget;
        ShowLog('%s doesn''t like %s wandering off and harshly pulls her closer', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickle);
        if Leash.LeashLength > ParentActor.Size then
        begin
          ShowLog('Slightly shortening the leash length for misbehavior', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickle);
          Leash.LeashLength -= MarkData.LeashShorteningOwner;
        end;
        TargetPlayer.HitStamina(MarkData.DragStaminaDamage);
      end;
    end else
    begin
      Leash.LeashHolder := ParentActor;
      ShowLog('%s catches the loose end of %s', [ParentActor.Data.DisplayName, Leash.Data.DisplayName], ColorLogBondage);
      TargetPlayer.Particle('LEASH', ColorParticlePlayerBound);
    end;
  end;

  ViewGame.WakeUp(true, true);
end;

{ TMarkMarkAttachLeashData ----------------------------- }

procedure TMarkMarkAttachLeashData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if LeashData = nil then
    raise EDataValidationError.CreateFmt('LeashData = nil in %s', [Self.ClassName]);
  if CollarData = nil then
    raise EDataValidationError.CreateFmt('CollarData = nil in %s', [Self.ClassName]);
  if LeashLength <= 1 then
    raise EDataValidationError.CreateFmt('LeashLength <= 1 in %s', [Self.ClassName]);
  if LeashLength >= 32 then
    raise EDataValidationError.CreateFmt('LeashLength >= 32 in %s', [Self.ClassName]);
  if LeashBoost <= 0.0 then
    raise EDataValidationError.CreateFmt('LeashBoost <= 0.0 in %s', [Self.ClassName]);
  if LeashBoost > 1.0 then
    raise EDataValidationError.CreateFmt('LeashBoost > 1.0 in %s', [Self.ClassName]);
  if StaminaDamage <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamage <= 0 in %s', [Self.ClassName]);
  if DragStaminaDamage <= 0 then
    raise EDataValidationError.CreateFmt('DragStaminaDamage <= 0 in %s', [Self.ClassName]);
  if LeashShorteningOwner < 0 then
    raise EDataValidationError.CreateFmt('LeashShorteningOwner < 0 in %s', [Self.ClassName]);
  if ObedienceWillHealing < 0 then
    raise EDataValidationError.CreateFmt('ObedienceWillHealing < 0 in %s', [Self.ClassName]);
  if ObedienceDuration <= 0 then
    raise EDataValidationError.CreateFmt('ObedienceDuration <= 0 in %s', [Self.ClassName]);
end;

procedure TMarkMarkAttachLeashData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  LeashData := ItemsDataDictionary[Element.AttributeString('LeashData')] as TItemData; // tood: post-processing
  CollarData := ItemsDataDictionary[Element.AttributeString('CollarData')] as TItemData; // tood: post-processing
  LeashLength := Element.AttributeSingle('LeashLength');
  LeashBoost := Element.AttributeSingle('LeashBoost');
  StaminaDamage := Element.AttributeSingle('StaminaDamage');
  DragStaminaDamage := Element.AttributeSingle('DragStaminaDamage');
  LeashShorteningOwner := Element.AttributeSingle('LeashShorteningOwner');
  ObedienceWillHealing := Element.AttributeSingle('ObedienceWillHealing');
  ObedienceDuration := Element.AttributeSingle('ObedienceDuration');
end;

function TMarkMarkAttachLeashData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will attempt to attach %s to heroine''s collar. If there is no collar - will try to equip %s. If the leash is loose, will catch it and hold. And finallhy if the leash is already held, will reinforce it by %d%%. In some situations can also "shorten" the leash for trying to wander off.', [LeashData.DisplayName, CollarData.DisplayName, Round(100 * LeashBoost)]),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('If the heroine is not resisting, will heal current value of willpower and temporary make unable to attack the leash holder.', []),
    Classname + '_healing', 1));
end;

function TMarkMarkAttachLeashData.Mark: TMarkClass;
begin
  Exit(TMarkMarkAttachLeash);
end;

initialization
  RegisterSerializableObject(TMarkMarkAttachLeash);
  RegisterSerializableData(TMarkMarkAttachLeashData);

end.


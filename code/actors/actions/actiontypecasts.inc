{$IFDEF SafeTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$DEFINE ParentPlayer:=(Parent as TPlayerCharacter)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE TargetMonster:=(Target as TMonster)}
{$DEFINE ActionData:=(Data as DataClass)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$DEFINE ParentPlayer:=TPlayerCharacter(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE TargetMonster:=TMonster(Target)}
{$DEFINE ActionData:=DataClass(Data)}
{$ENDIF}


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionKnockback;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameSimpleSerializableObject,
  GameActionAbstract, GameActionRoll;

type
  { Exactly the same as ActionRoll, but cannot be canceled }
  TActionKnockback = class(TActionRoll)
  public
    function CanStop: Boolean; override;
    function Immobilized: Boolean; override;
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
  end;

  TActionKnockbackData = class(TActionRollData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  GameSerializableData;

function TActionKnockback.CanStop: Boolean;
begin
  //inherited - no need;
  Exit(false); // cannot stop knockback
end;

function TActionKnockback.Immobilized: Boolean;
begin
  Exit(true);
end;

function TActionKnockback.NoiseMultiplier: Single;
begin
  //Result := inherited NoiseMultiplier;
  Exit(2.0);
end;

function TActionKnockback.NoiseAddition: Single;
begin
  Exit(0.5);
end;

{ TActionKnockbackData ------------------------------ }

function TActionKnockbackData.Action: TActionClass;
begin
  // inherited --- overriding parent
  Exit(TActionKnockback);
end;

initialization
  RegisterSimpleSerializableObject(TActionKnockback);
  RegisterSerializableData(TActionKnockbackData);

end.


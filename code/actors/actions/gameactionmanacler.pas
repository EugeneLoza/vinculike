{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionManacler;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  CastleVectors,
  GameActionAbstract, GameActionOnTarget, GameActionRoll,
  GameUnlockableEntry;

type
  TActionManacler = class(TActionOnTarget)
  strict private
    OldVector: TVector2;
    Phase: Single;
    PreHit: Boolean;
    SubActionRoll: TActionRoll;
    procedure FinishRollToTarget;
    procedure FinishRollFromTarget;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function CanStop: Boolean; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    destructor Destroy; override;
  end;

  TActionManaclerData = class(TActionOnTargetData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    RollWarmUp: Single;
    function Description: TEntriesList; override;
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData, GameGarbageCollector,
  GameActor, GamePlayerCharacter, GameMarkAbstract,
  GameMonster, GameMap,
  GameViewGame, GameLog, GameStats, GameDifficultyLevel,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TActionManaclerData}
{$INCLUDE actiontypecasts.inc}

procedure TActionManacler.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
  Element.AttributeSet('PreHit', PreHit);
  Element.AttributeSet('OldVector', OldVector);
  if SubActionRoll <> nil then // TODO: It most likely will crash Loading --- but can happen as soon as action started but didn't receive Update yet.
    SubActionRoll.Save(Element.CreateChild('SubActionRoll'));
end;

procedure TActionManacler.Load(const Element: TDOMElement);
var
  SubActionRollElement: TDOMElement;
begin
  inherited Load(Element);
  Phase := Element.AttributeSingle('Phase');
  PreHit := Element.AttributeBoolean('PreHit');
  OldVector := Element.AttributeVector2('OldVector');
  SubActionRollElement := Element.ChildElement('SubActionRoll', false);
  if SubActionRollElement <> nil then
  begin
    SubActionRoll := TActionAbstract.LoadClass(SubActionRollElement) as TActionRoll;
    if PreHit then
      SubActionRoll.OnActionFinished := @FinishRollToTarget
    else
      SubActionRoll.OnActionFinished := @FinishRollFromTarget;
  end else
    SubActionRoll := nil;
end;

function TActionManacler.NoiseMultiplier: Single;
begin
  Exit(1.1);
end;

function TActionManacler.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionManacler.CanStop: Boolean;
begin
  Exit(Phase < ActionData.RollWarmUp * Difficulty.MonsterAttackDelayMultiplier / 2); // "/2" because otherwise it won't be as dumb as it should be
end;

procedure TActionManacler.FinishRollToTarget;
begin
  if SubActionRoll <> nil then
    SubActionRoll.Stop;
  GarbageCollector.AddAndNil(SubActionRoll);
  Phase := 0;
  PreHit := true;
end;

procedure TActionManacler.FinishRollFromTarget;
begin
  if SubActionRoll <> nil then
    SubActionRoll.Stop;
  GarbageCollector.AddAndNil(SubActionRoll);
  Phase := 0;
  PreHit := true;
  ParentMonster.Ai.AiFlee := true;
end;

procedure TActionManacler.Update(const SecondsPassed: Single);

  procedure StartRollToTarget;
  begin
    SubActionRoll := TActionRoll.NewAction(Parent) as TActionRoll;
    SubActionRoll.MoveVector := Vector2(TargetPlayer.CenterX - ParentActor.CenterX, TargetPlayer.CenterY - ParentActor.CenterY);
    OldVector := SubActionRoll.MoveVector;
    SubActionRoll.MoveVectorNormalized := TActionRoll(SubActionRoll).MoveVector.Normalize;
    SubActionRoll.MoveVector := 15 * TActionRoll(SubActionRoll).MoveVectorNormalized;
    SubActionRoll.Start;
    SubActionRoll.OnActionFinished := @FinishRollToTarget;
  end;

  procedure HitTargetAndRollBack;
  var
    Mark: TMarkTargetAbstract;
  begin
    LocalStats.IncStat(Data.ClassName);

    if not ActionData.MarkData.Independent then
      ShowError('Mark %s must be independent to be used with %s', [ActionData.MarkData.ClassName, ClassName]);
    if ActionData.MarkData.Size <> 3 then
      ShowError('Mark %s must have size 3 for %s', [ActionData.MarkData.ClassName, ClassName]);
    Mark := ActionData.MarkData.Mark.Create as TMarkTargetAbstract;
    Mark.Target := TargetPlayer;
    Mark.Parent := ParentActor;
    Mark.Data := ActionData.MarkData;
    Mark.SetSize(Mark.Data.Size);
    Mark.X := TargetPlayer.X;
    Mark.Y := TargetPlayer.Y;
    Mark.CenterX := TargetPlayer.CenterX;
    Mark.CenterY := TargetPlayer.CenterY;
    Mark.LastTileX := TargetPlayer.LastTileX;
    Mark.LastTileY := TargetPlayer.LastTileY;
    Mark.LastTile := TargetPlayer.LastTile;
    //Mark.OnFinished := @MarkFinished;
    Map.MarksList.Add(Mark);

    if SubActionRoll <> nil then
      SubActionRoll.Stop;
    GarbageCollector.AddAndNil(SubActionRoll);
    SubActionRoll := TActionRoll.NewAction(Parent) as TActionRoll;
    TActionRoll(SubActionRoll).MoveVectorNormalized := -OldVector.Normalize;
    TActionRoll(SubActionRoll).MoveVector := -OldVector;
    SubActionRoll.Start;
    SubActionRoll.OnActionFinished := @FinishRollFromTarget;
    PreHit := false;
  end;

begin
  inherited;

  if not TargetPlayer.CanAct then
  begin
    ActionFinished;
    Exit;
  end;

  Phase += SecondsPassed;
  if (SubActionRoll = nil) and (Phase >= ActionData.RollWarmUp * Difficulty.MonsterAttackDelayMultiplier) then
    StartRollToTarget;
  if SubActionRoll <> nil then
  begin
    if PreHit and (ParentActor.Collides(TargetPlayer, 0) or ViewGame.BuggyLargeTimeFlowSpeed) then
      HitTargetAndRollBack;
    SubActionRoll.Update(SecondsPassed);
  end;
end;

class function TActionManacler.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionManacler).Phase := 0;
  (Result as TActionManacler).PreHit := true;
end;

destructor TActionManacler.Destroy;
begin
  GarbageCollector.FreeNowAndNil(SubActionRoll);
  inherited Destroy;
end;

{ TActionManaclerData -------------------------------}

procedure TActionManaclerData.Validate;
begin
  inherited Validate;
  if RollWarmUp < 0 then
    raise Exception.CreateFmt('RollWarmUp < 0 in %s', [Self.ClassName]);
end;

procedure TActionManaclerData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  RollWarmUp := Element.AttributeFloat('RollWarmUp');
end;

function TActionManaclerData.Description: TEntriesList;
var
  ActionEntries: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will dash at target and throw a mark, then without waiting for the result will dash back to safety and run away.', []),
    Classname, 1));
  ActionEntries := MarkData.Description;
  Result.AddRange(ActionEntries);
  FreeAndNil(ActionEntries);
end;

function TActionManaclerData.Action: TActionClass;
begin
  Exit(TActionManacler);
end;

initialization
  RegisterSimpleSerializableObject(TActionManacler);
  RegisterSerializableData(TActionManaclerData);

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionRamTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameActionAbstract, GameActionOnTarget, GameActionRoll,
  GameUnlockableEntry;

type
  TActionRamTarget = class(TActionOnTarget)
  strict private
    Phase: Single;
    PreHit: Boolean;
    SubActionRoll: TActionRoll;
    procedure FinishRoll;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function CanStop: Boolean; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    destructor Destroy; override;
  end;

  TActionRamTargetData = class(TActionOnTargetData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    HitSound: String;
    RollWarmUp: Single;
    StaminaDamage: Single;
    HealthDamage: Single;
    NudeStaminaDamage: Single;
    NudeHealthDamage: Single;
    function Description: TEntriesList; override;
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleVectors, CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData, GameGarbageCollector,
  GameActor, GamePlayerCharacter, GameActionKnockback, GameApparelSlots,
  GameViewGame, GameLog, GameColors, GameRandom, GameStats, GameSounds, GameDifficultyLevel,
  GameVinculopediaEntryText;

{$DEFINE DataClass:=TActionRamTargetData}
{$INCLUDE actiontypecasts.inc}

procedure TActionRamTarget.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
  Element.AttributeSet('PreHit', PreHit);
  if SubActionRoll <> nil then // TODO: It most likely will crash Loading --- but can happen as soon as action started but didn't receive Update yet.
    SubActionRoll.Save(Element.CreateChild('SubActionRoll'));
end;

procedure TActionRamTarget.Load(const Element: TDOMElement);
var
  SubActionRollElement: TDOMElement;
begin
  inherited Load(Element);
  Phase := Element.AttributeSingle('Phase');
  PreHit := Element.AttributeBoolean('PreHit');
  SubActionRollElement := Element.ChildElement('SubActionRoll', false);
  if SubActionRollElement <> nil then
  begin
    SubActionRoll := TActionAbstract.LoadClass(SubActionRollElement) as TActionRoll;
    SubActionRoll.OnActionFinished := @FinishRoll;
  end else
    SubActionRoll := nil;
end;

function TActionRamTarget.NoiseMultiplier: Single;
begin
  Exit(1.1);
end;

function TActionRamTarget.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionRamTarget.CanStop: Boolean;
begin
  Exit(Phase < ActionData.RollWarmUp * Difficulty.MonsterAttackDelayMultiplier / 10); // "/10" because otherwise it won't be as dumb as it should be
end;

procedure TActionRamTarget.FinishRoll;
begin
  if SubActionRoll <> nil then
    SubActionRoll.Stop;
  GarbageCollector.AddAndNil(SubActionRoll);
  Phase := 0;
  PreHit := true;
end;

procedure TActionRamTarget.Update(const SecondsPassed: Single);
var
  EscapeVector: TVector2;
  E: TApparelSlot;
begin
  inherited;

  if not TargetPlayer.CanAct then
  begin
    ActionFinished;
    Exit;
  end;

  Phase += SecondsPassed;
  if (SubActionRoll = nil) and (Phase >= ActionData.RollWarmUp * Difficulty.MonsterAttackDelayMultiplier) then
  begin
    SubActionRoll := TActionRoll.NewAction(Parent) as TActionRoll;
    TActionRoll(SubActionRoll).MoveVector := Vector2(TargetPlayer.CenterX - ParentActor.CenterX, TargetPlayer.CenterY - ParentActor.CenterY);
    if (TActionRoll(SubActionRoll).MoveVector.Length > 3)
      or TargetPlayer.Unsuspecting {hack to avoid bug that rams cannot attack sleeping target, because they are too close - it doesn't seem to work too good, but it works} then
    begin
      TActionRoll(SubActionRoll).MoveVectorNormalized := TActionRoll(SubActionRoll).MoveVector.Normalize;
      TActionRoll(SubActionRoll).MoveVector := 15 * TActionRoll(SubActionRoll).MoveVectorNormalized;
    end else
    begin
      //roll away if target is too close;
      LocalStats.IncStat(Data.ClassName + '_retreat');
      TActionRoll(SubActionRoll).MoveVectorNormalized := TActionRoll(SubActionRoll).MoveVector.Normalize;
      repeat
        EscapeVector := Vector2(Rnd.Random - 0.5, Rnd.Random - 0.5).Normalize;
      until TVector2.DotProduct(EscapeVector, TActionRoll(SubActionRoll).MoveVectorNormalized) <= 0;
      TActionRoll(SubActionRoll).MoveVectorNormalized := EscapeVector;
      TActionRoll(SubActionRoll).MoveVector := 5 * TActionRoll(SubActionRoll).MoveVectorNormalized;
      PreHit := false;
    end;
    SubActionRoll.Start;
    SubActionRoll.OnActionFinished := @FinishRoll;
  end;
  if SubActionRoll <> nil then
  begin
    if PreHit and (ParentActor.Collides(TargetPlayer, 0) or ViewGame.BuggyLargeTimeFlowSpeed) then
    begin
      //if SubActionRoll <> nil then // this is guaranteed by if above
      SubActionRoll.Stop;
      GarbageCollector.AddAndNil(SubActionRoll);
      SubActionRoll := TActionRoll.NewAction(Parent) as TActionRoll;
      TActionRoll(SubActionRoll).MoveVectorNormalized := -Vector2(TargetPlayer.CenterX - ParentActor.CenterX, TargetPlayer.CenterY - ParentActor.CenterY).Normalize;
      TActionRoll(SubActionRoll).MoveVector := 4 * TActionRoll(SubActionRoll).MoveVectorNormalized;
      SubActionRoll.Start;
      SubActionRoll.OnActionFinished := @FinishRoll;
      PreHit := false;

      LocalStats.IncStat(Data.ClassName);
      if TargetPlayer.Inventory.EquippedSlotsRemovable > 0 then
      begin
        E := TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem;
        ShowLog('%s rams into %s knocking %s off her %s', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName, EquipSlotsToHumanReadableString(TargetPlayer.Inventory.Equipped[E].Data.EquipSlots)], ColorLogItemSteal);
        TargetPlayer.Inventory.UnequipAndDrop(E, true);
        TargetPlayer.HitStamina(ActionData.StaminaDamage);
        TargetPlayer.Hit(ActionData.HealthDamage);
      end else
      begin
        //otherwise hit double power
        TargetPlayer.HitStamina(ActionData.NudeStaminaDamage);
        TargetPlayer.Hit(ActionData.NudeHealthDamage);
      end;

      Sound(ActionData.HitSound);
      ParentActor.DamageWeapon;
      // Push target
      if Rnd.Random > TargetPlayer.ResistKnockback then
      begin
        TargetPlayer.CurrentAction := TActionKnockback.NewAction(Target);
        TActionKnockback(TargetPlayer.CurrentAction).MoveVector := Vector2(TargetPlayer.CenterX - ParentActor.CenterX, TargetPlayer.CenterY - ParentActor.CenterY);
        TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized := TActionKnockback(TargetPlayer.CurrentAction).MoveVector.Normalize;
        TActionKnockback(TargetPlayer.CurrentAction).MoveVector := 20 * TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized;
        TargetPlayer.CurrentAction.Start;
        TargetPlayer.AddResistKnockback;
        ViewGame.ShakeMap;
      end else
        ShowLog('%s manages to hold the ground', [TargetActor.Data.DisplayName], ColorLogStaminaDamage);
    end;
    SubActionRoll.Update(SecondsPassed); // note that SubActionRoll <> nil here in any case
  end;
end;

class function TActionRamTarget.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionRamTarget).Phase := 0;
  (Result as TActionRamTarget).PreHit := true;
end;

destructor TActionRamTarget.Destroy;
begin
  GarbageCollector.FreeNowAndNil(SubActionRoll);
  inherited Destroy;
end;

{ TActionRamTargetData -------------------------------}

procedure TActionRamTargetData.Validate;
begin
  inherited Validate;
  if not SoundExists(HitSound) then
    raise EDataValidationError.CreateFmt('Invalid HitSound = "%s" in %s', [HitSound, Self.ClassName]);
  if RollWarmUp < 0 then
    raise EDataValidationError.CreateFmt('RollWarmUp must be >= 0 in %s', [ClassName]);
  if StaminaDamage < 0 then
    raise EDataValidationError.CreateFmt('StaminaDamage must be >= 0 in %s', [ClassName]);
  if HealthDamage < 0 then
    raise EDataValidationError.CreateFmt('HealthDamage must be >= 0 in %s', [ClassName]);
  if NudeStaminaDamage < 0 then
    raise EDataValidationError.CreateFmt('NudeStaminaDamage must be >= 0 in %s', [ClassName]);
  if NudeHealthDamage < 0 then
    raise EDataValidationError.CreateFmt('NudeHealthDamage must be >= 0 in %s', [ClassName]);
end;

procedure TActionRamTargetData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  HitSound := Element.AttributeString('HitSound');
  RollWarmUp := Element.AttributeSingle('RollWarmUp');
  StaminaDamage := Element.AttributeSingle('StaminaDamage');
  HealthDamage := Element.AttributeSingle('HealthDamage');
  NudeStaminaDamage := Element.AttributeSingle('NudeStaminaDamage');
  NudeHealthDamage := Element.AttributeSingle('NudeHealthDamage');
end;

function TActionRamTargetData.Description: TEntriesList;
begin
  Result := inherited Description;
  Result.Add(
    NewEntryText(
      Format('ATTACK: Will try to ram into the character. Unable to change direction while moving.', []),
    Classname, 1));
  Result.Add(
    NewEntryText(
      Format('Needs to build up momentum before hitting the target, therefore will instead retreat if the target is too close, hoping for a better attack position.', []),
    Classname + '_retreat', 1));
end;

function TActionRamTargetData.Action: TActionClass;
begin
  Exit(TActionRamTarget);
end;

initialization
  RegisterSimpleSerializableObject(TActionRamTarget);
  RegisterSerializableData(TActionRamTargetData);

end.


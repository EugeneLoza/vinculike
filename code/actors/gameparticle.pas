{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ particles, for now mostly informative }
unit GameParticle;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, Generics.Collections,
  CastleVectors, CastleColors,
  GameMapTypes, GamePositionedObject;

const
  { How long the particle flies }
  ParticleDuration = 0.5;

type
  { text-based particle }
  TParticle = class(TPositionedObject)
  public
    Phase: Single;
    { Text to display }
    Value: String;
    { Cokor of the text }
    Color: TCastleColor;
    { Half-width of the text (cached) }
    HalfWidth: Single;
    { Direction of the particle movement }
    Dir: TVector2;
    procedure Update(const SecondsPassed: Single); virtual;
  end;
  TParticlesList = specialize TObjectList<TParticle>;

{ Spawn a new particle on the map }
procedure NewParticle(const AX, AY: Single; const AValue: String; const AColor: TCastleColor);
implementation
uses
  GameMap, GameFonts;

procedure NewParticle(const AX, AY: Single; const AValue: String;
  const AColor: TCastleColor);
var
  P: TParticle;
begin
  P := TParticle.Create;
  P.MoveMeTo(AX, AY);
  P.Value := AValue;
  P.Color := AColor;
  P.Dir := Vector2(0, 5);
  P.Phase := 0;
  P.HalfWidth := FontSoniano16.TextWidth(AValue) / 2;
  Map.ParticlesList.Add(P);
end;

procedure TParticle.Update(const SecondsPassed: Single);
begin
  Phase += SecondsPassed;
  if (Phase > ParticleDuration) then
  begin
    Map.ParticlesList.Remove(Self);
    Exit;
  end;
  // not MoveMeTo because we don't need to change other stuff like LastTile
  X += Dir.X * SecondsPassed;
  Y += Dir.Y * SecondsPassed;
  CenterX := X;
  CenterY := Y;
end;

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Additional data on top of TActorDataused by more complex entities}
unit GameMonsterData;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM, Generics.Collections,
  CastleGlImages,
  GameActorData, GameAiAbstract;

const
  { Non-despawning monster life duration }
  MaxLifeTime = Single(99999);

type
  { Data used by monsters actors, defining stats, behavior, spawn rules etc. }
  TMonsterData = class(TActorData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Image: TDrawableImage; // Temporary: animated texture
    ImageXRay: TDrawableImage; // unlike Image: not animated?
    Trap: Boolean; // TODO: Traps as a separate class
    Chest: Boolean; // TODO: as a separate class
    VacuumCleaner: Boolean; // TODO: ????
    IsGuard: Boolean; // TODO: ????
    Mimic: Boolean; // TODO
    { Invisible monsters are treated slightly different by UI
      (e.g. won't display health, effectively hiding an easily noticeable label) }
    IsInvisible: Boolean;
    { Monster uses "curious ai", actively exploring the map }
    CuriousAi: Boolean;
    { Support monster, which is spawned by slightly different rule
      (can't be the only monster type on the map or in a partol wave)
      usually this means the monster cannot harm player character under some (easily available) circumstances }
    Rare: Boolean;
    { Class of ai this monster uses }
    AiData: TAiAbstractData;
    { How far away this monster can be heard }
    Noisiness: Single;
    { For spawned minions, time after which it despawns }
    LifeTime: Single;
    { Sound to play when monster becomes suspicious and moves to investigate }
    SoundInvestigate: String;
    { Sound to play when monster sees the target }
    SoundAttack: String;
    { Sound to play when the monster inerts }
    SoundDie: String;
    { Relative frequency of the monster spawn on the map
      default 1.0 means that monster will use all offered spawn slots
      0.5 means that monster will spawn 50% less often than a regular monster,
      but always at least once, even if this value is 0 }
    SpawnFrequency: Single;
    StartSpawningAtDepth: Integer;
    StopSpawningAtDepth: Integer;
    StartPatrolAtDepth: Integer;
    StopPatrolAtDepth: Integer;
    VisionBonus: Single;
    VisionBottomClothed: Single;
    VisionBottomNaked: Single;
    VisionTopClothed: Single;
    VisionTopNaked: Single;
    { Danger is a coefficient of comparing different monsters
      affects quantity of monsters spawned
      e.g. if danger=1 and monsters spawned 21
      then in the same situation will spawn only 7 of monsters with danger=3 }
    Danger: Single;
    { How much experience the character will gain for killing this monster }
    XP: Integer;
  public
    destructor Destroy; override;
  end;
  TMonstersDataList = specialize TObjectList<TMonsterData>;
  TMonstersDataDictionary = specialize TObjectDictionary<String, TMonsterData>;

implementation
uses
  CastleXMLUtils, CastleStringUtils, CastleVectors, CastleUriUtils,
  GameCachedImages, GameSerializableData,
  GameMonstersDatabase;

procedure TMonsterData.Validate;
begin
  inherited;
  if Image = nil then
    raise EActorDataValidationError.Create('Image = nil : ' + DisplayName);
  if Trap and Chest then
    raise EActorDataValidationError.Create('Trap and Chest : ' + DisplayName);
  if Trap and Mimic then
    raise EActorDataValidationError.Create('Trap and Mimic : ' + DisplayName);
  if Trap and VacuumCleaner then
    raise EActorDataValidationError.Create('Trap and VacuumCleaner : ' + DisplayName);
  if Trap and IsGuard then
    raise EActorDataValidationError.Create('Trap and IsGuard : ' + DisplayName);
  if Chest and VacuumCleaner then
    raise EActorDataValidationError.Create('Chest and VacuumCleaner : ' + DisplayName);
  if Chest and IsGuard then
    raise EActorDataValidationError.Create('Chest and IsGuard : ' + DisplayName);
  if VacuumCleaner and IsGuard then
    raise EActorDataValidationError.Create('VacuumCleaner and IsGuard : ' + DisplayName);
  if Mimic and not Chest then
    raise EActorDataValidationError.Create('Mimic and not Chest : ' + DisplayName);
  if Mimic and VacuumCleaner then
    raise EActorDataValidationError.Create('Mimic and VacuumCleaner : ' + DisplayName);
  if LifeTime <= 0 then
    raise EActorDataValidationError.Create('LifeTime <= 0 : ' + Id);
  if SpawnFrequency < 0 then
    raise EActorDataValidationError.Create('SpawnFrequency < 0 : ' + Id);
  if StartSpawningAtDepth < 0 then
    raise EActorDataValidationError.Create('StartSpawningAtDepth < 0 : ' + DisplayName);
  if StopSpawningAtDepth < StartSpawningAtDepth then
    raise EActorDataValidationError.Create('StopSpawningAtDepth < StartSpawningAtDepth : ' + DisplayName);
  if StartPatrolAtDepth < 0 then
    raise EActorDataValidationError.Create('StartPatrolAtDepth < 0 : ' + DisplayName);
  if StopPatrolAtDepth < StartPatrolAtDepth then
    raise EActorDataValidationError.Create('StopPatrolAtDepth < StartPatrolAtDepth : ' + DisplayName);
  if VisionBonus < 0 then
    raise EActorDataValidationError.Create('VisionBonus < 0 : ' + DisplayName);
  if VisionBottomClothed < 0 then
    raise EActorDataValidationError.Create('VisionBottomClothed < 0 : ' + DisplayName);
  if VisionBottomNaked < 0 then
    raise EActorDataValidationError.Create('VisionBottomNaked < 0 : ' + DisplayName);
  if VisionTopClothed < 0 then
    raise EActorDataValidationError.Create('VisionTopClothed < 0 : ' + DisplayName);
  if VisionTopNaked < 0 then
    raise EActorDataValidationError.Create('VisionTopNaked < 0 : ' + DisplayName);
  if Danger < 0 then
    raise EActorDataValidationError.Create('Danger < 0 : ' + DisplayName);
  if XP < 0 then
    raise EActorDataValidationError.Create('XP < 0 : ' + DisplayName);
  {TODO: will break chests
  if ActionData = nil then
    raise EActorDataValidationError.Create('ActionData = nil : ' + DisplayName);}
end;

procedure TMonsterData.Read(const Element: TDOMElement);
begin
  Noisiness := Element.AttributeSingle('Noisiness');
  LifeTime := Element.AttributeSingleDef('LifeTime', MaxLifeTime+1); //+1 is to make floating-point errors happy
  Trap := Element.AttributeBooleanDef('Trap', false);
  Chest := Element.AttributeBooleanDef('Chest', false);
  VacuumCleaner := Element.AttributeBooleanDef('VacuumCleaner', false);
  IsGuard := Element.AttributeBooleanDef('IsGuard', false);
  Mimic := Element.AttributeBooleanDef('Mimic', false);
  IsInvisible := Element.AttributeBooleanDef('IsInvisible', false);
  CuriousAi := Element.AttributeBooleanDef('CuriousAi', false);
  Rare := Element.AttributeBooleanDef('Rare', false);
  Danger := Element.AttributeSingle('Danger');
  XP := Element.AttributeInteger('XP');
  SpawnFrequency := Element.AttributeFloatDef('SpawnFrequency', 1.0);
  StartSpawningAtDepth := Element.AttributeInteger('StartSpawningAtDepth');
  StopSpawningAtDepth := Element.AttributeInteger('StopSpawningAtDepth');
  StartPatrolAtDepth := Element.AttributeInteger('StartPatrolAtDepth');
  StopPatrolAtDepth := Element.AttributeInteger('StopPatrolAtDepth');
  VisionBonus := Element.AttributeSingleDef('VisionBonus', 0);
  VisionBottomClothed := Element.AttributeSingleDef('VisionBottomClothed', 0);
  VisionBottomNaked := Element.AttributeSingleDef('VisionBottomNaked', 0);
  VisionTopClothed := Element.AttributeSingleDef('VisionTopClothed', 0);
  VisionTopNaked := Element.AttributeSingleDef('VisionTopNaked', 0);
  AiData := TAiAbstractData.ReadClass(FBaseUrl, Element.Child('Ai')) as TAiAbstractData;
  Image := LoadDrawable(CombineURI(FBaseUrl, Element.AttributeString('Image')));
  ImageXRay := TDrawableImage.Create(CombineURI(FBaseUrl, Element.AttributeString('Image')));
  ImageXRay.Color := Vector4(1, 1, 1, 0.5);
  SoundDie := Element.AttributeStringDef('SoundDie', 'monster_die');
  SoundAttack := Element.AttributeStringDef('SoundAttack', 'monster_attack');
  SoundInvestigate := Element.AttributeStringDef('SoundInvestigate', 'monster_investigate');
  inherited;
end;

destructor TMonsterData.Destroy;
begin
  FreeAndNil(AiData);
  FreeAndNil(ImageXRay);
  inherited;
end;

initialization
  RegisterSerializableData(TMonsterData);
end.


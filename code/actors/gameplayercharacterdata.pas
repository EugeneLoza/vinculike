{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Data for player characters
  note: some values here are generated and currently cannot be saved }
unit GamePlayerCharacterData;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameActorData;

type
  TPlayerCharacterData = class(TActorData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    MaxStamina: Single;
    MaxWill: Single;
    MaxMagic: Single;
    HealthRegen, MaxHealthRegen: Single;
    StaminaRegen, MaxStaminaRegen: Single;
    WillRegen, MaxWillRegen: Single;
    MagicRegen, MaxMagicRegen: Single;
    { Stamina needed to perform a dash }
    RollStaminaCost: Single;
  end;

implementation
uses
  GameSerializableData;

procedure TPlayerCharacterData.Validate;
begin
  inherited Validate;
  if MaxStamina <= 0 then
    raise EActorDataValidationError.Create('MaxStamina <= 0 : ' + DisplayName);
  if MaxWill <= 0 then
    raise EActorDataValidationError.Create('MaxWill <= 0 : ' + DisplayName);
  if MaxMagic <= 0 then
    raise EActorDataValidationError.Create('MaxMagic <= 0 : ' + DisplayName);
  if HealthRegen <= 0 then
    raise EActorDataValidationError.Create('HealthRegen <= 0 : ' + DisplayName);
  if MaxHealthRegen <= 0 then
    raise EActorDataValidationError.Create('MaxHealthRegen <= 0 : ' + DisplayName);
  if StaminaRegen <= 0 then
    raise EActorDataValidationError.Create('StaminaRegen <= 0 : ' + DisplayName);
  if MaxStaminaRegen <= 0 then
    raise EActorDataValidationError.Create('MaxStaminaRegen <= 0 : ' + DisplayName);
  if WillRegen <= 0 then
    raise EActorDataValidationError.Create('WillRegen <= 0 : ' + DisplayName);
  if MaxWillRegen <= 0 then
    raise EActorDataValidationError.Create('MaxWillRegen <= 0 : ' + DisplayName);
  if RollStaminaCost <= 0 then
    raise EActorDataValidationError.Create('RollStaminaCost <= 0 : ' + DisplayName);
end;

procedure TPlayerCharacterData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  // TODO
end;

initialization
  RegisterSerializableData(TPlayerCharacterData);
end.


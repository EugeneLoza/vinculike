{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Data that determines values for all actors
  note: this may have been a bad idea,
  currently player character data is generated, not static
  also blueprints are actually the same thing, but are handled differently
  finally its non-saveable, so contains a lot of contradictions
  it may not be wise to refactor it into something different at this moment though }
unit GameActorData;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM, Generics.Collections,
  GameSerializableData, GameActionMoveAbstract;

type
  EActorDataValidationError = class(Exception);

type
  TActorData = class(TSerializableData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    { id of this data item, must be unique!
      warning: is broken for player characters
      warning: should be properly serialized instead of accessing from a dictionary? }
    Id: String;
    { Human readable name of this actor }
    DisplayName: String;
    { Max health for this actor (kind) }
    MaxHealth: Single;
    { Size of this actor in tiles, size is always square NxN
      there isn't much complication to make x-y size independent,
      but pathfinding will quickly become non-obvious for the players }
    Size: Byte;
    { speed of move action}
    MovementSpeed: Single;
    { speed of "dash/roll" action.
      note: most actors don't use this values
      and practically it's needed separate only for player characters }
    RollSpeed: Single;
    { Max range of roll/dash action.
      note: most actors don't use this values }
    RollRange: Single;
    { Style of the actor movement,
      e.g. using pathfinding or ghosting through walls are}
    MoveAction: TActionMoveClass;
  end;
  TActorDataList = specialize TObjectList<TActorData>;
  TActorDataDictionary = specialize TObjectDictionary<String, TActorData>;

implementation
uses
  CastleXMLUtils,
  GameMapTypes, GameActionMoveGhost, GameActionMove;

procedure TActorData.Validate;
begin
  if Id = '' then
    raise EActorDataValidationError.CreateFmt('Id = "" in %s', [ClassName]);
  if DisplayName = '' then
    raise EActorDataValidationError.Create('DisplayName = "" : ' + Id);
  if MaxHealth <= 1 then
    raise EActorDataValidationError.Create('MaxHealth <= 1 : ' + Id);
  if Size < 1 then
    raise EActorDataValidationError.Create('Size < 1 : ' + Id);
  if Size > PredMaxColliderSize + 1 then
    raise EActorDataValidationError.Create('Size > PredMaxColliderSize + 1 : ' + Id);
  if MovementSpeed < 0 then
    raise EActorDataValidationError.Create('MovementSpeed < 0 : ' + Id);
  if RollSpeed < 0 then
    raise EActorDataValidationError.Create('RollSpeed < 0 : ' + Id);
  if RollRange < 0 then
    raise EActorDataValidationError.Create('RollRange < 0 : ' + Id);
end;

procedure TActorData.Read(const Element: TDOMElement);
begin
  Id := Element.AttributeString('Id');
  DisplayName := Element.AttributeString('Name');
  MaxHealth := Element.AttributeSingle('Health');
  Size := Element.AttributeInteger('Size');
  MovementSpeed := Element.AttributeSingle('Speed');
  RollSpeed := Element.AttributeSingleDef('RollSpeed', 1); // default values should not necessarily make sense, but must allow validation
  RollRange := Element.AttributeSingleDef('RollRange', 1);

  // Ok, I'm confused, let's ignore this thing for now:
  //MoveAction := SerializableDataByName(Element.AttributeString('MoveAction')) as TActionMoveAbstractData;
  case Element.AttributeString('MoveAction') of
    'TActionMove': MoveAction := TActionMove;
    'TActionMoveGhost': MoveAction := TActionMoveGhost;
    else
      raise EActorDataValidationError.Create('MoveAction="' + Element.AttributeString('MoveAction') + '" unexpected : ' + Id);
  end;
end;

end.


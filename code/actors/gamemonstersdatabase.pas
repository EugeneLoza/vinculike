{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ contains data related to complex entities,
  in different lists and dictionary for different uses }
unit GameMonstersDatabase;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  GameActorData, GameMonsterData;

var
  { Monsters - aggressive or conditionally-aggressive entities}
  MonstersData: TMonstersDataList;
  { Containers}
  ChestsData: TMonstersDataList;
  { Mimics that can be used as chests to place items}
  MimicsData: TMonstersDataList;
  { Traps - passive entities triggered under some circumstances }
  TrapsData: TMonstersDataList;
  { Guard monsters, spawned at prisoners }
  GuardsData: TMonstersDataList;
  { Vacuum cleaners, periodically enter the map to pick up items lying around }
  VacuumCleanersData: TMonstersDataList;
  { the main container for all complex entities, owns objects }
  MonstersDataDictionary: TMonstersDataDictionary;

{ Clear all monsters definitions and lists }
procedure ClearMonstersData;
{ Load monsters data from a package }
procedure LoadMonstersData(const Url: String);
{ Free all lists }
procedure FreeMonstersData;
implementation
uses
  Generics.Collections,
  CastleXmlUtils, CastleUriUtils,
  GameLog;

procedure ClearMonstersData;
begin
  if MonstersDataDictionary = nil then
  begin
    MonstersData := TMonstersDataList.Create(false);
    ChestsData := TMonstersDataList.Create(false);
    MimicsData := TMonstersDataList.Create(false);
    TrapsData := TMonstersDataList.Create(false);
    GuardsData := TMonstersDataList.Create(false);
    VacuumCleanersData := TMonstersDataList.Create(false);
    MonstersDataDictionary := TMonstersDataDictionary.Create([doOwnsValues]);
  end else
  begin
    MonstersData.Clear;
    ChestsData.Clear;
    MimicsData.Clear;
    TrapsData.Clear;
    GuardsData.Clear;
    VacuumCleanersData.Clear;
    MonstersDataDictionary.Clear;
  end;
end;

procedure LoadMonstersData(const Url: String);
var
  AMonsterData: TMonsterData;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
  Path: String;
begin
  LogNormal('Loading monsters: %s', [Url]);
  Doc := URLReadXML(Url);
  Path := ExtractUriPath(Url);
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('MonsterData');
    try
      while Iterator.GetNext do
      begin
        AMonsterData := TMonsterData.ReadClass(Path, Iterator.Current) as TMonsterData;
        if AMonsterData.VacuumCleaner then
          VacuumCleanersData.Add(AMonsterData)
        else
        if AMonsterData.Trap then
          TrapsData.Add(AMonsterData)
        else
        if AMonsterData.Mimic then // WARNING: Mimics are also chests, so we shouldn't add them to ChestsData TODO
          MimicsData.Add(AMonsterData)
        else
        if AMonsterData.Chest then
          ChestsData.Add(AMonsterDAta)
        else
        if AMonsterData.IsGuard then
          GuardsData.Add(AMonsterData)
        else
          MonstersData.Add(AMonsterData);
        MonstersDataDictionary.Add(AMonsterData.Id, AMonsterData);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;

procedure FreeMonstersData;
begin
  FreeAndNil(MonstersData);
  FreeAndNil(ChestsData);
  FreeAndNil(TrapsData);
  FreeAndNil(MimicsData);
  FreeAndNil(GuardsData);
  FreeAndNil(VacuumCleanersData);
  FreeAndNil(MonstersDataDictionary);
end;

end.


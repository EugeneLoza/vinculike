{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ An object that has a position on the Map
  and methods to handle those convenient }
unit GamePositionedObject;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, Classes, DOM,
  CastleColors,
  GameSerializableObject, GameMapTypes;

type
  TPositionedObject = class abstract(TSerializableObject)
  protected
    { Teleport this actor to a random passable tile on the map }
    procedure TeleportToRandomPoint;
    { Picks best visible grade of the object area }
    function VisibleGrade: TVisibleGrade;
  public
    { CACHE of object's size. It should not be saved/loaded/changed }
    Size, PredSize: Byte;
    HalfSize: Single;
    { Properly set size of the object }
    procedure SetSize(const ASize: Byte);
  public
    { Bottom-left of the object }
    X, Y: Single;
    { Calculated value of object center }
    CenterX, CenterY: Single;
    { Last valid tile this actor has been }
    LastTileX, LastTileY: Int16;
    LastTile: SizeInt;
    { Teleports object to coordinates.
      In addition to MoveMeTo also resets the actor to idle }
    procedure Teleport(const ToX, ToY: Int16); virtual;
    procedure Teleport(const ToX, ToY: Single); virtual;
    procedure TeleportCenter(const ToX, ToY: Single);
    procedure MoveMeTo(const ToX, ToY: Int16);
    procedure MoveMeTo(const ToX, ToY: Single);
    { Same as MoveMeTo, but doesn't update LastTile if can't }
    procedure MoveMeToSafe(const ToX, ToY: Single);
    { Returns true if move succeeded, false if stuck in a corner }
    function MoveMeToDelta(const Delta, VX, VY: Single): Boolean;
    procedure MoveCenterTo(const ToX, ToY: Single);
    procedure MoveCenterTo(const ThatObject: TPositionedObject);
    { Add Seed of this object to the provided TSeedList }
    procedure AddSeed(const SeedList: TIntCoordList);
    procedure Particle(const Caption: String; const Color: TCastleColor); // todo: image
  public
    function IsVisible: Boolean; virtual;
    { If this object has unobstructed line of sight to that object? - performs a raycast on integer map coordinates }
    function LineOfSight(const ThatObject: TPositionedObject): Boolean;
    function LineOfSightMore(const ThatObject: TPositionedObject): Boolean;
    function LineOfSightMore(const AX, AY: Int16): Boolean;
    function LineOfSight(const AX, AY: Int16): Boolean;
    { Is object at given coordinates? Used for UI clicks }
    function IsHere(const AX, AY: Single; const AdditionalMargin: Single): Boolean; inline;
    function IsHere(const AX, AY: Int16; const AdditionalMargin: Int16): Boolean; inline;
    function SphericalDistanceNormalizedSqr(const ThatObject: TPositionedObject): Single;
    function Collides(const ThatObject: TPositionedObject; const AdditionalMargin: Single): Boolean; inline;
    function CollidesInt(const ThatObject: TPositionedObject; const AdditionalMargin: Int16): Boolean; inline;
    function CollidesInt(const AX, AY: Int16; const AdditionalMargin: Int16): Boolean; inline;
    function DistanceTo(const ThatObject: TPositionedObject): Single;
    function DistanceToSqr(const ThatObject: TPositionedObject): Single;
    function DistanceTo(const AX, AY: Single): Single;
    function DistanceToSqr(const AX, AY: Single): Single;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  end;

implementation
uses
  CastleXmlUtils,
  GameMap, GameRandom, GameLog, GameParticle, GameMath;

procedure TPositionedObject.TeleportToRandomPoint;
begin
  repeat
    LastTileX := Rnd.Random(Map.SizeX);
    LastTileY := Rnd.Random(Map.SizeY);
  until Map.PassableTiles[PredSize][LastTileX + Map.SizeX * LastTileY];
  Teleport(LastTileX, LastTileY);
end;

procedure TPositionedObject.SetSize(const ASize: Byte);
begin
  Size := ASize;
  PredSize := Pred(ASize);
  HalfSize := Single(ASize) / 2.0;
end;

procedure TPositionedObject.Teleport(const ToX, ToY: Int16);
begin
  MoveMeTo(ToX, ToY);
end;

procedure TPositionedObject.Teleport(const ToX, ToY: Single);
begin
  MoveMeTo(ToX, ToY);
end;

procedure TPositionedObject.TeleportCenter(const ToX, ToY: Single);
begin
  Teleport(ToX - HalfSize, ToY - HalfSize);

  { This may happen, as usually "teleporting actor" doesn't check if the "teleported one" fits
    Most likely smaller entity (like tickling slime) drags larger entity (Player character)
    Resulting in trying to drag larger one into the wall }
  if not Map.PassableTiles[PredSize][LastTile] then
  begin
    LastTileX := LastTileX + 1;
    LastTileY := LastTileY + 1;
    LastTile := LastTileX + Map.SizeX * LastTileY;
    if not Map.PassableTiles[PredSize][LastTile] then
    begin
      ShowError('Failed to adjust LastTile after TeleportCenter');
      // TODO : some safety workaround here?
    end;
  end;
end;

procedure TPositionedObject.MoveMeTo(const ToX, ToY: Int16);
begin
  if (ToX <= 0) or (ToY <=0) or (ToX >= Map.PredSizeX) or (ToY >= Map.PredSizeY) then
  begin
    ShowError('%s is trying to teleport to broken coordinates (%d, %d) on map %dx%d. Teleporting to random point instead.', [ClassName, ToX, ToY, Map.SizeX, Map.SizeY]);
    TeleportToRandomPoint;
    Exit;
  end;
  X := ToX;
  Y := ToY;
  CenterX := X + HalfSize;
  CenterY := Y + HalfSize;
  LastTileX := ToX;
  LastTileY := ToY;
  LastTile := LastTileX + Map.SizeX * LastTileY;
end;

procedure TPositionedObject.MoveMeTo(const ToX, ToY: Single);
begin
  if (ToX <= 0) or (ToY <=0) or (ToX >= Map.PredSizeX) or (ToY >= Map.PredSizeY) then
  begin
    ShowError('%s is trying to teleport to broken coordinates (%.1n, %.1n) on map %dx%d. Teleporting to random point instead.', [ClassName, ToX, ToY, Map.SizeX, Map.SizeY]);
    TeleportToRandomPoint;
    Exit;
  end;
  X := ToX;
  Y := ToY;
  CenterX := X + HalfSize;
  CenterY := Y + HalfSize;
  LastTileX := Trunc(ToX);
  LastTileY := Trunc(ToY);
  LastTile := LastTileX + Map.SizeX * LastTileY;
end;

procedure TPositionedObject.MoveMeToSafe(const ToX, ToY: Single);
begin
  if (ToX <= 0) or (ToY <= 0) or (ToX >= Map.PredSizeX) or (ToY >= Map.PredSizeY) then
  begin
    ShowError('%s is trying to teleport to broken coordinates (%.1n, %.1n) on map %dx%d. Teleporting to random point instead.', [ClassName, ToX, ToY, Map.SizeX, Map.SizeY]);
    TeleportToRandomPoint;
    Exit;
  end;
  X := ToX;
  Y := ToY;
  CenterX := X + HalfSize;
  CenterY := Y + HalfSize;
  if Map.PassableTiles[PredSize][Trunc(X) + Map.SizeX * Trunc(Y)] then
  begin
    LastTileX := Trunc(ToX);
    LastTileY := Trunc(ToY);
    LastTile := LastTileX + Map.SizeX * LastTileY;
  end; // else just don't update last tile
end;

function TPositionedObject.MoveMeToDelta(const Delta, VX, VY: Single): Boolean;
var
  DX, DY, DXX, DYY: Single;

  function TrySlideYSimple: Boolean;
  begin
    if DYY = 0 then
      Exit(false)
    else
    if Map.PassableTiles[PredSize][LastTileX + Map.SizeX * (LastTileY + Sign(DYY))] then
    begin
      DX := 0;
      DY := DYY;
      Exit(true);
    end else
      Exit(false);
  end;

  function TrySlideYCorner(const VY: Single): Boolean;
  begin
    if Map.PassableTiles[PredSize][LastTileX + Map.SizeX * (LastTileY + Sign(VY))] and Map.PassableTiles[PredSize][LastTileX + Sign(DXX) + Map.SizeX * (LastTileY + Sign(VY))] then
    begin
      DX := 0;
      DY := VY;
      Exit(true);
    end else
      Exit(false);
  end;

  function TrySlideYCorner2(const VY: Single): Boolean;
  begin
    if Map.PassableTiles[PredSize][LastTileX + Map.SizeX * (LastTileY + Sign(VY))] and Map.PassableTiles[PredSize][LastTileX + Map.SizeX * (LastTileY + 2 * Sign(VY))] and Map.PassableTiles[PredSize][LastTileX + Sign(DXX) + Map.SizeX * (LastTileY + 2 * Sign(VY))] then
    begin
      DX := 0;
      DY := VY;
      Exit(true);
    end else
      Exit(false);
  end;

  function TrySlideXSimple: Boolean;
  begin
    if DXX = 0 then
      Exit(false)
    else
    if Map.PassableTiles[PredSize][LastTileX + Sign(DXX) + Map.SizeX * LastTileY] then
    begin
      DX := DXX;
      DY := 0;
      Exit(true);
    end else
      Exit(false);
  end;

  function TrySlideXCorner(const VX: Single): Boolean;
  begin
    if Map.PassableTiles[PredSize][LastTileX + Sign(VX) + Map.SizeX * LastTileY] and Map.PassableTiles[PredSize][LastTileX + Sign(VX) + Map.SizeX * (LastTileY + Sign(DYY))] then
    begin
      DX := VX;
      DY := 0;
      Exit(true);
    end else
      Exit(false);
  end;

  function TrySlideXCorner2(const VX: Single): Boolean;
  begin
    if Map.PassableTiles[PredSize][LastTileX + Sign(VX) + Map.SizeX * LastTileY] and Map.PassableTiles[PredSize][LastTileX + 2 * Sign(VX) + Map.SizeX * LastTileY] and Map.PassableTiles[PredSize][LastTileX + 2 * Sign(VX) + Map.SizeX * (LastTileY + Sign(DYY))] then
    begin
      DX := VX;
      DY := 0;
      Exit(true);
    end else
      Exit(false);
  end;

begin
  if (LastTileX <> Trunc(X)) or (LastTileY <> Trunc(Y)) then
  begin
    LogNormal('Starting MoveMeToDelta from unsafe position %d,%d <-> %.3n,%.3n', [LastTileX, LastTileY, X, Y]);
    if Map.PassableTiles[PredSize][Trunc(X) + Map.SizeX * Trunc(Y)] then
    begin
      { So... we try just to adjust LastTile for now... It's a bad solution, but it is. }
      LastTileX := Trunc(X);
      LastTileY := Trunc(Y);
      LastTile := LastTileX + Map.SizeX * LastTileY;
    end else
    begin
      if Map.PassableTiles[PredSize][LastTile] then
        { It's already bad enough }
        LogWarning('MoveMeToDelta: %s failed to adjust integer coordinates to floats. Relying on old value of LastTile.', [ClassName])
        //X := LastTileX;
        //Y := LastTileY;
      else
      begin
        { And here it's really, really bad }
        ShowError('MoveMeToDelta: %s cannot adjust integers to floats and the last tile was not walkable.', [ClassName]);
      end;
    end;
  end;

  DX := Delta * VX;
  DY := Delta * VY;
  DXX := Delta * Sign(VX);
  DYY := Delta * Sign(VY);

  if not Map.PassableTiles[PredSize][Trunc(X + DX) + Map.SizeX * Trunc(Y + DY)] then
    if Abs(DX) > Abs(DY) then
    begin
      if Map.PassableTiles[PredSize][Trunc(X + DXX) + Map.SizeX * LastTileY] then
      begin
        DX := DXX;
        DY := 0;
      end else
        if not TrySlideYSimple and not TrySlideYCorner(Delta) and not TrySlideYCorner(-Delta) and not TrySlideYCorner2(Delta) and not TrySlideYCorner2(-Delta) then
          Exit(false);
    end else
    begin
      if Map.PassableTiles[PredSize][LastTileX + Map.SizeX * Trunc(Y + DYY)] then
      begin
        DX := 0;
        DY := DYY;
      end else
      begin
        if not TrySlideXSimple and not TrySlideXCorner(Delta) and not TrySlideXCorner(-Delta) and not TrySlideXCorner2(Delta) and not TrySlideXCorner2(-Delta) then
          Exit(false);
      end;
    end;

  MoveMeToSafe(X + DX, Y + DY);
  Exit(true);
end;

procedure TPositionedObject.MoveCenterTo(const ToX, ToY: Single);
begin
  if (ToX <= 0) or (ToY <=0) or (ToX >= Map.PredSizeX) or (ToY >= Map.PredSizeY) then
  begin
    ShowError('%s is trying to teleport to broken coordinates (%.1n, %.1n) on map %dx%d. Teleporting to random point instead.', [ClassName, ToX, ToY, Map.SizeX, Map.SizeY]);
    TeleportToRandomPoint;
    Exit;
  end;
  CenterX := ToX;
  CenterY := ToY;
  X := CenterX - HalfSize;
  Y := CenterY - HalfSize;
  LastTileX := Trunc(X);
  LastTileY := Trunc(Y);
  LastTile := LastTileX + Map.SizeX * LastTileY;
end;

procedure TPositionedObject.MoveCenterTo(const ThatObject: TPositionedObject);
begin
  MoveCenterTo(ThatObject.CenterX, ThatObject.CenterY);
end;

procedure TPositionedObject.AddSeed(const SeedList: TIntCoordList);
var
  DX, DY: Integer;
begin
  for DX := 0 to PredSize do
    for DY := 0 to PredSize do
      SeedList.Add(IntCoord(LastTileX + DX, LastTileY + DY));
end;

procedure TPositionedObject.Particle(const Caption: String; const Color: TCastleColor);
begin
  NewParticle(CenterX, CenterY, Caption, Color);
end;

function TPositionedObject.IsVisible: Boolean;
begin
  Exit(VisibleGrade >= RememberedVisible);
end;

function TPositionedObject.LineOfSight(const ThatObject: TPositionedObject): Boolean;
var
  TX, TY: Int16;
  //IX, IY: Integer;
  JX, JY: Integer;
begin
  //for IX := 0 to PredSize do
    //for IY := 0 to PredSize do
      for JX := 0 to ThatObject.PredSize do
        for JY := 0 to ThatObject.PredSize do
        begin
          TX := ThatObject.LastTileX + JX;
          TY := ThatObject.LastTileY + JY;
          // WARNING in Ray ToX, ToY are var and values are changed! - TODO
          if Map.Ray({Data.PredSize}0, LastTileX + Size div 2, LastTileY + Size div 2, TX, TY) then
            Exit(true);
        end;
  Exit(false);
end;

function TPositionedObject.LineOfSightMore(
  const ThatObject: TPositionedObject): Boolean;
var
  IX, IY, TX, TY: Int16;
begin
  TX := Trunc(ThatObject.CenterX);
  TY := Trunc(ThatObject.CenterY);
  for IX := 0 to PredSize do
    for IY := 0 to PredSize do
      if not Map.Ray(0, LastTileX + IX, LastTileY + IY, TX, TY) then
        Exit(false);
  Exit(true);
end;

function TPositionedObject.LineOfSightMore(const AX, AY: Int16): Boolean;
var
  IX, IY, TX, TY: Int16;
begin
  TX := AX;
  TY := AY;
  for IX := 0 to PredSize do
    for IY := 0 to PredSize do
      if not Map.Ray(0, LastTileX + IX, LastTileY + IY, TX, TY) then
        Exit(false);
  Exit(true);
end;

function TPositionedObject.LineOfSight(const AX, AY: Int16): Boolean;
var
  TX, TY: Int16;
begin
  TX := AX;
  TY := AY;
  Exit(Map.Ray({Data.PredSize}0, LastTileX + Size div 2, LastTileY + Size div 2, TX, TY));
end;

function TPositionedObject.VisibleGrade: TVisibleGrade;
var
  IX, IY: Integer;
begin
  if PredSize = 0 then
    Exit(Map.Visible[LastTile])
  else
  begin
    Result := 0;
    for IX := LastTileX to LastTileX + PredSize do  // here we count on that the object cannot be beyond map border
      for IY := LastTileY to LastTileY + PredSize do
        if Map.Visible[IX + Map.SizeX * IY] > Result then
          Result := Map.Visible[IX + Map.SizeX * IY]; // TODO: Optimize!
  end;
end;

function TPositionedObject.IsHere(const AX, AY: Single; const AdditionalMargin: Single): Boolean; inline;
begin
  Exit((AX >= X - AdditionalMargin) and (AY >= Y - AdditionalMargin) and (AX <= X + Size + AdditionalMargin) and (AY <= Y + Size + AdditionalMargin));
end;

function TPositionedObject.IsHere(const AX, AY: Int16; const AdditionalMargin: Int16): Boolean; inline;
begin
  // note, we need PredSize for Int IsHere, and Size for float IsHere
  Exit((AX >= LastTileX - AdditionalMargin) and (AY >= LastTileY - AdditionalMargin) and
       (AX <= LastTileX + PredSize + AdditionalMargin) and (AY <= LastTileY + PredSize + AdditionalMargin));
end;

function TPositionedObject.SphericalDistanceNormalizedSqr(const ThatObject: TPositionedObject): Single;
begin
  Exit(
    (Sqr(CenterX - ThatObject.CenterX) + Sqr(CenterY - ThatObject.CenterY)) // DistanceToSqr
    / (HalfSize + ThatObject.HalfSize));
end;

function TPositionedObject.Collides(const ThatObject: TPositionedObject;
  const AdditionalMargin: Single): Boolean; inline;
begin
  Exit((X <= ThatObject.X + ThatObject.Size + AdditionalMargin) and (X + Size + AdditionalMargin >= ThatObject.X) and
       (Y <= ThatObject.Y + ThatObject.Size + AdditionalMargin) and (Y + Size + AdditionalMargin >= ThatObject.Y));
end;

function TPositionedObject.CollidesInt(const ThatObject: TPositionedObject;
  const AdditionalMargin: Int16): Boolean; inline;
begin
  // note, we need PredSize for Int collision, and Size for float collision
  Exit((LastTileX <= ThatObject.LastTileX + ThatObject.PredSize + AdditionalMargin) and (LastTileX + PredSize + AdditionalMargin >= ThatObject.LastTileX) and
       (LastTileY <= ThatObject.LastTileY + ThatObject.PredSize + AdditionalMargin) and (LastTileY + PredSize + AdditionalMargin >= ThatObject.LastTileY));
end;

function TPositionedObject.CollidesInt(const AX, AY: Int16;
  const AdditionalMargin: Int16): Boolean;
begin
  Exit((LastTileX <= AX + AdditionalMargin) and (LastTileX + PredSize + AdditionalMargin >= AX) and
       (LastTileY <= AY + AdditionalMargin) and (LastTileY + PredSize + AdditionalMargin >= AY));
end;

function TPositionedObject.DistanceTo(const ThatObject: TPositionedObject): Single;
begin
  Exit(Sqrt(Sqr(CenterX - ThatObject.CenterX) + Sqr(CenterY - ThatObject.CenterY)));
end;

function TPositionedObject.DistanceToSqr(const ThatObject: TPositionedObject): Single;
begin
  Exit(Sqr(CenterX - ThatObject.CenterX) + Sqr(CenterY - ThatObject.CenterY));
end;

function TPositionedObject.DistanceTo(const AX, AY: Single): Single;
begin
  Exit(Sqrt(Sqr(CenterX - AX) + Sqr(CenterY - AY)));
end;

function TPositionedObject.DistanceToSqr(const AX, AY: Single): Single;
begin
  Exit(Sqr(CenterX - AX) + Sqr(CenterY - AY));
end;

procedure TPositionedObject.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('X', X);
  Element.AttributeSet('Y', Y);
  Element.AttributeSet('CenterX', CenterX); // as PositionedObject doesn't have Size we have to store CenterX/Y too
  Element.AttributeSet('CenterY', CenterY);
  Element.AttributeSet('LastTileX', LastTileX);
  Element.AttributeSet('LastTileY', LastTileY);
end;

procedure TPositionedObject.Load(const Element: TDOMElement);
begin
  inherited;
  X := Element.AttributeSingle('X');
  Y := Element.AttributeSingle('Y');
  CenterX := Element.AttributeSingle('CenterX');
  CenterY := Element.AttributeSingle('CenterY');
  LastTileX := Element.AttributeInteger('LastTileX');
  LastTileY := Element.AttributeInteger('LastTileY');
  if (LastTileX <= 0) or (LastTileY <=0) or (LastTileX >= Map.PredSizeX) or (LastTileY >= Map.PredSizeY) then
    ShowError('%s is trying to load broken coordinates (%d, %d) on map %dx%d.', [ClassName, LastTileX, LastTileY, Map.SizeX, Map.SizeY]);
  LastTile := LastTileX + Map.SizeX * LastTileY;
end;

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Definition of TActor - an entity that exists, acts and has starts }
unit GameActor;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  CastleVectors,
  GameMapTypes, GameActorData, GamePositionedObject,
  GameActionAbstract, GameActionMoveAbstract;

type
  { An entity capable of existing and acting in the world
    has a CurrentAction and Health, can Moved and Die }
  TActor = class abstract(TPositionedObject)
  strict private
    FCurrentAction: TActionAbstract;
    procedure SetCurrentAction(const AValue: TActionAbstract);
  protected
    { Data record for this actor - contains actor's base stats and other features
      note: it's inconsistent with Body Blueprint,
      because it's constant for monsters, but variable for player characters (depends on their level and skills)
      Maybe TODO: make blueprint as constant data and data as perennially variable }
    FData: TActorData;
    { Events that happen after this actor is incapacitated
      Maybe TODO: as player characters can't actually die,
      use this only for monsters and have a more elaborate system for player characters }
    procedure Die; virtual; abstract;
    { Assign data record to this actor and potentially initialize/update other fields accordingly }
    procedure SetData(const AData: TActorData); virtual;
  public
    { current and current maximin value of the actor's Health
      Data.MaxHealth is the upper maximum of the value:
      Health <= MaxHealth <= Data.MaxHealth
      the current value of MaxHealth can change during the game
      and can affect all actors that can regenerate stats or be healed }
    Health, MaxHealth: Single;
    procedure HealHealth(const AValue: Single);
    { Resting regenerates current value of MaxHealth, this is a simple method to
      considers all necessary checks }
    procedure HealMaxHealth(const AValue: Single);
    { Reset this actor to default state:
      all stats at their max values according to Data
      and resets the CurrentAction to Idle }
    procedure Reset; virtual;
  public
    { Data entry for this actor: defines max values of stats
      and additional stats and parameters, like actions }
    property Data: TActorData read FData write SetData;
    { CurrentAction of this actor,
      Default is TActionIdle (also for dead actors)
      must never be nil with a few exceptions (actor was just created and didn't reset/load yet) }
    property CurrentAction: TActionAbstract read FCurrentAction write SetCurrentAction;
    procedure Teleport(const ToX, ToY: Int16); override;
    procedure Teleport(const ToX, ToY: Single); override;
    { Roll/Dash to a coordinates }
    procedure RollTo(const ToX, ToY: Single);
    { Move to coordinates using collisions and pathfinding
      will select the most appropriate target point if movement to the requested one is impossible }
    procedure MoveTo(const ToX, ToY: Single);
    { Move to a specific Target actor using colliders and pathfinding
      and perform an action when the distance is small enough }
    procedure MoveAndAct(const Target: TActor; const Action: TActionAbstractData);
    { Have the actor "look around".
      Takes effect only for Player characters currently }
    procedure UpdateVisible; virtual;
    procedure Update(const SecondsPassed: Single); virtual;
    { Hit this actor for Damage
      clothes will absorb the damage }
    procedure Hit(const Damage: Single); virtual;
    { If this actor can act (and be controlled by player or AI) }
    function CanAct: Boolean; virtual;
    { If this actor is interactive (and can be targeted by actions) }
    function CanBeInteractedWith: Boolean; virtual;
    { If this actor isn't aware of danger
      such actors can be stealth-attacked
      some monsters can have special interaction with an unsuspecting target }
    function Unsuspecting: Boolean; virtual; abstract;
    { If this actor is immobilized and cannot act or defend itself
    some monsters can have special interactions with immobilized targets }
    function Immobilized: Boolean; virtual;
    function IsPassive: Boolean; virtual;
    { Callback that the "attack" action had been postponed
      performed successfully and ask the Inventory to reduce the weapon durability
      Maybe TODO: only for actors with Inventory }
    procedure DamageWeapon; virtual; // Temporary?
    { Callback from Movement actions that the actor has passed one tile
      will ask the Inventory to damage shoes or
      return "barefoot" which in turn will hurt feet }
    procedure HurtFeet(const Damage: Single); virtual; // Temporary?
    { If this actor CurrentAction targets another actor - return the reference
      if no actor is being targeted returns nil }
    function GetActionTarget: TObject;
    { Forces CurrentAction to Idle }
    procedure ForceResetToIdle;
    { Sets CurrentAction to Idle if current CurrentAction can be stopped }
    procedure ResetToIdle;
  public
    { Retrieve this actor's movement speed }
    function GetSpeed: Single; virtual;
    { Current movement action of this actor }
    function MoveAction: TActionMoveClass; virtual;
    { Retrieve this actor's dash/roll speed }
    function GetRollSpeed: Single; virtual;
    { Actor's total noisiness (can be detected by hearing sense) }
    function GetNoise: Single; virtual; abstract;
  public
    procedure Save(const Element: TDOMElement); override;
    procedure AfterDeserealization; override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameGarbageCollector,
  GameMap, GameLog, GameActionOnTarget,
  GameActionIdle, GameActionRoll,
  GameActionMoveAndActTarget;

procedure TActor.SetCurrentAction(const AValue: TActionAbstract);
begin
  // NOTE: we do not check if the new action is same as old one, we always free the previous one and start a new one; Hypothetically we even can't possibly assign action to self, it'll be a valid "crashing bug"
  if FCurrentAction <> nil then
    FCurrentAction.Stop;
  GarbageCollector.AddAndNil(FCurrentAction);
  FCurrentAction := AValue;
end;

procedure TActor.SetData(const AData: TActorData);
begin
  FData := AData;
end;

procedure TActor.HealHealth(const AValue: Single);
begin
  if Health + AValue < MaxHealth then
    Health := Health + AValue
  else
    Health := MaxHealth;
end;

function TActor.MoveAction: TActionMoveClass;
begin
  Exit(Data.MoveAction);
end;

procedure TActor.HealMaxHealth(const AValue: Single);
begin
  if MaxHealth + AValue < Data.MaxHealth then
    MaxHealth := MaxHealth + AValue
  else
    MaxHealth := Data.MaxHealth;
end;

procedure TActor.Reset;
begin
  MaxHealth := FData.MaxHealth;
  Health := MaxHealth;
  SetSize(FData.Size);
end;

procedure TActor.ForceResetToIdle;
begin
  CurrentAction := TActionIdle.NewAction(Self);
  CurrentAction.Start;
end;

procedure TActor.Teleport(const ToX, ToY: Int16);
begin
  inherited;
  ResetToIdle;
end;

procedure TActor.Teleport(const ToX, ToY: Single);
begin
  inherited;
  ResetToIdle;
end;

procedure TActor.RollTo(const ToX, ToY: Single);
begin
  if not Map.PassableTiles[PredSize][LastTile] then
    ShowError('RollTo origin is unsafe');
  CurrentAction := TActionRoll.NewAction(Self);
  TActionRoll(CurrentAction).MoveVector := Vector2(ToX - CenterX, ToY - CenterY);
  TActionRoll(CurrentAction).MoveVectorNormalized := TActionRoll(CurrentAction).MoveVector.Normalize;
  CurrentAction.Start;
end;

procedure TActor.MoveTo(const ToX, ToY: Single);
begin
  CurrentAction := MoveAction.NewAction(Self);
  (CurrentAction as TActionMoveAbstract).MoveTo(ToX, ToY);
  CurrentAction.Start;
end;

procedure TActor.MoveAndAct(const Target: TActor; const Action: TActionAbstractData);
begin
  if not (CurrentAction is TActionMoveAndActTarget) or (TActionMoveAndActTarget(CurrentAction).Target <> Target) then
  begin
    CurrentAction := TActionMoveAndActTarget.NewAction(Self);
    TActionMoveAndActTarget(CurrentAction).Data := Action;
    TActionMoveAndActTarget(CurrentAction).Target := Target;
    CurrentAction.Start;
  end;
end;

procedure TActor.UpdateVisible;
begin
  //do nothing
end;

procedure TActor.Update(const SecondsPassed: Single);
begin
  try
    CurrentAction.Update(SecondsPassed);
  except
    on E: Exception do
    begin
      try
        ShowError('%s:"%s" when updating %s for %s', [E.ClassName, E.Message, CurrentAction.ClassName, Data.Id]);
      except
        ShowError('Exception while trying to report exception in TActor.Update'); // this can happen in many cases, e.g. CurrentAction=nil or this whole actor has been freed and is a dangling pointer
      end;
      CurrentAction := TActionIdle.NewAction(Self);
      CurrentAction.Start;
    end;
  end;
end;

procedure TActor.Hit(const Damage: Single);
begin
  Health -= Damage;
  MaxHealth -= Damage / 10;
  if Health <= 0 then
    Die;
end;

function TActor.CanAct: Boolean;
begin
  Exit(Health > 0);
end;

function TActor.CanBeInteractedWith: Boolean;
begin
  Exit(Health > 0); // Equal to CanAct for most actor types
end;

function TActor.Immobilized: Boolean;
begin
  if CurrentAction <> nil then // this shouldn't happen, should it?
    Exit(CurrentAction.Immobilized)
  else
    Exit(false);
end;

function TActor.IsPassive: Boolean;
begin
  Exit((CurrentAction is TActionIdle) or Immobilized);
end;

procedure TActor.DamageWeapon;
begin
  // do nothing
end;

procedure TActor.HurtFeet(const Damage: Single);
begin
  // do nothing
end;

function TActor.GetActionTarget: TObject;
begin
  if CurrentAction is TActionOnTarget then
    Exit(TActionOnTarget(CurrentAction).Target)
  else
    Exit(nil);
end;

procedure TActor.ResetToIdle;
begin
  if Immobilized and not CurrentAction.CanStop {to show logs if any} then
    Exit;
  CurrentAction := TActionIdle.NewAction(Self);
  CurrentAction.Start;
end;

function TActor.GetSpeed: Single;
begin
  Exit(FData.MovementSpeed);
end;

function TActor.GetRollSpeed: Single;
begin
  Exit(FData.RollSpeed);
end;

procedure TActor.Save(const Element: TDOMElement);
var
  ActionElement: TDOMElement;
begin
  inherited Save(Element);
  Element.AttributeSet('Vital', Health);
  Element.AttributeSet('MaxVital', MaxHealth);
  Element.AttributeSet('Data.Id', Data.Id);
  ActionElement := Element.CreateChild('Action');
  try
    CurrentAction.Save(ActionElement);
  except
    on E: Exception do
    begin
      try
        ShowError('%s:"%s" saving the action %s for %s', [E.ClassName, E.Message, CurrentAction.ClassName, Data.Id]);
      except
        ShowError('Exception while trying to report exception in TActor.Save');
      end;
      Element.RemoveChild(ActionElement);
      ActionElement := Element.CreateChild('Action');
      CurrentAction := TActionIdle.NewAction(Self);
      CurrentAction.Start;
      CurrentAction.Save(ActionElement);
    end;
  end;
end;

procedure TActor.AfterDeserealization;
begin
  inherited AfterDeserealization;
  CurrentAction.AfterDeserealization;
end;

procedure TActor.Load(const Element: TDOMElement);
begin
  inherited;
  Health := Element.AttributeSingle('Vital');
  MaxHealth := Element.AttributeSingle('MaxVital');
  //Data.Id will be processed by child - MAYBE TODO!
  try
    // CAUTION: Actor.Data is still nil at this point! Deserealization of the action must not depend on it or must resume action on in AfterDeserealization
    CurrentAction := TSimpleSerializableObject.LoadClass(Element.Child('Action', true)) as TActionAbstract;
    CurrentAction.Parent := Self;
    // We do not call CurrentAction.Start here because the action has already started
  except
    on E: Exception do
    begin
      try
        ShowError('%s:"%s". Action not loaded. Resetting %s to idle.', [E.ClassName, E.Message, Element.AttributeStringDef('Data.Id', 'undefined')]);
      except
        ShowError('Exception while trying to report exception in TActor.Load');
      end;
      CurrentAction := TActionIdle.NewAction(Self);
      CurrentAction.Start;
    end;
  end;
  // WARNING: Player character has Will = 0 at this point. We can't rely on CanAct
  if (Health <= 0) and not (CurrentAction is TActionIdle) then
  begin
    ShowError('Dead actor had non-idle action. Resetting %s to idle.', [Element.AttributeStringDef('Data.Id', 'undefined')]);
    CurrentAction := TActionIdle.NewAction(Self);
    CurrentAction.Start;
  end;
end;

destructor TActor.Destroy;
begin
  FreeAndNil(FCurrentAction);
  inherited Destroy;
end;

end.


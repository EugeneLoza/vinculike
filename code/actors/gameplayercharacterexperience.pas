{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GamePlayerCharacterExperience;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM;

const
  ExperienceBase = 200;
  ExperienceCurve = 0.666666667;

const
  LevelHealthBonus = 5;
  LevelStaminaBonus = 10;
  LevelWillBonus = 5;
  LevelMagicBonus = 5;

type
  TPlayerCharacterExperience = class(TObject)
  strict private
    procedure DoLevelUp;
  public
    XpAccumulatedInThisRun: Single;
    Xp: Single; // I want extensive per-skill experience system, but no idea how yet
    // TODO: cache level progress and current level, don't calculate POW every frame
    procedure Recalculate;
    function Level: UInt16;
    function ExperienceToNextLevel: Single;
    function ExperienceFractionToNextLevel: Single;
    class function ExperienceToLevel(const Lvl: Integer): Single;
    procedure AddExperience(const AddXp: Single);
  public const Signature = 'experience';
  public
    procedure Save(const Element: TDOMElement);
    procedure Load(const Element: TDOMElement);
    constructor Create; // override;
  public
    Parent: TObject;
  end;

implementation
uses
  CastleXmlUtils,
  GamePlayerCharacter, GameSounds, GameLog, GameColors, GameRandom, GameMath,
  GameEnchantmentExperienceMultiplier;

{$IFDEF SafeTypecast}
{$DEFINE ParentPlayerCharacter:=(Parent as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentPlayerCharacter:=TPlayerCharacter(Parent)}
{$ENDIF}

procedure TPlayerCharacterExperience.DoLevelUp;
begin
  ShowLog('LEVEL UP! Earned new level = %d', [Level], ColorLogLevelup);
  ShowLog('Experience: %d; To next level: %d', [Round(Xp), Round(ExperienceToNextLevel)], ColorLogExperience);
  Sound('level_up');
  ParentPlayerCharacter.MaxHealth += LevelHealthBonus;
  ParentPlayerCharacter.MaxStamina += LevelStaminaBonus;
  ParentPlayerCharacter.MaxWill += LevelWillBonus;
  Recalculate;
end;

procedure TPlayerCharacterExperience.Recalculate;
begin
  LogVerbose('TPlayerCharacterExperience.Recalculate');
  ParentPlayerCharacter.PlayerCharacterData.MaxHealth := 50 + LevelHealthBonus * Level;
  ParentPlayerCharacter.PlayerCharacterData.MaxStamina := 50 + LevelStaminaBonus * Level;
  ParentPlayerCharacter.PlayerCharacterData.MaxWill := 50 + LevelWillBonus * Level;
  ParentPlayerCharacter.PlayerCharacterData.MaxMagic := 50 + LevelMagicBonus * Level;
end;

function TPlayerCharacterExperience.Level: UInt16;
begin
  Result := Trunc(PowerSingle(Xp / ExperienceBase, ExperienceCurve));
end;

function TPlayerCharacterExperience.ExperienceToNextLevel: Single;
begin
  Result := ExperienceToLevel(Level + 1) - Xp;
  if Result < 1 then
    Result := 1;
end;

function TPlayerCharacterExperience.ExperienceFractionToNextLevel: Single;
begin
  Exit( 1.0 - (ExperienceToLevel(Level + 1) - Xp) / (ExperienceToLevel(Level + 1) - ExperienceToLevel(Level)) );
end;

class function TPlayerCharacterExperience.ExperienceToLevel(const Lvl: Integer): Single;
begin
  Exit(ExperienceBase * PowerSingle(Lvl, 1 / ExperienceCurve));
end;

procedure TPlayerCharacterExperience.AddExperience(const AddXp: Single);
var
  OldLevel: UInt16;
  RealAddXp: Single;
begin
  RealAddXp := AddXp * ParentPlayerCharacter.Inventory.FindEffectMultiplier(TEnchantmentExperienceMultiplier);
  if RealAddXp > 0 then
  begin
    OldLevel := Level;
    Xp += RealAddXp;
    XpAccumulatedInThisRun += RealAddXp;
    ShowLog('Received %d XP', [Round(RealAddXp)], ColorLogExperience);
    if Level > OldLevel then
      DoLevelUp;
  end else
  if RealAddXp < 0 then
    ShowError('Tried to add negative XP = %.1n (original value %.1n)', [RealAddXp, AddXp]);
end;

procedure TPlayerCharacterExperience.Save(const Element: TDOMElement);
begin
  Element.AttributeSet('Xp', Xp);
  Element.AttributeSet('XpAccumulatedInThisRun', XpAccumulatedInThisRun);
end;

procedure TPlayerCharacterExperience.Load(const Element: TDOMElement);
begin
  Xp := Element.AttributeSingle('Xp');
  XpAccumulatedInThisRun := Element.AttributeSingleDef('XpAccumulatedInThisRun', Rnd.Random * 8 * Sqrt(Xp)); // TODO: not DEF
  Recalculate;
end;

constructor TPlayerCharacterExperience.Create;
begin
  inherited;
  Xp := 0;
  XpAccumulatedInThisRun := 0;
end;

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ this is an ugly workaround for inability to properly save/load the player character data,
  or absence of "blueprints" system before }
unit TempData;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GamePlayerCharacterData, GameActionAbstract;

function TempPlayerCharacter: TPlayerCharacterData;
function PlayerActionAttack: TActionAbstractData;
function PlayerActionDisarm: TActionAbstractData;
function PlayerActionRescue: TActionAbstractData;
function PlayerActionChat: TActionAbstractData;
implementation
uses
  GameActionDirectAttack, GameActionPlayerDisarmTrap, GameActionPlayerRescue,
  GameActionPlayerRandomChat, GameActionMove,
  GameNamesGenerator;

function TempPlayerCharacter: TPlayerCharacterData;
begin
  Result := TPlayerCharacterData.Create;
  with Result do
  begin
    DisplayName := GenerateFemaleName;
    Size := 3;
    MaxHealth := 100;
    MaxStamina := 100;
    MaxWill := 100;
    MaxMagic := 0;
    MovementSpeed := 10;
    MoveAction := TActionMove;
    RollSpeed := 30;
    RollRange := 15;
    RollStaminaCost := 20;
    HealthRegen := 1.0;
    MaxHealthRegen := 0.01;
    StaminaRegen := 3.5;
    MaxStaminaRegen := 0.05;
    WillRegen := 1.5;
    MaxWillRegen := 0.01;
    MagicRegen := 0.7;
    MaxMagicRegen := 0.01;
  end;
end;

var
  FPlayerActionAttack: TActionAbstractData;
  FPlayerActionDisarm: TActionAbstractData;
  FPlayerActionRescue: TActionAbstractData;
  FPlayerActionChat: TActionAbstractData;

function PlayerActionAttack: TActionAbstractData;
begin
  if FPlayerActionAttack = nil then
  begin
    FPlayerActionAttack := TActionDirectAttackData.Create;
    FPlayerActionAttack.AttackRange := 3;
    FPlayerActionAttack.SqrAttackRange := Sqr(FPlayerActionAttack.AttackRange);
    FPlayerActionAttack.WarmUpTime := 0.5 - 0.3;
    FPlayerActionAttack.CoolDownTime := 0.5 + 0.3;
  end;
  Exit(FPlayerActionAttack);
end;

function PlayerActionDisarm: TActionAbstractData;
begin
  if FPlayerActionDisarm = nil then
  begin
    FPlayerActionDisarm := TActionPlayerDisarmTrapData.Create;
    FPlayerActionDisarm.AttackRange := 5;
    FPlayerActionDisarm.SqrAttackRange := Sqr(FPlayerActionDisarm.AttackRange);
    FPlayerActionDisarm.WarmUpTime := 0.5;
    FPlayerActionDisarm.CoolDownTime := 1.0;
  end;
  Exit(FPlayerActionDisarm);
end;

function PlayerActionRescue: TActionAbstractData;
begin
  if FPlayerActionRescue = nil then
  begin
    FPlayerActionRescue := TActionPlayerRescueData.Create;
    FPlayerActionRescue.AttackRange := 5;
    FPlayerActionRescue.SqrAttackRange := Sqr(FPlayerActionRescue.AttackRange);
    FPlayerActionRescue.WarmUpTime := 0.5;
    FPlayerActionRescue.CoolDownTime := 1.0;
  end;
  Exit(FPlayerActionRescue);
end;

function PlayerActionChat: TActionAbstractData;
begin
  if FPlayerActionChat = nil then
  begin
    FPlayerActionChat := TActionPlayerRandomChatData.Create;
    FPlayerActionChat.AttackRange := 3.5;
    FPlayerActionChat.SqrAttackRange := Sqr(FPlayerActionChat.AttackRange);
    FPlayerActionChat.WarmUpTime := 0.5;
    FPlayerActionChat.CoolDownTime := 1.0;
  end;
  Exit(FPlayerActionChat);
end;

finalization
  FreeAndNil(FPlayerActionAttack);
  FreeAndNil(FPlayerActionDisarm);
  FreeAndNil(FPlayerActionRescue);
  FreeAndNil(FPlayerActionChat);
end.


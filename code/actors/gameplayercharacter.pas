{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ An actor with inventory, player control - player's character (PC)
  and some other features needed for a playable character
  (doesn't necessarily need to be under player's control,
  can make a full use of AI-only behavior) }
unit GamePlayerCharacter;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, Generics.Collections, DOM,
  CastleTimeUtils,
  GameSerializableObject,
  GameAiAbstract, GameActionMoveAbstract,
  GameActor, GamePlayerCharacterData, GameMapTypes,
  GamePlayerCharacterExperience, GameViewEndGame,
  GameInventory, GameApparelSlots, GameInventoryItem,
  GameBlueprint;

type
  { all heroes are supposed to have two meaningful body halves }
  TBodyHalf = (bhTop, bhBottom);

type
  { Actor with bells and whistles }
  TPlayerCharacter = class(TActor)
  public const
    { Some "default" value for PC's vision range
      it may be significantly different for a specific character blueprint,
      but some external (character-unrelated) procedures
      need to take this value into account (like map generation) }
    DefaultVisionRange = 24;
    { Otherwise will break floodfill on passable tiles,
      Player will have no idea where walls are - which wile makes sense
      isn't good for gameplay. Maybe add something here? TODO}
    AbsoluteMinimumVisionRangeThatDoesntBreakTheGame = 4;
    { visible range below which pathfinding stops making sense }
    VisibleRangeBelowWhichCharacterIsConsideredBlindfolded = 7;
  strict private const
    { Delay between some notifications about this character
      for now: those that happen when the PC changes current
      (hurting feet, walking nude)
      in seconds, randomized }
    NotificationDelay = 30;
  strict private
    FLastSoundTime: TTimerResult;
    FBarefootNotificationDelay: Single;
    FLastBarefootNotification: TTimerResult;
    FNudeNotificationDelay: Single;
    FLastNudeNotification: TTimerResult;
    FLastSoundDuration: Single;
    FSphericalCollisionWithMonstersSpeedMultiplier: Single;
    { if spherical float-collides with any monster
      returns normalized distance to the nearest monster
      or 1.0 if no collision occurs }
    function SphericalCollisionWithMonsters: Single;
    procedure AfterTeleport;
  protected
    { Todo: do PCs even need this? }
    procedure Die; override;
  public
    Ai: TAiAbstract;
  public
    { Contains definitions of the character base stats and features }
    Blueprint: TBlueprint;
    { Contains items this character has + functions related to those,
      including getting special items effects such as enchantment }
    Inventory: TInventory;
    { Contains experience and skills of this character
      recalculates parent's stats }
    Experience: TPlayerCharacterExperience;
    { How many days this character needs to recover
      before will be available for dungeon run
      note as the day updates AFTER the run, set this value to +1 }
    Exhausted: Integer;
    { Stats specific to PC }
    Stamina, MaxStamina: Single;
    Will, MaxWill: Single;
    Magic, MaxMagic: Single;
    procedure HealWill(const AValue: Single);
    procedure HealMaxWill(const AValue: Single);
    procedure HealStamina(const AValue: Single);
    procedure HealMaxStamina(const AValue: Single);
    procedure HealMagic(const AValue: Single);
    procedure HealMaxMagic(const AValue: Single);
    { Reset this character to max stats }
    procedure Reset; override;
    { Deal physical damage to random body half }
    procedure Hit(const Damage: Single); override;
    { Deal physical damage to a specific body half }
    procedure HitSpecific(const Damage: Single; const BodyHalf: TBodyHalf); // TODO: Damage kind
    { Reduce health, and reduce max health accordingly }
    procedure DegradeHealth(const Damage: Single);
    { Damage health directly and show log message }
    procedure HitHealth(const Damage: Single);
    procedure HitStamina(const Damage: Single);
    procedure DegradeStamina(const Damage: Single);
    procedure HitWill(const Damage: Single);
    procedure DegradeWill(const Damage: Single);
    procedure DegradeMagic(const Damage: Single);
    { Damage currently held weapon after it hits target
      Todo: more flexible event on hit (e.g. for bondage items) }
    procedure DamageWeapon; override;
    { The PC changed the tile: it affects the stats
      damages footwear, if none hurts Health
      if nude hurts willpower }
    procedure HurtFeet(const Damage: Single); override;
    function CanAct: Boolean; override;
    function CanBeInteractedWith: Boolean; override;
    function Unsuspecting: Boolean; override;
    function IsPassive: Boolean; override;
  public
    { restore stats with time }
    procedure RegenerateStats(const SecondsPassed: Single);
    { restore max stats with time and testing quality}
    procedure RegenerateMaxStats(const SecondsPassed: Single; const Quality: Single);
    { Calculate willpower regeneration multiplier based on current state and clothes }
    function WillRegenCoefficient(const Quality: Single): Single;
    function MagicRegenCoefficient(const Quality: Single): Single;
  public
    function GetShapePose: String;
  public
    { Play some sounds as this character:
      makes sure that last sounds has finished
      so that vocals won't overlap }
    { Laughing/giggles }
    procedure PlayGiggleSound;
    { Doing something hard }
    procedure PlayGruntSound;
    { Screaming in pain }
    procedure PlayHurtSound;
    { Heavy breathing }
    procedure PlayBreathSound;
    { Surprised "ouch" }
    procedure PlaySurpriseSound;
  public
    function GetDamageMultiplier: Single;
    function GetSpeed: Single; override;
    function MoveAction: TActionMoveClass; override;
    function GetSpeedMultiplier: Single;
    function GetRollSpeed: Single; override;
    function GetRollCost: Single;
    function GetRollCostMultiplier: Single;
    { Total "noisiness" of the character's body and inventory }
    function GetNoise: Single; override;
    function Blindfolded: Boolean;
    function Disoriented: Boolean;
    function CanInteractWith(const ATarget: TActor): Boolean;
    { how effectively can this character resist?
      for now affects only few monsters chance to equip restraints -
      the lower the value, the lower resistance;
      at zero character cannot resist at all
      Note: Immobilized is treated separately (as often it shows a different log) }
    function ResistMultiplier: Single;
    function ResistStun: Single;
    procedure AddResistStun;
    function ResistKnockback: Single;
    procedure AddResistKnockback;
    { Real visible range of this character
      includes all modifiers }
    function VisibleRange: Single;
    { Actor's data typecasted }
    function PlayerCharacterData: TPlayerCharacterData; inline;
  public
    { If this character is a prisoner
      Todo: have it as inventor result - incapacitated, fully restrained}
    IsCaptured: Boolean;
    IsStranger: Boolean;
    { Currently at the dungeon depth }
    AtMap: Byte;
    { Some centralized checks if the character is ready to go into the dungeon }
    function IsAvailableForDungeon: Boolean;
    procedure TemporaryCraftItem;
  public
    //procedure RespawnInRandomPlaceNaked;
    { Temporary: properly mark this character as captured
      Todo: better run end conditions }
    procedure GetCaptured(const CaptureKind: TEndGameKind);
    { Same as GetCaptured, but doesn't call anything except this character functions
      For internal use - e.g. to recover from crash }
    procedure GetCapturedSafe;
    procedure ResetLeashSafe;
    procedure Teleport(const ToX, ToY: Int16); override;
    procedure Teleport(const ToX, ToY: Single); override;
    { (try to) teleport to unexplored map area
      (trying to avoid aggressive monsters around)
      if fails: just teleport to a random spot }
    procedure TeleportToUnknown(const MinTeleportDistanceFraction: Single);
    procedure UpdateVisible; override;
    procedure Update(const SecondsPassed: Single); override;
  public const Signature = 'playerchar'; // deprecated
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    constructor Create(const NotLoading: Boolean = true); override;
    destructor Destroy; override;
  end;
  TPlayerCharactersList = specialize TObjectList<TPlayerCharacter>;

implementation
uses
  CastleXmlUtils,
  GameRandom, GameSounds, GameTranslation, GameMap, GameLog, GameColors,
  GameMonster, GameLeash, GameMath, GameStatusEffect,
  TempData,
  GameAiPlayerSettlement,
  GameEnchantmentSpeedMultiplier, GameEnchantmentDamageMultiplier,
  GameEnchantmentNoiseMultiplier, GameEnchantmentNoiseAddition,
  GameEnchantmentRollCostMultiplier, GameEnchantmentResistMultiplier,
  GameEnchantmentIncomingDamageMultiplier,
  GameEnchantmentIncomingWillDamageMultiplier,
  GameEnchantmentIncomingStaminaDamageMultiplier,
  GameEnchantmentDisorientation,
  GameEnchantmentResistKnockback, GameEnchantmentResistStun,
  GameActionIdle, GameActionPlayerRest, GameActionPlayerUnconscious,
  GameActionMoveWithoutPathfinding,
  GameItemData, GameItemsDatabase, GameMapItem,
  GameViewGame, GameParticle, GameDifficultyLevel, GameBlueprintsDatabase;

type
  EDamageCalculationError = class(Exception);

procedure TPlayerCharacter.Reset;
begin
  LogVerbose('TPlayerCharacter.Reset');
  Experience.Recalculate;
  inherited;
  MaxStamina := PlayerCharacterData.MaxStamina;
  Stamina := MaxStamina;
  MaxWill := PlayerCharacterData.MaxWill;
  Will := MaxWill;
  MaxMagic := PlayerCharacterData.MaxMagic;
  Magic := MaxMagic;
  //Exhausted := 0;
  Inventory.StatusEffects.Clear;
  Inventory.InvalidateInventory;
end;

procedure TPlayerCharacter.Hit(const Damage: Single);
var
  FinalDamage: Single;
begin
  //inherited Hit(Damage); ---- different logic here
  FinalDamage := ClampMin(Damage * Inventory.FindEffectMultiplier(TEnchantmentIncomingDamageMultiplier), 0.5);
  HitSpecific(FinalDamage, TBodyHalf(Rnd.Random(Ord(High(TBodyHalf)) + 1)));
end;

{$PUSH} // PUSH-POP work only on a whole method, they don't work locally for some reason
procedure TPlayerCharacter.HitSpecific(const Damage: Single; const BodyHalf: TBodyHalf);
var
  RemainingDamage: Single;
  AbsorbedDamage: Single;
  DamageAbsorbers: TInventoryItemsList;
  WillDamage: Single;
  Item: TInventoryItem;
  E: TApparelSlot;
begin
  RemainingDamage := Damage;

  DamageAbsorbers := TInventoryItemsList.Create(false);
  {$WARN 6018 OFF}
  case BodyHalf of
    // note protection > 0 ---- weapons/restraints have protection = 0, other items shouldn't, otherwise they'll be indestructible
    bhTop: begin
         for E in Blueprint.EquipmentSlots do
           if (Inventory.Apparel[E] <> nil) and (Inventory.Apparel[E].Data.MainSlot = E) and (Inventory.Equipped[E].ItemData.ProtectionTop > 0) then
             DamageAbsorbers.Add(Inventory.Equipped[E]);
      end;
    bhBottom: begin
         for E in Blueprint.EquipmentSlots do
           if (Inventory.Apparel[E] <> nil) and (Inventory.Apparel[E].Data.MainSlot = E) and (Inventory.Equipped[E].ItemData.ProtectionBottom > 0) then
             DamageAbsorbers.Add(Inventory.Equipped[E]);
      end;
    else
      raise EDamageCalculationError.CreateFmt('unexpected BodyHalf %s', [BodyHalf]);
  end;

  for Item in DamageAbsorbers do
    begin
      case BodyHalf of
        bhTop:
          AbsorbedDamage := RemainingDamage * Item.ItemData.ProtectionTop;
        bhBottom:
          AbsorbedDamage := RemainingDamage * Item.ItemData.ProtectionBottom;
        else
          raise EDamageCalculationError.CreateFmt('unexpected BodyHalf %s', [BodyHalf]);
      end;
      RemainingDamage -= AbsorbedDamage;
      if (Self = ViewGame.CurrentCharacter) and (AbsorbedDamage > 0.05) then
        ShowLog(GetTranslation('ArmorAbsorbsDamageLog'), [Item.Data.DisplayName, AbsorbedDamage], ColorLogAbsorbDamage);
      Inventory.DamageItem(Item, AbsorbedDamage);
    end;

  FreeAndNil(DamageAbsorbers);

  DegradeHealth(RemainingDamage);
  if Self = ViewGame.CurrentCharacter then
  begin
    if Abs(RemainingDamage - Damage) < 0.1 then
      ShowLog(GetTranslation('PlayerReceivedUnabsorbedDamageLog'), [FData.DisplayName, RemainingDamage], ColorLogUnabsorbedDamage)
    else
      ShowLog(GetTranslation('PlayerReceivedAbsorbedDamageLog'), [FData.DisplayName, RemainingDamage, Round(100 * RemainingDamage / Damage)], ColorLogAbsorbedDamage);
  end;

  if Health <= 0 then
  begin
    Die;
    Exit;
  end;

  WillDamage := Difficulty.LowHealthWillpowerDamage * RemainingDamage / MaxHealth;

  //Inventory.UpdateCoveredStatus; No need to, if an item was broken, then it was already force-recalculated in UnequipAndReturn
  if (Inventory.TopCovered = 0) and (BodyHalf = bhTop) then
  begin
    if Inventory.TouchMultiplier > 0.5 then
      ShowLog(GetTranslation('PlayerReceivedNakedTouchTop'), [FData.DisplayName], ColorLogNaked);
    WillDamage += Difficulty.HitNakedWillpowerDamage * Inventory.TouchMultiplier;
  end;
  if (Inventory.BottomCovered = 0) and (BodyHalf = bhBottom) then
  begin
    if Inventory.TouchMultiplier > 0.5 then
      ShowLog(GetTranslation('PlayerReceivedNakedTouchBottom'), [FData.DisplayName], ColorLogNaked);
    WillDamage += Difficulty.HitNakedWillpowerDamage * Inventory.TouchMultiplier;
  end;
  HitWill(WillDamage);

  ViewGame.WakeUp(true, true);
  Particle(Round(Damage).ToString, ColorParticlePlayerHurt);
end;
{$POP}

procedure TPlayerCharacter.DegradeHealth(const Damage: Single);
var
  DamageResist: TStatusEffect;
begin
  if Damage >= 0 then
  begin
    if Damage / Health > 0.25 then
      PlayHurtSound;
    if Damage / Health > 0.05 then
    begin
      DamageResist := TStatusEffect.Create;
      DamageResist.Timeout := 0.7;
      DamageResist.Effect := TEnchantmentIncomingDamageMultiplier.Create;
      DamageResist.Effect.Strength := Clamp(1 - Damage / Health, 0.01, 0.99);
      Inventory.AddStatusEffect(DamageResist);
    end;
    Health -= Damage;
    MaxHealth -= Damage * Difficulty.StatsDegradationSpeed;
  end else
    ShowError('Cannot degrade health by negative value %.1n', [Damage]);
end;

procedure TPlayerCharacter.HitHealth(const Damage: Single);
var
  FinalDamage: Single;
begin
  FinalDamage := ClampMin(Damage * Inventory.FindEffectMultiplier(TEnchantmentIncomingDamageMultiplier), 0.5);
  if (FinalDamage > MaxHealth / 100.0) and (FinalDamage > 0.5) then
    ShowLog('%s lost %d health', [FData.DisplayName, Round(FinalDamage)], ColorLogUnabsorbedDamage);
  DegradeHealth(FinalDamage);
  Particle(Round(FinalDamage).ToString, ColorParticlePlayerHurt);
end;

procedure TPlayerCharacter.HitStamina(const Damage: Single);
var
  PreviousStamina: Single;
  FinalDamage: Single;
begin
  FinalDamage := ClampMin(Damage * Inventory.FindEffectMultiplier(TEnchantmentIncomingStaminaDamageMultiplier), 0.5);
  if (FinalDamage > MaxStamina / 100.0) and (FinalDamage > 0.5) then
    ShowLog('%s lost %d stamina', [FData.DisplayName, Round(FinalDamage)], ColorLogStaminaDamage);
  PreviousStamina := Stamina;
  DegradeStamina(FinalDamage);
  if (PreviousStamina > 0) and (Stamina < 0) then
    ShowLog('%s is exhausted and can barely stand on her feet', [FData.DisplayName], ColorLogStaminaExhausted);
end;

procedure TPlayerCharacter.DegradeStamina(const Damage: Single);
begin
  if Damage >= 0 then
  begin
    Stamina -= Damage;
    MaxStamina -= Damage * Difficulty.StatsDegradationSpeed;
  end else
    ShowError('Cannot degrade stamina by negative value %.1n', [Damage]);
end;

procedure TPlayerCharacter.HitWill(const Damage: Single);
var
  FinalDamage: Single;
begin
  FinalDamage := ClampMin(Damage * Inventory.FindEffectMultiplier(TEnchantmentIncomingWillDamageMultiplier), 0.5);
  if (FinalDamage > MaxWill / 100.0) and (FinalDamage > 0.5) then
    ShowLog('%s lost %d willpower', [FData.DisplayName, Round(FinalDamage)], ColorLogWillDamage);
  DegradeWill(FinalDamage);
end;

procedure TPlayerCharacter.DegradeWill(const Damage: Single);
var
  DamageResist: TStatusEffect;
begin
  if Damage >= 0 then
  begin
    if Damage / Will > 0.05 then
    begin
      DamageResist := TStatusEffect.Create;
      DamageResist.Timeout := 0.7;
      DamageResist.Effect := TEnchantmentIncomingWillDamageMultiplier.Create;
      DamageResist.Effect.Strength := Clamp(1 - Damage / Will, 0.01, 0.99);
      Inventory.AddStatusEffect(DamageResist);
    end;
    Will -= Damage;
    MaxWill -= Damage * Difficulty.StatsDegradationSpeed;
  end else
    ShowError('Cannot degrade willpower by negative value %.1n', [Damage]);
end;

procedure TPlayerCharacter.DegradeMagic(const Damage: Single);
begin
  if Damage >= 0 then
  begin
    Magic -= Damage;
    MaxMagic -= Damage * Difficulty.StatsDegradationSpeed;
  end else
    ShowError('Cannot degrade Magic by negative value %.1n', [Damage]);
end;

procedure TPlayerCharacter.DamageWeapon;
begin
  inherited DamageWeapon;
  Inventory.DamageWeapon;
end;

procedure TPlayerCharacter.HurtFeet(const Damage: Single);
var
  WillDamageMultiplier: Single;
begin
  if Map.CurrentDepth = 0 then Exit; // no hurting feet in settlement
  inherited;

  // walking barefoot
  if Inventory.HurtFeet(Damage) then
  begin
    if FLastBarefootNotification.ElapsedTime > FBarefootNotificationDelay then
    begin
      if FLastBarefootNotification.ElapsedTime < FBarefootNotificationDelay + 2 then // avoid showing the log instantly
        ShowLog('%s stepped on a sharp stone hurting her foot', [Data.DisplayName], ColorLogUnabsorbedDamage);
      FBarefootNotificationDelay := Sqrt(Rnd.Random) * NotificationDelay;
      FLastBarefootNotification := Timer;
    end;
    if Health > Blueprint.WalkFeetDamageMultiplier * Damage / Difficulty.StatsDegradationSpeed + 1 then
      DegradeHealth(Blueprint.WalkFeetDamageMultiplier * Damage / Difficulty.StatsDegradationSpeed)
    else
    if Health > 1 then
      DegradeHealth(Health - 1);
  end;

  // walking around nude
  if Inventory.PartiallyOrFullyNude then
  begin
    if (FLastNudeNotification.ElapsedTime > FNudeNotificationDelay) then
    begin
      if FLastNudeNotification.ElapsedTime < FNudeNotificationDelay + 2 then // avoid showing the log instantly
      begin
        if Inventory.EmbarrasmentMultiplier <= 0.5 then
          case Rnd.Random(5) of
            0: ShowLog('%s feels oddly confident about how she looks', [Data.DisplayName], ColorLogUnabsorbedDamage);
            1: ShowLog('Surprised %s feels even a bit excited walking like that', [Data.DisplayName], ColorLogUnabsorbedDamage);
            2: ShowLog('%s wonders why she was avoiding naked walks before', [Data.DisplayName], ColorLogUnabsorbedDamage);
            3: ShowLog('%s can''t help but enjoy air flowing around her sensitive bits', [Data.DisplayName], ColorLogUnabsorbedDamage);
            else ShowLog('%s definitely wants to try something like that again', [Data.DisplayName], ColorLogUnabsorbedDamage);
          end
        else
        if Inventory.Nude then
          case Rnd.Random(4) of
            0: ShowLog('%s has an eerie feeling as if she''s being watched', [Data.DisplayName], ColorLogUnabsorbedDamage);
            1: ShowLog('%s feels awkward walking around... like that', [Data.DisplayName], ColorLogUnabsorbedDamage);
            2: ShowLog('%s mumbles to herself that there is nobody here to see her like that', [Data.DisplayName], ColorLogUnabsorbedDamage);
            else ShowLog('%s feels really uneasy walking around completely naked', [Data.DisplayName], ColorLogUnabsorbedDamage);
          end
        else
        if (Inventory.TopCovered = 0) then
          case Rnd.Random(3) of
            0: ShowLog('%s can''t help but notice her breasts jiggle with every step', [Data.DisplayName], ColorLogUnabsorbedDamage);
            1: ShowLog('%s is unable to stop thinking she''s barechested', [Data.DisplayName], ColorLogUnabsorbedDamage);
            else ShowLog('%s has a weird sensation on her exposed nipples', [Data.DisplayName], ColorLogUnabsorbedDamage);
          end
        else
        if (Inventory.BottomCovered = 0) then
          case Rnd.Random(4) of
            0: ShowLog('%s senses like her exposed crotch is literally asking for trouble', [Data.DisplayName], ColorLogUnabsorbedDamage);
            1: ShowLog('%s feels unnaturally warm down there', [Data.DisplayName], ColorLogUnabsorbedDamage);
            2: ShowLog('%s senses as cold dungeon air tingles her sensitive bits', [Data.DisplayName], ColorLogUnabsorbedDamage);
            else ShowLog('%s is very uncomfortable walking with her bottom exposed', [Data.DisplayName], ColorLogUnabsorbedDamage);
          end
      end;
      FNudeNotificationDelay := Sqrt(Rnd.Random) * NotificationDelay;
      FLastNudeNotification := Timer;
    end;
    if Inventory.Nude then
      WillDamageMultiplier := 0.5
    else
    if (Inventory.TopCovered = 0) then
      WillDamageMultiplier := 0.2
    else
    if (Inventory.BottomCovered = 0) then
      WillDamageMultiplier := 0.3
    else
      raise Exception.Create('Unexpected situation calculating WillDamageMultiplier in TPlayerCharacter.HurtFeet');

    WillDamageMultiplier *= Inventory.EmbarrasmentMultiplier;

    if Will > WillDamageMultiplier * Damage / Difficulty.StatsDegradationSpeed + 1 then
      DegradeWill(WillDamageMultiplier * Damage / Difficulty.StatsDegradationSpeed)
    else
    if Will > 1 then
      DegradeWill(Will - 1);
  end;
end;

function TPlayerCharacter.GetDamageMultiplier: Single;
begin
  Exit(Inventory.FindEffectMultiplier(TEnchantmentDamageMultiplier));
end;

function TPlayerCharacter.GetSpeed: Single;
begin
  Exit((inherited GetSpeed) * GetSpeedMultiplier * FSphericalCollisionWithMonstersSpeedMultiplier);
end;

function TPlayerCharacter.GetSpeedMultiplier: Single;
begin
  Exit(Inventory.FindEffectMultiplier(TEnchantmentSpeedMultiplier));
end;

function TPlayerCharacter.GetRollSpeed: Single;
begin
  Exit((inherited GetRollSpeed) * FSphericalCollisionWithMonstersSpeedMultiplier);
end;

function TPlayerCharacter.GetRollCostMultiplier: Single;
begin
  Exit(Inventory.FindEffectMultiplier(TEnchantmentRollCostMultiplier));
end;

function TPlayerCharacter.GetNoise: Single;
begin
  Exit(ClampMin(
      (Blueprint.BodyNoise + Inventory.TotalIdleNoise * Inventory.FindEffectMultiplier(TEnchantmentNoiseMultiplier))
      * CurrentAction.NoiseMultiplier + CurrentAction.NoiseAddition
      + Inventory.FindEffectAdditive(TEnchantmentNoiseAddition),
    Blueprint.BodyNoise));
end;

function TPlayerCharacter.Blindfolded: Boolean;
begin
  // blindfolded status means both can't see far and can't remember map
  Exit((VisibleRange < VisibleRangeBelowWhichCharacterIsConsideredBlindfolded) and Disoriented);
end;

function TPlayerCharacter.Disoriented: Boolean;
begin
  Exit(Inventory.FindEffectAdditive(TEnchantmentDisorientation) > 0);
end;

function TPlayerCharacter.CanInteractWith(const ATarget: TActor): Boolean;
begin
  // this may be a heavy operation (especially as it's called in a loop) but hopefully will only occasionally contain 1 element, so shouldn't be too big deal
  Exit(not Inventory.IgnoredEntities.Contains(ATarget.ReferenceId) and (Inventory.IgnoredTypes.IndexOf(ATarget.Data.Id) < 0));
end;

function TPlayerCharacter.ResistMultiplier: Single;
begin
  Exit(
    Stamina / PlayerCharacterData.MaxStamina
    * Inventory.FindEffectMultiplier(TEnchantmentResistMultiplier)
  );
end;

function TPlayerCharacter.ResistStun: Single;
begin
  Exit(Inventory.FindEffectAdditive(TEnchantmentResistStun));
end;

procedure TPlayerCharacter.AddResistStun;
var
  ReistStatusEffect: TStatusEffect;
begin
  ReistStatusEffect := TStatusEffect.Create;
  ReistStatusEffect.Timeout := 1.0;
  ReistStatusEffect.Effect := TEnchantmentResistStun.Create;
  ReistStatusEffect.Effect.Strength := 0.5;
  Inventory.AddStatusEffect(ReistStatusEffect);
end;

function TPlayerCharacter.ResistKnockback: Single;
begin
  Exit(Inventory.FindEffectAdditive(TEnchantmentResistKnockback));
end;

procedure TPlayerCharacter.AddResistKnockback;
var
  ReistStatusEffect: TStatusEffect;
begin
  ReistStatusEffect := TStatusEffect.Create;
  ReistStatusEffect.Timeout := 1.0;
  ReistStatusEffect.Effect := TEnchantmentResistKnockback.Create;
  ReistStatusEffect.Effect.Strength := 0.5;
  Inventory.AddStatusEffect(ReistStatusEffect);
end;

function TPlayerCharacter.GetRollCost: Single;
begin
  Result := PlayerCharacterData.RollStaminaCost * GetRollCostMultiplier;
end;

function TPlayerCharacter.CanAct: Boolean;
begin
  Exit(not IsCaptured and inherited and (Will > 0));
end;

function TPlayerCharacter.CanBeInteractedWith: Boolean;
begin
  //Result := inherited CanBeInteractedWith;
  Result := true; // always? TODO
end;

function TPlayerCharacter.Unsuspecting: Boolean;
begin
  Exit((CurrentAction is TActionPlayerRest) or (CurrentAction is TActionPlayerUnconscious));
end;

function TPlayerCharacter.IsPassive: Boolean;
begin
  Exit((CurrentAction is TActionPlayerRest) or inherited);
end;

procedure TPlayerCharacter.PlayGiggleSound;
begin
  if (FLastSoundTime.ElapsedTime > FLastSoundDuration) and not Unsuspecting then
  begin
    Self.FLastSoundTime := Timer;
    FLastSoundDuration := SoundWithDuration('female_giggle');
  end;
end;

procedure TPlayerCharacter.PlayGruntSound;
begin
  if (FLastSoundTime.ElapsedTime > FLastSoundDuration) and not Unsuspecting then
  begin
    Self.FLastSoundTime := Timer;
    FLastSoundDuration := SoundWithDuration('female_grunt');
  end;
end;

procedure TPlayerCharacter.PlayHurtSound;
begin
  if (FLastSoundTime.ElapsedTime > FLastSoundDuration) and not Unsuspecting then
  begin
    Self.FLastSoundTime := Timer;
    FLastSoundDuration := SoundWithDuration('female_hurt');
  end;
end;

procedure TPlayerCharacter.PlayBreathSound;
begin
  if (FLastSoundTime.ElapsedTime > FLastSoundDuration) and not Unsuspecting then
  begin
    Self.FLastSoundTime := Timer;
    FLastSoundDuration := SoundWithDuration('female_breath');
  end;
end;

procedure TPlayerCharacter.PlaySurpriseSound;
begin
  if (FLastSoundTime.ElapsedTime > FLastSoundDuration) and not Unsuspecting then
  begin
    Self.FLastSoundTime := Timer;
    FLastSoundDuration := SoundWithDuration('female_surprise');
  end;
end;

procedure TPlayerCharacter.Die;
begin
  // leave it here for now? let the monsters properly check if target needs to be attacked
  // ShowLog(GetTranslation('PlayerBlacksOutLog'), [FData.DisplayName], ColorLogDefeat);
end;

function TPlayerCharacter.MoveAction: TActionMoveClass;
begin
  // TODO: may confuse non-current characters, most likely Inventory.Blindfolded needs to affect only current one; non-currents need to have their AI take care of this parameter
  if {Inventory.Blindfolded or} ((ViewGame.CurrentCharacter = Self) and ViewGame.PathfindingDisabled) then
    Result := TActionMoveWithoutPathfinding
  else
    Result := inherited MoveAction;
end;

procedure TPlayerCharacter.HealWill(const AValue: Single);
begin
  if Will + AValue < MaxWill then
    Will := Will + AValue
  else
    Will := MaxWill;
end;

procedure TPlayerCharacter.HealMaxWill(const AValue: Single);
begin
  if MaxWill + AValue < PlayerCharacterData.MaxWill then
    MaxWill := MaxWill + AValue
  else
    MaxWill := PlayerCharacterData.MaxWill;
end;

procedure TPlayerCharacter.HealStamina(const AValue: Single);
begin
  if Stamina + AValue < MaxStamina then
    Stamina := Stamina + AValue
  else
    Stamina := MaxStamina;
end;

procedure TPlayerCharacter.HealMaxStamina(const AValue: Single);
begin
  if MaxStamina + AValue < PlayerCharacterData.MaxStamina then
    MaxStamina := MaxStamina + AValue
  else
    MaxStamina := PlayerCharacterData.MaxStamina;
end;

procedure TPlayerCharacter.HealMagic(const AValue: Single);
begin
  if Magic + AValue < MaxMagic then
    Magic := Magic + AValue
  else
    Magic := MaxMagic;
end;

procedure TPlayerCharacter.HealMaxMagic(const AValue: Single);
begin
  if MaxMagic + AValue < PlayerCharacterData.MaxMagic then
    MaxMagic := MaxMagic + AValue
  else
    MaxMagic := PlayerCharacterData.MaxMagic;
end;

function TPlayerCharacter.SphericalCollisionWithMonsters: Single;
var
  M: TMonster;
  D: Single;
begin
  Result := 1.0;
  for M in Map.MonstersList do
    if M.CanAct and M.Aggressive then
    begin
      D := M.SphericalDistanceNormalizedSqr(Self);
      if D < Result then
        Result := D;
    end;
end;

procedure TPlayerCharacter.AfterTeleport;
begin
  if not IsCaptured then
    UpdateVisible;
  if Self = ViewGame.CurrentCharacter then
    ViewGame.StopShakers;
  if Inventory.Apparel[esLeash] <> nil then
    (Inventory.Apparel[esLeash] as TLeash).MaybeReleaseLeashByDistance;
end;

procedure TPlayerCharacter.UpdateVisible;
begin
  inherited; // parent is empty
  //Map.LookAround(LastTileX + PredSize div 2, LastTileY + PredSize div 2);
  Map.ScheduleUpdateVisible;
end;

procedure TPlayerCharacter.Teleport(const ToX, ToY: Int16);
begin
  inherited;
  AfterTeleport;
end;

procedure TPlayerCharacter.Teleport(const ToX, ToY: Single);
begin
  inherited;
  AfterTeleport
end;

procedure TPlayerCharacter.TeleportToUnknown(const MinTeleportDistanceFraction: Single);
const
  MaxRetryCountInvisible = Integer(10000);
  MaxRetryCountMonsters = Integer(3000);
var
  AX, AY: Int16;
  RetryCount: Integer;
  DistanceSeed: TIntCoordList;
  DistanceMap: TDistanceMapArray;
  MaxDistance: TDistanceQuant;
begin
  DistanceSeed := TIntCoordList.Create;
  // TODO: cycle all active players
  AddSeed(DistanceSeed);
  DistanceSeed.Add(IntCoord(Map.ExitX, Map.ExitY));
  DistanceMap := Map.DistanceMap(PredSize, true, DistanceSeed);
  MaxDistance := Map.MaxDistance(DistanceMap);
  FreeAndNil(DistanceSeed);

  RetryCount := 0;
  repeat
    repeat
      AX := Rnd.Random(Map.SizeX);
      AY := Rnd.Random(Map.SizeY);
    until (Map.PassableTiles[PredSize][AX + Map.SizeX * AY]) and (Sqr(AX - Map.ExitX) + Sqr(AY - Map.ExitY) > Sqr(16)) and (DistanceMap[AX + Map.SizeX * AY] > MaxDistance * MinTeleportDistanceFraction);
    Inc(RetryCount);
  until ((Map.Visible[AX + Map.SizeX * AY] = 0) or (RetryCount > MaxRetryCountInvisible)) and (Map.NoMonstersNearby(AX, AY, Size) or (RetryCount > MaxRetryCountMonsters));
  DistanceMap := nil;
  Teleport(AX, AY);
  if ViewGame.CurrentCharacter = Self then
    ViewGame.InternalMapInterface.TeleportTo(AX, AY);
end;

function TPlayerCharacter.PlayerCharacterData: TPlayerCharacterData; inline;
begin
  Exit({$IFDEF SafeTypecast}FData as TPlayerCharacterData{$ELSE}TPlayerCharacterData(FData){$ENDIF});
end;

function TPlayerCharacter.IsAvailableForDungeon: Boolean;
begin
  Exit(not IsCaptured and (Exhausted <= 0) and (AtMap = 0)); //AtMap is redundant? Exhausted should only matter in settlement
end;

procedure TPlayerCharacter.TemporaryCraftItem;
const
  Retries = 1;
var
  Count: Integer;
  LastItemData, NewItemData: TItemData;
begin
  if Inventory.Nude then
  begin
    ShowLog('%s finds herself at least some rags', [Data.DisplayName], ColorLogInventory);
    Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['sandals']));
    Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['rags-top']));
    Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['rags-bottom']));
    Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['stick']));
    Exit;
  end;
  if Experience.Level = 0 then
    Exit; // newbies can't craft anything - TODO should be something smarter
  Count := 0;
  LastItemData := nil;
  repeat
    NewItemData := ItemsDataList[Rnd.Random(ItemsDataList.Count)];
    if (not NewItemData.Bondage) and (NewItemData.StartSpawningAtDepth <= 3.0 * Sqrt(Experience.Level)) then
    begin
      Inc(Count);
      if (LastItemData = nil) or (NewItemData.StartSpawningAtDepth > LastItemData.StartSpawningAtDepth) then
        LastItemData := NewItemData;
    end;
  until Count >= Retries;
  ShowLog('%s crafts %s', [Data.DisplayName, LastItemData.DisplayName], ColorLogInventory);
  TMapItem.DropItem(LastTileX, LastTileY, TInventoryItem.NewItem(LastItemData), true);
end;

procedure TPlayerCharacter.GetCaptured(const CaptureKind: TEndGameKind);

  procedure MoveCapturedCharacterIfNecessary;
  var
    NewDungeonLevel: Byte;
  begin
    NewDungeonLevel := AtMap;
    // note: the current character is not yet marked as "captured" so condition below is ">=", not ">"
    while (Map.CapturedCharacterOnLevel(NewDungeonLevel) >= Difficulty.MaxPrisonersAtLevel(NewDungeonLevel)) and (NewDungeonLevel > 0) do
      Dec(NewDungeonLevel);
    if (NewDungeonLevel > 0) and (NewDungeonLevel <> AtMap) then
    begin
      AtMap := NewDungeonLevel;
      ShowLog('To avoid logistic inconveniences the prisoner was moved to dungeon level %d', [ViewGame.CurrentCharacter.AtMap], ColorLogCaptured);
    end;
    // NOTE: the player may still accumulate multiple captured character on early levels. This issue might need to be addressed better to avoid new players being overwhelmed before understanding how the game works. TODO
  end;

begin
  ResetLeashSafe;
  ShowLog('%s was captured at dungeon level %d', [ViewGame.CurrentCharacter.Data.DisplayName, ViewGame.CurrentCharacter.AtMap], ColorLogCaptured);
  MoveCapturedCharacterIfNecessary;

  IsCaptured := true;
  CurrentAction := TActionIdle.NewAction(Self);
  CurrentAction.Start;
  Inventory.UnequipAndDisintegrateEverything;
  ViewGame.ScheduleEndGame;
  ViewEndGame.EndGameKind := CaptureKind;
end;

procedure TPlayerCharacter.GetCapturedSafe;
begin
  ResetLeashSafe;
  IsCaptured := true;
  CurrentAction := TActionIdle.NewAction(Self);
  CurrentAction.Start;
  Inventory.UnequipAndDisintegrateEverything;
end;

procedure TPlayerCharacter.ResetLeashSafe;
begin
  if Inventory.Apparel[esLeash] <> nil then
    (Inventory.Apparel[esLeash] as TLeash).LeashHolder := nil;
  // note: no notification about leash release here:
  // 1. Leash holder is most likely a dangling pointer
  // 2. Leash wearer is in uncertain state (maybe captured? Did CurrentCharacter update to a random free one?)
end;

function TPlayerCharacter.WillRegenCoefficient(const Quality: Single): Single;
begin
  // sleeping peaks Will regen at 100%
  Result := 1.0;
  if Inventory.TopCovered = 0 then
    Result := Result / 1.5;
  if Inventory.BottomCovered = 0 then
    Result := Result / 3.5;

  // Bonus for sleeping quality
  if Result < Quality - 1.0 then
  begin
    Result := Quality - 1.0;
    if Result > 1.0 then
      Result := 1.0;
  end;
end;

function TPlayerCharacter.MagicRegenCoefficient(const Quality: Single): Single;
begin
  // sleeping peaks Will regen at 100%
  Result := 1.0;
  if Inventory.TopCovered > 0 then
    Result := Result / 1.5;
  if Inventory.BottomCovered > 0 then
    Result := Result / 3.5;

  // Bonus for sleeping quality
  if Result < Quality - 1.0 then
  begin
    Result := Quality - 1.0;
    if Result > 1.0 then
      Result := 1.0;
  end;
end;

function TPlayerCharacter.GetShapePose: String;
begin
  {$Warning 'Implement shape-pose properly'}
  Exit('default:default');
end;

procedure TPlayerCharacter.RegenerateMaxStats(const SecondsPassed: Single; const Quality: Single);
var
  Efficiency: Single;
begin
  Efficiency := SecondsPassed * Quality;

  { I'm not sure about MaxStat < 0 part,
    of course we can do
    (MaxStat < 0) and (Stat > MaxStat * 1.01)
    but this doesn't look like a good solution either
    for now it's causing problems only with stamina
    but in future capture logic it will need to be addressed properly }

  if (Health > MaxHealth * 0.99) or (MaxHealth < 0) then
  begin
    MaxHealth += Efficiency * PlayerCharacterData.MaxHealthRegen;
    if MaxHealth > PlayerCharacterData.MaxHealth then
      MaxHealth := PlayerCharacterData.MaxHealth;
  end;

  if (Stamina > MaxStamina * 0.99) or (MaxStamina < 0) then
  begin
    MaxStamina += Efficiency * PlayerCharacterData.MaxStaminaRegen;
    if MaxStamina > PlayerCharacterData.MaxStamina then
      MaxStamina := PlayerCharacterData.MaxStamina;
  end;

  if (Will > MaxWill * 0.99) or (MaxWill < 0) then
  begin
    MaxWill += Efficiency * WillRegenCoefficient(Quality) * PlayerCharacterData.MaxWillRegen;
    if MaxWill > PlayerCharacterData.MaxWill then
      MaxWill := PlayerCharacterData.MaxWill;
  end;

  if (Magic > MaxMagic * 0.99) or (MaxMagic < 0) then
  begin
    MaxMagic += Efficiency * MagicRegenCoefficient(Quality) * PlayerCharacterData.MaxMagicRegen;
    if MaxMagic > PlayerCharacterData.MaxMagic then
      MaxMagic := PlayerCharacterData.MaxMagic;
  end;
end;

procedure TPlayerCharacter.RegenerateStats(const SecondsPassed: Single);
begin
  Health += SecondsPassed * PlayerCharacterData.HealthRegen;
  if Health > MaxHealth then
    Health := MaxHealth;

  Stamina += SecondsPassed * PlayerCharacterData.StaminaRegen;
  if Stamina > MaxStamina then
    Stamina := MaxStamina;

  Will += SecondsPassed * WillRegenCoefficient(0) * PlayerCharacterData.WillRegen;
  if Will > MaxWill then
    Will := MaxWill;

  Magic += SecondsPassed * MagicRegenCoefficient(0) * PlayerCharacterData.MagicRegen;
  if Magic > MaxMagic then
    Magic := MaxMagic;
end;

procedure TPlayerCharacter.Update(const SecondsPassed: Single);
var
  FSphericalCollisionWithMonsters: Single;
begin
  // cache collision with monsters
  FSphericalCollisionWithMonsters := SphericalCollisionWithMonsters;
  if FSphericalCollisionWithMonsters < 1.0 then
    FSphericalCollisionWithMonstersSpeedMultiplier := 0.1 + 0.9 * Sqrt(Sqrt(FSphericalCollisionWithMonsters))
  else
    FSphericalCollisionWithMonstersSpeedMultiplier := 1.0;

  // WARNING: Temporary.
  { In a normal flow the player character should be forced into unconscious/panic state
    and get captured only after restrained properly and all characters are defeated.
    +So in a team the chars can be rescued }
  if (Will <= 0) and not IsCaptured then
  begin
    ShowLog(GetTranslation('PlayerMentalBreakdown'), [FData.DisplayName], ColorLogDefeat);
    GetCaptured(egMental);
    Exit;
  end;
  if (Health <= 0) and not IsCaptured then
  begin
    ShowLog(GetTranslation('PlayerBlacksOutLog'), [FData.DisplayName], ColorLogDefeat);
    GetCaptured(egHealth);
    Exit;
  end;

  if not CanAct then
    Exit;

  RegenerateStats(SecondsPassed);
  RegenerateMaxStats(SecondsPassed, 1.0);

  if ViewGame.CurrentCharacter <> Self then
    Ai.Update(SecondsPassed);

  inherited;

  Inventory.Update(SecondsPassed);
end;

function TPlayerCharacter.VisibleRange: Single;
begin
   Exit(MaxSingle(Blueprint.VisionRange + Inventory.VisionBonus, AbsoluteMinimumVisionRangeThatDoesntBreakTheGame));
end;

procedure TPlayerCharacter.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('TEMPORARY-NAME', FData.DisplayName);
  Element.AttributeSet('Blueprint', Blueprint.Id);
  Element.AttributeSet('IsCaptured', IsCaptured);
  Element.AttributeSet('IsStranger', IsStranger);
  Element.AttributeSet('AtMap', AtMap);
  Element.AttributeSet('Vigor', Stamina);
  Element.AttributeSet('MaxVigor', MaxStamina);
  Element.AttributeSet('Psyche', Will);
  Element.AttributeSet('MaxPsyche', MaxWill);
  Element.AttributeSet('Enigma', Magic);
  Element.AttributeSet('MaxEnigma', MaxMagic);
  Element.AttributeSet('Exhausted', Exhausted);
  Inventory.Save(Element); // Element.CreateChild(TInventory.ClassName)
  Experience.Save(Element.CreateChild(TPlayerCharacterExperience.Signature));
end;

procedure TPlayerCharacter.Load(const Element: TDOMElement);
begin
  inherited;
  Data := TempPlayerCharacter; // TEMPORARY TODO
  Blueprint := BlueprintsDictionary[Element.AttributeStringDef('Blueprint', 'vulpine-female')];
  Data.DisplayName := Element.AttributeString('TEMPORARY-NAME');
  IsCaptured := Element.AttributeBoolean('IsCaptured');
  IsStranger := Element.AttributeBooleanDef('IsStranger', false);
  AtMap := Element.AttributeInteger('AtMap');
  Stamina := Element.AttributeSingle('Vigor');
  MaxStamina := Element.AttributeSingle('MaxVigor');
  Will := Element.AttributeSingle('Psyche');
  MaxWill := Element.AttributeSingle('MaxPsyche');
  Magic := Element.AttributeSingle('Enigma');
  MaxMagic := Element.AttributeSingle('MaxEnigma');
  Exhausted := Element.AttributeIntegerDef('Exhausted', 0); // todo: not def
  Inventory.Load(Element); //Element.Child(TInventory.ClassName, true)
  Experience.Load(Element.Child(TPlayerCharacterExperience.Signature, true));
  SetSize(FData.Size);
  FLastSoundDuration := 0;
end;

constructor TPlayerCharacter.Create(const NotLoading: Boolean = true);
begin
  inherited;
  Blueprint := BlueprintsList[Rnd.Random(BlueprintsList.Count)];  //TODO: just not to crash
  Experience := TPlayerCharacterExperience.Create;
  Experience.Parent := Self;
  Inventory := TInventory.Create;
  Inventory.Parent := Self;
  Inventory.CreatePaperDoll; {$WARNING: bad place to do it? Schedule until after loading}
  IsCaptured := false;
  IsStranger := false;
  AtMap := 0;
  FLastSoundDuration := 0;
  Ai := TAiPlayerSettlement.Create;
  Ai.Parent := Self;
  Ai.Data := TAiPlayerSettlementData.Create;
end;

destructor TPlayerCharacter.Destroy;
begin
  FreeAndNil(Inventory);
  FreeAndNil(Experience);
  FreeAndNil(FData);
  FreeAndNil(Ai.Data); // TODO: Wait, why?
  FreeAndNil(Ai);
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TPlayerCharacter);
end.


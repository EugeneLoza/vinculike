{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Container for items and bodyparts + methids to handle those
  + a lot of other gameplay related functions }
unit GameInventory;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM, Generics.Collections,
  CastleColors,
  GameSerializableObject,
  GameApparelSlots, GameItemData, GameAbstractItem, GameInventoryItem, GameStatusEffect,
  GamePaperDollSprite, GameEnchantmentAbstract, GameColorTags, GameBlueprint, GameSpell;

const
  WeaponDamageOnHit = Single(2.0);
  BondageDamageOnHit = Single(0.5); // I really don't like having 2 different coefficients, but we need to balance stamina-durability-damage
  StealthDamageMultiplier = 5;  // min player damage = 5 * 0.73 = 3.6 [3.6*5 = 18.1; 5*5 = 25; all weapons will insta-kill most enemies]

type
  { If an item can be equipped, and if not - reason }
  TEquipItemResult = (eiOk, eiNoHands, eiNoLegs, eiSlotsUnavailable, eiSlotRestrained);

type
  TBodyPreset = class; // forward declaration
  { The most basic elements of inventory
    first of all to be used as body preset }
  TInventoryCore = class(TObject) //todo: simple serializable
  protected
    { Schedule update inventory at the end of the frame
      affects entities that may take considerable time to calculate
      like enchantments cache and therefore shouldn't be called multiple times per frame}
    FUpdateInventory: Boolean;
  public
    { Parent Actor for this Inventory
      Cannot be properly typed because of cyclic reference
      this value is nil for presets and cannot be nil otherwise }
    Parent: TObject;
    { Items in all inventory slots;
      nil means no item equipped in the slot
      one item can use multiple slots, in this case the pointer to the item
      will be copied over all used slots, check for Item.Data.MainSlot for simplicity}
    Apparel: array [TApparelSlot] of TAbstractItem;
    { Colors of the body features}
    BodyColor: TColorDictionary;
    { If this character should be censored? if yes - overrides global configuration}
    Censored: Boolean;
    { If this character should be censored in screenshots? if yes - overrides global configuration}
    CensoredInScreenshots: Boolean;
    { Blueprint of this character - wip/todo }
    function Blueprint: TBlueprint; virtual; abstract;
    { Mark inventory "dirty", will update at the end of the frame }
    procedure InvalidateInventory;
    { equip an item }
    // TODO: not abstract?
    procedure EquipItem(const AItem: TInventoryItem); virtual; abstract;
    { equip an item or a bodypart }
    procedure EquipApparel(const AItem: TAbstractItem);
    { Destroy an equipped item safely }
    procedure DestroyEquippedItemSafe(const AEquipSlot: TApparelSlot);
  public
    procedure Save(const Element: TDOMElement); virtual; //override
    procedure Load(const Element: TDOMElement); virtual; //override
    { Make a preset out of this inventory}
    function CloneBody: TBodyPreset;
    { Generate a random body for the character keeping the blueprint}
    class function RandomBody: TBodyPreset;
    { Copy body from a given preset}
    procedure AssignBody(const APreset: TInventoryCore);
  public
    constructor Create; virtual;// override;
    destructor Destroy; override;
  end;

  { Body parts, colors and additional metadata }
  TBodyPreset = class(TInventoryCore)
  public
    { Human-readable name of this preset }
    DisplayName: String;
    { If this preset "local" (created by Player and saved into CastleConfig folder)
      - these ones will be automatically saved every time something changes in presets
      or loaded form a package, read-only }
    IsLocal: Boolean;
    { Name of the blueprint - wip/todo }
    BlueprintId: String;
    //preview image could have been cool here, but it's a lot of UI work (TODO?) - now we just have a name, then we'd need a popup with a fancy palette
    function Blueprint: TBlueprint; override;
  public
    procedure Save(const Element: TDOMElement); override;
    procedure Load(const Element: TDOMElement); override;
  end;
  TBodyPresetsList = specialize TObjectList<TBodyPreset>;

type
  TInventory = class(TInventoryCore)
  strict private
    WasTopCovered, WasBottomCovered: Boolean;
    procedure StartForcedCoveredChange;
    procedure EndForcedCoveredChange;
    procedure UpdateCoveredStatus;
  public
    TopCovered, BottomCovered: Integer;
    TopCoveredRemovable, BottomCoveredRemovable: Integer;
  strict private
    FEnchantmentsMultiplierCache: TEnchantmentCache;
    FEnchantmentsAdditiveCache: TEnchantmentCache;
    FEnchantmentsMaxCache: TEnchantmentCache;
    FEnchantmentsMinCache: TEnchantmentCache;
    function GetColorForSlot(ApparelSlot: TApparelSlot): TCastleColor;
    function GetIsBondageItem(ApparelSlot: TApparelSlot): Boolean;
    function GetEquipped(const ApparelSlot: TApparelSlot): TInventoryItem;
    function GetIsRemovableItem(ApparelSlot: TApparelSlot): Boolean;
    procedure SetEquipped(const ApparelSlot: TApparelSlot; const AItem: TInventoryItem);
    procedure RegenerateEnchantmentsCache;
  public
    StatusEffects: TStatusEffectsList;
    IgnoredEntities: TReferenceList;
    IgnoredTypes: TStringList;
    Spells: TSpellsList;
    property Equipped [ApparelSlot: TApparelSlot]: TInventoryItem read GetEquipped write SetEquipped;
    property Bondage [ApparelSlot: TApparelSlot]: Boolean read GetIsBondageItem;
    property RemovableItem [ApparelSlot: TApparelSlot]: Boolean read GetIsRemovableItem;
    property ColorForSlot [ApparelSlot: TApparelSlot]: TCastleColor read GetColorForSlot;
    procedure AddStatusEffect(const AStatusEffect: TStatusEffect);
    // WARNING: it can free the incoming status effect if it stacks
    procedure AddStatusEffectNoStack(const AStatusEffect: TStatusEffect);
    function GetRandomClothesSlot: TApparelSlot;
    function GetRandomClothesSlotEquippedItem: TApparelSlot;
    function GetRandomClothesSlotEquippedItemOrBondage: TApparelSlot;
    { Clothes and weapons }
    function GetRandomEquipmentSlot: TApparelSlot;
    function GetRandomEquipmentSlotEquippedItem: TApparelSlot;
    function GetRandomEquipmentSlotEquippedItemOrBondage: TApparelSlot;
    // TODO: move those to UpdateCoveredStatus
    function EquippedSlotsRemovable: Integer;
    function EquippedClothesRemovable: Integer;
    function EquippedClothesAndBondage: Integer;
    { Which slots are hidden in this Inventory
      First of all checks if "censored" mode is on, or is on for this character in all cases
      ClothesHideSlots is required for "nude mode" renders, when while clothes
      do hide slots, but they aren't rendered themselves, so no body slots sould be hidden }
    function GetHiddenSlots(const ClothesHideSlots: Boolean): TApparelSlotsSet;
    // .
    function TotalIdleNoise: Single;
    function GetItemNoiseSafe(const Slot: TApparelSlot): Single;
    procedure DamageItem(const Slot: TApparelSlot; const Damage: Single);
    procedure DamageItem(const AItem: TInventoryItem; const Damage: Single);
    procedure DisintegrateItem(const Slot: TApparelSlot);
    { Gets damage of the Player item or unarmed }
    function GetBaseDamage: Single;
    { Including normal damage bonuses }
    function GetDamage: Single;
    { Including stealth damage bonuses }
    function GetStealthDamage: Single;
    procedure DamageWeapon;
    procedure PlayAttackSound;
    { Will either damage footwear and report false or return true (to hurt the parent) }
    function HurtFeet(const Damage: Single): Boolean;
    function PartiallyOrFullyNude: Boolean;
    function Nude: Boolean;
    function EmbarrasmentMultiplier: Single;
    function TouchMultiplier: Single;
    procedure WardrobeMalfunction;
    function EnchantmentRequirementsMet(const Enchantment: TEnchantmentAbstract): Boolean;
    function CanEquipItem(const AItem: TInventoryItem): TEquipItemResult;
    function RestraintInTheWay(const AItem: TInventoryItem): TInventoryItem;
    procedure EquipItem(const AItem: TInventoryItem); override;
    function UnequipAndReturn(const AEquipSlot: TApparelSlot; const Forced: Boolean): TInventoryItem;
    procedure UnequipAndDrop(const AEquipSlot: TApparelSlot; const Forced: Boolean);
    procedure UnequipAndDisintegrate(const AEquipSlot: TApparelSlot; const Forced: Boolean);
    { TODO: item is equipped but passed as InventoryItem instance,
      that's because it comes from DamageItem
      but it doesn't make sense in this specific case }
    procedure DisintegrateEquippedItem(const AItem: TInventoryItem; const Forced: Boolean);
    procedure DisintegrateOrDrop(const AEquipSlot: TApparelSlot; const Forced: Boolean);
    function DisintegrateOrReturn(const AEquipSlot: TApparelSlot; const Forced: Boolean): TInventoryItem;
    procedure UnequipAndDisintegrateEverything;
  public
    function CanUseHands: Boolean; //override; todo?
    function HearingBonus: Single;
    function VisionBonus: Single;
    function XRayVision: Boolean;
    { Leash is attached and held }
    function LeashHeld: Boolean;
    function FindEffectAdditive(const EffectClass: TEnchantmentClass): Single;
    function FindEffectMultiplier(const EffectClass: TEnchantmentClass): Single;
    { note: if no effect exists max = 0, so can work only on positive values }
    function FindEffectMax(const EffectClass: TEnchantmentClass; const DefaultValue: Single): Single;
    function FindEffectMin(const EffectClass: TEnchantmentClass; const DefaultValue: Single): Single;
  public
    procedure MaximizeDurability(const AEquipSlot: TApparelSlot);
    procedure ReinforceItem(const AEquipSlot: TApparelSlot);
    function ShouldMaximizeDurability(const AEquipSlot: TApparelSlot): Boolean;
    function ShouldMaximizeDurability(const AItemName: String): Boolean;
    function HasItem(const AOtherItemData: TItemData): Boolean;
    function HasItem(const AOtherItemName: String): Boolean;
    function Blueprint: TBlueprint; override;
  public
    PaperDollSprite: TPaperDollSprite;
    procedure Update(const SecondsPassed: Single);
  public
    procedure Save(const Element: TDOMElement); override;
    procedure Load(const Element: TDOMElement); override;
  public
    constructor Create; override;
    { Must be called after constructor because needs
      a properly assigned Parent and initialized Parent.Inventory }
    procedure CreatePaperDoll;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameConfiguration, GameViewGame, GameRandom, GameLog, GameTranslation, GameSounds, GameColors,
  GameMap, GameMapItem, GameActor, GamePlayerCharacter, GameStats, GameLeash, GameMath, GameEnumUtils,
  GameItemsDatabase, GameBodyPart, GameDifficultyLevel, GameGarbageCollector, GameBlueprintsDatabase,
  GameEnchantmentHearingBonus, GameEnchantmentXRayVision, GameEnchantmentVisionBonus,
  GameEnchantmentDamageBonus, GameEnchantmentDamageMultiplier,
  GameEnchantmentDisorientation, GameEnchantmentStripWillpowerDamageMultiplier,
  GameEnchantmentCannotInteractWithActor, GameEnchantmentCannotInteractWithType,
  GameEnchantmentStealthDamageBonus, GameEnchantmentStealthDamageMultiplier,
  GameEnchantmentIncomingWillDamageMultiplier,
  GameEnchantmentNudeTouchWillpowerDamageMultiplier,
  GameEnchantmentWalkNudeWillpowerDamageMultiplier,
  GameViewAbstract, GameItemDataAbstract;

{$IFDEF SafeTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentPlayer:=(Parent as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentPlayer:=TPlayerCharacter(Parent)}
{$ENDIF}

{ TInventoryCore ----------------------------------------- }

procedure TInventoryCore.EquipApparel(const AItem: TAbstractItem);
var
  E: TApparelSlot;
begin
  if AItem is TInventoryItem then
    EquipItem(TInventoryItem(AItem))
  else
    for E in AItem.Data.EquipSlots do
      Apparel[E] := AItem;
  InvalidateInventory;
end;

procedure TInventoryCore.DestroyEquippedItemSafe(const AEquipSlot: TApparelSlot);
var
  AItem: TAbstractItem;
  E: TApparelSlot;
begin
  AItem := Apparel[AEquipSlot];
  if (AItem <> nil) then
  begin
    for E in AItem.Data.EquipSlots do
      if Apparel[E] = AItem then
        Apparel[E] := nil;
    if AItem is TInventoryItem then
      TInventoryItem(AItem).Stop;
    GarbageCollector.AddAndNil(AItem);
    InvalidateInventory;
  end;
end;

procedure TInventoryCore.InvalidateInventory;
begin
  FUpdateInventory := true;
  // we need to call it twice? Because otherwise Inventory buttons can get updated for obsolete state of the inventory (and SIGSEGV)
  // It shouldn't cause any issues, just sets a flag to update the inventory
  if (ViewGame <> nil) and (Parent <> nil) then
    ViewGame.InvalidateInventory(Parent);
end;

procedure TInventoryCore.Save(const Element: TDOMElement);
var
  E: TApparelSlot;
  ColorTag: String;
begin
  for E in TApparelSlot do
    if (Apparel[E] <> nil) and (Apparel[E].Data.MainSlot = E) then
      Apparel[E].Save(Element.CreateChild(specialize EnumToStr<TApparelSlot>(E)));

  for ColorTag in BodyColor.Keys do
    Element.AttributeColorSet('ColorTag_' + ColorTag, BodyColor[ColorTag]);

  Element.AttributeSet('Censored', Censored);
  Element.AttributeSet('CensoredInScreenshots', CensoredInScreenshots);
end;

procedure TInventoryCore.Load(const Element: TDOMElement);
var
  E: TApparelSlot;
  ColorTag: String;
  AItem: TAbstractItem;
  PlayerName: String;
begin
  inherited;

  if Parent = nil then
    PlayerName := '(preset)'
  else
    PlayerName := ParentPlayer.Data.DisplayName;

  for E in TApparelSlot do
    if Element.Child(specialize EnumToStr<TApparelSlot>(E), false) <> nil then
    begin
      try
        AItem := TAbstractItem.LoadClass(Element.Child(specialize EnumToStr<TApparelSlot>(E), true)) as TAbstractItem;
        EquipApparel(AItem);
        AItem.AfterDeserealization;
      except
        on Ex: Exception do
        begin
          try
            if Element.Child(specialize EnumToStr<TApparelSlot>(E), false) = nil then
              ShowError('%s:"%s". Cannot load player "%s" inventory slot %s. Record not found.', [Ex.ClassName, Ex.Message, PlayerName, specialize EnumToStr<TApparelSlot>(E)])
            else
              ShowError('%s:"%s". Failed to load item %s in player "%s" inventory.', [Ex.ClassName, Ex.Message, Element.Child(specialize EnumToStr<TApparelSlot>(E), true).AttributeStringDef('Data.Id', 'N/A'), PlayerName]);
          except
            ShowError('Exception while trying to report exception in TInventory.Load (Item)');
          end;
          Apparel[E] := nil; // just to make sure
        end;
      end;
    end;
  for E in Blueprint.BodySlots do
    if (Apparel[E] = nil) or not (Apparel[E] is TBodyPart) then
    begin
      ShowError('Failed to load body slot %s for %s. Applying random body slot.', [specialize EnumToStr<TApparelSlot>(E), PlayerName]);
      EquipApparel(TBodyPart.NewBodyPart(GetRandomItemForSlot(E)));
    end;

  BodyColor.Add('none', White);
  for ColorTag in Blueprint.ColorTags.Keys do
    BodyColor.Add(ColorTag, Element.AttributeColorDef('ColorTag_' + ColorTag, Blueprint.ColorTags[ColorTag][Rnd.Random(Blueprint.ColorTags[ColorTag].Count)]));

  Censored := Element.AttributeBooleanDef('Censored', false);
  CensoredInScreenshots := Element.AttributeBooleanDef('CensoredInScreenshots', false);

  InvalidateInventory; // won't hurt
end;

function TInventoryCore.CloneBody: TBodyPreset;
var
  BodySlot: TApparelSlot;
  ColorTag: String;
begin
  if Parent = nil then
    raise Exception.Create('Cannot clone a body with no parent');
  Result := TBodyPreset.Create;
  Result.DisplayName := ParentPlayer.Data.DisplayName;
  Result.Parent := nil;
  Result.IsLocal := true;
  Result.BlueprintId := ParentPlayer.Blueprint.Id;
  for BodySlot in ParentPlayer.Blueprint.AllBodySlots do
    if (Apparel[BodySlot] <> nil) {is this even possible?} and
      (Apparel[BodySlot].Data.MainSlot = BodySlot) then
        Result.EquipApparel(TBodyPart.NewBodyPart(Apparel[BodySlot].Data));
  for ColorTag in BodyColor.Keys do
    Result.BodyColor.Add(ColorTag, BodyColor[ColorTag]);
  Result.Censored := Censored;
  Result.CensoredInScreenshots := CensoredInScreenshots;
end;

class function TInventoryCore.RandomBody: TBodyPreset;
begin
  Result := TBodyPreset.Create;
  Result.DisplayName := 'Random';
  Result.Parent := nil;
  Result.IsLocal := false;
  Result.BlueprintId := ''; // Random body must not be saved
  //Apparel[E] is nil by default
  //BodyColor is empty by default
  Result.Censored := false;
  Result.CensoredInScreenshots := false;
end;

procedure TInventoryCore.AssignBody(const APreset: TInventoryCore);
var
  BodySlot: TApparelSlot;
  ColorTag: String;
begin
  for BodySlot in ParentPlayer.Blueprint.BodySlots do
  begin
    // Note that if a bodypart occupies multiple slots, we'll destroy and create it again several times, TODO
    DestroyEquippedItemSafe(BodySlot);
    if APreset.Apparel[BodySlot] <> nil then
      EquipApparel(TBodyPart.NewBodyPart(APreset.Apparel[BodySlot].Data))
    else
      // if not in preset - assume random
      EquipApparel(TBodyPart.NewBodyPart(GetRandomItemForSlot(BodySlot)));
  end;
  BodyColor.Clear;
  for ColorTag in APreset.BodyColor.Keys do
    BodyColor.Add(ColorTag, APreset.BodyColor[ColorTag]);
  if not BodyColor.ContainsKey('none') then
    BodyColor.Add('none', White);
  // if not in preset - assume random
  for ColorTag in ParentPlayer.Blueprint.ColorTags.Keys do
    if not BodyColor.ContainsKey(ColorTag) then
      BodyColor.Add(ColorTag, ParentPlayer.Blueprint.ColorTags[ColorTag][Rnd.Random(ParentPlayer.Blueprint.ColorTags[ColorTag].Count)]);
end;

constructor TInventoryCore.Create;
begin
  inherited; //parent is non-virtual
  BodyColor := TColorDictionary.Create;
  Censored := false;
  CensoredInScreenshots := false;
end;

destructor TInventoryCore.Destroy;
var
  E, E2: TApparelSlot;
begin
  FreeAndNil(BodyColor);
  for E in TApparelSlot do
  begin
    for E2 in TApparelSlot do
      if (E2 <> E) and (Apparel[E] = Apparel[E2]) then
        Apparel[E2] := nil;
    FreeAndNil(Apparel[E]);
  end;
  inherited Destroy;
end;

{ TBodyPreset ----------------------------------------------------------------- }

function TBodyPreset.Blueprint: TBlueprint;
begin
  Exit(BlueprintsDictionary[BlueprintId]);
end;

procedure TBodyPreset.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('DisplayName', DisplayName);
  Element.AttributeSet('BlueprintId', BlueprintId);
end;

procedure TBodyPreset.Load(const Element: TDOMElement);
begin
  BlueprintId := Element.AttributeString('BlueprintId'); // before inherited
  inherited Load(Element);
  DisplayName := Element.AttributeString('DisplayName');
end;

{ TInventory --------------------------------------------}

procedure TInventory.StartForcedCoveredChange;
begin
  WasTopCovered := TopCovered > 0;
  WasBottomCovered := BottomCovered > 0;
end;

procedure TInventory.EndForcedCoveredChange;
var
  SomethingExposed: Boolean;
  WillpowerDisrobingDamage: Single = 0;
begin
  SomethingExposed := false;
  if WasTopCovered and (TopCovered = 0) and WasBottomCovered and (BottomCovered = 0) then
  begin
    ShowLog('%s is buck naked', [ParentActor.Data.DisplayName], ColorLogNaked);
    WillpowerDisrobingDamage := ParentPlayer.Will * FindEffectMultiplier(TEnchantmentStripWillpowerDamageMultiplier) *
      (Difficulty.StripTopWillpowerFractionDamage + Difficulty.StripBottomWillpowerFractionDamage *
      (1 - Difficulty.StripTopWillpowerFractionDamage)); // Otherwise it'll result in higher fraction if otherwise those would have been applied sequentially which gives a noticeable disadvantage to items that cover both top and bottom
    SomethingExposed := true;
  end else
  if WasTopCovered and (TopCovered = 0) then
  begin
    ShowLog(GetTranslation('PlayerLostTop'), [ParentActor.Data.DisplayName], ColorLogNaked);
    WillpowerDisrobingDamage := ParentPlayer.Will * Difficulty.StripTopWillpowerFractionDamage;
    SomethingExposed := true;
  end else
  if WasBottomCovered and (BottomCovered = 0) then
  begin
    ShowLog(GetTranslation('PlayerLostBottom'), [ParentActor.Data.DisplayName], ColorLogNaked);
    WillpowerDisrobingDamage := ParentPlayer.Will * Difficulty.StripBottomWillpowerFractionDamage;
    SomethingExposed := true;
  end;
  if SomethingExposed then
  begin
    WillpowerDisrobingDamage *= FindEffectMultiplier(TEnchantmentStripWillpowerDamageMultiplier);
    WillpowerDisrobingDamage *= FindEffectMultiplier(TEnchantmentIncomingWillDamageMultiplier);
    WillpowerDisrobingDamage := MinSingle(ParentPlayer.Will - 1, WillpowerDisrobingDamage);
    if WillpowerDisrobingDamage < 0 then
      WillpowerDisrobingDamage := 0; // just to be sure in case Will - 1 < 0
    ParentPlayer.Will -= WillpowerDisrobingDamage;
    { note a logic bug here:
      If character had an item covering both top and bottom - will lose 5
      If character had 2 items covering top and bottom - loses 10 as those are removed in 2 calls}
    //ParentPlayer.MaxWill -= MinSingle(5.0, WillpowerDisrobingDamage / 2 * Difficulty.StatsDegradationSpeed);
    // so for now using a simpler formula, i.e. 1/5 of what should normally be degraded
    ParentPlayer.MaxWill -= WillpowerDisrobingDamage / 5 * Difficulty.StatsDegradationSpeed;
    if WillpowerDisrobingDamage > ParentPlayer.Will * 0.01 then
      ShowLog('%s lost %.1n willpower', [ParentActor.Data.DisplayName, WillpowerDisrobingDamage], ColorLogWillDamage);
    if FindEffectMultiplier(TEnchantmentStripWillpowerDamageMultiplier) > 0.5 then
      ParentPlayer.PlaySurpriseSound;
  end;
end;

procedure TInventory.UpdateCoveredStatus;
var
  E: TApparelSlot;
begin
  TopCovered := 0;
  BottomCovered := 0;
  TopCoveredRemovable := 0;
  BottomCoveredRemovable := 0;
  for E in ParentPlayer.Blueprint.EquipmentSlots do
    if (Apparel[E] <> nil) and (E = Apparel[E].Data.MainSlot) then
      begin
        if Equipped[E].ItemData.CoversTop then
        begin
          Inc(TopCovered);
          if not Bondage[E] then
            Inc(TopCoveredRemovable);
        end;
        if Equipped[E].ItemData.CoversBottom then
        begin
          Inc(BottomCovered);
          if not Bondage[E] then
            Inc(BottomCoveredRemovable);
        end;
      end;
end;

function TInventory.GetEquipped(const ApparelSlot: TApparelSlot): TInventoryItem;
begin
  Exit(Apparel[ApparelSlot] as TInventoryItem);
end;

function TInventory.GetIsRemovableItem(ApparelSlot: TApparelSlot): Boolean;
begin
  if Apparel[ApparelSlot] = nil then
    Exit(false)
  else
    Exit(not (Apparel[ApparelSlot] as TInventoryItem).IsBondage(Self));
end;

function TInventory.GetIsBondageItem(ApparelSlot: TApparelSlot): Boolean;
begin
  if Apparel[ApparelSlot] = nil then
    Exit(false)
  else
    Exit((Apparel[ApparelSlot] as TInventoryItem).IsBondage(Self));
end;

function TInventory.GetColorForSlot(ApparelSlot: TApparelSlot): TCastleColor;
var
  ImageColorTag: String;
  ApparelImagesDictionary: TApparelImagesDictionary;
begin
  if Apparel[ApparelSlot] = nil then
    Exit(White); // we shouldn't have asked, actually. Maybe show error?

  ApparelImagesDictionary := Apparel[ApparelSlot].Data.Images[ViewGame.CurrentCharacter.GetShapePose];
  ImageColorTag := ApparelImagesDictionary[ApparelSlot].ColorTag;
  if ApparelSlot in ParentPlayer.Blueprint.AllBodySlots then
  begin
    // TODO: optimize
    if BodyColor.ContainsKey(ImageColorTag) then
      Exit(BodyColor[ImageColorTag])
    else
    begin
      ShowError('ColorTag %s not found in body', [ImageColorTag]);
      Exit(White);
    end;
  end else
  if ApparelSlot in ParentPlayer.Blueprint.AllEquipmentSlots then
    Exit(Equipped[ApparelSlot].Color[ImageColorTag])
  else
  begin
    ShowError('Unexpected slot %s in TInventory.GetColorForSlot', [specialize EnumToStr<TApparelSlot>(ApparelSlot)]);
    Exit(White);
  end;
end;

procedure TInventory.SetEquipped(const ApparelSlot: TApparelSlot;
  const AItem: TInventoryItem);
begin
  Apparel[ApparelSlot] := AItem;
  InvalidateInventory;
end;

function TInventory.GetRandomClothesSlot: TApparelSlot;
begin
  Result := ParentPlayer.Blueprint.ClothesSlotsList[Rnd.Random(ParentPlayer.Blueprint.ClothesSlotsList.Count)];
end;

function TInventory.GetRandomClothesSlotEquippedItem: TApparelSlot;
begin
  repeat
    Result := GetRandomClothesSlot;
  until (Equipped[Result] <> nil) and not Bondage[Result]; // warning, no checks! It can freeze if all are nil
end;

function TInventory.GetRandomClothesSlotEquippedItemOrBondage: TApparelSlot;
begin
  repeat
    Result := GetRandomClothesSlot;
  until Equipped[Result] <> nil; // warning, no checks! It can freeze if all are nil
end;

function TInventory.GetRandomEquipmentSlot: TApparelSlot;
begin
  Result := ParentPlayer.Blueprint.EquipmentSlotsList[Rnd.Random(ParentPlayer.Blueprint.EquipmentSlotsList.Count)];
end;

function TInventory.GetRandomEquipmentSlotEquippedItem: TApparelSlot;
begin
  repeat
    Result := GetRandomEquipmentSlot;
  until (Equipped[Result] <> nil) and (not Bondage[Result]); // warning, no checks! It can freeze if all are nil
end;

function TInventory.GetRandomEquipmentSlotEquippedItemOrBondage: TApparelSlot;
begin
  repeat
    Result := GetRandomEquipmentSlot;
  until Equipped[Result] <> nil; // warning, no checks! It can freeze if all are nil
end;

function TInventory.EquippedSlotsRemovable: Integer;
var
  E: TApparelSlot;
begin
  Result := 0;
  for E in ParentPlayer.Blueprint.EquipmentSlots do
    if (Apparel[E] <> nil) and not Bondage[E] then
      Inc(Result);
end;

function TInventory.EquippedClothesRemovable: Integer;
var
  E: TApparelSlot;
begin
  Result := 0;
  for E in ParentPlayer.Blueprint.ClothesSlots do
    if (Apparel[E] <> nil) and not Bondage[E] then
      Inc(Result);
end;

function TInventory.EquippedClothesAndBondage: Integer;
var
  E: TApparelSlot;
begin
  Result := 0;
  for E in ParentPlayer.Blueprint.ClothesSlots do
    if Apparel[E] <> nil then
      Inc(Result);
end;

function TInventory.GetHiddenSlots(const ClothesHideSlots: Boolean
  ): TApparelSlotsSet;
var
  E: TApparelSlot;
begin
  Result := [];
  if Configuration.Censored or Censored or (ScreenshotNow and (Configuration.CensoredScreenshots or CensoredInScreenshots)) then
    Result += CensoredLayers;
  if ClothesHideSlots then
    for E in TApparelSlot do
      if (Apparel[E] <> nil) then
        Result += Apparel[E].Data.HidesSlots;
end;

function TInventory.TotalIdleNoise: Single;
var
  E: TApparelSlot;
begin
  Result := 0;
  for E in ParentPlayer.Blueprint.EquipmentSlots do
    if (Apparel[E] <> nil) and (Apparel[E].Data.MainSlot = E) then
      Result += Equipped[E].ItemData.Noise;
end;

function TInventory.GetItemNoiseSafe(const Slot: TApparelSlot): Single;
begin
  if Equipped[Slot] <> nil then
    Exit(Equipped[Slot].ItemData.Noise)
  else
    Exit(0); // while this is an erroneous situation, we don't show log here - we properly workaround it
end;

procedure TInventory.DamageItem(const Slot: TApparelSlot;
  const Damage: Single);
begin
  DamageItem(Equipped[Slot], Damage);
end;

procedure TInventory.DamageItem(const AItem: TInventoryItem;
  const Damage: Single);
begin
  if AItem.ItemData.Indestructible then
    Exit;

  AItem.Durability -= Damage;
  // item breaks
  if AItem.IsBondage(Self) then
  begin
    if AItem.Durability <= 0 then
    begin
      if Parent = ViewGame.CurrentCharacter then
      begin
        ViewGame.ShakeCharacter;
      end;
      Sound(AItem.ItemData.SoundDisintegrate);
      DisintegrateEquippedItem(AItem, true);
    end;
  end else
  if Rnd.Random > (3 * AItem.Durability / AItem.MaxDurability) then
  begin
    ViewGame.ShakeCharacter;

    if (Rnd.Random < AItem.Durability / ItemDisintegrationThreshold) and AItem.ItemData.CanBeRepaired then
    begin
      if Parent = ViewGame.CurrentCharacter then
        ShowLog(GetTranslation('ItemBreaksLog'), [AItem.Data.DisplayName], ColorLogItemBreak);
      AItem.Broken := true;
      Sound(AItem.ItemData.SoundBreak);
      UnequipAndDrop(AItem.Data.MainSlot, true);
    end else
    begin
      Sound(AItem.ItemData.SoundDisintegrate);
      DisintegrateEquippedItem(AItem, true);
    end;

    ParentPlayer.HitWill(Difficulty.BrokenItemWillpowerDamage);
  end else
  // a chance to slip off
  if (Rnd.Random < AItem.ItemData.WardrobeMalfunctionChance) then
  begin
    if Parent = ViewGame.CurrentCharacter then
      ShowLog(GetTranslation('ItemWardrobeMalfunctionLog'), [AItem.Data.DisplayName], ColorLogItemBreak);
    Sound(AItem.ItemData.SoundUnequip);
    UnequipAndDrop(AItem.Data.MainSlot, true);
  end;
end;

procedure TInventory.DisintegrateItem(const Slot: TApparelSlot);
begin
  // TODO
  DamageItem(Slot, 1000);
end;

function TInventory.GetBaseDamage: Single;
begin
  if (Equipped[esWeapon] <> nil) and not Bondage[esWeapon] then
    Result := Equipped[esWeapon].ItemData.Damage
  else
  if (Equipped[esWeapon] <> nil) {and Equipped[esWeapon].IsBondage -- obvious} then
  begin
    if Equipped[esWeapon].ItemData.Damage > 0 then
      Result := Equipped[esWeapon].ItemData.Damage
    else
      Result := ParentPlayer.Blueprint.PunchDamage; // Set damage to -1 if character can use unarmed damage with this bondage item
  end else
    Result := ParentPlayer.Blueprint.PunchDamage;
end;

function TInventory.GetDamage: Single;
begin
  Result := GetBaseDamage * FindEffectMultiplier(TEnchantmentDamageMultiplier) + FindEffectAdditive(TEnchantmentDamageBonus);
  if Result < 1 then
    Result := 1;
end;

function TInventory.GetStealthDamage: Single;
begin
  Result := FindEffectAdditive(TEnchantmentStealthDamageBonus) + GetBaseDamage * StealthDamageMultiplier * FindEffectMultiplier(TEnchantmentStealthDamageMultiplier);
  if Result < 1 then
    Result := 1;
end;

procedure TInventory.WardrobeMalfunction;
var
  E: TApparelSlot;
begin
  for E in ParentPlayer.Blueprint.EquipmentSlots do
    if (Apparel[E] <> nil) and (Apparel[E].Data.MainSlot = E) and (Rnd.Random < Equipped[E].ItemData.WardrobeMalfunctionChance) then
    begin
      if Parent = ViewGame.CurrentCharacter then
        ShowLog(GetTranslation('ItemWardrobeMalfunctionLog'), [Apparel[E].Data.DisplayName], ColorLogItemBreak);
      Sound(Equipped[E].ItemData.SoundUnequip);
      UnequipAndDrop(E, true);
    end;
end;

function TInventory.CanEquipItem(const AItem: TInventoryItem): TEquipItemResult;
var
  E: TApparelSlot;
begin
  for E in AItem.Data.EquipSlots do
    if not (E in ParentPlayer.Blueprint.AllEquipmentSlots) then
      Exit(eiSlotsUnavailable); // without specifying which exactly

  // TODO: more generic handling of equip/bondage
  for E in AItem.Data.EquipSlots do
    if (Apparel[E] <> nil) and Bondage[E] then
      Exit(eiSlotRestrained); // without specifying which exactly

  //if AItem.ItemData.RequiresArms and (not ParentPlayer.Blueprint.HasArms) then
  //  Exit(eiNoHands);

  if AItem.ItemData.RequiresLegs and (not ParentPlayer.Blueprint.HasLegs) then
    Exit(eiNoLegs);

  Exit(eiOk);
end;

function TInventory.RestraintInTheWay(const AItem: TInventoryItem): TInventoryItem;
var
  E: TApparelSlot;
begin
  for E in AItem.Data.EquipSlots do
    if ViewGame.CurrentCharacter.Inventory.Bondage[E] then
      Exit(ViewGame.CurrentCharacter.Inventory.Equipped[E]);
  Exit(nil);
end;

procedure TInventory.EquipItem(const AItem: TInventoryItem);
var
  E: TApparelSlot;
begin
  for E in AItem.Data.EquipSlots do
    UnequipAndDrop(E, false); // TODO: fail, maybe crash: make sure all slots are empty before equipping anything
  for E in AItem.Data.EquipSlots do
    Apparel[E] := AItem; // the item takes all slots
  AItem.Parent := Self;
  InvalidateInventory;
  UpdateCoveredStatus; // WARNING: Do not optimize, next actions may require this
  LocalStats.IncStat('equipped_' + AItem.Data.Id);
end;

procedure TInventory.UnequipAndDrop(const AEquipSlot: TApparelSlot;
  const Forced: Boolean);
var
  AItem: TInventoryItem;
begin
  if Apparel[AEquipSlot] = nil then
    Exit; // TODO: This is not needed, make call safe on logic level
  AItem := UnequipAndReturn(AEquipSlot, Forced);
  TMapItem.DropItem(ParentPlayer.LastTileX + ParentPlayer.PredSize div 2, ParentPlayer.LastTileY + ParentPlayer.PredSize div 2, AItem, false);
end;

procedure TInventory.UnequipAndDisintegrate(const AEquipSlot: TApparelSlot;
  const Forced: Boolean);
var
  AItem: TInventoryItem;
begin
  if Apparel[AEquipSlot] = nil then
    Exit; // TODO: This is not needed, make call safe on logic level
  AItem := UnequipAndReturn(AEquipSlot, Forced);
  AItem.Stop;
  GarbageCollector.AddAndNil(AItem);
end;

procedure TInventory.DisintegrateEquippedItem(const AItem: TInventoryItem;
  const Forced: Boolean);
var
  I: Integer;
  DisintegratedItem: TInventoryItem;
begin
  if AItem.ItemData.DisintegratesInto.Count = 0 then
  begin
    if Forced and (Parent = ViewGame.CurrentCharacter) then
      ShowLog(GetTranslation('ItemDisintegratesLog'), [AItem.Data.DisplayName], ColorLogItemDisintegrated);
    UnequipAndDisintegrate(AItem.Data.MainSlot, Forced);
  end else
  begin
    if AItem.ItemData.DisintegrationReequip then
    begin
      if Forced and (Parent = ViewGame.CurrentCharacter) then
        ShowLog('%s breaks', [AItem.Data.DisplayName], ColorLogItemDisintegrated);
      if Forced then
        StartForcedCoveredChange;
      DisintegratedItem := UnequipAndReturn(AItem.Data.MainSlot, false); // forced=false because we'll check if something got uncovered after reequipping remains
      for I := 0 to Pred(AItem.ItemData.DisintegratesInto.Count) do
        EquipItem(TInventoryItem.NewItem(ItemsDataDictionary[AItem.ItemData.DisintegratesInto[I]] as TItemData));
        // TODO set durability based on parent item https://gitlab.com/EugeneLoza/vinculike/-/issues/1278
      DisintegratedItem.Stop;
      GarbageCollector.AddAndNil(DisintegratedItem);
      if Forced then
        EndForcedCoveredChange; // note: both UnequipAndDisintegrate and EquipItem will ask for UpdateCoveredStatus, no need to call it manually
    end else
    begin
      if Forced and (Parent = ViewGame.CurrentCharacter) then
        ShowLog(GetTranslation('ItemDisintegratesLog'), [AItem.Data.DisplayName], ColorLogItemDisintegrated);
      DisintegratedItem := UnequipAndReturn(AItem.Data.MainSlot, Forced);
      for I := 0 to Pred(AItem.ItemData.DisintegratesInto.Count) do
        TMapItem.DropItem(ParentPlayer.LastTileX + ParentPlayer.PredSize div 2, ParentPlayer.LastTileY + ParentPlayer.PredSize div 2, TInventoryItem.NewItem(ItemsDataDictionary[AItem.ItemData.DisintegratesInto[I]] as TItemData), false);
        // TODO set durability based on parent item https://gitlab.com/EugeneLoza/vinculike/-/issues/1278
      DisintegratedItem.Stop;
      GarbageCollector.AddAndNil(DisintegratedItem);
    end;
  end;
end;

procedure TInventory.DisintegrateOrDrop(const AEquipSlot: TApparelSlot;
  const Forced: Boolean);
begin
  if Apparel[AEquipSlot] = nil then
    Exit;
  if Equipped[AEquipSlot].ItemData.UnequipDisintegrates then
    UnequipAndDisintegrate(AEquipSlot, Forced)
  else
    UnequipAndDrop(AEquipSlot, Forced);
end;

function TInventory.DisintegrateOrReturn(const AEquipSlot: TApparelSlot;
  const Forced: Boolean): TInventoryItem;
begin
  if Apparel[AEquipSlot] = nil then
    Exit(nil);
  if Equipped[AEquipSlot].ItemData.UnequipDisintegrates then
  begin
    UnequipAndDisintegrate(AEquipSlot, Forced);
    Exit(nil);
  end else
    Exit(UnequipAndReturn(AEquipSlot, Forced));
end;

procedure TInventory.UnequipAndDisintegrateEverything;
var
  E: TApparelSlot;
begin
  for E in ParentPlayer.Blueprint.EquipmentSlots do
    UnequipAndDisintegrate(E, false);
end;

function TInventory.UnequipAndReturn(const AEquipSlot: TApparelSlot;
  const Forced: Boolean): TInventoryItem;
var
  E: TApparelSlot;
begin
  if Forced then
    StartForcedCoveredChange;

  Result := Equipped[AEquipSlot];
  for E in Result.Data.EquipSlots do
    if Apparel[E] = Result then
      Apparel[E] := nil;
  Result.Stop;
  InvalidateInventory;

  UpdateCoveredStatus;
  if Forced then
    EndForcedCoveredChange;
end;

procedure TInventory.DamageWeapon;
begin
  inherited;
  if (Equipped[esWeapon] <> nil) and not Equipped[esWeapon].ItemData.Bondage then // note, not "Bondage[esWeapon]" which may change due to enchantments
    DamageItem(Equipped[esWeapon], WeaponDamageOnHit)
  else
  if (Equipped[esWeapon] <> nil) then
  begin
    // TODO: more generic
    if Equipped[esWeapon].ItemData.Damage > 0 then
      DamageItem(Equipped[esWeapon], BondageDamageOnHit)
  end else
  //Hit(1)
    ;
end;

procedure TInventory.PlayAttackSound;
begin
  if Equipped[esWeapon] <> nil then
    Sound(Equipped[esWeapon].ItemData.SoundAttack)
  else
    Sound('hit_punch');
end;

function TInventory.HurtFeet(const Damage: Single): Boolean;
var
  E: TApparelSlot;
begin
  for E in ParentPlayer.Blueprint.EquipmentSlots do
    if (Apparel[E] <> nil) and Equipped[E].ItemData.ProtectsFeet then // warning if there are multiple items protecting feet, it'll pick the first one
    begin
      Equipped[E].Durability -= Damage * Difficulty.FootwearDamageMultiplier;
      if Equipped[E].Durability < 0 then
      begin
        if Rnd.Random < Equipped[E].MaxDurability / ItemDisintegrationThreshold then
        begin
          if Parent = ViewGame.CurrentCharacter then
          begin
            ViewGame.ShakeCharacter;
            ShowLog('%s wear off', [Apparel[E].Data.DisplayName], ColorLogItemDisintegrated);
          end;
          Sound(Equipped[E].ItemData.SoundBreak);
          Equipped[E].Durability := 0.01; // just avoid it going < 0
          Equipped[E].Broken := true;
          DisintegrateOrDrop(E, false);
        end else
        begin
          if Parent = ViewGame.CurrentCharacter then
          begin
            ViewGame.ShakeCharacter;
            ShowLog('%s completely wear off', [Apparel[E].Data.DisplayName], ColorLogItemDisintegrated);
          end;
          Sound(Equipped[E].ItemData.SoundDisintegrate);
          UnequipAndDisintegrate(E, false);
        end;
      end;
      Exit(false);
    end;
  Exit(true);
end;

function TInventory.PartiallyOrFullyNude: Boolean;
begin
  Result := (TopCovered = 0) or (BottomCovered = 0);
end;

function TInventory.Nude: Boolean;
begin
  Result := (TopCovered = 0) and (BottomCovered = 0);
end;

function TInventory.EmbarrasmentMultiplier: Single;
begin
  Exit(FindEffectMultiplier(TEnchantmentWalkNudeWillpowerDamageMultiplier));
end;

function TInventory.TouchMultiplier: Single;
begin
  Exit(FindEffectMultiplier(TEnchantmentNudeTouchWillpowerDamageMultiplier));
end;

function TInventory.CanUseHands: Boolean;
begin
  // TODO: more generic, will hurt when poses will be introduced
  if Equipped[esWeapon] <> nil then
    Exit(not Equipped[esWeapon].ItemData.HandsUnavailable)
  else
    Exit(true);
end;

function TInventory.HearingBonus: Single;
begin
  // TODO: This is an expensive call and we call it on every monster every frame! Optimize!
  Exit(FindEffectAdditive(TEnchantmentHearingBonus));
end;

function TInventory.VisionBonus: Single;
begin
  // TODO: This is an expensive call and we call it on every monster every frame! Optimize!
  Exit(FindEffectAdditive(TEnchantmentVisionBonus));
end;

function TInventory.XRayVision: Boolean;
begin
  // TODO: This is an expensive call and we call it on every monster every frame! Optimize!
  Exit(FindEffectAdditive(TEnchantmentXRayVision) > 0);
end;

function TInventory.LeashHeld: Boolean;
begin
  Exit((Apparel[esLeash] <> nil) and ((Apparel[esLeash] as TLeash).LeashHolder <> nil))
end;

function TInventory.EnchantmentRequirementsMet(const Enchantment: TEnchantmentAbstract): Boolean;

  function RequiredItemsEquipped: Boolean;
  var
    I: Integer;
  begin
    Result := true;
    if Enchantment.RequiresItems <> nil then
      for I := 0 to Pred(Enchantment.RequiresItems.Count) do
        if not HasItem(Enchantment.RequiresItems[I]) then
          Exit(false);
  end;

  function HasFootwear: Boolean;
  var
    E: TApparelSlot;
  begin
    for E in TApparelSlot do
      if (Apparel[E] is TInventoryItem) and Equipped[E].ItemData.IsFootwear then // is TInventoryItem also checks against "nil"
        Exit(true);
    Exit(false);
  end;

  function HasBlindfold: Boolean;
  var
    E: TApparelSlot;
  begin
    for E in TApparelSlot do
      if (Apparel[E] is TInventoryItem) and Equipped[E].IsBlindfold(Self) then // is TInventoryItem also checks against "nil"
        Exit(true);
    Exit(false);
  end;

  function HasWeapon: Boolean;
  var
    E: TApparelSlot;
  begin
    for E in TApparelSlot do
      if (Apparel[E] is TInventoryItem) and Equipped[E].ItemData.IsWeapon then // is TInventoryItem also checks against "nil"
        Exit(true);
    Exit(false);
  end;

begin
  Exit(
    (not Enchantment.RequiresNudeTop or (TopCovered = 0)) and
    (not Enchantment.RequiresDressedTop or (TopCovered > 0)) and
    (not Enchantment.RequiresNudeBottom or (BottomCovered = 0)) and
    (not Enchantment.RequiresDressedBottom or (BottomCovered > 0)) and
    (not Enchantment.RequiresUnarmed or not HasWeapon) and
    (not Enchantment.RequiresBarefoot or not HasFootwear) and
    (not Enchantment.RequiresBlindfolded or HasBlindfold) and
    (not Enchantment.RequiresLeashHeld or LeashHeld) and
    (ParentPlayer.Health / ParentPlayer.PlayerCharacterData.MaxHealth <= Enchantment.RequiresHealthBelow) and
    RequiredItemsEquipped
  );
end;

procedure TInventory.RegenerateEnchantmentsCache;
var
  A: TApparelSlot;
  I: Integer;

  procedure AddEnchantment(const AEnchantment: TEnchantmentAbstract);
  begin
    if EnchantmentRequirementsMet(AEnchantment) then
    begin
      if FEnchantmentsMultiplierCache.ContainsKey(AEnchantment.EnchantmentClass) then
      begin
        FEnchantmentsMultiplierCache[AEnchantment.EnchantmentClass] := FEnchantmentsMultiplierCache[AEnchantment.EnchantmentClass] * AEnchantment.Strength;
        FEnchantmentsAdditiveCache[AEnchantment.EnchantmentClass] := FEnchantmentsAdditiveCache[AEnchantment.EnchantmentClass] + AEnchantment.Strength;
        if FEnchantmentsMaxCache[AEnchantment.EnchantmentClass] < AEnchantment.Strength then
          FEnchantmentsMaxCache[AEnchantment.EnchantmentClass] := AEnchantment.Strength;
        if FEnchantmentsMinCache[AEnchantment.EnchantmentClass] < AEnchantment.Strength then
          FEnchantmentsMinCache[AEnchantment.EnchantmentClass] := AEnchantment.Strength;
      end else
      begin
        FEnchantmentsMultiplierCache.Add(AEnchantment.EnchantmentClass, AEnchantment.Strength);
        FEnchantmentsAdditiveCache.Add(AEnchantment.EnchantmentClass, AEnchantment.Strength);
        FEnchantmentsMaxCache.Add(AEnchantment.EnchantmentClass, AEnchantment.Strength);
        FEnchantmentsMinCache.Add(AEnchantment.EnchantmentClass, AEnchantment.Strength);
      end;
      if AEnchantment is TEnchantmentCannotInteractWithType then
        IgnoredTypes.Add(TEnchantmentCannotInteractWithType(AEnchantment).ActorType);
      if AEnchantment is TEnchantmentCannotInteractWithActor then
        IgnoredEntities.Add(TEnchantmentCannotInteractWithActor(AEnchantment).ActorReferenceId);
    end;
  end;

begin
  FEnchantmentsAdditiveCache.Clear;
  FEnchantmentsMultiplierCache.Clear;
  FEnchantmentsMaxCache.Clear;
  FEnchantmentsMinCache.Clear;
  IgnoredEntities.Clear;
  IgnoredTypes.Clear;

  for A in ParentPlayer.Blueprint.EquipmentSlots do
    if (Apparel[A] <> nil) and (Apparel[A].Data.MainSlot = A) then
      for I := 0 to Pred(Equipped[A].Enchantments.Count) do
        AddEnchantment(Equipped[A].Enchantments[I]);
  for I := 0 to Pred(StatusEffects.Count) do
    AddEnchantment(StatusEffects[I].Effect);
end;

procedure TInventory.AddStatusEffect(const AStatusEffect: TStatusEffect);
begin
  if not AStatusEffect.Effect.CanBeStatusEffect then
  begin
    ShowError('Tried to add %s but it cannot be a status effect', [AStatusEffect.Effect.ClassName]);
    AStatusEffect.Free; // can't free and nil const
    Exit;
  end;
  if AStatusEffect.Timeout <= 0 then
    ShowError('Status effect %s has no timeout specified', [AStatusEffect.Effect.ClassName]); // but we still add it :)
  //DebugLog('STARTED: %s', [AStatusEffect.Description]);
  AStatusEffect.Parent := Self;
  StatusEffects.Add(AStatusEffect);
  InvalidateInventory;
end;

procedure TInventory.AddStatusEffectNoStack(const AStatusEffect: TStatusEffect);
var
  SE: TStatusEffect;
begin
  for SE in StatusEffects do
    if SE.Effect.ClassName = AStatusEffect.Effect.ClassName then
    begin
      // warning we don't look any further
      if SE.Effect.Strength < AStatusEffect.Effect.Strength then
        SE.Effect.Strength := AStatusEffect.Effect.Strength;
      // Timeout stacks
      SE.Timeout += AStatusEffect.Timeout;
      // we don't check conditions, though probably we should?
      AStatusEffect.Free; // can't free and nil const
      Exit;
    end;
  AddStatusEffect(AStatusEffect);
end;

function TInventory.FindEffectAdditive(const EffectClass: TEnchantmentClass): Single;
begin
  if FEnchantmentsAdditiveCache.ContainsKey(EffectClass) then
    Exit(FEnchantmentsAdditiveCache[EffectClass])
  else
    Exit(0.0);
end;

function TInventory.FindEffectMultiplier(const EffectClass: TEnchantmentClass): Single;
begin
  if FEnchantmentsMultiplierCache.ContainsKey(EffectClass) then
    Exit(FEnchantmentsMultiplierCache[EffectClass])
  else
    Exit(1.0);
end;

function TInventory.FindEffectMax(const EffectClass: TEnchantmentClass;
  const DefaultValue: Single): Single;
begin
  if FEnchantmentsMaxCache.ContainsKey(EffectClass) then
    Exit(FEnchantmentsMaxCache[EffectClass])
  else
    Exit(DefaultValue);
end;

function TInventory.FindEffectMin(const EffectClass: TEnchantmentClass;
  const DefaultValue: Single): Single;
begin
  if FEnchantmentsMaxCache.ContainsKey(EffectClass) then
    Exit(FEnchantmentsMinCache[EffectClass])
  else
    Exit(DefaultValue);
end;


procedure TInventory.MaximizeDurability(const AEquipSlot: TApparelSlot);
begin
  Equipped[AEquipSlot].MaxDurability := Equipped[AEquipSlot].ItemData.Durability;
  Equipped[AEquipSlot].Durability := Equipped[AEquipSlot].MaxDurability;
end;

procedure TInventory.ReinforceItem(const AEquipSlot: TApparelSlot);
begin
  Equipped[AEquipSlot].MaxDurability := (Equipped[AEquipSlot].MaxDurability + Equipped[AEquipSlot].ItemData.Durability) / 2;
  Equipped[AEquipSlot].Durability := (Equipped[AEquipSlot].Durability + Equipped[AEquipSlot].MaxDurability) / 2;
end;

function TInventory.ShouldMaximizeDurability(const AEquipSlot: TApparelSlot): Boolean;
begin
  Exit((Equipped[AEquipSlot] <> nil) and (Equipped[AEquipSlot].Durability < Equipped[AEquipSlot].ItemData.Durability / 2));
end;

function TInventory.ShouldMaximizeDurability(const AItemName: String): Boolean;
var
  AItem: TItemData;
begin
  AItem := ItemsDataDictionary[AItemName] as TItemData;
  if HasItem(AItem) then
    Exit(ShouldMaximizeDurability(AItem.MainSlot))
  else
    Exit(false);
end;

function TInventory.HasItem(const AOtherItemData: TItemData): Boolean;
begin
  Exit((Equipped[AOtherItemData.MainSlot] <> nil) and (Equipped[AOtherItemData.MainSlot].Data = AOtherItemData));
end;

function TInventory.HasItem(const AOtherItemName: String): Boolean;
begin
  //Exit(HasItem(ItemsDataDictionary[AOtherItemName]));
  Exit((Equipped[ItemsDataDictionary[AOtherItemName].MainSlot] <> nil) and (Apparel[ItemsDataDictionary[AOtherItemName].MainSlot].Data.Id = AOtherItemName));
end;

function TInventory.Blueprint: TBlueprint;
begin
  Exit(ParentPlayer.Blueprint);
end;

procedure TInventory.Update(const SecondsPassed: Single);
var
  Disorient: Integer;
  E: TApparelSlot;
  I: Integer;
begin
  I := 0;
  while I < StatusEffects.Count do
  begin
    StatusEffects[I].Update(SecondsPassed);
    if StatusEffects[I].Timeout < 0 then
    begin
      StatusEffects[I].Stop;
      StatusEffects.Delete(I);
      InvalidateInventory;
    end else
      Inc(I);
  end;

  for E in ParentPlayer.Blueprint.EquipmentSlots do
    if (Apparel[E] <> nil) and (Apparel[E].Data.MainSlot = E) then
      Equipped[E].Update(SecondsPassed);

  if FUpdateInventory then
  begin
    FUpdateInventory := false;
    UpdateCoveredStatus; // As we always do this in Equip/Unequip calls, it's redundant here, but better safe than sorry
    // Next one must account for proper covered status
    RegenerateEnchantmentsCache;
    //ViewGame.InvalidateInventory(Parent); - we do that in InvalidateInventory and it may be already updated at this point
    //ViewGame.InvalidatePosition(Parent); // in case some vision-related enchantments changed
    Map.FScheduleUpdateVisible := true;
  end;

  if Map.FScheduleUpdateVisible then // TEMPORARY: TODO
  begin
    Disorient := Round(FindEffectAdditive(TEnchantmentDisorientation));
    if Disorient > 0 then
      Map.ForgetVisible(Disorient);
  end;
end;

procedure TInventory.Save(const Element: TDOMElement);
var
  I: Integer;
begin
  inherited;
  for I := 0 to Pred(StatusEffects.Count) do
    StatusEffects[I].Save(Element.CreateChild('StatusEffect'));
  for I := 0 to Pred(Spells.Count) do
    Spells[I].Save(Element.CreateChild('Spell'));
end;

procedure TInventory.Load(const Element: TDOMElement);
var
  Iterator: TXMLElementIterator;
  SE: TStatusEffect;
  SP: TSpell;
begin
  inherited;

  Iterator := Element.ChildrenIterator('StatusEffect');
  try
    while Iterator.GetNext do
    begin
      try
        SE := TStatusEffect.LoadClass(Iterator.Current) as TStatusEffect;
      except
        on Ex: Exception do
        begin
          try
            ShowError('%s:"%s". Failed to load status effect for Player Character.', [Ex.ClassName, Ex.Message])
          except
            ShowError('Exception while trying to report exception in TInventory.Load (Status Effect)');
          end;
          continue;
        end;
      end;
      SE.AfterDeserealization;
      SE.Parent := Self;
      StatusEffects.Add(SE);
    end;
  finally FreeAndNil(Iterator) end;

  Iterator := Element.ChildrenIterator('Spell');
  try
    while Iterator.GetNext do
    begin
      try
        SP := TSpell.LoadClass(Iterator.Current) as TSpell;
      except
        on Ex: Exception do
        begin
          try
            ShowError('%s:"%s". Failed to load spell for Player Character.', [Ex.ClassName, Ex.Message])
          except
            ShowError('Exception while trying to report exception in TInventory.Load (Spell)');
          end;
          continue;
        end;
      end;
      SP.AfterDeserealization;
      SP.Parent := Parent;
      Spells.Add(SP);
    end;
  finally FreeAndNil(Iterator) end;
end;

constructor TInventory.Create;
begin
  inherited; //Parent is non-virtual and empty
  StatusEffects := TStatusEffectsList.Create(true);
  FEnchantmentsMultiplierCache := TEnchantmentCache.Create;
  FEnchantmentsAdditiveCache := TEnchantmentCache.Create;
  FEnchantmentsMaxCache := TEnchantmentCache.Create;
  FEnchantmentsMinCache := TEnchantmentCache.Create;
  IgnoredEntities := TReferenceList.Create;
  IgnoredTypes := TStringList.Create;
  IgnoredTypes.Sorted := true;
  IgnoredTypes.Duplicates := dupIgnore;
  Spells := TSpellsList.Create(true);
end;

procedure TInventory.CreatePaperDoll;
begin
  PaperDollSprite := TPaperDollSprite.Create;
  PaperDollSprite.Parent := Parent;
  // TODO: something more meaningful? This one will just blink an empty sprite for 1 frame.
  PaperDollSprite.UpdateToInventory; // just to avoid image = nil // TODO - optimize
end;

destructor TInventory.Destroy;
begin
  FreeAndNil(Spells);
  FreeAndNil(PaperDollSprite);
  FreeAndNil(StatusEffects);
  FreeAndNil(FEnchantmentsMultiplierCache);
  FreeAndNil(FEnchantmentsAdditiveCache);
  FreeAndNil(FEnchantmentsMaxCache);
  FreeAndNil(FEnchantmentsMinCache);
  FreeAndNil(IgnoredEntities);
  FreeAndNil(IgnoredTypes);
  inherited Destroy;
end;

end.


{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Database for blueprints }
unit GameBlueprintsDatabase;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  GameBlueprint;

var
  { List and dictionary of all loaded blueprints }
  BlueprintsList: TBlueprintsList;
  BlueprintsDictionary: TBlueprintsDictionary;

{ Clear all blueprints data}
procedure ClearBlueprints;
{ Load blueprints from package }
procedure LoadBlueprints(const Url: String);
{ Free all data related to a blueprint}
procedure FreeBlueprints;
implementation
uses
  CastleXmlUtils, CastleUriUtils,
  GameLog;

procedure ClearBlueprints;
begin
  if BlueprintsDictionary = nil then
  begin
    BlueprintsList := TBlueprintsList.Create(true);
    BlueprintsDictionary := TBlueprintsDictionary.Create([]);
  end else
  begin
    BlueprintsList.Clear;
    BlueprintsDictionary.Clear;
  end;
end;

procedure LoadBlueprints(const Url: String);
var
  Blueprint: TBlueprint;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
  Path: String;
begin
  LogNormal('Loading blueprints: %s', [Url]);
  Doc := URLReadXML(Url);
  Path := ExtractUriPath(Url);
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('Blueprint');
    try
      while Iterator.GetNext do
      begin
        Blueprint := TBlueprint.ReadClass(Path, Iterator.Current) as TBlueprint;
        BlueprintsList.Add(Blueprint);
        BlueprintsDictionary.Add(Blueprint.Id, Blueprint);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;

procedure FreeBlueprints;
begin
  FreeAndNil(BlueprintsList);
  FreeAndNil(BlueprintsDictionary);
end;

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameItemsDatabase;

{$INCLUDE compilerconfig.inc}

interface

uses
  GameApparelSlots, GameItemDataAbstract, GameItemData;

var
  { ... included bodyparts }
  ItemsDataDictionary: TItemsDataAbstractDictionary;
  { ... excludes bodyparts }
  ItemsDataList: TItemsDataList;

procedure ClearItemsAndBodypartsData;
procedure LoadItemsData(const Url: String);
procedure LoadBodypartsData(const Url: String);
procedure FreeItemsData;
{ ... included bodyparts }
function GetItemsForSlot(const ApparelSlot: TApparelSlot): TItemsDataAbstractList;
{ ... included bodyparts }
function GetRandomItemForSlot(const ApparelSlot: TApparelSlot): TItemDataAbstract;
implementation
uses
  SysUtils, DOM, Generics.Collections,
  CastleXMLUtils, CastleUriUtils,
  GameLog, GameRandom, GameBodypartData, GameEnumUtils;

procedure ClearItemsAndBodypartsData;
begin
  if ItemsDataDictionary = nil then
  begin
    ItemsDataDictionary := TItemsDataAbstractDictionary.Create([doOwnsValues]);
    ItemsDataList := TItemsDataList.Create(false)
  end else
  begin
    ItemsDataDictionary.Clear;
    ItemsDataList.Clear;
  end;
end;

procedure LoadItemsData(const Url: String);
var
  ItemData: TItemDataAbstract;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
  Path: String;
begin
  LogNormal('Loading items: %s', [Url]);
  Doc := URLReadXML(Url);
  Path := ExtractUriPath(Url);
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('ItemData');
    try
      while Iterator.GetNext do
      begin
        ItemData := TItemData.ReadClass(Path, Iterator.Current) as TItemData;
        ItemsDataDictionary.Add(ItemData.Id, ItemData);
        ItemsDataList.Add(ItemData as TItemData);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;

procedure LoadBodypartsData(const Url: String);
var
  BodypartData: TBodypartData;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
  Path: String;
begin
  LogNormal('Loading bodyparts: %s', [Url]);
  Doc := URLReadXML(Url);
  Path := ExtractUriPath(Url);
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('BodyPart');
    try
      while Iterator.GetNext do
      begin
        BodypartData := TBodypartData.ReadClass(Path, Iterator.Current) as TBodypartData;
        ItemsDataDictionary.Add(BodypartData.Id, BodypartData);
        //BodyPartsList.Add(ItemData);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;

function GetRandomItemForSlot(
  const ApparelSlot: TApparelSlot): TItemDataAbstract;
var
  DataList: TItemsDataAbstractList;
begin
  DataList := GetItemsForSlot(ApparelSlot);
  if DataList.Count = 0 then
    raise Exception.CreateFmt('No items for slot %s', [specialize EnumToStr<TApparelSlot>(ApparelSlot)]);
  Result := DataList[Rnd.Random(DataList.Count)];
  FreeAndNil(DataList);
end;

function GetItemsForSlot(const ApparelSlot: TApparelSlot): TItemsDataAbstractList;
var
  I: TItemDataAbstract;
begin
  Result := TItemsDataAbstractList.Create(false);
  for I in ItemsDataDictionary.Values do
    if ApparelSlot in I.EquipSlots then
      Result.Add(I);
end;

procedure FreeItemsData;
begin
  FreeAndNil(ItemsDataDictionary);
  FreeAndNil(ItemsDataList);
end;

end.


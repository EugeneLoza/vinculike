{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Data for bodypart: practically just a non abstract child of TItemDataAbstract}
unit GameBodypartData;

{$INCLUDE compilerconfig.inc}

interface

uses
  Generics.Collections,
  GameItemDataAbstract;

type
  TBodypartData = class(TItemDataAbstract)
    // looks like we don't need anything more than what abstract parent has.
  end;
  TBodypartsDataList = specialize TObjectList<TBodypartData>;
  TBodypartsDataDictionary = specialize TObjectDictionary<String, TBodypartData>;

implementation
uses
  GameSerializableData;

initialization
  RegisterSerializableData(TBodypartData);
end.


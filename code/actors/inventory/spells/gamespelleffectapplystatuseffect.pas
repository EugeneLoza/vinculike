{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameSpellEffectApplyStatusEffect;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameStatusEffect, GameSpellEffectAbstract;

type
  TSpellEffectApplyStatusEffect = class (TSpellEffectAbstract)
  public
    Effect: TStatusEffect;
    procedure Cast; override;
    function Description: String; override;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    destructor Destroy; override;
  end;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameSimpleSerializableObject,
  GamePlayerCharacter;

{$INCLUDE spelltypecast.inc}

procedure TSpellEffectApplyStatusEffect.Cast;
begin
  //inherited -- parent is abstract;
  case SpellTarget of
    stSelf: ParentPlayer.Inventory.AddStatusEffect(Effect.Clone);
    //stFriend: something's not right here, TODO
    else
      ; // ERROR/TODO
  end;
end;

function TSpellEffectApplyStatusEffect.Description: String;
begin
  Exit(Format('Applies status effect to %s: %s', [DescribeEffectTarget, Effect.Description]));
end;

procedure TSpellEffectApplyStatusEffect.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Effect.Save(Element.CreateChild('StatusEffect'));
end;

procedure TSpellEffectApplyStatusEffect.Load(const Element: TDOMElement);
begin
  inherited Load(Element);
  Effect := LoadClass(Element.Child('StatusEffect')) as TStatusEffect;
end;

destructor TSpellEffectApplyStatusEffect.Destroy;
begin
  FreeAndNil(Effect);
  inherited Destroy;
end;

initialization
  RegisterSimpleSerializableObject(TSpellEffectApplyStatusEffect);
end.


{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Generic parent for all spell effects}
unit GameSpellEffectAbstract;

{$INCLUDE compilerconfig.inc}

interface

uses
  Generics.Collections, DOM,
  GameSimpleSerializableObject;

type
  { Note that a single spell can have multiple different targets in each effect }
  TSpellTarget = (stSelf, stFriend, stFoe);

type
  TSpellEffectAbstract = class abstract (TSimpleSerializableObject)
  protected
    function DescribeEffectTarget: String;
  public
    Parent: TObject; // cannot typecast to PlayerCharacter
    { what does it affect }
    SpellTarget: TSpellTarget;
    { Execute this effect}
    procedure Cast; virtual; abstract;
    function Description: String; virtual; abstract;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  end;
  TSpellEffectsList = specialize TObjectList<TSpellEffectAbstract>;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameLog, GameEnumUtils;

type
  ESpellTargetConversionError = class(Exception);

function TSpellEffectAbstract.DescribeEffectTarget: String;
begin
  case SpellTarget of
    stSelf: Exit('Self');
    stFriend: Exit('Friend');
    stFoe: Exit('Foe');
    else
    begin
      ShowError('Unexpected SpellTarget: %s', [specialize EnumToStr<TSpellTarget>(SpellTarget)]);
      Exit('Unknown');
    end;
  end;
end;

procedure TSpellEffectAbstract.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('SpellTarget', specialize EnumToStr<TSpellTarget>(SpellTarget));
end;

procedure TSpellEffectAbstract.Load(const Element: TDOMElement);
begin
  inherited Load(Element);
  SpellTarget := specialize StrToEnum<TSpellTarget>(Element.AttributeString('SpellTarget'));
end;

end.


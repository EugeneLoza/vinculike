{$IFDEF SafeTypecast}
{$DEFINE ParentPlayer:=(Parent as TPlayerCharacter)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE TargetMonster:=(Target as TMonster)}
{$ELSE}
{$DEFINE ParentPlayer:=TPlayerCharacter(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE TargetMonster:=TMonster(Target)}
{$ENDIF}


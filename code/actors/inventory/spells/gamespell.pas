{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

 { Spells are containers for a list of spell effects
   can be "cast" by the Player at a price of magic
   by itself the spell is just metadata and actual execution
   happens inside Effects }
unit GameSpell;

{$INCLUDE compilerconfig.inc}

interface

uses
  Generics.Collections, DOM,
  GameSimpleSerializableObject,
  GameSpellEffectAbstract;

// Spell can always target self
// Spell can target either friend or foe, but not both (? why not - can heal allies and hurt enemies in the same area )
// Spell can create a mark with IFF-per-effect (having several effects targeting either friend or foe)

type
  { Container and metadata for the spell }
  TSpell = class(TSimpleSerializableObject)
  public
    Parent: TObject; // cannot typecast to PlayerCharacter
    { Spell name shown to the Player }
    DisplayName: String;
    { Base cost of this spell in Magic }
    Price: Single;
    { List of effects this spell has }
    // todo: throwing a mark should automatically be detected from those; however maybe let the spell able to have > 1 mark?
    Effects: TSpellEffectsList;
    { Deduce the cost and execute all spell effects}
    procedure Cast;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;
  TSpellsList = specialize TObjectList<TSpell>;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GamePlayerCharacter, GameColors;

{$INCLUDE spelltypecast.inc}

procedure TSpell.Cast;
var
  I: Integer;
begin
  ParentPlayer.Particle('SPELL', ColorParticleChat);
  ParentPlayer.DegradeMagic(Price);
  for I := 0 to Pred(Effects.Count) do
    Effects[I].Cast;
end;

procedure TSpell.Save(const Element: TDOMElement);
var
  I: Integer;
begin
  inherited Save(Element);
  Element.AttributeSet('DisplayName', DisplayName);
  for I := 0 to Pred(Effects.Count) do
    Effects[I].Save(Element.CreateChild('SpellEffect'));
end;

procedure TSpell.Load(const Element: TDOMElement);
var
  Iterator: TXMLElementIterator;
  SE: TSpellEffectAbstract;
begin
  inherited;
  DisplayName := Element.AttributeString('DisplayName');
  Iterator := Element.ChildrenIterator('SpellEffect');
  try
    while Iterator.GetNext do
    begin
      SE := TSpellEffectAbstract.LoadClass(Iterator.Current) as TSpellEffectAbstract;
      SE.AfterDeserealization;
      SE.Parent := Parent;
      Effects.Add(SE);
    end;
  finally FreeAndNil(Iterator) end;
end;

constructor TSpell.Create;
begin
  inherited;
  Effects := TSpellEffectsList.Create(true);
end;

destructor TSpell.Destroy;
begin
  FreeAndNil(Effects);
  inherited Destroy;
end;

initialization
  RegisterSimpleSerializableObject(TSpell);
end.


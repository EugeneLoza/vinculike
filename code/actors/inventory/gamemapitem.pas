{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMapItem;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, Generics.Collections, DOM,
  GameSerializableObject,
  GamePositionedObject, GameInventoryItem;

type
  TMapItem = class(TPositionedObject)
  public
    { ReferenceID for actor that interacts with this map item
      used to avoid two actors interacting with the same item simultaneously
      if equals to zero, then nobody is interacting with this item }
    InteractingActorReferenceID: UInt64;
    Item: TInventoryItem;
  public const Signature = 'mapitem';
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    class procedure DropItem(const AX, AY: Int16; const AItem: TInventoryItem; const SimpleDrop: Boolean);
    class procedure DropItems(const AX, AY: Int16; const AItemsList: TInventoryItemsList);
    destructor Destroy; override;
  end;
  TMapItemsList = specialize TObjectList<TMapItem>;

implementation
uses
  CastleXmlUtils,
  GameRandom, GameMap, GameMapTypes, GameLog,
  GameViewGame;

type
  EMapItemLoadError = class(Exception);

procedure TMapItem.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Item.Save(Element.CreateChild(TInventoryItem.Signature));
  Element.AttributeSet('InteractingActorReferenceID', InteractingActorReferenceID);
end;

procedure TMapItem.Load(const Element: TDOMElement);
begin
  inherited;
  try
    Item := TInventoryItem.LoadClass(Element.Child(TInventoryItem.Signature, true)) as TInventoryItem;
    InteractingActorReferenceID := Element.AttributeQWordDef('InteractingActorReferenceID', 0);
  except
    on E: Exception do
    begin
      try
        if Element.Child(TInventoryItem.Signature, false) = nil then
          ShowError('%s:"%s". Map item doesn''t contain item record', [E.ClassName, E.Message])
        else
          ShowError('%s:"%s". Failed to load map item %s', [E.ClassName, E.Message, Element.Child(TInventoryItem.Signature, true).AttributeStringDef('Data.Id', 'N/A')]);
      except
        ShowError('Exception while trying to report exception in TMapItem.Load');
      end;
      raise EMapItemLoadError.Create('Failed to load map item');
    end;
  end;
  SetSize(1);
end;

class procedure TMapItem.DropItem(const AX,
  AY: Int16; const AItem: TInventoryItem; const SimpleDrop: Boolean);
var
  NX, NY: Int16;
  MapItem: TMapItem;

  function CanPutHere: Boolean;
  var
    I: TMapItem;
  begin
    if not Map.CanMove(NX + Map.SizeX * NY) then
      Exit(false);
    // Temporary TODO
    if (NX >= Map.ExitX - 3) and (NX <= Map.ExitX + 6) and
      (NY >= Map.ExitY - 3) and (NY <= Map.ExitY + 6) then
        Exit(false);
    for I in Map.MapItemsList do
      if (I.LastTileX = NX) and (I.LastTileY = NY) then
        Exit(false);
    Exit(true);
  end;

  procedure DoDrop;
  begin
    MapItem := TMapItem.Create(true);
    MapItem.InteractingActorReferenceID := 0;
    MapItem.Item := AItem;
    MapItem.SetSize(1);
    MapItem.MoveMeTo(NX, NY);
    Map.MapItemsList.Add(MapItem);
    ViewGame.InvalidatePosition(nil);
  end;

  procedure DoSimpleDrop;
  begin
    while not CanPutHere do
    begin
      NX += Rnd.Random(3) - 1;
      NY += Rnd.Random(3) - 1;
      if NX < 1 then NX := 1;
      if NY < 1 then NY := 1;
      if NX > Map.PredSizeX - 1 then NX := Map.PredSizeX - 1;
      if NY > Map.PredSizeY - 1 then NY := Map.PredSizeY - 1;
    end;
    DoDrop;
  end;

var
  FakeList: TInventoryItemsList;
begin
  NX := AX;
  NY := AY;
  if (NX <= 0) or (NY <= 0) or (NX >= Map.PredSizeX) or (NY >= Map.PredSizeY) then
  begin
    ShowError('ERROR: Can''t put item %s at %d,%d.', [AItem.Data.Id, AX, AY]);
    if NX < 1 then NX := 1;
    if NY < 1 then NY := 1;
    if NX > Map.PredSizeX - 1 then NX := Map.PredSizeX - 1;
    if NY > Map.PredSizeY - 1 then NY := Map.PredSizeY - 1;
  end;

  if CanPutHere then
    DoDrop
  else
  if SimpleDrop then
    DoSimpleDrop
  else
  if not Map.PassableTiles[0][NX + Map.SizeX * NY] then
  begin
    // Handle a critical issue - we're trying to put item over the wall; distance map may fail in this case
    ShowError('ERROR: Cannot put item %s at (%d, %d) the tile is not passable', [AItem.Data.Id, NX, NY]);
    DoSimpleDrop;
  end else
  begin
    // to avoid repeating the algorithm again, it's not too optimal, but shouldn't be too big problem
    FakeList := TInventoryItemsList.Create(false);
    FakeList.Add(AItem);
    DropItems(AX, AY, FakeList);
    FreeAndNil(FakeList);
  end;
end;

class procedure TMapItem.DropItems(const AX,
  AY: Int16; const AItemsList: TInventoryItemsList);
var
  FloodFillMap: packed array of Byte;
  CurrentPass: Byte;
  PreviousPass: Byte;
  X1, Y1, X2, Y2: Int16;

  procedure DropItemsPass;
  var
    IX, IY: Int16;
    I: SizeInt;
  begin
    for IY := Y1 to Y2 do
    begin
      I := X1 + Map.SizeX * IY;
      for IX := X1 to X2 do
      begin
        if ((FloodFillMap[I] = 0) or (FloodFillMap[I] = 254))
          and
           ((CurrentPass = 1) or
            (FloodFillMap[I + 1] = PreviousPass) or
            (FloodFillMap[I - 1] = PreviousPass) or
            (FloodFillMap[I + Map.SizeX] = PreviousPass) or
            (FloodFillMap[I - Map.SizeX] = PreviousPass)) then
        begin
          if FloodFillMap[I] = 0 then
          begin
            TMapItem.DropItem(IX, IY, AItemsList[0], true);
            AItemsList.Delete(0);
            if AItemsList.Count = 0 then
              Exit;
          end;
          FloodFillMap[I] := CurrentPass;
          if (IX = X1) and (X1 > 1) then
            Dec(X1);
          if (IX = X2) and (X2 < Map.PredSizeX) then
            Inc(X2);
          if (IY = Y1) and (Y1 > 1) then
            Dec(Y1);
          if (IY = Y2) and (Y2 < Map.PredSizeY) then
            Inc(Y2);
        end;
        Inc(I);
      end;
    end;
  end;

var
  J: SizeInt;
  JX, JY: Int16;
begin
  if AItemsList.Count = 0 then
    Exit;
  {if AItemsList.Count = 1 then
  begin
    Map.MapItemsList.Add(CreateItem(AX, AY, AItemsList[0], false));
    AItemsList.Clear;
    Exit;
  end; ----- ends up in in an infinite loop and stack overflow}

  if not Map.PassableTiles[0][AX + Map.SizeX * AY] then
  begin
    ShowError('ERROR: Cannot put items list at (%d, %d) the tile is not passable', [AX, AY]);
    for J := 0 to Pred(AItemsList.Count) do
      DropItem(AX, AY, AItemsList[J], true);
    AItemsList.Clear;
  end else
  begin
    FloodFillMap := nil;
    SetLength(FloodFillMap, Map.SizeX * Map.SizeY);
    //FillByte(FloodFillMap[0], Length(FloodFillMap) * SizeOf(Byte), 0); // 0 = undefined

    for J := 0 to Pred(Length(Map.PassableTiles[0])) do
      if Map.PassableTiles[0][J] then
        FloodFillMap[J] := 0
      else
        FloodFillMap[J] := 255;
    for J := 0 to Pred(Map.MapItemsList.Count) do
      FloodFillMap[Map.MapItemsList[J].LastTile] := 254;
    for JY := Map.ExitY - 3 to Map.ExitY + 6 do
      for JX := Map.ExitX - 3 to Map.ExitX + 6 do
        if (JY > 0) and (JY < Map.PredSizeY) and (JX > 0) and (JX < Map.PredSizeX) and (FloodFillMap[JX + Map.SizeX * JY] = 0) then
          FloodFillMap[JX + Map.SizeX * JY] := 254;

    X1 := AX;
    Y1 := AY;
    X2 := AX;
    Y2 := AY;
    CurrentPass := 0;
    repeat
      PreviousPass := CurrentPass;
      Inc(CurrentPass);
      if CurrentPass = 254 then
      begin
        ShowError('Cannot drop items! Max range 254 tiles');
        Break;
      end;
      DropItemsPass;
    until AItemsList.Count = 0;
    FloodFillMap := nil;
  end;
  ViewGame.InvalidatePosition(nil);
end;

destructor TMapItem.Destroy;
begin
  FreeAndNil(Item);
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TMapItem);
end.


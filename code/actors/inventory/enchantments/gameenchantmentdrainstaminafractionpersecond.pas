{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentDrainStaminaFractionPerSecond;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Drains a fraction of stamina per second }
  TEnchantmentDrainStaminaFractionPerSecond = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject;const Quality: Single; const CanBeNegative: Boolean); override;
    function IdentificationComplexity(const AQuality: Single): Single; override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
    procedure Update(const AParentActor: TObject; const AParentItem: TObject;
      const SecondsPassed: Single); override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath,
  GamePlayerCharacter;

procedure TEnchantmentDrainStaminaFractionPerSecond.Validate;
begin
  inherited Validate;
  if Strength <= 0 then
    raise EDataValidationError.CreateFmt('Strength <= 0 in %s', [Self.ClassName]);
  if Strength > 1 then
    raise EDataValidationError.CreateFmt('Strength > 1 in %s', [Self.ClassName]);
end;

procedure TEnchantmentDrainStaminaFractionPerSecond.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  Strength := Clamp(Sqr(Rnd.Random) * Quality / 200, 0.005, 0.03);
end;

function TEnchantmentDrainStaminaFractionPerSecond.IdentificationComplexity(const AQuality: Single): Single;
begin
  Exit(3.0 * Rnd.Random * AQuality);
end;

function TEnchantmentDrainStaminaFractionPerSecond.IsPositive: Boolean;
begin
  Exit(false);
end;

function TEnchantmentDrainStaminaFractionPerSecond.Description: String;
begin
  Result := Format('Drains %.1n%%%% of current stamina per second' + RequirementDescription, [100 * Strength]);
end;

procedure TEnchantmentDrainStaminaFractionPerSecond.Update(const AParentActor: TObject;
  const AParentItem: TObject; const SecondsPassed: Single);
begin
  inherited Update(AParentActor, AParentItem, SecondsPassed);
  if (AParentActor as TPlayerCharacter).Stamina > 0 then
    (AParentActor as TPlayerCharacter).DegradeStamina((AParentActor as TPlayerCharacter).Stamina * Strength * SecondsPassed);
end;

initialization
  RegisterSerializableData(TEnchantmentDrainStaminaFractionPerSecond);
  RegisterRandomEnchantment(TEnchantmentDrainStaminaFractionPerSecond, 0.3, ekNegative);
  RegisterEnchantment(TEnchantmentDrainStaminaFractionPerSecond);

end.


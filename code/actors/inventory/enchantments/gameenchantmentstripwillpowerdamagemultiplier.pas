{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentStripWillpowerDamageMultiplier;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Multiplier to willpower damage received after forced stripping }
  TEnchantmentStripWillpowerDamageMultiplier = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath;

procedure TEnchantmentStripWillpowerDamageMultiplier.Validate;
begin
  inherited Validate;
  if Strength < 0 then
    raise EDataValidationError.CreateFmt('Strength < 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentStripWillpowerDamageMultiplier.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  if CanBeNegative and Rnd.RandomBoolean then
    Strength := 1 + Clamp(Sqr(Rnd.Random) * Quality * 2, 0.2, 100)
  else
    Strength := 1 - Clamp(Sqr(Rnd.Random) * Quality / 2, 0.1, 1);
  // those don't make sense with this enchantment
  RequiresNudeBottom := false;
  RequiresNudeTop := false;
end;

function TEnchantmentStripWillpowerDamageMultiplier.IsPositive: Boolean;
begin
  Exit(Strength < 1);
end;

function TEnchantmentStripWillpowerDamageMultiplier.Description: String;
begin
  if Strength > 1 then
    Result := Format('Increases damage from forced stripping by %d%%%%' + RequirementDescription, [Round(100 * (Strength - 1))])
  else
    Result := Format('Decreases damage from forced stripping by %d%%%%' + RequirementDescription, [Round(100 * (1 - Strength))]);
end;

initialization
  RegisterSerializableData(TEnchantmentStripWillpowerDamageMultiplier);
  RegisterRandomEnchantment(TEnchantmentStripWillpowerDamageMultiplier, 1.0, ekBoth);
  RegisterEnchantment(TEnchantmentStripWillpowerDamageMultiplier);

end.


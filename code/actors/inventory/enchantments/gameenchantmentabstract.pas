{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentAbstract;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM, Generics.Collections,
  CastleGlImages,
  GameSerializableData; // TODO: Refactor

type
  TEnchantmentKind = (ekNeutral, ekPositive, ekNegative, ekBoth);
  TEnchantmentKindSet = Set of TEnchantmentKind;

const AllEnchantmentKindsAllowed: TEnchantmentKindSet = [ekNeutral, ekPositive, ekNegative, ekBoth];

type
  { Enchantment is some effect that affects the parent item or actor
    todo: not all enchantments can be status effect (need to be item-agnostic)
    can offer some persistent multiplier (which is cached and detected in Player Character class)
    or directly and actively interact with other in-game objects, first of all in Update
    Note: not all enchantments can be random
    Todo: events (e.g. on unequip)
  }
  TEnchantmentClass = class of TEnchantmentAbstract;
  TEnchantmentAbstract = class(TSerializableData)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
    { Create a copy of this class (only data, not state }
    function CloneInternal: TEnchantmentAbstract; virtual;
    { Initialize this enchantment with random data }
    procedure RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean); virtual;
    function IdentificationComplexity(const AQuality: Single): Single; virtual;
  public
    { How hard it is to identify this item?
      success chance depends on character's skill and time the item was worn
      see TActionPlayerIdentifyItem for calculation formula }
    IdentificationThreshold: Single;
    { Some generic "strength" of this enchantment,
      interpreted differently in a specific enchantment class }
    Strength: Single;
    { Was this enchantment identified? }
    Identified: Boolean;
    { Is this enchantment randomly generated or deterministically attacked to the item? }
    IsStochastic: Boolean;
    { Requirements of this enchantment.
      some sort of ECS could do a better job (a list of classes attached to this enchantment),
      but looks like an overkill, at least for now - both complicates code and data }
    RequiresNudeTop: Boolean;
    RequiresDressedTop: Boolean;
    RequiresNudeBottom: Boolean;
    RequiresDressedBottom: Boolean;
    RequiresUnarmed: Boolean;
    RequiresBarefoot: Boolean;
    RequiresBlindfolded: Boolean;
    RequiresLeashHeld: Boolean;
    RequiresHealthBelow: Single;
    { List of items, to be required for this enchantment to work }
    RequiresItems: TStringList;
    { If equipping item with this enchantment identifies it automatically
      (if the enchainment effect becomes immediately obvious )
      also requires the Requirements to be meet }
    function EquipIdentifies: Boolean; virtual;
    { Description of the enchantment requirements visible to the Player }
    function RequirementDescription: String;
    { Description of the enchantment visible to the Player }
    function Description: String; virtual; abstract; // Temporary: TODO
    { Create a copy of this enchantment }
    function Clone: TEnchantmentAbstract;
    function Image: TDrawableImage;
    function IsPositive: Boolean; virtual; abstract;
    { Typecasted class of this enchantment
      Wrapper for Self.ClassType }
    function EnchantmentClass: TEnchantmentClass;
    { Called every frame }
    procedure Update(const AParentActor: TObject; const AParentItem: TObject; const SecondsPassed: Single); virtual;
    { Generate a random enchantment
      for specific item (parent)
      of given quality (determines strength of this enchantment)
      can be required to have only positive enchantments
      For now let's assume Quality = Sqrt(dungeon level) }
    class function Random(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean): TEnchantmentAbstract;
    class function MakesSense(const AItem: TObject): Boolean; virtual;
    { makes sure this enchantment finished all its tasks }
    procedure Stop; virtual;
    { if this enchantment can be saved properly on the save game
      some enchantments can have additional data that doesn't have "save" implemented
      it can be implemented, but that may be a lot of additional work for no apartment benefit
      currently: enchantment that throws marks do not save marks }
    function CanBeSaved: Boolean; virtual;
    function CanBeStatusEffect: Boolean; virtual;
    procedure Save(const Element: TDOMElement); virtual; //todo: override
  public
    constructor Create; //virtual;
    destructor Destroy; override;
  end;
  TEnchantmentsList = specialize TObjectList<TEnchantmentAbstract>;
  TEnchantmentClassesList = specialize TList<TEnchantmentClass>;
  TEnchantmentImagesCache = specialize TObjectDictionary<String, TDrawableImage>;
  TEnchantmentCache = specialize TDictionary<TEnchantmentClass, Single>;

type
  TEnchantmentPeriodicAbstract = class abstract(TEnchantmentAbstract)
  strict private
    LastSecondTime: Single;
  protected
    EffectPeriod: Single;
    procedure Perform(const AParentActor: TObject; const AParentItem: TObject); virtual; abstract;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
    function CloneInternal: TEnchantmentAbstract; override;
  public
    procedure Save(const Element: TDOMElement); override;
    procedure Update(const AParentActor: TObject; const AParentItem: TObject;
      const SecondsPassed: Single); override;
  end;

{ call this on an enchantment class to enable it being randomly added to items }
procedure RegisterRandomEnchantment(const AClass: TEnchantmentClass;
  const AFrequency: Single; const AKind: TEnchantmentKind);
procedure RegisterEnchantment(const AClass: TEnchantmentClass);
procedure InitEnchantments;
procedure FreeEnchantments;
implementation
uses
  CastleXmlUtils, CastleStringUtils, CastleUriUtils,
  GameCachedImages, GameItemsDatabase,
  GameApparelSlots, GameInventoryItem, GameRandom, GameMath;

type
  { Additional info for stochastic enchantments about how they should be randomly generated }
  TRandomEnchantmentMetadata = record
    { Chance to pick this enchantment for randomly generated one }
    Frequency: Single;
    { What kind of enchantment can be randomly generated:
      positive, negative or either?
      used to spawn items with only positive enchantments }
    Kind: TEnchantmentKind;
    { Class of the enchantment to spawn }
    EnchantmentClass: TEnchantmentClass;
  end;
  TRandomEnchantmentsList = specialize TList<TRandomEnchantmentMetadata>;

var
  { Enchantments that can be randomly added to an item }
  RandomEnchantmentsList: TRandomEnchantmentsList;

  EnchantmentImagesCache: TEnchantmentImagesCache;

type
  { Clone and save to CSV string }
  TStringListHelper = class helper for TStringList // TODO
  public
    { Return a TStringList with content exactly matching that of this one
      Note: list is loaded as TCastleStringList through CreateTokens, so even class type can mismatch }
    function Clone: TStringList;
    { Encode contents of this string list as a single CSV string
      Warning: we do not validate if elements contain comma symbol or has duplicates }
    function ToCsv: String;
  end;

function TStringListHelper.Clone: TStringList;
var
  I: Integer;
begin
  Result := TStringList.Create;
  Result.Capacity := Count;
  for I := 0 to Pred(Count) do
    Result.Add(Self[I]);
end;

function TStringListHelper.ToCsv: String;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to Pred(Count) do
  begin
    if I > 0 then
      Result := Result + ',';
    Result := Result + Self[I];
  end;
end;

procedure TEnchantmentAbstract.Validate;
begin
  //inherited Validate; parent is abstract
  if RequiresNudeTop and RequiresDressedTop then
    raise EDataValidationError.CreateFmt('RequiresNudeTop and RequiresDressedTop in %s', [Self.ClassName]);
  if RequiresNudeBottom and RequiresDressedBottom then
    raise EDataValidationError.CreateFmt('RequiresNudeBottom and RequiresDressedBottom in %s', [Self.ClassName]);
  if RequiresHealthBelow <= 0 then
    raise EDataValidationError.CreateFmt('RequiresHealthBelow <= 0 in %s', [Self.ClassName]);
  if (RequiresItems <> nil) and (RequiresItems.Count = 0) then
    raise EDataValidationError.CreateFmt('RequiresItems.Count = 0 in %s', [Self.ClassName]);
  if CanBeStatusEffect and not CanBeSaved then
    raise EDataValidationError.CreateFmt('CanBeStatusEffect and not CanBeSaved in %s', [Self.ClassName]);
end;

procedure TEnchantmentAbstract.Read(const Element: TDOMElement);
var
  RequiresItemsString: String;
begin
  //inherited Read(Element); Parent is abstract

  Strength := Element.AttributeSingle('Strength');

  RequiresNudeTop := Element.AttributeBooleanDef('RequiresNudeTop', false);
  RequiresDressedTop := Element.AttributeBooleanDef('RequiresDressedTop', false);
  RequiresNudeBottom := Element.AttributeBooleanDef('RequiresNudeBottom', false);
  RequiresDressedBottom := Element.AttributeBooleanDef('RequiresDressedBottom', false);
  RequiresUnarmed := Element.AttributeBooleanDef('RequiresUnarmed', false);
  RequiresBarefoot := Element.AttributeBooleanDef('RequiresBarefoot', false);
  RequiresBlindfolded := Element.AttributeBooleanDef('RequiresBlindfolded', false);
  RequiresHealthBelow := Element.AttributeSingleDef('RequiresHealthBelow', 1.0);
  RequiresLeashHeld := Element.AttributeBooleanDef('RequiresLeashHeld', false);
  IdentificationThreshold := Element.AttributeSingleDef('IdentificationThreshold', 0);
  Identified := Element.AttributeBooleanDef('Identified', true);
  IsStochastic := Element.AttributeBooleanDef('IsStochastic', false);

  if Element.AttributeString('RequiresItems', RequiresItemsString) then
    RequiresItems := CreateTokens(RequiresItemsString, [','])
  else
    RequiresItems := nil; // redundant
end;

procedure TEnchantmentAbstract.Save(const Element: TDOMElement);
begin
  //inherited; todo
  Element.AttributeSet('Class', ClassName);

  Element.AttributeSet('Strength', Strength);
  Element.AttributeSet('IdentificationThreshold', IdentificationThreshold);
  Element.AttributeSet('Identified', Identified);
  Element.AttributeSet('IsStochastic', IsStochastic);

  Element.AttributeSet('RequiresNudeTop', RequiresNudeTop);
  Element.AttributeSet('RequiresDressedTop', RequiresDressedTop);
  Element.AttributeSet('RequiresNudeBottom', RequiresNudeBottom);
  Element.AttributeSet('RequiresDressedBottom', RequiresDressedBottom);
  Element.AttributeSet('RequiresUnarmed', RequiresUnarmed);
  Element.AttributeSet('RequiresBarefoot', RequiresBarefoot);
  Element.AttributeSet('RequiresBlindfolded', RequiresBlindfolded);
  Element.AttributeSet('RequiresHealthBelow', RequiresHealthBelow);
  Element.AttributeSet('RequiresLeashHeld', RequiresLeashHeld);

  if RequiresItems <> nil then
    Element.AttributeSet('RequiresItems', RequiresItems.ToCsv);
end;

constructor TEnchantmentAbstract.Create;
begin
  //inherited - parent is non virtual
  // clear all requirements to "none'
  RequiresNudeTop := false;
  RequiresDressedTop := false;
  RequiresNudeBottom := false;
  RequiresDressedBottom := false;
  RequiresUnarmed := false;
  RequiresBarefoot := false;
  RequiresBlindfolded := false;
  RequiresHealthBelow := 1.0;
  RequiresLeashHeld := false;
end;

function TEnchantmentAbstract.RequirementDescription: String;
var
  I: Integer;
begin
  if RequiresNudeTop and RequiresNudeBottom then
    Result := ' when nude'
  else
  if RequiresDressedTop and RequiresDressedBottom then
    Result := ' when properly dressed'
  else
  if RequiresNudeTop and RequiresDressedBottom then
    Result := ' when barechested but bottom covered'
  else
  if RequiresNudeBottom and RequiresDressedTop then
    Result := ' when bottom not covered but breasts are'
  else
  if RequiresNudeTop then
    Result := ' when barechested'
  else
  if RequiresNudeBottom then
    Result := ' when bottom not covered'
  else
  if RequiresDressedTop then
    Result := ' when breasts covered'
  else
  if RequiresDressedBottom then
    Result := ' when bottom covered'
  else
    Result := '';

  if RequiresUnarmed then
  begin
    if Result = '' then
      Result += ' when '
    else
      Result += ' and ';
    Result += 'unarmed';
  end;

  if RequiresBarefoot then
  begin
    if Result = '' then
      Result += ' when '
    else
      Result += ' and ';
    Result += 'barefoot';
  end;

  if RequiresBlindfolded then
  begin
    if Result = '' then
      Result += ' when '
    else
      Result += ' and ';
    Result += 'blindfolded';
  end;

  if RequiresHealthBelow < 1.0 then
  begin
    if Result = '' then
      Result += ' when '
    else
      Result += ' and ';
    Result += 'health is below ' + Round(100 * RequiresHealthBelow).ToString() + '%%%%';
  end;

  if RequiresLeashHeld then
  begin
    if Result = '' then
      Result += ' when '
    else
      Result += ' and ';
    Result += 'held on a leash';
  end;

  if RequiresItems <> nil then
  begin
    // RequiresItems.Count = 0 is impossible, filtered by validation
    if Result = '' then
      Result += ' when '
    else
      Result += ' and ';
    Result += 'worn together with ';
    for I := 0 to Pred(RequiresItems.Count) do
    begin
      if (I > 0) and (I = Pred(RequiresItems.Count)) then
        Result += ' and '
      else
      if (I > 0) then
        Result += ', ';
      Result += ItemsDataDictionary[RequiresItems[I]].DisplayName;
    end;
  end;
end;

function TEnchantmentAbstract.CloneInternal: TEnchantmentAbstract;
begin
  Result := ClassType.Create as TEnchantmentAbstract;
  TEnchantmentAbstract(Result).Strength := Strength;
  TEnchantmentAbstract(Result).RequiresNudeTop := RequiresNudeTop;
  TEnchantmentAbstract(Result).RequiresDressedTop := RequiresDressedTop;
  TEnchantmentAbstract(Result).RequiresNudeBottom := RequiresNudeBottom;
  TEnchantmentAbstract(Result).RequiresDressedBottom := RequiresDressedBottom;
  TEnchantmentAbstract(Result).RequiresUnarmed := RequiresUnarmed;
  TEnchantmentAbstract(Result).RequiresBarefoot := RequiresBarefoot;
  TEnchantmentAbstract(Result).RequiresBlindfolded := RequiresBlindfolded;
  TEnchantmentAbstract(Result).RequiresHealthBelow := RequiresHealthBelow;
  TEnchantmentAbstract(Result).RequiresLeashHeld := RequiresLeashHeld;
  TEnchantmentAbstract(Result).Identified := Identified;
  TEnchantmentAbstract(Result).IdentificationThreshold := IdentificationThreshold;
  TEnchantmentAbstract(Result).IsStochastic := IsStochastic;
  if RequiresItems <> nil then
    TEnchantmentAbstract(Result).RequiresItems := RequiresItems.Clone
  else
    TEnchantmentAbstract(Result).RequiresItems := nil;
end;

function TEnchantmentAbstract.Clone: TEnchantmentAbstract;
begin
  Result := CloneInternal;
  {$IFDEF ValidateData}
  Result.Validate;
  {$ENDIF}
end;

function TEnchantmentAbstract.Image: TDrawableImage;
begin
  if IsPositive then
    Exit(EnchantmentImagesCache[Self.ClassName + '_positive'])
  else
    Exit(EnchantmentImagesCache[Self.ClassName + '_negative']);
end;

function TEnchantmentAbstract.EnchantmentClass: TEnchantmentClass;
begin
  Exit(TEnchantmentClass(ClassType));
end;

procedure TEnchantmentAbstract.RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  if not CanBeSaved then
    raise Exception.CreateFmt('%s cannot be a random enchantment', [ClassName]);

  {$IFDEF SafeTypecast}
  {$DEFINE ParentItem:=(AParent as TInventoryItem)}
  {$ELSE}
  {$DEFINE ParentItem:=TInventoryItem(AParent)}
  {$ENDIF}

  // Strength will be assigned by children!

  Identified := false;
  IsStochastic := true;

  // Random requirements
  if Rnd.Random < 0.4 then // 40% chance to have any requirements; note none of the below may actually manifest, so true chance is lower than this value
  begin
    if (Rnd.Random < 0.3) and (not ParentItem.ItemData.CoversTop) then
    begin
      RequiresNudeTop := Rnd.RandomBoolean;
      RequiresDressedTop := not RequiresNudeTop;
    end;
    if (Rnd.Random < 0.3) and (not ParentItem.ItemData.CoversBottom) then
    begin
      RequiresNudeBottom := Rnd.RandomBoolean;
      RequiresDressedBottom := not RequiresNudeBottom;
    end;

    if (Rnd.Random < 0.08) and (not ParentItem.ItemData.IsWeaponOrMakesWeaponImpossible) then
      RequiresUnarmed := true;
    if (Rnd.Random < 0.10) and (not ParentItem.ItemData.IsFootwearOrMakesFootwearImpossible) then
      RequiresBarefoot := true;
    if (Rnd.Random < 0.05) and (not ParentItem.IsBlindfold(nil)) then
      RequiresBlindfolded := true;

    if Rnd.Random < 0.09 then
      RequiresHealthBelow := Clamp(Sqrt(Sqrt(Rnd.Random)), 0.2, 0.9);

    //RequiresLeashHeld never on stochastic?
  end;

  // No RequiresItems in random enchantments, at least for now
end;

function TEnchantmentAbstract.EquipIdentifies: Boolean;
begin
  Exit(false);
end;

function TEnchantmentAbstract.IdentificationComplexity(const AQuality: Single): Single;
begin
  Exit(2.0 * Rnd.Random * AQuality);
end;

class function TEnchantmentAbstract.Random(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean): TEnchantmentAbstract;
var
  I: Integer;
  AllowedKinds: TEnchantmentKindSet;
begin
  AllowedKinds := AllEnchantmentKindsAllowed;
  if not CanBeNegative then
    AllowedKinds -= [ekNegative];
  repeat
    I := Rnd.Random(RandomEnchantmentsList.Count);
  until (Rnd.Random < RandomEnchantmentsList[I].Frequency) and
    (RandomEnchantmentsList[I].Kind in AllowedKinds) and
    RandomEnchantmentsList[I].EnchantmentClass.MakesSense(AParent); {$WARNING can freeze here}
  Result := RandomEnchantmentsList[I].EnchantmentClass.Create;
  Result.RandomInternal(AParent, Quality, CanBeNegative);
  // we put it here, because it may use parameters of the enchantment (e.g. strength)
  Result.IdentificationThreshold := Result.IdentificationComplexity(Quality);
  Result.Validate;
end;

class function TEnchantmentAbstract.MakesSense(const AItem: TObject): Boolean;
begin
  Exit(true);
end;

procedure TEnchantmentAbstract.Update(const AParentActor: TObject;
  const AParentItem: TObject; const SecondsPassed: Single);
begin
  // do nothing;
end;

procedure TEnchantmentAbstract.Stop;
begin
  // do nothing;
end;

function TEnchantmentAbstract.CanBeSaved: Boolean;
begin
  Exit(true);
end;

function TEnchantmentAbstract.CanBeStatusEffect: Boolean;
begin
  Exit(true);
end;

destructor TEnchantmentAbstract.Destroy;
begin
  FreeAndNil(RequiresItems);
  inherited Destroy;
end;

{ TEnchantmentPeriodicAbstract }

procedure TEnchantmentPeriodicAbstract.Validate;
begin
  inherited Validate;
  if EffectPeriod <= 0 then
    raise EDataValidationError.CreateFmt('EffectPeriod <= 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentPeriodicAbstract.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  EffectPeriod := Element.AttributeSingle('EffectPeriod');
end;

procedure TEnchantmentPeriodicAbstract.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('EffectPeriod', EffectPeriod);
end;

function TEnchantmentPeriodicAbstract.CloneInternal: TEnchantmentAbstract;
begin
  Result := inherited CloneInternal;
  TEnchantmentPeriodicAbstract(Result).EffectPeriod := EffectPeriod;
end;

procedure TEnchantmentPeriodicAbstract.Update(const AParentActor: TObject;
  const AParentItem: TObject; const SecondsPassed: Single);
begin
  inherited Update(AParentActor, AParentItem, SecondsPassed);
  LastSecondTime += SecondsPassed;
  if LastSecondTime > EffectPeriod then
  begin
    LastSecondTime -= EffectPeriod;
    if Rnd.Random < Strength then
      Perform(AParentActor, AParentItem);
  end;
end;

procedure RegisterRandomEnchantment(const AClass: TEnchantmentClass;
  const AFrequency: Single; const AKind: TEnchantmentKind);
var
  R: TRandomEnchantmentMetadata;
begin
  // Note: we Validate the data here, though it's hardcoded
  if RandomEnchantmentsList = nil then // NOTE: we can't guess in which order initialization will requrest RegisterRandomEnchantment, so the only reliable way is to create RandomEnchantmentsList at first request
    RandomEnchantmentsList := TRandomEnchantmentsList.Create;
  R.EnchantmentClass := AClass;
  R.Kind := AKind;
  R.Frequency := AFrequency;
  if AFrequency <= 0 then
    raise Exception.CreateFmt('Frequency <= 0 for %s', [AClass.ClassName]);
  RandomEnchantmentsList.Add(R);
end;

var
  FRegisteredEnchantments: TEnchantmentClassesList;

procedure RegisterEnchantment(const AClass: TEnchantmentClass);
begin
  if FRegisteredEnchantments = nil then
    FRegisteredEnchantments := TEnchantmentClassesList.Create;
  FRegisteredEnchantments.Add(AClass);
end;

procedure InitEnchantments;
var
  E: TEnchantmentClass;
begin
  EnchantmentImagesCache := TEnchantmentImagesCache.Create([]);
  for E in FRegisteredEnchantments do
  begin
    if UriFileExists('castle-data:/core/enchantments_icons/' + E.ClassName + '.png') then
    begin
      EnchantmentImagesCache.Add(E.ClassName + '_positive', LoadDrawable('castle-data:/core/enchantments_icons/' + E.ClassName + '.png'));
      EnchantmentImagesCache.Add(E.ClassName + '_negative', LoadDrawable('castle-data:/core/enchantments_icons/' + E.ClassName + '.png'));
    end else
    begin
      EnchantmentImagesCache.Add(E.ClassName + '_positive', LoadDrawable('castle-data:/core/enchantments_icons/' + E.ClassName + '_positive.png'));
      EnchantmentImagesCache.Add(E.ClassName + '_negative', LoadDrawable('castle-data:/core/enchantments_icons/' + E.ClassName + '_negative.png'));
    end;
  end;
  FreeAndNil(FRegisteredEnchantments);
end;

procedure FreeEnchantments;
begin
  FreeAndNil(RandomEnchantmentsList);
  FreeAndNil(EnchantmentImagesCache);
end;

end.


{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentCannotInteractWithType;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameEnchantmentAbstract;

type
  { Prevents character from interacting with an monster type
    note, we can easily extend this to any kind... but need to adjust Description then
    to support more target types. }
  TEnchantmentCannotInteractWithType = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    ActorType: String;
    function CanBeStatusEffect: Boolean; override;
    function IsPositive: Boolean; override;
    function Description: String; override;
    procedure Save(const Element: TDOMElement); override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableData, GameMonstersDatabase;

procedure TEnchantmentCannotInteractWithType.Validate;
begin
  inherited Validate;
  if Strength <= 0 then
    raise EDataValidationError.CreateFmt('Strength <= 0 in %s', [Self.ClassName]);
  if ActorType = '' then
    raise EDataValidationError.CreateFmt('ActorType = empty in %s', [ClassName]);
end;

procedure TEnchantmentCannotInteractWithType.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  ActorType := Element.AttributeString('ActorType');
end;

function TEnchantmentCannotInteractWithType.CanBeStatusEffect: Boolean;
begin
  Exit(true);
end;

function TEnchantmentCannotInteractWithType.IsPositive: Boolean;
begin
  Exit(false);
end;

function TEnchantmentCannotInteractWithType.Description: String;
begin
  Result := Format('Prevents character from interacting with "%s" objects' + RequirementDescription, [MonstersDataDictionary[ActorType].DisplayName]);
end;

procedure TEnchantmentCannotInteractWithType.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('ActorType', ActorType);
end;

initialization
  RegisterSerializableData(TEnchantmentCannotInteractWithType);
  RegisterEnchantment(TEnchantmentCannotInteractWithType);

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentDamageBonus;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Additive bonus to damage
    applied after multiplier, and so unaffected by it
    warning: will also affect stealth damage (it's a bug, Todo) }
  TEnchantmentDamageBonus = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath;

procedure TEnchantmentDamageBonus.Validate;
begin
  inherited Validate;
  if Strength = 0 then
    raise EDataValidationError.CreateFmt('Strength = 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentDamageBonus.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  if Rnd.RandomBoolean or not CanBeNegative then
    Strength := Clamp(Sqr(Rnd.Random) * Quality + 0.2, 0.3, 100)
  else
    Strength := -Clamp(Sqr(Rnd.Random) * Quality + 0.2, 0.3, 4);
end;

function TEnchantmentDamageBonus.IsPositive: Boolean;
begin
  Exit(Strength > 0);
end;

function TEnchantmentDamageBonus.Description: String;
begin
  if Strength > 0 then
    Result := Format('Increases damage by %.1n' + RequirementDescription, [Strength])
  else
    Result := Format('Decreases damage by %.1n' + RequirementDescription, [-Strength]);
end;

initialization
  RegisterSerializableData(TEnchantmentDamageBonus);
  RegisterRandomEnchantment(TEnchantmentDamageBonus, 1.0, ekBoth);
  RegisterEnchantment(TEnchantmentDamageBonus);

end.


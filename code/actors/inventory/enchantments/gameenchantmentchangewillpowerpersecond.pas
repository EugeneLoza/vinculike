{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentChangeWillpowerPerSecond;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Heals/drains absolute quantity of willpower per second }
  TEnchantmentChangeWillpowerPerSecond = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject;const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
    procedure Update(const AParentActor: TObject; const AParentItem: TObject;
      const SecondsPassed: Single); override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath,
  GamePlayerCharacter;

procedure TEnchantmentChangeWillpowerPerSecond.Validate;
begin
  inherited Validate;
  if Strength = 0 then
    raise EDataValidationError.CreateFmt('Strength = 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentChangeWillpowerPerSecond.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  // random can be only negative, for now at least; positive will break balancing and used for "spells" instead
  Strength := -Clamp(Sqr(Rnd.Random) * Quality / 3 + 1, 1.1, 2);
end;

function TEnchantmentChangeWillpowerPerSecond.IsPositive: Boolean;
begin
  Exit(Strength > 0);
end;

function TEnchantmentChangeWillpowerPerSecond.Description: String;
begin
  if Strength > 0 then
    Result := Format('Heals %.1n willpower per second' + RequirementDescription, [Strength])
  else
    Result := Format('Drains %.1n willpower per second' + RequirementDescription, [-Strength])
end;

procedure TEnchantmentChangeWillpowerPerSecond.Update(const AParentActor: TObject;
  const AParentItem: TObject; const SecondsPassed: Single);
begin
  inherited Update(AParentActor, AParentItem, SecondsPassed);
  if Strength > 0 then
    (AParentActor as TPlayerCharacter).HealWill(Strength * SecondsPassed)
  else
    (AParentActor as TPlayerCharacter).DegradeWill(-Strength * SecondsPassed);
end;

initialization
  RegisterSerializableData(TEnchantmentChangeWillpowerPerSecond);
  RegisterRandomEnchantment(TEnchantmentChangeWillpowerPerSecond, 1.0, ekNegative); // note, random can be only negative
  RegisterEnchantment(TEnchantmentChangeWillpowerPerSecond);

end.


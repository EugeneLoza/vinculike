{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentChangeHealthPerSecond;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Heals/drains absolutely quantity of health per second }
  TEnchantmentChangeHealthPerSecond = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject;const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
    procedure Update(const AParentActor: TObject; const AParentItem: TObject;
      const SecondsPassed: Single); override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath,
  GamePlayerCharacter;

procedure TEnchantmentChangeHealthPerSecond.Validate;
begin
  inherited Validate;
  if Strength = 0 then
    raise EDataValidationError.CreateFmt('Strength = 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentChangeHealthPerSecond.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  // random can be only negative, for now at least; positive will break balancing and used for "spells" instead
  Strength := -Clamp(Sqr(Rnd.Random) * Quality / 3 + 1, 1.1, 2);
end;

function TEnchantmentChangeHealthPerSecond.IsPositive: Boolean;
begin
  Exit(Strength > 0);
end;

function TEnchantmentChangeHealthPerSecond.Description: String;
begin
  if Strength > 0 then
    Result := Format('Heals %.1n health per second' + RequirementDescription, [Strength])
  else
    Result := Format('Drains %.1n health per second' + RequirementDescription, [-Strength]);
end;

procedure TEnchantmentChangeHealthPerSecond.Update(const AParentActor: TObject;
  const AParentItem: TObject; const SecondsPassed: Single);
begin
  inherited Update(AParentActor, AParentItem, SecondsPassed);
  if Strength > 0 then
    (AParentActor as TPlayerCharacter).HealHealth(Strength * SecondsPassed)
  else
    (AParentActor as TPlayerCharacter).DegradeHealth(-Strength * SecondsPassed);
end;

initialization
  RegisterSerializableData(TEnchantmentChangeHealthPerSecond);
  RegisterRandomEnchantment(TEnchantmentChangeHealthPerSecond, 1.0, ekNegative); // note, random only negative
  RegisterEnchantment(TEnchantmentChangeHealthPerSecond);

end.


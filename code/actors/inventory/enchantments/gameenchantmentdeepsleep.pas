{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentDeepSleep;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { makes Character unable to wake up through player input until stamina is at certain threshold
    WARNING: we treat this effect as additive. If value > 1.0 Player character will never ever wake up. it's a bug, todo  }
  TEnchantmentDeepSleep = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean); override;
    function IdentificationComplexity(const AQuality: Single): Single; override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath;

procedure TEnchantmentDeepSleep.Validate;
begin
  inherited Validate;
  if Strength <= 0 then
    raise EDataValidationError.CreateFmt('Strength <= 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentDeepSleep.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  Strength := Clamp(Sqr(Rnd.Random) * Quality / 6, 0.05, 0.99);
end;

function TEnchantmentDeepSleep.IdentificationComplexity(const AQuality: Single): Single;
begin
  Exit(3.0 * Rnd.Random * AQuality);
end;

function TEnchantmentDeepSleep.IsPositive: Boolean;
begin
  Exit(false);
end;

function TEnchantmentDeepSleep.Description: String;
begin
  if Strength <= 1 then
    Result := Format('Can''t wake up until stamina is at %d%%%%' + RequirementDescription, [Round(100 * Strength)])
  else
    Result := Format('Prevents from waking up' + RequirementDescription, [])
end;

initialization
  RegisterSerializableData(TEnchantmentDeepSleep);
  RegisterRandomEnchantment(TEnchantmentDeepSleep, 1.0, ekNegative);
  RegisterEnchantment(TEnchantmentDeepSleep);

end.


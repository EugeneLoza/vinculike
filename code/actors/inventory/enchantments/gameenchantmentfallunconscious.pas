{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentFallUnconscious;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  DOM,
  GameEnchantmentAbstract;

type
  { Periodically teleports target for short distances }
  TEnchantmentFallUnconscious = class(TEnchantmentPeriodicAbstract)
  protected
    StaminaTreshold: Single;
    procedure Perform(const AParentActor: TObject; const AParentItem: TObject); override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
    function CloneInternal: TEnchantmentAbstract; override;
    procedure RandomInternal(const AParent: TObject;const Quality: Single; const CanBeNegative: Boolean); override;
    function IdentificationComplexity(const AQuality: Single): Single; override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
    procedure Save(const Element: TDOMElement); override;
  end;

implementation
uses
  CastleXmlUtils, CastleStringUtils,
  GameSerializableData, GameMath,
  GamePlayerCharacter, GameActionPlayerUnconscious,
  GameRandom, GameLog, GameColors;

procedure TEnchantmentFallUnconscious.Validate;
begin
  inherited Validate;
  if StaminaTreshold > 1 then
    raise EDataValidationError.CreateFmt('StaminaTreshold > 1 in %s', [Self.ClassName]);
  if Strength <= 0 then
    raise EDataValidationError.CreateFmt('Strength <= 0 in %s', [Self.ClassName]);
  if Strength > 1 then
    raise EDataValidationError.CreateFmt('Strength > 1 in %s', [Self.ClassName]);
end;

procedure TEnchantmentFallUnconscious.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  StaminaTreshold := Element.AttributeSingle('StaminaTreshold');
end;

procedure TEnchantmentFallUnconscious.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('StaminaTreshold', StaminaTreshold);
end;

function TEnchantmentFallUnconscious.CloneInternal: TEnchantmentAbstract;
begin
  Result := inherited CloneInternal;
  TEnchantmentFallUnconscious(Result).StaminaTreshold := StaminaTreshold;
end;

procedure TEnchantmentFallUnconscious.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  Strength := 0.1 + Rnd.Random * 0.9;
  if Rnd.RandomBoolean then
    StaminaTreshold := Clamp(-1 + Sqr(Rnd.Random) * Quality / 5, -1, 0.5)
  else
    StaminaTreshold := -1 + -2 * Rnd.Random;
  EffectPeriod := 0.5 + Rnd.Random * 60;
end;

function TEnchantmentFallUnconscious.IdentificationComplexity(const AQuality: Single): Single;
begin
  Exit(3.0 * Rnd.Random * AQuality);
end;

function TEnchantmentFallUnconscious.IsPositive: Boolean;
begin
  Exit(false);
end;

function TEnchantmentFallUnconscious.Description: String;
begin
  Result := Format('Can drop unconscious if stamina is below %d%%%%' + RequirementDescription, [Round(100 * StaminaTreshold)]);
end;

procedure TEnchantmentFallUnconscious.Perform(const AParentActor: TObject;
  const AParentItem: TObject);
begin
  {$DEFINE ParentPlayer:=(AParentActor as TPlayerCharacter)}
  if not ParentPlayer.Unsuspecting then
  begin
    ParentPlayer.CurrentAction := TActionPlayerUnconscious.NewAction(AParentActor);
    ParentPlayer.CurrentAction.Start;
    ShowLog('Out of the blue %s''s feels so tired that can''t keep her eyes open', [ParentPlayer.Data.DisplayName], ColorLogNotEnoughStamina);
  end;
end;

initialization
  RegisterSerializableData(TEnchantmentFallUnconscious);
  RegisterRandomEnchantment(TEnchantmentFallUnconscious, 0.5, ekNegative);
  RegisterEnchantment(TEnchantmentFallUnconscious);

end.


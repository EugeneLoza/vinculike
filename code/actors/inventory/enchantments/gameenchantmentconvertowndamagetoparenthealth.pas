{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentConvertOwnDamageToParentHealth;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameEnchantmentAbstract;

type
  { Converts a fraction of damage this item receives into health change for the wearer:
    Strength > 0 = healing
    Strength < 0 = hurting }
  TEnchantmentConvertOwnDamageToParentHealth = class(TEnchantmentAbstract)
  strict private
    LastDurability: Single;
  protected
    procedure RandomInternal(const AParent: TObject;const Quality: Single; const CanBeNegative: Boolean); override;
    function CloneInternal: TEnchantmentAbstract; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    function CanBeStatusEffect: Boolean; override;
    class function MakesSense(const AItem: TObject): Boolean; override;
    function IsPositive: Boolean; override;
    function Description: String; override;
    procedure Save(const Element: TDOMElement); override;
    procedure Update(const AParentActor: TObject; const AParentItem: TObject;
      const SecondsPassed: Single); override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableData, GameRandom, GameMath,
  GamePlayerCharacter, GameInventoryItem, GameLog, GameColors;

procedure TEnchantmentConvertOwnDamageToParentHealth.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  if Rnd.RandomBoolean or not CanBeNegative then
    Strength := -Clamp(Rnd.Random * Quality / 6 + 0.2, 0.3, 5)
  else
    Strength := Clamp(Rnd.Random * Quality / 6 + 0.2, 0.3, 5);
end;

procedure TEnchantmentConvertOwnDamageToParentHealth.Validate;
begin
  inherited Validate;
  if Strength = 0 then
    raise EDataValidationError.CreateFmt('Strength = 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentConvertOwnDamageToParentHealth.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  LastDurability := Element.AttributeSingleDef('LastDurability', -1);
end;

function TEnchantmentConvertOwnDamageToParentHealth.CanBeStatusEffect: Boolean;
begin
  Exit(false);
end;

class function TEnchantmentConvertOwnDamageToParentHealth.MakesSense(
  const AItem: TObject): Boolean;
begin
  Exit(not (AItem as TInventoryItem).ItemData.Indestructible);
end;

function TEnchantmentConvertOwnDamageToParentHealth.IsPositive: Boolean;
begin
  Exit(Strength > 0);
end;

function TEnchantmentConvertOwnDamageToParentHealth.Description: String;
begin
  if Strength > 0 then
    Result := Format('Heals the wearer for %d%%%% damage recieved by this item' + RequirementDescription, [Round(100 * Strength)])
  else
    Result := Format('Hurts the wearer for %d%%%% damage recieved by this item' + RequirementDescription, [-Round(100 * Strength)])
end;

function TEnchantmentConvertOwnDamageToParentHealth.CloneInternal: TEnchantmentAbstract;
begin
  Result := inherited;
  (Result as TEnchantmentConvertOwnDamageToParentHealth).LastDurability := -1;
end;

procedure TEnchantmentConvertOwnDamageToParentHealth.Save(
  const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('LastDurability', LastDurability);
end;

procedure TEnchantmentConvertOwnDamageToParentHealth.Update(const AParentActor: TObject;
  const AParentItem: TObject; const SecondsPassed: Single);
var
  DurabilityDiff: Single;
  ChangeValue: Single;
begin
  inherited Update(AParentActor, AParentItem, SecondsPassed);
  if LastDurability < 0 then
    LastDurability := (AParentItem as TInventoryItem).Durability;

  DurabilityDiff := (LastDurability - (AParentItem as TInventoryItem).Durability);

  if DurabilityDiff > 0 then
  begin
    if Strength > 0 then
    begin
      ChangeValue := Strength * DurabilityDiff;
      if (ChangeValue > 1) and ((AParentActor as TPlayerCharacter).Health < (AParentActor as TPlayerCharacter).MaxHealth) then
        ShowLog('%s suddenly feels invigorated (%.1n healed)', [(AParentActor as TPlayerCharacter).Data.DisplayName, ChangeValue], ColorLogBondageItemSave);
      (AParentActor as TPlayerCharacter).HealHealth(ChangeValue);
    end else
    begin
      ChangeValue := -Strength * DurabilityDiff;
      if (ChangeValue > 1) then
        ShowLog('%s suddenly feels hurt (%.1n damage)', [(AParentActor as TPlayerCharacter).Data.DisplayName, ChangeValue], ColorLogUnabsorbedDamage);
      (AParentActor as TPlayerCharacter).DegradeHealth(ChangeValue);
    end;
  end;
  LastDurability := TInventoryItem(AParentItem).Durability;
end;

initialization
  RegisterSerializableData(TEnchantmentConvertOwnDamageToParentHealth);
  RegisterRandomEnchantment(TEnchantmentConvertOwnDamageToParentHealth, 1.0, ekNegative);
  RegisterEnchantment(TEnchantmentConvertOwnDamageToParentHealth);

end.


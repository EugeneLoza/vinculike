{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentNudeTouchWillpowerDamageMultiplier;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Multiplier to willpower damage received from damage in nude top/bottom
    can be >1 (increase damage) or <1 (reduce damage, 0 is full Negation) }
  TEnchantmentNudeTouchWillpowerDamageMultiplier = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean); override;
  public
    class function MakesSense(const AItem: TObject): Boolean; override;
    function IsPositive: Boolean; override;
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData,
  GameRandom, GameMath, GameInventoryItem;

procedure TEnchantmentNudeTouchWillpowerDamageMultiplier.Validate;
begin
  inherited Validate;
  if Strength < 0 then
    raise EDataValidationError.CreateFmt('Strength < 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentNudeTouchWillpowerDamageMultiplier.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  if CanBeNegative and Rnd.RandomBoolean then
    Strength := 1 + Clamp(Sqr(Rnd.Random) * Quality * 2, 0.5, 100)
  else
    Strength := 1 - Clamp(Sqr(Rnd.Random) * Quality / 3 + 0.2, 0.2, 1);
  // those don't make sense with this enchantment
  RequiresDressedBottom := false;
  RequiresDressedTop := false;
  // those are kinda redundant, while technically they _can_ make sense, better just not to
  RequiresNudeBottom := false;
  RequiresNudeTop := false;
end;

class function TEnchantmentNudeTouchWillpowerDamageMultiplier.MakesSense(
  const AItem: TObject): Boolean;
begin
  // it can be "or" but still makes more sense if it doesn't cover either
  Exit((not (AItem as TInventoryItem).ItemData.CoversTop) and (not (AItem as TInventoryItem).ItemData.CoversBottom));
end;

function TEnchantmentNudeTouchWillpowerDamageMultiplier.IsPositive: Boolean;
begin
  Exit(Strength < 1);
end;

function TEnchantmentNudeTouchWillpowerDamageMultiplier.Description: String;
begin
  if Strength > 1 then
    Result := Format('Increases damage from nude touch by %d%%%%' + RequirementDescription, [Round(100 * (Strength - 1))])
  else
    Result := Format('Decreases damage from nude touch by %d%%%%' + RequirementDescription, [Round(100 * (1 - Strength))]);
end;

initialization
  RegisterSerializableData(TEnchantmentNudeTouchWillpowerDamageMultiplier);
  RegisterRandomEnchantment(TEnchantmentNudeTouchWillpowerDamageMultiplier, 1.0, ekBoth);
  RegisterEnchantment(TEnchantmentNudeTouchWillpowerDamageMultiplier);

end.


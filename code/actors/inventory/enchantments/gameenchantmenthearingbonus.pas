{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentHearingBonus;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Additive bonus to hearing range }
  TEnchantmentHearingBonus = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData,
  GameRandom, GameMath;

procedure TEnchantmentHearingBonus.Validate;
begin
  inherited Validate;
  if Strength = 0 then
    raise EDataValidationError.CreateFmt('Strength = 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentHearingBonus.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  if Rnd.RandomBoolean and CanBeNegative then
    Strength := -Clamp(Sqr(Rnd.Random) * Quality * 4, 3, 100)
  else
    Strength := Clamp(Sqr(Rnd.Random) * Quality * 4, 3, 100);
end;

function TEnchantmentHearingBonus.IsPositive: Boolean;
begin
  Exit(Strength > 0);
end;

function TEnchantmentHearingBonus.Description: String;
begin
  if Strength > 0 then
    Result := Format('Increases hearing range by %.0n' + RequirementDescription, [Strength])
  else
    Result := Format('Decreases hearing range by %.0n' + RequirementDescription, [-Strength]);
end;

initialization
  RegisterSerializableData(TEnchantmentHearingBonus);
  RegisterRandomEnchantment(TEnchantmentHearingBonus, 1.0, ekBoth);
  RegisterEnchantment(TEnchantmentHearingBonus);

end.


{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentIncomingWillDamageMultiplier;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Changes the incoming willpower damage by a fraction }
  TEnchantmentIncomingWillDamageMultiplier = class(TEnchantmentAbstract)
  protected
    procedure RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath;

procedure TEnchantmentIncomingWillDamageMultiplier.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  if Rnd.RandomBoolean or CanBeNegative then
    Strength := 1 + Clamp(Sqr(Rnd.Random) * Quality / 60, 0.01, 0.25)
  else
    Strength := 1 - Clamp(Sqr(Rnd.Random) * Quality / 60, 0.03, 0.25);
end;

function TEnchantmentIncomingWillDamageMultiplier.IsPositive: Boolean;
begin
  Exit(Strength < 1);
end;

function TEnchantmentIncomingWillDamageMultiplier.Description: String;
begin
  if Strength > 1 then
    Result := Format('Increases incoming willpower damage by %d%%%%' + RequirementDescription, [Round(100 * (Strength - 1))]) // We must call stupid "%%%%" here because when showing log we again call Format whic will "eat" %%->% symbol, we need two
  else
    Result := Format('Negates %d%%%% of incoming willpower damage' + RequirementDescription, [Round(100 * (1 - Strength))]);
end;

initialization
  RegisterSerializableData(TEnchantmentIncomingWillDamageMultiplier);
  RegisterRandomEnchantment(TEnchantmentIncomingWillDamageMultiplier, 1.0, ekBoth);
  RegisterEnchantment(TEnchantmentIncomingWillDamageMultiplier);

end.


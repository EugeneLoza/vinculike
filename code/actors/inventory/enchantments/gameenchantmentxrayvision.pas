{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentXRayVision;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Enables recognizing monsters behind walls of they are hearable }
  TEnchantmentXRayVision = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData;

procedure TEnchantmentXRayVision.Validate;
begin
  inherited Validate;
  if Strength <= 0 then
    raise EDataValidationError.CreateFmt('Strength <= 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentXRayVision.RandomInternal(const AParent: TObject;
  const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited RandomInternal(AParent, Quality, CanBeNegative);
  Strength := 1;
end;

function TEnchantmentXRayVision.IsPositive: Boolean;
begin
  Exit(true);
end;

function TEnchantmentXRayVision.Description: String;
begin
  Result := Format('Reveals monsters behind walls if hearable' + RequirementDescription, []);
end;

initialization
  RegisterSerializableData(TEnchantmentXRayVision);
  RegisterRandomEnchantment(TEnchantmentXRayVision, 1.0, ekPositive);
  RegisterEnchantment(TEnchantmentXRayVision);

end.


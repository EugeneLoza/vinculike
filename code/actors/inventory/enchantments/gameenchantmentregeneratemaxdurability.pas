{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentRegenerateMaxDurability;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Regenerates max value of parent item durability per second
    doesn't affect current value of durability }
  TEnchantmentRegenerateMaxDurability = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject;const Quality: Single; const CanBeNegative: Boolean); override;
  public
    class function MakesSense(const AItem: TObject): Boolean; override;
    function IsPositive: Boolean; override;
    function Description: String; override;
    procedure Update(const AParentActor: TObject; const AParentItem: TObject;
      const SecondsPassed: Single); override;
    function CanBeStatusEffect: Boolean; override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath,
  GameInventoryItem, GamePlayerCharacter;

procedure TEnchantmentRegenerateMaxDurability.Validate;
begin
  inherited Validate;
  if Strength = 0 then
    raise EDataValidationError.CreateFmt('Strength = 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentRegenerateMaxDurability.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  if Rnd.RandomBoolean or not CanBeNegative then
    Strength := Clamp(Sqr(Rnd.Random) * Quality / 66, 0.006, 0.2)
  else
    Strength := -Clamp(Sqr(Rnd.Random) * Quality / 66, 0.006, 0.3);
end;

class function TEnchantmentRegenerateMaxDurability.MakesSense(
  const AItem: TObject): Boolean;
begin
  Exit(not (AItem as TInventoryItem).ItemData.Indestructible and (AItem as TInventoryItem).ItemData.CanBeRepaired);
end;

function TEnchantmentRegenerateMaxDurability.IsPositive: Boolean;
begin
  Exit(Strength > 0);
end;

function TEnchantmentRegenerateMaxDurability.Description: String;
begin
  if Strength > 0 then
    Result := Format('Regenerates %.2n max durability per second' + RequirementDescription, [Strength])
  else
    Result := Format('Degrades %.2n max durability per second' + RequirementDescription, [-Strength])
end;

procedure TEnchantmentRegenerateMaxDurability.Update(const AParentActor: TObject;
  const AParentItem: TObject; const SecondsPassed: Single);
var
  AItem: TInventoryItem;
begin
  inherited Update(AParentActor, AParentItem, SecondsPassed);
  AItem := AParentItem as TInventoryItem;
  AItem.MaxDurability += SecondsPassed * Strength;
  if AItem.MaxDurability > AItem.ItemData.Durability then
    AItem.MaxDurability := AItem.ItemData.Durability;
  if AItem.Durability > AItem.MaxDurability then
    AItem.Durability := AItem.MaxDurability;
  if AItem.MaxDurability < 0 then
    (AParentActor as TPlayerCharacter).Inventory.DamageItem(AItem, 1.0);
end;

function TEnchantmentRegenerateMaxDurability.CanBeStatusEffect: Boolean;
begin
  Exit(false);
end;

initialization
  RegisterSerializableData(TEnchantmentRegenerateMaxDurability);
  RegisterRandomEnchantment(TEnchantmentRegenerateMaxDurability, 1.0, ekPositive); // note: it's *usually* positive
  RegisterEnchantment(TEnchantmentRegenerateMaxDurability);

end.


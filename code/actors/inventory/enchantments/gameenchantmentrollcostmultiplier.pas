{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentRollCostMultiplier;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Multiplier to stamina cost for dodge roll action }
  TEnchantmentRollCostMultiplier = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath;

procedure TEnchantmentRollCostMultiplier.Validate;
begin
  inherited Validate;
  if Strength < 0 then
    raise EDataValidationError.CreateFmt('Strength < 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentRollCostMultiplier.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  if CanBeNegative and Rnd.RandomBoolean then
    Strength := 1 + Clamp(Sqr(Rnd.Random) * Quality / 12, 0.03, 1)
  else
    Strength := 1 - Clamp(Sqr(Rnd.Random) * Quality / 12, 0.03, 0.75);
end;

function TEnchantmentRollCostMultiplier.IsPositive: Boolean;
begin
  Exit(Strength < 1);
end;

function TEnchantmentRollCostMultiplier.Description: String;
begin
  if Strength > 1 then
    Result := Format('Increases roll cost by %d%%%%' + RequirementDescription, [Round(100 * (Strength - 1))]) // We must call stupid "%%%%" here because when showing log we again call Format whic will "eat" %%->% symbol, we need two
  else
    Result := Format('Reduces roll cost by %d%%%%' + RequirementDescription, [Round(100 * (1 - Strength))]);
end;

initialization
  RegisterSerializableData(TEnchantmentRollCostMultiplier);
  RegisterRandomEnchantment(TEnchantmentRollCostMultiplier, 1.0, ekBoth);
  RegisterEnchantment(TEnchantmentRollCostMultiplier);

end.


{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentUnequipInventory;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  DOM,
  GameEnchantmentAbstract, GameApparelSlots;

type
  { Periodically unequips equipped items from a set of slots }
  TEnchantmentUnequipInventory = class(TEnchantmentPeriodicAbstract)
  protected
    Slots: TApparelSlotsList;
    UnequipBondage: Boolean;
    procedure Perform(const AParentActor: TObject; const AParentItem: TObject); override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
    function CloneInternal: TEnchantmentAbstract; override;
    procedure RandomInternal(const AParent: TObject;const Quality: Single; const CanBeNegative: Boolean); override;
    function IdentificationComplexity(const AQuality: Single): Single; override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
    procedure Save(const Element: TDOMElement); override;
  public
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils, CastleStringUtils,
  GameSerializableData, GamePlayerCharacter, GameInventoryItem, GameEnumUtils,
  GameRandom, GameLog, GameColors, GameSounds;

procedure TEnchantmentUnequipInventory.Validate;
begin
  inherited Validate;
  if Slots.Count = 0 then
    raise EDataValidationError.CreateFmt('Slots.Count = 0 in %s', [Self.ClassName]);
  if Strength <= 0 then
    raise EDataValidationError.CreateFmt('Strength <= 0 in %s', [Self.ClassName]);
  if Strength > 1 then
    raise EDataValidationError.CreateFmt('Strength > 1 in %s', [Self.ClassName]);
end;

procedure TEnchantmentUnequipInventory.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  Slots := specialize StrtoEnumsList<TApparelSlot>(Element.AttributeString('Slots'));
  UnequipBondage := Element.AttributeBoolean('UnequipBondage');
end;

procedure TEnchantmentUnequipInventory.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Slots', specialize ListToEnumsStr<TApparelSlot>(Slots));
  Element.AttributeSet('UnequipBondage', UnequipBondage);
end;

function TEnchantmentUnequipInventory.CloneInternal: TEnchantmentAbstract;
begin
  Result := inherited CloneInternal;
  TEnchantmentUnequipInventory(Result).Slots := Slots.Clone;
  TEnchantmentUnequipInventory(Result).UnequipBondage := UnequipBondage;
end;

procedure TEnchantmentUnequipInventory.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
var
  I, J: Integer;
  E: TApparelSlot;
  AllSlots: TApparelSlotsList;
begin
  inherited;
  AllSlots := TApparelSlotsList.Create;
  AllSlots.Add(esGlasses);
  AllSlots.Add(esAmulet);
  AllSlots.Add(esWeapon);
  AllSlots.Add(esTopOver);
  AllSlots.Add(esTopUnder);
  AllSlots.Add(esBottomOver);
  AllSlots.Add(esBottomUnder);
  AllSlots.Add(esFeet);

  for E in (AParent as TInventoryItem).Data.EquipSlots do
    AllSlots.Remove(E); // if not in the list, will silently not do anything
  if (AllSlots.Count = 0) or (Rnd.Random < 0.1) then
    AllSlots.Add((AParent as TInventoryItem).Data.MainSlot); // has a chance to unequip self too

  Slots := TApparelSlotsList.Create;
  I := Rnd.Random(AllSlots.Count) + 1;
  while I > 0 do
  begin
    J := Rnd.Random(AllSlots.Count);
    Slots.Add(AllSlots[J]);
    AllSlots.Delete(J);
    Dec(I);
    if AllSlots.Count = 0 then
      break;
  end;
  AllSlots.Free;

  Strength := 0.1 + Rnd.Random * 0.9;
  EffectPeriod := 1.0 + Rnd.Random * 60;
  UnequipBondage := Rnd.RandomBoolean;
end;

function TEnchantmentUnequipInventory.IdentificationComplexity(
  const AQuality: Single): Single;
begin
  Exit(3.0 * Rnd.Random * AQuality);
end;

function TEnchantmentUnequipInventory.IsPositive: Boolean;
begin
  Exit(false);
end;

function TEnchantmentUnequipInventory.Description: String;
var
  I: Integer;
  SlotsString: String;
begin
  SlotsString := '';
  for I := 0 to Pred(Slots.Count) do
  begin
    SlotsString += EquipmentSlotToHumanReadableString(Slots[I]);
    if I < Pred(Slots.Count) then
      SlotsString += ', ';
  end;

  Result := Format('Randomly unequips items (%s)' + RequirementDescription, [SlotsString]);
end;

procedure TEnchantmentUnequipInventory.Perform(const AParentActor: TObject;
  const AParentItem: TObject);
var
  E: TApparelSlot;
begin
  E := Slots[Rnd.Random(Slots.Count)];
  if ((AParentActor as TPlayerCharacter).Inventory.Apparel[E] <> nil) and (UnequipBondage or not (AParentActor as TPlayerCharacter).Inventory.Bondage[E]) then
  begin
    ShowLog('Without any warning %s slips off', [(AParentActor as TPlayerCharacter).Inventory.Apparel[E].Data.DisplayName], ColorLogItemBreak);
    Sound((AParentActor as TPlayerCharacter).Inventory.Equipped[E].ItemData.SoundUnequip);
    (AParentActor as TPlayerCharacter).Inventory.UnequipAndDrop(E, not (AParentActor as TPlayerCharacter).Unsuspecting);
  end;
end;

destructor TEnchantmentUnequipInventory.Destroy;
begin
  FreeAndNil(Slots);
  inherited Destroy;
end;

initialization
  RegisterSerializableData(TEnchantmentUnequipInventory);
  RegisterRandomEnchantment(TEnchantmentUnequipInventory, 1.0, ekNegative);
  RegisterEnchantment(TEnchantmentUnequipInventory);

end.


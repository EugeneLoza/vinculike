{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentBondage;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Makes this item unremovable }
  TEnchantmentBondage = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean); override;
    function IdentificationComplexity(const AQuality: Single): Single; override;
  public
    function CanBeStatusEffect: Boolean; override;
    function IsPositive: Boolean; override;
    function Description: String; override;
    function EquipIdentifies: Boolean; override;
    class function MakesSense(const AItem: TObject): Boolean; override;
  end;

implementation
uses
  GameSerializableData,
  GameInventoryItem, GameRandom;

procedure TEnchantmentBondage.Validate;
begin
  inherited Validate;
  if Strength <= 0 then
    raise EDataValidationError.CreateFmt('Strength <= 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentBondage.RandomInternal(const AParent: TObject;
  const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited RandomInternal(AParent, Quality, CanBeNegative);
  Strength := 1;
end;

function TEnchantmentBondage.IdentificationComplexity(const AQuality: Single): Single;
begin
  Exit(4.0 * Rnd.Random * AQuality);
end;

function TEnchantmentBondage.CanBeStatusEffect: Boolean;
begin
  Exit(false);
end;

function TEnchantmentBondage.IsPositive: Boolean;
begin
  Exit(false);
end;

function TEnchantmentBondage.Description: String;
begin
  Result := Format('Cannot be removed easily' + RequirementDescription, []);
end;

function TEnchantmentBondage.EquipIdentifies: Boolean;
begin
  Exit(true);
end;

class function TEnchantmentBondage.MakesSense(const AItem: TObject): Boolean;
begin
  Exit(not (AItem as TInventoryItem).ItemData.Bondage);
end;

initialization
  RegisterSerializableData(TEnchantmentBondage);
  RegisterRandomEnchantment(TEnchantmentBondage, 1.0, ekNegative);
  RegisterEnchantment(TEnchantmentBondage);

end.


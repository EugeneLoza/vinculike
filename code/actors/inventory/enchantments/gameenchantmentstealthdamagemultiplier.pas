{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentStealthDamageMultiplier;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Multiplier to stealth damage }
  TEnchantmentStealthDamageMultiplier = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath;

procedure TEnchantmentStealthDamageMultiplier.Validate;
begin
  inherited Validate;
  if Strength <= 0 then
    raise EDataValidationError.CreateFmt('Strength <= 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentStealthDamageMultiplier.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  if Rnd.RandomBoolean or not CanBeNegative then
    Strength := 1 + Clamp(Sqr(Rnd.Random) * Quality / 20, 0.05, 1)
  else
    Strength := 1 - Clamp(Sqr(Rnd.Random) * Quality / 20, 0.05, 0.9);
end;

function TEnchantmentStealthDamageMultiplier.IsPositive: Boolean;
begin
  Exit(Strength > 1);
end;

function TEnchantmentStealthDamageMultiplier.Description: String;
begin
  if Strength > 1 then
    Result := Format('Increases stealth damage by %d%%%%' + RequirementDescription, [Round(100 * (Strength - 1))]) // We must call stupid "%%%%" here because when showing log we again call Format whic will "eat" %%->% symbol, we need two
  else
    Result := Format('Reduces stealth damage by %d%%%%' + RequirementDescription, [Round(100 * (1 - Strength))]);
end;

initialization
  RegisterSerializableData(TEnchantmentStealthDamageMultiplier);
  RegisterRandomEnchantment(TEnchantmentStealthDamageMultiplier, 1.0, ekBoth);
  RegisterEnchantment(TEnchantmentStealthDamageMultiplier);

end.


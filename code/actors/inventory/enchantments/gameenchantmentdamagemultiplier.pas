{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentDamageMultiplier;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Apply a multiplier to regular
    warning: also affects stealth damage, it's a bug, Todo }
  TEnchantmentDamageMultiplier = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath;

procedure TEnchantmentDamageMultiplier.Validate;
begin
  inherited Validate;
  if Strength = 0 then
    raise EDataValidationError.CreateFmt('Strength = 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentDamageMultiplier.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  if Rnd.RandomBoolean or not CanBeNegative then
    Strength := 1 + Clamp(Sqr(Rnd.Random) * Quality / 10 / 2, 0.03, 1)
  else
    Strength := 1 - Clamp(Sqr(Rnd.Random) * Quality / 10 / 2, 0.03, 0.8);
end;

function TEnchantmentDamageMultiplier.IsPositive: Boolean;
begin
  Exit(Strength > 1);
end;

function TEnchantmentDamageMultiplier.Description: String;
begin
  if Strength > 1 then
    Result := Format('Increases damage by %d%%%%' + RequirementDescription, [Round(100 * (Strength - 1))]) // We must call stupid "%%%%" here because when showing log we again call Format whic will "eat" %%->% symbol, we need two
  else
    Result := Format('Reduces damage by %d%%%%' + RequirementDescription, [Round(100 * (1 - Strength))]);
end;

initialization
  RegisterSerializableData(TEnchantmentDamageMultiplier);
  RegisterRandomEnchantment(TEnchantmentDamageMultiplier, 1.0, ekBoth);
  RegisterEnchantment(TEnchantmentDamageMultiplier);

end.


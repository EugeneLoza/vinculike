{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentRegenerateDurability;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Regenerate durability of the parent item per
    cannot go above max durability }
  TEnchantmentRegenerateDurability = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject;const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
    class function MakesSense(const AItem: TObject): Boolean; override;
    procedure Update(const AParentActor: TObject; const AParentItem: TObject;
      const SecondsPassed: Single); override;
    function CanBeStatusEffect: Boolean; override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath,
  GameInventoryItem, GamePlayerCharacter;

procedure TEnchantmentRegenerateDurability.Validate;
begin
  inherited Validate;
  if Strength = 0 then
    raise EDataValidationError.CreateFmt('Strength = 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentRegenerateDurability.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  if Rnd.RandomBoolean or not CanBeNegative then
    Strength := Clamp(Sqr(Rnd.Random) * Quality / 33, 0.01, 0.2)
  else
    Strength := -Clamp(Sqr(Rnd.Random) * Quality / 33, 0.01, 0.3);
end;

function TEnchantmentRegenerateDurability.IsPositive: Boolean;
begin
  Exit(Strength > 0);
end;

function TEnchantmentRegenerateDurability.Description: String;
begin
  if Strength > 0 then
    Result := Format('Regenerates %.2n durability per second' + RequirementDescription, [Strength])
  else
    Result := Format('Degrades %.2n durability per second' + RequirementDescription, [-Strength])
end;

class function TEnchantmentRegenerateDurability.MakesSense(const AItem: TObject): Boolean;
begin
  Exit(not (AItem as TInventoryItem).ItemData.Indestructible);
end;

procedure TEnchantmentRegenerateDurability.Update(const AParentActor: TObject;
  const AParentItem: TObject; const SecondsPassed: Single);
var
  AItem: TInventoryItem;
begin
  inherited Update(AParentActor, AParentItem, SecondsPassed);
  AItem := AParentItem as TInventoryItem;
  AItem.Durability += SecondsPassed * Strength;
  if AItem.Durability > AItem.MaxDurability then
    AItem.Durability := AItem.MaxDurability;
  if AItem.Durability < 0 then
    (AParentActor as TPlayerCharacter).Inventory.DamageItem(AItem, 1.0);
end;

function TEnchantmentRegenerateDurability.CanBeStatusEffect: Boolean;
begin
  Exit(false);
end;

initialization
  RegisterSerializableData(TEnchantmentRegenerateDurability);
  RegisterRandomEnchantment(TEnchantmentRegenerateDurability, 1.0, ekPositive); // note: it's *usually* positive
  RegisterEnchantment(TEnchantmentRegenerateDurability);

end.


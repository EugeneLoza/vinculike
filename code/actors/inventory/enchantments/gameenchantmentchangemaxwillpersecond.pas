{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentChangeMaxWillPerSecond;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Heals/drains absolute value of max willpower per second }
  TEnchantmentChangeMaxWillPerSecond = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject;const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
    procedure Update(const AParentActor: TObject; const AParentItem: TObject;
      const SecondsPassed: Single); override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath,
  GamePlayerCharacter;

procedure TEnchantmentChangeMaxWillPerSecond.Validate;
begin
  inherited Validate;
  if Strength = 0 then
    raise EDataValidationError.CreateFmt('Strength = 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentChangeMaxWillPerSecond.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  if Rnd.RandomBoolean or not CanBeNegative then
    Strength := Clamp(Sqr(Sqr(Rnd.Random)) * Quality / 300, 0.005, 0.03)
  else
    Strength := -Clamp(Sqr(Sqr(Rnd.Random)) * Quality / 300, 0.005, 0.03);
end;

function TEnchantmentChangeMaxWillPerSecond.IsPositive: Boolean;
begin
  Exit(Strength > 0);
end;

function TEnchantmentChangeMaxWillPerSecond.Description: String;
begin
  if Strength > 0 then
    Result := Format('Recovers %.3n of max will per second' + RequirementDescription, [Strength])
  else
    Result := Format('Drains %.3n of max will per second' + RequirementDescription, [-Strength]);
end;

procedure TEnchantmentChangeMaxWillPerSecond.Update(const AParentActor: TObject;
  const AParentItem: TObject; const SecondsPassed: Single);
begin
  inherited Update(AParentActor, AParentItem, SecondsPassed);
  if Strength > 0 then
    (AParentActor as TPlayerCharacter).HealMaxWill(Strength * SecondsPassed)
  else
  begin
    (AParentActor as TPlayerCharacter).MaxWill += Strength * SecondsPassed; // Strength is negative
    if (AParentActor as TPlayerCharacter).MaxWill < 1 then
      (AParentActor as TPlayerCharacter).MaxWill := 1;
    if (AParentActor as TPlayerCharacter).Will > (AParentActor as TPlayerCharacter).MaxWill then
      (AParentActor as TPlayerCharacter).Will := (AParentActor as TPlayerCharacter).MaxWill;
  end;
end;

initialization
  RegisterSerializableData(TEnchantmentChangeMaxWillPerSecond);
  RegisterRandomEnchantment(TEnchantmentChangeMaxWillPerSecond, 1.0, ekBoth);
  RegisterEnchantment(TEnchantmentChangeMaxWillPerSecond);

end.


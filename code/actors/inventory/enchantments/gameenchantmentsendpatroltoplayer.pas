{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentSendPatrolToPlayer;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  DOM,
  GameEnchantmentAbstract, GameApparelSlots, GameMonsterData;

type
  { Periodically spawns and sends a few monsters to player's current location }
  TEnchantmentSendPatrolToPlayer = class(TEnchantmentPeriodicAbstract)
  protected
    MonsterId: String; // TODO: cache TMonsterData
    MaxTotalDanger: Single;
    procedure Perform(const AParentActor: TObject; const AParentItem: TObject); override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
    function CloneInternal: TEnchantmentAbstract; override;
    procedure RandomInternal(const AParent: TObject;const Quality: Single; const CanBeNegative: Boolean); override;
    function IdentificationComplexity(const AQuality: Single): Single; override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
    procedure Save(const Element: TDOMElement); override;
  end;

implementation
uses
  CastleXmlUtils, CastleStringUtils,
  GameSerializableData,
  GameRandom, GameActionMoveAbstract,
  GameMonstersDatabase, GameMonster, GamePositionedObject, GameMap;

procedure TEnchantmentSendPatrolToPlayer.Validate;
begin
  inherited Validate;
  if Strength <= 0 then
    raise EDataValidationError.CreateFmt('Strength <= 0 in %s', [Self.ClassName]);
  if Strength > 1 then
    raise EDataValidationError.CreateFmt('Strength > 1 in %s', [Self.ClassName]);
  if MaxTotalDanger <= 0 then
    raise EDataValidationError.CreateFmt('MaxTotalDanger <= 0 in %s', [Self.ClassName]);
  if not MonstersDataDictionary.ContainsKey(MonsterId) then
    raise EDataValidationError.CreateFmt('MonsterId = "%s" not found in %s', [MonsterId, Self.ClassName]);
end;

procedure TEnchantmentSendPatrolToPlayer.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  MonsterId := Element.AttributeString('MonsterId');
  MaxTotalDanger := Element.AttributeSingle('MaxTotalDanger');
end;

procedure TEnchantmentSendPatrolToPlayer.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('MonsterId', MonsterId);
  Element.AttributeSet('MaxTotalDanger', MaxTotalDanger);
end;

function TEnchantmentSendPatrolToPlayer.CloneInternal: TEnchantmentAbstract;
begin
  Result := inherited CloneInternal;
  (Result as TEnchantmentSendPatrolToPlayer).MonsterId := MonsterId;
  (Result as TEnchantmentSendPatrolToPlayer).MaxTotalDanger := MaxTotalDanger;
end;

procedure TEnchantmentSendPatrolToPlayer.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
var
  MD: TMonsterData;
begin
  inherited;
  Strength := 0.1 + Rnd.Random * 0.9;
  EffectPeriod := 40.0 + Rnd.Random * 60;
  //MD := Map.PatrolList[Rnd.Random(Map.PatrolList.Count)];
  repeat
    MD := MonstersData[Rnd.Random(MonstersData.Count)];
  until MD.StartPatrolAtDepth <= Map.CurrentDepth;
  MonsterId := MD.Id;
  MaxTotalDanger := 3.0 + 2.0 * Rnd.Random * Quality; // 15 max at lvl.36
end;

function TEnchantmentSendPatrolToPlayer.IdentificationComplexity(
  const AQuality: Single): Single;
begin
  Exit(4.0 * Rnd.Random * AQuality);
end;

function TEnchantmentSendPatrolToPlayer.IsPositive: Boolean;
begin
  Exit(false);
end;

function TEnchantmentSendPatrolToPlayer.Description: String;
begin
  Result := Format('Occasionally calls a few monsters (%s) to character''s location' + RequirementDescription, [MonstersDataDictionary[MonsterId].DisplayName]);
end;

procedure TEnchantmentSendPatrolToPlayer.Perform(const AParentActor: TObject;
  const AParentItem: TObject);
var
  I: Integer;
  SX, SY: Int16;
  GoToX, GoToY: Int16;
  MonsterData: TMonsterData;
  AMonster: TMonster;
begin
  {$DEFINE ParentActor:=(AParentActor as TPositionedObject)}
  MonsterData := MonstersDataDictionary[MonsterId] as TMonsterData;
  for I := 0 to Round(Rnd.Random * MaxTotalDanger / MonsterData.Danger) do
  begin
    AMonster := TMonster.Create;
    AMonster.Data := MonsterData;
    AMonster.Reset;
    repeat
      SX := Map.ExitX - AMonster.PredSize + Rnd.Random(3 + AMonster.PredSize * 2);
      SY := Map.ExitY - AMonster.PredSize + Rnd.Random(3 + AMonster.PredSize * 2);
    until (SX > 0) and (SY > 0) and (SX < Map.SizeX - AMonster.Size) and (SY < Map.SizeY - AMonster.Size) and Map.PassableTiles[AMonster.PredSize][SX + Map.SizeX * SY];
    AMonster.Teleport(SX, SY);
    AMonster.Ai.Guard := false;
    repeat
      GoToX := ParentActor.LastTileX + Rnd.RandomSign;
      GoToY := ParentActor.LastTileY + Rnd.RandomSign;
    until Map.PassableTiles[AMonster.PredSize][GoToX + Map.SizeX * GoToY];
    AMonster.MoveTo(GoToX + Rnd.Random, GoToY + Rnd.Random);
    AMonster.Ai.Timeout := (AMonster.CurrentAction as TActionMoveAbstract).RemainingTime;
    Map.MonstersList.Add(AMonster);
  end;
end;

initialization
  RegisterSerializableData(TEnchantmentSendPatrolToPlayer);
  RegisterRandomEnchantment(TEnchantmentSendPatrolToPlayer, 0.5, ekNegative);
  RegisterEnchantment(TEnchantmentSendPatrolToPlayer);

end.


{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentStealthDamageBonus;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Addition to absolutely value of stealth damage (after multiplier, and so unaffected by it) }
  TEnchantmentStealthDamageBonus = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath;

procedure TEnchantmentStealthDamageBonus.Validate;
begin
  inherited Validate;
  if Strength = 0 then
    raise EDataValidationError.CreateFmt('Strength = 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentStealthDamageBonus.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  if Rnd.RandomBoolean or not CanBeNegative then
    Strength := Clamp(Sqr(Rnd.Random) * Quality * 4 + 3, 3.1, 100)
  else
    Strength := -Clamp(Sqr(Rnd.Random) * Quality * 4 + 3, 3.1, 24);
end;

function TEnchantmentStealthDamageBonus.IsPositive: Boolean;
begin
  Exit(Strength > 0);
end;

function TEnchantmentStealthDamageBonus.Description: String;
begin
  if Strength > 0 then
    Result := Format('Increases stealth damage by %.1n' + RequirementDescription, [Strength])
  else
    Result := Format('Decreases stealth damage by %.1n' + RequirementDescription, [-Strength]);
end;

initialization
  RegisterSerializableData(TEnchantmentStealthDamageBonus);
  RegisterRandomEnchantment(TEnchantmentStealthDamageBonus, 1.0, ekBoth);
  RegisterEnchantment(TEnchantmentStealthDamageBonus);

end.


{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentChangeMaxHealthPerSecond;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Heals/drains an absolute value of Max health per second }
  TEnchantmentChangeMaxHealthPerSecond = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject;const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
    procedure Update(const AParentActor: TObject; const AParentItem: TObject;
      const SecondsPassed: Single); override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath,
  GamePlayerCharacter;

procedure TEnchantmentChangeMaxHealthPerSecond.Validate;
begin
  inherited Validate;
  if Strength = 0 then
    raise EDataValidationError.CreateFmt('Strength = 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentChangeMaxHealthPerSecond.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  if Rnd.RandomBoolean or not CanBeNegative then
    Strength := Clamp(Sqr(Sqr(Rnd.Random)) * Quality / 300, 0.007, 0.03)
  else
    Strength := -Clamp(Sqr(Sqr(Rnd.Random)) * Quality / 300, 0.007, 0.03);
end;

function TEnchantmentChangeMaxHealthPerSecond.IsPositive: Boolean;
begin
  Exit(Strength > 0);
end;

function TEnchantmentChangeMaxHealthPerSecond.Description: String;
begin
  if Strength > 0 then
    Result := Format('Recovers %.3n of max health per second' + RequirementDescription, [Strength])
  else
    Result := Format('Degrades %.3n of max health per second' + RequirementDescription, [-Strength])
end;

procedure TEnchantmentChangeMaxHealthPerSecond.Update(const AParentActor: TObject;
  const AParentItem: TObject; const SecondsPassed: Single);
begin
  inherited Update(AParentActor, AParentItem, SecondsPassed);
  if Strength > 0 then
    (AParentActor as TPlayerCharacter).HealMaxHealth(Strength * SecondsPassed)
  else
  begin
    (AParentActor as TPlayerCharacter).MaxHealth += Strength * SecondsPassed; // Strength is negative
    if (AParentActor as TPlayerCharacter).MaxHealth < 1 then
      (AParentActor as TPlayerCharacter).MaxHealth := 1;
    if (AParentActor as TPlayerCharacter).Health > (AParentActor as TPlayerCharacter).MaxHealth then
      (AParentActor as TPlayerCharacter).Health := (AParentActor as TPlayerCharacter).MaxHealth;
  end;
end;

initialization
  RegisterSerializableData(TEnchantmentChangeMaxHealthPerSecond);
  RegisterRandomEnchantment(TEnchantmentChangeMaxHealthPerSecond, 1.0, ekBoth);
  RegisterEnchantment(TEnchantmentChangeMaxHealthPerSecond);

end.


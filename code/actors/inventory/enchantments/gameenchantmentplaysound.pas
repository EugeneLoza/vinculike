{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentPlaySound;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  CastleSoundEngine,
  GameEnchantmentAbstract;

type
  { Plays a looping sound
    doesn't keep track of the game is in foreground or background
    on Android workarounded by checking if the sound is played in Update and restarting it if needed }
  TEnchantmentPlaySound = class(TEnchantmentAbstract)
  strict private
    SoundName: String;
    SoundPlaying: TCastlePlayingSound;
  protected
    function CloneInternal: TEnchantmentAbstract; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
    procedure Update(const AParentActor: TObject; const AParentItem: TObject;
      const SecondsPassed: Single); override;
    function CanBeStatusEffect: Boolean; override;
    procedure Save(const Element: TDOMElement); override;
    procedure Stop; override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableData,
  GameSounds;

function TEnchantmentPlaySound.CloneInternal: TEnchantmentAbstract;
begin
  Result := inherited CloneInternal;
  (Result as TEnchantmentPlaySound).SoundName := SoundName;
end;

procedure TEnchantmentPlaySound.Validate;
begin
  inherited Validate;
  if Strength <= 0 then
    raise EDataValidationError.CreateFmt('Strength <= 0 in %s', [Self.ClassName]);
  if not SoundExists(SoundName) then
    raise EDataValidationError.CreateFmt('Unable to find sound %s in %s', [SoundName, Self.ClassName]);
end;

procedure TEnchantmentPlaySound.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  SoundName := Element.AttributeString('SoundName');
end;

function TEnchantmentPlaySound.IsPositive: Boolean;
begin
  Exit(false);
end;

procedure TEnchantmentPlaySound.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('SoundName', SoundName);
end;

function TEnchantmentPlaySound.Description: String;
begin
  Result := Format('Plays an annoying sound' + RequirementDescription, []);
end;

procedure TEnchantmentPlaySound.Update(const AParentActor: TObject;
  const AParentItem: TObject; const SecondsPassed: Single);
begin
  inherited Update(AParentActor, AParentItem, SecondsPassed);
  if (SoundPlaying = nil) then
  begin
    SoundPlaying := TCastlePlayingSound.Create(nil);
    SoundPlaying.Sound := GetSoundByName(SoundName); // Note: when sound not found GetSoundByName can be nil, this shouldn't hurt but keep an eye on this line
    SoundPlaying.Loop := true;
    SoundPlaying.Volume := Strength;
  end;
  if not SoundPlaying.Playing then
  begin
    SoundEngine.Play(SoundPlaying);
  end;
end;

function TEnchantmentPlaySound.CanBeStatusEffect: Boolean;
begin
  Exit(false);
end;

procedure TEnchantmentPlaySound.Stop;
begin
  inherited Stop;
  if SoundPlaying <> nil then
    SoundPlaying.Stop;
end;

destructor TEnchantmentPlaySound.Destroy;
begin
  if SoundPlaying <> nil then
  begin
    SoundPlaying.Stop;
    FreeAndNil(SoundPlaying);
  end;
  inherited Destroy;
end;

initialization
  RegisterSerializableData(TEnchantmentPlaySound);
  //RegisterRandomEnchantment(TEnchantmentPlaySound); - makes no sense being random?
  RegisterEnchantment(TEnchantmentPlaySound);

end.


{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentVisionBonus;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Increases or decreases vision range of the character }
  TEnchantmentVisionBonus = class(TEnchantmentAbstract)
  public const
    BlindfoldMargin = -999;
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject;const Quality: Single; const CanBeNegative: Boolean); override;
  public
    class function MakesSense(const AItem: TObject): Boolean; override;
    function IsPositive: Boolean; override;
    function Description: String; override;
    function CanBeStatusEffect: Boolean; override;
  end;

implementation
uses
  GameSerializableData,
  GameInventoryItem, GameApparelSlots, GameRandom, GameMath;

procedure TEnchantmentVisionBonus.Validate;
begin
  inherited Validate;
  if Strength = 0 then
    raise EDataValidationError.CreateFmt('Strength = 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentVisionBonus.RandomInternal(const AParent: TObject;
  const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  if Rnd.RandomBoolean and CanBeNegative then
    Strength := -Clamp(Sqr(Rnd.Random) * Quality * 3, 3, 100)
  else
    Strength := Clamp(Sqr(Rnd.Random) * Quality * 3, 3, 100);
end;

class function TEnchantmentVisionBonus.MakesSense(const AItem: TObject): Boolean;
begin
  Exit(true);
end;

function TEnchantmentVisionBonus.IsPositive: Boolean;
begin
  Exit(Strength > 0);
end;

function TEnchantmentVisionBonus.Description: String;
begin
  if Strength > 0 then
    Result := Format('Increases visible range by %.0n' + RequirementDescription, [Strength])
  else
  if Strength > BlindfoldMargin then // arbitrary number which is very unlikely to be "countered" by any visible range bonuses; all blindfolds should have -9999 or something like that
    Result := Format('Reduces visible range by %.0n' + RequirementDescription, [-Strength])
  else
    Result := Format('Blindfolds the character' + RequirementDescription, [-Strength])
end;

function TEnchantmentVisionBonus.CanBeStatusEffect: Boolean;
begin
  Exit(true);
end;

initialization
  RegisterSerializableData(TEnchantmentVisionBonus);
  RegisterRandomEnchantment(TEnchantmentVisionBonus, 1.0, ekBoth);
  RegisterEnchantment(TEnchantmentVisionBonus);

end.


{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentDisorientation;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Disorients the character, making all tiles not directly visible as "forgotten" }
  TEnchantmentDisorientation = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function EquipIdentifies: Boolean; override;
    function IsPositive: Boolean; override;
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath;

procedure TEnchantmentDisorientation.Validate;
begin
  inherited Validate;
  if Strength < 1 then
    raise EDataValidationError.CreateFmt('Strength < 1 in %s', [Self.ClassName]);
end;

procedure TEnchantmentDisorientation.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  Strength := Clamp(Sqr(Rnd.Random) * Quality * 250, 250, 1500);
end;

function TEnchantmentDisorientation.EquipIdentifies: Boolean;
begin
  Exit(true);
end;

function TEnchantmentDisorientation.IsPositive: Boolean;
begin
  Exit(false);
end;

function TEnchantmentDisorientation.Description: String;
begin
  Result := Format('Disorients the character by %.0n' + RequirementDescription, [Strength]);
end;

initialization
  RegisterSerializableData(TEnchantmentDisorientation);
  RegisterRandomEnchantment(TEnchantmentDisorientation, 0.5, ekNegative);
  RegisterEnchantment(TEnchantmentDisorientation);

end.


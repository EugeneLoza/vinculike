{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentStunParent;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  DOM,
  GameEnchantmentAbstract;

type
  { Periodically teleports target for short distances }
  TEnchantmentStunParent = class(TEnchantmentPeriodicAbstract)
  protected
    StunDuration: Single;
    procedure Perform(const AParentActor: TObject; const AParentItem: TObject); override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
    function CloneInternal: TEnchantmentAbstract; override;
    procedure RandomInternal(const AParent: TObject;const Quality: Single; const CanBeNegative: Boolean); override;
    function IdentificationComplexity(const AQuality: Single): Single; override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
    procedure Save(const Element: TDOMElement); override;
  end;

implementation
uses
  CastleXmlUtils, CastleStringUtils,
  GameSerializableData,
  GamePlayerCharacter, GameActionPlayerStunned,
  GameRandom, GameLog, GameColors;

procedure TEnchantmentStunParent.Validate;
begin
  inherited Validate;
  if StunDuration <= 0 then
    raise EDataValidationError.CreateFmt('StunDuration <= 0 in %s', [Self.ClassName]);
  if Strength <= 0 then
    raise EDataValidationError.CreateFmt('Strength <= 0 in %s', [Self.ClassName]);
  if Strength > 1 then
    raise EDataValidationError.CreateFmt('Strength > 1 in %s', [Self.ClassName]);
end;

procedure TEnchantmentStunParent.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  StunDuration := Element.AttributeSingle('StunDuration');
end;

procedure TEnchantmentStunParent.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('StunDuration', StunDuration);
end;

function TEnchantmentStunParent.CloneInternal: TEnchantmentAbstract;
begin
  Result := inherited CloneInternal;
  TEnchantmentStunParent(Result).StunDuration := StunDuration;
end;

procedure TEnchantmentStunParent.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  Strength := 0.1 + Rnd.Random * 0.9;
  StunDuration := 0.3 + Sqr(Rnd.Random)* Quality / 10;
  EffectPeriod := 0.5 + StunDuration + Rnd.Random * 60;
end;

function TEnchantmentStunParent.IdentificationComplexity(const AQuality: Single): Single;
begin
  Exit(2.5 * Rnd.Random * AQuality);
end;

function TEnchantmentStunParent.IsPositive: Boolean;
begin
  Exit(false);
end;

function TEnchantmentStunParent.Description: String;
begin
  Result := Format('Randomly stuns character' + RequirementDescription, []);
end;

procedure TEnchantmentStunParent.Perform(const AParentActor: TObject;
  const AParentItem: TObject);
begin
  {$DEFINE ParentPlayer:=(AParentActor as TPlayerCharacter)}
  if not ParentPlayer.Unsuspecting then
  begin
    if Rnd.Random > ParentPlayer.ResistStun then
    begin
      ParentPlayer.CurrentAction := TActionPlayerStunned.NewAction(AParentActor, StunDuration);
      ParentPlayer.CurrentAction.Start;
      ParentPlayer.AddResistStun;
      ShowLog('Suddenly %s''s muscles spasm (stunned for %.1ns)', [ParentPlayer.Data.DisplayName, StunDuration], ColorLogStaminaDamage);
      ParentPlayer.Particle('STUN', ColorParticlePlayerStunned);
    end else
      ShowLog('%s suddenly feels her muscles spasm, but manages to stay in control', [ParentPlayer.Data.DisplayName], ColorLogStaminaDamage);
  end;
end;

initialization
  RegisterSerializableData(TEnchantmentStunParent);
  RegisterRandomEnchantment(TEnchantmentStunParent, 1.0, ekNegative);
  RegisterEnchantment(TEnchantmentStunParent);

end.


{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentThrowMarkAtParent;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  GameEnchantmentAbstract, GameMarkAbstract;

type
  { Throws a (harmful) mark around the parent actor regularly
    mark must be independent, as enchantment/item cannot be parent of a mark
    note: if item with this enchantment will be destroyed/removed: mark will be removed too
    note: not TEnchantmentPeriodicAbstract child, because delays are random }
  TEnchantmentThrowMarkAtParent = class(TEnchantmentAbstract)
  strict private
    Mark: TMarkTargetAbstract;
    Phase: Single;
    Duration: Single;
    NextDelay: Single;
    Range: Single;
    MarkData: TMarkAbstractData; // Shouldn't it be in something like TEnchantmentData? TODO
    OriginalEnchantment_WorkaroundAllEnchantmentsReferenceToSameMarkDataAndThereforeCrashingOnFree: Boolean;
    procedure MarkFinished;
  protected
    function CloneInternal: TEnchantmentAbstract; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
    function CanBeSaved: Boolean; override;
    function CanBeStatusEffect: Boolean; override;
    procedure Save(const Element: TDOMElement); override;
    procedure Update(const AParentActor: TObject; const AParentItem: TObject;
      const SecondsPassed: Single); override;
    procedure Stop; override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableData,
  GamePlayerCharacter, GameMap, GameRandom;

function TEnchantmentThrowMarkAtParent.Description: String;
begin
  Result := Format('Once in %.1n-%.1n seconds tries to affect the wearer' + RequirementDescription, [Duration/2, Duration]);
end;

function TEnchantmentThrowMarkAtParent.CanBeSaved: Boolean;
begin
  Exit(false);
end;

function TEnchantmentThrowMarkAtParent.CanBeStatusEffect: Boolean;
begin
  Exit(false);
end;

procedure TEnchantmentThrowMarkAtParent.Update(const AParentActor: TObject;
  const AParentItem: TObject; const SecondsPassed: Single);
var
  SX, SY: Single;
  TargetActor: TPlayerCharacter;
begin
  inherited Update(AParentActor, AParentItem, SecondsPassed);
  Phase += SecondsPassed;
  if (Mark = nil) and (Phase > NextDelay) then
  begin
    if Rnd.Random < Strength then
    begin
      Mark := MarkData.Mark.Create as TMarkTargetAbstract;
      Mark.Target := AParentActor;
      Mark.Parent := AParentActor;
      Mark.Data := MarkData;
      Mark.SetSize(Mark.Data.Size);
      TargetActor := AParentActor as TPlayerCharacter;;
      repeat
        SX := TargetActor.CenterX + (Rnd.Random - 0.5) * (TargetActor.Data.Size + Range);
        SY := TargetActor.CenterY + (Rnd.Random - 0.5) * (TargetActor.Data.Size + Range);
      until (SX - Mark.HalfSize > 0.5) and (SY - Mark.HalfSize > 0.5) and (SX + Mark.HalfSize < Map.SizeX - 0.5) and (SY + Mark.HalfSize < Map.SizeY - 0.5);
      Mark.MoveCenterTo(SX, SY);
      Mark.OnFinished := @MarkFinished;
      Map.MarksList.Add(Mark);
    end;
    Phase -= NextDelay;
    NextDelay := Duration * (0.5 + 0.5 * Rnd.Random);
  end;
end;

procedure TEnchantmentThrowMarkAtParent.Stop;
begin
  inherited Stop;
  if Mark <> nil then
  begin
    Map.MarksList.Remove(Mark); // this will free it
    Mark := nil;
  end;
end;

destructor TEnchantmentThrowMarkAtParent.Destroy;
begin
  {{$WARNING 'We are making clone of enchantments but not of MarkData which results that when we free enchantment clones we free MarkData for every clone and crash for the second one'} ---- won'tfix, it works, so let it work. I don't see any meaningful solution other than cloning a mark too, which is a huge load of work }
  if OriginalEnchantment_WorkaroundAllEnchantmentsReferenceToSameMarkDataAndThereforeCrashingOnFree then
    FreeAndNil(MarkData);
  inherited Destroy;
end;

procedure TEnchantmentThrowMarkAtParent.MarkFinished;
begin
  Mark := nil;
end;

function TEnchantmentThrowMarkAtParent.CloneInternal: TEnchantmentAbstract;
begin
  Result := inherited;
  (Result as TEnchantmentThrowMarkAtParent).Phase := 0;
  (Result as TEnchantmentThrowMarkAtParent).OriginalEnchantment_WorkaroundAllEnchantmentsReferenceToSameMarkDataAndThereforeCrashingOnFree := false;
  (Result as TEnchantmentThrowMarkAtParent).Duration := Duration;
  (Result as TEnchantmentThrowMarkAtParent).NextDelay := Duration * (0.5 + 0.5 * Rnd.Random);
  (Result as TEnchantmentThrowMarkAtParent).Range := Range;
  (Result as TEnchantmentThrowMarkAtParent).MarkData := MarkData;
end;

procedure TEnchantmentThrowMarkAtParent.Validate;
begin
  inherited Validate;
  if Strength <= 0 then
    raise EDataValidationError.CreateFmt('Strength <= 0 in %s', [Self.ClassName]);
  if Duration <= 0 then
    raise EDataValidationError.CreateFmt('Duration <= 0 in %s', [Self.ClassName]);
  if Range <= 1 then
    raise EDataValidationError.CreateFmt('Range <= 1 in %s', [Self.ClassName]);
  if MarkData = nil then
    raise EDataValidationError.CreateFmt('MarkData = nil in %s', [Self.ClassName]);
end;

procedure TEnchantmentThrowMarkAtParent.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  Duration := Element.AttributeSingle('Duration');
  Range := Element.AttributeSingle('Range');
  MarkData := TMarkAbstractData.ReadClass(FBaseUrl, Element.Child('Mark', false)) as TMarkAbstractData;
  OriginalEnchantment_WorkaroundAllEnchantmentsReferenceToSameMarkDataAndThereforeCrashingOnFree := true;
end;

function TEnchantmentThrowMarkAtParent.IsPositive: Boolean;
begin
  Exit(false); // depends on the mark, but for now this way. TODO?
end;

procedure TEnchantmentThrowMarkAtParent.Save(const Element: TDOMElement);
begin
  raise Exception.CreateFmt('%s cannot be saved', [Self.ClassName]);
end;

initialization
  RegisterSerializableData(TEnchantmentThrowMarkAtParent);
  RegisterEnchantment(TEnchantmentThrowMarkAtParent);

end.


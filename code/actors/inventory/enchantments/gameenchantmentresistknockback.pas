{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentResistKnockback;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Increases chance to resist most (but not all) knockback effects
    for now base chance is zero, so negative values don't make sense }
  TEnchantmentResistKnockback = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath;

procedure TEnchantmentResistKnockback.Validate;
begin
  inherited Validate;
  if Strength = 0 then
    raise EDataValidationError.CreateFmt('Strength = 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentResistKnockback.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  // note: currently doesn't make sense negative
  Strength := Clamp(Sqr(Sqr(Rnd.Random)) * Quality / 12, 0.05, 0.3)
end;

function TEnchantmentResistKnockback.IsPositive: Boolean;
begin
  Exit(Strength > 0);
end;

function TEnchantmentResistKnockback.Description: String;
begin
  if Strength > 0 then
    Result := Format('Increases knockback resistance by %d%%%%' + RequirementDescription, [Round(100 * Strength)]) // We must call stupid "%%%%" here because when showing log we again call Format whic will "eat" %%->% symbol, we need two
  else
    Result := Format('Decreases knockback resistance by %d%%%%' + RequirementDescription, [Round(-100 * Strength)]);
end;

initialization
  RegisterSerializableData(TEnchantmentResistKnockback);
  RegisterRandomEnchantment(TEnchantmentResistKnockback, 0.3, ekPositive);
  RegisterEnchantment(TEnchantmentResistKnockback);

end.


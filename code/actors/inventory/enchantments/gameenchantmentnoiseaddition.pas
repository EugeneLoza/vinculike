{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentNoiseAddition;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Adds a static noise value, not affected by any multipliers
    note, lower noise value is always clamped at "body noise"}
  TEnchantmentNoiseAddition = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath;

procedure TEnchantmentNoiseAddition.Validate;
begin
  inherited Validate;
  if Strength = 0 then
    raise EDataValidationError.CreateFmt('Strength = 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentNoiseAddition.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  if CanBeNegative and Rnd.RandomBoolean then
    Strength := Clamp(Sqr(Rnd.Random) * 2 * Quality, 1, 16)
  else
    Strength := -Clamp(Sqr(Rnd.Random) * Quality, 0.5, 7);
end;

function TEnchantmentNoiseAddition.IsPositive: Boolean;
begin
  Exit(Strength < 0);
end;

function TEnchantmentNoiseAddition.Description: String;
begin
  if Strength > 0 then
    Result := Format('Increases total noise by %.1n' + RequirementDescription, [Strength])
  else
    Result := Format('Reduces total noise by %.1n' + RequirementDescription, [-Strength]);
end;

initialization
  RegisterSerializableData(TEnchantmentNoiseAddition);
  RegisterRandomEnchantment(TEnchantmentNoiseAddition, 0.5, ekBoth);
  RegisterEnchantment(TEnchantmentNoiseAddition);

end.


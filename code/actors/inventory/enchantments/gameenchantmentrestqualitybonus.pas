{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentRestQualityBonus;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  GameEnchantmentAbstract;

type
  { Adds resting quality }
  TEnchantmentRestQualityBonus = class(TEnchantmentAbstract)
  protected
    procedure Validate; override;
    procedure RandomInternal(const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean); override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData, GameRandom, GameMath;

procedure TEnchantmentRestQualityBonus.Validate;
begin
  inherited Validate;
  if Strength = 0 then
    raise EDataValidationError.CreateFmt('Strength = 0 in %s', [Self.ClassName]);
end;

procedure TEnchantmentRestQualityBonus.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  if Rnd.RandomBoolean or not CanBeNegative then
    Strength := Clamp(Sqr(Rnd.Random) * Quality, 0.1, 100)
  else
    Strength := -Clamp(Sqr(Rnd.Random) * Quality / 5, 0.1, 100);
end;

function TEnchantmentRestQualityBonus.IsPositive: Boolean;
begin
  Exit(Strength > 0);
end;

function TEnchantmentRestQualityBonus.Description: String;
begin
  if Strength > 0 then
    Result := Format('Increases resting quality by %d%%%%' + RequirementDescription, [Round(100 * (Strength))]) // We must call stupid "%%%%" here because when showing log we again call Format whic will "eat" %%->% symbol, we need two
  else
    Result := Format('Reduces resting quality by %d%%%%' + RequirementDescription, [Round(100 * (-Strength))]);
end;

initialization
  RegisterSerializableData(TEnchantmentRestQualityBonus);
  RegisterRandomEnchantment(TEnchantmentRestQualityBonus, 1.0, ekBoth);
  RegisterEnchantment(TEnchantmentRestQualityBonus);

end.


{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentTeleportRandomly;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  DOM,
  GameEnchantmentAbstract;

type
  { Periodically teleports target for short distances }
  TEnchantmentTeleportRandomly = class(TEnchantmentPeriodicAbstract)
  protected
    MaxDistance: Single;
    procedure Perform(const AParentActor: TObject; const AParentItem: TObject); override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
    function CloneInternal: TEnchantmentAbstract; override;
    procedure RandomInternal(const AParent: TObject;const Quality: Single; const CanBeNegative: Boolean); override;
    function IdentificationComplexity(const AQuality: Single): Single; override;
  public
    function IsPositive: Boolean; override;
    function Description: String; override;
    procedure Save(const Element: TDOMElement); override;
  end;

implementation
uses
  CastleXmlUtils, CastleStringUtils,
  GameSerializableData, GameActor,
  GameRandom, GameLog, GameColors, GameMap, GameViewGame;

procedure TEnchantmentTeleportRandomly.Validate;
begin
  inherited Validate;
  if MaxDistance <= 2 then
    raise EDataValidationError.CreateFmt('MaxDistance <= 2 in %s', [Self.ClassName]);
  if Strength <= 0 then
    raise EDataValidationError.CreateFmt('Strength <= 0 in %s', [Self.ClassName]);
  if Strength > 1 then
    raise EDataValidationError.CreateFmt('Strength > 1 in %s', [Self.ClassName]);
end;

procedure TEnchantmentTeleportRandomly.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  MaxDistance := Element.AttributeSingle('MaxDistance');
end;

procedure TEnchantmentTeleportRandomly.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('MaxDistance', MaxDistance);
end;

function TEnchantmentTeleportRandomly.CloneInternal: TEnchantmentAbstract;
begin
  Result := inherited CloneInternal;
  TEnchantmentTeleportRandomly(Result).MaxDistance := MaxDistance;
end;

procedure TEnchantmentTeleportRandomly.RandomInternal(
  const AParent: TObject; const Quality: Single; const CanBeNegative: Boolean);
begin
  inherited;
  Strength := 0.1 + Rnd.Random * 0.9;
  EffectPeriod := 1.0 + Rnd.Random * 60;
  MaxDistance := 3 + Sqr(Rnd.Random) * 100;
end;

function TEnchantmentTeleportRandomly.IdentificationComplexity(
  const AQuality: Single): Single;
begin
  Exit(2.5 * Rnd.Random * AQuality);
end;

function TEnchantmentTeleportRandomly.IsPositive: Boolean;
begin
  Exit(false);
end;

function TEnchantmentTeleportRandomly.Description: String;
begin
  Result := Format('Randomly teleports character' + RequirementDescription, []);
end;

procedure TEnchantmentTeleportRandomly.Perform(const AParentActor: TObject;
  const AParentItem: TObject);
var
  RetryCount: Integer;
  SX, SY: Single;
  AX, AY: Int16;
begin
  {$DEFINE ParentActor:=(AParentActor as TActor)}
  if not ParentActor.Unsuspecting then
  begin
    RetryCount := 0;
    repeat
      repeat
        SX := ParentActor.X + 2.0 * (Rnd.Random - 0.5) * (MaxDistance);
        SY := ParentActor.Y + 2.0 * (Rnd.Random - 0.5) * (MaxDistance);
        AX := Trunc(SX);
        AY := Trunc(SY);
        // note PassablePlayerTiles because otherwise pathfinding system may fail
      until (AX > 0) and (AY > 0) and (AX < Map.PredSizeX) and (AY < Map.PredSizeY) and (Map.PassableTiles[ParentActor.PredSize][AX + Map.SizeX * AY]) and (Sqr(SX - ParentActor.X) + Sqr(SY - ParentActor.Y) < Sqr(MaxDistance));
      Inc(RetryCount);
    until Map.Ray(ParentActor.PredSize, ParentActor.X, ParentActor.Y, SX, SY); // may be redundant to PassablePlayerTiles check
    ParentActor.Teleport(SX, SY);

    if ViewGame.CurrentCharacter = AParentActor then
    begin
      ViewGame.InternalMapInterface.TeleportTo(AX, AY);
      ViewGame.ShakeCharacter;
    end;

    ShowLog('%s suddenly feels really weird', [ParentActor.Data.DisplayName], ColorLogTeleport);
    ParentActor.Particle('TELEPORT', ColorParticlePlayerTeleport);
  end;
end;

initialization
  RegisterSerializableData(TEnchantmentTeleportRandomly);
  RegisterRandomEnchantment(TEnchantmentTeleportRandomly, 1.0, ekNegative);
  RegisterEnchantment(TEnchantmentTeleportRandomly);

end.


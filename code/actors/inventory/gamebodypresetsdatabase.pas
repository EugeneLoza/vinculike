{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Database of character body presets
  Creates a "ready mod" to persist between installations
  and even different systems
  If you want to give the created mood to others,
  make sure to edit it and assign a unique ID }
unit GameBodyPresetsDatabase;

{$INCLUDE compilerconfig.inc}

interface

uses
  GameInventory;

var
  BodyPresets: TBodyPresetsList;

{ Clear all body presets }
procedure ClearBodyPresets;
{ Load body presets from an URL}
procedure LoadBodyPresets(const Url: String);
{ Save presets marked as "local" into CastleConfig folder }
procedure SaveLocalPresets;
{ Free everything related to body presets,
  note: doesn't save anything automatically }
procedure FreeBodyPresets;
implementation
uses
  SysUtils, DOM,
  CastleXMLUtils, CastleUriUtils, CastleFilesUtils,
  GameLog, GameRandom;

var
   { XML file with presets }
  LocalPresetsUrl: String;
  { XML file with package definition, aka mod metadata }
  LocalPresetsPackageUrl: String;

procedure ClearBodyPresets;
begin
  LocalPresetsUrl := ApplicationConfig('presets/presets.xml');
  LocalPresetsPackageUrl := ApplicationConfig('presets/package.xml');
  if BodyPresets = nil then
    BodyPresets := TBodyPresetsList.Create(true)
  else
    BodyPresets.Clear;
  BodyPresets.Add(TInventory.RandomBody);
end;

procedure LoadBodyPresets(const Url: String);
var
  Preset: TBodyPreset;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  LogNormal('Loading body presets: %s', [Url]);
  Doc := URLReadXML(Url);
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('BodyPreset');
    try
      while Iterator.GetNext do
      begin
        Preset := TBodyPreset.Create;
        Preset.Load(Iterator.Current);
        Preset.IsLocal := Url = LocalPresetsUrl;
        BodyPresets.Add(Preset);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;

procedure SaveLocalPresets;
var
  Doc: TXMLDocument;
  I: Integer;
  Element: TDOMElement;
begin
  try
    // write presets
    Doc := TXMLDocument.Create;
    Doc.AppendChild(Doc.CreateElement('Root'));
    for I := 0 to Pred(BodyPresets.Count) do
      if BodyPresets[I].IsLocal then
      begin
        BodyPresets[I].Save(Doc.DocumentElement.CreateChild('BodyPreset'));
        //here also try to account for dependencies of BodyPresets[I]
      end;
    UrlWriteXML(Doc, LocalPresetsUrl);
    FreeAndNil(Doc);

    // write package.xml
    Doc := TXMLDocument.Create;
    Doc.AppendChild(Doc.CreateElement('Root'));
    // note that we add a random number here to minimize potential conflicts in case the player forgets to change it before using the local present as a mod
    Doc.DocumentElement.AttributeSet('Id', 'local_presets_' + Rnd.Random32bit.ToString + ' (change before converting this to a mod)');
    // TODO: automatically detect dependencies from bodyparts
    Doc.DocumentElement.AttributeSet('Dependencies', 'core');
    Doc.DocumentElement.AttributeSet('BaseGame', false);
    Element := Doc.DocumentElement.CreateChild('Presets');
    Element.AttributeSet('Url', ExtractUriName(LocalPresetsUrl));
    UrlWriteXML(Doc, LocalPresetsPackageUrl);
  finally
    FreeAndNil(Doc);
  end;
end;

procedure FreeBodyPresets;
begin
  FreeAndNil(BodyPresets);
end;

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameInventoryItem;

{$INCLUDE compilerconfig.inc}

interface

uses
  Generics.Collections, DOM,
  GameSerializableObject,
  GameItemDataAbstract, GameItemData, GameAbstractItem, GameEnchantmentAbstract,
  GameColorTags;

type
  { An item that can be used by the player character (equipped or consumed, etc.)
    Note "TInventoryItem", not just "TItem" - to make sure it won't conflict
    with some FPC/CGE internal stuff }
  TInventoryItem = class(TAbstractItem)
  strict private
    function HasBondageEnchantment(const Inventory: TObject; const IncludeUnidentified: Boolean): Boolean;
  public
    Parent: TObject; // cannot typecast to Inventory
    Broken: Boolean;
    Durability: Single;
    MaxDurability: Single;
    Enchantments: TEnchantmentsList; // todo: if no enchantments = nil
    Color: TColorDictionary;
    TimeWorn: Single;
    function IsBondage(const Inventory: TObject): Boolean;
    function IsBlindfold(const Inventory: TObject): Boolean;
    function HasStochasticEnchantments: Boolean;
    function HasDeterministicEnchantments: Boolean;
    function IsKnownBondage: Boolean;
    function ClampedDurability: Integer;
    function ClampedMaxDurability: Integer;
    { SkillPreserveMax corresponds to how efficiently Max value is preserved
      SkillRepairCurrent corresponds to how efficiently the item current value
      will be increased }
    procedure Fix(const SkillPreserveMax, SkillRepairCurrent: Single);
    function ItemData: TItemData;
    procedure Update(const SecondsPassed: Single); virtual;
    { Stops this item's internal stuff }
    procedure Stop; virtual;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    class function NewItem(const AItemData: TItemDataAbstract): TInventoryItem;
    procedure AddRandomEnchantments(const EnchantmentChance: Single; const EnchantmentQuality: Single);
    constructor Create(const NotLoading: Boolean = true); override;
    destructor Destroy; override;
  end;
  TInventoryItemsList = specialize TObjectList<TInventoryItem>;

implementation
uses
  SysUtils,
  CastleXmlUtils, CastleColors,
  GameEnchantmentBondage, GameEnchantmentVisionBonus,
  GameInventory, GameApparelSlots,
  GameRandom, GameSounds, GameLog, GameMath;

{$IFDEF SafeTypecast}
{$DEFINE ParentInventory:=(Parent as TInventory)}
{$ELSE}
{$DEFINE ParentInventory:=TInventory(Parent)}
{$ENDIF}

function TInventoryItem.HasBondageEnchantment(const Inventory: TObject;
  const IncludeUnidentified: Boolean): Boolean;
var
  I: Integer;
begin
  Result := false;
  // if Enchantments <> nil then
  for I := 0 to Pred(Enchantments.Count) do
    if (Enchantments[i] is TEnchantmentBondage) and (IncludeUnidentified or Enchantments[i].Identified) then
      if (Inventory = nil) or (Inventory as TInventory).EnchantmentRequirementsMet(Enchantments[i]) then
        Exit(true);
end;

function TInventoryItem.IsBondage(const Inventory: TObject): Boolean;
begin
  Exit(ItemData.Bondage or HasBondageEnchantment(Inventory, true));
end;

function TInventoryItem.IsBlindfold(const Inventory: TObject): Boolean;
var
  I: Integer;
begin
  Result := false;
  // if Enchantments <> nil then
  for I := 0 to Pred(Enchantments.Count) do
    if (Enchantments[i] is TEnchantmentVisionBonus) and (Enchantments[i].Strength <= TEnchantmentVisionBonus.BlindfoldMargin) then
      if (Inventory = nil) or (Inventory as TInventory).EnchantmentRequirementsMet(Enchantments[i]) then
        Exit(true);
end;

function TInventoryItem.HasStochasticEnchantments: Boolean;
var
  I: Integer;
begin
  for I := 0 to Pred(Enchantments.Count) do
    if Enchantments[I].IsStochastic then
      Exit(true);
  Exit(false);
end;

function TInventoryItem.HasDeterministicEnchantments: Boolean;
var
  I: Integer;
begin
  for I := 0 to Pred(Enchantments.Count) do
    if not Enchantments[I].IsStochastic then
      Exit(true);
  Exit(false);
end;

function TInventoryItem.IsKnownBondage: Boolean;
begin
  Exit(ItemData.Bondage or HasBondageEnchantment(nil, false));
end;

function TInventoryItem.ClampedDurability: Integer;
begin
  if Durability < 1.5 then
    Exit(1)
  else
  if Durability >= 998.5 then
    Exit(999)
  else
    Exit(Round(Durability));
end;

function TInventoryItem.ClampedMaxDurability: Integer;
begin
  if MaxDurability < 1.5 then
    Exit(1)
  else
  if MaxDurability >= 998.5 then
    Exit(999)
  else
    Exit(Round(MaxDurability));
end;

procedure TInventoryItem.Fix(const SkillPreserveMax, SkillRepairCurrent: Single);
begin
  //A2+0,5 * B2 * ((B2-A2)/B2)^1,1
  MaxDurability := Durability + SkillPreserveMax * MaxDurability * PowerSingle((MaxDurability - Durability) / MaxDurability, 1.1);
  Durability := Durability + SkillRepairCurrent * (MaxDurability - Durability);
  if Broken then
    Broken := false;
  Sound(ItemData.SoundRepair);
end;

function TInventoryItem.ItemData: TItemData;
begin
  Exit(Data as TItemData);
end;

procedure TInventoryItem.Update(const SecondsPassed: Single);
var
  E: TEnchantmentAbstract;
begin
  TimeWorn += SecondsPassed;
  for E in Enchantments do
    if Parent <> nil then // in case an enchantment unequipped or disintegrated this item, don't go crazy
      if ParentInventory.EnchantmentRequirementsMet(E) then
        E.Update(ParentInventory.Parent, Self, SecondsPassed); // TODO
end;

procedure TInventoryItem.Stop;
var
  E: TEnchantmentAbstract;
begin
  Parent := nil;
  for E in Enchantments do
    E.Stop;
end;

procedure TInventoryItem.Save(const Element: TDOMElement);
var
  ColorTag: String;
  E: TEnchantmentAbstract;
begin
  inherited Save(Element);
  Element.AttributeSet('Broken', Broken);
  Element.AttributeSet('Durability', Durability);
  Element.AttributeSet('MaxDurability', MaxDurability);
  Element.AttributeSet('TimeWorn', TimeWorn);
  if ItemData.ColorTags <> nil then
    for ColorTag in ItemData.ColorTags.Keys do
      Element.AttributeColorSet('ColorTag_' + ColorTag, Color[ColorTag]);
  for E in Enchantments do
    if E.CanBeSaved then
      E.Save(Element.CreateChild('Enchantment'));
end;

procedure TInventoryItem.Load(const Element: TDOMElement);
var
  ColorTag: String;
  E: TEnchantmentAbstract;
  Iterator: TXMLElementIterator;
begin
  inherited;
  Broken := Element.AttributeBoolean('Broken');
  Durability := Element.AttributeSingle('Durability');
  MaxDurability := Element.AttributeSingle('MaxDurability');
  TimeWorn := Element.AttributeSingleDef('TimeWorn', 0);
  if ItemData.ColorTags <> nil then
    for ColorTag in ItemData.ColorTags.Keys do
      Color.Add(ColorTag, Element.AttributeColorDef('ColorTag_' + ColorTag, ItemData.ColorTags[ColorTag][Rnd.Random(ItemData.ColorTags[ColorTag].Count)]));
  Enchantments := TEnchantmentsList.Create(true);

  Iterator := Element.ChildrenIterator('Enchantment');
  try
    while Iterator.GetNext do
    begin
      E := TEnchantmentAbstract.ReadClass('', Iterator.Current) as TEnchantmentAbstract; // note: enchantments can't access base url (Data.FBaseUrl - doesn't seem like it'll help)? doesn't seem to affect anything, but need to keep an eye on it // what it _CAN_ break is enchantments that use assets, but all of those are not loadable
      Enchantments.Add(E);
    end;
  finally FreeAndNil(Iterator) end;

  for E in ItemData.Enchantments do // we must readd enchantments from item data, the ones that cannot be saved normally
    if not E.CanBeSaved then
      Enchantments.Add(E.Clone);

  if (Enchantments.Count = 0) and (ItemData.Enchantments.Count > 0) then
  begin
    ShowError('No enchantments loaded for %s. Assuming default ones', [ItemData.Id]);
    for E in ItemData.Enchantments do
      Enchantments.Add(E.Clone);
  end;
end;

class function TInventoryItem.NewItem(const AItemData: TItemDataAbstract
  ): TInventoryItem;
var
  E: TEnchantmentAbstract;
  ColorTag: String;
begin
  Result := TInventoryItem.Create(true);
  if not (AItemData is TItemData) then
    raise Exception.CreateFmt('Got item data: %s, expected: TItemData in %s', [AItemData.ClassName, Self.ClassName]);
  Result.Data := AItemData;
  Result.MaxDurability := 1 + Sqrt(Rnd.Random) * (Result.ItemData.Durability - 1);
  Result.Durability := 1 + Sqrt(Rnd.Random) * (Result.MaxDurability - 1);

  Result.Enchantments := TEnchantmentsList.Create(true);
  for E in Result.ItemData.Enchantments do
    Result.Enchantments.Add(E.Clone);

  if Result.ItemData.ColorTags <> nil then
    for ColorTag in Result.ItemData.ColorTags.Keys do
      Result.Color.Add(ColorTag, Result.ItemData.ColorTags[ColorTag][Rnd.Random(Result.ItemData.ColorTags[ColorTag].Count)]);
end;

procedure TInventoryItem.AddRandomEnchantments(const EnchantmentChance: Single;
  const EnchantmentQuality: Single);
var
  E, RandomEnchantment: TEnchantmentAbstract;
  CanBeNegative: Boolean;
begin
  CanBeNegative := Rnd.RandomBoolean;
  if Rnd.Random < EnchantmentChance then
  begin
    repeat
      RandomEnchantment := TEnchantmentAbstract.Random(Self, {Rnd.Random * }EnchantmentQuality, CanBeNegative); // TODO: multiply by Random here, not in the enchantments?
      for E in Enchantments do
        if E.ClassName = RandomEnchantment.ClassName then
        begin
          FreeAndNil(RandomEnchantment);
          break;
        end;
      if RandomEnchantment <> nil then
        Enchantments.Add(RandomEnchantment);
    until not(Rnd.Random < Clamp(0.2 * ItemData.EnchantmentPotential, 0.2, 0.9)); // chance for multiple enchantments, it's not straightforward
  end;
end;

constructor TInventoryItem.Create(const NotLoading: Boolean);
begin
  inherited;
  Color := TColorDictionary.Create;
  Color.Add('none', White);
end;

destructor TInventoryItem.Destroy;
begin
  FreeAndNil(Color);
  FreeAndNil(Enchantments);
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TInventoryItem);
end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameItemData;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, Generics.Collections, DOM,
  CastleImages, CastleGlImages, CastleRectangles, CastleStringUtils,
  GameApparelSlots, GameItemDataAbstract, GameEnchantmentAbstract,
  GameColorTags;

const
  // Below this the item has a growing chance to disintegrate when breaking
  ItemDisintegrationThreshold = 10;

type
  TItemData = class(TItemDataAbstract)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    MapImageRect: TFloatRectangle;
    MapImage: TRgbAlphaImage;
    MapImageDrawable: TDrawableImage; // for now only for Vinculopedia?
    CoversTop, CoversBottom: Boolean;
    { 1 = decent clothes covering nipples/crotch
      0 = doesn't affect
      -1 = indecent clothes that accents nudity/skimpiness }
    //DecentNipples, DecentCrotch: Single (-1..1)
    { ipNipples, ipBreasts, ipCrotch, ipNethers, ipArmpits, ipNeck, ipSoles, ipRibs, ipBelly, ipNose, ipMouth, ipEyes }
    //ProtectsInteractionPoints: TSetOfInteractionPoints;
    { Cached number of slots of this item }
    NumberOfSlots: Byte;
    { WIP: how strong the enchantments on this item can be }
    EnchantmentPotential: Single;
    { Damage >0 means it's a weapon that can deal damage
      Damage <=0 means it's a restraint that uses "punch" damage
      Doesn't matter if esWeapon not in equip slots }
    Damage: Single;
    Durability: Single;
    CanBeRepaired: Boolean;
    Indestructible: Boolean; //TODO: Temporary
    WardrobeMalfunctionChance: Single;
    Noise: Single;
    SpawnFrequency: Single;
    StartSpawningAtDepth: Integer;
    Bondage: Boolean;
    HasVinculopediaArticle: Boolean;
    UnequipDisintegrates: Boolean;
    DisintegratesInto: TCastleStringList; // TODO: direct references
    { if item disintegrates into something,
      then should those objects be reequipped back (true) or drop to the ground (false)? }
    DisintegrationReequip: Boolean;
    ProtectsFeet: Boolean; // TODO: Temporary?
    ProtectionTop, ProtectionBottom: Single;
    HandsUnavailable: Boolean; // TODO: Temporary?
    { If the item can be worn only by characters with ... legs }
    RequiresLegs: Boolean;
    { No need to currently: this is obvious from available equip slots, e.g. esWeapon/esShoulders/esAnkles;
      while esBottomUnder/esBottomOver don't technically specify if legs are split or not
      However this might change in future if some problems arise }
    //RequiresArms: Boolean;
    SoundAttack: String;
    SoundEquip: String;
    SoundUnequip: String;
    SoundBreak: String;
    SoundDisintegrate: String;
    SoundRepair: String;
    Enchantments: TEnchantmentsList;
    ColorTags: TColorTagsDictionary;
    function IsFootwear: Boolean;
    { variant of the previous one but tells if this item prevents from
      equipping footwear, i.e. restraints on the feet that make impossible
      to wear shoes }
    function IsFootwearOrMakesFootwearImpossible: Boolean;
    function IsWeapon: Boolean;
    function IsWeaponOrMakesWeaponImpossible: Boolean;
  public
    destructor Destroy; override;
  end;
  TItemsDataList = specialize TObjectList<TItemData>;
  TItemsDataDictionary = specialize TObjectDictionary<String, TItemData>;

implementation
uses
  TypInfo,
  CastleXMLUtils, CastleColors, CastleUriUtils,
  GameCachedImages, GameSerializableData;

procedure TItemData.Validate;
begin
  inherited;
  if MapImage = nil then
    raise EItemDataValidationError.Create('MapImage = nil : ' + Id);
  if MapImageDrawable = nil then
    raise EItemDataValidationError.Create('MapImageDrawable = nil : ' + Id);
  if NumberOfSlots <= 0 then
    raise EItemDataValidationError.Create('NumberOfSlots <= 0 : ' + Id);
  if EnchantmentPotential < 0 then
    raise EItemDataValidationError.Create('EnchantmentPotential < 0 : ' + Id);
  {if (Damage <= 0) and not IsBondage then
    raise EItemDataValidationError.Create('Damage <= 0 : ' + Id);}
  if Durability <= 1 then
    raise EItemDataValidationError.Create('Durability <= 1 : ' + Id);
  if SpawnFrequency < 0 then
    raise EItemDataValidationError.Create('SpawnFrequency < 0 : ' + Id);
  if (WardrobeMalfunctionChance < 0) or (WardrobeMalfunctionChance > 1) then
    raise EItemDataValidationError.Create('(WardrobeMalfunctionChance < 0) or (WardrobeMalfunctionChance > 1) : ' + Id);
  if IsWeapon and (SoundAttack = '') then // TODO: weapons as a different class?
    raise EItemDataValidationError.Create('Weapon has no attack sound : ' + Id);
  if SoundEquip = '' then
    raise EItemDataValidationError.Create('SoundEquip missing : ' + Id);
  if SoundUnequip = '' then
    raise EItemDataValidationError.Create('SoundUnequip missing : ' + Id);
  if SoundBreak = '' then
    raise EItemDataValidationError.Create('SoundBreak missing : ' + Id);
  if SoundDisintegrate = '' then
    raise EItemDataValidationError.Create('SoundDisintegrate missing : ' + Id);
  if SoundRepair = '' then
    raise EItemDataValidationError.Create('SoundRepair missing : ' + Id);
  if DisintegrationReequip and (DisintegratesInto.Count = 0) then
    raise EItemDataValidationError.Create('DisintegrationReequip and (DisintegratesInto.Count = 0) : ' + Id);
  {if not ProtectionTop and not ProtectionBottom and not (esWeapon in EquipSlots) then // currently item must absorb either top or bottom damage, otherwise it will never get destroyed / weapons are an exception / restraints are exception too... unfortunately this check is not valid
    raise EItemDataValidationError.Create('not ProtectsTop and not ProtectsBottom : ' + Id);}
  // TODO: validate that if reequip and disintegration fragments do not collide in their equipment slots (disintegration fragments should perfectly fit inside parent otherwise they'll break something else
end;

procedure TItemData.Read(const Element: TDOMElement);
var
  E: TApparelSlot;
  Iterator: TXMLElementIterator;
  Enchantment: TEnchantmentAbstract;
begin
  inherited;
  CoversTop := Element.AttributeBoolean('CoversTop');
  CoversBottom := Element.AttributeBoolean('CoversBottom');
  MapImage := LoadRgba(CombineURI(FBaseUrl, Element.AttributeString('MapImageUrl')));
  MapImageDrawable := LoadDrawable(CombineURI(FBaseUrl, Element.AttributeString('MapImageUrl')));
  Damage := Element.AttributeFloatDef('Damage', 0);
  Durability := Element.AttributeFloat('Durability');
  CanBeRepaired := Element.AttributeBooleanDef('CanBeRepaired', true);
  Indestructible := Element.AttributeBooleanDef('Indestructible', false);
  WardrobeMalfunctionChance := Element.AttributeFloatDef('WardrobeMalfunctionChance', 0);
  Noise := Element.AttributeFloat('Noise');
  SpawnFrequency := Element.AttributeFloatDef('SpawnFrequency', 1.0);
  StartSpawningAtDepth := Element.AttributeInteger('StartSpawningAtDepth');
  Bondage := Element.AttributeBooleanDef('Bondage', false);
  HasVinculopediaArticle := Element.AttributeBooleanDef('HasVinculopediaArticle', true);
  UnequipDisintegrates := Element.AttributeBooleanDef('UnequipDisintegrates', false);
  DisintegratesInto := CreateTokens(Element.AttributeStringDef('DisintegratesInto', ''), [',']);
  DisintegrationReequip := Element.AttributeBooleanDef('DisintegrationReequip', false);
  ProtectsFeet := Element.AttributeBooleanDef('ProtectsFeet', false);
  ProtectionTop := Element.AttributeSingle('ProtectionTop');
  ProtectionBottom := Element.AttributeSingle('ProtectionBottom');
  HandsUnavailable := Element.AttributeBooleanDef('HandsUnavailable', false);
  RequiresLegs := Element.AttributeBooleanDef('RequiresLegs', false);

  SoundAttack := Element.AttributeStringDef('SoundAttack', '');
  SoundEquip := Element.AttributeStringDef('SoundEquip', '');
  SoundUnequip := Element.AttributeStringDef('SoundUnequip', '');
  SoundBreak := Element.AttributeStringDef('SoundBreak', '');
  SoundDisintegrate := Element.AttributeStringDef('SoundDisintegrate', '');
  SoundRepair := Element.AttributeStringDef('SoundRepair', '');

  ColorTags := GetColorTags(Element);

  Enchantments := TEnchantmentsList.Create(true);
  Iterator := Element.ChildrenIterator('Enchantment');
  try
    while Iterator.GetNext do
    begin
      Enchantment := TEnchantmentAbstract.ReadClass(FBaseUrl, Iterator.Current) as TEnchantmentAbstract;
      Enchantments.Add(Enchantment);
    end;
  finally FreeAndNil(Iterator) end;

  NumberOfSlots := 0;
  for E in EquipSlots * GlobalEquipmentSlots do
    Inc(NumberOfSlots);

  // WIP: todo, balancing and make those manual
  EnchantmentPotential := Sqrt(NumberOfSlots);
  if esWeapon in EquipSlots then
    EnchantmentPotential += 1;
  if esTopOver in EquipSlots then
    EnchantmentPotential += 1;
  if esBottomOver in EquipSlots then
    EnchantmentPotential += 1;
end;

function TItemData.IsFootwear: Boolean;
begin
  Exit(ProtectsFeet and (esFeet in EquipSlots));
end;

function TItemData.IsFootwearOrMakesFootwearImpossible: Boolean;
begin
  Exit(esFeet in EquipSlots);
end;

function TItemData.IsWeapon: Boolean;
begin
  Exit((esWeapon in EquipSlots) and (Damage > 0));
end;

function TItemData.IsWeaponOrMakesWeaponImpossible: Boolean;
begin
  Exit(esWeapon in EquipSlots);
end;

destructor TItemData.Destroy;
begin
  FreeAndNil(ColorTags);
  FreeAndNil(Enchantments);
  FreeAndNil(DisintegratesInto);
  inherited Destroy;
end;

initialization
  RegisterSerializableData(TItemData);
end.


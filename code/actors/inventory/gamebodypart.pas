{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ A body part "item" that can be equipped in a body slot
  overall it's just a simple item, without much data or methods }
unit GameBodyPart;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Generics.Collections, DOM,
  GameSerializableObject,
  GameAbstractItem, GameItemDataAbstract;

type
  { Technically it's equal to TAbstractItem,
    the only difference is we check if we've gotten the valid TBodypartData
    and RegisterSerializableObject }
  TBodyPart = class(TAbstractItem)
  public
    //note: bodypart doesn't have color because it's inventory-global
    class function NewBodyPart(const AItemData: TItemDataAbstract): TBodyPart;
  end;
  TBodyPartsList = specialize TObjectList<TBodyPart>;

implementation
uses
  CastleXmlUtils, CastleColors,
  GameBodypartData;

class function TBodyPart.NewBodyPart(const AItemData: TItemDataAbstract): TBodyPart;
begin
  Result := TBodyPart.Create(true);
  if not (AItemData is TBodypartData) then
    raise Exception.CreateFmt('Got item data: %s, expected: TBodypartData in %s', [AItemData.ClassName, Self.ClassName]);
  Result.Data := AItemData;
end;

initialization
  RegisterSerializableObject(TBodyPart);
end.


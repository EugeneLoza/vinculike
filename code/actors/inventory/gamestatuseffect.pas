{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameStatusEffect;

{$INCLUDE compilerconfig.inc}

interface

uses
  Generics.Collections, DOM,
  GameSimpleSerializableObject,
  GameEnchantmentAbstract,
  GameColorTags;

type
  { Status effect is in practice extremely similar to Inventory Item:
    however in logic it contains only one enchantment (effect) and nothing more,
    will wear off after timeout }
  TStatusEffect = class(TSimpleSerializableObject)
  public
    Parent: TObject; // cannot typecast to Inventory
    Effect: TEnchantmentAbstract;
    Timeout: Single;
    procedure Update(const SecondsPassed: Single); //virtual - for now no children
    { Stops this status effect's internal stuff }
    procedure Stop;
    function Description: String;
    function Clone: TStatusEffect;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    //class function NewStatusEffect(const AItemData: TItemDataAbstract): TStatusEffect; // maybe
    destructor Destroy; override;
  end;
  TStatusEffectsList = specialize TObjectList<TStatusEffect>;

function LoadStatusEffectsList(const Element: TDomElement): TStatusEffectsList;
implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameInventory,
  GameLog;

function LoadStatusEffectsList(const Element: TDomElement): TStatusEffectsList;
var
  Iterator: TXMLElementIterator;
begin
  Result := TStatusEffectsList.Create(true);
  Iterator := Element.ChildrenIterator('StatusEffect');
  try
    while Iterator.GetNext do
      Result.Add(TStatusEffect.LoadClass(Iterator.Current) as TStatusEffect);
  finally FreeAndNil(Iterator) end;
end;

{$IFDEF SafeTypecast}
{$DEFINE ParentInventory:=(Parent as TInventory)}
{$ELSE}
{$DEFINE ParentInventory:=TInventory(Parent)}
{$ENDIF}

procedure TStatusEffect.Update(const SecondsPassed: Single);
begin
  Timeout -= SecondsPassed; // If timeout < 0 then Parent inventory will handle deletion of this item
  // we don't check timeout >=0 below, so that timeout can be 0, i.e. will call Update for one frame - maybe not a good idea
  if ParentInventory.EnchantmentRequirementsMet(Effect) then
    Effect.Update(ParentInventory.Parent, nil, SecondsPassed); // CAREFUL: Self is not TInventoryItem
end;

procedure TStatusEffect.Stop;
begin
  Parent := nil;
  Effect.Stop;
  //DebugLog('FINISHED: %s', [Description]);
end;

function TStatusEffect.Description: String;
begin
  Exit(Effect.Description + Format(' for %.1n seconds', [Timeout]));
end;

function TStatusEffect.Clone: TStatusEffect;
begin
  Result := TStatusEffect.Create;
  Result.Effect := Effect.Clone;
  Result.Timeout := Timeout;;
end;

procedure TStatusEffect.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Timeout', Timeout);
  Effect.Save(Element.CreateChild('Effect'));
end;

procedure TStatusEffect.Load(const Element: TDOMElement);
begin
  inherited;
  Timeout := Element.AttributeSingle('Timeout');
  Effect := TEnchantmentAbstract.ReadClass('', Element.ChildElement('Effect', true, true)) as TEnchantmentAbstract; // note: enchantments can't access base url (Data.FBaseUrl - doesn't seem like it'll help)? doesn't seem to affect anything, but need to keep an eye on it // what it _CAN_ break is enchantments that use assets, but all of those are not loadable
end;

destructor TStatusEffect.Destroy;
begin
  FreeAndNil(Effect);
  inherited Destroy;
end;

initialization
  RegisterSimpleSerializableObject(TStatusEffect);
end.


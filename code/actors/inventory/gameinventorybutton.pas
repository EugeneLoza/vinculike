{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameInventoryButton;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleControls, CastleMobileButton,
  {temporary}GameViewGame,{/temporary}
  GameInventoryItem, GameMapItem, GameApparelSlots;

type
  TInventoryButtonAbstract = class abstract(TCastleMobileButton)
  protected
    HorizontalGroup: TCastleHorizontalGroup;
    SlotsVerticalGroup: TCastleVerticalGroup;
    function CreateImageForSlot(const ASlot: TApparelSlot): TCastleImageControl;
    procedure SetupCore(const Slots: Integer);
  public
    procedure Setup; virtual; abstract;
  end;

  TEmptyInventoryButton = class(TInventoryButtonAbstract)
  public
    Slot: TApparelSlot;
    procedure Setup; override;
  end;

  TInventoryButton = class(TInventoryButtonAbstract)
  strict private
    CaptionLabel: TCastleLabel;
    CaptionImage: TCastleImageControl;
    DurabilityLabel: TCastleLabel;
    procedure SetItemColor;
    procedure SetupCaption;
  public
    Item: TInventoryItem;
    procedure Setup; override;
    procedure UpdateCaption;
  end;

  TMapItemButton = class(TInventoryButton)
  public
    MapItem: TMapItem;
    procedure Setup; override;
  end;

implementation
uses
  CastleUiControls, CastleVectors, CastleColors,
  GameFonts, GameColors, GameCachedImages, GameThemedButton,
  GameItemData, GameMath;

function TInventoryButtonAbstract.CreateImageForSlot(
  const ASlot: TApparelSlot): TCastleImageControl;
begin
  Result := TCastleImageControl.Create(SlotsVerticalGroup);
  Result.DrawableImage := SlotImage[ASlot];
  Result.Color := Vector4(1, 1, 1, 0.2);
  Result.OwnsDrawableImage := false;
end;

procedure TInventoryButtonAbstract.SetupCore(const Slots: Integer);
begin
  SetTheme('inventory_button_light');
  Width := 400; // WARNING, will/must be reset extrenally (todo?)
  Height := 20 + 40 * Slots;
  EnableParentDragging := true;
  HorizontalGroup := TCastleHorizontalGroup.Create(Self);
  HorizontalGroup.Anchor(vpMiddle, vpMiddle, 0);
  HorizontalGroup.Anchor(hpLeft, hpLeft, 10);
  HorizontalGroup.Spacing := 8;
  InsertFront(HorizontalGroup);
  SlotsVerticalGroup := TCastleVerticalGroup.Create(HorizontalGroup);
  SlotsVerticalGroup.Spacing := 8;
  HorizontalGroup.InsertFront(SlotsVerticalGroup);
end;

{=====================================================}

procedure TEmptyInventoryButton.Setup;
begin
  SetupCore(1);
  Caption := '';
  SlotsVerticalGroup.InsertFront(CreateImageForSlot(Slot));
  Enabled := false;
end;

procedure TInventoryButton.SetupCaption;
var
  EI: TApparelSlot;
begin
  for EI in Item.Data.EquipSlots do
    if EI in GlobalUiApparelSlotsSet then
      SlotsVerticalGroup.InsertBack(CreateImageForSlot(EI));
  CaptionLabel := TCastleLabel.Create(HorizontalGroup);
  CaptionLabel.Html := true;
  CaptionLabel.AutoSize := false;
  CaptionLabel.Height := 40;
  CaptionLabel.Alignment := hpMiddle;
  CaptionLabel.CustomFont := FontBender40;
  Captionlabel.FontScale := 0.85;
  CaptionImage := TCastleImageControl.Create(HorizontalGroup);
  CaptionImage.Stretch := true;
  CaptionImage.Height := 40;
  CaptionImage.Width := CaptionImage.Height;
  DurabilityLabel := TCastleLabel.Create(HorizontalGroup);
  DurabilityLabel.CustomFont := FontBender20;
  HorizontalGroup.InsertFront(CaptionLabel);
  HorizontalGroup.InsertFront(CaptionImage);
  HorizontalGroup.InsertFront(DurabilityLabel);
end;

procedure TInventoryButton.SetItemColor;
var
  ItemColor: TCastleColor;
begin
  if Item.Broken then
    ItemColor := ColorUiItemBroken
  else
  if Item.IsKnownBondage then
  begin
    if Item.HasStochasticEnchantments then
      ItemColor := ColorUiItemBondageStochastic
    else
      ItemColor := ColorUiItemBondage;
  end else
  if Item.HasStochasticEnchantments then
  begin
    if Item.Durability / Item.MaxDurability > 1 / 3.0 then
      ItemColor := ColorUiItemStochastic
    else
    if (Item.Durability > ItemDisintegrationThreshold) or (not Item.ItemData.CanBeRepaired) then
      ItemColor := ColorUiItemStochasticDamaged
    else
      ItemColor := ColorUiItemStochasticDamagedCanDisintegrate;
  end else
  if Item.Durability / Item.MaxDurability > 1 / 3.0 then
  begin
    if (Item.Durability > ItemDisintegrationThreshold) or (not Item.ItemData.CanBeRepaired) then
      ItemColor := ColorUiItemNormal
    else
      ItemColor := ColorUiItemNormalCanDisintegrate
  end else
  begin
    if (Item.Durability > ItemDisintegrationThreshold) or (not Item.ItemData.CanBeRepaired) then
      ItemColor := ColorUiItemDamaged
    else
      ItemColor := ColorUiItemDamagedCanDisintegrate;
  end;
  CaptionLabel.Color := ItemColor;
  if TCastleColor.Equals(ItemColor, ColorUiItemNormal) then
    CaptionImage.Color := CastleColors.White // hack, but for now acceptable
  else
  if TCastleColor.Equals(ItemColor, ColorUiItemNormalCanDisintegrate) then
    CaptionImage.Color := CastleColors.Silver // hack, but for now acceptable
  else
    CaptionImage.Color := ItemColor;
  DurabilityLabel.Color := ItemColor;
end;

procedure TInventoryButton.UpdateCaption;
var
  ItemName: String;
begin
  SetItemColor;
  if ViewGame.FullInventory then
  begin
    CaptionLabel.Exists := true;
    CaptionLabel.Width := EffectiveWidth - 40 - 40; // TODO: move into onresize?
    CaptionImage.Exists := false;
    DurabilityLabel.Exists := false;
    ItemName := Item.Data.DisplayName;
    if Item.HasStochasticEnchantments then
      ItemName := '*' + ItemName + '*';
    if Item.ItemData.Indestructible then
      CaptionLabel.Caption := ItemName
    else
      CaptionLabel.Caption := Format('%s %d/%d', [ItemName, Item.ClampedDurability, Item.ClampedMaxDurability]);
  end else
  begin
    CaptionLabel.Exists := false;
    CaptionImage.Exists := true;
    CaptionImage.Image := Item.ItemData.MapImage;
    CaptionImage.OwnsImage := false; // ugh, this is not good here, but if we ask it at creation - we don't free default image which gets automatically created
    if Item.ItemData.Indestructible then
      DurabilityLabel.Exists := false
    else
      DurabilityLabel.Caption := Item.ClampedDurability.ToString;
  end;
end;

procedure TInventoryButton.Setup;
begin
  SetupCore(Item.ItemData.NumberOfSlots);
  SetupCaption;
  UpdateCaption;
end;

procedure TMapItemButton.Setup;
begin
  Item := MapItem.Item;
  inherited;
end;

end.


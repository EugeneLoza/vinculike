{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameLeash;

{$INCLUDE compilerconfig.inc}

interface

uses
  Generics.Collections, DOM,
  GameInventoryItem, GameApparelSlots, GameItemDataAbstract;

type
  {}
  TLeash = class(TInventoryItem)
  private const
    {How large can a leash drag be per frame }
    MinLeashDelta = 0.1;
  strict private
    FLeashHolder: TObject;
    LeashHolderId: UInt64;
    procedure SetLeashHolder(AValue: TObject);
  public
    //AttachedTo: TApparelSlot; // usually esCollar but not always
    LeashLength: Single;
    property LeashHolder: TObject read FLeashHolder write SetLeashHolder; // cannot typecast; warning: can be nil = not held
    procedure Update(const SecondsPassed: Single); override;
    { Stops this item's internal stuff }
    procedure Stop; override;
    procedure ReleaseLeash;
    procedure MaybeReleaseLeashByDistance;
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    class function NewLeash(const AItemData: TItemDataAbstract): TLeash; // no idea why TInventoryItem(ClassType).Create doesn't work
    procedure AfterDeserealization; override;
    //class function NewLeash(const AItemData: TItemDataAbstract): TLeash;
  end;
  TLeashsList = specialize TObjectList<TLeash>;

implementation
uses
  SysUtils,
  CastleXmlUtils, CastleColors,
  GameSerializableObject,
  GameEnchantmentAbstract, GameItemData, GameMath,
  GameInventory, GameActor, GamePlayerCharacter,
  GameActionRoll, GameDifficultyLevel,
  GameRandom, GameLog, GameColors;

{$IFDEF SafeTypecast}
{$DEFINE ParentInventory:=(Parent as TInventory)}
{$DEFINE ParentActor:=((Parent as TInventory).Parent as TActor)}
{$DEFINE ParentPlayer:=((Parent as TInventory).Parent as TPlayerCharacter)}
{$DEFINE HolderActor:=(LeashHolder as TActor)}
{$ELSE}
{$DEFINE ParentInventory:=TInventory(Parent)}
{$DEFINE ParentActor:=TActor(TInventory(Parent).Parent)}
{$DEFINE ParentPlayer:=TPlayerCharacter(TInventory(Parent).Parent)}
{$DEFINE HolderActor:=TActor(LeashHolder)}
{$ENDIF}

procedure TLeash.ReleaseLeash;
begin
  if LeashHolder <> nil then
  begin
    //HolderActor.ForceResetToIdle; // no need for now?
    LeashHolder := nil;
  end;
end;

procedure TLeash.SetLeashHolder(AValue: TObject);
begin
  if FLeashHolder <> AValue then
  begin
    FLeashHolder := AValue;
    if Parent <> nil then // may happen if leash just got broken
      ParentInventory.InvalidateInventory; // we need to rebuild enchantments cache if leash owner has changed
    // we need to stop any action, because it can be move with pathfinding,
    // and move with pathfinding doesn't work with MoveMeToDelta trying to break it
    if (FLeashHolder <> nil) and (not ParentActor.Unsuspecting) and ParentActor.CurrentAction.CanStop then
    begin
      ParentActor.ResetToIdle;
    end;
  end;
end;

procedure TLeash.Update(const SecondsPassed: Single);
var
  DX, DY, DistSqr, OldDistSqr: Single;
  DistDiff, MaxLeashLength: Single;
  MoveDiff, StaminaExpense: Single;
begin
  inherited;
  if ParentInventory.Apparel[esNeck] = nil then
  begin
    ShowLog('%s is no longer attached to anything', [Data.DisplayName], ColorLogBondageItemSave);
    ParentInventory.DisintegrateItem(esLeash);
    Exit;
  end;

  if LeashHolder = nil then
    Exit; // nobody is holding the leash

  if not HolderActor.CanAct then // monster died
  begin
    ShowLog('%s angrily pulls the leash end free', [ParentActor.Data.DisplayName], ColorLogBondageItemSave);
    ReleaseLeash;
    Exit;
  end;

  //todo: use pathfinding DistSqrance
  DX := HolderActor.CenterX - ParentActor.CenterX;
  DY := HolderActor.CenterY - ParentActor.CenterY;
  DistSqr := Sqr(DX) + Sqr(DY);
  if DistSqr > Sqr(LeashLength) then
  begin
    DistDiff := 3 * (Sqrt(DistSqr) - LeashLength) * SecondsPassed;
    MaxLeashLength := MaxSingle(Sqrt(DistSqr) - DistDiff, LeashLength);
    OldDistSqr := Single.MaxValue;
    while (OldDistSqr > DistSqr) and (DistSqr > Sqr(MaxLeashLength)) do
    begin
      MoveDiff := MinSingle(Sqrt(DistSqr) - LeashLength, MinLeashDelta);

      StaminaExpense := MoveDiff;
      // unaffected by any multipliers, otherwise than difficulty level
      if ParentActor.CurrentAction is TActionRoll then
      begin
        // note that Player has already spent stamina on dash roll
        // note 2: stealth attack is free, but also is TActionRoll
        Durability -= 3 * StaminaExpense / Difficulty.BondageStruggleStaminaToProgressRatio
      end else
      begin
        Durability -= 1 * StaminaExpense / Difficulty.BondageStruggleStaminaToProgressRatio
      end;
      ParentPlayer.DegradeStamina(StaminaExpense);
      if Durability < 0 then
      begin
        ShowLog('%s snaps letting %s free', [Data.DisplayName, ParentActor.Data.DisplayName], ColorLogBondageItemSave);
        ParentInventory.DisintegrateItem(esLeash);
        Exit;
      end;

      ParentActor.MoveMeToDelta(MoveDiff, DX / Sqrt(DistSqr), DY /  Sqrt(DistSqr));
      // This may conflict if HolderActor.CurrentAction contained pathfinding (and if player tries to run away - it often does)
      {if HolderActor.Data.MaxHealth < 50 then // tempoary: something smarter here
        HolderActor.MoveMeToDelta(Min(Sqrt(DistSqr) - LeashLength, MinLeashDelta), -DX /  Sqrt(DistSqr), -DY /  Sqrt(DistSqr));}
      OldDistSqr := DistSqr;
      DX := HolderActor.CenterX - ParentActor.CenterX;
      DY := HolderActor.CenterY - ParentActor.CenterY;
      DistSqr := Sqr(DX) + Sqr(DY);
    end;
  end;
end;

procedure TLeash.Stop;
begin
  inherited;
  ReleaseLeash;
end;

procedure TLeash.MaybeReleaseLeashByDistance;
begin
  if LeashHolder <> nil then
    if Sqr(HolderActor.CenterX - ParentActor.CenterX) + Sqr(HolderActor.CenterY - ParentActor.CenterY) > 2 * Sqr(LeashLength) then // 1.41 times exceed the leash length
    begin
      ShowLog('%s lets free the leash holding %s', [HolderActor.Data.DisplayName, ParentActor.Data.DisplayName], ColorLogBondageItemSave);
      ReleaseLeash;
    end;
end;

procedure TLeash.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  if HolderActor <> nil then
    Element.AttributeSet('LeashHolderId', HolderActor.ReferenceId)
  else
    Element.AttributeSet('LeashHolderId', 0);
  Element.AttributeSet('LeashLength', LeashLength);
end;

procedure TLeash.Load(const Element: TDOMElement);
begin
  inherited;
  LeashHolderId := Element.AttributeQWord('LeashHolderId');
  LeashLength := Element.AttributeSingle('LeashLength');
end;

procedure TLeash.AfterDeserealization;
begin
  inherited AfterDeserealization;
  if LeashHolderId > 0 then
    LeashHolder := ObjectByReferenceId(LeashHolderId)
  else
    LeashHolder := nil; // paranoid
end;

class function TLeash.NewLeash(const AItemData: TItemDataAbstract): TLeash;
var
  E: TEnchantmentAbstract;
  ColorTag: String;
begin
  Result := TLeash.Create(true);
  if not (AItemData is TItemData) then
    raise Exception.CreateFmt('Got item data: %s, expected: TItemData in %s', [AItemData.ClassName, Self.ClassName]);
  Result.Data := AItemData;
  Result.MaxDurability := 1 + Sqrt(Rnd.Random) * (Result.ItemData.Durability - 1);
  Result.Durability := 1 + Sqrt(Rnd.Random) * (Result.MaxDurability - 1);

  Result.Enchantments := TEnchantmentsList.Create(true);
  for E in Result.ItemData.Enchantments do
    Result.Enchantments.Add(E.Clone);

  if Result.ItemData.ColorTags <> nil then
    for ColorTag in Result.ItemData.ColorTags.Keys do
      Result.Color.Add(ColorTag, Result.ItemData.ColorTags[ColorTag][Rnd.Random(Result.ItemData.ColorTags[ColorTag].Count)]);
end;

initialization
  RegisterSerializableObject(TLeash);
end.


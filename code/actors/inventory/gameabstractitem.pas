{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Base for all items that may be interacted with by the Player
  (equipped, consumed, etc.)
  note that under the hood bodyparts are also just items
  equipped in the inventory and handled in the same way as clothes }
unit GameAbstractItem;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameSerializableObject,
  GameItemDataAbstract;

type
  { most basic entity that can be equipped and/or used by the Player}
  TAbstractItem = class abstract(TSerializableObject)
  public
    Data: TItemDataAbstract;
  public const Signature = 'item'; // deprecated
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  end;

implementation
uses
  CastleXmlUtils,
  GameItemsDatabase;

procedure TAbstractItem.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Data.Id', Data.Id);
end;

procedure TAbstractItem.Load(const Element: TDOMElement);
begin
  inherited;
  Data := ItemsDataDictionary[Element.AttributeString('Data.Id')];
end;

end.


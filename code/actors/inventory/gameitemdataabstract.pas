{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameItemDataAbstract;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Generics.Collections, DOM,
  CastleImages, CastleColors, CastleShiftedImage,
  GameApparelSlots, GameSerializableData;

type
  EItemDataValidationError = class(Exception);

type
  TShapePoseName = String;

type
  TApparelImage = class(TObject)
  public
    Image: TCastleShiftedImage;
    Sprite: TCastleShiftedImage;
    UseAverageColor: Boolean;
    AverageColor: TCastleColor;
    ColorTag: String;
    procedure Validate;
    constructor Create(const BaseUrl: String; const Element: TDOMElement);
  end;
  TApparelImagesDictionary = specialize TObjectDictionary<TApparelSlot, TApparelImage>;
  TShapePoseImagesDictionary = specialize TObjectDictionary<TShapePoseName, TApparelImagesDictionary>;

type
  TItemDataAbstract = class abstract(TSerializableData)
  strict private
    FMainSlot: TApparelSlot;
  protected
    procedure Validate; override;
    procedure ReadImage(const Element: TDOMElement);
    procedure Read(const Element: TDOMElement); override;
  public
    Id: String;
    DisplayName: String;
    EquipSlots: TApparelSlotsSet;
    HidesSlots: TApparelSlotsSet;
    Images: TShapePoseImagesDictionary;
    property MainSlot: TApparelSlot read FMainSlot;
  public
    destructor Destroy; override;
  end;
  TItemsDataAbstractList = specialize TObjectList<TItemDataAbstract>;
  TItemsDataAbstractDictionary = specialize TObjectDictionary<String, TItemDataAbstract>;

implementation
uses
  CastleXMLUtils, CastleStringUtils, CastleUriUtils,
  GameLog, GameEnumUtils,
  GameCachedImages, GameAverageColorCalculator;

{ TApparelImage =============================================================== }

procedure TApparelImage.Validate;
begin
  if Image = nil then
    raise EItemDataValidationError.CreateFmt('Image = nil : [%s]', [ClassName]);
  // sprite can be nil
end;

constructor TApparelImage.Create(const BaseUrl: String;
  const Element: TDOMElement);
var
  SpriteUrl: String;
begin
  Image := LoadShiftedImage(CombineURI(BaseUrl, Element.AttributeString('ImageUrl')));

  SpriteUrl := Element.AttributeString('SpriteUrl');
  if SpriteUrl <> '' then
    Sprite := LoadShiftedImage(CombineURI(BaseUrl, SpriteUrl));

  ColorTag := Element.AttributeStringDef('ColorTag', 'none');

  if Element.HasAttribute('AverageColor') then
  begin
    UseAverageColor := true;
    AverageColor := Element.AttributeColor('AverageColor');
  end else
  if ColorTag = 'none' then
  begin
    UseAverageColor := true;
    AverageColor := (Image.Image as TRGBAlphaImage).CalculateAverageColor;
  end else
    UseAverageColor := false;
end;

{ TItemDataAbstract =========================================================== }

procedure TItemDataAbstract.Validate;
var
  S: String;
  ApparelImagesDictionary: TApparelImagesDictionary;
  E: TApparelSlot;

  // otherwise Error: Identifier not found "specialize", see https://gitlab.com/freepascal.org/fpc/source/-/issues/40890
  function ApparelSlotToStrWorkaround(const Value: TApparelSlot): String; inline;
  begin
    Exit(specialize EnumToStr<TApparelSlot>(E));
  end;

begin
  //inherited; - parent is abstract
  if Id = '' then
    raise EItemDataValidationError.CreateFmt('Id = "" in %s', [ClassName]);
  if DisplayName = '' then
    raise EItemDataValidationError.CreateFmt('DisplayName = "" : [%s:%s]', [Id, ClassName]);
  if EquipSlots = [] then
    raise EItemDataValidationError.CreateFmt('EquipSlots = [] : [%s:%s]', [Id, ClassName]);
  if EquipSlots * HidesSlots <> [] then
    raise EItemDataValidationError.CreateFmt('EquipSlots * HidesSlots <> [] : [%s:%s]', [Id, ClassName]);

  if Images.Count = 0 then
    raise EItemDataValidationError.CreateFmt('Images.Count = 0 : [%s:%s]', [Id, ClassName]);
  for S in Images.Keys do
  begin
    ApparelImagesDictionary := Images[S];
    for E in ApparelImagesDictionary.Keys do
    begin
      if not (E in EquipSlots) then
        raise EItemDataValidationError.CreateFmt('ShapePose %s : RenderSlot %s not in EquipSlots : [%s:%s]', [S, specialize EnumToStr<TApparelSlot>(E), Id, ClassName]);
      try
        ApparelImagesDictionary[E].Validate;
      except
        raise EItemDataValidationError.CreateFmt('ShapePose %s : RenderSlot[%s].Validate failed : [%s:%s]', [S, ApparelSlotToStrWorkaround(E), Id, ClassName]);
      end;
    end;
  end;
end;

procedure TItemDataAbstract.ReadImage(const Element: TDOMElement);
var
  ShapePose: String;
  RenderSlot: TApparelSlot;
  ShapePoseImage: TApparelImagesDictionary;
  ApparelImage: TApparelImage;
begin
  ShapePose := Element.AttributeString('ShapePose');
  if Images.ContainsKey(ShapePose) then
    ShapePoseImage := Images[ShapePose]
  else
  begin
    ShapePoseImage := TApparelImagesDictionary.Create([doOwnsValues]);
    Images.Add(ShapePose, ShapePoseImage);
  end;

  RenderSlot := specialize StrToEnum<TApparelSlot>(Element.AttributeString('Slot'));
  ApparelImage := TApparelImage.Create(FBaseUrl, Element);
  if ShapePoseImage.ContainsKey(RenderSlot) then
  begin
    LogWarning('Overwriting Render Slot');
    ShapePoseImage.Remove(RenderSlot); // will also free
  end;
  ShapePoseImage.Add(RenderSlot, ApparelImage);
end;

procedure TItemDataAbstract.Read(const Element: TDOMElement);
var
  E: TApparelSlot;
  Iterator: TXMLElementIterator;
begin
  //inherited -- Parent is abstract
  Id := Element.AttributeString('Id');
  DisplayName := Element.AttributeString('DisplayName');

  EquipSlots := ApparelSlotsStringToSlotsSet(Element.AttributeStringDef('EquipSlot', '')); // TODO: Not "def"?

  HidesSlots := ApparelSlotsStringToSlotsSet(Element.AttributeStringDef('HidesSlots', ''));

  Images := TShapePoseImagesDictionary.Create([doOwnsValues]);
  Iterator := Element.ChildrenIterator('Image');
  try
    while Iterator.GetNext do
      ReadImage(Iterator.Current);
  finally FreeAndNil(Iterator) end;

  for E in EquipSlots do
  begin
    FMainSlot := E;
    if FMainSlot in GlobalUiApparelSlotsSet then
      break;
  end;
end;

destructor TItemDataAbstract.Destroy;
begin
  FreeAndNil(Images);
  inherited Destroy;
end;

end.


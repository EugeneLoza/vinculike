{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Apparel slots are slots where items (or bodyparts) can be equipped
  each item has multiple apparel slots (with important MainSlot for the game logic )
  item can have multiple images each assigned to a single apparel slot
  apparel slots are sorted strictly in render order, the upper one
  from lowest trnder order down to lowest one as the topmost render order
  Some apparel slots are "exposed" in the UI for Player interactions
  Equipment slots - slots that correspond to equipment which is usually
  Player-interactions, are displayed in the UI (some slots may be hidden
  if no item is equipped in this slot, usually meaning those are slots
  for restraints only (like collar, leash, gag))
  Body slots - slots for "equipping" bodyparts. While Player interacts with
  those in a different way, practically they work in the same way
  Render slots - additional slots that may be used by items and bodyparts
  but are not user-interactable; note that they still follow all the logic
  of a regular apparel slot, so it's a good idea not to go wild with those
  and make sure they are "paired" with a valid body or equipment SlotStringList
  (e.g. equipment slot must make sure no other item that doesn't have this
  equipment slot will not use the same render slot, resulting in conflict
  equipping this item will unequip that item (as slots conflict) confusing Player)
  Virtual slots - similar to above, can be used to host an image, but their primary role
  is to allow blueprints to specify body features that affect game logic
  (e.g. whether the character has legs or is a naga? e.g. naga can equip some kinds
  of underwear but not all of them)}
unit GameApparelSlots;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, Generics.Collections;

type
  { Possible apparel (body/equipment) slots of the item. Prefixes:
    asXxxx - apparel slots (generic)
    bsXxxx - body slots
    esXxxx - equipment slots (player/monsters can interact with those)
    vsXxxx - virtual slot (equipment slot that is not shown in the inventory but can be used for some logic or to attach images for items that need to be more fragmented)
    NOTE: this order = render order }

  TApparelSlot = (

    //asNone,

    bsTail,
    rsTailTip,

    rsHairBack,
    rsBackOverOver,
    rsBackOver,

    bsTorsoUnder,
    bsLegs,
    bsLegsClaws,
    rsTorsoOver,
    vsArms,
    vsLegsSplit,
    bsNavel,
    bsBreasts,
    bsNipples,
    rsNethersFlesh,
    bsNethers,
    bsPubic,
    bsBodyTattoo,

    esBottomUnder,
    esTopUnder,
    esBottomOver,
    esTopOver,

    bsHands,
    bsNails,

    esTopOverOver,
    esFeet,
    esAmulet,

    esNeck, // TODO: change to esCollar
    bsHead,
    bsHair, // After "head shape" before "ears"
    bsMouth,
    bsNose,
    bsEars,

    bsEyes,
    bsEyebrows,
    esGag,
    esGlasses,

    rsHairFront, // After eyes, glasses, ears

    esWeapon,
    esLeash
  );

const
  { Each blueprint may have less than these, but not more
    note: these are rather UI related, not logic,
    see AllEquipmentSlots for a specific blueprint}
  { Equipment Slots considered for top half of the body }
  GlobalTopSlots = [esTopOverOver, esTopOver, esTopUnder, esAmulet, esNeck, esGlasses, esGag, esLeash];
  { Equipment Slots considered for top half of the body }
  GlobalBottomSlots = [esFeet, esBottomUnder, esBottomOver];
  { All equipment slots }
  GlobalEquipmentSlots = GlobalTopSlots + GlobalBottomSlots + [esWeapon];

type
  TApparelSlotsSet = Set of TApparelSlot;
  TApparelSlotsList = specialize TList<TApparelSlot>;

const
  { layers that will be hidden in censored mode }
  CensoredLayers : TApparelSlotsSet = [bsNipples, rsNethersFlesh, bsNethers, bsPubic];

type
  { A few useful extensions to
    Note, those may be more useful for other usercases
    so making a child of TList is better fir the future, TODO }
  TApparelSlotsListHelper = class helper for TApparelSlotsList
  public
    function Clone: TApparelSlotsList;
    function Random: TApparelSlot; // TODO: make a generic "get random element"
  end;

type
  EApparelSlotConversionError = class(Exception);

var
  { Apparel slots used in the UI }
  GlobalUiApparelSlotsList: TApparelSlotsList;
  GlobalUiApparelSlotsSet: TApparelSlotsSet;

{ Convert between strings and apparel slots }
function ApparelSlotsStringToSlotsSet(const AString: String): TApparelSlotsSet;

{ Display apparel slots as human readable strings }
function EquipSlotsToHumanReadableString(const AEquipSlots: TApparelSlotsSet): String;
function EquipmentSlotToHumanReadableString(const AEquipSlot: TApparelSlot): String;
function BodySlotToHumanReadableString(const AEquipSlot: TApparelSlot): String;

{ Init caches of apparel slots}
procedure InitApparelSlots;
{ Free Apparel slots caches }
procedure FreeApparelSlots;
implementation
uses
  TypInfo,
  CastleStringUtils,
  GameRandom, GameEnumUtils;

function ApparelSlotsStringToSlotsSet(const AString: String): TApparelSlotsSet; // unfortunately cannot be made generic due to generic function not allowing result of "set of T" type
var
  SlotStringList: TCastleStringList;
  S: String;
begin
  SlotStringList := CreateTokens(AString, [',']);
  Result := [];
  for S in SlotStringList do
    Result := Result + [specialize StrToEnum<TApparelSlot>(S)];
  FreeAndNil(SlotStringList);
  //Exit(specialize TStrToEnumsSet<TApparelSlot>.StrToEnumsSet(AString));
end;

procedure InitApparelSlots;
var
  E: TApparelSlot;
begin
  GlobalUiApparelSlotsList := TApparelSlotsList.Create;
  { Note to self: If I'll be as sleepy and dizzy again, the list below is
    filled in manually on purpose, it's the order in which slots appear
    in the UI and hence you cannot just automatically fill them in by
    render order.
  for E in GlobalUiApparelSlotsSet do
    GlobalUiApparelSlotsList.Add(E);}

  GlobalUiApparelSlotsList.Add(esFeet);
  GlobalUiApparelSlotsList.Add(esBottomOver);
  GlobalUiApparelSlotsList.Add(esBottomUnder);
  GlobalUiApparelSlotsList.Add(esTopUnder);
  GlobalUiApparelSlotsList.Add(esTopOver);
  GlobalUiApparelSlotsList.Add(esTopOverOver);
  GlobalUiApparelSlotsList.Add(esAmulet);
  GlobalUiApparelSlotsList.Add(esLeash);
  GlobalUiApparelSlotsList.Add(esGag);
  GlobalUiApparelSlotsList.Add(esNeck);
  GlobalUiApparelSlotsList.Add(esGlasses);
  GlobalUiApparelSlotsList.Add(esWeapon);

  GlobalUiApparelSlotsSet := [];
  for E in GlobalUiApparelSlotsList do
    GlobalUiApparelSlotsSet += [E];
end;

procedure FreeApparelSlots;
begin
  FreeAndNil(GlobalUiApparelSlotsList);
end;

function TApparelSlotsListHelper.Clone: TApparelSlotsList;
var
  I: Integer;
begin
  Result := TApparelSlotsList.Create;
  Result.Capacity := Count;
  for I := 0 to Pred(Count) do
    Result.Add(Self[I]);
end;

function TApparelSlotsListHelper.Random: TApparelSlot;
begin
  Exit(Self[Rnd.Random(Count)]);
end;

function EquipSlotsToHumanReadableString(const AEquipSlots: TApparelSlotsSet): String;
var
  WSlots: Integer;
  TSlots: Integer;
  BSlots: Integer;
  E: TApparelSlot;
begin
  WSlots := 0;
  TSlots := 0;
  BSlots := 0;
  for E in AEquipSlots do
    if E = esWeapon then
      Inc(WSlots)
    else
    if E in GlobalTopSlots then
      Inc(TSlots)
    else
    if E in GlobalBottomSlots then
      Inc(BSlots);
    // can be else (a  render slot), just skip it

  if WSlots + TSlots + BSlots = 1 then
    for E in AEquipSlots * GlobalEquipmentSlots do // TODO: something smarter here
      Result := EquipmentSlotToHumanReadableString(E) // this is really weird looking cat, TODO: fix properly
  else
  if WSlots + TSlots = 0 then
    Result := 'lower body'
  else
  if WSlots + BSlots = 0 then
    Result := 'upper body'
  else
    Result := 'body';
end;

function EquipmentSlotToHumanReadableString(const AEquipSlot: TApparelSlot): String;
begin
  case AEquipSlot of
    esWeapon: Result := 'hands';
    esTopOver: Result := 'arms and chest';
    esTopOverOver: Result := 'shoulders';
    esTopUnder: Result := 'breasts';
    esBottomOver: Result := 'legs';
    esBottomUnder: Result := 'waist and groin';
    esNeck, esAmulet, esLeash: Result := 'neck';
    esGag: Result := 'mouth';
    esFeet: Result := 'feet';
    esGlasses: Result := 'nose';
    else
      raise EApparelSlotConversionError.Create('Cannot convert EquipSlot to human readable string: ' + specialize EnumToStr<TApparelSlot>(AEquipSlot));
  end;
end;

function BodySlotToHumanReadableString(const AEquipSlot: TApparelSlot): String;
begin
  case AEquipSlot of
    bsTail: Result := 'Tail';
    bsTorsoUnder: Result := 'Torso';
    bsLegs: Result := 'Legs';
    bsLegsClaws: Result := 'Claws';
    bsNavel: Result := 'Belly Button';
    bsBreasts: Result := 'Breasts';
    bsNipples: Result := 'Nipples';
    bsNethers: Result := 'Nethers';
    bsPubic: Result := 'Pubic';
    bsBodyTattoo: Result := 'Body Tattoo';
    bsHead: Result := 'Head';
    bsEyes: Result := 'Eyes';
    bsEyebrows: Result := 'Eyebrows';
    bsMouth: Result := 'Mouth';
    bsNose: Result := 'Nose';
    bsHair: Result := 'Hair';
    bsEars: Result := 'Ears';
    bsHands: Result := 'Arms';
    bsNails: Result := 'Nails';
    else
      raise EApparelSlotConversionError.Create('Cannot convert EquipSlot to human readable string: ' + specialize EnumToStr<TApparelSlot>(AEquipSlot));
  end;
end;

end.


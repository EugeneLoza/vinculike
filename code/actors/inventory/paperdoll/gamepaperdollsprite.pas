{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GamePaperDollSprite;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  CastleGlImages, CastleImages,
  GameApparelSlots;

type
  TPaperDollSprite = class(TObject)
  strict private
    BlankImage: TRgbAlphaImage;
    procedure SetSize(const AWidth, AHeight: Integer); // unfortunately we can't move it to abstract parent, because TPaperDoll is a child of TCastleUserInterface
  public
    Image: TDrawableImage;
    Parent: TObject;
    procedure UpdateToInventory;
    constructor Create; //override
    destructor Destroy; override;
  end;

implementation
uses
  CastleConfig, CastleVectors, CastleColors,
  GamePlayerCharacter, GameLog,
  GameViewGame, GameCachedImages, GameItemDataAbstract, GameViewAbstract;

{$IFDEF SafeTypecast}
{$DEFINE ParentInventory:=(Parent as TPlayerCharacter).Inventory}
{$ELSE}
{$DEFINE ParentInventory:=TPlayerCharacter(Parent).Inventory}
{$ENDIF}

procedure TPaperDollSprite.SetSize(const AWidth, AHeight: Integer);
begin
  if (BlankImage.Width <> AWidth) or (BlankImage.Height <> AHeight) then
  begin
    BlankImage.SetSize(AWidth, AHeight);
    BlankImage.Clear(Vector4Byte(0, 0, 0, 0));
  end;
end;

procedure TPaperDollSprite.UpdateToInventory;
var
  E: TApparelSlot;
  Hidden: TApparelSlotsSet;
  AImage: TApparelImage;
  ApparelImagesDictionary: TApparelImagesDictionary;
begin
  //SetSize(99, 99); - sprite size always stays the same
  Image.Load(BlankImage);
  Image.Alpha := acBlending; // autodetect fails on image with no features, force alpha blending mode

  Hidden := ParentInventory.GetHiddenSlots(true);

  Image.RenderToImageBegin;

  if Parent = ViewGame.CurrentCharacter then
    SelectedCharacterHighlight.DrawUnscaled;

  // TODO: maybe optimize (ParentInventory.Apparel[E] <> nil) and (ParentInventory.Apparel[E].Data.RenderSlots.ContainsKey(E)) -> Data.RenderSlotsSet
  for E in TApparelSlot do
    if not (E in Hidden) and (ParentInventory.Apparel[E] <> nil) then
    begin
      ApparelImagesDictionary := ParentInventory.Apparel[E].Data.Images[ViewGame.CurrentCharacter.GetShapePose];
      if ApparelImagesDictionary.ContainsKey(E) then
      begin
        AImage := ApparelImagesDictionary[E];
        {$WARNING Not ViewGame.CurrentCharacter.GetShapePose but ParentPlayer.GetShapePose OR move GetShapePose to Inventory - which also makes sense}
        if AImage.Sprite <> nil then
        begin
          if AImage.UseAverageColor then
            AImage.Sprite.Color := AImage.AverageColor
          else
            AImage.Sprite.Color := ParentInventory.ColorForSlot[E];
          AImage.Sprite.DrawUnscaled;
        end;
      end;
    end;

  Image.RenderToImageEnd;
end;

constructor TPaperDollSprite.Create;
begin
  BlankImage := TRgbAlphaImage.Create;
  SetSize(99, 99);
  Image := TDrawableImage.Create(BlankImage, true, false);
end;

destructor TPaperDollSprite.Destroy;
begin
  FreeAndNil(Image);
  FreeAndNil(BlankImage);
  inherited Destroy;
end;

end.


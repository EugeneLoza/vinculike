{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GamePaperDoll;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleUiControls, CastleGlImages, CastleImages,
  GameApparelSlots;

type
  TPaperDoll = class(TCastleUserInterface)
  strict private
    RenderImage: TDrawableImage;
    BlankImage: TRgbAlphaImage;
    procedure SetSize(const AWidth, AHeight: Integer); // unfortunately we can't move it to abstract parent, because TPaperDoll is a child of TCastleUserInterface
  public
    Nude: Boolean;
    procedure UpdateToInventory;
    procedure Render; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleConfig, CastleRectangles, CastleVectors, CastleColors,
  GameViewGame, GameItemDataAbstract, GameLog, GameViewAbstract,
  GameAbstractItem;

procedure TPaperDoll.SetSize(const AWidth, AHeight: Integer);
begin
  if (BlankImage.Width <> AWidth) or (BlankImage.Height <> AHeight) then
  begin
    BlankImage.SetSize(AWidth, AHeight);
    BlankImage.Clear(Vector4Byte(0, 0, 0, 0));
  end;
end;

procedure TPaperDoll.UpdateToInventory;
var
  E: TApparelSlot;
  Hidden: TApparelSlotsSet;
  AImage: TApparelImage;
  ShapePose: String;
  ItemTorsoUnder: TAbstractItem;
  ApparelImagesDictionary: TApparelImagesDictionary;
begin
  ShapePose := ViewGame.CurrentCharacter.GetShapePose;
  ItemTorsoUnder := ViewGame.CurrentCharacter.Inventory.Apparel[bsTorsoUnder];
  ApparelImagesDictionary := ItemTorsoUnder.Data.Images[ShapePose];
  SetSize(
    ApparelImagesDictionary[bsTorsoUnder].Image.Width, // TODO: More generic
    ApparelImagesDictionary[bsTorsoUnder].Image.Height
  );

  RenderImage.Load(BlankImage);
  RenderImage.Alpha := acBlending; // autodetect fails on image with no features, force alpha blending mode

  Hidden := ViewGame.CurrentCharacter.Inventory.GetHiddenSlots(not Nude);
  if Nude then
    Hidden += ViewGame.CurrentCharacter.Blueprint.AllEquipmentSlots;

  RenderImage.RenderToImageBegin;
  // TODO: maybe optimize (ViewGame.CurrentCharacter.Inventory.Apparel[E] <> nil) and ViewGame.CurrentCharacter.Inventory.Apparel[E].Data.RenderSlots.ContainsKey(E) -> Data.RenderSlotsSet
  for E in TApparelSlot do
  begin
    if not (E in Hidden) and (ViewGame.CurrentCharacter.Inventory.Apparel[E] <> nil) then
    begin
      ShapePose := ViewGame.CurrentCharacter.GetShapePose;
      ApparelImagesDictionary := ViewGame.CurrentCharacter.Inventory.Apparel[E].Data.Images[ShapePose];
      if ApparelImagesDictionary.ContainsKey(E) then
      begin
        AImage := ApparelImagesDictionary[E];
        AImage.Image.Color := ViewGame.CurrentCharacter.Inventory.ColorForSlot[E];
        AImage.Image.DrawUnscaled;
      end;
    end;
  end;

  RenderImage.RenderToImageEnd;
end;

procedure TPaperDoll.Render;
var
  E: TApparelSlot;
  R: TFloatRectangle;
  W: Single;
begin
  inherited Render;
  // TODO: cache in Resize?
  W := Single(RenderRect.Height) * Single(RenderImage.Width) / Single(RenderImage.Height);
  R := FloatRectangle(RenderRect.Left + (RenderRect.Width - W) / 2, RenderRect.Bottom, W, RenderRect.Height);
  for E in TApparelSlot do
    RenderImage.Draw(R);
end;

constructor TPaperDoll.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FullSize := true; // TODO: Maybe temporary
  Nude := false;
  BlankImage := TRgbAlphaImage.Create;
  RenderImage := TDrawableImage.Create(BlankImage, true, false);
end;

destructor TPaperDoll.Destroy;
begin
  FreeAndNil(RenderImage);
  FreeAndNil(BlankImage);
  inherited Destroy;
end;

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Handling of sounds and music }
unit GameSounds;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, Classes, DOM,
  CastleSoundEngine;

type
  { Read TCastleSound + name}
  TSoundContainer = class(TCastleSound) // cannot call it TSound because of deprecated TSound in CastleEngine which confuses FPC
  public
    SoundName: String;
    class function Read(const BaseUrl: String; Element: TDOMElement): TSoundContainer;
  end;

{ Play a sound }
procedure Sound(const SoundName: String);
{ Play a sound and return its duration}
function SoundWithDuration(const SoundName: String): Single;
{ Start playing a music track by name (looping)}
procedure Music(const MusicName: String);
{ If a sound with the name has been loaded from  game data}
function SoundExists(const SoundName: String): Boolean;
{ Return sound container by sound name (internal)}
function GetSoundByName(const SoundName: String): TSoundContainer; inline;

{ Clear all sounds loaded }
procedure ClearSounds;
{ Load sounds from XML file }
procedure LoadSounds(const Url: String);
{ Free all containers related to sounds and their content }
procedure FreeSounds;
implementation
uses
  Generics.Collections,
  CastleDownload, CastleXmlUtils, CastleUriUtils,
  GameRandom, GameLog;

type
  ESoundLoadError = class(Exception);

class function TSoundContainer.Read(const BaseUrl: String; Element: TDOMElement): TSoundContainer;
begin
  // crop of TRepoSoundEngine.TSoundInfoBuffer.ReadElement
  Result := TSoundContainer.Create(nil);
  Result.SoundName := Element.AttributeString('Name'); //  Note: not Result.Name, because latter one is component name and adds a lot of (weird) requirements on top
  Result.Volume := Element.AttributeSingleDef('Volume', 1);
  Result.Priority := Element.AttributeSingleDef('Priority', 0.5);
  Result.Stream := Element.AttributeBooleanDef('Stream', false);
  { set URL at the end, to avoid reloading when Sound.Stream changes }
  Result.Url := CombineURI(BaseUrl, Element.AttributeString('Url'));
end;

type
  TSoundsList = specialize TObjectList<TSoundContainer>;
  TLastSoundsDictionary = specialize TDictionary<String, String>;
  TSoundsDictionary = specialize TObjectDictionary<String, TSoundsList>;

var
  LastSoundsDictionary: TLastSoundsDictionary; // TODO: we don't need THAT much? Only last sound/last music track - two strings
  SoundsDictionary: TSoundsDictionary;

function GetSoundByName(const SoundName: String): TSoundContainer; inline;
var
  S: TSoundContainer;
  DictionaryEntry: TSoundsList;
begin
  if SoundsDictionary.TryGetValue(SoundName, DictionaryEntry) then
  begin
    repeat
      S := DictionaryEntry[Rnd.Random(DictionaryEntry.Count)];
    until (DictionaryEntry.Count = 1) or (S.SoundName <> LastSoundsDictionary[SoundName]);
    LastSoundsDictionary[SoundName] := S.SoundName;
    Exit(S);
  end
  else
  begin
    LogWarning('Sound not found: %s', [SoundName]);
    Exit(nil);
  end;
end;

procedure Sound(const SoundName: String);
begin
  SoundEngine.Play(GetSoundByName(SoundName));
end;

function SoundWithDuration(const SoundName: String): Single;
var
  TheSound: TSoundContainer;
begin
  TheSound := GetSoundByName(SoundName);
  if TheSound <> nil then
  begin
    Result := TheSound.Duration;
    SoundEngine.Play(TheSound);
  end else
    Exit(0);
end;

procedure Music(const MusicName: String);
begin
  SoundEngine.LoopingChannel[0].Sound := GetSoundByName(MusicName);
end;

function SoundExists(const SoundName: String): Boolean;
begin
  Exit(GetSoundByName(SoundName) <> nil);
end;

procedure ClearSounds;
begin
  if SoundsDictionary = nil then
  begin
    SoundsDictionary := TSoundsDictionary.Create([doOwnsValues]);
    LastSoundsDictionary := TLastSoundsDictionary.Create;
  end else
  begin
    SoundsDictionary.Clear;
    LastSoundsDictionary.Clear;
  end;
end;

procedure LoadSounds(const Url: String);
var
  Doc: TXMLDocument;
  S: String;
  Sound: TSoundContainer;
  Iterator: TXMLElementIterator;
  AllSoundNames: TStringList;
begin
  LogNormal('Loading sounds: %s', [Url]);
  Doc := URLReadXML(Url);

  Iterator := Doc.DocumentElement.ChildrenIterator('Sound');
  try
    while Iterator.GetNext do
    begin
      Sound := TSoundContainer.Read(ExtractUriPath(Url), Iterator.Current);
      if Pos('#', Sound.SoundName) > 0 then
        S := Copy(Sound.SoundName, 0, Pos('#', Sound.SoundName) - 1)
      else
        S := Sound.SoundName;
      if not SoundsDictionary.ContainsKey(S) then
        SoundsDictionary.Add(S, TSoundsList.Create(true));
      SoundsDictionary[S].Add(Sound);
    end;
  finally
    FreeAndNil(Iterator);
  end;
  FreeAndNil(Doc);

  for S in SoundsDictionary.Keys do
    LastSoundsDictionary.AddOrSetValue(S, '');

  //validate sound names are unique; failing this check may result in "endless loop" in GetSoundByName
  AllSoundNames := TStringList.Create;
  AllSoundNames.Duplicates := dupIgnore;
  AllSoundNames.Sorted := true; // not sure about performance, but hopefully search is faster on a sorted list
  for S in SoundsDictionary.Keys do
    for Sound in SoundsDictionary[S] do
    begin
      if AllSoundNames.IndexOf(Sound.SoundName) = -1 then
        AllSoundNames.Add(Sound.SoundName)
      else
        LogWarning('Duplicate sound name: %s', [Sound.SoundName]);
    end;
  FreeAndNil(AllSoundNames);
end;

procedure FreeSounds;
begin
  FreeAndNil(SoundsDictionary);
  FreeAndNil(LastSoundsDictionary);
end;

end.


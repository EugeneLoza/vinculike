unit CastleSlider;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  CastleUiControls, CastleControls, CastleClassUtils,
  CastleKeysMouse, CastleVectors;

type
  { For now we are using a TCastleButton to have
    Normal, Focused, Pressed and Disabled images easily
    and almost automatically handled by CGE
    TODO: Make a real "rollover" image
    With less features bloat like label, color, etc. }
  TCastleRolloverImage = TCastleButton;

type
  { A generic slider,
    Allows input value by dragging, clicking or setting by code
    Value is clamped within 0..1
    The slider handle image is created as a child rollover image
    and can be customized as necessary through Castle Editor }
  TCastleSlider = class(TCastleUserInterface)
  strict private
    const
      DefaultValue = 0.5;
    var
      FOnChanged: TNotifyEvent;
      WasPressedFinger: Integer;
      FValue: Single;
      FVertical: Boolean;
      FImage: TCastleRolloverImage;
    procedure UpdateValueToScreen(const ScreenPos: TVector2);
    procedure SetVertical(AValue: Boolean);
  strict protected
    { Clamps and quantizes the value if necessary }
    function QuantizeValue(const AValue: Single): Single; virtual;
    procedure SetValue(const AValue: Single);
  public
    function PropertySections(const PropertyName: String): TPropertySections; override;
    function PreviewPress(const Event: TInputPressRelease): boolean; override;
    function PreviewRelease(const Event: TInputPressRelease): boolean; override;
    function Motion(const Event: TInputMotion): boolean; override;
    constructor Create(AOwner: TComponent); override;
  published
    { If this slider is vertical }
    property Vertical: Boolean read FVertical write SetVertical;
    { Value of this slider, clamped within 0..1 range }
    property Value: Single read FValue write SetValue {$ifdef FPC}default DefaultValue{$endif};
    { Rollover image of the handler }
    property HandleImage: TCastleRolloverImage read FImage;
    { Called when the value is changed by user or by code }
    property OnChanged: TNotifyEvent read FOnChanged write FOnChanged;
  end;

  { Additionally quantizes the slider value
    over Steps amount of fixed evenly spaced positions
    within 0..1 range
    Useful for input of discrete values }
  TCastleDiscreteSlider = class(TCastleSlider)
  strict private
  const
    DefaultSteps = Integer(5);
  var
    FSteps: Integer;
  procedure SetSteps(AValue: Integer);
  strict protected
    function QuantizeValue(const AValue: Single): Single; override;
  public
    function PropertySections(const PropertyName: String): TPropertySections; override;
    constructor Create(AOwner: TComponent); override;
  published
    { Number of fixed positions on the slider }
    property Steps: Integer read FSteps write SetSteps {$ifdef FPC}default DefaultSteps{$endif};
  end;

implementation
uses
  SysUtils,
  CastleComponentSerialize, CastleLog
  {$ifdef CASTLE_DESIGN_MODE}, PropEdits, CastlePropEdits{$endif};

function TCastleSlider.QuantizeValue(const AValue: Single): Single;
begin
  if AValue < 0 then
    Exit(0)
  else
  if AValue > 1.0 then
    Exit(1)
  else
    Exit(AValue);
end;

procedure TCastleSlider.UpdateValueToScreen(const ScreenPos: TVector2);
begin
  if FVertical then
  begin
    if RenderRect.Height <= HandleImage.RenderRect.Height then
    begin
      WriteLnWarning('Slider height = %.0n must not be smaller than handle height = $.0n', [Height, HandleImage.Height]);
      Exit;
    end;
    SetValue((ScreenPos.Y - (RenderRect.Bottom + HandleImage.RenderRect.Height / 2)) / (RenderRect.Height - HandleImage.RenderRect.Height))
  end else
  begin
    if RenderRect.Width <= HandleImage.RenderRect.Width then
    begin
      WriteLnWarning('Slider width = %.0n must not be smaller than handle width = $.0n', [Width, HandleImage.Width]);
      Exit;
    end;
    SetValue((ScreenPos.X - (RenderRect.Left + HandleImage.RenderRect.Width / 2)) / (RenderRect.Width - HandleImage.RenderRect.Width));
  end;
end;

procedure TCastleSlider.SetValue(const AValue: Single);
begin
  if FValue <> QuantizeValue(AValue) then
  begin
    FValue := QuantizeValue(AValue);
    { Note, we already show error about slider width/height smaller than handle width/height
      It will always spam logs when trying to move handle manually
      but will not show error when value is assigned by code.
      Maybe it should? }
    if FVertical then
      FImage.Translation := Vector2(0, FValue * (Height - HandleImage.Height) + HandleImage.Height / 2)
    else
      FImage.Translation := Vector2(FValue * (Width - HandleImage.Width) + HandleImage.Width / 2, 0);
    if Assigned(FOnChanged) then
      FOnChanged(Self);
  end;
end;

procedure TCastleSlider.SetVertical(AValue: Boolean);
begin
  FVertical := AValue;
  if FVertical then
  begin
    FImage.Anchor(vpMiddle, vpBottom);
    FImage.Anchor(hpMiddle, hpMiddle);
  end else
  begin
    FImage.Anchor(vpMiddle, vpMiddle);
    FImage.Anchor(hpMiddle, hpLeft);
  end;
  SetValue(Value);
end;

function TCastleSlider.PropertySections(const PropertyName: String): TPropertySections;
begin
  if (PropertyName = 'Value') or
     (PropertyName = 'Vertical') then
    Result := [psBasic]
  else
    Result := inherited PropertySections(PropertyName);
end;

function TCastleSlider.PreviewPress(const Event: TInputPressRelease): boolean;
begin
  Result := inherited PreviewPress(Event);
  if (WasPressedFinger < 0) and Event.IsMouseButton(buttonLeft) then
  begin
    { Ok, bug:
      If we set Result := true, child doesn't receive Pressed
      and therefore rollover image doesn't show Pressed state
      But if we set comment it out...
      I don't know why but the slider slides without any press }
    Result := true; // wherever we click inside Slider we handle it
    // So here is the workaround, we manually set it to pressed:
    FImage.Pressed := true;
    WasPressedFinger := Event.FingerIndex;
    UpdateValueToScreen(Event.Position);
  end;
end;

function TCastleSlider.PreviewRelease(const Event: TInputPressRelease): boolean;
begin
  Result := inherited PreviewRelease(Event);
  if (WasPressedFinger = Event.FingerIndex) and Event.IsMouseButton(buttonLeft) then
  begin
    Result := true;
    FImage.Pressed := false;
    WasPressedFinger := -1;
    UpdateValueToScreen(Event.Position);
  end;
end;

function TCastleSlider.Motion(const Event: TInputMotion): boolean;
begin
  Result := inherited Motion(Event);
  if WasPressedFinger = Event.FingerIndex then
  begin
    Result := true;
    // Otherwise button will stop being pressed as soon as we mouse away from it
    FImage.Pressed := true;
    UpdateValueToScreen(Event.Position);
  end;
end;

constructor TCastleSlider.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FImage := TCastleRolloverImage.Create(Self);
  FImage.CustomBackground := false;
  FImage.Name := 'Handle';
  FImage.Caption := ''; // TODO
  InsertFront(FImage);
  SetVertical(false); // will anchor image
  SetValue(DefaultValue); // will position image
  WasPressedFinger := -1;
end;

procedure TCastleDiscreteSlider.SetSteps(AValue: Integer);
begin
  if FSteps <> AValue then
  begin
    FSteps := AValue;
    SetValue(Value);
  end;
end;

function TCastleDiscreteSlider.QuantizeValue(const AValue: Single): Single;
var
  ClampedValue: Single;
begin
  ClampedValue := inherited;
  Exit(Single(Round(ClampedValue * (FSteps - 1))) / Single((FSteps - 1)));
end;

function TCastleDiscreteSlider.PropertySections(const PropertyName: String): TPropertySections;
begin
  if (PropertyName = 'Steps') then
    Result := [psBasic]
  else
    Result := inherited PropertySections(PropertyName);
end;

constructor TCastleDiscreteSlider.Create(AOwner: TComponent);
begin
  Steps := DefaultSteps;
  inherited Create(AOwner);
end;

initialization
  RegisterSerializableComponent(TCastleSlider, 'Slider');
  RegisterSerializableComponent(TCastleDiscreteSlider, 'Slider (Discrete)');
end.


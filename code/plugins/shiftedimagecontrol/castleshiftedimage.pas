unit CastleShiftedImage;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  CastleImages, CastleGlImages, CastleColors, CastleRectangles;

type
  // a better idea may be inheriting from TCastleImagePersistent (and this way it may even become compatible with a regular TCastleImageControl) but TCastleImagePersistent has a lot of non-virtual methods which would have to be overridden (Width, Height, Draw first of all), and I don't want to modify CGE or copy-paste TCastleImagePersistent
  TCastleShiftedImage = class(TPersistent)
  private
    FUrl: String;
    FImage: TCastleImage;
    FDrawableImage: TDrawableImage;
    FColor: TCastleColor;
    function GetColor: TCastleColor;
    procedure SetColor(const AValue: TCastleColor);
    procedure SetImage(const Value: TCastleImage);
    procedure SetUrl(const AValue: String);
  public
    ImageFileName: String;
    Width, Height: Integer;
    ShiftX, ShiftY: Integer;

    OnChange: TNotifyEvent;

    property Url: String read FUrl write SetUrl;
    property Color: TCastleColor read GetColor write SetColor;
    property Image: TCastleImage read FImage write SetImage;
    property DrawableImage: TDrawableImage read FDrawableImage;
    procedure Save(const AUrl: String);
    procedure Load(const AUrl: String);
    { Draw the image in given screen area. }
    procedure Draw(const ScreenRectangle: TFloatRectangle);
    procedure DrawUnscaled;
    constructor Create; //override
    destructor Destroy; override;
  end;

implementation
uses
  SysUtils, DOM,
  CastleXmlUtils, CastleUriUtils, CastleLog;

procedure TCastleShiftedImage.SetImage(const Value: TCastleImage);
begin
  if Value <> FImage then
  begin
    FreeAndNil(FImage);
    FImage := Value;
    FreeAndNil(FDrawableImage);
    FDrawableImage := TDrawableImage.Create(FImage, false, false); // WARNING: no smooth scaling
    FDrawableImage.Color := FColor;
    if Assigned(OnChange) then
      OnChange(Self);
  end;
end;

procedure TCastleShiftedImage.SetUrl(const AValue: String);
begin
  if FUrl = AValue then
    Exit;
  Load(AValue);
end;

function TCastleShiftedImage.GetColor: TCastleColor;
begin
  Result := FColor;
end;

procedure TCastleShiftedImage.SetColor(const AValue: TCastleColor);
begin
  FColor := AValue;
  if FDrawableImage <> nil then
    FDrawableImage.Color := FColor;
end;

procedure TCastleShiftedImage.Save(const AUrl: String);
var
  XMLDoc: TXMLDocument;
  XMLElement: TDOMElement;
begin
  FUrl := AUrl;
  ImageFileName := ExtractURIName(AUrl);

  XMLDoc := TXMLDocument.Create;
  XMLElement := XMLDoc.CreateElement('Root');
  XMLDoc.AppendChild(XMLElement);
  XMLElement.AttributeSet('file', ImageFileName);
  XMLElement.AttributeSet('width', Width);
  XMLElement.AttributeSet('height', Height);
  XMLElement.AttributeSet('x', ShiftX);
  XMLElement.AttributeSet('y', ShiftY);
  URLWriteXML(XMLdoc, AUrl + '.xml');
  XMLDoc.Free;

  SaveImage(Image, AUrl);
end;

procedure TCastleShiftedImage.Load(const AUrl: String);
var
  XMLDoc: TXMLDocument;
  XMLElement: TDOMElement;
begin
  FUrl := AUrl;
  if AUrl = '' then
  begin
    FreeAndNil(FDrawableImage);
    FreeAndNil(FImage);
    Exit;
  end;
  try
    try
      URLReadXML(XMLDoc, AUrl + '.xml');
      XMLElement := XmlDoc.DocumentElement;
      ImageFileName := XMLElement.AttributeStringDef('file', '');
      Width := XMLElement.AttributeIntegerDef('width', 0);
      Height := XMLElement.AttributeIntegerDef('height', 0);
      ShiftX := XMLElement.AttributeIntegerDef('x', 0);
      ShiftY := XMLElement.AttributeIntegerDef('y', 0);
      Image := LoadImage(ExtractUriPath(AUrl) + ImageFileName, [TRGBAlphaImage]);
    except
      Image := LoadImage(AUrl, [TRGBAlphaImage]);
      Width := Image.Width;
      Height := Image.Height;
      ShiftX := 0;
      ShiftY := 0;
      WritelnWarning('Failed to load metadata for %s. Assuming image is not shifted.', [AUrl]);
    end;
  finally
    XMLDoc.Free;
  end;
end;

procedure TCastleShiftedImage.Draw(const ScreenRectangle: TFloatRectangle);
var
  RenderRectangle: TFloatRectangle;
  UiScaleX, UiScaleY: Single;
begin
  if FDrawableImage = nil then
    Exit;
  UiScaleX := ScreenRectangle.Width / Width;
  UiScaleY := ScreenRectangle.Height / Height;
  RenderRectangle := FloatRectangle(
    ScreenRectangle.Left + ShiftX * UiScaleX,
    ScreenRectangle.Bottom + ShiftY * UiScaleY,
    FDrawableImage.Width * UiScaleX,
    FDrawableImage.Height * UiScaleY
  );
  FDrawableImage.Draw(RenderRectangle);
end;

procedure TCastleShiftedImage.DrawUnscaled;
begin
  FDrawableImage.Draw(FloatRectangle(ShiftX, ShiftY, FDrawableImage.Width, FDrawableImage.Height));
end;

constructor TCastleShiftedImage.Create;
begin
  inherited;
  FColor := White;
end;

destructor TCastleShiftedImage.Destroy;
begin
  FreeAndNil(FImage);
  FreeAndNil(FDrawableImage);
  inherited;
end;

end.


{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Difficulty levels define a set of in-game parameters
  that affect gameplay difficulty }
unit GameDifficultyLevel;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, Generics.Collections,
  GameValidated;

{ TODO: Some of those may (should) go into skills, maybe remain as multipliers on top of those }
type
  TDifficultyLevel = class(TObject, IValidated)
  public
    { User-readable name of this difficulty level }
    Name: String;
    {}
    MonsterAttackDelayMultiplier: Single; // not used yet!
    { Multiplier to quantity of monsters generated on the map }
    MonstersNumberMultiplier: Single;
    { Max quantity of prisoners that can be held on a single dungeon level }
    MaxPrisonersPerLevel: Integer;
    { multiplier to number of items generated on a dungeon level}
    ItemsNumberMultiplier: Single;
    { Delay between the first monsters patrol enters the map }
    FirstPatrolSpawnTime: Single;
    { Delay after first and subsequent patrol waves to the next one}
    EnemyPatrolSpawnTime: Single;
    { Willpower damage caused by a useful item broken while worn }
    BrokenItemWillpowerDamage: Single;
    { Fraction of willpower lost due to "lost top" }
    StripTopWillpowerFractionDamage: Single;
    { Fraction of willpower lost due to "lost bottom"}
    StripBottomWillpowerFractionDamage: Single;
    { Willpower damage due to harmful touch on uncovered private parts }
    HitNakedWillpowerDamage: Single;
    { Additional willpower damage due to receiving significant health damage}
    LowHealthWillpowerDamage: Single;
    { Rate at which "max" stats degrade with depletion of current values }
    StatsDegradationSpeed: Single;
    { base value of expense of stamina while struggling against restraints}
    BondageStruggleStaminaPerSecond: Single;
    { Ratio of stamina spent to damage of a restraining while struggling}
    BondageStruggleStaminaToProgressRatio: Single;
    { How much damage a footwear item takes per tile walked }
    FootwearDamageMultiplier: Single;
    { Calculate max quantity of prisoners on the given dungeon deoth }
    function MaxPrisonersAtLevel(const ALevel: Integer): Integer;
    { Validation if these settings make sense}
    procedure Validate;
  end;
  TDifficultyLevelsList = specialize TObjectList<TDifficultyLevel>;

var
  { Shortcut to currently selected difficulty level definition }
  Difficulty: TDifficultyLevel;

  { Difficulty level selected by default
    warning: this value is initialized while reading difficulty
    definitions from an XML file }
  DefaultDifficultyLevel: Integer = -1;
  { All difficulty levels definitions }
  DifficultyLevels: TDifficultyLevelsList;

{ Properly set a difficulty level by id }
procedure SetDifficultyLevel(const DL: Integer);
{ Load difficulty levels definition from gmae data }
procedure LoadDifficultyLevels;
{ Free everything related to difficulty levels }
procedure FreeDifficultyLevels;
implementation
uses
  DOM,
  CastleXMLUtils,
  GameConfiguration, GameLog;

type
  EDifficultyLevelValidationFailed = class(Exception);

procedure SetDifficultyLevel(const DL: Integer);
begin
  Configuration.DifficultyLevel := DL;
  Difficulty := DifficultyLevels[DL];
  LogNormal('Setting Difficulty level to %s', [Difficulty.Name]);
end;

procedure LoadDifficultyLevels;
var
  DL: TDifficultyLevel;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  DifficultyLevels := TDifficultyLevelsList.Create(true);

  DefaultDifficultyLevel := -1;
  Doc := URLReadXML('castle-data:/difficultylevels.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('DifficultyLevel');
    try
      while Iterator.GetNext do
      begin
        DL := TDifficultyLevel.Create;
        DL.Name := Iterator.Current.AttributeString('Name');
        DL.MonsterAttackDelayMultiplier := Iterator.Current.AttributeFloat('MonsterAttackDelayMultiplier');
        DL.MonstersNumberMultiplier := Iterator.Current.AttributeFloat('MonstersNumberMultiplier');
        DL.ItemsNumberMultiplier := Iterator.Current.AttributeFloat('ItemsNumberMultiplier');
        DL.FirstPatrolSpawnTime := Iterator.Current.AttributeFloat('FirstPatrolSpawnTime');
        DL.EnemyPatrolSpawnTime := Iterator.Current.AttributeFloat('EnemyPatrolSpawnTime');
        DL.BrokenItemWillpowerDamage := Iterator.Current.AttributeFloat('BrokenItemWillpowerDamage');
        DL.StripTopWillpowerFractionDamage := Iterator.Current.AttributeFloat('StripTopWillpowerFractionDamage');
        DL.StripBottomWillpowerFractionDamage := Iterator.Current.AttributeFloat('StripBottomWillpowerFractionDamage');
        DL.HitNakedWillpowerDamage := Iterator.Current.AttributeFloat('HitNakedWillpowerDamage');
        DL.LowHealthWillpowerDamage := Iterator.Current.AttributeFloat('LowHealthWillpowerDamage');
        DL.StatsDegradationSpeed := Iterator.Current.AttributeFloat('StatsDegradationSpeed');
        DL.BondageStruggleStaminaPerSecond := Iterator.Current.AttributeFloat('BondageStruggleStaminaPerSecond');
        DL.BondageStruggleStaminaToProgressRatio := Iterator.Current.AttributeFloat('BondageStruggleStaminaToProgressRatio');
        DL.MaxPrisonersPerLevel := Iterator.Current.AttributeInteger('MaxPrisonersPerLevel');
        DL.FootwearDamageMultiplier := Iterator.Current.AttributeSingle('FootwearDamageMultiplier');
        DL.Validate;
        if Iterator.Current.AttributeBoolean('Default') then
        begin
          if DefaultDifficultyLevel >= 0 then
            LogWarning('Difficulty levels can contain only one default difficulty level, but %s and %s are marked as default', [DL.Name, DifficultyLevels[DefaultDifficultyLevel]]);
          DefaultDifficultyLevel := DifficultyLevels.Add(DL);
        end else
          DifficultyLevels.Add(DL);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;

  if Configuration.DifficultyLevel < 0 then // Configuration is reset before we call this routine, -1 means DifficultyLevel is not initialized yet
    SetDifficultyLevel(DefaultDifficultyLevel)
  else
    SetDifficultyLevel(Configuration.DifficultyLevel);
end;

function TDifficultyLevel.MaxPrisonersAtLevel(const ALevel: Integer): Integer;
begin
  Result := ALevel div 5;
  if Result > MaxPrisonersPerLevel then
    Result := MaxPrisonersPerLevel;
end;

procedure TDifficultyLevel.Validate;
begin
  if Name = '' then
    raise EDifficultyLevelValidationFailed.Create('Difficulty level has empty name');
  if MonsterAttackDelayMultiplier <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('MonsterAttackDelayMultiplier = %n <= 0', [MonsterAttackDelayMultiplier]);
  if MonstersNumberMultiplier <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('MonstersNumberMultiplier = %n <= 0', [MonstersNumberMultiplier]);
  if ItemsNumberMultiplier <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('ItemsNumberMultiplier = %n <= 0', [ItemsNumberMultiplier]);
  if FirstPatrolSpawnTime <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('FirstPatrolSpawnTime = %n <= 0', [FirstPatrolSpawnTime]);
  if EnemyPatrolSpawnTime <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('EnemyPatrolSpawnTime = %n <= 0', [EnemyPatrolSpawnTime]);
  if BrokenItemWillpowerDamage <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('BrokenItemWillpowerDamage = %n <= 0', [BrokenItemWillpowerDamage]);
  if StripTopWillpowerFractionDamage <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('StripTopWillpowerFractionDamage = %n <= 0', [StripTopWillpowerFractionDamage]);
  if StripBottomWillpowerFractionDamage <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('StripBottomWillpowerFractionDamage = %n <= 0', [StripBottomWillpowerFractionDamage]);
  if HitNakedWillpowerDamage <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('HitNakedWillpowerDamage = %n <= 0', [HitNakedWillpowerDamage]);
  if LowHealthWillpowerDamage <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('LowHealthWillpowerDamage = %n <= 0', [EnemyPatrolSpawnTime]);
  if StatsDegradationSpeed <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('StatsDegradationSpeed = %n <= 0', [StatsDegradationSpeed]);
  if BondageStruggleStaminaPerSecond <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('BondageStruggleStaminaPerSecond = %n <= 0', [BondageStruggleStaminaPerSecond]);
  if BondageStruggleStaminaToProgressRatio <= 0 then
    raise EDifficultyLevelValidationFailed.CreateFmt('BondageStruggleStaminaToProgressRatio = %n <= 0', [BondageStruggleStaminaToProgressRatio]);
end;

procedure FreeDifficultyLevels;
begin
  FreeAndNil(DifficultyLevels);
end;

end.


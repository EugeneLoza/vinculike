{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Generates names for characters, female only for now}
unit GameNamesGenerator;

{$INCLUDE compilerconfig.inc}

interface

const
  { Max symbols that can be in character's name}
  MaxNameSymbols = Integer(12);

{ Initialize data relative to names generation}
procedure InitNamesGenerator;
{ Generate a random name for a female}
function GenerateFemaleName: String;
{ Check if a given name is "free"}
function PlayerNameNotExists(const AName: String; const IgnoreCharacter: TObject = nil): Boolean;
implementation
uses
  SysUtils,
  GameRandom, GameMap, GameLog;

const
  FirstPart : array of String =
  (
    'Acac',
    'Acanth',
    'Achill',
    'Adamant',
    'Adelph',
    'Adonic',
    'Agath',
    'Agneth',
    'Akon',
    'Aldor',
    'Alenk',
    'Aleth',
    'Algal',
    'Aliz',
    'Almond',
    'Alph',
    'Amal',
    'Amanth',
    'Amar',
    'Ambros',
    'Amint',
    'Anast',
    'Andr',
    'Androm',
    'Aneesh',
    'Anemon',
    'Anet',
    'Ang',
    'Angel',
    'Anis',
    'Ant',
    'Anton',
    'Apel',
    'Aquar',
    'Arcad',
    'Aret',
    'Arsen',
    'Aspas',
    'Aster',
    'Atalant',
    'Athen',
    'Avr',
    'Azal',
    'Barv',
    'Basil',
    'Bellad',
    'Beren',
    'Bet',
    'Bogar',
    'Bon',
    'Buz',
    'Calandr',
    'Calyps',
    'Cassandr',
    'Cassiop',
    'Cat',
    'Cather',
    'Celen',
    'Celest',
    'Char',
    'Chlor',
    'Choros',
    'Chrysanth',
    'Cind',
    'Ciprian',
    'Corin',
    'Cyan',
    'Cycl',
    'Cypress',
    'Daffod',
    'Damar',
    'Daphn',
    'Dar',
    'Delt',
    'Dex',
    'Demetr',
    'Delekt',
    'Diakr',
    'Diamant',
    'Diandr',
    'Dianth',
    'Dimitr',
    'Din',
    'Dittan',
    'Domn',
    'Dorel',
    'Doroth',
    'Dzvon',
    'Ech',
    'Edelv',
    'Eff',
    'Effim',
    'Egan',
    'Eirin',
    'Elar',
    'Electr',
    'Elen',
    'Elian',
    'Elpid',
    'Eos',
    'Epidex',
    'Epsilon',
    'Eranth',
    'Eren',
    'Eudor',
    'Euphem',
    'Eurid',
    'Euthal',
    'Fantas',
    'Fedor',
    'Fial',
    'Filip',
    'Filomen',
    'Fir',
    'Flor',
    'Galat',
    'Galen',
    'Gamm',
    'Garden',
    'Gefistik',
    'Georg',
    'Geran',
    'Gladiol',
    'Glyc',
    'Halcyon',
    'Harmon',
    'Hecat',
    'Helen',
    'Helios',
    'Hellebor',
    'Her',
    'Herb',
    'Hest',
    'Hilar',
    'Hyacinth',
    'Ilian',
    'Indig',
    'Ioan',
    'Iolanth',
    'Ion',
    'Iot',
    'Iris',
    'Isador',
    'Ismin',
    'Jocast',
    'Jorin',
    'Jovian',
    'Justin',
    'Kad',
    'Kalend',
    'Kalliop',
    'Kamel',
    'Kapp',
    'Karis',
    'Karrin',
    'Kassandr',
    'Katar',
    'Katelin',
    'Kath',
    'Ket',
    'Khron',
    'Klem',
    'Kolos',
    'Konstantin',
    'Konval',
    'Kor',
    'Koral',
    'Kulb',
    'Kvit',
    'Lambd',
    'Larkspur',
    'Latat',
    'Led',
    'Len',
    'Lenor',
    'Leth',
    'Levand',
    'Lexis',
    'Libr',
    'Lil',
    'Liz',
    'Lotos',
    'Louiz',
    'Lyd',
    'Maarit',
    'Macar',
    'Magnol',
    'Malin',
    'Malv',
    'Mart',
    'Margar',
    'Marik',
    'Marth',
    'Maryam',
    'Meagan',
    'Medor',
    'Megal',
    'Mel',
    'Melind',
    'Melit',
    'Melor',
    'Michael',
    'Milenk',
    'Milisend',
    'Mim',
    'Mint',
    'Monik',
    'Myrt',
    'Nan',
    'Nard',
    'Natas',
    'Neol',
    'Nerin',
    'Nikol',
    'Nostim',
    'Obel',
    'Ocean',
    'Odel',
    'Olg',
    'Omeg',
    'Ophel',
    'Orch',
    'Orien',
    'Pall',
    'Pameel',
    'Pandor',
    'Panth',
    'Parask',
    'Parthen',
    'Pash',
    'Pavlin',
    'Peg',
    'Pelag',
    'Peon',
    'Per',
    'Permel',
    'Perrin',
    'Pers',
    'Persephon',
    'Petal',
    'Petr',
    'Petron',
    'Phaedr',
    'Phil',
    'Photin',
    'Pivon',
    'Polis',
    'Polem',
    'Polyx',
    'Prim',
    'Psych',
    'Ren',
    'Revek',
    'Rhet',
    'Rhod',
    'Rig',
    'Romas',
    'Rosmar',
    'Roxan',
    'Sapphir',
    'Sar',
    'Sel',
    'Selen',
    'Shafr',
    'Sibil',
    'Sigm',
    'Skopol',
    'Soph',
    'Spith',
    'Stas',
    'Stefan',
    'Stel',
    'Syren',
    'Tas',
    'Telek',
    'Teleios',
    'Teres',
    'Thal',
    'Them',
    'Thessalon',
    'Thet',
    'Tiffan',
    'Timoth',
    'Tin',
    'Titan',
    'Topaz',
    'Toul',
    'Troyan',
    'Tulpan',
    'Vanes',
    'Varvar',
    'Vas',
    'Vasil',
    'Veronik',
    'Viktor',
    'Violet',
    'Volosh',
    'Yarr',
    'Xanth',
    'Xen',
    'Ximen',
    'Xyl',
    'Zantin',
    'Zen',
    'Zetr',
    'Zil',
    'Zin',
    'Zinov'
  );
  SecondPartFemale : array of String =
  (
    'a',
    'acta',
    'ada',
    'ala',
    'ali',
    'alla',
    'ama',
    'ami',
    'amma',
    'ana',
    'ani',
    'anna',
    'ara',
    'ari',
    'ata',
    'ati',
    'atta',
    'axa',
    'axi',
    'e',
    'ea',
    'eia',
    'eka',
    'eki',
    'ela',
    'eli',
    'ella',
    'ema',
    'emi',
    'emma',
    'ena',
    'eni',
    'era',
    'eri',
    'esa',
    'esi',
    'essa',
    'eta',
    'eti',
    'etta',
    'i',
    'ia',
    'iana',
    'ida',
    'idda',
    'ika',
    'ikka',
    'ima',
    'imma',
    'ina',
    'inna',
    'ira',
    'isa',
    'ita',
    'itta',
    'oka',
    'oki',
    'okka',
    'ola',
    'oli',
    'olla',
    'oma',
    'omi',
    'omma',
    'ona',
    'oni',
    'ora',
    'ori',
    'osa',
    'osi',
    'ota',
    'oti'
  );

function PlayerNameNotExists(const AName: String; const IgnoreCharacter: TObject): Boolean;
var
  I: Integer;
begin
  for I := 0 to Pred(Map.PlayerCharactersList.Count) do
    if (Map.PlayerCharactersList[I] <> IgnoreCharacter) and (Map.PlayerCharactersList[I].Data.DisplayName = AName) then
      Exit(false);
  Exit(true);
end;

function PlayerNameNotContains(const AName: String): Boolean;
var
  I: Integer;
begin
  for I := 0 to Pred(Map.PlayerCharactersList.Count) do
    if Pos(AName, Map.PlayerCharactersList[I].Data.DisplayName) > 0 then
      Exit(false);
  Exit(true);
end;

function NoDuplicateConsonants(const String1, String2: String): Boolean;
const
  ConsonantsTest : array of String =
  (
    'q',
    'w',
    'r',
    't',
    'p',
    's',
    'd',
    'f',
    'g',
    'h',
    'j',
    'k',
    'l',
    'z',
    'x',
    'c',
    'v',
    'b',
    'n',
    'm'
  );
var
  S1, S2, S: String;
begin
  S1 := StringReplace(LowerCase(String1), 'c', 'k', [rfReplaceAll]);
  S2 := StringReplace(LowerCase(String2), 'c', 'k', [rfReplaceAll]);
  for S in ConsonantsTest do
    if (Pos(S, S1) > 0) and (Pos(S, S2) > 0) then Exit(false);
  Exit(true);
end;

procedure InitNamesGenerator;
begin
  LogNormal('Female: Names first parts: %d; Names last parts: %d; Total max names: %d', [Length(FirstPart), Length(SecondPartFemale), Length(FirstPart) * Length(SecondPartFemale)]);
end;

function GenerateFemaleName: String;
const
  MaxRetries = Integer(10000);
var
  First, Second: String;
  Count: Integer;
begin
  Count := 0;
  repeat
    Inc(Count);
    First := FirstPart[Rnd.Random(Length(FirstPart))];
    Second := SecondPartFemale[Rnd.Random(Length(SecondPartFemale))];
    Result := First + Second;
  until (Length(Result) <= MaxNameSymbols) and (PlayerNameNotContains(First) or ((Count > MaxRetries) and PlayerNameNotExists(Result))) and NoDuplicateConsonants(First, Second);
end;


end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameSerializableObject;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, Classes, DOM, Generics.Collections,
  GameGarbageCollector;

type
  EClassDeserealizationError = class(Exception);

type
  TSerializableObject = class abstract(TGarbageCollectObject)
  strict private
    FReferenceId: UInt64;
    procedure SetReferenceId(const AValue: UInt64);
  public
    property ReferenceId: UInt64 read FReferenceId write SetReferenceId;
  public
    class function LoadClass(const Element: TDOMElement): TSerializableObject;
    procedure Save(const Element: TDOMElement); virtual;
    { Should be called when there is something that may need
      converting ReferenceIds into pointers -
      after loading all the classes of the kind }
    procedure AfterDeserealization; virtual;
  protected
    procedure Load(const Element: TDOMElement); virtual;
  public
    constructor Create(const NotLoading: Boolean = true); virtual;
    destructor Destroy; override;
  end;
  TSerializableClass = class of TSerializableObject;
  TReferenceList = specialize TList<UInt64>;

var
  { Global IDs start from 1 (inc before assignment) therefore 0 is good for "null reference" }
  GlobalId: UInt64 = 0;

procedure RegisterSerializableObject(const AClass: TSerializableClass);
function ObjectByReferenceId(const AReferenceId: UInt64): TSerializableObject;
function SerializableClassByName(const AClassName: String): TSerializableClass;
procedure ClearObjectReferences;

procedure InitSerializableObjects;
procedure FreeSerializableObjects;
implementation
uses
  CastleXmlUtils;

type
  TSerializableClassDictionary = specialize TDictionary<String, TSerializableClass>;
  TObjectRefIdDictionary = specialize TDictionary<UInt64, TSerializableObject>;

var
  SerializableClassDictionary: TSerializableClassDictionary;
  ObjectRefIdDictionary: TObjectRefIdDictionary;

procedure RegisterSerializableObject(const AClass: TSerializableClass);
begin
  if SerializableClassDictionary = nil then // NOTE: we can't guess in which order initialization will requrest RegisterSerializableObject, so the only reliable way is to create dictionary at first request
    SerializableClassDictionary := TSerializableClassDictionary.Create;
  SerializableClassDictionary.Add(AClass.ClassName, AClass);
end;

function ObjectByReferenceId(const AReferenceId: UInt64): TSerializableObject;
begin
  Exit(ObjectRefIdDictionary[AReferenceId]);
end;

function SerializableClassByName(const AClassName: String): TSerializableClass;
begin
  Exit(SerializableClassDictionary[AClassName]);
end;

procedure ClearObjectReferences;
begin
  ObjectRefIdDictionary.Clear;
end;

procedure TSerializableObject.SetReferenceId(const AValue: UInt64);
begin
  if FReferenceId <> AValue then
  begin
    if FReferenceId <> 0 then
      ObjectRefIdDictionary.Remove(FReferenceId);
    FReferenceId := AValue;
    ObjectRefIdDictionary.Add(FReferenceId, Self);
  end;
end;

class function TSerializableObject.LoadClass(const Element: TDOMElement): TSerializableObject;
begin
  Result := nil; // sometimes can be not nil, especially due to an exception
  try
    Result := SerializableClassDictionary[Element.AttributeString('ClassName')].Create(false);
    Result.Load(Element);
  except
    FreeAndNil(Result);
    raise;
  end;
end;

procedure TSerializableObject.Save(const Element: TDOMElement);
begin
  Element.AttributeSet('ClassName', ClassName);
  Element.AttributeSet('ReferenceId', ReferenceId);
end;

procedure TSerializableObject.AfterDeserealization;
begin
  // by default, no post-processing is needed
end;

procedure TSerializableObject.Load(const Element: TDOMElement);
begin
  ReferenceId := Element.AttributeQWord('ReferenceId');
end;

constructor TSerializableObject.Create(const NotLoading: Boolean = true);
begin
  inherited Create; // ancestor is empty
  if NotLoading then
  begin
    Inc(GlobalId);
    FReferenceId := 0;
    ReferenceId := GlobalId;
  end; // else Load will set it
end;

destructor TSerializableObject.Destroy;
begin
  if (FReferenceId <> 0) and (ObjectRefIdDictionary <> nil) then
    ObjectRefIdDictionary.Remove(FReferenceId);
  inherited Destroy;
end;

procedure InitSerializableObjects;
begin
  ObjectRefIdDictionary := TObjectRefIdDictionary.Create;
end;

procedure FreeSerializableObjects;
begin
  FreeAndNil(SerializableClassDictionary);
  FreeAndNil(ObjectRefIdDictionary);
end;

end.


{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Collection of all mods found in the game, definition of a mod + methods to load it }
unit GameModsDatabase;

{$INCLUDE compilerconfig.inc}

interface
uses
  Classes, Generics.Collections;

type
  TMod = class;
  TModList = specialize TObjectList<TMod>;
  { A package referring to "modules" that contain game data
    if the file is named package.xml and located in "data" folder
    it will be loaded automatically
    name can also be different to avoid automatic loading of the package
    it can be referenced and load by other package }
  TMod = class(TObject)
  public
    { Url of this package XML}
    Url: String;
    { Id of this mod must be unique over all mods }
    Id: String;
    { IDs of other mods this one depends on }
    Dependencies: TStringList;
    { If this package part of the base GameModsDatabase
      (to show a warning if Player tried to change it,
      as it might break game balance or even make the game glitch) }
    BaseGame: Boolean;

    { Package-like mosules that are loaded by this package
      warning: may not be named "package.xml" because will be loaded twice }
    SubPackages: TModList;
    { Contains absolute URLs to modules }
    Sounds: TStringList;
    Items: TStringList;
    Bodyparts: TStringList;
    Blueprints: TStringList;
    Presets: TStringList;
    Monsters: TStringList;
    Vinculopedia: TStringList;
    { Load contents of the package}
    procedure Load;
    //procedure Validate; Todo
    { Read definition of this package (without loading contents) }
    constructor Read(const AUrl: String);
    destructor Destroy; override;
  end;

var
  { database of all packages found at game initialization}
  Mods: TModList;

{ load packages definitions }
procedure InitModsDatabase;
{ Free all data related to packages (but not their content) }
procedure FreeModsDatabase;
implementation
uses
  SysUtils, DOM,
  CastleXmlUtils, CastleUriUtils, CastleStringUtils,
  CastleFindFiles, CastleFilesUtils,
  GameItemsDatabase, GameVinculopedia, GameMonstersDatabase,
  GameBlueprintsDatabase, GameBodyPresetsDatabase, GameSounds,
  GameLog;

{ Callback fires after every package.xml found }
procedure FoundPackage(const FileInfo: TFileInfo; Data: Pointer; var StopSearch: Boolean);
begin
  Mods.Add(TMod.Read(FileInfo.Url));
end;

procedure InitModsDatabase;
begin
  Mods := TModList.Create(true);
  { Looking for mods in game data folder }
  FindFiles('castle-data:/package.xml', false, @FoundPackage, nil, [ffRecursive]);
  { Looking for mods in persistent data folder (configuration folder)
    important on Android or other builds where data folder is not user-writeable }
  FindFiles(ApplicationConfig('package.xml'), false, @FoundPackage, nil, [ffRecursive]);
end;

procedure FreeModsDatabase;
begin
  FreeAndNil(Mods);
end;

procedure TMod.Load;
var
  J: Integer;
begin
  LogNormal('Loading package: %s', [Url]);
  for J := 0 to Pred(SubPackages.Count) do
    SubPackages[J].Load;
  for J := 0 to Pred(Sounds.Count) do
    LoadSounds(Sounds[J]);
  for J := 0 to Pred(Bodyparts.Count) do
    LoadBodypartsData(Bodyparts[J]);
  for J := 0 to Pred(Items.Count) do
    LoadItemsData(Items[J]);
  for J := 0 to Pred(Blueprints.Count) do
    LoadBlueprints(Blueprints[J]); // after items/bodyparts
  for J := 0 to Pred(Presets.Count) do
    LoadBodyPresets(Presets[J]);
  for J := 0 to Pred(Monsters.Count) do
    LoadMonstersData(Monsters[J]);
  for J := 0 to Pred(Vinculopedia.Count) do
    LoadVinculopedia(Vinculopedia[J]);
end;

constructor TMod.Read(const AUrl: String);
var
  Path: String;
  Doc: TXMLDocument;
  Root: TDOMElement;
  Iterator: TXMLElementIterator;

  procedure ReadSubPackages;
  begin
    SubPackages := TModList.Create;
    Iterator := Doc.DocumentElement.ChildrenIterator('Package');
    try
      while Iterator.GetNext do
        SubPackages.Add(TMod.Read(CombineURI(Path, Iterator.Current.AttributeString('Url'))));
    finally FreeAndNil(Iterator) end;
  end;

  function ReadList(const ListName: String): TStringList;
  begin
    Result := TStringList.Create;
    Iterator := Doc.DocumentElement.ChildrenIterator(ListName);
    try
      while Iterator.GetNext do
        Result.Add(CombineURI(Path, Iterator.Current.AttributeString('Url')));
    finally FreeAndNil(Iterator) end;
  end;

begin
  LogNormal('Found package: %s', [AUrl]);
  Url := AUrl;
  Path := ExtractUriPath(Url);
  Doc := URLReadXML(AUrl);

  Root := Doc.DocumentElement;
  Id := Root.AttributeString('Id');
  Dependencies := CreateTokens(Root.AttributeStringDef('Dependencies', ''), [',']);
  BaseGame := Root.AttributeBooleanDef('BaseGame', false);
  try
    ReadSubPackages;
    Sounds := ReadList('Sounds');
    Items := ReadList('Items');
    Bodyparts := ReadList('Bodyparts');
    Blueprints := ReadList('Blueprints');
    Presets := ReadList('Presets');
    Monsters := ReadList('Monsters');
    Vinculopedia := ReadList('Vinculopedia');
  finally FreeAndNil(Doc) end;
end;

destructor TMod.Destroy;
begin
  FreeAndNil(Sounds);
  FreeAndNil(Blueprints);
  FreeAndNil(Presets);
  FreeAndNil(Items);
  FreeAndNil(Bodyparts);
  FreeAndNil(Monsters);
  FreeAndNil(Vinculopedia);
  FreeAndNil(Dependencies);
  FreeAndNil(SubPackages);
  inherited Destroy;
end;

end.


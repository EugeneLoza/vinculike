{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Load game data packages (mods) }
unit GameLoadData;

{$INCLUDE compilerconfig.inc}

interface

procedure LoadGameData;
procedure FreeGameData;
implementation
uses
  SysUtils,
  GameItemsDatabase, GameTranslation, GameVinculopedia, GameMonstersDatabase,
  GameBlueprintsDatabase, GameTilesetMap, GameTileset, GameSounds,
  GameModsDatabase, GameBodyPresetsDatabase,
  GameLog;

procedure LoadGameData;
var
  I: Integer;
begin
  InitModsDatabase;

  ClearSounds;
  ClearItemsAndBodypartsData;
  ClearBlueprints;
  ClearBodyPresets;
  ClearMonstersData;
  ClearVinculopedia;

  // todo: sort mods by load order
  for I := 0 to Pred(Mods.Count) do
    Mods[I].Load;

  LoadTilesets;
  LoadTranslation;

  // validation and post-processing

  Vinculopedia.Validate;

  LogNormal('Loaded items and bodyparts total: %d', [ItemsDataDictionary.Count]);
  LogNormal('Loaded items: %d', [ItemsDataList.Count]);
  LogNormal('Loaded bodyparts: %d', [ItemsDataDictionary.Count - ItemsDataList.Count]);
  LogNormal('Loaded monsters and objects total: %d', [MonstersDataDictionary.Count]);
  LogNormal('Loaded chests: %d', [ChestsData.Count]);
  LogNormal('Loaded mimics: %d', [MimicsData.Count]);
  LogNormal('Loaded traps: %d', [TrapsData.Count]);
  LogNormal('Loaded guards: %d', [GuardsData.Count]);
  LogNormal('Loaded vacuum cleaners: %d', [VacuumCleanersData.Count]);
  LogNormal('Loaded monsters: %d', [MonstersData.Count]);
  LogNormal('Loaded blueprints: %d', [BlueprintsDictionary.Count]);
  LogNormal('Loaded body presets: %d', [BodyPresets.Count - 1]); //-1 because one is initially Random
  LogNormal('Loaded tilesets: %d', [AllTilesets.Count]);
end;

procedure FreeGameData;
begin
  FreeModsDatabase;
  FreeVinculopedia;
  FreeTranslation;
  FreeMonstersData;
  FreeBlueprints;
  FreeItemsData;
  FreeTilesetMap;
  FreeTilesets;
  FreeSounds;
end;

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Global handling of the fonts }
unit GameFonts;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  CastleFonts;

var
  FontSonianoNumbers20: TCastleFont;
  FontSonianoNumbers30: TCastleFont;
  FontSoniano16: TCastleFont;
  FontSoniano50: TCastleFont;
  FontSoniano90: TCastleFont;
  FontBender20: TCastleFont;
  FontBender40: TCastleFont;
  FontBender90: TCastleFont;
  //FontBenderBold90: TCastleFont;
  FontBenderBold150: TCastleFont;

procedure LoadFonts;
procedure FreeFonts;
implementation
uses
  CastleStringUtils, CastleUnicode,
  GameMath;

type
  TNumericFont = class(TCastleFont)
  protected
    procedure Measure(out ARowHeight, ARowHeightBase, ADescend: Single); override;
  end;

procedure TNumericFont.Measure(out ARowHeight, ARowHeightBase, ADescend: Single);
begin
  //inherited Measure(ARowHeight, ARowHeightBase, ADescend);
  ARowHeight := TextHeight('1234567890.-');
  ARowHeightBase := TextHeightBase('1234567890.-');
  ADescend := MaxSingle(0, TextHeight('.') - TextHeight('0'));
end;

procedure LoadFonts;
var
  CharList: TUnicodeCharList;
begin
  CharList := TUnicodeCharList.Create;
  CharList.Add(SimpleAsciiCharacters);
  CharList.Add('∀');
  //CharList.Add('∞');
  // TODO: Characters
  FontSonianoNumbers20 := TNumericFont.Create('castle-data:/ui/fonts/soniano/soniano-sans-unicode.regular_fixed.ttf', 20, true, ['0','1','2','3','4','5','6','7','8','9','.','-',':']);
  FontSonianoNumbers30 := TNumericFont.Create('castle-data:/ui/fonts/soniano/soniano-sans-unicode.regular_fixed.ttf', 30, true, ['0','1','2','3','4','5','6','7','8','9','.','-',':']);
  FontSoniano16 := TCastleFont.Create('castle-data:/ui/fonts/soniano/soniano-sans-unicode.regular_fixed.ttf', 16, true, CharList);
  FontSoniano50 := TCastleFont.Create('castle-data:/ui/fonts/soniano/soniano-sans-unicode.regular_fixed.ttf', 50, true, CharList);
  FontSoniano90 := TCastleFont.Create('castle-data:/ui/fonts/soniano/soniano-sans-unicode.regular_fixed.ttf', 90, true, CharList);
  FontBender20 := TCastleFont.Create('castle-data:/ui/fonts/bender/bender.regular_fixed.ttf', 20, true, CharList);
  FontBender40 := TCastleFont.Create('castle-data:/ui/fonts/bender/bender.regular_fixed.ttf', 40, true, CharList);
  FontBender90 := TCastleFont.Create('castle-data:/ui/fonts/bender/bender.regular_fixed.ttf', 90, true, CharList);
  //FontBenderBold90 := TCastleFont.Create('castle-data:/ui/fonts/bender/bender.bold_fixed.ttf', 90, true, CharList);
  FontBenderBold150 := TCastleFont.Create('castle-data:/ui/fonts/bender/bender.bold_fixed.ttf', 150, true, CharList);
  FreeAndNil(CharList);
end;

procedure FreeFonts;
begin
  { WARNING: We have a serious bug here
    I'm not sure if it's possible to fix it, but if we free the fonts now,
    due to finalization order we can still sometimes try to access them
    from ShowLog(...) which will crash with access violation
    Therefore we must FreeAndNil them, it doesn't seem like this has
    any obvious negative consequences, but this is a workaround for
    undefined finalization order, not a proper solution TODO }
  FreeAndNil(FontSonianoNumbers20);
  FreeAndNil(FontSonianoNumbers30);
  FreeAndNil(FontSoniano16);
  FreeAndNil(FontSoniano50);
  FreeAndNil(FontSoniano90);
  FreeAndNil(FontBender20);
  FreeAndNil(FontBender40);
  FreeAndNil(FontBender90);
  //FreeAndNil(FontBenderBold90);
  FreeAndNil(FontBenderBold150);
end;

end.


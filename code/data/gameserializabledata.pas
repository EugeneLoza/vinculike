{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameSerializableData;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, Classes, DOM,
  GameValidated;

type
  EDataValidationError = class(Exception);
  EClassDeserealizationError = class(Exception);

type
  TSerializableData = class abstract(TObject, IValidated)
  protected
    FBaseUrl: String;
    procedure Validate; virtual; abstract;
    procedure Read(const Element: TDOMElement); virtual; abstract;
  public
    class function ReadClass(const BaseUrl: String; const Element: TDOMElement): TSerializableData;
    // WARNING: Constructor is not virtual; Children should not create constructors
  end;
  TSerializableDataClass = class of TSerializableData;

procedure RegisterSerializableData(const AClass: TSerializableDataClass);
function SerializableDataByName(const AClassName: String): TSerializableDataClass;

procedure FreeSerializableData;
implementation
uses
  Generics.Collections,
  CastleXmlUtils;

type
  TSerializableDataClassDictionary = specialize TDictionary<String, TSerializableDataClass>;

var
  SerializableDataClassDictionary: TSerializableDataClassDictionary;

procedure RegisterSerializableData(const AClass: TSerializableDataClass);
begin
  if SerializableDataClassDictionary = nil then // NOTE: we can't guess in which order initialization will requrest RegisterSerializableData, so the only reliable way is to create dictionary at first request
    SerializableDataClassDictionary := TSerializableDataClassDictionary.Create;
  SerializableDataClassDictionary.Add(AClass.ClassName, AClass);
end;

function SerializableDataByName(const AClassName: String): TSerializableDataClass;
begin
  Exit(SerializableDataClassDictionary[AClassName]);
end;

class function TSerializableData.ReadClass(const BaseUrl: String;
  const Element: TDOMElement): TSerializableData;
var
  ClassString: String;
begin
  Result := nil; // sometimes can be not nil, especially due to an exception
  if Element = nil then
    Exit(nil);
  try
    ClassString := Element.AttributeString('Class');
    if SerializableDataClassDictionary.ContainsKey(ClassString) then
      Result := SerializableDataClassDictionary[ClassString].Create
    else
      raise EClassDeserealizationError.CreateFmt('Could not find class name in serializable data: %s', [ClassString]);
    Result.FBaseUrl := BaseUrl;
    Result.Read(Element);
    {$IFDEF ValidateData}
    Result.Validate;
    {$ENDIF}
  except
    FreeAndNil(Result);
    raise;
  end;
end;

procedure FreeSerializableData;
begin
  FreeAndNil(SerializableDataClassDictionary);
end;

end.


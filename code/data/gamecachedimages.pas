{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Contains a cache of images used by all entities in game to avoid loading the same assets several
  Also contains some universal images that are used throughout the game }
unit GameCachedImages;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  CastleGlImages, CastleImages, CastleShiftedImage,
  GameApparelSlots;

var
  { overlay for captured characters }
  TempPlayerBinds: TDrawableImage;
  { Sprite of exit to next level }
  TempExit: TDrawableImage;
  { Sprite of a monster that cannot be seen, but can be heard }
  TempEcho: TDrawableImage;
  { Debug images }
  DebugColliderImage: TDrawableImage;
  DebugPlayerOriginImage: TDrawableImage;
  DebugMonsterOriginImage: TDrawableImage;
  DebugExtremaImage: TDrawableImage;
  { Two concentric circles that show monster detection range }
  StealthMark: TDrawableImage;
  { Highlight frame around currently selected character }
  SelectedCharacterHighlight: TCastleShiftedImage;
  { Images for inventory slots }
  SlotImage: array [TApparelSlot] of TDrawableImage;
  { Displayed when there are too many status effects to display them all at once in the gameplay UI }
  StatusEffectsOverflowIcon: TDrawableImage;
  { Icon for unidentified enchantment }
  StatusEffectUnidentified: TDrawableImage;
  { A single leash segment drawn on the map }
  LeashMap: TDrawableImage;

{ Load a "shifted" image or return one from cache }
function LoadShiftedImage(const Url: String): TCastleShiftedImage;
{ Load a regular RGB+Alpha image }
function LoadRgba(const Url: String): TRgbAlphaImage;
{ Load a fully opaque image }
function LoadRgb(const Url: String): TRgbImage;
{ Load a RGBA image used to directly render images on screen }
function LoadDrawable(const Url: String): TDrawableImage;

{ Initialize images cached and load universal images }
procedure LoadCachedImages;
{ Free everything related to images }
procedure FreeCachedImages;
implementation
uses
  Generics.Collections,
  CastleVectors;

type
  TShiftedImagesCache = specialize TObjectDictionary<String, TCastleShiftedImage>;
  TRgbaImagesCache = specialize TObjectDictionary<String, TRgbAlphaImage>;
  TRgbImagesCache = specialize TObjectDictionary<String, TRgbImage>;
  TDrawableImagesCache = specialize TObjectDictionary<String, TDrawableImage>;
var
  FShiftedImagesCache: TShiftedImagesCache;
  FRgbaImagesCache: TRgbaImagesCache;
  FRgbImagesCache: TRgbImagesCache;
  FDrawableImagesCache: TDrawableImagesCache;

procedure LoadCachedImages;
begin
  FRgbaImagesCache := TRgbaImagesCache.Create([doOwnsValues]);
  FShiftedImagesCache := TShiftedImagesCache.Create([doOwnsValues]);
  FRgbImagesCache := TRgbImagesCache.Create([doOwnsValues]);
  FDrawableImagesCache := TDrawableImagesCache.Create([doOwnsValues]);

  TempPlayerBinds := LoadDrawable('castle-data:/core/light-thorny-triskelion_CC-BY_by_Lorc.png');
  StealthMark := LoadDrawable('castle-data:/core/stealth_mark.png');
  StealthMark.Color := Vector4(1, 1, 1, 0.03);
  TempExit := LoadDrawable('castle-data:/core/enter.png');
  TempEcho := LoadDrawable('castle-data:/core/question.png');

  DebugColliderImage := LoadDrawable('castle-data:/core/debug/debugcollider.png');
  DebugColliderImage.Color := Vector4(1, 1, 1, 0.1);
  DebugPlayerOriginImage := LoadDrawable('castle-data:/core/debug/debugplayerorigin.png');
  DebugMonsterOriginImage := LoadDrawable('castle-data:/core/debug/debugmonsterorigin.png');
  DebugExtremaImage := LoadDrawable('castle-data:/core/debug/debugextrema.png');

  SlotImage[esWeapon] := LoadDrawable('castle-data:/core/apparel_slots/weapon.png');
  SlotImage[esBottomOver] := LoadDrawable('castle-data:/core/apparel_slots/bottom-over.png');
  SlotImage[esBottomUnder] := LoadDrawable('castle-data:/core/apparel_slots/bottom-under.png');
  SlotImage[esTopOver] := LoadDrawable('castle-data:/core/apparel_slots/top-over.png');
  SlotImage[esTopOverOver] := LoadDrawable('castle-data:/core/apparel_slots/top-overover.png');
  SlotImage[esTopUnder] := LoadDrawable('castle-data:/core/apparel_slots/top-under.png');
  SlotImage[esAmulet] := LoadDrawable('castle-data:/core/apparel_slots/amulet.png');
  SlotImage[esNeck] := LoadDrawable('castle-data:/core/apparel_slots/collar.png');
  SlotImage[esLeash] := LoadDrawable('castle-data:/core/apparel_slots/leash_slot.png');
  SlotImage[esGag] := LoadDrawable('castle-data:/core/apparel_slots/gag_slot.png');
  SlotImage[esGlasses] := LoadDrawable('castle-data:/core/apparel_slots/glasses.png');
  SlotImage[esFeet] := LoadDrawable('castle-data:/core/apparel_slots/feet.png');
  StatusEffectsOverflowIcon := LoadDrawable('castle-data:/core/enchantments_icons/enchantments_overflow.png');
  StatusEffectUnidentified := LoadDrawable('castle-data:/core/enchantments_icons/enchantment_unidentified.png');

  LeashMap := LoadDrawable('castle-data:/core/leash.png');

  SelectedCharacterHighlight := LoadShiftedImage('castle-data:/core/selected.png');
end;

function LoadShiftedImage(const Url: String): TCastleShiftedImage;
var
  Img: TCastleShiftedImage;
begin
  if not FShiftedImagesCache.ContainsKey(Url) then
  begin
    Img := TCastleShiftedImage.Create;
    Img.Load(Url);
    FShiftedImagesCache.Add(Url, Img);
  end;
  Exit(FShiftedImagesCache[Url]);
end;

function LoadRgba(const Url: String): TRgbAlphaImage;
begin
  if not FRgbaImagesCache.ContainsKey(Url) then
    FRgbaImagesCache.Add(Url, LoadImage(Url, [TRgbAlphaImage]) as TRgbAlphaImage);
  Exit(FRgbaImagesCache[Url]);
end;

function LoadRgb(const Url: String): TRgbImage;
begin
  if not FRgbImagesCache.ContainsKey(Url) then
    FRgbImagesCache.Add(Url, LoadImage(Url, [TRgbImage]) as TRgbImage);
  Exit(FRgbImagesCache[Url]);
end;

function LoadDrawable(const Url: String): TDrawableImage;
begin
  if not FDrawableImagesCache.ContainsKey(Url) then
    FDrawableImagesCache.Add(Url, TDrawableImage.Create(Url, true));
  Exit(FDrawableImagesCache[Url]);
end;

procedure FreeCachedImages;
begin
  FreeAndNil(FShiftedImagesCache);
  FreeAndNil(FRgbaImagesCache);
  FreeAndNil(FRgbImagesCache);
  FreeAndNil(FDrawableImagesCache);
end;

end.


{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ A centralized place to store all user configuration }
unit GameConfiguration;

{$INCLUDE compilerconfig.inc}

interface

type
  { Container for configuration values
    and methods to handle those, save/load in UserConfig first of all }
  TConfiguration = class(TObject)
  public const
    { Defaults for some parameters }
    { Default master volume }
    DefaultVolume = 0.5;
    { Default click margin. See in-game Options screen for details
      Note that defaults are different on Desktop and Mobile }
    DefaultClickMargin = Single({$IFDEF Mobile}0.07{$ELSE}0.02{$ENDIF});
    { Default account of lines visible in the log}
    DefaultLogLength = 12;
  public
    { It's the app full screen? }
    FullScreen: Boolean;
    { Master Volume }
    MasterVolume: Single;
    { Volume of the soundtrack}
    MusicVolume: Single;
    { Request to limit FPS to a lower value than max possible on the hardware
      note: it doesn't affect Android build for now }
    LimitFps: Integer;
    { If congruent warning should be displayed before the Main
      Default behavior: show once and never show again }
    AskContentWarning, AlwaysAskContentWarning: Boolean;
    { Click margin for most actions. See description in Options}
    ClickMargin: Single;
    { Max number of log lines displayed on screen}
    LogLength: Integer;
    { Use screen effects shaders }
    ScreenEffects: Boolean;
    { If screen shaking effects are enabled}
    ShakeScreen: Boolean;
    { if vibration is enabled on Android }
    Vibration: Boolean;
    { Show real time clock on screen}
    ShowClock: Boolean;
    { Show FPS counter on screen}
    ShowFps: Boolean;
    { currently set difficulty level}
    DifficultyLevel: Integer;
    { Global values of censorship status
      individual characters can additionally ask to be censored}
    Censored, CensoredScreenshots: Boolean;
    { Show detection circles around the monsters
      (from how far away they can detect the Current Character) }
    VisualizeNoise: Boolean;
    { if Party UI is active in game }
    PartyActive: Boolean;
    { if pause UI is active in game }
    PauseMenuActive: Boolean;
    { Load configuration from UserConfig}
    procedure Load;
    // Assigns proper values to CastleUserConfig, but doesn't call UserConfig.Save
    procedure PrepareToSave;
    { Request saving UserConfig}
    procedure Save;
    { Reset all settings to default}
    procedure Reset;
    constructor Create; // override;
  end;

var
  { Stores all game configuration }
  Configuration: TConfiguration;

{ Initialize game configuration }
procedure LoadGameConfiguration;
{ Release all game configuration
  note: doesn't call "save", make sure to call it manually in appropriate moment}
procedure FreeGameConfiguration;
implementation
uses
  SysUtils,
  CastleConfig,
  GameDifficultyLevel;

procedure LoadGameConfiguration;
begin
  if Configuration = nil then
  begin
    UserConfig.Load;
    Configuration := TConfiguration.Create;
  end;
  Configuration.Load;
end;

procedure FreeGameConfiguration;
begin
  // we do not save it here
  FreeAndNil(Configuration);
end;

procedure TConfiguration.Load;
begin
  // note, the values are already reset to default, so we try get them and leave at default if not found
  FullScreen := UserConfig.GetValue('fullscreen', FullScreen);
  MasterVolume := UserConfig.GetFloat('master_volume', MasterVolume);
  MusicVolume := UserConfig.GetFloat('music_volume', MusicVolume);
  LimitFps := UserConfig.GetInteger('limit_fps', LimitFps);
  AskContentWarning := UserConfig.GetValue('ask_content_warning', AskContentWarning);
  AlwaysAskContentWarning := UserConfig.GetValue('always_ask_content_warning', AlwaysAskContentWarning);
  ClickMargin := UserConfig.GetFloat('click_margin', ClickMargin);
  LogLength := UserConfig.GetInteger('log_length', LogLength);
  ScreenEffects := UserConfig.GetValue('screen_effects', ScreenEffects);
  ShakeScreen := UserConfig.GetValue('shake_screen', ShakeScreen);
  Vibration := UserConfig.GetValue('vibration', Vibration);
  ShowClock := UserConfig.GetValue('show_clock', ShowClock);
  ShowFps := UserConfig.GetValue('show_fps', ShowFps);
  DifficultyLevel := UserConfig.GetInteger('difficulty_level', DifficultyLevel);
  Censored := UserConfig.GetValue('censored', Censored);
  CensoredScreenshots := UserConfig.GetValue('censored_screenshots', CensoredScreenshots);
  VisualizeNoise := UserConfig.GetValue('VisualizeNoise', VisualizeNoise);
  PartyActive := UserConfig.GetValue('PartyActive', PartyActive);
  PauseMenuActive := UserConfig.GetValue('PauseMenuActive', PauseMenuActive);
end;

procedure TConfiguration.PrepareToSave;
begin
  UserConfig.SetValue('fullscreen', FullScreen);
  UserConfig.SetFloat('master_volume', MasterVolume);
  UserConfig.SetFloat('music_volume', MusicVolume);
  UserConfig.SetValue('limit_fps', LimitFps);
  UserConfig.SetValue('ask_content_warning', AskContentWarning);
  UserConfig.SetValue('always_ask_content_warning', AlwaysAskContentWarning);
  UserConfig.SetFloat('click_margin', ClickMargin);
  UserConfig.SetValue('log_length', LogLength);
  UserConfig.SetValue('screen_effects', ScreenEffects);
  UserConfig.SetValue('shake_screen', ShakeScreen);
  UserConfig.SetValue('vibration', Vibration);
  UserConfig.SetValue('show_clock', ShowClock);
  UserConfig.SetValue('show_fps', ShowFps);
  UserConfig.SetValue('difficulty_level', DifficultyLevel);
  UserConfig.SetValue('censored', Censored);
  UserConfig.SetValue('censored_screenshots', CensoredScreenshots);
  UserConfig.SetValue('VisualizeNoise', VisualizeNoise);
  UserConfig.SetValue('PauseMenuActive', PauseMenuActive);
  UserConfig.SetValue('PartyActive', PartyActive);
end;

procedure TConfiguration.Save;
begin
  PrepareToSave;
  UserConfig.Save;
end;

procedure TConfiguration.Reset;
begin
  FullScreen := true;
  MasterVolume := DefaultVolume;
  MusicVolume := 1.0;
  LimitFps := 60;
  AskContentWarning := true;
  AlwaysAskContentWarning := false;
  ClickMargin := DefaultClickMargin;
  LogLength := DefaultLogLength;
  ScreenEffects := true;
  ShakeScreen := true;
  Vibration := true;
  ShowClock := false;
  ShowFps := false;
  DifficultyLevel := DefaultDifficultyLevel; // WARNING: when initializing config this is not known yet (-1)! Will be reset properly in LoadDifficultyLevels
  Censored := false;
  CensoredScreenshots := false;
  VisualizeNoise := false;
  PartyActive := true;
  PauseMenuActive := true;
end;

constructor TConfiguration.Create;
begin
  Reset;
end;

end.


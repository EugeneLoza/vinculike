{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Global handler for save game and related functions }
unit GameSaveGame;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM;

const
  SaveGameVersion = Integer(3);

procedure SaveGame;
procedure LoadGame;
function SaveGameExists: Boolean;
function SaveGameHasCurrentCharacter: Boolean;
procedure SaveGameResetCurrentCharacter;
function SaveGameCompatible: Boolean;
procedure DeleteSaveGame;
function SecondsSinceLastSave: Single;
function SaveAsString: String;

implementation
uses
  CastleConfig, CastleXmlUtils, CastleTimeUtils,
  GameSerializableObject,
  GameMap, GamePlayerCharacter, GameLog, GameConfiguration,
  GameViewGame, GameStats, GameDifficultyLevel;

type
  ELoadGameFailed = class(Exception);

var
  LastSaveTimer: TTimerResult;

procedure SaveGame;
var
  Element: TDOMElement;
  T: TTimerResult;
begin
  if not ViewGame.Active then
  begin
    LogWarning('Cannot save the game, it is not started');
    Exit;
  end;
  LogNormal('Saving the game started');

  T := Timer;
  UserConfig.DeletePath('save_game');
  Element := UserConfig.MakePathElement('save_game');

  Configuration.PrepareToSave;

  Element.AttributeSet('version', SaveGameVersion);
  Element.AttributeSet('GlobalId', GlobalId);
  Element.AttributeSet('CurrentCharacter', ViewGame.CurrentCharacter.ReferenceId);
  try
    Map.Save(Element);
  except
    on E: Exception do
    begin
      LogWarning('EXCEPTION %s : %s', [E.ClassName, E.Message]);
      LogWarning('THERE WAS AN UNHANDLED EXCEPTION SAVING THE MAP.');
      LogWarning('NOTHING CAN BE DONE HERE, WE JUST CRASH TO PREVENT FROM OVERWRITING THE SAVE WITH FAULTY DATA.');
      raise;
    end;
  end;

  LocalStats.Save(Element);

  UserConfig.DeletePath('global_stats');
  GlobalStats.Save(UserConfig.MakePathElement('global_stats'));

  UserConfig.MarkModified;
  UserConfig.Save;
  LogNormal('Game saved in %.1n s.', [T.ElapsedTime]);
  LastSaveTimer := Timer;
end;

procedure LoadGame;
var
  Element: TDOMElement;

  procedure RecoverFromCurrentCharacterError;
  var
    P: TPlayerCharacter;
    DeepestLevel: Byte;
    NewDungeonLevel: Byte;
  begin
    for P in Map.PlayerCharactersList do
      if not P.IsCaptured and (P.AtMap <> 0) then
      begin
        ShowError('%s is set as captured at dungeon level %d.', [P.Data.DisplayName, P.AtMap]);
        P.GetCapturedSafe;
      end;
    for P in Map.PlayerCharactersList do
      if P.IsCaptured and (Map.CapturedCharacterOnLevel(P.AtMap) > Difficulty.MaxPrisonersAtLevel(P.AtMap)) then
      begin
        NewDungeonLevel := P.AtMap;
        while (Map.CapturedCharacterOnLevel(NewDungeonLevel) >= Difficulty.MaxPrisonersAtLevel(NewDungeonLevel)) and (NewDungeonLevel > 0) do
          Dec(NewDungeonLevel);
        if (NewDungeonLevel <> P.AtMap) then // Note: NewDungeonLevel = 0 is valid here, we'll fix those later
        begin
          P.AtMap := NewDungeonLevel;
          if NewDungeonLevel > 0 then
            ShowError('Moved %s to dungeon level %d', [P.Data.DisplayName, P.AtMap]);
        end;
      end;
    DeepestLevel := Map.DeepestLevel;
    for P in Map.PlayerCharactersList do
      if (P.IsCaptured) and (P.AtMap = 0) then
      begin
        Inc(DeepestLevel);
        P.AtMap := DeepestLevel;
        ShowError('Moved %s to dungeon level %d', [P.Data.DisplayName, P.AtMap]);
      end;

    ShowError('We''ll start a new run and hope for the best...');
    ViewGame.StartNewRun;
  end;

begin
  Element := UserConfig.PathElement('save_game', true);

  if not SaveGameCompatible then
    raise ELoadGameFailed.CreateFmt('Cannot load incompatible save game version: %d. Expected: %d', [Element.AttributeIntegerDef('version', 0), SaveGameVersion]);

  GlobalId := Element.AttributeQWord('GlobalId');
  LocalStats.Load(Element.ChildElement(LocalStats.ClassName, false)); // TODO: true ; TODO: We must load this _before_ map.load because map adds equip events when loading player characters, and will do so every load game - if we "discard those" by loading stats later: it'll show unlock events every load
  Map.Load(Element.ChildElement(TMap.Signature, true));
  try
    ViewGame.CurrentCharacter := ObjectByReferenceId(Element.AttributeQWord('CurrentCharacter')) as TPlayerCharacter;
  except
    on E: Exception do
    begin
      ShowError('%s:"%s". Cannot read current character.', [E.ClassName, E.Message]);
      RecoverFromCurrentCharacterError;
      //raise ELoadGameFailed.Create('Error while loading CurrentCharacter');
    end;
  end;
  LastSaveTimer := Timer;
end;

function SaveGameExists: Boolean;
var
  Element: TDOMElement;
begin
  Element := UserConfig.PathElement('save_game', false);
  Result := (Element <> nil) and (Element.FirstChild <> nil) and SaveGameCompatible;
end;

function SaveGameHasCurrentCharacter: Boolean;
begin
  Exit(SaveGameExists and (UserConfig.PathElement('save_game', true).AttributeQWord('CurrentCharacter') <> 0));
end;

procedure SaveGameResetCurrentCharacter;
begin
  UserConfig.PathElement('save_game', true).AttributeSet('CurrentCharacter', 0);
end;

function SaveGameCompatible: Boolean;
var
  Element: TDOMElement;
begin
  Element := UserConfig.PathElement('save_game', false);
  if Element <> nil then
    Result := Element.AttributeIntegerDef('version', 0) = SaveGameVersion
  else
    Result := true;
end;

procedure DeleteSaveGame;
begin
  UserConfig.DeletePath('save_game');
  UserConfig.Save;
end;

function SecondsSinceLastSave: Single;
begin
  Result := LastSaveTimer.ElapsedTime;
end;

function SaveAsString: String;
begin
  Exit(UserConfig.SaveToString);
end;

end.


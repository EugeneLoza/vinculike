{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Load tiles and metadata to be used in visual map generation }
unit GameTileset;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM, Generics.Collections,
  CastleImages;

type
  TCastleImageList = specialize TObjectList<TCastleImage>;

type
  { A tileset that consists of several images for walls and floor tiles
    maybe: keep walls/floors separate so that they can be mixed
    will hopefully become clear how to handle those after implementing the new map generation algorithm
    most likely each meta-tile of the dungeon will have a list of "supported floors/walls"
    maybe even without any further need for this class: but each map will have a set of rules that define those
    (from macros? per-meta-tile will be tedious) }
  TTileset = class(TObject)
  public
    { Brighten/darken the loaded wall/floor images
      may be different for walks and floor, but equal within the group }
    WallLightness: Single;
    FloorLightness: Single;
    { Apply blur to the corresponding images }
    WallBlur: Single;
    FloorBlur: Single;
    { images for walls and floors }
    Wall: TCastleImageList;
    Floor: TCastleImageList;
    constructor Create(const Element: TDOMElement);
    destructor Destroy; override;
  end;
  TTilesetsList = specialize TObjectList<TTileset>;

var
  { all loaded tilesets }
  AllTilesets: TTilesetsList;

{ initialize and load tilesets
 Todo: from url }
procedure LoadTilesets;
{ Free all data related to tilesets }
procedure FreeTilesets;

implementation
uses
  CastleXMLUtils, CastleUriUtils,
  GameLog, GameCachedImages;

procedure LoadTilesets;
var
  Tileset: TTileset;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  AllTilesets := TTilesetsList.Create(true);
  Doc := URLReadXML(CombineURI('castle-data:/', 'map/tileset/tilesets.xml'));
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('Tileset');
    try
      while Iterator.GetNext do
      begin
        Tileset := TTileset.Create(Iterator.Current);
        AllTilesets.Add(Tileset);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;

constructor TTileset.Create(const Element: TDOMElement);

  function ReadSet(const Signature: String): TCastleImageList;
  var
    Img: TCastleImage;
    Iterator: TXMLElementIterator;
  begin
    Result := TCastleImageList.Create(false);
    Iterator := Element.ChildrenIterator(Signature);
    try
      while Iterator.GetNext do
      begin
        // TODO: RGBA? not RGB?
        Img := LoadRgba('castle-data:/map/tileset/' + Iterator.Current.AttributeString('Url'));
        Result.Add(Img);
      end;
    finally FreeAndNil(Iterator) end;
  end;

begin
  WallLightness := Element.AttributeSingleDef('WallLightness', 1.0);
  FloorLightness := Element.AttributeSingleDef('FloorLightness', 1.0);
  WallBlur := Element.AttributeSingleDef('WallBlur', -1.0);
  FloorBlur := Element.AttributeSingleDef('FloorBlur', -1.0);
  Floor := ReadSet('Floor');
  Wall := ReadSet('Wall');
end;

destructor TTileset.Destroy;
begin
  FreeAndNil(Wall);
  FreeAndNil(Floor);
  inherited Destroy;
end;

procedure FreeTilesets;
begin
  FreeAndNil(AllTilesets);
end;

end.


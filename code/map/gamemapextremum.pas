{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ A point on the map corresponding to deadend or corner }
unit GameMapExtremum;

{$INCLUDE compilerconfig.inc}

interface

uses
  Classes,
  Generics.Collections, DOM,
  GameSimpleSerializableObject;

type
  { Coordinates and metadata (last checked by a curious monster) }
  TExtremum = class(TSimpleSerializableObject)
  public
    procedure Save(const Element: TDOMElement); override;
  protected
    procedure Load(const Element: TDOMElement); override;
  public
    X: Int16;
    Y: Int16;
    { Last "dungeon level time" when this point was visited by a curious monster
      note: this value is set when the monster decides to go
      not when reaches the destination - though it shouldn't matter
      the idea is that curious monsters will not visit some spots too often
      neither too rarely due to random.
      note2 "visible/invisible" status of the point also plays a role
      when the decision is made }
    LastChecked: Single;
  end;
  TExtremaList = specialize TObjectList<TExtremum>;

implementation
uses
  CastleXmlUtils;

procedure TExtremum.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('X', X);
  Element.AttributeSet('Y', Y);
  Element.AttributeSet('LastChecked', LastChecked);
end;

procedure TExtremum.Load(const Element: TDOMElement);
begin
  inherited Load(Element);
  X := Element.AttributeInteger('X');
  Y := Element.AttributeInteger('Y');
  LastChecked := Element.AttributeSingle('LastChecked');
end;

initialization
  RegisterSimpleSerializableObject(TExtremum);

end.


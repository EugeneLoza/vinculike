{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Renders the map and processes clicks }
{ todo: rework into X3D nodes graph to avoid memory reallocation,
  allow rotation of the map, deformation animations, etc.
  it's straightforward, but will take time to adapt everything }
unit GameMapInterface;

{$INCLUDE compilerconfig.inc}
//{$DEFINE BlindfoldHidesMonsters}

interface

uses
  SysUtils, Classes, {Generics.Collections,}
  CastleUIControls, CastleRectangles, CastleKeysMouse, CastleVectors, CastleTimeUtils,
  GameMap;

const
  { Zoom level at which tall walls are rendered flat
    (i.e. render 1 tile instead of 5)}
  SwitchToMapModeZoom = 0.3;

type
  { Map click callback }
  TMapClick = procedure (const ClickX, ClickY: Single) of object;

type
  { Finger press/mouse click
    for now we process only one finger }
  TTouch = record
    Start: TTimerResult;
    Position: TVector2;
  end;
  TTouches = array[0..10] of TTouch; //specialize TDictionary<TFingerIndex, TTouch>; --- yes, it's glitchy as hell: Internal error 200510032

type
  { User interface that renders the map, all entities in it
    and catches user clicks }
  TMapInterface = class(TCastleUserInterface)
  strict private
    { Currently active pressed fingers }
    Touches: TTouches;
    { Memory allocated for rendering purposes, for optimization }
    ScreenRects, ImageRects: array of TFloatRectangle;
    { Coordinates at which the map is centered}
    CenterX, CenterY: Single;
    { If drag has started (in progress)}
    DragStarted: Boolean;
    { Coordinates of drag start }
    DragStart: TVector2;
    DragCenterStartX: Single;
    DragCenterStartY: Single;
  public
    { Visualize additional debug entities}
    DebugNavgrid: Integer;
    DebugPlayerOrigins: Boolean;
    DebugMonsterOrigins: Boolean;
    DebugExtrema: Boolean;
  public
    { Current zoom level }
    Zoom: Single;
    TargetX, TargetY: Single;
    GameplayAreaWidth: Single;
    { Callbacks for map interactions events }
    OnMapClick: TMapClick;
    OnMapLongClick: TMapClick;
    OnMapSwipe: TMapClick;
    { Teleport map center to ax,ay without animation}
    procedure TeleportTo(const AX, AY: Single);
    { If Exit from the dungeon level was discovered}
    function ExitVisible: Boolean;
  public
    procedure Render; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
    function Press(const Event: TInputPressRelease): Boolean; override;
    function Motion(const Event: TInputMotion): Boolean; override;
    function Release(const Event: TInputPressRelease): Boolean; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleColors,
  GameCachedImages, GameMapItem, GameMonster, GameMapTypes, GameApparelSlots,
  GameActionRoll, GameActionPlayerZeroG, GameConfiguration, GameMath,
  GameMarkAbstract, GamePlayerCharacter, GameParticle, GameLeash,
  GameViewGame, GameFonts, GameColors, GameTilesetMap, GameAiAbstract,
  GameBatchedRenderer, GameLog;

constructor TMapInterface.Create(AOwner: TComponent);
begin
  inherited;
  // it _should_ be already nil, but to avoid surprises...
  ScreenRects := nil;
  ImageRects := nil;
  DebugNavgrid := -1;
  DebugPlayerOrigins := false;
  DebugMonsterOrigins := false;
  //Touches := TTouches.Create;
end;

destructor TMapInterface.Destroy;
begin
  //FreeAndNil(Touches);
  inherited Destroy;
end;

procedure TMapInterface.TeleportTo(const AX, AY: Single);
begin
  TargetX := AX;
  TargetY := AY;
  CenterX := TargetX;
  CenterY := TargetY;
end;

function TMapInterface.ExitVisible: Boolean;
var
  IX, IY: Integer;
begin
  for IX := 0 to 2 do
    for IY := 0 to 2 do
      if Map.Visible[Map.ExitX + IX + Map.SizeX * (Map.ExitY + IY)] >= RememberedVisible then
        Exit(true);
  Exit(false);
end;

procedure TMapInterface.Render;
var
  RenderCenterX, RenderCenterY: Int16;
  RenderStartX, RenderStartY, RenderEndX, RenderEndY: Int16;
  ZoomH: array [0..PredMaxWallHeight] of Single;
  TrueCenterX, TrueCenterY: array [0..PredMaxWallHeight] of Single;

  procedure CacheScale;
  var
    H: Integer;
    HalfWidthLeft, HalfWidthRight, HalfHeight: Integer;
  begin
    // resize the render array only in case it changed (increased)
    if Length(ScreenRects) < Map.RenderRects then
    begin
      SetLength(ScreenRects, Map.RenderRects);
      SetLength(ImageRects, Length(ScreenRects));
    end;

    HalfWidthLeft := Trunc(GameplayAreaWidth * UIScale / 2.0 / (Zoom * RenderTileSize)) + 1;
    HalfWidthRight := Trunc((GameplayAreaWidth * UIScale / 2.0 + (RenderRect.Width - GameplayAreaWidth * UIScale)) / (Zoom * RenderTileSize)) + 1;
    HalfHeight := Trunc(RenderRect.Height / (Zoom * RenderTileSize)) div 2 + 1;

    RenderCenterX := Trunc(CenterX);
    RenderCenterY := Trunc(CenterY);

    if RenderCenterX - HalfWidthLeft >= 0 then
      RenderStartX := -HalfWidthLeft
    else
      RenderStartX := -RenderCenterX;
    if RenderCenterY - HalfHeight >= 0 then
      RenderStartY := -HalfHeight
    else
      RenderStartY := -RenderCenterY;

    if RenderCenterX + HalfWidthRight <= Map.PredSizeX then
      RenderEndX := HalfWidthRight
    else
      RenderEndX := Map.PredSizeX - RenderCenterX;
    if RenderCenterY + HalfHeight <= Map.PredSizeY then
      RenderEndY := HalfHeight
    else
      RenderEndY := Map.PredSizeY - RenderCenterY;

    for H := 0 to PredMaxWallHeight do
    begin
      ZoomH[H] := H / 40.0;
      if Zoom < 1.0 then
        ZoomH[H] *= Sqr(Zoom);
      ZoomH[H] := RenderTileSize * (Zoom * (1 + ZoomH[H]));
      TrueCenterX[H] := GameplayAreaWidth * UIScale / 2.0 - Frac(CenterX) * ZoomH[H];
      TrueCenterY[H] := RenderRect.Center.Y - Frac(CenterY) * ZoomH[H];
    end;
  end;

  procedure RenderMap;
  var
    Count: SizeInt;
    X, Y: Int16;
    I: SizeInt;
    H: Integer;
  begin
    Count := 0;
    for H := 0 to PredMaxWallHeight do
    begin
      if (Zoom < SwitchToMapModeZoom) and (H <> 0) then
        continue;
      for Y := RenderStartY to RenderEndY do
        begin
          //I := 0; optimize
          for X := RenderStartX to RenderEndX do
          begin
            I := X + RenderCenterX + (Y + RenderCenterY) * Map.SizeX;
            if (Length(Map.FMap[I]) > H) and (Map.Visible[I] > ForgottenVisible) then
            begin
              ScreenRects[Count] := FloatRectangle(
                TrueCenterX[H] + X * ZoomH[H],
                TrueCenterY[H] + Y * ZoomH[H],
                ZoomH[H], ZoomH[H]
              );
              if Map.Visible[I] >= DirectlyVisible then
                ImageRects[Count] := FloatRectangle(1 + TileSizeWithPadding * Map.FMap[I][H], 1 + TileSizeWithPadding * 0, RenderTileSize, RenderTileSize)
              else
                ImageRects[Count] := FloatRectangle(1 + TileSizeWithPadding * Map.FMap[I][H], 1 + TileSizeWithPadding * 1, RenderTileSize, RenderTileSize);
              Inc(Count);
            end;
          end;
        end;
      end;

    TilesetMap.DrawBatched(ScreenRects, ImageRects, Count);
  end;

  procedure RenderNavgrid;
  var
    Count: SizeInt;
    X, Y: Int16;
    I: SizeInt;
  begin
    Count := 0;
    for Y := RenderStartY to RenderEndY do
      begin
        //I := 0; optimize
        for X := RenderStartX to RenderEndX do
        begin
          I := X + RenderCenterX + (Y + RenderCenterY) * Map.SizeX;
          if not Map.PassableTiles[DebugNavgrid][I] then
          begin
            ScreenRects[Count] := FloatRectangle(
              TrueCenterX[0] + X * ZoomH[0],
              TrueCenterY[0] + Y * ZoomH[0],
              ZoomH[0], ZoomH[0]
            );
            ImageRects[Count] := FloatRectangle(0, 0, 32, 32);
            Inc(Count);
          end;
        end;
      end;

    DebugColliderImage.DrawBatched(ScreenRects, ImageRects, Count);
  end;

  procedure RenderPlayerOrigins;
  var
    Count: SizeInt;
    I: Integer;
  begin
    Count := 0;
    for I := 0 to Pred(Map.CharactersOnThisLevel.Count) do
      if (Map.CharactersOnThisLevel[I].LastTileX >= RenderStartX + RenderCenterX) and (Map.CharactersOnThisLevel[I].LastTileX <= RenderEndX + RenderCenterX + 3-1) and
         (Map.CharactersOnThisLevel[I].LastTileY >= RenderStartY + RenderCenterY) and (Map.CharactersOnThisLevel[I].LastTileY <= RenderEndY + RenderCenterY + 3-1) then
        begin
          ScreenRects[Count] := FloatRectangle(
            TrueCenterX[0] + (Map.CharactersOnThisLevel[I].LastTileX - RenderCenterX) * ZoomH[0],
            TrueCenterY[0] + (Map.CharactersOnThisLevel[I].LastTileY - RenderCenterY) * ZoomH[0],
            ZoomH[0], ZoomH[0]
          );
          ImageRects[Count] := FloatRectangle(0, 0, 32, 32);
          Inc(Count);
        end;
    DebugPlayerOriginImage.DrawBatched(ScreenRects, ImageRects, Count);
  end;

  procedure RenderMonsterOrigins;
  var
    Count: SizeInt;
    I: Integer;
  begin
    Count := 0;
    for I := 0 to Pred(Map.MonstersList.Count) do
      if Map.MonstersList[I].CanAct then
      if (Map.MonstersList[I].LastTileX >= RenderStartX + RenderCenterX) and (Map.MonstersList[I].LastTileX <= RenderEndX + RenderCenterX + 3-1) and
         (Map.MonstersList[I].LastTileY >= RenderStartY + RenderCenterY) and (Map.MonstersList[I].LastTileY <= RenderEndY + RenderCenterY + 3-1) then
        begin
          ScreenRects[Count] := FloatRectangle(
            TrueCenterX[0] + (Map.MonstersList[I].LastTileX - RenderCenterX) * ZoomH[0],
            TrueCenterY[0] + (Map.MonstersList[I].LastTileY - RenderCenterY) * ZoomH[0],
            ZoomH[0], ZoomH[0]
          );
          ImageRects[Count] := FloatRectangle(0, 0, 32, 32);
          Inc(Count);
        end;
    DebugMonsterOriginImage.DrawBatched(ScreenRects, ImageRects, Count);
  end;

  procedure RenderExtrema;
  var
    Count: SizeInt;
    I: Integer;
  begin
    Count := 0;
    for I := 0 to Pred(Map.Extrema.Count) do
      if (Map.Extrema[I].X >= RenderStartX + RenderCenterX) and (Map.Extrema[I].X <= RenderEndX + RenderCenterX + 3-1) and
         (Map.Extrema[I].Y >= RenderStartY + RenderCenterY) and (Map.Extrema[I].Y <= RenderEndY + RenderCenterY + 3-1) then
        begin
          ScreenRects[Count] := FloatRectangle(
            TrueCenterX[0] + (Map.Extrema[I].X - RenderCenterX) * ZoomH[0],
            TrueCenterY[0] + (Map.Extrema[I].Y - RenderCenterY) * ZoomH[0],
            ZoomH[0], ZoomH[0]
          );
          ImageRects[Count] := FloatRectangle(0, 0, 32, 32);
          Inc(Count);
        end;
    DebugExtremaImage.DrawBatched(ScreenRects, ImageRects, Count);
  end;

  procedure RenderItems;
  var
    Count: SizeInt;
    I: TMapItem;
  begin
    Count := 0;
    for I in Map.MapItemsList do
      if (I.LastTileX >= RenderStartX + RenderCenterX) and (I.LastTileX <= RenderEndX + RenderCenterX) and
         (I.LastTileY >= RenderStartY + RenderCenterY) and (I.LastTileY <= RenderEndY + RenderCenterY) and
         I.IsVisible then
      begin
        ScreenRects[Count] := FloatRectangle(
          TrueCenterX[0] + (I.LastTileX - RenderCenterX) * ZoomH[0],
          TrueCenterY[0] + (I.LastTileY - RenderCenterY) * ZoomH[0],
          ZoomH[0], ZoomH[0]
        );
        ImageRects[Count] := I.Item.ItemData.MapImageRect;
        Inc(Count);
      end;

    TilesetItems.DrawBatched(ScreenRects, ImageRects, Count);
  end;

  procedure RenderMonsters;
  var
    Count: SizeInt;
    M: TMonster;
    AiString: String;
    CanSee: Boolean;
    {$IFDEF BlindfoldHidesMonsters}
    CanSeeAnything: Boolean;
    {$ENDIF}
    NoiseRange: Single;
    InversePlayerDamage, InversePlayerStealthDamage: Single;
    HitsToKill: Integer;
    HealthColor: TCastleColor;
  begin
    {$IFDEF BlindfoldHidesMonsters}
    // have to cache it because the call is expensive
    CanSeeAnything := not ViewGame.CurrentCharacter.Blindfolded;
    {$ENDIF}
    InversePlayerDamage := 1 / ViewGame.CurrentCharacter.Inventory.GetDamage;
    InversePlayerStealthDamage := 1 / ViewGame.CurrentCharacter.Inventory.GetStealthDamage;
    Count := 0;
    for M in Map.MonstersList do
      if M.CanAct and
        (M.LastTileX + M.Size >= RenderStartX + RenderCenterX) and (M.LastTileX <= RenderEndX + RenderCenterX) and
        (M.LastTileY + M.Size >= RenderStartY + RenderCenterY) and (M.LastTileY <= RenderEndY + RenderCenterY) then
      begin
        CanSee := {$IFDEF BlindfoldHidesMonsters}CanSeeAnything and{$ENDIF} M.IsVisible;
        if CanSee or M.IsHearable then
        begin
          ScreenRects[Count] := FloatRectangle(
           TrueCenterX[0] + (M.X - RenderCenterX) * ZoomH[0],
           TrueCenterY[0] + (M.Y - RenderCenterY) * ZoomH[0],
           ZoomH[0] * M.Data.Size, ZoomH[0] * M.Data.Size
          );
          if CanSee then
          begin
            ImageRects[Count] := FloatRectangle(0, 0, M.MonsterData.Image.Width, M.MonsterData.Image.Height);
            M.MonsterData.Image.Draw(ScreenRects[Count], ImageRects[Count]);
            if M.Aggressive and not M.MonsterData.IsInvisible then
            begin
              // TEMPORARY (everything here is temporary :)) render AI state
              case M.Ai.AiState of
                asIdle: AiString := '...';
                asInvestigate: AiString := '???';
                asAttack: AiString := '!!!';
                asFlee: AiString := '>>>'
              end;
              FontSoniano16.Print(
                ScreenRects[Count].Right,
                ScreenRects[Count].Top,
                ColorLogTemporary, AiString);
              // hits-to-kill healthbar
              if M.Size > 1 then
              begin
                HitsToKill := Ceil(M.Health * InversePlayerDamage);
                if HitsToKill > 9 then
                  AiString := 'X'
                else
                  AiString := IntToStr(HitsToKill);
                if M.Health >= M.MaxHealth - 0.1 then
                  HealthColor := ColorLogTemporary
                else
                if M.Health <= M.MaxHealth / 3 then
                  HealthColor := ColorLogMapItemBroken // TODO
                else
                  HealthColor := ColorLogMapItemDamaged; // TODO
                FontSoniano16.Print(
                  ScreenRects[Count].Left,
                  ScreenRects[Count].Bottom,
                  HealthColor, AiString);
                if M.Unsuspecting then
                begin
                  HitsToKill := Ceil(M.Health * InversePlayerStealthDamage);
                  if HitsToKill > 9 then
                    AiString := 'X'
                  else
                    AiString := IntToStr(HitsToKill);
                  FontSoniano16.Print(
                    ScreenRects[Count].Right,
                    ScreenRects[Count].Bottom,
                    HealthColor, AiString);
                end;
              end;
              // Visible range to current character, not current monster's target
              if Configuration.VisualizeNoise and M.Unsuspecting then
              begin
                NoiseRange := M.VisibilityRange;
                ScreenRects[Count] := FloatRectangle(
                  TrueCenterX[0] + (M.CenterX - NoiseRange - RenderCenterX) * ZoomH[0],
                  TrueCenterY[0] + (M.CenterY - NoiseRange - RenderCenterY) * ZoomH[0],
                  ZoomH[0] * NoiseRange * 2, ZoomH[0] * NoiseRange * 2
                );
                ImageRects[Count] := FloatRectangle(0, 0, StealthMark.Width, StealthMark.Height);
                StealthMark.Draw(ScreenRects[Count], ImageRects[Count]);
              end;
            end;
          end else // if IsHearable
          begin
            if ViewGame.CurrentCharacter.Inventory.XRayVision then
            begin
              ImageRects[Count] := FloatRectangle(0, 0, M.MonsterData.Image.Width, M.MonsterData.Image.Height);
              M.MonsterData.ImageXRay.Draw(ScreenRects[Count], ImageRects[Count])
            end else
            begin
              ImageRects[Count] := FloatRectangle(0, 0, TempEcho.Width, TempEcho.Height);
              TempEcho.Draw(ScreenRects[Count], ImageRects[Count]);
            end;
          end;
          Inc(Count);
        end;
      end;
  end;

  procedure RenderMarks;
  var
    Count: SizeInt;
    M: TMarkAbstract;
    H_Squeeze, V_Squeeze: Single;
  begin
    Count := 0;
    for M in Map.MarksList do
      if (M.LastTileX + M.Size >= RenderStartX + RenderCenterX) and (M.LastTileX <= RenderEndX + RenderCenterX) and
         (M.LastTileY + M.Size >= RenderStartY + RenderCenterY) and (M.LastTileY <= RenderEndY + RenderCenterY) {and
         //TODO: Any part of Mark // no need to hide marks - even if they are in invisible, let them be seen
         (Map.Visible[M.LastTile] > 0)} then
      begin
        V_Squeeze := 1 - Sqr(Sqr(Sqr(M.Progress)));
        if M.Data.SqueezeHorizontal then
          H_Squeeze := V_Squeeze
        else
          H_Squeeze := 1;

        ScreenRects[Count] := FloatRectangle(
          TrueCenterX[0] + (M.CenterX - RenderCenterX - M.HalfSize * H_Squeeze) * ZoomH[0],
          TrueCenterY[0] + (M.CenterY - RenderCenterY - M.HalfSize * V_Squeeze) * ZoomH[0],
          ZoomH[0] * M.Size * H_Squeeze, ZoomH[0] * M.Size * V_Squeeze
        );
        ImageRects[Count] := FloatRectangle(0, 0, M.Data.Image.Width, M.Data.Image.Height);
        M.Data.Image.Draw(ScreenRects[Count], ImageRects[Count]);
        Inc(Count);
      end;
  end;

  procedure RenderParticles;
  var
    Count: SizeInt;
    P: TParticle;
  begin
    Count := 0;
    FontSoniano16.Scale := UIScale;
    for P in Map.ParticlesList do
      if (P.LastTileX + P.Size >= RenderStartX + RenderCenterX) and (P.LastTileX <= RenderEndX + RenderCenterX) and
         (P.LastTileY + P.Size >= RenderStartY + RenderCenterY) and (P.LastTileY <= RenderEndY + RenderCenterY) then
      begin
        FontSoniano16.Print(
          TrueCenterX[0] + (P.CenterX - RenderCenterX) * ZoomH[0] - P.HalfWidth,
          TrueCenterY[0] + (P.CenterY - RenderCenterY) * ZoomH[0],
          P.Color, P.Value);
        Inc(Count);
      end;
  end;

  procedure RenderPlayerCharacters;
  const
    ObjSize = 3.0;
    AddSize = 0.5;
  var
    I: Integer;

    procedure RenderFreeCharacter(const PlayerCharacter: TPlayerCharacter);
    var
      ImageRect, ScreenRect: TFloatRectangle;
      J, LeashLength: Integer;
      Leash: TLeash;
      LeashHolder: TMonster;
      Count: SizeInt;
    begin
      if PlayerCharacter.CurrentAction is TActionRoll then
        if TActionRoll(PlayerCharacter.CurrentAction).MoveVector.X <= 0 then
          PlayerCharacter.Inventory.PaperDollSprite.Image.Rotation := 2 * Pi * TActionRoll(PlayerCharacter.CurrentAction).MovePhase / TActionRoll(PlayerCharacter.CurrentAction).MoveVector.Length
        else
          PlayerCharacter.Inventory.PaperDollSprite.Image.Rotation := -2 * Pi * TActionRoll(PlayerCharacter.CurrentAction).MovePhase / TActionRoll(PlayerCharacter.CurrentAction).MoveVector.Length
      else
      if PlayerCharacter.CurrentAction is TActionPlayerZeroG then
        PlayerCharacter.Inventory.PaperDollSprite.Image.Rotation := 2 * Pi * TActionPlayerZeroG(PlayerCharacter.CurrentAction).Phase / TActionPlayerZeroG(PlayerCharacter.CurrentAction).Duration
      else
        PlayerCharacter.Inventory.PaperDollSprite.Image.Rotation := 0;

      ScreenRect := FloatRectangle(
        TrueCenterX[0] + (PlayerCharacter.X - RenderCenterX) * ZoomH[0],
        TrueCenterY[0] + (PlayerCharacter.Y - RenderCenterY) * ZoomH[0],
        ZoomH[0] * PlayerCharacter.Data.Size, ZoomH[0] * PlayerCharacter.Data.Size
      );
      ImageRect := FloatRectangle(0, 0, PlayerCharacter.Inventory.PaperDollSprite.Image.Width, PlayerCharacter.Inventory.PaperDollSprite.Image.Height);
      PlayerCharacter.Inventory.PaperDollSprite.Image.Draw(ScreenRect, ImageRect);

      if (PlayerCharacter.Inventory.Apparel[esLeash] <> nil) and ((PlayerCharacter.Inventory.Apparel[esLeash] as TLeash).LeashHolder <> nil) then
      begin
        Count := 0;
        Leash := PlayerCharacter.Inventory.Apparel[esLeash] as TLeash;
        LeashHolder := Leash.LeashHolder as TMonster;
        LeashLength := Succ(Trunc(Leash.LeashLength * 2));
        for J := 1 to Pred(LeashLength) do
        begin
          ScreenRects[Count] := FloatRectangle(
            TrueCenterX[0] + ( (PlayerCharacter.CenterX * J + LeashHolder.CenterX * (LeashLength - J)) / LeashLength - RenderCenterX - 0.15) * ZoomH[0],
            TrueCenterY[0] + ( (PlayerCharacter.CenterY * J + LeashHolder.CenterY * (LeashLength - J)) / LeashLength - RenderCenterY - 0.15) * ZoomH[0],
            ZoomH[0] * 0.3, ZoomH[0] * 0.3
          );
          ImageRects[Count] := FloatRectangle(0, 0, LeashMap.Width, LeashMap.Height);
          Inc(Count);
        end;
        LeashMap.DrawBatched(ScreenRects, ImageRects, Count);
      end;
    end;

    procedure RenderCapturedCharacter(const PlayerCharacter: TPlayerCharacter);
    var
      ImageRect, ScreenRect: TFloatRectangle;
    begin
      if PlayerCharacter.IsVisible then
      begin
        ScreenRect := FloatRectangle(
          TrueCenterX[0] + (PlayerCharacter.X - RenderCenterX) * ZoomH[0],
          TrueCenterY[0] + (PlayerCharacter.Y - RenderCenterY) * ZoomH[0],
          ZoomH[0] * PlayerCharacter.Data.Size, ZoomH[0] * PlayerCharacter.Data.Size
        );
        ImageRect := FloatRectangle(0, 0, PlayerCharacter.Inventory.PaperDollSprite.Image.Width, PlayerCharacter.Inventory.PaperDollSprite.Image.Height);
        PlayerCharacter.Inventory.PaperDollSprite.Image.Rotation := 0;
        PlayerCharacter.Inventory.PaperDollSprite.Image.Draw(ScreenRect, ImageRect);
        ImageRect := FloatRectangle(0, 0, TempPlayerBinds.Width, TempPlayerBinds.Height);
        TempPlayerBinds.Draw(ScreenRect, ImageRect);
      end;
    end;

  begin
    for I := 0 to Pred(Map.CharactersOnThisLevel.Count) do
      if Map.CharactersOnThisLevel[I].IsCaptured then
        RenderCapturedCharacter(Map.CharactersOnThisLevel[I]);
    for I := 0 to Pred(Map.CharactersOnThisLevel.Count) do
      if not Map.CharactersOnThisLevel[I].IsCaptured and (Map.CharactersOnThisLevel[I] <> ViewGame.CurrentCharacter) then
        RenderFreeCharacter(Map.CharactersOnThisLevel[I]);
    RenderFreeCharacter(ViewGame.CurrentCharacter); // never IsCaptured
  end;

  procedure RenderExit;
  var
    ScreenRect, ImageRect: TFloatRectangle;
  begin
    if (Map.ExitX >= RenderStartX + RenderCenterX) and (Map.ExitX <= RenderEndX + RenderCenterX + 3-1) and
       (Map.ExitY >= RenderStartY + RenderCenterY) and (Map.ExitY <= RenderEndY + RenderCenterY + 3-1) and
       ExitVisible then
    begin
      ScreenRect := FloatRectangle(
        TrueCenterX[0] + (Map.ExitX - RenderCenterX) * ZoomH[0],
        TrueCenterY[0] + (Map.ExitY - RenderCenterY) * ZoomH[0],
        ZoomH[0] * 3, ZoomH[0] * 3
      );
      ImageRect := FloatRectangle(0, 0, 32, 32);
      TempExit.Draw(ScreenRect, ImageRect);
    end;
  end;

begin
  { TODO: Rework Render map into Viewport -> single shape as discussed with @michaliskambi
    Until then there is not much point to introduce complex light shading
    or fix color jump in MAP zoom
    Also: add lighting (togglable in Options? and Lit/Unlit material?)
    Also: everything is a single scene so that items and monsters will be properly
    hidden behind the walls. }
  CacheScale;
  RenderMap;
  if DebugNavgrid >= 0 then
    RenderNavgrid;
  if DebugPlayerOrigins then
    RenderPlayerOrigins;
  if DebugMonsterOrigins then
    RenderMonsterOrigins;
  if DebugExtrema then
    RenderExtrema;
  RenderExit;
  RenderItems;
  RenderMonsters;
  RenderMarks;
  RenderPlayerCharacters;
  RenderParticles;
end;

procedure TMapInterface.Update(const SecondsPassed: Single;
  var HandleInput: Boolean);
begin
  inherited Update(SecondsPassed, HandleInput);
  CenterX := (2 * CenterX + TargetX) / 3;
  CenterY := (2 * CenterY + TargetY) / 3;
end;

function TMapInterface.Press(const Event: TInputPressRelease): Boolean;
var
  Touch: TTouch;
begin
  Result := inherited;
  if Result then
    Exit;

  if Event.EventType = itMouseButton then
  begin
    Touch.Position := Event.Position;
    Touch.Start := Timer;
    Touches[Event.FingerIndex] := Touch;
    {OnMapClick(
      CenterX + (Event.Position.X - RenderRect.Center.X * CenterFractionShift) / (RenderTileSize * Zoom),
      CenterY + (Event.Position.Y - RenderRect.Center.Y) / (RenderTileSize * Zoom)
    ); }
    Result := true;

    if ViewGame.IsActivePause then
    begin
      DragStarted := true;
      DragStart := Event.Position;
      DragCenterStartX := CenterX;
      DragCenterStartY := CenterY;
    end;
  end;
end;

function TMapInterface.Motion(const Event: TInputMotion): Boolean;
begin
  Result := inherited;
  if DragStarted and ViewGame.IsActivePause then
  begin
    TargetX := DragCenterStartX - (Event.Position.X - DragStart.X) / (RenderTileSize * Zoom);
    TargetY := DragCenterStartY - (Event.Position.Y - DragStart.Y) / (RenderTileSize * Zoom);
  end;
end;

function TMapInterface.Release(const Event: TInputPressRelease): Boolean;
var
  ClickX, ClickY: Single;
begin
  Result := inherited;
  if Result then
    Exit;

  if Event.EventType = itMouseButton then
  begin
    DragStarted := false;

    if (ViewGame.IsPause) and ((Event.Position - Touches[Event.FingerIndex].Position).Length / (RenderTileSize * Zoom) > 5) then // TODO: Something smarter here, we need OnClick to give orders, maybe even swipes
      Exit;

    ClickX := CenterX + (Event.Position.X - GameplayAreaWidth * UIScale / 2.0) / (RenderTileSize * Zoom);
    ClickY := CenterY + (Event.Position.Y - RenderRect.Center.Y) / (RenderTileSize * Zoom);
    { It may be a bad idea to have multitouch gestures
      because the game is expected to be fast-paced and therefore such gestures
      may get in the way of other, more critical input commands.
      Moreover, for now I don't think anything but zoom would use those,
      and zoom is an extremely rudimentary feature that shouldn't be used often
      and maybe in the end - should smoothly switch between 3 modes:
      close up view (during struggle or rest), normal zoom and map
      though during multi-character missions it might be reasonable to have more flexibility }
    if (Event.MouseButton = buttonLeft) and
      ((Event.Position - Touches[Event.FingerIndex].Position).Length > Self.RenderRect.Width / 15) and
      ((Event.Position - Touches[Event.FingerIndex].Position).Length / Touches[Event.FingerIndex].Start.ElapsedTime > Self.RenderRect.Width / 5) then
      OnMapSwipe(ClickX, ClickY)
    else
    if (Event.MouseButton = buttonRight) or (Touches[Event.FingerIndex].Start.ElapsedTime > 0.5) then
      OnMapLongClick(ClickX, ClickY)
    else
      OnMapClick(ClickX, ClickY);
    Result := true;
  end;
end;

end.


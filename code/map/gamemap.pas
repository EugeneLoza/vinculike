{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Handling of all functions related to map }
unit GameMap;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes, DOM,
  CastleVectors,
  TempStamps,
  GameMapTypes, GameMarkAbstract, GameMonster, GamePlayerCharacter, GameMapItem,
  GameParticle, GameActorData, GameMapExtremum, GamePositionedObject, GameMonsterData;

type
  ENavGridError = class(Exception);

type
  TSetOfByte = Set of Byte;

type
  TMap = class(TObject)
  public const
    { As we store map depth in Byte - for no clear reason
      this limits the max depth that can be used
      note that depths > 100 will most likely be reserved for
      missions and special levels, like boss arenas }
    MaxDungeonDepth = 255;
  strict private const
    { Max amount of different monster kinds that can
      spawn on a single given level
      it seems that higher values results in too chaotic levels
      note that each monster from the spawn list will appear at least once }
    MaxMonstersKindsSpawnPerLevel = Integer(12);
    { Max amount of different monsters in patrol list }
    MaxMonstersKindsPatrolPerLevel = Integer(7);
  strict private
    FSizeX: UInt16;
    FSizeY: UInt16;
    MaxDistanceFromEntrance: TDistanceQuant;
    DistanceFromEntranceCache: TDistanceMapArray;
    { Remove wall-only rows and columns of the map}
    procedure CropMap;
    { generate "passable tiles" arrays for different collider sizes }
    procedure GeneratePassableTilesForColliders;
    { Calculate amount of "render tiles" on the map (for log) }
    procedure CalculateRenderRects;
    { find extremas (passage ends, room corners) on the map }
    procedure FindDistanceMapExtrema;
    { Flood fill a map array from a given point (can-cannot be reached) }
    function FloodFill(const SrcArray: TMoveArray; const StartX, StartY: UInt32): TMoveArray;
    { Look from a specific point on the map,
      marks all visible tiles as visible }
    procedure LookAround(const AX, AY: Int16; const VisibleRange: Single);
    { mark all map tiles as invisible}
    procedure ClearVisible;
    procedure ClearActiveVisible;
    { Generates a map of passable tiles with this collider }
    function GeneratePassableMap(const PredColliderSize: Integer): TMoveArray;
    { Recall all "forgotten" visible areas
      note : very likely will not work good for multiple characters
      and needs a proper fix / rework}
    procedure RememberVisible;
  public
    FScheduleUpdateVisible: Boolean;
  public
    { Generator: random map }
    procedure GenerateRandom;
    { Generator: stamp map }
    procedure GenerateStamp(const Stamp: TStamp);
  public
    { Current dungeon level/ id of a mission}
    CurrentDepth: Int16;
    Tileset: Int16;
    { Random seed to pick tiles for the tileset}
    TilesetRandomSeed: LongWord;
    { Cached "pred" values of SizeX/SizeY, i.e. value - 1}
    PredSizeX: UInt16;
    PredSizeY: UInt16;
    { Coordinates of the map entry point
      I don't think they are used after map generation is finished }
    EntranceX: Int16;
    EntranceY: Int16;
    { Coordinates of map exit position.
      note: it should rather be an object on the map, than hacky coordinate pair, todo }
    ExitX: Int16;
    ExitY: Int16;
    { All tiles on the map }
    FMap: TMapArray;
    { Cached values for if a certain tile can be walked over by this PredColliderSize }
    PassableTiles: array [0..PredMaxColliderSize] of TMoveArray;
    { What is visible by Current Character
      note: currently it's a "dump" used by all characters and therefore
      shared between them. It results in a bunch of bugs and limitations
      todo: this should be per-character }
    Visible: TVisibleArray;
    { A list of extrema on the map (such as deadends, corners) }
    Extrema: TExtremaList;
    { Extrema used for generation (i.e. starting from a copy of Extrema list
      and once some object has been added there,it gets removed from the list }
    ExtremaGeneration: TExtremaList;
    { Amount of render rectangles on the
      used to allocate memory once and avoid reallocating it again}
    RenderRects: Integer;
    { length of the longest route between two points in the
      now it corresponds to distance between entry and exit
      used in pathfinding to avoid reallocating memory}
    LongestRoute: Integer;
    procedure MakeGenerationCache;
    { find max distance value on a TDistanceMapArray}
    function MaxDistance(const MapArray: TDistanceMapArray): TDistanceQuant;
    { Find the longest route in the map and assign one end to entrance and the other to exit }
    procedure GenerateEntryAndExit;
    { Free all objects and lists used during the generation
      to pass values between different generation steps }
    procedure ClearGenerationCache;
    { build a distance map from a "Seed" (list of coordinate pairs) }
    function DistanceMap(const PredSize: Byte; const IgnoreVisible: Boolean; const Seed: TIntCoordList; const StopAtDestination: Boolean = false; const DestinationX: Int16 = 0; const DestinationY: Int16 = 0): TDistanceMapArray;
    { build a distance map from an Actor (will automatically generate and handle the Seed )}
    function DistanceMap(const Actor: TObject; const IgnoreVisible: Boolean): TDistanceMapArray;
    { Find a nearest tile that can be walked over by a specific collider
      note: it doesn't determine if this tile can actually be reachable
      (for now this is always true for monsters, but not for the
      in future "large  monsters" may also be unable to reach certain map regions
      also doors and similar obstacles can adjust the set of areas accessible by the actor
      the determination of where to go happens in pathfinding algorithm)}
    procedure GetNearestPassableTile(const PredColliderSize: Integer; const IgnoreVisible: Boolean; const ToX, ToY: Int16; out OutX, OutY: Int16);
    { Generate list of waypoints based on a TDistanceMapArray }
    function GetWaypoints(const DistMap: TDistanceMapArray; const FromX, FromY, ToX, ToY: Single; const FromXInt, FromYInt: Int16): TFloatCoordList;
    { Convert a path to a seed to build a distance
      (used to position prisoner as far from the entrance-exit path as possible ) }
    {$IFDEF DijkstraPathfinding}
    function GenerateDijkstra(const PredColliderSize: Integer; const IgnoreVisible: Boolean; const FromX, FromY, ToX, ToY: Int16): TDirectionArray;
    function GetWaypoints(const Directions: TDirectionArray; const FromX, FromY, ToX, ToY: Single; const FromXInt, FromYInt: Int16): TFloatCoordList;
    {$ENDIF}
    function GetPathSeed(const Waypts: TFloatCoordList): TIntCoordList;
    { Queue to update visible area at the end of the frame}
    procedure ScheduleUpdateVisible;
    { Asks all Active characters to look around, handles disorientation and remembering visible areas}
    procedure UpdateVisible;
    { Marks non-directly visible tiles as forgotten
      (result of disorientation ) }
    procedure ForgetVisible(const DisorientationAmount: SizeInt);
    { Casts a ray between a pair of integer coordinate
      true - ray passes
      false - ray is blocked }
    function Ray(const PredSize: Byte; const FromX, FromY: Int16; var ToX, ToY: Int16): Boolean;
    { Casts a ray between a pair of floating point coordinates
      note: current implementation just does an integer ray, todo }
    function Ray(const PredSize: Byte; const FromX, FromY, ToX, ToY: Single): Boolean;
    { Debug option to mark all tiles as directly visible
      note that before Update the tiles won't be marked as remembered or forgotten
      so if the game is on pause the map will stay fully visible}
    procedure ShowAllMap;
    { Same as ShowAllMap but doesn't mark "deep walls" as visible
      only walls adjacent to a passable tile is revealed
      (as it would happen in the game) }
    procedure ShowPassableMap;
  public
    { Can this tile be walked over }
    function CanMove(const Tile: SizeInt): Boolean; inline;
    { Can this tile be looked through}
    function CanSee(const Tile: SizeInt): Boolean; inline;
  public
    { Value of (remaining) disorientation (in tiles)
      when this reaches zero, character will remember forgotten areas }
    Disorientation: SizeInt;
    { Marks (only active) on this map }
    MarksList: TMarksList;
    { Monsters (dead and alive) on this map }
    MonstersList: TMonstersList;
    { All player characters in the game
      (regardless of dungeon level and captured status ) }
    PlayerCharactersList: TPlayerCharactersList;
    { All characters on this dungeon level
      both captured and free }
    CharactersOnThisLevel: TPlayerCharactersList;
    { list of monsters data of monsters that can spawn in patrol on this map }
    PatrolList: TMonstersDataList;
    { list of items on this map (but not held by character or monsters) }
    MapItemsList: TMapItemsList;
    { list of active (information) particles on this map }
    ParticlesList: TParticlesList;
    { time until next patrol}
    EnemySpawnTime: Single; // Call Map.Update(secondsPassed) and hide this one
    { Time the character has spent on this map }
    TimeSpentOnTheMap: Single;
    { Sets all monsters on the map to idle
      (e.g. to make them stop chasing the character after teleportation) }
    procedure MonstersToIdle;
    function MonsterSeen(const AX, AY: Int16; const Range: Single): Boolean;
    { Checks if no monsters are nearby with the additional CollisionMargin }
    function NoMonstersNearby(const AX, AY: Int16; const CollisionMargin: Int16): Boolean;
    function NoMarksNearby(const Obj: TPositionedObject; const AdditionalMargin: Single): Boolean;
    { Is at least one monster in attack mode }
    function MonstersAttacking: Boolean;
    { Count captured characters on a specific dungeon level }
    function CapturedCharacterOnLevel(const DungeonLevel: Byte): Integer;
    { Deepest level reached by the Player}
    function DeepestLevel: Byte;
    function UnlockedCharacters: Integer;
    { List of dungeon levels that can be teleported to from the Settlement}
    function UnlockedDestinations: TSetOfByte;
  public const Signature = 'map'; // deprecated
  public
    procedure Save(const Element: TDOMElement);
    procedure Load(const Element: TDOMElement);
  public
    { Amount of walkable tiles on the map }
    FreeArea: UInt32;
    { Size of the map }
    property SizeX: UInt16 read FSizeX;
    property SizeY: UInt16 read FSizeY;
    { Generate monsters in the map until they add up to TotalDanger }
    procedure GenerateMonsters(const TotalDanger: Single);
    { Gwnerate traps on the map }
    procedure GenerateTraps(const NumberOfTraps: Integer);
    { Generate items on the map - lying around and in containers }
    procedure GenerateItems(const GenerateItems: Integer);
    { Build a cache of characters on current dungeon level}
    procedure CacheCharactersOnThisLevel;
    { put prisoners on the map }
    procedure PositionCapturedCharacters;
    { Captured characters remaining on this dungeon level }
    function CapturedCharactersAtLevel: Integer;
    { Reset all map containers and parameters to default/clear}
    procedure Init(const ASizeX, ASizeY: UInt16);
  public
    { Clear the contents of the map }
    procedure Clear;
    constructor Create; //override;
    destructor Destroy; override;
  end;

var
  { global map container;
    todo: multiple containers, move global variables to a different class }
  Map: TMap;

procedure InitMap;
procedure FreeMap;
implementation
uses
  Base64,
  CastleXmlUtils,
  GameRandom, GameMath,
  GameInventoryItem, GameItemData, GameItemsDatabase,
  GameActionIdle, GameLog, GameColors, GameTileset, GameTilesetMap,
  GameMonstersDatabase,
  GameViewGame;

procedure TMap.Init(const ASizeX, ASizeY: UInt16);
var
  I: Integer;
begin
  FSizeX := ASizeX;
  FSizeY := ASizeY;
  PredSizeX := Pred(FSizeX);
  PredSizeY := Pred(FSizeY);
  FMap := nil;
  SetLength(FMap, FSizeX * FSizeY);
  Visible := nil;
  SetLength(Visible, FSizeX * FSizeY);
  for I := 0 to PredMaxColliderSize do
    PassableTiles[I] := nil;
  ClearVisible;
  MonstersList.Clear;
  PatrolList.Clear;
  MapItemsList.Clear;
  MarksList.Clear;
  ParticlesList.Clear;
  Extrema.Clear;
  CharactersOnThisLevel.Clear;
  TimeSpentOnTheMap := 0;
  FScheduleUpdateVisible := true;
  Disorientation := 0;
  LongestRoute := 1024; // if it remains so, it's a bug, so for now just a number
end;

procedure TMap.Clear;
begin
  Init(1, 1);
  PlayerCharactersList.Clear;
end;

constructor TMap.Create;
begin
  inherited; //parent is empty
  PatrolList := TMonstersDataList.Create(false);
  MarksList := TMarksList.Create(true);
  MonstersList := TMonstersList.Create(true);
  PlayerCharactersList := TPlayerCharactersList.Create(true);
  MapItemsList := TMapItemsList.Create(true);
  ParticlesList := TParticlesList.Create(true);
  CharactersOnThisLevel := TPlayerCharactersList.Create(false); // it's a cache, doesn't own children
  Extrema := TExtremaList.Create(true);
end;

destructor TMap.Destroy;
begin
  FreeAndNil(PatrolList);
  FreeAndNil(MonstersList); // Monsters must be freed before Marks because their actions still reference marks
  FreeAndNil(PlayerCharactersList);
  FreeAndNil(MapItemsList);
  FreeAndNil(MarksList);
  FreeAndNil(ParticlesList);
  FreeAndNil(CharactersOnThisLevel);
  FreeAndNil(Extrema);
  inherited Destroy;
end;

procedure TMap.ClearVisible;
var
  I: SizeInt;
begin
  for I := 0 to Pred(SizeX * SizeY) do
    Visible[I] := 0;
  // TODO: FillByte
end;

procedure TMap.ClearActiveVisible;
var
  I: SizeInt;
begin
  for I := 0 to Pred(SizeX * SizeY) do
    if Visible[I] = DirectlyVisible then
      Visible[I] := RememberedVisible;
end;

procedure TMap.RememberVisible;
var
  I: SizeInt;
begin
  ShowLog('%s recalls this place', [ViewGame.CurrentCharacter.Data.DisplayName], ColorDefault);
  for I := 0 to Pred(SizeX * SizeY) do
    if Visible[I] = ForgottenVisible then
      Visible[I] := RememberedVisible;
end;

procedure TMap.ForgetVisible(const DisorientationAmount: SizeInt);
var
  I: SizeInt;
begin
  if DisorientationAmount <= 0 then
    raise Exception.CreateFmt('Unexpected DisorientationAmount value: %d in %s', [DisorientationAmount, Self.ClassName]);
  if Disorientation < DisorientationAmount then
    Disorientation := DisorientationAmount;
  for I := 0 to Pred(SizeX * SizeY) do
    if Visible[I] > ForgottenVisible then
      Visible[I] := ForgottenVisible;
  { To prevent from "Accidentally" navigating into forgotten area,
    Will be rebuilt upon next request }
  ScheduleUpdateVisible;
end;

procedure TMap.ShowAllMap;
var
  I: SizeInt;
begin
  for I := 0 to Pred(SizeX * SizeY) do
    Visible[I] := DirectlyVisible;
end;

procedure TMap.ShowPassableMap;
var
  IX, IY: Int16;
  I: SizeInt;
begin
  I := 0;
  for IY := 0 to PredSizeY do
    for IX := 0 to PredSizeX do
    begin
      if CanMove(I) then
        Visible[I] := RememberedVisible
      else
      if ((IX > 0) and CanMove(I - 1)) or
         ((IX < PredSizeX) and CanMove(I + 1)) or
         ((IY > 0) and CanMove(I - SizeX)) or
         ((IY < PredSizeY) and CanMove(I + SizeX)) then
           Visible[I] := RememberedVisible;
      Inc(I);
    end;
end;

procedure TMap.CropMap;
var
  StartX, StartY, EndX, EndY: UInt16;
  OldSizeX{, OldSizeY}: UInt16;
  X, Y: Int16;
  PassageFound: Boolean;
  OldMap: TMapArray;
  I: SizeInt;
begin
  X := -1;
  PassageFound := false;
  repeat
    Inc(X);
    for Y := 0 to PredSizeY do
      if CanMove(X + SizeX * Y) then
      begin
        PassageFound := true;
        break;
      end;
  until PassageFound;
  StartX := X;

  X := SizeX;
  PassageFound := false;
  repeat
    Dec(X);
    for Y := 0 to PredSizeY do
      if CanMove(X + SizeX * Y) then
      begin
        PassageFound := true;
        break;
      end;
  until PassageFound;
  EndX := X;

  Y := -1;
  PassageFound := false;
  repeat
    Inc(Y);
    for X := 0 to PredSizeX do
      if CanMove(X + SizeX * Y) then
      begin
        PassageFound := true;
        break;
      end;
  until PassageFound;
  StartY := Y;

  Y := SizeY;
  PassageFound := false;
  repeat
    Dec(Y);
    for X := 0 to PredSizeX do
      if CanMove(X + SizeX * Y) then
      begin
        PassageFound := true;
        break;
      end;
  until PassageFound;
  EndY := Y;

  if (StartX > 1) or (EndX < PredSizeX - 1) or
     (StartY > 1) or (EndY < PredSizeY - 1) then
  begin
    OldMap := FMap;
    OldSizeX := FSizeX;
    //OldSizeY := FSizeY;
    EntranceX := EntranceX - StartX + 1;
    EntranceY := EntranceY - StartY + 1;
    LogNormal('Cropping map from %dx%d to %dx%d', [FSizeX, FSizeY, EndX - StartX + 3, EndY - StartY + 3]);
    Init(EndX - StartX + 3, EndY - StartY + 3);
    I := 0;
    for Y := StartY - 1 to EndY + 1 do
      for X := StartX - 1 to EndX + 1 do
      begin
        FMap[I] := OldMap[X + OldSizeX * Y];
        Inc(I);
      end;
    // We call Cropmap from generation algorithm right before executing those:
    //GeneratePassableTilesForColliders;
    //CalculateRenderRects;
  end;

end;

procedure TMap.GeneratePassableTilesForColliders;
var
  K: Integer;
begin
  // TODO : cache move maps for different possible sizes + faster generation of "next steps" as if the tile is not passable by 2-sized chreature, it's most certainly not passable by 3-sized one
  for K := 0 to PredMaxColliderSize do
    PassableTiles[K] := GeneratePassableMap(K);
end;

procedure TMap.GenerateRandom;
var
  X, Y: Integer;
  I: SizeInt;
  PassableTilesPlayerTemp: TMoveArray;

  function CannotStepHere: Boolean;
  var
    X1, Y1: Integer;
    J: SizeInt;
  begin
    for Y1 := 0 to 2 do
      begin
        J := (Y - Y1) * SizeX + X;
        for X1 := 0 to 2 do
        begin
          if (Y - Y1 >= 0) and (X - X1 >= 0) and PassableTilesPlayerTemp[J] then
            Exit(false);
          Dec(J);
        end;
      end;
    Exit(true);
  end;
var
  Count: SizeInt;
  LastTileset: Int16;
begin
  LastTileset := Tileset;
  repeat
    Tileset := Rnd.Random(AllTilesets.Count);
  until Tileset <> LastTileset;
  TilesetRandomSeed := Rnd.Random32bit;
  PrepareTilesetMap;

  repeat
    // Generate random map
    I := 0;
    for Y := 0 to PredSizeY do
      for X := 0 to PredSizeX do
      begin
        if (X = 0) or (Y = 0) or (X = PredSizeX) or (Y = PredSizeY) then
          FMap[I] := Wall
        else
          if Rnd.Random < 0.50 then
            FMap[I] := Wall
          else
            FMap[I] := Floor;
        Inc(I);
      end;
    // Make entry
    EntranceX := 10;
    EntranceY := SizeY - 10;
    for X := -3 to 3 do
      for Y := -3 to 3 do
        FMap[(Y + EntranceY) * SizeX + X + EntranceX] := Floor;
    // Clean columns
    for Count := 0 to 10 do
    begin
      I := 0;
      for Y := 0 to PredSizeY do
        for X := 0 to PredSizeX do
        begin
          if not CanMove(I) then
            if ((X > 0) and CanMove(I - 1) and
                (X < PredSizeX) and CanMove(I + 1)) or
               ((Y > 0) and CanMove(I - SizeX) and
                (Y < PredSizeY) and CanMove(I + SizeX)) then
                  FMap[I] := Floor;
          Inc(I);
        end;
    end;

    PassableTilesPlayerTemp := FloodFill(GeneratePassableMap(PredPlayerColliderSize), EntranceX, EntranceY);

    I := 0;
    for Y := 0 to PredSizeY do
      for X := 0 to PredSizeX do
      begin
        if CanMove(I) and CannotStepHere then
          FMap[I] := Wall;
        Inc(I);
      end;

    Count := 0;
    for I := 0 to Pred(SizeX * SizeY) do
      if CanMove(I) then
        Inc(Count);
    LogNormal('Map area: %.4n', [Single(Count) / Single(SizeX * SizeY)]);
  until (Count > 0.5 * SizeX * SizeY) and (Count < 0.7 * SizeX * SizeY);
  FreeArea := Count;

  CropMap;

  GeneratePassableTilesForColliders;

  CalculateRenderRects;
end;

procedure TMap.GenerateStamp(const Stamp: TStamp);
var
  PassableTilesPlayerTemp: TMoveArray;

  function CannotStepHere(const AX, AY: Int16): Boolean;
  var
    X1, Y1: Integer;
    J: SizeInt;
  begin
    if (AX <= 0) or (AY <= 0) or (AX >= PredSizeX) or (AY >= PredSizeY) then
      Exit(true);
    for Y1 := 0 to PredPlayerColliderSize do
      begin
        J := (AY - Y1) * SizeX + AX;
        for X1 := 0 to 2 do
        begin
          if (AY - Y1 >= 0) and (AX - X1 >= 0) and PassableTilesPlayerTemp[J] then
            Exit(false);
          Dec(J);
        end;
      end;
    Exit(true);
  end;

var
  StampX, StampY, DX, DY, X, Y: Integer;
  SeedX, SeedY: Int16;
  I: SizeInt;
  Count: SizeInt;
  ThisStamp: Integer;
  LastTileset: Int16;
begin
  LastTileset := Tileset;
  repeat
    Tileset := Rnd.Random(AllTilesets.Count);
  until Tileset <> LastTileset;
  TilesetRandomSeed := Rnd.Random32bit;
  PrepareTilesetMap;

  repeat
    // Apply stamps
    for StampX := 0 to PredSizeX div Stamp.SizeX do
      for StampY := 0 to PredSizeY div Stamp.SizeY do
      begin
        ThisStamp := Rnd.Random(Length(Stamp.Stamps));
        for DX := 0 to Pred(Stamp.SizeX) do
          for DY := 0 to Pred(Stamp.SizeY) do
          begin
            X := StampX * Stamp.SizeX + DX;
            Y := StampY * Stamp.SizeY + DY;
            if (X < 0) or (Y < 0) or (X > PredSizeX) or (Y > PredSizeY) then
              Continue;
            if (X = 0) or (Y = 0) or (X = PredSizeX) or (Y = PredSizeY) then
              FMap[X + SizeX * Y] := Wall
            else
              if Stamp.Stamps[ThisStamp][DY][DX] = 0 then
                FMap[X + SizeX * Y] := Floor
              else
                FMap[X + SizeX * Y] := Wall;
          end;
      end;

    PassableTilesPlayerTemp := GeneratePassableMap(PredPlayerColliderSize);

    // Make entry
    repeat
      SeedX := Rnd.Random(SizeX - 10) + 5;
      SeedY := Rnd.Random(SizeY - 10) + 5;
    until PassableTilesPlayerTemp[SeedX + SizeX * SeedY];

    PassableTilesPlayerTemp := FloodFill(GeneratePassableMap(PredPlayerColliderSize), SeedX, SeedY);

    I := 0;
    for Y := 0 to PredSizeY do
      for X := 0 to PredSizeX do
      begin
        if CanMove(I) and CannotStepHere(X, Y) and
          CannotStepHere(X + 1, Y) and CannotStepHere(X - 1, Y) and
          CannotStepHere(X, Y + 1) and CannotStepHere(X, Y - 1) then
            FMap[I] := Wall;
        Inc(I);
      end;

    Count := 0;
    for I := 0 to Pred(SizeX * SizeY) do
      if CanMove(I) then
        Inc(Count);
    LogNormal('Map area: %.4n', [Single(Count) / Single(SizeX * SizeY)]);
  until (Count > 0.3 * SizeX * SizeY) and (Count < 0.6 * SizeX * SizeY);
  FreeArea := Count;

  EntranceX := SeedX; // TODO: Temporary, to pass those to MakeGenerationCache
  EntranceY := SeedY;

  if not PassableTilesPlayerTemp[EntranceX + SizeX * EntranceY] then // it may so happen that during the clean-ups of the map the original EntranceXY is no longer walkable (or could it so happen that it never was? shouldn't be possible)
    repeat
      EntranceX := Rnd.Random(SizeX - 10) + 5;
      EntranceY := Rnd.Random(SizeY - 10) + 5;
    until PassableTilesPlayerTemp[EntranceX + SizeX * EntranceY];

  CropMap; // note, updates EntranceX, EntranceY

  // note, PassableTilesPlayerTemp from previous steps no longer makes sense after cropping, we need to recalculate it again
  GeneratePassableTilesForColliders;
  PassableTilesPlayerTemp := FloodFill(PassableTiles[PredPlayerColliderSize], EntranceX, EntranceY); // we don't need this for anything but the next check which shouldn't ever matter

  if not PassableTilesPlayerTemp[EntranceX + SizeX * EntranceY] then // it may so happen that during the clean-ups of the map the original EntranceXY is no longer walkable (or could it so happen that it never was? shouldn't be possible)
    raise ENavGridError.CreateFmt('Entry point (%d,%d) is not walkable after cropping the map to %dx%d!', [EntranceX, EntranceY, SizeX, SizeY]);

  CalculateRenderRects;

  FindDistanceMapExtrema;
  GenerateEntryAndExit;
  MakeGenerationCache;
end;

procedure TMap.GenerateMonsters(const TotalDanger: Single);
var
  I, J: Integer;
  SpawnTypesCount: Integer;
  RemainingDanger: Single;
  X, Y: Int16;
  AMonster: TMonster;
  SpawnableList, SpawnList: TMonstersDataList;
  MinSpawnDistance: TDistanceQuant;
  MinSpawnRange: Single;
  NotOnlyRareMonsters: Boolean;
begin
  LogNormal('Generating monsters, total danger: %.0n', [TotalDanger]);
  if TotalDanger <= 0 then
    Exit;

  SpawnableList := TMonstersDataList.Create(false);
  for I := 0 to Pred(MonstersData.Count) do
    if (not MonstersData[I].Trap) and
       (not MonstersData[I].Chest) and
       (MonstersData[I].StartSpawningAtDepth <= CurrentDepth) and
       (MonstersData[I].StopSpawningAtDepth > CurrentDepth) then
      SpawnableList.Add(MonstersData[I]);

  SpawnList := TMonstersDataList.Create(false);
  SpawnTypesCount := MinInteger(Rnd.Random(MaxMonstersKindsSpawnPerLevel), Pred(SpawnableList.Count));
  NotOnlyRareMonsters := false; // Make sure there is no level with only rare monsters
  I := 0;
  repeat
    J := Rnd.Random(SpawnableList.Count);
    Inc(I);
    SpawnList.Add(SpawnableList[J]);
    if not SpawnableList[J].Rare then
      NotOnlyRareMonsters := true;
    SpawnableList.Delete(J);
  until (I > SpawnTypesCount) and NotOnlyRareMonsters;

  if SpawnList.Count = 0 then
  begin
    ShowError('Cannot find any monsters to spawn at level %d', [Self.CurrentDepth]);
    Exit;
  end;

  // create monsters
  MinSpawnDistance := DistanceStep * MinInteger(Round(2 * TPlayerCharacter.DefaultVisionRange) + 1, MinInteger(SizeX div 2, SizeY div 2));
  MinSpawnRange := MinSingle(TPlayerCharacter.DefaultVisionRange + 1, Min16(SizeX div 2, SizeY div 2));
  RemainingDanger := TotalDanger;
  while RemainingDanger > 0 do
  begin
    AMonster := TMonster.Create;
    if MonstersList.Count < SpawnList.Count then
      AMonster.Data := SpawnList[MonstersList.Count] // make sure each monster is represented at least once
    else
      repeat
        AMonster.Data := SpawnList[Rnd.Random(SpawnList.Count)];
      until Rnd.Random < AMonster.MonsterData.SpawnFrequency;
    AMonster.Reset;
    repeat
      X := Rnd.Random(SizeX);
      Y := Rnd.Random(SizeY);
    until PassableTiles[AMonster.PredSize][X + SizeX * Y] and (Sqr(EntranceX - X) + Sqr(EntranceY - Y) > Sqr(MinSpawnRange)) and (Rnd.Random < Sqr(Single(DistanceFromEntranceCache[X + SizeX * Y] - MinSpawnDistance) / Single(2 * MinSpawnDistance))) and not Ray(0, EntranceX, EntranceY, Single(X), Single(Y));
    AMonster.Teleport(X, Y);
    AMonster.Ai.SoftGuard := Rnd.RandomBoolean and not AMonster.MonsterData.CuriousAi;
    AMonster.Ai.Guard := Rnd.RandomBoolean and not AMonster.MonsterData.CuriousAi and not AMonster.Ai.SoftGuard;
    AMonster.Ai.GuardPointX := AMonster.LastTileX;
    AMonster.Ai.GuardPointY := AMonster.LastTileY;
    MonstersList.Add(AMonster);
    RemainingDanger -= AMonster.MonsterData.Danger;
  end;
  if MonstersList.Count < SpawnList.Count then
    LogWarning('%d monsters were generated out of %d monster types can spawn', [MonstersList.Count, SpawnList.Count]);

  //Prepre PatrolList
  PatrolList.Clear;
  SpawnableList.Clear;
  for J := 0 to Pred(MonstersData.Count) do
    if (MonstersData[J].StartPatrolAtDepth <= Map.CurrentDepth) and (MonstersData[J].StopPatrolAtDepth > Map.CurrentDepth) then
      SpawnableList.Add(MonstersData[J]);

  SpawnTypesCount := MinInteger(Rnd.Random(MaxMonstersKindsPatrolPerLevel), Pred(SpawnableList.Count));
  // already spawned monsters are always in the PatrolList
  // Also making sure that they can patrol at this depth in the loop above
  for I := 0 to Pred(SpawnList.Count) do
    if SpawnableList.Contains(SpawnList[I]) then
    begin
      PatrolList.Add(SpawnList[I]);
      SpawnableList.Remove(SpawnList[I]);
    end;

  // Also add a random number of other valid types
  if SpawnableList.Count > 0 then
  begin
    for I := PatrolList.Count to SpawnTypesCount do
    begin
      J := Rnd.Random(SpawnableList.Count);
      PatrolList.Add(SpawnableList[J]);
      SpawnableList.Delete(J);
    end;
  end;

  NotOnlyRareMonsters := false;
  for I := 0 to Pred(PatrolList.Count) do
    if not PatrolList[I].Rare then
      NotOnlyRareMonsters := true;

  while (SpawnableList.Count > 0) and (not NotOnlyRareMonsters) do
  begin
    J := Rnd.Random(SpawnableList.Count);
    if not SpawnableList[J].Rare then
    begin
      PatrolList.Add(SpawnableList[J]);
      NotOnlyRareMonsters := true;
    end;
    SpawnableList.Delete(J);
  end;

  if PatrolList.Count = 0 then
    ShowError('Failed to generate PatrolList : PatrolList.Count = 0');
  if not NotOnlyRareMonsters then
    ShowError('Failed to generate PatrolList : OnlyRareMonsters');

  FreeAndNil(SpawnList);
  FreeAndNil(SpawnableList);
end;

procedure TMap.GenerateTraps(const NumberOfTraps: Integer);
const
  MaxRetryCount = 10000;
var
  I: Integer;
  X, Y: Int16;
  ATrap: TMonster;
  SpawnList: TActorDataList;
  RetryCount: Integer;

  function NotCollidesWithOtherTraps: Boolean;
  var
    M: TMonster;
  begin
    for M in MonstersList do
      if M.CanAct and M.MonsterData.Trap then
        if M.Collides(ATrap, 0) then
          Exit(false);
    Exit(true);
  end;

begin
  if NumberOfTraps = 0 then
    Exit;
  SpawnList := TActorDataList.Create(false);
  for I := 0 to Pred(TrapsData.Count) do
    if (TrapsData[I].StartSpawningAtDepth <= CurrentDepth) and
      (TrapsData[I].StopSpawningAtDepth > CurrentDepth) then
      SpawnList.Add(TrapsData[I]);
  if SpawnList.Count = 0 then
  begin
    ShowError('Cannot find any traps to spawn at level %d', [Self.CurrentDepth]);
    Exit;
  end;

  // create traps
  RetryCount := 0; // let's accumulate it between traps
  for I := 0 to Pred(NumberOfTraps) do
  begin
    ATrap := TMonster.Create;
    repeat
      ATrap.Data := SpawnList[Rnd.Random(SpawnList.Count)];
    until Rnd.Random < ATrap.MonsterData.SpawnFrequency;
    ATrap.Reset;
    repeat
      repeat
        X := Rnd.Random(SizeX);
        Y := Rnd.Random(SizeY);
      until PassableTiles[ATrap.PredSize][X + SizeX * Y] and (DistanceFromEntranceCache[X + SizeX * Y] > TPlayerCharacter.DefaultVisionRange * DistanceStep) and (Sqr(X - EntranceX) + Sqr(Y - EntranceY) > 36);
      ATrap.Teleport(X, Y);
      Inc(RetryCount);
    until NotCollidesWithOtherTraps or (RetryCount > MaxRetryCount);
    MonstersList.Insert(0, ATrap);
  end;
  FreeAndNil(SpawnList);
end;

procedure TMap.GenerateItems(const GenerateItems: Integer);
var
  I, K, L: Integer;
  X, Y: Int16;
  ItemData: TItemData;
  AItem: TInventoryItem;
  SpawnList: TItemsDataList;
  NewChest: TMonster;
begin
  if GenerateItems = 0 then
    Exit;
  SpawnList := TItemsDataList.Create(false);
  for I := 0 to Pred(ItemsDataList.Count) do
    if ItemsDataList[I].StartSpawningAtDepth <= CurrentDepth then
      SpawnList.Add(ItemsDataList[I]);
  if SpawnList.Count = 0 then
  begin
    ShowError('Cannot find any items to spawn at level %d', [Self.CurrentDepth]);
    FreeAndNil(SpawnList);
    Exit;
  end;

  // TEMPORARY TODO
  // create items
  K := GenerateItems;
  while K > 0 do
  begin
    if (Rnd.Random < 0.8) and (CurrentDepth > 0) then
    begin
      NewChest := TMonster.Create;
      if Rnd.Random < 0.001 * (CurrentDepth - 1) then // 0.1% at lvl.2 -> 3.0% at lvl.30
      begin
        repeat
          NewChest.Data := MimicsData[Rnd.Random(MimicsData.Count)]
        until (NewChest.MonsterData.StartSpawningAtDepth <= CurrentDepth) and (NewChest.MonsterData.StopSpawningAtDepth >= CurrentDepth);
      end else
      begin
        repeat
          NewChest.Data := ChestsData[Rnd.Random(ChestsData.Count)];
        until (NewChest.MonsterData.StartSpawningAtDepth <= CurrentDepth) and (NewChest.MonsterData.StopSpawningAtDepth >= CurrentDepth);
      end;
      NewChest.Reset;
      if ExtremaGeneration.Count > 0 then
      begin
        L := Rnd.Random(ExtremaGeneration.Count);
        X := ExtremaGeneration[L].X;
        Y := ExtremaGeneration[L].Y;
        ExtremaGeneration.Delete(L);
      end else
      begin // this shouldn't happen normally
        LogWarning('Ran out of ExtremaGeneration in GenerateItems');
        repeat
          X := Rnd.Random(SizeX);
          Y := Rnd.Random(SizeY);
        until PassableTiles[NewChest.PredSize][X + SizeX * Y];
      end;
      for I := 0 to MinInteger(K, Trunc(5 * Sqr(Rnd.Random)) - 1) do
      begin
        repeat
          ItemData := SpawnList[Rnd.Random(SpawnList.Count)];
        until Rnd.Random < ItemData.SpawnFrequency;
        AItem := TInventoryItem.NewItem(ItemData);
        AItem.AddRandomEnchantments(0.15 * CurrentDepth / 20, Sqrt(CurrentDepth));
        NewChest.Loot.Add(AItem);
        Dec(K);
      end;
      NewChest.Teleport(X, Y);
      MonstersList.Insert(0, NewChest);
    end else
    begin
      repeat
        X := Rnd.Random(SizeX);
        Y := Rnd.Random(SizeY);
      until PassableTiles[0][X + SizeX * Y];
      repeat
        ItemData := SpawnList[Rnd.Random(SpawnList.Count)];
      until Rnd.Random < ItemData.SpawnFrequency;
      AItem := TInventoryItem.NewItem(ItemData);
      AItem.AddRandomEnchantments(0.15 * CurrentDepth / 20, Sqrt(CurrentDepth));
      TMapItem.DropItem(X, Y, AItem, true);
      Dec(K);
    end;
  end;
  FreeAndNil(SpawnList);
end;

procedure TMap.CacheCharactersOnThisLevel;
var
  P: TPlayerCharacter;
begin
  CharactersOnThisLevel.Clear;
  for P in PlayerCharactersList do
    if P.AtMap = CurrentDepth then
      CharactersOnThisLevel.Add(P);
end;

procedure TMap.PositionCapturedCharacters;

  procedure SpawnGuards(const Player: TPlayerCharacter); // todo: PositionedObject, guards type
  var
    Dst: TDistanceMapArray;
    GuardsQuantity: Integer;
    Guard: TMonster;
    GuardsList: TMonstersDataList;
    AX, AY: Int16;
    I: Integer;
  begin
    Dst := DistanceMap(Player, true);

    GuardsList := TMonstersDataList.Create(false);
    for I := 0 to Pred(GuardsData.Count) do
      GuardsList.Add(GuardsData[I]);

    //GuardsQuantity := MinInteger(Rnd.Random(Round(Sqrt(CurrentDepth))) + 1, GuardsList.Count); // lvl.0,1 = 1; lvl.3 = 1-2; lvl.10 = 1-3; lvl.25 = 1-5
    GuardsQuantity := MinInteger(Rnd.Random(Round(Sqrt(CurrentDepth / 3))) + 1, GuardsList.Count); // lvl.0-6 = 1; lvl.7-18 = 1-2; lvl.19-35 = 1-3
    for I := 0 to Pred(GuardsQuantity) do
    begin
      repeat
        AX := Rnd.Random(SizeX);
        AY := Rnd.Random(SizeY);
      until PassableTiles[Player.PredSize][AX + SizeX * AY] and (Dst[AX + SizeX * AY] < 10 * 5);
      Guard := TMonster.Create;
      Guard.Data := GuardsList[Rnd.Random(GuardsList.Count)];
      GuardsList.Remove(Guard.MonsterData);
      Guard.Reset;
      Guard.Teleport(AX, AY);
      Guard.Ai.Guard := true;
      Guard.Ai.GuardPointX := AX;
      Guard.Ai.GuardPointY := AY;
      MonstersList.Add(Guard);
    end;
    FreeAndNil(GuardsList);
  end;

var
  I, SelectedExtrema: Integer;
  P: TPlayerCharacter;
  Captives: TPlayerCharactersList;
  EntranceToExitPath: TFloatCoordList;
  Seed, EntranceSeed: TIntCoordList;
  Dist: TDistanceMapArray;
  MaxDst: TDistanceQuant;
  AX, AY: Int16;
begin
  Captives := TPlayerCharactersList.Create(false);
  for P in CharactersOnThisLevel do
    if P.IsCaptured then
      Captives.Add(P);

  EntranceSeed := TIntCoordList.Create;
  EntranceSeed.Add(IntCoord(EntranceX, EntranceY));
  EntranceToExitPath := GetWaypoints(
      DistanceMap(PredPlayerColliderSize, true, EntranceSeed), // no need to stop at Exit, we guarantee that it's the longest route: but maybe it will change in future
      EntranceX, EntranceY, ExitX, ExitY, EntranceX, EntranceY);
  Seed := GetPathSeed(EntranceToExitPath);
  FreeAndNil(EntranceToExitPath);
  FreeAndNil(EntranceSeed);
  LongestRoute := Seed.Count;

  {for P in CharactersOnThisLevel do
    if not P.IsCaptured then
      P.AddSeed(Seed);} // let's remove this for now, and use EntranceX, EntranceY explicitly

  for P in Captives do
  begin
    Dist := DistanceMap(P.PredSize, true, Seed);
    //MaxDst := MaxDistance(Dist);
    if ExtremaGeneration.Count > 0 then
    begin
      MaxDst := 0;
      SelectedExtrema := -1;
      for I := 0 to Pred(ExtremaGeneration.Count) do
        if Dist[ExtremaGeneration[I].X + SizeX * ExtremaGeneration[I].Y] >= MaxDst then // >= to avoid extreme possible case that the only extremas exist have zero distance, won't hurt performance too much
        begin
          SelectedExtrema := I;
          MaxDst := Dist[ExtremaGeneration[I].X + SizeX * ExtremaGeneration[I].Y];
        end;
      if SelectedExtrema < 0 then
        raise Exception.Create('SelectedExtrema < 0 in PositionCapturedCharacters');
      AX := ExtremaGeneration[SelectedExtrema].X;
      AY := ExtremaGeneration[SelectedExtrema].Y;
      ExtremaGeneration.Delete(SelectedExtrema);
    end else
    begin
      LogWarning('Ran out of ExtremaGeneration in PositionCapturedCharacters');
      repeat
        AX := Rnd.Random(SizeX);
        AY := Rnd.Random(SizeY);
      until PassableTiles[P.PredSize][AX + SizeX * AY] and (Dist[AX + SizeX * AY] >= MaxDst - 5);
    end;
    Dist := nil;
    P.Teleport(AX, AY);
    P.Reset; // TODO: workaround, that P.CanAct = false and therefore actions with them as targets don't work
    P.AddSeed(Seed);
    SpawnGuards(P);
  end;
  FreeAndNil(Seed);
  FreeAndNil(Captives);
end;

function TMap.CapturedCharactersAtLevel: Integer;
var
  P: TPlayerCharacter;
begin
  Result := 0;
  for P in CharactersOnThisLevel do
    if P.IsCaptured then
      Inc(Result);
end;

procedure TMap.MakeGenerationCache;
var
  Seed: TIntCoordList;
  I: Integer;
begin
  Seed := TIntCoordList.Create;
  Seed.Add(IntCoord(EntranceX, EntranceY));
  DistanceFromEntranceCache := DistanceMap(PredPlayerColliderSize, true, Seed);
  FreeAndNil(Seed);
  MaxDistanceFromEntrance := MaxDistance(DistanceFromEntranceCache);
  ExtremaGeneration := TExtremaList.Create(false);
  for I := 0 to Pred(Extrema.Count) do
    if (Abs(Extrema[I].X - EntranceX) > 2) or (Abs(Extrema[I].Y - EntranceY) > 2) then
      ExtremaGeneration.Add(Extrema[I]);
end;

function TMap.MaxDistance(const MapArray: TDistanceMapArray): TDistanceQuant;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to Pred(SizeX * SizeY) do
    if (Result < MapArray[I]) and (MapArray[I] < UndefinedDistance) then
      Result := MapArray[I];
end;

procedure TMap.GenerateEntryAndExit;
var
  I, J: SizeInt;
  Seed: TIntCoordList;
  Dist: TDistanceMapArray;

  D, MaxDist: TDistanceQuant;
  MaxI, MaxJ: SizeInt;
begin
  Seed := TIntCoordList.Create;
  MaxDist := 0;
  MaxI := -1;
  MaxJ := -1;
  for I := 0 to Pred(Extrema.Count) do
  begin
    Seed.Clear;
    Seed.Add(IntCoord(Extrema[I].X, Extrema[I].Y));
    Dist := DistanceMap(PredPlayerColliderSize, true, Seed);
    for J := 0 to Pred(Extrema.Count) do
    begin
      D := Dist[Extrema[J].X + SizeX * Extrema[J].Y];
      if D > MaxDist then
      begin
        MaxDist := D;
        MaxI := I;
        MaxJ := J;
      end;
    end;
  end;

  LogNormal('Max Distance on map = %.0n', [MaxDist / DistanceStep]);
  EntranceX := Extrema[MaxI].X;
  EntranceY := Extrema[MaxI].Y;
  ExitX := Extrema[MaxJ].X;
  ExitY := Extrema[MaxJ].Y;

  //Extrema.Delete(MaxI); // entrance - do not delete it; monsters might want to check it from time to time
  //Extrema.Delete(MaxJ); -- this one may be changed by the previous one. We'll delete it by next check safer

  Seed.Clear;
  Seed.Add(IntCoord(ExitX, ExitY));
  Dist := DistanceMap(PredPlayerColliderSize, true, Seed); // TODO: cache, do not recalculate
  I := 0;
  while I < Extrema.Count do
    if Dist[Extrema[I].X + SizeX * Extrema[I].Y] <= DistanceStep * 10 then
      Extrema.Delete(I)
    else
      Inc(I);
  FreeAndNil(Seed);

  for I := 0 to Pred(Extrema.Count) do
    Extrema[I].LastChecked := Dist[Extrema[I].X + SizeX * Extrema[I].Y] - MaxDist; // this is distance from Exit, see above ---> in an inverted way = 0 at Entry, = -MaxDist at Exit
end;

procedure TMap.ClearGenerationCache;
begin
  DistanceFromEntranceCache := nil;
  FreeAndNil(ExtremaGeneration);
end;

procedure TMap.CalculateRenderRects;
var
  I: SizeInt;
begin
  RenderRects := 0;
  for I := 0 to Pred(SizeX * SizeY) do
    RenderRects += Length(FMap[I]);
  LogNormal('Render rects: %d', [RenderRects]);
end;

procedure TMap.FindDistanceMapExtrema;
var
  Seed: TIntCoordList;
  Dist: TDistanceMapArray;
  IX, IY: Integer;
  J: SizeInt;
  E: TExtremum;
begin
  Seed := TIntCoordList.Create;
  Seed.Add(IntCoord(EntranceX, EntranceY)); // TODO: It's seed of the map, not real entrance
  Dist := DistanceMap(PredPlayerColliderSize, true, Seed);
  FreeAndNil(Seed);
  J := 0;
  for IY := 0 to PredSizeY do
    for IX := 0 to PredSizeX do
    begin
      if Dist[J] = UndefinedDistance then
        Dist[J] := 0;
      Inc(J);
    end;

  //J := 0;
  for IY := 1 to PredSizeY - 1 do
    for IX := 1 to PredSizeX - 1 do
    begin
      J := IX + SizeX * IY; // TODO: Optimize
      if (Dist[J] > 0) and
         (Dist[J] > Dist[J - 1]) and (Dist[J] > Dist[J + 1]) and
         (Dist[J] > Dist[J - SizeX]) and (Dist[J] > Dist[J + SizeX]) and
         (Dist[J] > Dist[J - 1 - SizeX]) and (Dist[J] > Dist[J - 1 + SizeX]) and
         (Dist[J] > Dist[J + 1 - SizeX]) and (Dist[J] > Dist[J + 1 + SizeX]) then
      begin
        E := TExtremum.Create;
        E.X := IX;
        E.Y := IY;
        //E.LastChecked := -(Sqr(EntranceX - E.X) + Sqr(EntranceY - E.Y)); This no longer makes sense, TODO
        Extrema.Add(E);
      end;
      //Inc(J);
    end;
  Dist := nil;
  LogNormal('Extremas Found %d', [Extrema.Count]);
end;

function TMap.GeneratePassableMap(const PredColliderSize: Integer): TMoveArray;
var
  X, Y: Integer;
  I: SizeInt;

  function CanStepHere: ByteBool;
  var
    X1, Y1: Integer;
    J: SizeInt;
  begin
    for Y1 := 0 to PredColliderSize do
      begin
        J := (Y + Y1) * SizeX + X;
        for X1 := 0 to PredColliderSize do
        begin
          // TODO: Cache zero-size-map?
          if not CanMove(J) then
            Exit(false);
          Inc(J);
        end;
      end;
    Exit(true);
  end;

begin
  Result := nil;
  SetLength(Result, FSizeX * FSizeY);
  I := 0;
  for Y := 0 to PredSizeY do
    for X := 0 to PredSizeX do
    begin
      if (X = 0) or (Y = 0) or (X >= PredSizeX - PredColliderSize) or (Y >= PredSizeY - PredColliderSize) then
        Result[I] := false
      else
        Result[I] := CanStepHere;
      Inc(I);
    end;
end;

function TMap.DistanceMap(const PredSize: Byte; const IgnoreVisible: Boolean;
  const Seed: TIntCoordList; const StopAtDestination: Boolean;
  const DestinationX: Int16; const DestinationY: Int16): TDistanceMapArray;
var
  X, Y: Integer;
  MinX, MaxX, MinY, MaxY: Integer;
  NoMoreMoves: Boolean;
  I, DestinationI: SizeInt;
  Target: TIntCoord;
  CurrentPassableTiles: TMoveArray;

  procedure TryMovementWithDeltas(const DeltaX, DeltaY: Integer);
  var
    ThatDistance, NewDistance, DeltaDistance: TDistanceQuant;
  begin
    ThatDistance := Result[I + DeltaX + SizeX * DeltaY];
    if ThatDistance < UndefinedDistance then
    begin
      if (DeltaX <> 0) and (DeltaY <> 0) then
        DeltaDistance := DistanceStepDiagonal
      else
        DeltaDistance := DistanceStep;
      NewDistance := ThatDistance + DeltaDistance;
      if Result[I] > NewDistance then
      begin
        Result[I] := NewDistance;
        NoMoreMoves := false;
        // note that MinX/Y can be 0 or PredSize here - so adding deltas may result in range check error; While we are "protected" by the fact that the dungeon is surrounded by 1-tile border, it's still not a safe thing to do.
        if (X = MinX) and (MinX > 0) then
          Dec(MinX);
        if (X = MaxX) and (MaxX < PredSizeX) then
          Inc(MaxX);
        if (Y = MinY) and (MinY > 0) then
          Dec(MinY);
        if (Y = MaxY) and (MaxY < PredSizeY) then
          Inc(MaxY);
      end;
    end;
  end;

begin
  CurrentPassableTiles := PassableTiles[PredSize];
  Result := nil;
  SetLength(Result, Length(CurrentPassableTiles));
  FillByte(Result[0], Length(Result) * SizeOf(TDistanceQuant), 255); // note as TDistanceQuant is UInt32 - this will be equal to High(UInt32) as defined by DistanceUnreachable constant
  DestinationI := DestinationX + SizeX * DestinationY;
  MinX := SizeX;
  MaxX := 0;
  MinY := SizeY;
  MaxY := 0;
  for Target in Seed do
  begin
    if MinX > Target[0] then
      MinX := Target[0];
    if MaxX < Target[0] then
      MaxX := Target[0];
    if MinY > Target[1] then
      MinY := Target[1];
    if MaxY < Target[1] then
      MaxY := Target[1];
    Result[Target[0] + SizeX * Target[1]] := 0;
  end;
  if MinX > 0 then
    Dec(MinX);
  if MaxX < PredSizeX then
    Inc(MaxX);
  if MinY > 0 then
    Dec(MinY);
  if MaxY < PredSizeY then
    Inc(MaxY);

  repeat
    NoMoreMoves := true;
    for Y := MinY to MaxY do
    begin
      I := MinX + Y * SizeX;
      for X := MinX to MaxX do
      begin
        if CurrentPassableTiles[I] and (IgnoreVisible or (Visible[I] >= RememberedVisible)) then
        begin
          TryMovementWithDeltas(-1,  0);
          TryMovementWithDeltas(+1,  0);
          TryMovementWithDeltas( 0, -1);
          TryMovementWithDeltas( 0, +1);
          TryMovementWithDeltas(-1, +1);
          TryMovementWithDeltas(+1, +1);
          TryMovementWithDeltas(-1, -1);
          TryMovementWithDeltas(+1, -1);
        end;
        Inc(I);
      end;
    end;
  until NoMoreMoves or (StopAtDestination and (Result[DestinationI] < DistanceUnreachable));
end;

function TMap.DistanceMap(const Actor: TObject; const IgnoreVisible: Boolean): TDistanceMapArray;
var
  Seed: TIntCoordList;
begin
  if not (Actor is TPositionedObject) then
    raise Exception.CreateFmt('DistanceMap can work only on TPositionedObject, got %s instead', [Actor.ClassName]);
  Seed := TIntCoordList.Create;
  TPositionedObject(Actor).AddSeed(Seed);
  Result := DistanceMap(TPositionedObject(Actor).PredSize, IgnoreVisible, Seed);
  FreeAndNil(Seed);
end;

function TMap.FloodFill(const SrcArray: TMoveArray; const StartX, StartY: UInt32): TMoveArray;
var
  X, Y: Integer;
  I: SizeInt;
  Finished: Boolean;
begin
  Result := nil;
  SetLength(Result, SizeX * SizeY);
  FillByte(Result[0], Length(Result), Byte(ByteBool(false)));
  Result[StartX + SizeX * StartY] := true;
  repeat
    Finished := true;
    I := 0;
    for Y := 0 to PredSizeY do
      for X := 0 to PredSizeX do
      begin
        if not Result[I] and SrcArray[I] then
         if ((X > 0) and Result[I - 1]) or
            ((X < PredSizeX) and Result[I + 1]) or
            ((Y > 0) and Result[I - SizeX]) or
            ((Y < PredSizeY) and Result[I + SizeX]) then
         begin
           Result[I] := true;
           Finished := false;
         end;
        Inc(I);
      end;
  until Finished;
end;

procedure TMap.GetNearestPassableTile(const PredColliderSize: Integer;
  const IgnoreVisible: Boolean; const ToX, ToY: Int16; out OutX, OutY: Int16);
var
  Range: Integer;

  procedure GetNearestClick;
  var
    DX, DY: Integer;
  begin
    for DX := -Range to Range do
      for DY := -Range to Range do
        if Sqr(DX) + Sqr(DY) <= Sqr(Range) then
        begin
          OutX := ToX + DX;
          OutY := ToY + DY;
          if (OutX >= 0) and (OutX <= Map.PredSizeX) and (OutY >= 0) and (OutY <= Map.PredSizeY) and Map.PassableTiles[PredColliderSize][OutX + Map.SizeX * OutY] and (IgnoreVisible or (Visible[OutX + SizeX * OutY] >= RememberedVisible)) then
            Exit;
        end;
  end;

begin
  Range := 0;
  OutX := ToX;
  OutY := ToY;
  while (OutX < 0) or (OutX > Map.PredSizeX) or (OutY < 0) or (OutY > Map.PredSizeY) or (not PassableTiles[PredColliderSize][OutX + SizeX * OutY]) or ((not IgnoreVisible) and (Visible[OutX + SizeX * OutY] < RememberedVisible)) do
  begin
    GetNearestClick;
    Inc(Range);
  end;
end;

function TMap.GetWaypoints(const DistMap: TDistanceMapArray; const FromX,
  FromY, ToX, ToY: Single; const FromXInt, FromYInt: Int16): TFloatCoordList;
var
  IX, IY: Int16;
  I: SizeInt;

  procedure FindNearestNavPointWorkaround;
  var
    Dist, DX, DY: Int16;
  begin
    Dist := 1;
    while true do
    begin
      for DX := -Dist to Dist do
        for DY := -Dist to Dist do
          if (IX + DX > 0) and (IX + DX < PredSizeX) and
             (IY + DY > 0) and (IY + DY < PredSizeY) then
          if DistMap[IX + DX + SizeX * (IY + DY)] < DistanceUnreachable then
          begin
            IX := IX + DX;
            IY := IY + DY;
            I := IX + SizeX * IY;
            Exit;
          end;
      Inc(Dist);
    end;
  end;

var
  DDX, DDY, DMX, DMY: Integer; // delta-delta-x, delta-move-x
  D: TDistanceQuant;
begin
  Result := TFloatCoordList.Create;
  // to avoid resize in future
  Result.Capacity := LongestRoute;
  IX := Trunc(ToX);
  IY := Trunc(ToY);
  I := IX + SizeX * IY;
  if DistMap[I] = DistanceUnreachable then
  begin
    FindNearestNavPointWorkaround;
    Result.Add(FloatCoord(IX, IY));
    I := IX + SizeX * IY;
  end else
    Result.Add(FloatCoord(ToX, ToY));

  repeat
    D := DistMap[I];
    DMX := 0;
    DMY := 0;
    for DDX := -1 to 1 do
      for DDY := -1 to 1 do
        if (DDX <> 0) or (DDY <> 0) then
          if (DistMap[I + DDX + SizeX * DDY] < D) then
          begin
            D := DistMap[I + DDX + SizeX * DDY];
            DMX := DDX;
            DMY := DDY;
          end;

    IX += DMX;
    IY += DMY;
    Result.Add(FloatCoord(IX + Frac(ToX), IY + Frac(ToY)));
    if (DMX = 0) and (DMY = 0) and not (I = FromXInt + FromYInt * SizeX) then
    begin
      // Antifreeze. This _May be_ actually a valid solution
      // it might happen in case there's just nowhere to go
      // I hope it gets caught by (I = FromXInt + FromYInt * SizeX) above, but let's keep an eye on it
      ShowError('(DMX = 0) and (DMY = 0)');
      Exit;
    end;
    I := IX + SizeX * IY;
  until (I = FromXInt + FromYInt * SizeX);
  // ugly workaround for broken first waypoint
  Result.Delete(Pred(Result.Count));
  Result.Add(FloatCoord(FromX, FromY));
end;

{$IFDEF DijkstraPathfinding}
function TMap.GenerateDijkstra(const PredColliderSize: Integer; const IgnoreVisible: Boolean;
  const FromX, FromY, ToX, ToY: Int16): TDirectionArray;
var
  DX, DY: Integer;
  MinX, MaxX, MinY, MaxY: Integer;
  NoMoreMoves: Boolean;
  //TotalMovementDistance: Integer;
  NewMovementMap, OldMovementMap: TDirectionArray;
  I, TargetI: SizeInt;

  procedure TryMovementWithDeltas(const DeltaX, DeltaY: Integer; const MovementDirection: TMovementDirection);
  var
    J: SizeInt;
  begin
    J := I + DeltaX + DeltaY * SizeX;
    if (OldMovementMap[J] = mdUndefined) and
      { Second part of "if" is unfortunate, but we try to optimize everything,
        as a result - diagonal movement can come "before" straiht movement
        So in this case we "overwrite" the diagonal movement with strait one }
      ((NewMovementMap[J] = mdUndefined) or (NewMovementMap[J] > DiagonalMovementMD) and (MovementDirection < DiagonalMovementMD)) then
    begin
      if PassableTiles[PredColliderSize][J] and
        (IgnoreVisible or (Visible[J] >= RememberedVisible)) then
      begin
        NewMovementMap[J] := MovementDirection;
        NoMoreMoves := false;
        if (DX = MinX) and (FromX + MinX >= 0) then
          Dec(MinX);
        if (DX = MaxX) and (FromX + MaxX <= PredSizeX) then
          Inc(MaxX);
        if (DY = MinY) and (FromY + MinY >= 0) then
          Dec(MinY);
        if (DY = MaxY) and (FromY + MaxY <= PredSizeY) then
          Inc(MaxY);
      end else
        OldMovementMap[J] := mdWall;
    end
  end;

begin
  NewMovementMap := nil;
  OldMovementMap := nil;
  SetLength(NewMovementMap, Length(FMap));
  SetLength(OldMovementMap, Length(FMap));
  FillByte(NewMovementMap[0], Length(NewMovementMap), mdUndefined);
  NewMovementMap[FromX + FromY * SizeX] := mdZero;
  if (ToX = FromX) and (ToY = FromY) {and StopAtTarget} then
  begin
    // We return something initialized here, to avoid returning nil
    Exit(NewMovementMap);
  end;
  TargetI := ToX + ToY * SizeX;

  //TotalMovementDistance := 0;
  MinX := 0;
  MaxX := 0;
  MinY := 0;
  MaxY := 0;
  repeat
    Move(NewMovementMap[0], OldMovementMap[0], Length(NewMovementMap));
    NoMoreMoves := true;
    for DX := MinX to MaxX do
      for DY := MinY to MaxY do
      begin
        I := FromX + DX + (FromY + DY) * SizeX; // FromI + DX + DY * SizeX
        if (OldMovementMap[I] > CanGoMD) then
        begin
          //Warning: We've lost a big optimization here and call all of the below every initialized tile; but let's make them work properly and then try optimize again
          TryMovementWithDeltas(+1,  0, mdLeft);
          TryMovementWithDeltas(-1,  0, mdRight);
          TryMovementWithDeltas( 0, +1, mdDown);
          TryMovementWithDeltas( 0, -1, mdUp);
          TryMovementWithDeltas(+1, -1, mdUpLeft);
          TryMovementWithDeltas(-1, -1, mdUpRight);
          TryMovementWithDeltas(+1, +1, mdDownLeft);
          TryMovementWithDeltas(-1, +1, mdDownRight);
        end;
      end;
    {Inc(TotalMovementDistance);}
  until {(TotalMovementDistance > MaxMovementRange) or }NoMoreMoves or ({StopAtTarget and} (NewMovementMap[TargetI] <> mdUndefined));

  Result := NewMovementMap;
end;

function TMap.GetWaypoints(const Directions: TDirectionArray; const FromX,
  FromY, ToX, ToY: Single; const FromXInt, FromYInt: Int16): TFloatCoordList;
var
  IX, IY: Int16;
  I: SizeInt;

  procedure FindNearestNavPointWorkaround;
  var
    Dist, DX, DY: Int16;
  begin
    Dist := 1;
    while true do
    begin
      for DX := -Dist to Dist do
        for DY := -Dist to Dist do
          if (IX + DX > 0) and (IX + DX < PredSizeX) and
             (IY + DY > 0) and (IY + DY < PredSizeY) then
          if Directions[IX + DX + SizeX * (IY + DY)] > CanGoMD then
          begin
            IX := IX + DX;
            IY := IY + DY;
            I := IX + SizeX * IY;
            Exit;
          end;
      Inc(Dist);
    end;
  end;

  function DirectionToX(const Dir: TMovementDirection): Int16; inline;
  begin
    case Dir of
      mdWall, mdUndefined, mdZero: Exit(0);
      mdUp, mdDown: Exit(0);
      mdUpRight, mdRight, mdDownRight: Exit(1);
      mdDownLeft, mdLeft, mdUpLeft: Exit(-1);
      else
        raise Exception.Create('Unexpected Dir');
    end;
  end;
  function DirectionToY(const Dir: TMovementDirection): Int16; inline;
  begin
    case Dir of
      mdWall, mdUndefined, mdZero: Exit(0);
      mdLeft, mdRight: Exit(0);
      mdUp, mdUpLeft, mdUpRight: Exit(1);
      mdDown, mdDownLeft, mdDownRight: Exit(-1);
      else
        raise Exception.Create('Unexpected Dir');
    end;
  end;

begin
  Result := TFloatCoordList.Create;
  // to avoid resize in future
  Result.Capacity := LongestRoute;
  IX := Trunc(ToX);
  IY := Trunc(ToY);
  I := IX + SizeX * IY;
  if Directions[I] <= CanGoMD then
  begin
    FindNearestNavPointWorkaround;
    Result.Add(FloatCoord(IX, IY));
    I := IX + SizeX * IY;
  end else
    Result.Add(FloatCoord(ToX, ToY));

  repeat
    IX += DirectionToX(Directions[I]);
    IY += DirectionToY(Directions[I]);
    Result.Add(FloatCoord(IX + Frac(ToX), IY + Frac(ToY)));
    I := IX + SizeX * IY;
  until I = FromXInt + FromYInt * SizeX;
  // ugly workaround for broken first waypoint
  Result.Delete(Pred(Result.Count));
  Result.Add(FloatCoord(FromX, FromY));
end;
{$ENDIF}

function TMap.GetPathSeed(const Waypts: TFloatCoordList): TIntCoordList;
var
  I: Integer;
begin
  Result := TIntCoordList.Create;
  for I := 0 to Pred(Waypts.Count) do
    Result.Add(IntCoord(Trunc(Waypts[I][0]), Trunc(Waypts[I][1])));
end;

procedure TMap.ScheduleUpdateVisible;
begin
  FScheduleUpdateVisible := true;
end;

procedure TMap.UpdateVisible;
var
  P: TPlayerCharacter;
  ProcessDisorientation: Boolean;
begin
  if FScheduleUpdateVisible then
  begin
    FScheduleUpdateVisible := false;
    ClearActiveVisible;
    ProcessDisorientation := Disorientation > 0;
    for P in CharactersOnThisLevel do
      if P.CanAct then
        LookAround(P.LastTileX + P.Size div 2, P.LastTileY + P.Size div 2, P.VisibleRange);
    if ProcessDisorientation and (Disorientation <= 0) and (not ViewGame.CurrentCharacter.Disoriented) and (not ViewGame.CurrentCharacter.Unsuspecting) then
      RememberVisible;
  end;
end;

procedure TMap.LookAround(const AX, AY: Int16; const VisibleRange: Single);

  procedure WriteVisible(const A: SizeInt); inline;
  begin
    if Visible[A] = ForgottenVisible then
      Dec(Disorientation);
    Visible[A] := DirectlyVisible;
  end;

var
  DX, DY, IX, IY: Integer;
  Slope: Single;
  I: SizeInt;
  VisibilityRange: Integer;
begin
  VisibilityRange := Ceil(VisibleRange);
  //ClearCurrentlyVisible;
  for IX := -VisibilityRange to VisibilityRange do
    for IY := -VisibilityRange to VisibilityRange do
      if Sqr(IX) + Sqr(IY) <= Sqr(VisibleRange) then
        if (IX = 0) and (IY = 0) then
          WriteVisible(AX + SizeX * AY)
        else
        if Abs(IX) > Abs(IY) then
        begin
          DX := 0;
          DY := 0;
          Slope := Single(IY) / Single(IX);
          repeat
            DX += Sign(IX);
            I := AX + DX + SizeX * (AY + DY);
            WriteVisible(I);
            if not CanSee(I) then
              break;
            if Trunc(DX * Slope) <> Trunc((DX + Sign(IX)) * Slope) then
            begin
              DY += Sign(IY);
              I := AX + DX + SizeX * (AY + DY);
              WriteVisible(I);
              if not CanSee(I) then
                break;
            end;
          until (DX = IX) and (DY = IY);
        end else
        begin
          DX := 0;
          DY := 0;
          Slope := Single(IX) / Single(IY);
          repeat
            DY += Sign(IY);
            I := AX + DX + SizeX * (AY + DY);
            WriteVisible(I);
            if not CanSee(I) then
              break;
            if Trunc(DY * Slope) <> Trunc((DY + Sign(IY)) * Slope) then
            begin
              DX += Sign(IX);
              I := AX + DX + SizeX * (AY + DY);
              WriteVisible(I);
              if not CanSee(I) then
                break;
            end;
          until (DX = IX) and (DY = IY);
        end;
end;

function TMap.Ray(const PredSize: Byte; const FromX, FromY: Int16; var ToX, ToY: Int16): Boolean;
var
  IX, IY, DX, DY: Integer;
  Slope: Single;
begin
  if (FromX = ToX) and (FromY = ToY) then
    Exit(true);

  IX := ToX - FromX;
  IY := ToY - FromY;
  if Abs(IX) > Abs(IY) then
  begin
    DX := 0;
    DY := 0;
    Slope := Single(IY) / Single(IX);
    repeat
      ToX := FromX + DX;
      ToY := FromY + DY;
      DX += Sign(IX);
      if not PassableTiles[PredSize][FromX + DX + SizeX * (FromY + DY)] then // = Can't Pass.
        Exit(false);
      if Trunc(DX * Slope) <> Trunc((DX + Sign(IX)) * Slope) then
      begin
        DY += Sign(IY);
        if not PassableTiles[PredSize][FromX + DX + SizeX * (FromY + DY)] then
          Exit(false);
      end;
    until (DX = IX) and (DY = IY);
  end else
  begin
    DX := 0;
    DY := 0;
    Slope := Single(IX) / Single(IY);
    repeat
      ToX := FromX + DX;
      ToY := FromY + DY;
      DY += Sign(IY);
      if not PassableTiles[PredSize][FromX + DX + SizeX * (FromY + DY)] then
        Exit(false);
      if Trunc(DY * Slope) <> Trunc((DY + Sign(IY)) * Slope) then
      begin
        DX += Sign(IX);
        if not PassableTiles[PredSize][FromX + DX + SizeX * (FromY + DY)] then
          Exit(false);
      end;
    until (DX = IX) and (DY = IY);
  end;
  ToX := FromX + IX;
  ToY := FromY + IY;
  Result := true;
end;

function TMap.Ray(const PredSize: Byte; const FromX, FromY, ToX, ToY: Single): Boolean;
var
  TX, TY: Int16;
begin
  TX := Round(ToX); // TEMPORARY TODO
  TY := Round(ToY);
  Result := Ray(PredSize, Int16(Round(FromX)), Int16(Round(FromY)), TX, TY);
end;

function TMap.CanMove(const Tile: SizeInt): Boolean;
begin
  Exit(Length(FMap[Tile]) = 1);
end;

function TMap.CanSee(const Tile: SizeInt): Boolean;
begin
  Exit(Length(FMap[Tile]) = 1);
end;

procedure TMap.MonstersToIdle;
const
  MaxRetryCount = Integer(10000);
var
  X, Y: Int16;
  Monster: TMonster;
  RetryCount: Integer;
begin
  // TODO: if target is "current player", no need to reset if attacking a different one
  for Monster in MonstersList do
    if Monster.CanAct then
    begin
      Monster.CurrentAction := TActionIdle.NewAction(Monster);
      if Monster.Aggressive and Monster.LineOfSight(ViewGame.CurrentCharacter) then
      begin
        RetryCount := 0;
        repeat
          repeat
            X := Rnd.Random(SizeX);
            Y := Rnd.Random(SizeY);
          until PassableTiles[Monster.PredSize][X + SizeX * Y];
          Inc(RetryCount);
        until not Monster.LineOfSight(ViewGame.CurrentCharacter) or (RetryCount > MaxRetryCount);
        Monster.Teleport(X, Y);
        Monster.Ai.Guard := false;
        Monster.Ai.SoftGuard := false;
      end;
    end;
  MarksList.Clear; // Ugh? For some reason resetting purple slime to idle doesn't clear it's marks TODO
end;

function TMap.MonsterSeen(const AX, AY: Int16; const Range: Single): Boolean;
var
  M: TMonster;
begin
  for M in Map.MonstersList do
    if M.CanAct and M.Aggressive and (M.DistanceTo(AX, AY) <= Range) and M.LineOfSight(AX, AY) then
      Exit(true);
  Exit(false);
end;

function TMap.NoMonstersNearby(const AX, AY: Int16; const CollisionMargin: Int16): Boolean;
var
  M: TMonster;
begin
  for M in Map.MonstersList do
    if M.CanAct and not M.MonsterData.Chest and not M.MonsterData.Trap and M.CollidesInt(AX, AY, CollisionMargin) then
      Exit(false);
  if MonsterSeen(AX, AY, ViewGame.CurrentCharacter.VisibleRange + CollisionMargin) then
    Exit(false);
  Exit(true);
end;

function TMap.NoMarksNearby(const Obj: TPositionedObject;
  const AdditionalMargin: Single): Boolean;
var
  I: Integer;
begin
  for I := 0 to Pred(MarksList.Count) do
    if MarksList[I].Collides(Obj, AdditionalMargin) then
      Exit(false);
  Exit(true);
end;

function TMap.MonstersAttacking: Boolean;
var
  M: TMonster;
begin
  for M in MonstersList do
    if not M.Unsuspecting then //todo and target = current character
      Exit(true);
  Exit(false);
end;

function TMap.CapturedCharacterOnLevel(const DungeonLevel: Byte): Integer;
var
  P: TPlayerCharacter;
begin
  Result := 0;
  for P in Map.PlayerCharactersList do
    if P.IsCaptured and (P.AtMap = DungeonLevel) then
      Inc(Result);
end;

function TMap.DeepestLevel: Byte;
var
  P: TPlayerCharacter;
begin
  Result := 0;
  for P in Map.PlayerCharactersList do
    if (P.AtMap > Result) and (not P.IsStranger) then
      Result := P.AtMap;
end;

function TMap.UnlockedCharacters: Integer;
var
  P: TPlayerCharacter;
begin
  Result := 0;
  for P in Map.PlayerCharactersList do
    if not P.IsStranger then
      Inc(Result);
end;

function TMap.UnlockedDestinations: TSetOfByte;
var
  I: Byte;
begin
  Result := [1];
  if DeepestLevel > 8 then
    Result += [8];
  if DeepestLevel > 14 then
    Result += [14];
  I := 18;
  repeat
    if DeepestLevel > I then
      Result += [I];
    Inc(I, 4);
  until (I >= DeepestLevel) or (Integer(I) + 5 >= MaxDungeonDepth);
end;

procedure TMap.Save(const Element: TDOMElement);
var
  MapElement: TDOMElement;
  SaveElement: TDOMELement;
  Mark: TMarkAbstract;
  Monster: TMonster;
  Player: TPlayerCharacter;
  MapItem: TMapItem;

  PatrolRecord: TActorData;
  Extremum: TExtremum;
begin
  MapElement := Element.CreateChild(Signature);
  MapElement.AttributeSet('CurrentDepth', CurrentDepth);
  MapElement.AttributeSet('SizeX', SizeX);
  MapElement.AttributeSet('SizeY', SizeY);
  MapElement.AttributeSet('Tileset', Tileset);
  MapElement.AttributeSet('TilesetRandomSeed', TilesetRandomSeed);
  MapElement.AttributeSet('RenderTileSize', RenderTileSize);
  MapElement.AttributeSet('EntranceX', EntranceX);
  MapElement.AttributeSet('EntranceY', EntranceY);
  MapElement.AttributeSet('ExitX', ExitX);
  MapElement.AttributeSet('ExitY', ExitY);
  MapElement.AttributeSet('Map', EncodeStringBase64(MapArrayToString(FMap)));
  MapElement.AttributeSet('Visible', EncodeStringBase64(VisibleArrayToString(Visible)));
  MapElement.AttributeSet('EnemySpawnTime', EnemySpawnTime);
  MapElement.AttributeSet('TimeSpentOnTheMap', TimeSpentOnTheMap);
  MapElement.AttributeSet('Disorientation', Disorientation); // TODO: per-character
  MapElement.AttributeSet('LongestRoute', LongestRoute);

  // TODO: More universal saving of SerializableObjects
  for Mark in MarksList do
  begin
    SaveElement := MapElement.CreateChild(TMarkAbstract.Signature);
    try
      Mark.Save(SaveElement);
    except
      ShowError('ERROR: Failed to save mark %s.', [Mark.ClassName]);
      MapElement.RemoveChild(SaveElement).Free;
    end;
  end;

  for Monster in MonstersList do
  begin
    SaveElement := MapElement.CreateChild(TMonster.Signature);
    try
      Monster.Save(SaveElement);
    except
      if Monster.Data <> nil then
        ShowError('ERROR: Failed to save monster %s.', [Monster.Data.Id])
      else
        ShowError('ERROR: Failed to save monster %s.', [Monster.ClassName]);
      MapElement.RemoveChild(SaveElement).Free;
    end;
  end;

  for MapItem in MapItemsList do
  begin
    SaveElement := MapElement.CreateChild(TMapItem.Signature);
    try
      MapItem.Save(SaveElement);
    except
      if (MapItem.Item <> nil) and (MapItem.Item.Data <> nil) then
        ShowError('ERROR: Failed to save item %s.', [MapItem.Item.Data.Id])
      else
        ShowError('ERROR: Failed to save item %s.', [MapItem.ClassName]);
      MapElement.RemoveChild(SaveElement).Free;
    end;
  end;

  for Player in PlayerCharactersList do
  begin
    SaveElement := MapElement.CreateChild(TPlayerCharacter.Signature);
    try
      Player.Save(SaveElement);
    except
      if Player.Data <> nil then
        ShowError('ERROR: Failed to save player %s.', [Player.Data.DisplayName])
      else
        ShowError('ERROR: Failed to save player %s.', [Player.ClassName]);
      MapElement.RemoveChild(SaveElement).Free;
    end;
  end;

  for PatrolRecord in PatrolList do
  begin
    SaveElement := MapElement.CreateChild('Patrol');
    SaveElement.AttributeSet('Id', PatrolRecord.Id);
  end;

  for Extremum in Extrema do
    Extremum.Save(MapElement.CreateChild(Extremum.ClassName));
end;

procedure TMap.Load(const Element: TDOMElement);
var
  I: TXMLElementIterator;
  J: Integer;
begin
  Init(Element.AttributeInteger('SizeX'), Element.AttributeInteger('SizeY'));
  CurrentDepth := Element.AttributeInteger('CurrentDepth');
  Tileset := Element.AttributeInteger('Tileset');
  TilesetRandomSeed := Element.AttributeCardinalDef('TilesetRandomSeed', Rnd.Random32bit); // TODO: not Def
  EntranceX := Element.AttributeInteger('EntranceX');
  EntranceY := Element.AttributeInteger('EntranceY');
  ExitX := Element.AttributeInteger('ExitX');
  ExitY := Element.AttributeInteger('ExitY');
  FMap := StringToMapArray(DecodeStringBase64(Element.AttributeString('Map')), SizeX * SizeY);
  Visible := StringToVisibleArray(DecodeStringBase64(Element.AttributeString('Visible')), SizeX * SizeY);
  EnemySpawnTime := Element.AttributeSingle('EnemySpawnTime');
  TimeSpentOnTheMap := Element.AttributeSingleDef('TimeSpentOnTheMap', 60.0); // TODO: not def
  Disorientation := Element.AttributeIntegerDef('Disorientation', 0); // TODO: per-character; TODO: not def
  LongestRoute := Element.AttributeIntegerDef('LongestRoute', 1024);
  CalculateRenderRects;
  GeneratePassableTilesForColliders;

  if RenderTileSize <> Element.AttributeIntegerDef('RenderTileSize', 0) then
  begin
    ShowError('Tile render size has changed from %d to %d! Regenerating tileset.', [Element.AttributeIntegerDef('RenderTileSize', 0), RenderTileSize]);
    for J := 0 to Pred(SizeX * SizeY) do
      if Length(FMap[J]) = 1 then
        FMap[J] := Floor
      else
        FMap[J] := Wall;
  end;

  I := Element.ChildrenIterator(TMonster.Signature);
  try
    while I.GetNext do
      try
        MonstersList.Add(TMonster.LoadClass(I.Current) as TMonster);
      except
        ShowError('ERROR: Failed to load monster "%s"', [I.Current.AttributeStringDef('Data.Id', 'N/A')]);
      end;
  finally
    FreeAndNil(I)
  end;

  I := Element.ChildrenIterator(TMapItem.Signature);
  try
    while I.GetNext do
      try
        MapItemsList.Add(TMapItem.LoadClass(I.Current) as TMapItem);
      except
        ShowError('ERROR: Map item was not loaded');
      end;
  finally
    FreeAndNil(I)
  end;

  I := Element.ChildrenIterator(TPlayerCharacter.Signature);
  try
    while I.GetNext do
      try
        PlayerCharactersList.Add(TPlayerCharacter.LoadClass(I.Current) as TPlayerCharacter);
      except
        ShowError('ERROR: Player Character was not loaded');
      end;

  finally
    FreeAndNil(I)
  end;
  CacheCharactersOnThisLevel;

  { We can't load marks yet: need a way to get Target properly. Just drop them all. }
  I := Element.ChildrenIterator(TMarkAbstract.Signature);
  try
    while I.GetNext do
      ;
      //MarksList.Add(TMarkAbstract.Load(I.Current));
  finally
    FreeAndNil(I)
  end;

  for J := 0 to Pred(MonstersList.Count) do
    MonstersList[J].AfterDeserealization;
  for J := 0 to Pred(MapItemsList.Count) do
    MapItemsList[J].AfterDeserealization;
  for J := 0 to Pred(PlayerCharactersList.Count) do
    PlayerCharactersList[J].AfterDeserealization;
  for J := 0 to Pred(MarksList.Count) do
    MarksList[J].AfterDeserealization;

  I := Element.ChildrenIterator('Patrol');
  try
    while I.GetNext do
      PatrolList.Add(MonstersDataDictionary[I.Current.AttributeString('Id')]);
  finally
    FreeAndNil(I)
  end;

  ParticlesList.Clear; // no need to load them, they no longer make sense? Todo

  I := Element.ChildrenIterator(TExtremum.ClassName);
  try
    while I.GetNext do
      try
        Extrema.Add(TExtremum.LoadClass(I.Current) as TExtremum);
      except
        ShowError('ERROR: Extremum was not loaded in %s', [Self.ClassName]);
      end;
  finally
    FreeAndNil(I)
  end;
  if Extrema.Count = 0 then
  begin
    ShowError('Extrema were not found in save game. Rebuilding.');
    FindDistanceMapExtrema; // TODO: remove
  end;

  PrepareTilesetMap;
end;

procedure InitMap;
begin
  Map := TMap.Create;
end;

procedure FreeMap;
begin
  { Some things need to "remove themselves from the map" therefore map needs to
    still exist in "fully operational state. Map.Clear *seems* to do everything
    that is needed to clean up the instance. So after calling it, direclty freeing
    the map should be safe enough }
  Map.Clear;
  FreeAndNil(Map);
end;

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Load and inject screen effects into a TCastleView }
unit GameScreenEffect;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Classes,
  CastleScreenEffects, X3DNodes, X3DLoad, X3DFields,
  CastleColors, CastleUiControls;

type
  { Screen effects container UI }
  TScreenEffect = class(TCastleScreenEffects)
  strict private
    FilmGrainEffectRoot: TX3DRootNode;
    FilmGrainEffect: TScreenEffectNode;
    FilmGrainEffectRndX1Uniform, FilmGrainEffectRndY1Uniform,
      FilmGrainEffectRndX2Uniform, FilmGrainEffectRndY2Uniform: TSFFloat;
    { load shaders }
    procedure Load;
  public
    procedure Enable;
    { Detaches all children from EffectParent and packs them back into this TCastleScreenEffects }
    procedure Inject(const EffectParent: TCastleUserInterface);
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleVectors,
  GameConfiguration, GameRandom;

procedure TScreenEffect.Enable;
begin
  FilmGrainEffect.Enabled := Configuration.ScreenEffects;
end;

procedure TScreenEffect.Inject(const EffectParent: TCastleUserInterface);
var
  ThisControl: TCastleUserInterface;
  I: Integer;
begin
  for I := EffectParent.ControlsCount - 1 downto 0 do
  begin
    ThisControl := EffectParent.Controls[I];
    EffectParent.RemoveControl(ThisControl);
    InsertFront(ThisControl);
  end;
  EffectParent.InsertFront(Self);
end;

procedure TScreenEffect.Load;
begin
  FilmGrainEffectRoot := LoadNode('castle-data:/shaders/film_grain.x3dv');
  FilmGrainEffect := FilmGrainEffectRoot.FindNode(TScreenEffectNode, 'FilmGrainEffect') as TScreenEffectNode;
  AddScreenEffect(FilmGrainEffect);
  FilmGrainEffectRndX1Uniform := FilmGrainEffect.FindNode(TX3DNode, 'Shader').Field('rndx1', true) as TSFFloat;
  FilmGrainEffectRndY1Uniform := FilmGrainEffect.FindNode(TX3DNode, 'Shader').Field('rndy1', true) as TSFFloat;
  FilmGrainEffectRndX2Uniform := FilmGrainEffect.FindNode(TX3DNode, 'Shader').Field('rndx2', true) as TSFFloat;
  FilmGrainEffectRndY2Uniform := FilmGrainEffect.FindNode(TX3DNode, 'Shader').Field('rndy2', true) as TSFFloat;
end;

procedure TScreenEffect.Update(const SecondsPassed: Single;
  var HandleInput: Boolean);
begin
  inherited Update(SecondsPassed, HandleInput);
  FilmGrainEffectRndX1Uniform.Send(Rnd.Random);
  FilmGrainEffectRndY1Uniform.Send(Rnd.Random);
  FilmGrainEffectRndX2Uniform.Send(Rnd.Random);
  FilmGrainEffectRndY2Uniform.Send(Rnd.Random);
end;

constructor TScreenEffect.Create(AOwner: TComponent);
begin
  inherited;
  FullSize := true;
  Load; // TODO: optimise; load only once
  Enable;
end;

destructor TScreenEffect.Destroy;
begin
  FreeAndNil(FilmGrainEffectRoot);
  inherited Destroy;
end;

end.


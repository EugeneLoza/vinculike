{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ A button which has a larger click area than its image }
unit CastleBigButton;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  CastleControls, CastleVectors;

type
  { A TCastleButton, but captures click events also on borders.
    TODO : Make a plugin "Button with padding", but it's better to make a proper
    button with "Rollover image" and all the bells and whistles properly
    than to make a button for a specific usecase
    (don't repeat Mobile Button) }
  TCastleBigButton = class(TCastleButton)
  public
    function CapturesEventsAtPosition(const Position: TVector2): Boolean; override;
  end;

implementation
uses
  CastleComponentSerialize;

function TCastleBigButton.CapturesEventsAtPosition(const Position: TVector2): Boolean;
begin
  // override ---- reimplement parent completely
  Result := RenderRectWithBorder.Contains(Position);
end;


initialization
  RegisterSerializableComponent(TCastleBigButton, 'Big Button');
end.


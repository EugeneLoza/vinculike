{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Helpers for UI }
unit GameUiUtils;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  CastleUiControls;

type
  TCastleUserInterfaceHelper = class helper for TCastleUserInterface
  public
    { Simply clearing controls is not enough, we also need to free them
      even if they're owned by Parent element - they'll be freed on app shutdown
      creating hidden memory leaks, which may quickly spiral out of control
      if happens often, e.g. inventory buttons }
    procedure ClearAndFreeControls;
  end;

implementation

procedure TCastleUserInterfaceHelper.ClearAndFreeControls;
var
  C: TCastleUserInterface;
begin
  while ControlsCount > 0 do
  begin
    C := Controls[0];
    RemoveControl(C);
    FreeAndNil(C);
  end;
end;

end.


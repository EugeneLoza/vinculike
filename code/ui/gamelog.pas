{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Logs: for debug use (into log file only) and to display for the Player }
unit GameLog;

{$INCLUDE compilerconfig.inc}

interface

{$IFDEF DEBUG}
{$DEFINE VERBOSELOG}
{$ENDIF}

uses
  SysUtils,
  CastleColors;

{ Show log message to the player }
procedure ShowLog(const AMessage: String; const Args: array of Const; const AColor: TCastleColor);
{ Show error message to the player }
procedure ShowError(const AMessage: String; const Args: array of Const);
procedure ShowError(const AMessage: String);
{ Write a line to log file }
procedure LogNormal(const AMessage: String; const Args: array of Const); inline;
procedure LogNormal(const AMessage: String); inline;
procedure LogVerbose(const AMessage: String; const Args: array of Const); inline;
procedure LogVerbose(const AMessage: String); inline;
{ Write a log line to log file marked as warning }
procedure LogWarning(const AMessage: String; const Args: array of Const); inline;
procedure LogWarning(const AMessage: String); inline;
implementation
uses
  CastleLog,
  GameViewGame;

procedure ShowLog(const AMessage: String; const Args: array of const;
  const AColor: TCastleColor);
begin
  WriteLnLog('>' + AMessage, ARgs);
  if ViewGame.Active then
    ViewGame.AddLog(AMessage, Args, AColor);
end;

procedure ShowError(const AMessage: String; const Args: array of const);
begin
  ShowLog(AMessage, Args, CastleColors.Red);
end;

procedure ShowError(const AMessage: String);
begin
  ShowLog(AMessage, [], CastleColors.Red);
end;

procedure LogNormal(const AMessage: String; const Args: array of const);
begin
  WriteLnLog(AMessage, Args);
end;

procedure LogNormal(const AMessage: String);
begin
  WriteLnLog(AMessage);
end;

procedure LogVerbose(const AMessage: String; const Args: array of const);
begin
  {$IFDEF VERBOSELOG}
  WriteLnLog(AMessage, Args);
  {$ENDIF}
end;

procedure LogVerbose(const AMessage: String);
begin
  {$IFDEF VERBOSELOG}
  WriteLnLog(AMessage);
  {$ENDIF}
end;

procedure LogWarning(const AMessage: String; const Args: array of const);
begin
  WriteLnWarning(AMessage, Args);
end;

procedure LogWarning(const AMessage: String);
begin
  WriteLnWarning(AMessage);
end;

end.


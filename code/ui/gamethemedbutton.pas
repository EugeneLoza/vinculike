{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ helper to set up a button to match theme }
unit GameThemedButton;

{$INCLUDE compilerconfig.inc}

interface

uses
  CastleMobileButton;


type
  TThemedButtonHelper = class helper for TCastleMobileButton
  public
    procedure SetTheme(const UrlBase: String);
  end;

implementation
uses
  GameCachedImages;

procedure TThemedButtonHelper.SetTheme(const UrlBase: String);
begin
  CustomBackground := true;
  CustomBackgroundNormal.DrawableImage := LoadDrawable('castle-data:/ui/kodiakgraphics/' + UrlBase + '_normal.png');
  CustomBackgroundNormal.OwnsDrawableImage := false;
  CustomBackgroundNormal.ProtectedSides.AllSides := 15;
  CustomBackgroundFocused.DrawableImage := LoadDrawable('castle-data:/ui/kodiakgraphics/' + UrlBase + '_focus.png');
  CustomBackgroundFocused.OwnsDrawableImage := false;
  CustomBackgroundFocused.ProtectedSides.AllSides := 15;
  CustomBackgroundPressed.DrawableImage := LoadDrawable('castle-data:/ui/kodiakgraphics/' + UrlBase + '_press.png');
  CustomBackgroundPressed.OwnsDrawableImage := false;
  CustomBackgroundPressed.ProtectedSides.AllSides := 15;
  CustomBackgroundDisabled.DrawableImage := LoadDrawable('castle-data:/ui/kodiakgraphics/' + UrlBase + '_disabled.png');
  CustomBackgroundDisabled.OwnsDrawableImage := false;
  CustomBackgroundDisabled.ProtectedSides.AllSides := 15;
  Caption := '';
  AutoSize := false;
end;

end.


{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopediaPage;

{$INCLUDE compilerconfig.inc}

interface
uses
  DOM,
  CastleGlImages,
  GameVinculopediaPageAbstract;

type
  TVinculopediaPage = class(TVinculopediaPageAbstract)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableData,
  GameStoryMacros;

procedure TVinculopediaPage.Validate;
begin
  inherited;
  //image
end;

procedure TVinculopediaPage.Read(const Element: TDOMElement);
var
  ImageUrl: String;
begin
  inherited;
  Caption := HeaderMacros(Caption);
  ImageUrl := Element.AttributeStringDef('Image', '');
  if ImageUrl <> '' then
    Image := ImageHeaderMacros(FBaseUrl, ImageUrl);
end;

initialization
  RegisterSerializableData(TVinculopediaPage);

end.


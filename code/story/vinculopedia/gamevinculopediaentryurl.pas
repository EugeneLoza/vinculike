{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopediaEntryUrl;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  CastleUiControls,
  GameUnlockableEntry;

type
  TVinculopediaEntryUrl = class(TUnlockableEntry)
  private
    procedure ClickUrl(Sender: TObject);
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Caption: String;
    Url: String;
    procedure InsertEntry(AOwner: TCastleUserInterface); override;
  end;

implementation
uses
  CastleXmlUtils, CastleOpenDocument,
  GameSerializableData, GameVinculopediaTopicButton,
  GameColors, GameStoryMacros;

procedure TVinculopediaEntryUrl.ClickUrl(Sender: TObject);
begin
  OpenUrl(Url);
end;

procedure TVinculopediaEntryUrl.Validate;
begin
  inherited;
  if Caption = '' then
    raise EDataValidationError.Create('Caption = "" in TVinculopediaEntryUrl');
  if Url = '' then
    raise EDataValidationError.Create('Url = "" in TVinculopediaEntryUrl');
end;

procedure TVinculopediaEntryUrl.Read(const Element: TDOMElement);
begin
  inherited;
  Caption := HeaderMacros(Element.AttributeString('Caption'));
  Url := Element.AttributeString('Url');
end;

procedure TVinculopediaEntryUrl.InsertEntry(AOwner: TCastleUserInterface);
var
  U: TVinculopediaTopicButton;
begin
  U := TVinculopediaTopicButton.Create(AOwner); // TODO: Something better here
  U.Selected := false;
  U.Setup;
  if IsUnlocked then
  begin
    U.Caption := Caption;
    U.OnClick := @ClickUrl;
  end else
    U.Caption := '???????';
  U.AutoSize := true;
  U.AutoSizeHeight := true;
  U.AutoSizeWidth := true;
  U.CustomTextColor := ColorVinculopediaHeader;
  AOwner.InsertFront(U);
end;

initialization
  RegisterSerializableData(TVinculopediaEntryUrl);

end.


{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopediaPageAbstract;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM, Generics.Collections,
  CastleUiControls, CastleGlImages, CastleColors,
  GameUnlockableEntry;

type
  TUnlockKind = (ukNone, ukUnlocked, ukUpdated);
type
  TVinculopediaPageAbstract = class abstract(TUnlockableEntry)
  protected
    Entries: TEntriesList;
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Tab: String;
    Id: String;
    Caption: String;
    Image: TDrawableImage;
    TopicTextColor: TCastleColor;
    procedure InsertEntry(AOwner: TCastleUserInterface); override;
    function CheckUnlock(const AStat: String; const AStatValue: Integer): TUnlockKind;
  public
    destructor Destroy; override;
  end;
  TVinculopediaPagesList = specialize TObjectList<TVinculopediaPageAbstract>;

implementation
uses
  CastleXmlUtils,
  GameSerializableData,
  GameLog, GameColors;

procedure TVinculopediaPageAbstract.Validate;
begin
  inherited;
  if Tab = '' then
    raise EDataValidationError.Create('Tab = "" in TVinculopediaPageAbstract');
  if Entries.Count = 0 then
    raise EDataValidationError.CreateFmt('Vinculopedia page "%s" doesn''t contain any entries', [Caption]);
  //image
end;

procedure TVinculopediaPageAbstract.Read(const Element: TDOMElement);
var
  Iterator: TXMLElementIterator;
begin
  inherited;
  Tab := Element.AttributeString('Tab');
  Id := Element.AttributeString('Caption');
  Caption := Id;
  TopicTextColor := Element.AttributeColorDef('Color', ColorVinculopediaTopic);
  // Note: we don't read Image here
  Entries := TEntriesList.Create(true);
  try
    Iterator := Element.ChildrenIterator('Entry');
    while Iterator.GetNext do
    begin
      Entries.Add(TUnlockableEntry.ReadClass(FBaseUrl, Iterator.Current) as TUnlockableEntry);
    end;
  finally FreeAndNil(Iterator) end;
end;

destructor TVinculopediaPageAbstract.Destroy;
begin
  FreeAndNil(Entries);
  inherited Destroy;
end;

procedure TVinculopediaPageAbstract.InsertEntry(AOwner: TCastleUserInterface);
var
  I: Integer;
begin
  for I := 0 to Pred(Entries.Count) do
    Entries[I].InsertEntry(AOwner);
end;

function TVinculopediaPageAbstract.CheckUnlock(const AStat: String;
  const AStatValue: Integer): TUnlockKind;
var
  UE: TUnlockableEntry;
begin
  if Self.WillBeUnlocked(AStat, AStatValue) then
  begin
    ShowLog('New entry: "%s"', [Self.Caption], ColorLogVinculopedia);
    Exit(ukUnlocked);
  end else
  if Self.IsUnlocked then
    for UE in Entries do
      if UE.WillBeUnlocked(AStat, AStatValue) then
      begin
        ShowLog('Entry updated: "%s"', [Self.Caption], ColorLogVinculopedia);
        Exit(ukUpdated);
      end;
  Exit(ukNone);
end;


end.


{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopediaPageMonster;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, Classes, DOM,
  CastleGlImages,
  GameVinculopediaPageAbstract, GameMonsterData;

type
  TVinculopediaPageMonster = class(TVinculopediaPageAbstract)
  strict private
    MonsterData: TMonsterData;
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  end;

var
  MonstersEntries: TStringList;

procedure InitVinculopediaPagesMonsters;
procedure FreeVinculopediaPagesMonsters;
implementation
uses
  CastleXmlUtils,
  GameSerializableData,
  GameLog, GameUnlockableEntry,
  GameMonstersDatabase,
  GameVinculopediaEntryHeader, GameVinculopediaEntryMonsterStats,
  GameVinculopediaEntryText; // temporary

procedure TVinculopediaPageMonster.Validate;
begin
  inherited;
  //image
end;

procedure TVinculopediaPageMonster.Read(const Element: TDOMElement);

  function MonsterStatsEntry: TVinculopediaEntryMonsterStats;
  begin
    Result := TVinculopediaEntryMonsterStats.Create;
    Result.MonsterData := MonsterData;
    Result.UnlockStat := 'killed_' + MonsterData.Id;
    Result.UnlockValue := 12;
  end;

  function SpawnEntry(const Descript: String): TVinculopediaEntryText;
  begin
    Result := TVinculopediaEntryText.Create;
    Result.Contents := Descript;
    Result.UnlockStat := 'killed_' + MonsterData.Id;
    Result.UnlockValue := 12;
  end;

var
  BehaviorEntries: TEntriesList;
begin
  inherited;
  if MonstersEntries.IndexOf(Caption) >= 0 then
    LogWarning('Duplicate Vinculopedia monster entry: %s', [Caption])
  else
    MonstersEntries.Add(Caption);
  if MonstersDataDictionary.ContainsKey(Caption) then
    MonsterData := MonstersDataDictionary[Caption]
  else
    raise EDataValidationError.CreateFmt('Monster data %s was not found', [Caption]);
  Caption := MonsterData.DisplayName;
  UnlockStat := 'killed_' + MonsterData.Id;
  UnlockValue := 1;
  Image := MonsterData.Image;
  if Element.AttributeBooleanDef('Stats', true) then
  begin
    if MonsterData.Rare then
      Entries.Insert(0, SpawnEntry('Rare monster (cannot spawn or patrol as the only kind on the level)'));
    if MonsterData.StopPatrolAtDepth < 100 then
      Entries.Insert(0, SpawnEntry('Stops patroling at lvl. ' + MonsterData.StopPatrolAtDepth.ToString));
    if MonsterData.StartPatrolAtDepth < 100 then
      Entries.Insert(0, SpawnEntry('Starts patroling at lvl. ' + MonsterData.StartPatrolAtDepth.ToString));
    if MonsterData.SpawnFrequency < 1.0 then
      Entries.Insert(0, SpawnEntry('Spawn Frequency: ' + Round(100.0 * MonsterData.SpawnFrequency).Tostring + '%'));
    if MonsterData.StopSpawningAtDepth < 100 then
      Entries.Insert(0, SpawnEntry('Stops spawning at lvl. ' + MonsterData.StopSpawningAtDepth.ToString));
    if MonsterData.StartSpawningAtDepth < 100 then
      Entries.Insert(0, SpawnEntry('Starts spawning at lvl. ' + MonsterData.StartSpawningAtDepth.ToString));

    Entries.Insert(0, MonsterStatsEntry);
  end;
  Entries.Insert(0, NewEntryHeader(MonsterData.DisplayName, '', 0));
  BehaviorEntries := MonsterData.AiData.Description;
  if BehaviorEntries <> nil then
  begin
    Entries.AddRange(BehaviorEntries);
    FreeAndNil(BehaviorEntries);
  end;
end;


procedure InitVinculopediaPagesMonsters;
begin
  if MonstersEntries = nil then
    MonstersEntries := TStringList.Create
  else
    MonstersEntries.Clear;
end;

procedure FreeVinculopediaPagesMonsters;
begin
  // Just to make sure if something goes wrong during loading. It should be freed by Vinculopedia after validation
  FreeAndNil(MonstersEntries);
end;

initialization
  RegisterSerializableData(TVinculopediaPageMonster);

end.


{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopediaTab;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, Generics.Collections;

type
  TVinculopediaTab = class(TObject)
  public
    Id: String;
    Caption: String;
    constructor Create(const AId, ACaption: String);
  end;
  TVinculopediaTabsList = specialize TObjectList<TVinculopediaTab>;

implementation

constructor TVinculopediaTab.Create(const AId, ACaption: String);
begin
  Id := AId;
  Caption := ACaption;
end;

end.


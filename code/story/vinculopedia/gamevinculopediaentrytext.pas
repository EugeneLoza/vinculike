{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopediaEntryText;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  CastleUiControls,
  GameUnlockableEntry;

type
  TVinculopediaEntryText = class(TUnlockableEntry)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Contents: String;
    procedure InsertEntry(AOwner: TCastleUserInterface); override;
  end;

function NewEntryText(const Contents: String; const UnlockStat: String; const UnlockValue: Integer): TVinculopediaEntryText;
implementation
uses
  CastleXmlUtils, CastleControls,
  GameSerializableData,
  GameColors, GameFonts;

function NewEntryText(const Contents: String;
  const UnlockStat: String; const UnlockValue: Integer): TVinculopediaEntryText;
begin
  Result := TVinculopediaEntryText.Create;
  Result.Contents := Contents;
  Result.UnlockStat := UnlockStat;
  Result.UnlockValue := UnlockValue;
end;

procedure TVinculopediaEntryText.Validate;
begin
  inherited;
  if Contents = '' then
    raise EDataValidationError.Create('Contents = "" in TVinculopediaEntryText');
end;

procedure TVinculopediaEntryText.Read(const Element: TDOMElement);
begin
  inherited;
  Contents := Element.TextData;
end;

procedure TVinculopediaEntryText.InsertEntry(AOwner: TCastleUserInterface);
var
  L: TCastleLabel;
begin
  L := TCastleLabel.Create(AOwner);
  if IsUnlocked then
    L.Caption := Contents
  else
    L.Caption := '???????';
  L.Color := ColorDefault;
  L.CustomFont := FontBender40;
  L.Html := true;
  L.MaxWidth := 1300; // TODO: more generic
  AOwner.InsertFront(L);
end;

initialization
  RegisterSerializableData(TVinculopediaEntryText);

end.


{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopediaEntryReference;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  CastleUiControls,
  GameUnlockableEntry;

type
  TVinculopediaEntryReference = class(TUnlockableEntry)
  private
    procedure ClickReference(Sender: TObject);
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Reference: String;
    procedure InsertEntry(AOwner: TCastleUserInterface); override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableData, GameVinculopediaTopicButton,
  GameColors, GameViewVinculopedia, GameVinculopedia;

procedure TVinculopediaEntryReference.ClickReference(Sender: TObject);
begin
  ViewVinculopedia.OpenItem := Reference;
  ViewVinculopedia.RebuildView;
end;

procedure TVinculopediaEntryReference.Validate;
begin
  inherited;
  if Reference = '' then
    raise EDataValidationError.Create('Reference = "" in TVinculopediaEntryReference');
  {if Vinculopedia.GetPageById(Reference) = nil then
    raise EDataValidationError.CreateFmt('Reference = "%s" not found in Vinculopedia pages', [Reference]);}
end;

procedure TVinculopediaEntryReference.Read(const Element: TDOMElement);
begin
  inherited;
  Reference := Element.AttributeString('Reference');
end;

procedure TVinculopediaEntryReference.InsertEntry(AOwner: TCastleUserInterface);
var
  U: TVinculopediaTopicButton;
begin
  if Vinculopedia.GetPageById(Reference).IsUnlocked then // if the referenced page is not unlocked, just don't show "see also" at all
  begin
    U := TVinculopediaTopicButton.Create(AOwner); // TODO: Something better here
    U.Selected := false;
    U.Setup;
    U.Caption := 'See Also: ' + Vinculopedia.GetPageById(Reference).Caption;
    U.OnClick := @ClickReference;
    U.AutoSize := true;
    U.AutoSizeHeight := true;
    U.AutoSizeWidth := true;
    U.CustomTextColor := ColorVinculopediaHeader;
    AOwner.InsertFront(U);
  end;
end;

initialization
  RegisterSerializableData(TVinculopediaEntryReference);

end.


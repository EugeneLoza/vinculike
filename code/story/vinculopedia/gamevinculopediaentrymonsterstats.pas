{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopediaEntryMonsterStats;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  CastleUiControls,
  GameUnlockableEntry, GameMonsterData;

type
  TVinculopediaEntryMonsterStats = class(TUnlockableEntry)
  protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    MonsterData: TMonsterData;
    procedure InsertEntry(AOwner: TCastleUserInterface); override;
  end;

implementation
uses
  CastleXmlUtils, CastleControls,
  GameSerializableData,
  GameColors, GameFonts, GameMonstersDatabase;

procedure TVinculopediaEntryMonsterStats.Validate;
begin
  inherited;
end;

procedure TVinculopediaEntryMonsterStats.Read(const Element: TDOMElement);
var
  AName: String;
begin
  inherited;
  AName := Element.AttributeString('Monster');
  if MonstersDataDictionary.ContainsKey(AName) then
    MonsterData := MonstersDataDictionary[AName]
  else
    raise Exception.CreateFmt('Monster data "%s" was not found in monsters in %s.Read', [AName, Self.ClassName]);
end;

procedure TVinculopediaEntryMonsterStats.InsertEntry(AOwner: TCastleUserInterface);
var
  C: TCastleUserInterface;
  HorizontalGroup: TCastleHorizontalGroup;
  LabelLocked: TCastleLabel;

  procedure Stat(const Caption: String; const Value: Single);
  var
    Vert: TCastleVerticalGroup;
    L: TCastleLabel;
  begin
    Vert := TCastleVerticalGroup.Create(HorizontalGroup);
    Vert.Alignment := hpMiddle;
    Vert.Spacing := 10;
    HorizontalGroup.InsertFront(Vert);

    L := TCastleLabel.Create(Vert); // TODO: Image instead of name
    L.Caption := Caption;
    L.Color := ColorDefault;
    L.CustomFont := FontSoniano50;
    Vert.InsertFront(L);
    L := TCastleLabel.Create(Vert);
    L.Caption := Format('%.1n', [Value]);
    L.Color := ColorDefault;
    L.CustomFont := FontSoniano50;
    Vert.InsertFront(L);
  end;

begin
  if IsUnlocked then
  begin
    // TODO: Read from design
    C := TCastleUserInterface.Create(AOwner);
    C.Width := 1300;
    C.Height := 100;
    AOwner.InsertFront(C);

    HorizontalGroup := TCastleHorizontalGroup.Create(C);
    HorizontalGroup.Spacing := 20;
    C.InsertFront(HorizontalGroup);

    Stat('SIZE', MonsterData.Size);
    Stat('HP', MonsterData.MaxHealth);
    Stat('SPD', MonsterData.MovementSpeed);
    Stat('NOISE', MonsterData.Noisiness);
    if MonsterData.VisionBonus > 0 then
      Stat('VIS+', MonsterData.VisionBonus);
    if MonsterData.VisionTopNaked > 0 then
      Stat('TOP-N', MonsterData.VisionTopNaked);
    if MonsterData.VisionTopClothed > 0 then
      Stat('TOP-D', MonsterData.VisionTopClothed);
    if MonsterData.VisionBottomNaked > 0 then
      Stat('BTM-N', MonsterData.VisionBottomNaked);
    if MonsterData.VisionBottomClothed > 0 then
      Stat('BTM-D', MonsterData.VisionBottomClothed);
    Stat('XP', MonsterData.XP);
  end else
  begin
    LabelLocked := TCastleLabel.Create(AOwner);
    LabelLocked.Caption := '???????';
    LabelLocked.Color := ColorDefault;
    LabelLocked.CustomFont := FontBender40;
    AOwner.InsertFront(LabelLocked);
  end;
end;

initialization
  RegisterSerializableData(TVinculopediaEntryMonsterStats);

end.


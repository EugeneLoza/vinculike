{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopediaTabButton;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  CastleMobileButton,
  GameVinculopediaTab;

type
  TVinculopediaTabButton = class(TCastleMobileButton)
  public
    Selected: Boolean;
    Tab: TVinculopediaTab;
    procedure Setup;
  end;

implementation
uses
  GameFonts, GameColors, GameThemedButton;

procedure TVinculopediaTabButton.Setup;
begin
  if Selected then
    SetTheme('inventory_button_light_selected')
  else
    SetTheme('inventory_button_light');
  Caption := '';
  Height := 70;
  EnableParentDragging := true;
  CustomFont := FontSoniano50;
  CustomTextColorUse := true;
  CustomTextColor := ColorDefault;
end;

end.


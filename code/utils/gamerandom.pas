{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Holds global random numbers generator
  Note, some game mechanics can use own random generator or
  pseudo random transformation
  some cases of the latter are also done here }
unit GameRandom;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils,
  CastleRandom;

var
  { Global non-deterministic random numbers generator}
  Rnd: TCastleRandom;

{ Get a random number based on seed }
function GetRandomFromSeed64(const Seed: UInt64): Single;
function GetRandomFromSeed64(const Seed: UInt64; const N: UInt64): UInt64;

{ init/free global random generator }
procedure InitRandom;
procedure FreeRandom;
implementation

function CycleSeed(const Seed: UInt64): UInt64; inline;
var
  Seed64: UInt64;
  procedure XorShiftCycle; inline;
  begin
    Seed64 := Seed64 xor (Seed64 shl 12);
    Seed64 := Seed64 xor (Seed64 shr 25);
    Seed64 := Seed64 xor (Seed64 shl 27);
  end;
begin
  Seed64 := Seed;
  XorShiftCycle;
  XorShiftCycle;
  XorShiftCycle;
  XorShiftCycle;
  XorShiftCycle;
  Exit(Seed64);
end;

function GetRandomFromSeed64(const Seed: UInt64): Single;
begin
  Exit(Single(CycleSeed(Seed)) / UInt64.MaxValue);
end;

function GetRandomFromSeed64(const Seed: UInt64; const N: UInt64): UInt64;
begin
  Exit(CycleSeed(Seed) mod N);
end;

procedure InitRandom;
begin
  Rnd := TCastleRandom.Create;
end;

procedure FreeRandom;
begin
  FreeAndNil(Rnd);
end;

end.


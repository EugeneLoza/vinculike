{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Serialization of enums }
unit GameEnumUtils;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, Generics.Collections;

const
  EnumDelimiter = Char(',');

type
  // must be in interface, otherwise Error: Generic template in interface section references symbol in implementation section
  EEnumConversionError = class(Exception);

generic function EnumToStr<T>(const EnumValue: T): String;
generic function StrToEnum<T>(const EnumString: String): T;
generic function StrToEnumsList<T>(const EnumsString: String): specialize TList<T>;
generic function ListToEnumsStr<T>(const EnumsList: specialize TList<T>): String;
{type
  //Workaround: it's impossible to have function result as "Set of T" so we need to declare the type first, see https://forum.lazarus.freepascal.org/index.php/topic,68230.msg526877.html#msg526877
  generic TStrToEnumsSet<T> = class(TObject)
    type TEnumsSet = set of T;
    class function StrToEnumsSet(const EnumsString: String): TEnumsSet;
  end;}

implementation
uses
  TypInfo,
  CastleStringUtils;

generic function EnumToStr<T>(const EnumValue: T): String;
begin
  Exit(GetEnumName(TypeInfo(T), Ord(EnumValue)));
end;

generic function StrToEnum<T>(const EnumString: String): T;
var
  I: T;
begin
  // TODO: Optimize? // right now happens only while loading the game, not every frame, so no need to
  for I in T do
    if specialize EnumToStr<T>(I) = EnumString then
      Exit(I);
  raise EEnumConversionError.CreateFmt('Cannot find %s value: "%s"', [PTypeInfo(TypeInfo(T))^.Name, EnumString]);
end;

generic function StrToEnumsList<T>(const EnumsString: String): specialize TList<T>;
var
  EnumStringList: TCastleStringList;
  S: String;
begin
  EnumStringList := CreateTokens(EnumsString, [EnumDelimiter]);
  Result := (specialize TList<T>).Create;
  for S in EnumStringList do
    Result.Add(specialize StrToEnum<T>(S));
  FreeAndNil(EnumStringList);
end;

generic function ListToEnumsStr<T>(const EnumsList: specialize TList<T>): String;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to Pred(EnumsList.Count) do
  begin
    if I > 0 then
      Result := Result + EnumDelimiter;
    Result := Result + specialize EnumToStr<T>(EnumsList[I]);
  end;
end;

{class function TStrToEnumsSet.StrToEnumsSet(const EnumsString: String): TEnumsSet;
var
  EnumStringList: TCastleStringList;
  S: String;
begin
  EnumStringList := CreateTokens(AString, [EnumDelimiter]);
  Result := [];
  for S in EnumStringList do
    Result := Result + [specialize StrToEnum<T>(S)];
  FreeAndNil(EnumStringList);
end;}

end.


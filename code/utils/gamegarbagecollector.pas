{ Copyright (C) 2023-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ This is a workaround for situations when something must cease to exist immediately
  but it still needs to perform several operations before the memory is freed
  it may be not the best programming solution, but if a few languages use this
  workaround on core level, e.g. C# (sic!), then we can too
  the objects added will be freed around the end of the frame,
  but note specific implementation in TViewGame.Update }
unit GameGarbageCollector;

{$INCLUDE compilerconfig.inc}

interface
uses
  Generics.Collections;

type
  { An object that can be garbage-collected
    but not necessarily has to be }
  TGarbageCollectObject = class(TObject)
  private // note: not strict-private, accessible to TGarbageCollector but not to anyone else
    QueuedForFreeing: Boolean;
  public
    destructor Destroy; override;
  end;
  TGarbageCollectObjectList = specialize TObjectList<TGarbageCollectObject>;

type
  { Handler and container for garbage collected objects }
  TGarbageCollector = class(TObject)
  private
    FreeingQueue: TGarbageCollectObjectList;
  public
    { Queue class for freeing and set it to nil }
    procedure AddAndNil(var AClass);
    { Equivalent of FreeAndNil }
    procedure FreeNowAndNil(var AClass);
    { Undo adding class to free queue }
    procedure Remove(const AClass: TGarbageCollectObject);
    { Clears free queue and frees its content }
    procedure Clear;
    constructor Create; //override;
    destructor Destroy; override;
  end;

var
  GarbageCollector: TGarbageCollector;

procedure InitGarbageCollector;
procedure FreeGarbageCollector;
implementation
uses
  SysUtils,
  GameLog;

{ TGarbageCollectObject ------------------------------------------------------------------- }

destructor TGarbageCollectObject.Destroy;
begin
  if QueuedForFreeing then
  begin
    LogWarning('%s was queued for freeing but is freed manually!', [Self.ClassName]);
    GarbageCollector.Remove(Self);
  end;
  inherited Destroy;
end;

{ TGarbageCollector ----------------------------------------------------------------------- }

procedure TGarbageCollector.AddAndNil(var AClass);
var
  Temp: TObject;
begin
  { Mimicking FreeAndNil behavior here }
  Temp := TObject(AClass);
  Pointer(AClass) := nil;
  if Temp is TGarbageCollectObject then
  begin
    TGarbageCollectObject(Temp).QueuedForFreeing := true;
    FreeingQueue.Add(TGarbageCollectObject(Temp));
  end;
end;

procedure TGarbageCollector.FreeNowAndNil(var AClass);
var
  Temp: TObject;
begin
  { Mimicking FreeAndNil behavior here }
  Temp := TObject(AClass);
  Pointer(AClass) := nil;
  if Temp is TGarbageCollectObject then
  begin
    if TGarbageCollectObject(Temp).QueuedForFreeing then
    begin
      TGarbageCollectObject(Temp).QueuedForFreeing := false;
      FreeingQueue.Remove(TGarbageCollectObject(Temp)); // will also free it
    end else
      Temp.Free;
  end;
end;

procedure TGarbageCollector.Remove(const AClass: TGarbageCollectObject);
begin
  AClass.QueuedForFreeing := false;
  FreeingQueue.Remove(AClass)
end;

procedure TGarbageCollector.Clear;
var
  I: Integer;
begin
  for I := 0 to Pred(FreeingQueue.Count) do
    FreeingQueue[I].QueuedForFreeing := false; // to avoid call-back
  FreeingQueue.Clear;
end;

constructor TGarbageCollector.Create;
begin
  inherited; // parent is non-virtual and empty
  FreeingQueue := TGarbageCollectObjectList.Create(true);
end;

destructor TGarbageCollector.Destroy;
begin
  Clear; // to avoid callback
  FreeAndNil(FreeingQueue);
  inherited Destroy;
end;

procedure InitGarbageCollector;
begin
  GarbageCollector := TGarbageCollector.Create;
end;

procedure FreeGarbageCollector;
begin
  FreeAndNil(GarbageCollector);
end;

end.


{ Copyright (C) 2022-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Some inlined and overoptimized math operations
  also makes sure to avoid ambiguous overloading}
unit GameMath;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils;

{ Returns sing of the value: +1, 0, -1 }
function Sign(const A: Integer): Integer; inline;
function Sign(const A: Single): Integer; inline;
{ Returns minimum or maximum of the two values }
function MinInteger(const A, B: Integer): Integer; inline;
function MaxInteger(const A, B: Integer): Integer; inline;
function Min16(const A, B: Int16): Integer; inline;
function Max16(const A, B: Int16): Integer; inline;
function MinSingle(const A, B: Single): Single; inline;
function MaxSingle(const A, B: Single): Single; inline;
{ Simplified A ^ B
  Note, it skips some checks and optimizations of Math.Power so use that one instead of you are not sure }
function PowerSingle(const A, B: Single): Single; inline;
{ Clamp Value between Min and Max }
function Clamp(const Value, Min, Max: Single): Single; inline;
{ Makes sure the value is not less than Min }
function ClampMin(const Value, Min: Single): Single; inline;
{ Make sure value is zero or higher }
function ClampZeroOrHigher(const Value: Single): Single; inline;
{ Nearest integer higher than or equal to Value }
function Ceil(const Value: Single): Integer; inline;
implementation

function Sign(const A: Integer): Integer; inline;
begin
  if A > 0 then
    Exit(1)
  else
  if A < 0 then
    Exit(-1)
  else
    Exit(0);
end;

function Sign(const A: Single): Integer;
begin
  if A > 0 then
    Exit(1)
  else
  if A < 0 then
    Exit(-1)
  else
    Exit(0);
end;

function MinInteger(const A, B: Integer): Integer;
begin
  if A < B then
    Exit(A)
  else
    Exit(B);
end;

function MaxInteger(const A, B: Integer): Integer;
begin
  if A > B then
    Exit(A)
  else
    Exit(B);
end;

function Min16(const A, B: Int16): Integer;
begin
  if A < B then
    Exit(A)
  else
    Exit(B);
end;

function Max16(const A, B: Int16): Integer;
begin
  if A > B then
    Exit(A)
  else
    Exit(B);
end;

function MinSingle(const A, B: Single): Single;
begin
  if A < B then
    Exit(A)
  else
    Exit(B);
end;

function MaxSingle(const A, B: Single): Single;
begin
  if A > B then
    Exit(A)
  else
    Exit(B);
end;

function PowerSingle(const A, B: Single): Single;
begin
  if A = 0 then
    Exit(0)
  else
  if B = 0 then
    Exit(1.0)
  else
    Exit(Exp(B * Ln(A))); // Cropped copy of FPC Math.Power; we don't seem to make any use of their optimizations more often losing than winning
end;

function Clamp(const Value, Min, Max: Single): Single;
begin
  if Value >= Max then
    Exit(Max)
  else
  if Value <= Min then
    Exit(Min)
  else
    Exit(Value);
end;

function ClampMin(const Value, Min: Single): Single;
begin
  if Value <= Min then
    Exit(Min)
  else
    Exit(Value);
end;

function ClampZeroOrHigher(const Value: Single): Single;
begin
  if Value <= 0 then
    Exit(0)
  else
    Exit(Value);
end;

function Ceil(const Value: Single): Integer;
begin
  Exit(Trunc(Value) + Ord(Frac(Value) > 0)); // copy of FPC Math.Ceil
end;

end.

